<?php
require_once 'config/settings.php';
require_once 'model/users.php';

$userObj = new Model_Users();

$app_id = FACEBOOK_APPID; //Application ID 246201505539086
$app_secret = FACEBOOK_APP_SECRETE_KEY; // Secret ID 68b6d0e3c58d164534133e1a54026cc9
$my_url = "http://www.sportzmad.com/facebook.php"; // Redirect URL
//session_start();
$code = $_REQUEST["code"];

if (empty($code)) {
    $_SESSION['state'] = md5(uniqid(rand(), TRUE)); //CSRF protection
    $dialog_url = "https://www.facebook.com/dialog/oauth?client_id=" . $app_id . "&redirect_uri=" . urlencode($my_url) . "&state=" . $_SESSION['state'] . "&scope=email,read_stream,offline_access,publish_stream";
    echo("<script type='text/javascript'> window.location='" . $dialog_url . "';</script>");
}

if ($_REQUEST['state'] == $_SESSION['state']) {

    $token_url = "https://graph.facebook.com/oauth/access_token?client_id=" . $app_id . "&redirect_uri=" . urlencode($my_url) . "&client_secret=" . $app_secret . "&code=" . $code;
    $response = @file_get_contents($token_url);
    $params = null;
    parse_str($response, $params);
    $graph_url = "https://graph.facebook.com/me?access_token=" . $params['access_token'];
    $user = json_decode(file_get_contents($graph_url));
   if ($user) {
     // echo '<pre>';print_r($user);exit;

        $fbUId = $user->id;
        $email = $user->email;

        $rs = $userObj->fetchFacebookUserByfbuid($fbUId);
        // echo '<pre>';print_r($rs);exit;
        if (count($rs) < 1) {     // add record        
            $insertArray = array();
            $insertArray['fbUId'] = $user->id;
            $insertArray['username'] = $user->username;
            $insertArray['firstName'] = $user->first_name;
            $insertArray['lastName'] = $user->last_name;
            $insertArray['email'] = $user->email;
            $insertArray['regDate'] = date("Y-m-d H:i:s");
            $insertArray['status'] = 2;
            $insertArray['regIP'] = $_SERVER['REMOTE_ADDR'];
            //$insertArray['fbAccessToken']		= $params['access_token']; 	

            /* $updateAr['facebook_image']	= "http://graph.facebook.com/".$user->id."/picture?type=large";			
              $img = file_get_contents('http://graph.facebook.com/'.$user->id.'/picture?type=large');
              $file = 'dynamicAssets/user/'.$user->id.'.jpg';
              $imgName = $user->id.'.jpg';
              $insertArray['avatar']		= $imgName;
              file_put_contents($file, $img);

              $imageFolder="dynamicAssets/user/";
              $thumbFolder ="dynamicAssets/user/100x100/";
              $height = 100;
              $width= 100;
              $file1 = uploadImageAndCreateThumb($user->id.'.jpg',$imageFolder,$thumbFolder,$height,$width,'jpg'); */

            ## Add New User
            $userId = $userObj->addUserByValue($insertArray);

            $_SESSION['bakkenshipUserId'] = $userId;
            $_SESSION['email'] = $user->email;
            $_SESSION['FbUserId'] = $fbUId;
            $_SESSION['profileName'] = $user->first_name . " " . $user->last_name;

            # add login details 
            $userLoginArr = array();
            $userLoginArr['user_id'] = $userId;
            $userLoginArr['loginTime'] = date('Y-m-d H:i:s');
            $userLoginArr['loginIP'] = $_SERVER['REMOTE_ADDR'];
            $userLoginArr['logStatus'] = '1';
            $uLogID = $userObj->addLoginDetailsByValue($userLoginArr);
            ## creating login session
            $_SESSION['uLogID'] = $uLogID;

            header("Location:" . SITE_URL . "/users/profileType");
            exit();
        } else {
            $details = $userObj->getuserdetailsByuidandfbid($_SESSION['bakkenshipUserId'], $user->id);

            $usercnt = count($details);
            if ($usercnt < 1) {
                //update record     
                $_SESSION['bakkenshipUserId'] = $rs['userId'];
                $_SESSION['email'] = $user->email;
                $_SESSION['bakkenshipuserType'] = $rs['userType'];
                $_SESSION['bakkenshipUserName'] = $rs['username'];
                $_SESSION['profileName'] = $user->first_name . " " . $user->last_name;
                $_SESSION['FbUserId'] = $fbUId;

                # add login details 
                $userLoginArr = array();
                $userLoginArr['user_id'] = $userId;
                $userLoginArr['loginTime'] = date('Y-m-d H:i:s');
                $userLoginArr['loginIP'] = $_SERVER['REMOTE_ADDR'];
                $userLoginArr['logStatus'] = '1';
                $uLogID = $userObj->addLoginDetailsByValue($userLoginArr);
                ## creating login session
                $_SESSION['uLogID'] = $uLogID;
                if ($_SESSION['bakkenshipuserType'] == '3') {
                    header("Location:" . SITE_URL . "/customer/profile");
                    exit();
                } elseif ($_SESSION['bakkenshipuserType'] == '4') {
                    header("Location:" . SITE_URL . "/transporter/profile");
                }
            } else {
                $_SESSION['msg'] = '<section class="error_message">Already a account is connected by this details, Please login with different details.</section>';
                $url = SITE_URL;

                if ($_SESSION['bakkenshipuserType'] == '3') {
                    header("Location:" . SITE_URL . "/customer/profile");
                    exit();
                } elseif ($_SESSION['bakkenshipuserType'] == '4') {
                    header("Location:" . SITE_URL . "/transporter/profile");
                }
            }
        }
    }
} else {
    echo("The state does not match. You may be a victim of CSRF.");
}
?>
