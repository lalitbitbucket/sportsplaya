<?php
require_once 'config/settings.php';

// print_r($otheruserDetails);
//isLogin();

$jobsObj = new Model_Jobs();
$sportsObj = new Model_Sports();
$countryObj = new Model_Country();
$clubObj = new Model_Club();

$offset = $_POST['offset'];
if(isset($_POST['change_sport'])){

$sports_id      =    $_POST['sportsId'];    // Search SportsId
$change_sport   =    $_POST['change_sport']; 
$selectCountry     =   $_POST['selectCountry'];   // Search CityId
     
$jobsArray =  $jobsObj->getJobsDataBySpIdandCId($sports_id,$selectCountry,$change_sport,2,$offset);

$count_hidden = "";
if($change_sport!=0)
{   
    $countalbumArr =  $jobsObj->getJobsDataBySpIdandCId($sports_id,$selectCountry,$change_sport);
    $total_change_sport_flag  = count($countalbumArr);
    $count_hidden = '<input type="hidden" name="hidden_total" id="hidden_total" value="'.$total_change_sport_flag.'">';
}
if($count_hidden!="")
{
   echo $count_hidden;
}

foreach($jobsArray as $jobData) 
{
    $country = $countryObj->getCountryNameByCountryId($jobData['job_country']);
    if($jobData['sports']>0){
        $sports = $sportsObj->getSportsImageByid($jobData['sports']);
        $sportsTitle = $sports['sportsTitle'];
    }else{
        $sportsTitle = 'N/A';
    }
    $jobsName = trim(strtolower(str_replace(" ","_",$jobData['job_title'])));
    
    if($jobData['adzuna_job_id']<=0)
    {
        if($jobData['userType']==6){
            $clubArr = $clubObj->getClubDetailsByUserId($jobData['userId']);
            $club = $clubArr['clubName'];
        }
        else{
            $club = '';
        }
    }   
    else
    {
        if($jobData['company_name']!=''){
            $club = $jobData['company_name'].' - '.$jobData['job_address']; 
        }
        else{
            $club='';
        }
    }



    if($jobData['job_image']!=''){
      $images = SITE_URL.'/dynamicAssets/jobs/'.$jobData['job_image'];
    }else{
      $images = SITE_URL.'/siteAssets/images/newimages/club_logo_img.jpg';
    }

echo'
    <div class="followersdiv-set col-md-12 m-l0 " id="left_followersdiv_set">
    <div class="row border-bottom_dot4 p-tb27">

    <div class="col-xs-2 p-lr0">
        <img src="'.$images.'" title="" class="col-xs-12 col-md-11" />
    </div>
    <div class="col-xs-10 col-md-7">
            <p class="text-dtan p-22 text-segoe m-b0">'.$club.'</p>
            <h4 class="p-40 text-dgrey m-t0">'.$jobData['job_title'].'</h4>
                <p>'.$jobData['job_description'].'</p>
                <ul class="play_details">
                    <li class="col-xs-12 col-md-2 m-l0 p-lr0"><span class="icon sport m-l0"></span>'.$sportsTitle.'</li>
                    <li class="col-xs-12 col-md-2 m-l0 p-lr0"><span class="icon club m-l0"></span>'.$jobData['job_type'].'</li>
                    <li class="col-xs-12 col-md-5 m-l0 p-lr0"><span class="icon location m-l0"></span>'.$country.'</li>
                </ul>
        </div>
        <div class="col-xs-10 col-md-3">
                <p>
                    
                    <button class="btn text-uc p-20 font-uc bg-grey text-white m-b18 col-xs-12"';
                    ?> 
                    onclick="window.location.href='<?php echo SITE_URL.'/jobsdetails/'.$jobsName.'/'.base64_encode($jobData['id']); ?>'">
                    VIEW JOB DESCRIPTION
                    </button>

            <?php 
                echo '</p>
                    <p>
                    <button class="btn text-uc p-20 font-uc bg-grey text-white m-b40 col-xs-12"';
                    ?> 
                    onclick="window.location.href='<?php echo SITE_URL.'/jobsdetails/'.$jobsName.'/'.base64_encode($jobData['id']); ?>'">
                    APPLY FOR THIS JOB
                    </button>
                <?php echo '    
                </p>
        </div>
    </div>
</div>';

}
}
?>
