<?php
require_once 'config/settings.php';
$videosObj = new Model_videos();

$page_body_class = 'home_page';
$smarty->assign ('page_body_class', $page_body_class);

$siteurl = SITE_URL.'/';


if($_SESSION['stagUserId']!=''){
    $Logged_userId  =   $_SESSION['stagUserProfileId'];
    $userProfileId = $_SESSION['stagUserId'];
	$checkLogin =1;
}
else{
	$checkLogin =0;
}

$uID = $_REQUEST['uId'];
if ($uID=='') {
    header('location:'.SITE_URL.'/');
    exit;
}
$total_views = $videosObj->countTotalClickOnVideoByVideoId($uID);

$videoArray = $videosObj->getVideoByVideoId($uID);

$getComments = $videosObj->getCommentsByNomineesId(1, $uID);

$votingDtl = $videosObj->getVotingCountByUserId($userProfileId,$uID);
$getAlbumDtl = $videosObj->getAlbumDetailsByAlbumId($videoArray['albumId']);
$total_voting = $videosObj->getVotingPercentByVideoId($uID);
    
    if($videoArray['sportsid']!=0){
        $sports_arr = $sportsObj->getSportsDetailById($videoArray['sportsid']);
        $sports_name = $sports_arr['sportsTitle'];
    }
    else{
        $sports_name =  $videoArray['sportsTitle'];
    }

    if($videoArray['video_type']=='vimeo')
    {
        $videoArray['vcode'] = str_replace("\\","",$videoArray['vcode']);
        $video_url = explode('"', $videoArray['vcode']);
        $videoArray['video_url'] = $video_url[1]."?autoplay=1"; 
    }
    if($videoArray['video_type']=='youtube')
    {
        $videoArray['vcode'] = str_replace("\\","",$videoArray['vcode']);
        preg_match('/src="([^"]+)"/', $videoArray['vcode'], $match);
        $url = $match[1];
        $arr = explode("/",$url);
        $url= $arr[count($arr)-1];
        $videoArray['video_url'] ="https://www.youtube.com/embed/".$url."?autoplay=1";
    }
    
    $data =0;
	if($videoArray['video_type']=='youtubeurl'){
        $data =1;
        $url = $videoArray['vccode'];
        $videoArray['vccode'] = str_replace("\\","",$videoArray['vccode']);
        $regex_pattern = "/(youtube.com|youtu.be)\/(watch)?(\?v=)?(\S+)?/";
        //$match;
        if(preg_match($regex_pattern, $url, $match)){
            $videoArray['video_url'] = 'https://www.youtube.com/embed/'.$match[4].'?autoplay=1';
        } 
	   	/*$videoArray['vccode'] = str_replace("\\","",$videoArray['vccode']);
	   	$arr = explode("=",$videoArray['vccode']);
	 	$url= $arr[count($arr)-1];
		$videoArray['video_url'] = "https://www.youtube.com/embed/".trim($url)."?autoplay=1";*/	
	}
    
    if($videoArray['video_type']=='video'){
        $videoArray['video_url'] = SITE_URL.'/dynamicAssets/videos/'.$videoArray['video'].'?autoplay=1';
    }
    $album  = trim(strtolower(str_replace(" ","_",$getAlbumDtl['title'])));
?>
<link rel="stylesheet" type="text/css" href="siteAssets/responsive/css/bootstrap.css" />
<link rel="stylesheet" type="text/css" href="siteAssets/css/bootstrap-responsive.css" />
<link rel="stylesheet" type="text/css" href="siteAssets/css/styles.css" />
<link rel="stylesheet" type="text/css" href="siteAssets/css/fonts.css" />
<link rel="stylesheet" type="text/css" href="siteAssets/css/mobile.css" />
<style>
.font-bold{font-weight:bold !important;}
@media (min-width: 1024px){
html {font-size: 18px !important;}
}

@media (max-width: 767px) {
body {padding-right: 0 !important;padding-left: 0 !important;}
.mfp-iframe-holder{padding-top:0 !important;padding-bottom:0 !important;}
.mfp-container{padding:0 !important;}
.login_popup h1{margin-top:0;}
.video_popup #custom-content{ padding:3rem 1rem 1rem !important;}
.white-popup-block{background:#ffffff;}
}
	</style>

<div class="ajax-text-and-image white-popup-block">
    <div class="video_popup" >
<div class="col-xs-12 col-md-4">
            <div id="custom-content" style="padding:1rem 3rem;">
                <div class="col-xs-12 col-md-12 text-small-center p-lr0">
                    <h1 class="video_title font-segoe font-bold col-xs-12 col-md-10 p-lr0 m-t0">
					<?php echo stripcslashes($getAlbumDtl['title']); ?></h1>
                    <p class="text-grey p-b18 m-b0 text-small-center">
					<?php echo $videoArray['fname'].''.$videoArray['lname'].' ('.$videoArray['age'].')';?></p>
                </div>
                <div class="col-xs-7 col-md-6 p-lr0">
                    <p class="p-20 font-segoe font-bold m-b5"><strong>Rugby</strong></p>
                    <p class="text-grey p-b18 m-b0"><?php echo $sports_name; ?></p>
                </div>
                <div class="col-xs-5 col-md-6 p-lr0">
                    <p  class="p-20 font-segoe font-bold m-b5"><strong>Posted</strong></p>
                    <p class="text-grey p-b18 m-b0"><?php if($videoArray['date_diff'] > 0){echo $videoArray['date_diff']. ' days ago';}else{echo 'Just an hour ago.';}?></p>
                </div>
                <div class="col-xs-12 text-small-center p-lr0">
                    <p class="p-74 font-segoe p-b18 text-grey m-b0"><?php echo $total_voting['voteper'];?>%</p>
                    <p class="text-grey p-b18 m-b0"><?php echo $total_voting['total'];?> Votes</p>
                </div>
                
                <div style="margin-top: 12%;" id="button-box-area">
                <?php 
                if($checkLogin ==0){
                ?>
                <p>
                <button onclick="window.location.href='login_popup.php?uId=<?php echo $uID; ?>'" name="" class="col-xs-12 col-md-12 btn text-uc p-20 font-uc bg-grey text-white m-b0">Vote on this video</button>
                <?php } else{?>
                <?php if($videoArray['userprofile_id']!=$Logged_userId){?>
                <?php if($votingDtl['voting'] == 0 ){?>
                <button onclick="return videorating(<?php echo $uID.','.$userProfileId; ?>,1);"
			 	style="background-color: green;color: #fff;" class="col-xs-12 col-md-4 pull-left btn bg-green p-lr9 font-uc p-18 text-white" name="videovoting" id="videovoting" value="vote">Yeah!</button>
                <button onclick="return videorating(<?php echo $uID.','.$userProfileId; ?>,0);"
                style="background-color: red;color: #fff;" class="col-xs-12 col-md-4 pull-right btn bg-brickRed p-lr9 font-uc p-18 text-white" name="canclevoting" id="canclevoting">Nah!</button>
                <?php }
                
                else{
                   echo '<div  class="col-xs-12 col-md-12 font-bold btn bg-transparent p-lr9 p-18" style="color: red;">You have already voted. . .</div>';
                }
                }
                }
                ?>
				</p>
                </div>
                <div id="success-msg"  class="col-xs-12 col-md-12 font-bold  btn bg-transparent p-lr9 p-18" style="display: none;color: red;">Thanks For Voting. . .</div>
                <div class="col-xs-12 text-small-center p-lr0" style="padding-top: 7%;">
                </div>
                
                <div class="col-xs-12 p-lr0">
                <ul class="play_details" id="comment_box">
					<?php if(count($getComments) <= 0){ ?>
	                    <li class="p-b18">
                        <p class="m-b0 text-dgrey">No Comments. . .</p>
                        </li>
                    <?php }else{    
                    foreach ($getComments as $comment) { ?>
                        <li class="p-b18 m-8b0">
                        <p class="m-b0 text-dgrey">
                        <strong><?php echo $comment['fname'].' '.$comment['lname'];?>
						<?php // echo $comment['date_formated'];?>
                        </strong>
                        </p>
                        <p class="text-lgrey m-b0"><?php echo substr($comment['comment_text'],0,75)." ....";?></p>
                        </li>
                    <?php } }?>                
                </ul>
                <p class="text-grey p-b18 m-b0">
                 <a onclick="top.window.location.href='<?php echo SITE_URL; ?>/singlenominess/<?php echo $album;?>/<?php echo base64_encode($uID); ?>'" href="javascript:void(0);" class="col-xs-12 col-md-12 btn text-uc p-20 font-uc bg-dgrey text-white m-b18">Add a comment</a>
             </p>
                </div>


            </div>
        </div>        
        <div class="col-xs-12 col-md-8 p-lr0">

        <!-- <?php //if ($videoArray['video_type']=='video'){ ?>    
        <video autoplay="true" width="100%" height="80%" controls controlsList="nodownload">
            <source src="<?php //echo $videoArray['video_url'];?>" type="video/mp4" class="popup_video">
        </video>
        <?php //}else{?> -->
        <iframe width="100%" height="80%" src="<?php echo $videoArray['video_url'];?>" frameborder="0" class="popup_video" allowfullscreen></iframe>
        <!-- <?php// }?> -->
    </div>        
	<div style="clear:both; line-height: 0;"></div>
	
</div>
<script src="https://code.jquery.com/jquery-3.2.1.js"></script>
<script src="<?php echo SITE_URL; ?>/siteAssets/js/videovoting.js"></script>
<script>
document.addEventListener("touchmove",function(event){event.preventDefault();});
</script>