<?php
require_once 'config/settings.php';
require_once 'model/videos.php';

$siteurl = SITE_URL.'/';
if(empty($_SERVER['HTTP_REFERER']) || $_SERVER['HTTP_REFERER']!= $siteurl){
    header('location:'.SITE_URL.'/');
    exit;    
}


$videosObj = new Model_videos();
$vId = $_REQUEST['vId'];

$total_views = $videosObj->countTotalClickOnVideoByVideoId($vId);
$videoArray = $videosObj->getPlayaOfDayById($vId);
if($videoArray['video_type']=='vimeo'){
		$videoArray['video_url'] = str_replace("\\","",$videoArray['video_url']);
	    $video_url = explode('"', $videoArray['video_url']);
	    $videoArray['videourl'] = $video_url[1]."?autoplay=1"; 
	}
	if($videoArray['video_type']=='youtube'){
		$videoArray['video_url'] = str_replace("\\","",$videoArray['video_url']);
	    preg_match('/src="([^"]+)"/', $videoArray['video_url'], $match);
	    $url = $match[1];
	    $arr = explode("/",$url);
	    $url= $arr[count($arr)-1];
	    $videoArray['videourl'] ="https://www.youtube.com/embed/".$url."?autoplay=1";
	}
	$data =0;
	if($videoArray['video_type']=='youtubeurl'){
		$data =1;
	   	$videoArray['video_url'] = str_replace("\\","",$videoArray['video_url']);
	   	$arr = explode("=",$videoArray['video_url']);
	 	$url= $arr[count($arr)-1];
		$videoArray['videourl'] = "https://www.youtube.com/embed/".trim($url)."?autoplay=1";	
	}
	if($videoArray['video_type']=='video'){
		$videoArray['videourl'] = SITE_URL.'/dynamicAssets/videos/'.$videoArray['video_url'].'?autoplay=1';
	}
?>
<link rel="stylesheet" type="text/css" href="siteAssets/css/bootstrap.css"/>
<link rel="stylesheet" type="text/css" href="siteAssets/css/bootstrap-responsive.css" />
<link rel="stylesheet" type="text/css" href="siteAssets/css/styles.css"/>
<link rel="stylesheet" type="text/css" href="siteAssets/css/fonts.css"/>
<div class="ajax-text-and-image white-popup-block">

<style>
.font-bold{font-weight:bold !important;}
@media (min-width: 1024px){
html {font-size: 18px !important;}
}
</style>

    <div class="row-fluid">
        <div class="span12">
            <iframe width="100%" height="80%" src="<?php echo $videoArray['videourl'];?>" frameborder="0" class="popup_video" allowfullscreen></iframe>
    </div>
	</div>        
	<div style="clear:both; line-height: 0;"></div>
	
</div>
