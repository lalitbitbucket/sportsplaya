<?php
## Include Require Files
/***************************/
chdir("/var/www/html/redesign/");
$_SERVER['DOCUMENT_ROOT'] = dirname(__FILE__);

require_once '/var/www/html/redesign/config/settings.php';
require_once '/var/www/html/redesign/includes/phpmailer/class.phpmailer.php';
require_once '/var/www/html/redesign/model/email.php';
require_once '/var/www/html/redesign/model/country.php';
require_once '/var/www/html/redesign/model/jobs.php';
## Create Objects
/***************************/
$jobsObj = new Model_Jobs();
$emailObj  = new Model_Email();
$mail 	   = new PHPMailer(true);
$countryObj= new Model_Country();
## Function For Get Data
/***************************/
/* Get All Active user*/
$AllActiveUsers=$usersObj->getAllActiveUsers();
foreach($AllActiveUsers as $user){
	$matching_jobs =0;	

	$sportsId = $user['sportsID'];
	
	/* Get all jobs matching for users */
	$jobsArray = $jobsObj->getUsersExistsJobsBySportsId($sportsId);	
	if(count($jobsArray)>0){

			/* Email Template */
			$emailArray = $emailObj->getEmailById(30);
			$user_ID = $user['id'];
			echo '<br>';
			echo $to = $to = $user['email'];
			$toname = $user['fname'].' '.$user['lname'];		
			echo '<br>';
			
			$fromname = $emailArray['fName'];
			$from = $emailArray['fEmail'];
	    	
	    	/* Get Total Unread Message*/
	    	$totalMessages = $messageObj->checkMessageInboxCount($user_ID);
	    	
	    	$str ="";
	    	foreach ($jobsArray as $job_val) {

				/* Get user not apply this job */
				$jobsDetails = $jobsObj->getUsersExistsJobs($user['id'],$job_val['id']);
				
				/* if not apply */
				if(count($jobsDetails)==0){
					
					/* Get job Details */
					$jobData = $jobsObj->getJobsDetailsByjobId($job_val['id']);
					
					$t=0;
					
					foreach ($jobData as $jobDtl) {
						
						$jobs_url = trim(strtolower(str_replace(" ","_",$jobDtl['job_title'])));
						
						/* get Job logo */
						if($jobDtl['job_image']!=''){
							$image = SITE_URL.'/dynamicAssets/jobs/'.$jobDtl['job_image'];
						}
						else
						{
							$image = SITE_URL.'/dynamicAssets/jobs/club_logo_img.jpg';
						}

						$countryName = $countryObj->getCountryNameByCountryId($jobDtl['job_country']);
/***** start Template ********/
	
$str .='<tr><td align="center"><table width="86%" border="0" cellspacing="0" cellpadding="0"><tbody>
		<tr><td valign="top" width="80" style="padding-top:32px;width:80px;">';
$str .='<table width="80" border="0" cellspacing="0" cellpadding="0" style="width:80px;">
		<tbody><tr><td width="80" style="width:80px;">
		<a href="'.SITE_URL.'/jobsdetails/'.$jobs_url.'/'.base64_encode($jobDtl['id']).'" target="_blank" style="color:#009090;line-height:20px;text-decoration:none;">
		<img src="'.$image.'" alt="'.$jobDtl['job_title'].'" border="0" style="font-family:Arial,sans-serif;font-size:10px;color:#009090;width: 74%;">
		</a></td></tr></tbody></table>';
$str .='</td><td width="16" style="padding-top:28px;width:16px;">&nbsp;</td>
		<td valign="top" style="padding-top:28px;">';
$str .='<table border="0" cellspacing="0" cellpadding="0" style="min-width:178px;width:100%;">
		<tbody><tr><td style="color:#009090;line-height:24px;">
		<a href="'.SITE_URL.'/jobsdetails/'.$jobs_url.'/'.base64_encode($jobDtl['id']).'" target="_blank" style="font-family:Trebuchet MS,Arial,sans-serif;font-size:18px;color:#009090;line-height:24px;text-decoration:none;">'.$jobDtl['job_title'].'</a></td></tr>
		<tr><td style="color:#333333;line-height:20px;">
		<a style="font-family:Trebuchet MS,Arial,sans-serif;font-size:14px;color:#808080;line-height:20px;text-decoration:none;">'.$jobDtl['job_address'].' '.$countryName.'</a>
		</td></tr><tr>
		<tr><td style="color:#333333;line-height:20px;">
		<a style="font-family:Trebuchet MS,Arial,sans-serif;font-size:14px;color:#808080;line-height:20px;text-decoration:none;">AUD '.number_format($jobDtl['salary']).'+  '.$jobDtl['job_type'].'</a>
		</td></tr><tr>

		<tr><td>&nbsp;</td></tr>
		<tr><td style="color:#333333;line-height:20px;">
		<a style="font-family:Trebuchet MS,Arial,sans-serif;font-size:14px;color:#333333;line-height:20px;text-decoration:none;">'.substr($jobDtl['job_description'],0,70).'...</a>
		</td></tr><tr><td style="padding-top:8px;padding-bottom:8px;">
		</td></tr></tbody></table>';
$str	.='</td></tr></tbody></table></td></tr>
			<tr><td height="24" style="height:24px;border-bottom:1px solid #ebebeb;line-height:0px;font-size:0px;">&nbsp;</td></tr>';
/********/
$matching_jobs++;
		}
	
	

	}

}
/********/
if($matching_jobs >0){

$str .='<tr><td width="86%" style="padding-top:40px;max-width:516px;">
		<table width="100%" align="center" border="0" cellspacing="0" cellpadding="0">
			<tbody><tr><td width="50%" align="center" style="color:#333333;line-height:45px;">
		<a href="'.SITE_URL.'/'.$user['username'].'" target="_blank" style="text-decoration:none;font-family:Trebuchet MS,Arial,sans-serif;font-size:48px;color:#333333;">'.$matching_jobs.'</a></td>
		<td width="50%" align="center" style="color:#333333;line-height:45px;">
		<a href="'.SITE_URL.'/'.$user['username'].'" target="_blank" style="text-decoration:none;font-family:Trebuchet MS,Arial,sans-serif;font-size:48px;color:#333333;">'.$totalMessages.'</a></td></tr><tr>
		<td colspan="2" height="8" style="height:8px;line-height:0px;font-size:0px;">&nbsp;</td>
		</tr><tr><td align="center" valign="top" style="color:#009090;line-height:22px;">
		<a style="text-decoration:none;font-family:Trebuchet MS,Arial,sans-serif;font-size:18px;color:#009090;">matching <br>jobs</a></td>
		<td align="center" valign="top" style="color:#009090;line-height:22px;">
		<a style="text-decoration:none;font-family:Trebuchet MS,Arial,sans-serif;font-size:18px;color:#009090;">unread<br>messages</a></td></tr><tr>
		<td colspan="2" height="8" style="height:8px;line-height:0px;font-size:0px;">&nbsp;</td>
		</tr></tbody></table></td></tr>';

/************ End Template ******************/
				
		$subject = $emailArray['emailSub'];
		$subject = str_replace('[SITENAME]', SITENAME, $subject);
		$message = $emailArray['emailContent'];
		$message = str_replace('[SITEURL]', SITE_URL, $message);
		$message = str_replace('[SITENAME]', SITENAME, $message);
		$message = str_replace('[USERNAME]', $user['fname'].' '.$user['lname'], $message);
        $message = str_replace('[follow_list]', $str , $message);
        $message = str_replace('[userid]', $user_ID , $message);

        $template_msg	= str_replace('[MESSAGE]',$message , $message);
		$template_msg 	= str_replace('[LOGO]','<img src="'.SITE_URL.'/siteAssets/images/newimages/logo_blue/logo.png">', $template_msg);
		$template_msg 	= str_replace('[SITELINK]',SITE_URL , $template_msg);
		$template_msg 	= str_replace('[SUBJECT]',$subject , $template_msg);
		$template_msg	= str_replace('[SITEROOT]',SITE_URL , $template_msg);					
		$template_msg	= str_replace('[UNAME]',UNAME , $template_msg);
		$template_msg	= str_replace('[userid]',$user_ID , $template_msg);	
		$template_msg	= str_replace('[follow_list]',$str , $template_msg);
		//echo $template_msg;

		try
        {
		  	$mail->AddAddress("psrajender@gmail.com", $toname);
			$mail->SetFrom("admin@sportsplaya.com", $fromname);
			$mail->addBCC("psrajender@gmail.com","Rajendra Bhatia");
			$mail->addBCC("lalit.wiseit@gmail.com","Test");
			//$mail->addBCC("phillipb@oceaniainternet.com.au","Phillip Bell");
			$mail->Subject = $subject;
			echo $mail->Body = $template_msg; 
			  	echo "<br>";
				echo "<br>";
				echo "<br>";
				echo "<br>";
				echo "<br>";
				echo "<br>";
				$mail->Send();
		} 
		catch (phpmailerException $e)
	 	{
		    echo $_SESSION['msg']= $e->errorMessage(); //Pretty error messages from PHPMailer
		}  
		catch (Exception $e)
	 	{
		    echo $_SESSION['msg']= $e->getMessage(); //Boring error messages from anything else!
	 	}
	 }
	}
} /*End Foreach*/
?>