<?php
require_once 'config/settings.php';

// print_r($otheruserDetails);

// isLogin();

$sportsObj = new Model_Sports();
$videosObj = new Model_Videos();
$searchObj = new Model_Search();
$citiObj   = new Model_City();
$clubObj   = new Model_Club();

$limit = $_POST['offset'];

if(isset($_POST['offset'])){
    
    $search      =    $_POST['search'];    // Search SportsId    
    $sports_id      =    $_POST['sportsId'];    // Search SportsId
    $change_sport   =    $_POST['change_sport']; 
    $selectCountry     =   $_POST['selectCountry'];   // Search CityId
     
    $searchResultArray =  $searchObj->searchMoreData($search,$sports_id,$selectCountry,$change_sport,12,$limit);
    
    $change_sport_flag =0 ;
    
    $count_hidden = "";
    
    if($change_sport!=0)
    {   
        $countalbumArr =  $searchObj->searchMoreData($search,$sports_id,$selectCountry,'','','');
        $total_change_sport_flag  = count($countalbumArr);
$count_hidden = '<input type="hidden" name="hidden_total" id="hidden_total" value="'.$total_change_sport_flag.'">';
    }
    
    if($count_hidden!="")
    {
       echo $count_hidden;
    }

    /*$sports_images = $sportsObj->getAllActiveSports();
    $sports_image_arr =array();
    foreach($sports_images as $arr_row){
        $sports_image_arr[$arr_row['id']] =$arr_row['sportsTitle'];
    }*/

    $all_countries = $countryObj->getAllActiveCountry();
    $country_name_arr=array();

    foreach ($all_countries as $all_country) {
        $country_name_arr[$all_country['countryId']]=$all_country['countryName'];
    }
    
    foreach($searchResultArray as $sportsInfo) 
    {   
        $clubName = $clubObj->getClubNameByClubId($sportsInfo['sports_club']);
        $albumArray = $videosObj->getvideosByUserId($sportsInfo['id']);  /*Count User uploaded Videos*/
        $playaDtl = $videosObj->getPlayaOfDayByUserId($sportsInfo['id']);   /*Count User Playa Videos*/

        $total_playa = $playaDtl['total'];
        $total_videos = count($albumArray);

        $city_ID=$sportsInfo['city'];   /* Check cityID */
        if(trim($city_ID) > 0)
        {
            $city_Array= $citiObj->getCityDetailsByCityId($city_ID);  // Get City Name By CityId;
            $city_name = $city_Array['ctName'];
        }
        else{
            $city_name = 'N/A';   
        }
        
        $country_ID = $sportsInfo['country'];
        if($country_ID > 0)
        {
            $country_name = $country_name_arr[$country_ID];
        }
        else{
            $country_name = 'N/A';   
        }

        if($clubName==''){
            $clubName ='N/A';
        }

       /* $str = "";
        if(trim($sportsInfo['sportsID'])!="")
        {
            $sports_ids = explode(",",$sportsInfo['sportsID']);
            if(count($sports_ids)>0)
            {
                foreach($sports_ids as $sports_id)
                {
                    $str=$sports_image_arr[$sports_id];
                }  
            }
        }
        else{
            $str='N/A';
        }*/

        /******* Get Sports Name by SportsId *********/
    $sportsID = $sportsInfo['sportsID'];
    $sportnames = "";
    if(count($sportsID)>0)
    {
        $sp_id = explode(",", $sportsID);
        $sportsResult = $sportsObj->getSportsNameUsingSportsId($sp_id[0]);
        $sportnames = $sportsResult[0];
        //$sports = implode(",", $sportsResult);
    }
    if($sportnames =='')
    {
        $sportnames = 'N/A';
    }

    
        if ($sportsInfo['avatar'] == "")    /*Check IF Avatar*/
        {   
            if($sportsInfo['sex'] != "m")
            {
                $img_profile = SITE_URL.'/siteAssets/images/female_default_images1.png';
            }
            else
            {
                $img_profile = SITE_URL.'/siteAssets/images/male_default_images1.png';
            }
        }
        else
        {
            $img_profile =  SITE_URL.'/dynamicAssets/avatar/238x259/'.$sportsInfo['avatar'];
        }


        if($sportsInfo['sports_club']!=''){
            $sports_club = $sportsInfo['sports_club'];
        }
        else{
            $sports_club = 'N/A';   
        }
         if($sportsInfo['sports_position']!=''){
            $sports_position = $sportsInfo['sports_position'];
        }
        else{
            $sports_position = 'N/A';   
        }
// followers Data //
/*        echo $otheruserDetails['id'];*/
$followersUsers = $usersObj->getAllFollowingsByUserId($otheruserDetails['id'],'','','','','');
/*print_r($followersUsers);
*/
foreach ($followersUsers as $followersUser) 
{
    $followerID[]=$followersUser['followUID'];
}
// $followerID[] = $user_ID;
$myFollowId = $followerID;

$wisheduser = $usersObj->getAllWishListByUserId($otheruserDetails['id']);
foreach ($wisheduser as $wish) 
{   
    $wishUserId[] = $wish['user_wishId'];
}
$my_wishlist = $wishUserId;
    echo 
        '<div class="followersdiv-set col-md-6 m-l0" id="left_followersdiv_set">
            <div class="row border-bottom3-lgrey m-r9 p-tb27">
                <div class="col-md-3 p-lr0">
                    <a href="'.SITE_URL.'/'.$sportsInfo['username'].'">
                        <img id="files" title="'.$sportsInfo['fname'].''.$sportsInfo['lname'].'" alt="'.$sportsInfo['fname'].''.$sportsInfo['lname'].'" src="'.$img_profile.'" class="playa_search_img">
                    </a>';

            if($otheruserDetails['userType'] == 5){
                if(in_array($sportsInfo['id'],$my_wishlist)){
                echo '<input type="button" title="Remove From Wishlist" value="Remove Wishlist" id="remove_wishlist_'.$sportsInfo['id'].'" name="remove_wishlist_'.$sportsInfo['id'].'" class="btn p-tb9 font-uc" style="width: 120px;" onclick="return removeFromWishlist('.$otheruserDetails['id'].','.$sportsInfo['id'].')">
                    <input type="button" title="Add To Wishlist" value="Add WISHLIST" id="add_wishlist_'.$sportsInfo['id'].'" name="add_wishlist" class="btn p-tb9 font-uc" onclick="return addWishlist('.$otheruserDetails['id'].','.$sportsInfo['id'].')" style="width: 120px;display:none;">';

                }
                else{
                echo '<input type="button" title="Add To Wishlist" value="Add WISHLIST" id="add_wishlist_'.$sportsInfo['id'].'" name="add_wishlist" class="btn p-tb9 font-uc" onclick="return addWishlist('.$otheruserDetails['id'].','.$sportsInfo['id'].')" style="width: 120px;">

                    <input type="button" title="Remove From Wishlist" value="Remove Wishlist" 
                    id="remove_wishlist_'.$sportsInfo['id'].'" name="remove_wishlist_'.$sportsInfo['id'].'" class="btn p-tb9 font-uc" style="display: none;" onclick="return removeFromWishlist('.$otheruserDetails['id'].','.$sportsInfo['id'].')">';
                }
            }


            echo '</div>
                
                <div class="col-md-9 p-lr18" style="min-height: 14rem !important;">
                <a href="'.SITE_URL.'/'.$sportsInfo['username'].'">
                    <h4 class="p-28 text-dgrey m-t0">'.$sportsInfo['fname'].' '.$sportsInfo['lname'].'';
                    if($sportsInfo['age'] > 0){
                        echo '('.$sportsInfo['age'].')';
                    }
                echo'
                    </h4>
                    </a>
                        <ul class="play_details">
                            <li class="col-md-6 m-l0 text-lgrey">
							<span class="icon sport"></span>'.$sportnames.'</li>
                            <li class="col-md-6 m-l0">
							<span class="icon playofDay"></span>'.$total_playa.'x Play of the Day</li>
                            <li class="col-md-6 m-l0">
							<span class="icon club"></span>'.$clubName.'</li>
                            <li class="col-md-6 m-l0">
							<span class="icon video"></span>'.$total_videos. ' Videos</li>
                            <li class="col-md-6 m-l0">
							<span class="icon position"></span>'.$sports_position.'</li>
                            <li class="col-md-6 m-l0">
							<span class="icon location"></span>'.$country_name.'</li>
                        </ul>';

            echo '<div class="col-xs-12 folowed-block"><div class="col-xs-6">';
            if($_SESSION['stagUserId']!='')
            {    
                if(in_array($sportsInfo['userId'],$myFollowId))
                {
                   echo '<input type="button" value="Followed" id="add_followers_'.$sportsInfo['userId'].'" name="add_followers" class="btn p-tb9 font-uc" style="height:2rem;"  disabled>';
                }
                else
                {
                    echo '<input type="button" value="Follow" id="add_followers_'.$sportsInfo['userId'].'" name="add_followers" class="btn p-tb9 font-segoe bg-grey text-white font-uc"  onclick="return searchfollowhim('.$otheruserDetails['id'].','.$sportsInfo['userId'].',1)" style="height:2rem;">';
                    
                    echo '<input type="button"  value="Followed" id="added_followers_'.$sportsInfo['userId'].'" name="add_followers" class="btn p-tb9 font-uc"  style="display:none; height:2rem;" disabled/>';
                }
            }    
            else{
                    echo '<a href="'.SITE_URL.'/login_popup.php" class="ajax-popup-link btn p-tb9 font-segoe bg-grey text-white font-uc" style="height:2rem;">Follow</a>';
            }            
            echo '</div>';     
            echo '<div class="col-xs-6"><input type="button"  value="Visit Profile" class="btn p-tb9 font-segoe bg-grey text-white font-uc" style="height:2rem;"';?>onclick="window.location.href='<?php echo SITE_URL.'/'.$sportsInfo['username'];?>'">
            <?php echo '</div></div>
            </div>
            </div>
            </div>';
    }
}
?>
