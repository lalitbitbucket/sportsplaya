<?php
## Include Require Files
/***************************/
chdir("/var/www/html/redesign/");
$_SERVER['DOCUMENT_ROOT'] = dirname(__FILE__);

require_once '/var/www/html/redesign/config/settings.php';
require_once '/var/www/html/redesign/includes/phpmailer/class.phpmailer.php';
require_once '/var/www/html/redesign/model/email.php';
require_once '/var/www/html/redesign/model/message.php';

## Create Objects
/***************************/
$emailObj  = new Model_Email();
$mail 	   = new PHPMailer(true);
$messageObj = new Model_Messages();

## Function For Get Data
/***************************/

/* Get All Active user*/
$AllActiveUsers=$usersObj->getAllActiveUsers();

foreach($AllActiveUsers as $user){

	/* Check the unread messages */
	$totalMessages = $messageObj->checkMessageInboxCount($user['id']);
	if($totalMessages>0){
		
		/* Get Email Template */
		$emailArray = $emailObj->getEmailById(31);
		$user_ID = $user['id'];
		echo '<br>';
		echo $to = $user['email'];
		echo '<br>';
		echo $toname = $user['fname'].' '.$user['lname'];		
		echo '<br>';
		echo '<br>';
		
		$fromname = $emailArray['fName'];
		$from = $emailArray['fEmail'];

		/* get Messages Details */
		$messageArr = $messageObj->GetUnreadMessageInboxByuserId($user_ID);
		
		$str = "";
		foreach ($messageArr as $messagevalue) {
			
			/* Get Sender User Information */
			$senderUserProfile = $usersObj->getProfileDetailsByUserId($messagevalue['senderId']);
			
			if ($senderUserProfile['avatar'] == "") {
				if($senderUserProfile['sex'] != "m")
   				{
   					$senderImage = SITE_URL.'/siteAssets/images/female_default_images1.png';
   				}
   				else
   				{
   					$senderImage = SITE_URL.'/siteAssets/images/male_default_images1.png';
   				}
			}
			else
			{
				$senderImage =  SITE_URL.'/dynamicAssets/avatar/238x259/'.$senderUserProfile['avatar'];
			}
			
			$senderName = $senderUserProfile['fname'].' '.$senderUserProfile['lname'];
			$unreadMessage = $messagevalue['message'];
			$MessageDateTime = $messagevalue['Date_Formatted'];

			$str  .= '<style>@media only screen and (max-device-width: 480px){ tr td img {max-width: 100px !important;height: auto !important;} table {width:90% !important;border:1px #555555 !important;} h1,h3{ text-indent: 0px !important;} b{font-size:16px !important;} td a img{width:100% !important;} p{padding-left:2px !important; width:90% !important;font-size:12px !important;} #follow_mob{font-size:12px !important;padding:5px 10px !important; max-width:60px !important;margin-top: 5px !important;} tr td a img{max-width:80% !important;} #sports_img{width: 15px !important;margin-right: 2px !important;} #follow_mob{display:block !important;} #follow_all{display:none !important;}}'; 

        	$str  .= '<style>@media only screen and (min-device-width: 480px) and (max-device-width: 600px) {table {width:90% !important;border:1px #555555 !important;} h1,h3{ text-indent: 0px !important;} b{font-size:16px !important;} td a img{width:50% !important;} p{padding-left:2px !important; width:90% !important;}}</style>';

			$str  .= '<tr width="100%">';
        	$str  .= '<td><img src="'.$senderImage.'" width="100px" style="margin-bottom: 23%;"></td>';
        	$str  .= '<td style="width:100%"><span style="font-family:SegoeUI,Arial;font-size: 18px;color: #313D50;line-height: 24px;">'.$senderName.'</span><br>
        			<p style="margin-top:2px;font-family: Trebuchet MS,Arial,sans-serif;font-size: 14px;color: #808080;
    				line-height: 20px;">'.substr($unreadMessage, 0,12).'....'.'</p>';
        	$str  .= '<p style="margin-top:8px;font-family: Trebuchet MS,Arial,sans-serif;font-size: 14px;color: #808080;line-height: 20px;">'.$MessageDateTime.'</p>
        	<a href="'.SITE_URL.'/'.$user['username'].'/messages" target="_blank" style="display: inline-block;
    margin-bottom: 0;line-height: 1rem;color: #fff;text-align: center;vertical-align: middle !important;cursor: pointer;
    background: #313D50;border:0px;height: 1rem;font-size: 11px;    text-decoration: none;    padding: 2%;" title="Read More Unread Messages">Read More..</a>
        	<td>';
        	$str  .='</td></tr>';
        }
        	/* Setup the Email Templates */
    		$subject = $emailArray['emailSub'];
			$subject = str_replace('[SITENAME]', SITENAME, $subject);
			$message = $emailArray['emailContent'];
			$message = str_replace('[SITEURL]', SITE_URL, $message);
			$message = str_replace('[SITENAME]', SITENAME, $message);
            $message = str_replace('[UNAME]', $user['fname'].' '.$user['lname'], $message);
            $message = str_replace('[userid]', $user_ID , $message);

            $template_msg	= str_replace('[MESSAGE]',$message , $message);
			$template_msg 	= str_replace('[LOGO]','<img src="'.SITE_URL.'/siteAssets/images/newimages/logo.png">', $template_msg);
			$template_msg 	= str_replace('[SITELINK]',SITE_URL , $template_msg);
			$template_msg 	= str_replace('[SUBJECT]',$subject , $template_msg);
			$template_msg	= str_replace('[SITEROOT]',SITE_URL , $template_msg);					
			$template_msg	= str_replace('[UNAME]',UNAME , $template_msg);	
			$template_msg = str_replace('[follow_list]', $str , $template_msg);
			$template_msg	= str_replace('[userid]',$user_ID , $template_msg);	
			try
        	{	
			  	$mail->AddAddress('psrajender@gmail.com', $toname);
				$mail->SetFrom("admin@sportsplaya.com", $fromname);
				$mail->addBCC("psrajender@gmail.com","Rajendra Bhatia");
				$mail->addBCC("lalit.wiseit@gmail.com","Test");
				//$mail->addBCC("phillipb@oceaniainternet.com.au","Phillip Bell");
				$mail->Subject = $subject;
				echo $mail->Body = $template_msg; 
				  	echo "<br>";
					echo "<br>";
					echo "<br>";
					echo "<br>";
					echo "<br>";
					echo "<br>";
					$mail->Send();
			} 
			catch (phpmailerException $e)
	 		{
		    	echo $_SESSION['msg']= $e->errorMessage(); //Pretty error messages from PHPMailer
			}  
			catch (Exception $e)
	 		{
		    	echo $_SESSION['msg']= $e->getMessage(); //Boring error messages from anything else!
	 		}
		}

	}
?>