<?php
require_once 'config/settings.php';
require_once 'model/country.php';
require_once 'model/email.php';
require_once 'includes/phpmailer/class.phpmailer.php';

$siteurl = SITE_URL.'/';

## Create Objects
/*******************************/
$emailObj = new Model_Email();
$mail     = new PHPMailer(true);
/*******************************/

if ($_SESSION['stagUserId'] != '') {
    echo '<script>top.window.location.href="'.SITE_URL.'/"</script>';
    exit;
}


$uId = $_REQUEST['uId'];
$countryObj = new Model_Country();
$countryArray = $countryObj->getAllActiveCountry();

$page_body_class = 'home_page';
$smarty->assign ('page_body_class', $page_body_class);

$msg ='';
$flag=0;

if($_POST !='' && $_POST['register']=='SIGN UP') 
{   
    $fname = mysql_escape_string(preg_replace('/[^A-Za-z0-9\-]/', '',$_POST['fname']));
    $lname = mysql_escape_string(preg_replace('/[^A-Za-z0-9\-]/', '',$_POST['lname']));
    $email = mysql_escape_string($_POST['email']);
    $city = mysql_escape_string($_POST['city']);
    
    $password = trim($_POST['pass']);
    $dob = trim($_POST['dob']);
    $gender = $_POST['gender'];
    $dateOfBirth = date("Y-m-d", strtotime($_POST['dob']));
    $country = return_post_value($_POST['country']);
    $usertype = trim($_POST['usertype']);
    
    //$_SESSION['stagUserEmail'] = $email;
        
    if ($fname != '' && $lname != '' && $email != '' && $gender != '' && $dob != '' && $password != '') 
    {
        if (filter_var ($email, FILTER_VALIDATE_EMAIL)) 
        {
            $result = $usersObj->chkUserEmailExist(trim ($email),'');
            if (count ($result) < 1) 
            {
                $salt               = genenrate_salt();
                $userArray          = array ();
                $newpassword            = genenrate_password($salt,$password);
                $_SESSION['mail_psswd']=$password;
                $userArray['password']      = $newpassword; 
                $userArray['email']       = $email;
                $userArray['salt']          = $salt;
                $userArray['userType']      = $usertype;
                $userArray['regDate']       = date ('Y-m-d');
                $userArray['status']        = '2';
                $newuserId = $usersObj->addUserByValue($userArray);

                /************ Add details in user profile *********/
                $userprofileArray = array ();
                $userprofileArray['userId']     = $newuserId;
                $userprofileArray['userType']   = $usertype;
                $userprofileArray['email']  = $email;
                $userprofileArray['fname']  = $fname;
                $userprofileArray['lname']  = $lname;
                $userprofileArray['birthdate']  = $dateOfBirth;
                $userprofileArray['sex']    = $gender;
                $userprofileArray['regDate']    = date('Y-m-d');
                $userprofileArray['status']     ='2';
                $userprofileArray['country']     = $country;
                if($city!=''){
                    $userprofileArray['city']     = $city;    
                }
                else{
                    $userprofileArray['city']     = 0;   
                }
                $userprofileId = $usersObj->addUserProfileByValue($userprofileArray);
                    
                /************ Add User Details In Account Settings & Set Value Table Start *************/
                $settingId = '1';
                $stuffArray = array();
                $stuffArray['userId']    = $userprofileId;
                $stuffArray['settingId'] = $settingId;
                $usersObj->addAccountSetValues($stuffArray);

                ## Who Can Contact Me
                $settingId = '2';
                $friendArray = array();
                $friendArray['userId']   = $userprofileId;
                $stuffArray['settingId'] = $settingId;
                $usersObj->addAccountSetValues($friendArray);

                $userNameDetail = array ();

                $username  = trim(strtolower(str_replace(" ","",$fname))) . '_' . trim(strtolower(str_replace(" ","",$lname)));
                $username1 =  trim(strtolower(str_replace(" ","",$fname))) . '' . trim(strtolower(str_replace(" ","",$lname)));
                $username2 =  trim(strtolower(str_replace(" ","",$fname))) . '-' . trim(strtolower(str_replace(" ","",$lname)));
                
                $usernameexist = $usersObj->checkUserNameExists($username);
                $usernameexist1 = $usersObj->checkUserNameExists($username1);
                $usernameexist2 = $usersObj->checkUserNameExists($username2);

                if (count ($usernameexist) < 1) { 
                    $userNameDetail['username'] = $username; 
                }   
                elseif (count ($usernameexist1) < 1) { 
                    $userNameDetail['username'] = $username1; 
                }
                elseif (count ($usernameexist2) < 1) { 
                    $userNameDetail['username'] = $username2; 
                }
                else {
                    $username = trim(strtolower(str_replace(" ","",$fname))) . '' . trim(strtolower(str_replace(" ","",$lname))) . '' . $userprofileId;
                    $userNameDetail['username'] = $username;
                }
                
                $usersObj->editUserProfileValueById ($userNameDetail,$userprofileId);
                
                $usersObj->editUserNameByUserId($userNameDetail,$newuserId);

                /*********************************************************************************/
                $fetchdob = $usersObj->getUserdob($userprofileId);
                $calAge = CalculateAge($fetchdob);
                
                $dobArray = array();
                $dobArray['age']     = $calAge;
                $usersObj->editUserProfileValueById ($dobArray,$userprofileId);

                /*********************************************************************************/
                $userArray = $usersObj->getUserProfileDetailByUserID($newuserId, $usertype);
                $_SESSION['stagUserId']        = $newuserId;
                $_SESSION['stagUserName']      = $userArray['username'];
                $_SESSION['userType']          = $usertype;
                $_SESSION['stagUserFullName']  = ucfirst($userArray['fname']) . '' . ($userArray['lname']);
                $_SESSION['stagUserProfileId'] = $userArray['id'];
                $_SESSION['tmpUserId'] = $newuserId;
                $_SESSION['tmpUserProfileId'] = $userprofileId;
                $_SESSION['tmpUserFName'] = $fname;
                $_SESSION['stagUserEmail'] = $email;

                setcookie('userid',$userArray['id'],time()+3600,'/');

                /* Send Email */
                /*******************/
                $emailArray = $emailObj->getEmailById(4);
                $to = $email;
                $toname = $fname.' '.$lname;        
                $fromname = $emailArray['fName'];
                $from = $emailArray['fEmail'];
                
                $subject = $emailArray['emailSub'];
                $subject = str_replace('[SITENAME]', SITENAME, $subject);
                $message = $emailArray['emailContent'];
                        
                $message = str_replace('[NAME]', ucfirst(return_post_value(trim($fname))).' '.ucfirst(return_post_value(trim($lname))), $message);
                $message = str_replace('[EMAIL]',  $email, $message);
                $message = str_replace('[USERNAME]', ucfirst(return_post_value(trim($fname))).' '.ucfirst(return_post_value(trim($lname))), $message);
                $message = str_replace('[SITEURL]', SITE_URL, $message);
                $message = str_replace('[SITENAME]', SITENAME, $message);
                        
                $template_msg   = str_replace('[MESSAGE]',$message , $message);
                $template_msg   = str_replace('[LOGO]','<img src="'.SITE_URL.'/siteAssets/images/newimages/logo.png">', $template_msg);
                $template_msg   = str_replace('[PASSWORD]', $_SESSION['mail_psswd'], $template_msg);
                $template_msg   = str_replace('[SITELINK]',SITE_URL , $template_msg);
                $template_msg   = str_replace('[SUBJECT]',$subject , $template_msg);
                $template_msg   = str_replace('[SITEROOT]',SITE_URL , $template_msg);                   
                
                /*echo $subject."<br/>"; 
                echo $from."<br/>"; 
                echo $fromname."<br/>"; 
                echo $to."<br/>"; 
                echo $toname."<br/>";
                echo "<pre>"; print_r($template_msg); exit();*/
                        
                try {
                $mail->AddAddress($to, $toname);
                $mail->SetFrom($from, $fromname);
                $mail->addBCC("psrajender@gmail.com","Test");
                $mail->Subject = $subject;
                $mail->Body = $template_msg; 
                $mail->Send();
                } 
                catch (phpmailerException $e) {
                    $_SESSION['msg']= $e->errorMessage(); //Pretty error messages from PHPMailer
                } 
                catch (Exception $e) {
                    $_SESSION['msg']= $e->getMessage(); //Boring error messages from anything else!
                }
                
                /*******************/
                if($usertype=='5'){
                    echo'<script>top.window.location.href="'.SITE_URL.'/register/membership"</script>';
                    exit;   
                }
                $flag=1;

            }
            else
            {
                $msg = "Email already register please go to sign in page.";
            }
        } 
        else
        {
                $msg = "Please Enter a Valid E-mail Id.";
        }
    }
}
?>
<link rel="stylesheet" type="text/css" href="/siteAssets/responsive/css/bootstrap.css" />
<link rel="stylesheet" type="text/css" href="/siteAssets/css/bootstrap-responsive.css" />
<link rel="stylesheet" type="text/css" href="/siteAssets/css/styles.css" />
<link rel="stylesheet" type="text/css" href="/siteAssets/css/fonts.css" />
<link rel="stylesheet" type="text/css" href="/siteAssets/responsive/css/mobile.css" />
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<style>
@media (min-width: 1024px){
html {font-size: 18px !important;}
}
@media (max-width: 767px) {
body {padding-right: 0 !important;padding-left: 0 !important;}
.mfp-iframe-holder{padding-top:0 !important;padding-bottom:0 !important;}
.mfp-container{padding:0 !important;}
.login_popup h1{margin-top:0;}
}
.white-popup-block{background:#fff url(/siteAssets/images/newimages/bg_register.png) center top no-repeat;  
background-size:100%;}
/*#custom-content img {max-width: 100%;margin-bottom: 10px;}*/
@media only screen and (max-width: 472px)
{
.googlebtn
{max-width:100%;height:auto;/*width: 11rem !important;
margin-left: 15% !important;
height: 48px !important;
margin-top: 0rem !important;
margin-right: 15%;}
*/}
.white-popup-block{background:#fff;background-size:100%;}
}
/*.googlebtn {width: 13rem;margin-top: -2rem;height: 48px;}*/
</style>


<div class="white-popup-block">
    <div id="custom-content" class="p-t27" style="max-width:100%; padding:3rem 3rem 2rem;">
        
        <div>
            <?php if($msg!=''){echo '<p class="alert bg-transparent p-14 text-brickRed">'.$msg.'</p>';}?>
            <?php if($flag==0){echo '<p class="text-tan p-26 m-b0">You dont have to play the game to</p><h1     class="text-dgrey font-segoe p-74 font-uc m-tb0 m-b40">Become a <span id="sp">';
            if($_REQUEST['usertype'] == '5'){echo 'SCOUT';}
            else if($_REQUEST['usertype'] == '6'){echo 'CLUB';}
            else{echo 'PLAYA';}
            echo '</span></h1>';}else{echo '<h2>Thanks For Register,</br>SportsPlaya !</h2>';}?>
        </div>

        
        <?php if($flag==1) { ?>
            <div class="" style="padding-bottom: 10%;padding-top: 12%;">
                <?php if(isset($_REQUEST['uId']) && !empty($_REQUEST['uId'])){?>

                    <button class="btn bg-grey p-lr9 font-uc p-18 text-white" onclick="window.location.href='<?php echo SITE_URL.'/nominee_popuppage.php?uId='.$_REQUEST['uId'];?>'">CLICK FOR VOTING</button>&nbsp;&nbsp;
                    <button class="btn bg-grey p-lr9 font-uc p-18 text-white" onclick="top.window.location.href='<?php echo SITE_URL;?>/users/viewprofile'">GO YOUR PROFILE</button>
                <?php }else{?>
                    <button class="btn bg-grey p-lr9 font-uc p-18 text-white" onclick="top.window.location.href='<?php echo SITE_URL;?>/users/viewprofile'">GO YOUR PROFILE</button>
                <?php }?>
            </div>
        
        <?php } else { ?>

       <form method="post" name="frmRegister">
       <div class="row">
        <div class="col-xs-12 col-md-6">
        <div class="row">
            <div class="col-xs-12 col-md-12"><a href="#" class="text-dgrey">Create a FREE new profile</a></div>
            <div class="col-xs-12 col-md-5 m-l0 p-b9">
                <input type="text" name="fname" id="fname" class="col-xs-12 col-md-12 p-18 p-tb18 text-dgrey" 
                placeholder="First Name"
                value="<?php if(isset($_POST['fname'])){echo $_POST['fname'];}?>" required="required">
            </div>
            <div class="col-xs-12 col-md-5 p-b9">
                <input type="text" name="lname" id="lname" class="col-xs-12 col-md-12 p-18 p-tb18 text-dgrey" 
                placeholder="Last Name" value="<?php if(isset($_POST['lname'])){echo $_POST['lname'];}?>" 
                 required="required">
            </div>
            <div class="col-xs-12 col-md-5 m-l0 p-b9">
                <input type="text" name="email" id="email" class="col-xs-12 col-md-12 p-18 p-tb18 text-dgrey" autocomplete="off" 
                value="<?php if(isset($_POST['email'])){echo $_POST['email'];}?>" placeholder="Email" required="required">
            </div>
            <div class="col-xs-12 col-md-5 p-b9">
                <input type="password" placeholder="Password" name="pass" id="pass" class="col-xs-12 col-md-12 p-18 p-tb18 text-dgrey" 
                required="required">
            </div>
            <div class="col-xs-12 col-md-5 m-l0 m-b18 p-b9">
                <input type="text" name="dob" id="dob" class="col-xs-12 col-md-12 p-18 p-tb18 text-dgrey" autocomplete="off" 
                value="<?php if(isset($_POST['dob'])){echo $_POST['dob'];}?>" placeholder="Date Of Birth" required="required">
            </div>
            <div class="col-xs-12 col-md-12 m-l0 m-b9 p-b9">
                <input type="radio" name="gender" id="gender" <?php if(isset($_POST['gender']) && $_POST['gender'] == 'm') { echo'checked="checked"';} ?> 
                checked="" value='m' class="pull-left m-r9 m-t0" style="height:1rem"><span class=" m-r18 pull-left">Male</span>
                <input type="radio" name="gender" id="gender" <?php if(isset($_POST['gender']) && $_POST['gender'] =='f'){'checked="checked"';}?> 
                value="f" class="pull-left m-r9 m-t0" style="height:1rem"><span class="m-r18 pull-left">Female</span>
            </div>
            <div class="col-xs-12 col-md-12 m-b9 p-b9">
                <select name="country" id="country" required="required" class="col-xs-12 col-md-12 selectpicker p-tb9 m-b9"  style="border: 1px solid #767B87;min-height: 3rem;box-shadow: 0 1px 0 #767B87;background-position:97% 50%;">
                    <option value="">Select Country</option>
                    <?php foreach ($countryArray as $country) {?>
                    <option value="<?php echo $country['countryId'];?>"><?php echo $country['countryName']; ?></option>
                    <?php }?>
                </select>
            </div>

            <div class="col-xs-12 col-md-5 m-b18 p-b9">
                 <select name="city" id="city" class="col-xs-12 col-md-12 selectpicker p-tb9 m-b9"  style="border: 1px solid #767B87;min-height: 3rem;box-shadow: 0 1px 0 #767B87;background-position:97% 50%;">
                    <option value="">Select City</option>
                </select>
            </div>

            <div class="col-xs-12 col-md-12 m-b18 p-b9">
                <input type="radio" name="usertype" id="usertype"  value='3' 
                        <?php if(isset($_POST['usertype']) && $_POST['usertype'] == '3' 
                        || isset($_REQUEST['usertype']) && $_REQUEST['usertype'] == '3' ) { echo'checked="checked"';} ?>
                    checked="" class="pull-left m-r9 m-t0" style="height:1rem;"><span class="pull-left m-r18">PLAYA</span>

                <input type="radio" name="usertype" id="usertype" value='6'  
                        <?php if(isset($_POST['usertype']) && $_POST['usertype'] =='6' 
                        || isset($_REQUEST['usertype']) && $_REQUEST['usertype'] == '6'){echo'checked="checked"';}?>  
                    class="pull-left m-r9 m-t0" style="height:1rem;"><span class="pull-left m-r18">CLUB</span>
                
                <input type="radio" name="usertype" id="usertype"  value='5' 
                        <?php if(isset($_POST['usertype']) && $_POST['usertype'] == '5' 
                        || isset($_REQUEST['usertype']) && $_REQUEST['usertype'] == '5') { echo'checked="checked"';} ?>
                    class="pull-left m-r9 m-t0" style="height:1rem;"><span class="pull-left m-r18">SCOUT</span>
            </div>
            
			<div class="col-xs-12 col-md-2 text-center p-b9">
                <input type="submit" name="register" id="register" class="col-xs-12 col-md-12 btn bg-grey p-lr9 font-uc p-18 text-white" value="SIGN UP">
            </div>
            <div class="col-xs-12 col-md-3 text-center p-b9">
                <p class="col-xs-12 text-center p-tb18 m-b0">
                    Or sign in with :
                </p>
            </div>
			<div class="col-xs-12 col-md-7 text-center p-b9">
                    <fb:login-button scope="public_profile,email" onlogin="checkLoginState();" id="TEST" 
                        size="xlarge">
                    </fb:login-button>
                    <a href="javascript:void(0);" onclick="window.parent.location = '<?php echo SITE_URL;?>/google/login'">
                        <img src="<?php echo SITE_URL;?>/siteAssets/images/google_button.png" alt="SportsPlaya Google Login" title="Google Login" class="googlebtn" style="max-width: 11rem;float: right;">
                    </a>
            </div>
            </div>
        </div>
       </div>
        </form>
       <?php }?>
    </div>
    </div>

</div>
<script src="/siteAssets/js/jquery.js"></script>
  <script src="/siteAssets/js/jquery-ui.js"></script>
<script type="text/javascript">

    $('#country').change(function(){
            var cntyId = $('#country').val();
            // alert(cntyId);
            $.ajax({
                url:"/getCityByCntryId_ajax.php",
                type:'POST',
                data:'countryId='+cntyId,
                success: function(response){
                    // alert(response);
                    if(response!=0){
                        $("#city").html('');
                        $("#city").append(response);
                    }
                    else{
                        $("#city").html('<option value="0">None</option>');   
                    }
                }
            });
        });


$(document).ready(function() {
$('input[type=radio][name=usertype]').change(function() {
        if (this.value==3) {
            $('#sp').html('PLAYA');
        }
        else if (this.value ==6) {
            $('#sp').html('CLUB');
        }
        else if(this.value ==5){
            $('#sp').html('SCOUT');
        }
    });
});

document.getElementById('country').value='<?php echo $_POST['country']; ?>';
$( function() {
$( "#dob" ).datepicker({
    changeMonth: true,
    changeYear: true,
    yearRange: "-100:+0",
    dateFormat: 'yy-mm-dd'
});
});

function checkLoginState() {
    FB.getLoginStatus(function(response) {
      statusChangeCallback(response);
    });
}

window.fbAsyncInit = function() {
    FB.init({
        appId      : '1538410359534398',
        cookie     : true,  // enable cookies to allow the server to access 
                            // the session
        xfbml      : true,  // parse social plugins on this page
        version    : 'v2.8' // use graph api version 2.8
    });
};

// Load the SDK asynchronously
(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

function statusChangeCallback(response) {
   console.log('statusChangeCallback');
   console.log(response);
   // The response object is returned with a status field that lets the
   // app know the current login status of the person.
   // Full docs on the response object can be found in the documentation
   // for FB.getLoginStatus().
   if (response.status === 'connected') {
     // Logged into your app and Facebook.
     testAPI();
   } else {
     // The person is not logged into your app or we are unable to tell.
     document.getElementById('status').innerHTML = 'Please log ' +
       'into this app.';
   }
 }
  
function testAPI() {
    console.log('Welcome!  Fetching your information.... ');
    FB.api('/me?fields=id,first_name,last_name,email,gender,locale,picture', function(response) {
      console.log('Successful login for: ' + response.email);
    
    window.parent.location = '/facebook/login?fb_id='+response.id+'&email='+response.email+'&first_name='+response.first_name+'&last_name='+response.last_name+'&gender='+response.gender;
});

}
</script>
