<?php
## include required files
/*******************************/ 
require_once 'config/settings.php';
require_once '../model/users.php';
require_once '../model/common/functions.php';
/*******************************/

## Create Objects
/*******************************/
$adminObj= new Model_Users();
/*******************************/

if($_GET['id']!='') {
  $admin_res=$adminObj->getUserValueByMdUserId($_GET['id']);
  $userID = $admin_res['userId'];
  $urlDetails = $adminObj->getUserURLDetailByUserID($userID);
  if($urlDetails['status']=='0'){
	$smarty->assign('admin_res', $admin_res);
  } else {
	$_SESSION['msg'] = '<div class="error_msg"><span>Your link is broken or expired.</span></div>';
	header('location:'.SITE_URL.'/admin');
	exit();
  } 
} else {
  $_SESSION['msg'] = '<div class="error_msg"><span>Your link is broken or expired.</span></div>';
  header('location:'.SITE_URL.'/admin');
  exit();
}
if(isset($_POST['forgot_btn'])){	
	## apply PHP validation for required fields whenever you submit the form in your project
	if(trim($_POST['pass']) != '') {
	
		//echo"<pre/>"; print_r($_POST); exit;
		
	       $newPass = return_fetched_value($_POST['pass']);
			$cpArray = array();
			$salt = genenrate_salt();
			$newpassword = genenrate_password($salt ,$newPass);	
			$cpArray['password'] 		= $newpassword;				
			$cpArray['salt']     		= $salt;
		$adminObj->editUserValueById($cpArray,$userID);
		$adminObj->deleteURLDetailsByValue($userID);
		$_SESSION['msg'] = '<div class="success_msg"><span>Your password reset successfully!</span></div>';	
		header('location:'.SITE_URL.'/admin');
		exit();

	} else { // password else
		$_SESSION['msg'] = '<div class="error_msg"><span>Please enter your password</span></div>';
	}
// main if closed	
}

## Assign success or error msg to smarty variable and unset session variable
if(trim($_SESSION['msg']) != '') {
	$smarty->assign('msg', $_SESSION['msg']);
	unset($_SESSION['msg']);
}
## Unset all the objects created which are on this page
unset($adminObj);
unset($configObj);

$smarty->display(TEMPLATEDIR_ADMIN. 'resetpassword.tpl');
unset($smarty);
?>