<?php 
## include required files
/*******************************/
require_once 'config/settings.php';
require_once '../model/users.php';

/*******************************/
//error_reporting(E_ALL);
## Create Objects
/*******************************/
$userObj = new Model_Users();
/***********************/

## Check if is admin already logged in
/*******************************/
if(isset($_SESSION["adminSporttagId"]) != '' && trim($_SESSION["adminSporttagId"]) != '')
{
	header("location:".SITE_URL."/admin/home.php");	
	exit;
}
/*******************************/
//echo"<pre/>"; print_r($_POST);exit;

if(isset($_POST['username'])) {
	## apply PHP validation for required fields whenever you submit the form in your project
	if(trim($_POST['username']) != '' && trim($_POST['password']) != '') {
		if(trim($_POST['username']) != '' && trim($_POST['username']) != 'Username') {
			if(trim($_POST['password']) != '' && trim($_POST['password']) != 'Password') {
			
			$username = return_post_value($_POST['username']);
			$password = return_post_value($_POST['password']);
			   $userDtl = $userObj->getUserValueByDetailsUsername($username);
				$getpassword = genenrate_password($userDtl['salt'], $_POST['password']);
 
				//print_r($userDtl);
				//exit;
			
				//echo"<pre/>"; print_r($userDtl); exit;
				
				if ($userDtl['password'] == $getpassword) {					
						if($userDtl['userType'] != '3' and $userDtl['userType'] != '4'){
							if($userDtl['status'] == '2') {
								$_SESSION["adminSporttagId"] = $userDtl['userId'];
								$_SESSION["admin_username"] = $userDtl['username'];
								$_SESSION["admin_email"] = $userDtl['email'];
								$_SESSION["admin_firstname"] = $userDtl['fName'];
								$_SESSION["admin_lastname"] = $userDtl['lName'];
								$_SESSION["admin_type"] = $userDtl['userType'];
								
								$loginArray =array();
								$loginArray['user_id']  = $userDtl['userId'];
								$loginArray['loginTime']  = date("Y-m-d H:i:s");
								$loginArray['loginIP']= $_SERVER['REMOTE_ADDR'];
								$loginId= $userObj->addLoginDetailsByValue($loginArray);
								$_SESSION["loginId"] = $loginId;								
								header("location:".SITE_URL."/admin/home.php?q=dashboard");
								exit();	
							} else { 
								$_SESSION['msg'] = '<div class="error_msg"><span>Your account is inactive.Please contact site admin.</span></div>';
							}
						} else {
							$_SESSION['msg'] = '<div class="error_msg"><span>Your are not authorise to login here.</span></div>';
						}
					} else {
						$_SESSION['msg'] = '<div class="error_msg"><span>Your login details are incorrect.</span></div>';
					}
				}else {
					$_SESSION['msg'] = '<div class="error_msg"><span>Please enter your password.</span></div>';
				}
			} else { // password else
				$_SESSION['msg'] = '<div class="error_msg"><span>Please enter your username.</span></div>';
			}
		} else { // username else
			$_SESSION['msg'] = '<div class="error_msg"><span>Please enter your username and password.</span></div>';
	}
} // main if closed

## Assign success or error msg to smarty variable and unset session variable
if(trim($_SESSION['msg']) != '') {
	$smarty->assign('msg', $_SESSION['msg']);
	unset($_SESSION['msg']);
}


## Unset all the objects created which are on this page
unset($userObj);

$smarty->display(TEMPLATEDIR_ADMIN. 'index.tpl');
unset($smarty);
?>