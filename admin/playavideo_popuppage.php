<?php
## include required files
/*******************************/
require_once 'config/settings.php';
require_once '../model/videos.php';

## Create Objects
/*******************************/
$videosObj = new Model_videos();

## Get Playa Video By Request VideoId -
/*******************************/
$vId = $_REQUEST['vId'];
$videoArray = $videosObj->getPlayaOfDayById($vId);

	if($videoArray['video_type']=='vimeo'){
		$videoArray['video_url'] = str_replace("\\","",$videoArray['video_url']);
	    $video_url = explode('"', $videoArray['video_url']);
	    $videoArray['videourl'] = $video_url[1]."?autoplay=1"; 
	}
	if($videoArray['video_type']=='youtube'){
		$videoArray['video_url'] = str_replace("\\","",$videoArray['video_url']);
	    preg_match('/src="([^"]+)"/', $videoArray['video_url'], $match);
	    $url = $match[1];
	    $arr = explode("/",$url);
	    $url= $arr[count($arr)-1];
	    $videoArray['videourl'] ="https://www.youtube.com/embed/".$url."?autoplay=1";
	}
	$data =0;
	if($videoArray['video_type']=='youtubeurl'){
		$data =1;
	   	$videoArray['video_url'] = str_replace("\\","",$videoArray['video_url']);
	   	$arr = explode("=",$videoArray['video_url']);
	 	$url= $arr[count($arr)-1];
		$videoArray['videourl'] = "https://www.youtube.com/embed/".trim($url)."?autoplay=1";	
	}
	if($videoArray['video_type']=='video'){
		$videoArray['videourl'] = SITE_URL.'/dynamicAssets/videos/'.$videoArray['video_url'].'?autoplay=1';
	}
?>

<div class="row-fluid">
	<div class="span4">
		<div class="span8">
			<iframe width="100%" height="80%" src="<?php echo $videoArray['videourl'];?>" frameborder="0"    class="popup_video" allowfullscreen></iframe>
		</div>
	</div>        
</div>