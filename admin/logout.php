<?php
	require_once 'config/settings.php';
	require_once '../model/users.php';
	$userObj = new Model_Users();
	$loginArray =array();	
	if($_SESSION['loginId']!='')
	{						
		$loginArray['logOutTime']  = date("Y-m-d H:i:s");
		$userObj->updateLoginDetailsByValue($loginArray,$_SESSION['loginId']);
	}
	session_destroy();
	header("location:".SITE_URL."/admin/");	
	exit;
?>
