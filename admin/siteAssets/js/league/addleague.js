$(document).ready(function(){
	var validator = $("#frmAddLeague").validate({
		errorElement:'div',
		rules: {
			orgID:{
				required: true
			},
			image: {
				required: true,
				accept: "jpeg|png|gif|jpg"
			},
			leagueName:{
				required:true,
				remote:SITEROOT+'/home.php?q=checkLeagueNameExist&id='+$("#leagueID").val()
			},
			description:{
				required:true,
				maxlength: 500
			}
		},
		messages: {
			orgID:{
				required: "Please select organizer"
			},
			image: {
				required: "Please upload League image",
				accept: "Please upload valid image. Only jpg, jpeg, gif, png and bmp are allowed"
			},
			leagueName:{
				required:"Please enter League name",
				remote: jQuery.format("League name already in use")
			},
			description:{
				required:"Please enter League Details",
				maxlength: jQuery.format("Should be of atmost {0} characters")
			}
		}
	});

var validator = $("#frmEditLeague").validate({
		errorElement:'div',
		rules: {
			orgID:{
				required: true
			},
			image:{
				accept: "jpeg|png|gif|jpg"
			},
			leagueName:{
				required:true,
				remote:SITEROOT+'/home.php?q=checkLeagueNameExist&id='+$("#leagueID").val()
			},
			description:{
				required:true,
				maxlength: 500
			}
		},
		messages: {
			orgID:{
				required: "Please select organizer"
			},
			image:{
				accept: "Please upload valid image. Only jpg, jpeg, gif, png and bmp are allowed"
			},
			leagueName:{
				required:"Please enter League name",
				remote: jQuery.format("League name already in use")
			},
			description:{
				required:"Please enter League Details",
				maxlength: jQuery.format("Should be of atmost {0} characters")
			}
		}
	});
});
			
