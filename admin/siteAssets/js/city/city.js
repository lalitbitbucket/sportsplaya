jQuery.validator.addMethod("alpha", function(value, element) {
return this.optional(element) || value == value.match(/^[a-zA-Z]+$/);
},"Only Characters Allowed.");	

$(document).ready(function(){
	var validator = $("#frmAddCity").validate({
		
		errorElement:'div',
		rules: {
			search_state: {
				required: true
			},
			cityName: {
				required: true,
				alpha:true,
				remote:SITEROOT+'/home.php?q=checkExistingCity'
			}			
		},
		messages: {
		         search_state : {
				required: "Please select state"
			},
			cityName: {
				required: "Please enter city name",
				remote: jQuery.format("City name already in use")
			}
		}
	
		});
		
		// this for edit city
		var validator = $("#frmEditCity").validate({
		
		errorElement:'div',
		rules: {
			search_state: {
				required: true
			},
			cityName: {
				required: true,
				alpha:true,
				remote:SITEROOT+'/home.php?q=checkExistingCity&id='+$('#city_id').val()
			}			
		},
		messages: {
			search_state: {
				required: "Please select state"
			},
			cityName: {
				required: "Please enter city name",
				remote: jQuery.format("City name already in use")
			}
		}
	
		});
});
