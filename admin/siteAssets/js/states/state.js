jQuery.validator.addMethod("alpha", function(value, element) {
return this.optional(element) || value == value.match(/^[A-Za-z ]+$/);
},"Only Characters Allowed.");	
$(document).ready(function(){
	var validator = $("#frmAddState").validate({
		errorElement:'div',
		rules: {
			country: {
				required: true
			},
			statename: {
				required: true,
				alpha:true,
 				remote:SITEROOT+'/home.php?q=checkstateexist&id='+$("#stateId").val()
			}
		},
		messages: {
			country: {
				required: "Please Select Country"
			},
			statename: {
				required: "Please enter state name ",
 				remote: "State name already in use"
			}
		}	
		});	
	
	var validator = $("#frmEditState").validate({
		errorElement:'div',
		rules: {
			country: {
				required: true
			},
			statename: {
				required: true,	
				alpha:true,			
				remote:SITEROOT+'/home.php?q=checkstateexist&id='+$("#stateId").val()
			}
					
		},
		messages: {
			country: {
				required: "Select Country"
			},
			statename: {
				required: "Please enter state name",
				remote: "State name already in use"
			}
		}
	});
});