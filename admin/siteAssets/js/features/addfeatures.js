$(document).ready(function(){
	var validator = $("#frmFeatures").validate({
		errorElement:'div',
		rules: {
			features_name: {
				required: true
			}
		},
		messages: {
			features_name: {
				required: "Please enter feature name"
			}
		}
	});
	
});
