$(document).ready(function(){
	var validator = $("#frmAddGroup").validate({
		errorElement:'div',
		rules: {
			image: {
				required: true,
				accept: "jpeg|png|gif|jpg"
			},
			name:{
				required:true,
				remote:SITEROOT+'/home.php?q=checkgroupnameexist&id='+$("#group_id").val()
			},
			detail:{
				required:true,
				maxlength: 100
			}
		},
		messages: {
			image: {
				required: "Please upload Group image",
				accept: "Please upload valid image. Only jpg, jpeg, gif, png and bmp are allowed"
			},
			name:{
				required:"Please enter Group name",
				remote: jQuery.format("Group name already in use")
			},
			detail:{
				required:"Please enter Group Details",
				maxlength: jQuery.format("Should be of atmost {0} characters")
			}
		}
	});

var validator = $("#frmEditGroup").validate({
		errorElement:'div',
		rules: {
			image:{
				accept: "jpeg|png|gif|jpg"
			},
			name:{
				required:true,
				remote:SITEROOT+'/home.php?q=checkgroupnameexist&id='+$("#group_id").val()
			},
			detail:{
				required:true,
				maxlength: 100
			}
		},
		messages: {
			image:{
				accept: "Please upload valid image. Only jpg, jpeg, gif, png and bmp are allowed"
			},
			name:{
				required:"Please enter Group name",
				remote: jQuery.format("Group name already in use")
			},
			detail:{
				required:"Please enter Group Details",
				maxlength: jQuery.format("Should be of atmost {0} characters")
			}
		}
	});
});
			