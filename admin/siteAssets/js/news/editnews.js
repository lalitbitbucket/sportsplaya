$(document).ready(function(){	
	var validator = $("#frmEditNews").validate({
		errorElement:"div",
		rules: {
                name:
			{
			    	required: true,
				character:true
			},
				
		description:
			{
			    required: true 
			 }
                    
		},
		messages: {			
                 name: 
			{
			     	 required:  "Please enter name",
				 character: "Please enter characters only"
			 },
			description:
			{
				required:"Please enter description"
			} 
			 
		}
	});
});
