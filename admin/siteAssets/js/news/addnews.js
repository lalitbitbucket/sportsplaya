$(document).ready(function(){	
	var validator = $("#frmAddNews").validate({
		errorElement:"div",
		rules: {
		'sportIds[]':
			{
			    	required: true
				
			},
                title:
			{
			    	required: true
			},
				
		description:
			{
			    required: true 
			 }
                    
		},
		messages: {
		'sportIds[]': 
			{
			     	 required:  "Please select sports"
			},			
                 title: 
			{
			     	 required:  "Please enter name"
			},
		description:
			{
				required:"Please enter description"
		         } 
			 
		}
	});
});
