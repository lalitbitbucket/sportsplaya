$(document).ready(function(){
	var validator = $("#frmchangelogo").validate({
		errorElement:'div',
		rules: {
			configValue: {
				required: true
			}			
		},
		messages: {
			configValue: {
				required: "Enter config value"
			}
		}
	});
});