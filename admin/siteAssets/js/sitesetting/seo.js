$(document).ready(function(){
	var validator = $("#frmEditSeo").validate({
		errorElement:'div',
		rules: {
			seoTitle: {
				required: true
			},
			seoKeywords: {
				required: true
			},
			seoDescp: {
				required: true
			}			
		},
		messages: {
			seoTitle: {
				required: "Please enter seo title."
			},
			seoKeywords: {
				required: "Please enter seo keywords."
			},
			seoDescp: {
				required: "Please enter seo description."
			}
		}
	});
});