$(document).ready(function(){
	var validator = $("#frmSMTP").validate({
		errorElement:'div',
		rules: {
			isAuth: {
				required: true
			},
			title:{
				required: true,
				minlength: 3,
				maxlength: 50
			},
			secure:{
				required: true,
				minlength: 3,
				maxlength: 50
			},
			smtpPort:{
				required: true,
				minlength: 2,
				maxlength: 50
			},
			smtpHost:{
				required: true,
				minlength: 3,
				maxlength: 50
			},
			smtpUsername:{
				required: true,
				minlength: 6,
				maxlength: 50
			},
			smtpPassword:{
				required: true,
				minlength: 6,
				maxlength: 50
			},
			status: {
				required: true
			}
		},
		messages: {
			isAuth: {
				required: "Please select authentication"
			},
			title: {
				required: "Please enter title",
				minlength: jQuery.format("Enter at least {0} characters"),
				maxlength: jQuery.format("Enter maximum  {0} characters")
			},
			secure: {
				required: "Please enter secure",
				minlength: jQuery.format("Enter at least {0} characters"),
				maxlength: jQuery.format("Enter maximum  {0} characters")
			},
			smtpPort: {
				required: "Please enter Port",
				minlength: jQuery.format("Enter at least {0} characters"),
				maxlength: jQuery.format("Enter maximum  {0} characters")
			},
			smtpHost: {
				required: "Please enter host",
				minlength: jQuery.format("Enter at least {0} characters"),
				maxlength: jQuery.format("Enter maximum  {0} characters")
			},
			smtpUsername: {
				required: "Please enter username",
				minlength: jQuery.format("Enter at least {0} characters"),
				maxlength: jQuery.format("Enter maximum  {0} characters")
			},
			smtpPassword:{
				required: "Please enter password",
				minlength: jQuery.format("Enter at least {0} characters"),
				maxlength: jQuery.format("Enter maximum  {0} characters")
			},
			status: {
				required: "Please select status"
			}
		}
	});
});