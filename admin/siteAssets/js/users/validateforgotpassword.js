$(document).ready(function(){
	var validator = $("#frmforgotpassword").validate({
		errorElement:'div',
		rules: {
			email: {
				required: true,
				email: true
			}
			},
		messages: {
			email: {
				required: "Enter your email",
				email:"Invalid email address. Please re-enter valid email"
			}			
		}
	});
});