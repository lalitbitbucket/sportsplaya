$.validator.addMethod("characterSpace", function(value, element) {
return this.optional(element) || value == value.match(/^[A-Za-z0-9]+$/);
},"No symbols or spaces,please");

$.validator.addMethod("nowhitespace", function(value, element) {
return this.optional(element) || value == value.match(/^[\w\.@]{3,100}$/);
},"Please don't enter special character.");

$.validator.addMethod("noSpace", function(value, element) { 
  return value.indexOf(" ") < 0 && value != ""; 
}, "No space please and don't leave it empty");

jQuery.validator.addMethod("alpha", function(value, element) {
return this.optional(element) || value == value.match(/^[a-zA-Z]+$/);
},"Only Characters Allowed.");	

$.validator.addMethod("phonno", function(value, element) {
	return this.optional(element) || value == value.match(/^[0-9-+()]+$/);
}, "Enter valid contact number");		

$.validator.addMethod("code", function(value, element) {
return this.optional(element) || value == value.match(/^[a-zA-Z0-9]+$/);
},"Please enter valid postal code.");	

$(document).ready(function(){
	var validator = $("#frmAddSeller").validate({
		errorElement:"div",
		rules: {			
			username: {
				required: true,				
				remote:SITEROOT+'/home.php?q=checkSiteUserNameExist'													
			},
			first_name: {
				required: true
				//character:true,
				//minlength:3,
				//maxlength:20,
				//characterSpace:true
			},
			last_name: {
				required: true
				//character:true,
				//minlength:3,
				//maxlength:20,				
				//characterSpace:true
			},						
			email: {
				required: true,
				email: true,
				remote:SITEROOT+'/home.php?q=checkSiteUserEmailExist'
			},
			password: {
				required: true,
				minlength:6,
				maxlength:20,
				noSpace:true,
				nowhitespace:true
			},
			confirm_password:{
				required: true,
				equalTo:"#password",
				noSpace:true,
				nowhitespace:true
			},
	                gender:{
				required: true
			},
			ageRange:{
				required: true
			},
			image :{				
				accept:"gif|png|jpg|jpeg|GIF|PNG|JPG|JPEG"
			}	
		},
		messages: {			
			username:{
				required: "Please enter username",
				minlength: jQuery.format("Enter at least {0} characters"),
				maxlength: jQuery.format("Enter at most {0} characters"),				
				remote :"Username already exist "				
			},
			first_name: {
				required: "Please enter first name"
				//character: "Please enter characters only"
				//minlength: jQuery.format("Enter at least {0} characters"),
				//maxlength: jQuery.format("Enter at most {0} characters"),				
			},
			last_name: {
				required: "Please enter last name"
				//character: "Please enter characters only"
				//minlength: jQuery.format("Enter at least {0} characters"),
				//maxlength: jQuery.format("Enter at most {0} characters"),
			},					
			email: {
				required: "Enter email address",
				email: "Enter valid email address",
				remote: jQuery.format("Email address already in use")
			},
			password: {
				required: "Enter your password",
				minlength: jQuery.format("Enter at least 6 characters"),
				maxlength: jQuery.format("Enter maximum  20 characters")
			},
			confirm_password:{
				required: "Enter confirm password",
				equalTo:"Passwords do not match"
			},
			gender: {
				required: "Select gender type"
			},
			ageRange: {
				required: "Select age range"
			},
			image :{				
				accept:"Please upload valid image"
			}
		}
	});
	
	
	var validator = $("#frmEditUser").validate({
		errorElement:'div',
		rules: {	       
			username: {
				required: true,				
				//minlength:3,
				//maxlength:20,
				//characterSpace:true,
     			remote:SITEROOT+'/home.php?q=checkSiteUserNameExist&id='+$('#hidden_userid').val()													
			},
			first_name: {
				required: true
				//minlength:3,
				//maxlength:20,
				//character:true,
				//characterSpace:true
			},
			last_name: {
				required: true
				//minlength:3,
				//maxlength:20,
				//character:true,
				//characterSpace:true
			},					
			email: {
				required: true,
				email: true,
				remote:SITEROOT+'/home.php?q=checkSiteUserEmailExist&id='+$("#hidden_userid").val()
			},
			password: {
				required: true,
				minlength:6,
				maxlength:20,
				noSpace:true,
				nowhitespace:true
			},
			country:{
				required: true				
			},
			state:{
				/*required: true,
				alpha:true,
				nowhitespace:true,
				maxlength: 20,
				minlength: 3*/			
			},
			code:{
				required: true,
				code:true,
				maxlength: 6,
				minlength: 5
			},			
			contact:{
				required: true
				//phonno:true,
				//maxlength: 15,
				//minlength: 10
				
			},
			address:{
				required: true
			},
			image :{				
				accept:"gif|png|jpg|jpeg|GIF|PNG|JPG|JPEG"
			}
		},
		messages: {			
			username:{
				required: "Please enter username",
				minlength: jQuery.format("Enter at least {0} characters"),
				maxlength: jQuery.format("Enter at most {0} characters"),				
				remote :"Username already exist "				
			},
			first_name: {
				required: "Please enter first name"
				//minlength: jQuery.format("Enter at least {0} characters"),
				//maxlength: jQuery.format("Enter at most {0} characters"),
				//character: "Please enter characters only",
				//noSpace:"No space is allowed"
			},
			last_name: {
				required: "Please enter last name"
				//minlength: jQuery.format("Enter at least {0} characters"),
				//maxlength: jQuery.format("Enter at most {0} characters"),
				//character: "Please enter characters only",
				//noSpace:"No space is allowed"
			},		
			email: {
				required: "Enter email address",
				email: "Enter valid email address",
				remote: jQuery.format("Email address already in use")
			},
			password: {
				required: "Enter your password",
				minlength: jQuery.format("Enter at least 6 characters"),
				maxlength: jQuery.format("Enter maximum  20 characters")
			},
			country:{
				required: "Please select country"		
			},
			state:{
				//required: "Please enter state"
				
			},
			code:{
				required: "Please enter code",
				maxlength: jQuery.format("Should be of atmost {0} characters"),
				minlength: jQuery.format("Should be of atleast {0} characters")
			},	
			contact:{
				required: "Please enter contact number"
				//maxlength: jQuery.format("Should be of atmost {0} characters"),
				//minlength: jQuery.format("Should be of atleast {0} characters")
			},
			address:{
				required: "Please enter address"
			},
			image :{				
				accept:"Please upload valid image"
			}
		}
	});
	
	
});