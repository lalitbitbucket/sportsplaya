$(document).ready(function(){
	var validator = $("#frmChangePassword").validate({
		errorElement:'div',
		rules: {
			cpassword: {
				required: true,
				nowhitespace: true
			},
			npassword: {
				required: true,
				nowhitespace: true,
				maxlength: 16,
				minlength:5
			},
			confirmpassword: {
				required: true,
				nowhitespace: true,
				equalTo: "#npassword"
			}
		},
		messages: {
			cpassword: {
				required: "Enter your current password"
			},	
			npassword: {
				required: "Enter your new password",
				maxlength: jQuery.format("Enter at most {0} characters"),
				minlength: jQuery.format("Enter at least {0} characters")
			},
			confirmpassword: {
				required: "Enter confirm password",
				equalTo: "Confirm password should be same as password"
				
			}
		}
	});
	$.validator.addMethod("nowhitespace", function(value, element) {
    	return this.optional(element) || /^\S+$/i.test(value);
	}, "Blank space not allowed");
});