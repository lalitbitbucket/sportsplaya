$(document).ready(function(){
	var validator = $("#frmeditprofile").validate({
		errorElement:'div',
		rules: {
			user_avatar: {
				accept: "jpeg|png|gif|jpg"
			
			},
			first_name:{
				required:true,
				character:true
			},
			last_name:{
				required:true,
				character:true
			}
		},
		messages: {
			user_avatar: {
				accept:"Please upload only jpeg|png|gif|jpg type of image"
				
			},
			first_name:{
				required:"Please enter the first name",
				character: "Please enter characters only"
			},
			last_name:{
				required:"Please enter the last name",
				character: "Please enter characters only"
			}
		}
	});
});