$(document).ready(function(){
	var validator = $("#frmresetpassword").validate({
		errorElement:'div',
		rules: {
			pass: {
				required: true,
				minlength: 5,
				maxlength: 15
			},
			cpass: {
				required: true,
				equalTo: "#pass"
			}
		},
		messages: {
			pass: {
				required: "Enter your new password",
				minlength: jQuery.format("Enter at least {0} characters"),
				maxlength: jQuery.format("Enter maximum  {0} characters")
			},	
			cpass: {
				required: "Enter confirm password",
				equalTo: "Confirm password should be same as new password"
				
			}
		}
	});
});