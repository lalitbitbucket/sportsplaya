$(document).ready(function(){
	var validator = $("#frmLogin").validate({
		errorElement:'div',
		rules: {
			username: {
				required: true,
				nowhitespace: true
			},
			password: {
				required: true,
				nowhitespace: true
			}
		},
		messages: {
			username: {
				required: "Enter your username"
			},	
			password: {
				required: "Enter your password"
			}
		}
	});
	$.validator.addMethod("nowhitespace", function(value, element) {
    	return this.optional(element) || /^\S+$/i.test(value);
	}, "Blank space not allowed");
});
