$.validator.addMethod("characterSpace", function(value, element) {
return this.optional(element) || value == value.match(/^[A-Za-z0-9]+$/);
},"No symbols or spaces,please");

$.validator.addMethod("phonno", function(value, element) {
	return this.optional(element) || value == value.match(/^[0-9-+()]+$/);
}, "Enter valid contact number");
$(document).ready(function(){
	var validator = $("#frmAddUser").validate({
		errorElement:'div',
		rules: {
			first_name: {
				required: true,
				character:true
			},
			last_name: {
				required: true,
				character:true
			},
			uname: {
				required: true,
				characterSpace:"#username",				
				remote:SITEROOT+'/home.php?q=checkSiteUserNameExist'
			},
			email: {
				required: true,
				email: true,
				remote:SITEROOT+'/home.php?q=checkAdminEmailExist'
			}
                       
		},
		messages: {
			
			first_name: {
				required: "Enter first name",
				character: "Please enter characters only"
			},
			last_name: {
				required: "Enter last name",
				character: "Please enter characters only"
			},
			uname: {
				required: "Enter username",
				character: "Please enter characters only",
				remote: jQuery.format("Username already in use")
			},	
			email: {
				required: "Enter email address",
				email: "Enter valid email address",
				remote: jQuery.format("Email address already in use")
			}
                        
		}
	});
	
	
	var validator = $("#frmEditUser").validate({
		errorElement:'div',
		rules: {
			first_name: {
				required: true,
				character:true
			},
			last_name: {
				required: true,
				character:true
			},
			email: {
				required: true,
				email: true,
				remote:SITEROOT+'/home.php?q=checkAdminEmailExist&id='+$("#hidden_userid").val()
			}
                        
		},
		messages: {
			first_name: {
				required: "Enter first name",
				character: "Please enter characters only"
			},
			last_name: {
				required: "Enter last name",
				character: "Please enter characters only"
			},
			email: {
				required: "Enter email address",
				email: "Enter valid email address",
				remote: jQuery.format("Email address already in use")
			}
			
		}
	});
});