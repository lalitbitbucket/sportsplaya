$(document).ready(function(){
	var validator = $("#frmChangeUsername").validate({
		errorElement:'div',
		rules: {
			username: {
				required: true,
				nowhitespace: true,
				minlength:5,
				maxlength: 20,				
				remote:SITEROOT+'/home.php?q=checkUserNameExist'
			
			},
			password: {
				required: true,
				nowhitespace: true
			}
		},
		messages: {
			username: {
				required: "Enter your username",
				minlength: jQuery.format("Enter at least {0} characters"),
				maxlength: jQuery.format("Enter at most {0} characters"),				
				remote: jQuery.format("Username already in use")
				
			},	
			password: {
				required: "Enter your current password"
			}
		}
	});
	$.validator.addMethod("nowhitespace", function(value, element) {
    	return this.optional(element) || /^\S+$/i.test(value);
	}, "Blank space not allowed");
});