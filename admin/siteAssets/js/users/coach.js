$.validator.addMethod("characterSpace", function(value, element) {
          return this.optional(element) || value == value.match(/^[A-Za-z0-9]+$/);
},"No symbols or spaces,please");

$.validator.addMethod("noSpace", function(value, element) { 
	  return value.indexOf(" ") < 0 && value != ""; 
}, "No space please and don't leave it empty");

jQuery.validator.addMethod("phoneUS", function(phone_number, element) {
    phone_number = phone_number.replace(/\s+/g, "");    
    return this.optional(element) || phone_number.length > 9 &&
        phone_number.match(/^(1-?)?(\([2-9]\d{2}\)|[2-9]\d{2})-?[2-9]\d{2}-?\d{4}$/);
}, "Please specify a valid phone number");


$(document).ready(function(){
	var validator = $("#frmAddUser").validate({
		errorElement:'div',
		rules: {
			first_name: {
				required: true,
                                 minlength:2,
                                maxlength:20,
                                character:true
			},
			last_name: {
				required: true,
				 minlength:2,
                                maxlength:20,
                                character:true
			},
			email: {
				required: true,
				email: true,
				remote:SITEROOT+'/home.php?q=checkEmailExist'
			},
			password: {
			      required: true,
			      minlength:6,
			      maxlength:20,
			      noSpace:true
			},
                        address: {
			       required: true,
                               maxlength:200      
			},
                        city: {
			      required: true,
                              maxlength:20,
                              character:true       
                              
			},
                        state: {
			      required: true
			},
                        zip: {
                              required: true,
                              number:true,
                              minlength:5,
                              maxlength:5
			},
                        contact: {
			      required: true,
			      phoneUS:true                            
			}
		},
		messages: {
			first_name: {
				required: "Enter first name",
                                minlength: jQuery.format("Enter at least {0} characters"),
                                maxlength: jQuery.format("Enter at most {0} characters"),
                                character: "Please enter characters only"
			},
			last_name: {
				required: "Enter last name",
                                minlength: jQuery.format("Enter at least {0} characters"),
                                maxlength: jQuery.format("Enter at most {0} characters"),
                                character: "Please enter characters only"
			},
			email: {
				required: "Enter email address",
				email: "Enter valid email address",
				remote: jQuery.format("Email address already in use")
			},
			password: {
			      required: "Please Enter password",
			      minlength: jQuery.format("Enter at least {0} characters"),
                              maxlength: jQuery.format("Enter at most {0} characters")
              
			},
                        address: {
			      required: "Enter address ",
                               maxlength: jQuery.format("Enter at most {0} characters")
              
			},
                        city: {
			      required: "Enter city name",
                              maxlength: jQuery.format("Enter at most {0} characters"),
                              character: "Please enter characters only"   
			},
                        state: {
			      required: "Enter state name"
			},
                        zip: {
		              required: "Enter zip code",
                              number: "Please enter digit only",
                              minlength: jQuery.format("Enter maximum 5 digits"),
                              maxlength: jQuery.format("Enter maximum 5 digits")
			},
                        contact: {
			      required: "Enter contact number"                              
			}
		}
	});
	
	
	var validator = $("#frmEditUser").validate({
		errorElement:'div',
		rules: {
			first_name: {
				required: true,
				  minlength:2,
                                maxlength:20,
                                character:true
			},
			last_name: {
				required: true,
				  minlength:2,
                                maxlength:20,
                                character:true
			},
			email: {
				required: true,
				email: true,
				remote:SITEROOT+'/home.php?q=checkEmailExist&id='+$("#hidden_userid").val()
			},
                        address: {
			      required: true,
                               maxlength:200      
			},
                        city: {
			      required: true,
                              maxlength:20,
                              character:true       
			},
                        state: {
			      required: true
			},
                        zip: {
			      required: true,
                              number:true,
                              minlength:5,
                              maxlength:5
			},
                        contact: {
			      required: true,
			      phoneUS:true      
                             
			}
		},
		messages: {
			first_name: {
				required: "Enter first name",
				 minlength: jQuery.format("Enter at least {0} characters"),
                                maxlength: jQuery.format("Enter at most {0} characters"),
                                character: "Please enter characters only"
			},
			last_name: {
				required: "Enter last name",
				 minlength: jQuery.format("Enter at least {0} characters"),
                                maxlength: jQuery.format("Enter at most {0} characters"),
                                character: "Please enter characters only"
			},
			email: {
				required: "Enter email address",
				email: "Enter valid email address",
				remote: jQuery.format("Email address already in use")
			},
			address: {
			      required: "Enter address"
			},
                        city: {
			      required: "Enter city name",
                               maxlength: jQuery.format("Enter at most {0} characters"),
                              character: "Please enter characters only"   
			},
                        state: {
			      required: "Enter state name"
			},
                        zip: {
			      required: "Enter zip code",
                              number: "Please enter digit only",
                              minlength: jQuery.format("Enter maximum 5 digits"),
                              maxlength: jQuery.format("Enter maximum 5 digits")
			},
                        contact: {
			      required: "Enter contact number"                             
			}
		}
	});
});