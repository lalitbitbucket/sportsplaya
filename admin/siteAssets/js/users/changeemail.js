$(document).ready(function(){
	var validator = $("#frmChangeEmail").validate({
		errorElement:'div',
		rules: {
			email: {
				required: true,
				email: true,			
				remote:SITEROOT+'/home.php?q=checkEmailExist'
			},
			password: {
				required: true,
				nowhitespace: true
			}
		},
		messages: {
			email: {
				required: "Enter your email address.",
				email:"Please enter valid email address.",
				remote: jQuery.format("Email address already in use")	
			},	
			password: {
				required: "Enter your current password"
			}
		}
	});
	$.validator.addMethod("nowhitespace", function(value, element) {
    	return this.optional(element) || /^\S+$/i.test(value);
	}, "Blank space not allowed");
});