$(document).ready(function(){
	var validator = $("#frmAddOrganizer").validate({
		
		errorElement:'div',
			rules: {
				orgName: {
					required: true,	
					character:true,				
					remote:SITEROOT+'/home.php?q=checkOrganizerExist&id='+$("#organizerId").val()
				}
			},
			messages: {
				orgName: {
					required: "Please enter orgnizer name",	
					character:"Please enter character only",					
					remote: jQuery.format("Organizer name already in use")
				}
			}
		});
	
	
	var validator = $("#frmEditOrganizer").validate({		
		errorElement:'div',		
			rules: {
				orgName: {
					required: true,
					character:true,							
					remote:SITEROOT+'/home.php?q=checkOrganizerExist&id='+$("#organizerId").val()
				}
			},
			messages: {
				orgName: {
					required: "Please enter orgnizer name",		
					character:"Please enter character only",			
					remote: jQuery.format("Organizer name already in use")				
				}
			}
		});		
	
});
jQuery.validator.addMethod("alpha", function(value, element) {
return this.optional(element) || value == value.match(/^[a-zA-Z]+$/);
},"Only characters allowed.");

