$(document).ready(function(){
	var validator = $("#frmAddCountry").validate({
		
		errorElement:'div',
			rules: {
				countryname: {
					required: true,
					character:true,
					remote:SITEROOT+'/home.php?q=checkcountryexist'
				}
			},
			messages: {
				countryname: {
					required: "Please enter country name",					
					remote: jQuery.format("Country name already in use")
				}
			}
		});
	
	
	var validator = $("#frmEditCountry").validate({		
		errorElement:'div',		
			rules: {
				countryname: {
					required: true,		
					character:true,	
					remote:SITEROOT+'/home.php?q=checkcountryexist&id='+$("#country_id").val()
				}
			},
			messages: {
				countryname: {
					required: "Please enter country name",					
					remote: jQuery.format("Country name already in use")				
				}
			}
		});		
	
});
jQuery.validator.addMethod("alpha", function(value, element) {
return this.optional(element) || value == value.match(/^[a-zA-Z]+$/);
},"Only Characters Allowed.");

