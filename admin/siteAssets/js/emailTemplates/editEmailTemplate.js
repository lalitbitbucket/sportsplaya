$(document).ready(function(){
	var validator = $("#frmEditEmail").validate({
		errorElement:'div',
		rules: {
			email_type: {
				required: true
			},
			email_subject: {
				required: true
			},
			from_name: {
				required: true
			},
			from_email: {
				required: true,
				email: true
			},
			email_content:{
				required: true
			}
		},
		messages: {
			email_type: {
				required: "Enter email type"
			},
			email_subject: {
				required: "Enter email subject"
			},
			from_name: {
				required: "Enter from name"
			},
			from_email: {
				required: "Enter from email address",
				email: "Please enter valid email address"
			},
			email_content:{
				required: "Enter email content"
			}
		}
	});		
	
});
