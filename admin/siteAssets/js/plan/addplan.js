$(document).ready(function(){
	var validator = $("#frmplan").validate({
		errorElement:'div',
		rules: {
			plan_name: {
				required: true
			}
		},
		messages: {
			plan_name: {
				required: "Please enter your plan name"
			}
		}
	});
	
});
