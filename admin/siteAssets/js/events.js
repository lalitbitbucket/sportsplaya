$(document).ready(function(){
	var validator = $("#frmevnet").validate({
		errorElement:'div',
		rules: {
			event_name: {
				required: true,
				character:true,
				maxlength: 20,
				minlength: 2
			},
			address: {
				required: true,
				maxlength: 100,
				minlength: 10
			},
			city:{
				required: true,
                character:true 
				},
			state:{
					required:true,
				  character:true
			},
			zipcode: {
				required: true,
				number: true,				
				maxlength: 6,
				minlength: 5
			},
			start_date: {
					required:true,
					date:true,
					dateval:true,
			},
			starttime: {
					required:true,
					time:true
					},
			end_date: {
					required:true,
					date:true,
					dateval:true,
			},
			endtime: {
					required:true,
					time:true
					},
			attendees: {
				required:true,
				number: true,
				maxlength: 3,
				minlength: 2
			}
			
		},
		messages: {
		
			event_name: {
				required: "Please enter event name.",
				character: "Please enter characters only",
				maxlength: jQuery.format("Should be of atmost {0} characters"),
				minlength: jQuery.format("Should be of atleast {0} characters")
			},
			address: {
				required: "Please enter your address.",
				maxlength: jQuery.format("Should be of atmost {0} characters"),
				minlength: jQuery.format("Should be of atleast {0} characters")
			},
		
			city: {
				required: "Please enter your city name",
                character: "Please enter characters only"
			},	
			state: {
		    required: "Please enter your state name",
			 character: "Please enter characters only"
			},
			zipcode: {
				required: "Please enter your zip code.",			
				maxlength: jQuery.format("Should be of atmost {0} numbers"),
				minlength: jQuery.format("Should be of atleast {0} numbers")
			},
			start_date:{
			 required: "Please enter start date"
			},
			starttime:{
			 required: "Please enter start time"
			},
			end_date:{
			 required: "Please enter end date"
			},
			endtime:{
			 required: "Please enter end time"
			},
			
			attendees: {
				required: "Please enter attendees", 				
				maxlength: jQuery.format("Maximum {0} characters are required"),
				minlength: jQuery.format("Minimum {0} characters are required")
			}
				
		}
	});
		$.validator.addMethod("dateval", function(value, element) {
    	// return value.match(/^(0[1-9]|1[012])[/|-](0[1-9]|[12][0-9]|3[01])[/|-](19dd|2ddd)$/);
		return this.optional(element) || /^(\d{4})(-|\/)(([0-1]{1})([1-2]{1})|([0]{1})([0-9]{1}))(-|\/)(([0-2]{1})([1-9]{1})|([3]{1})([0-1]{1}))/.test(value);
		 
			}, "Please enter a date in the format yyyy-mm-dd");
		$.validator.addMethod("time", function(value, element) {  
return this.optional(element) || /^(([0-1]?[0-9])|([2][0-3])):([0-5]?[0-9])(:([0-5]?[0-9]))?$/i.test(value);  
}, "Please enter a valid time.");

});
