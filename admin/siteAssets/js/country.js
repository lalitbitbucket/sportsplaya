$(document).ready(function(){
	var validator = $("#frmAddCountry").validate({		
		errorElement:'div',
			rules: {
				countryname: {
					required: true,
					minlength:2,
					maxlength:50,
					remote:SITEROOT+'/home.php?q=checkcountryexist'
				},
				flag: {
                required: true,   
				accept: "jpg|jpeg|png|gif|bmp"
			}						
			},
			messages: {
				countryname: {
					required: "Please enter country name",
					minlength: jQuery.format("Enter at least {0} characters"),
					maxlength: jQuery.format("Enter at most {0} characters"),	
					remote: "Country name already in use"
				},
				flag: {
                required: "Pleaseupload country flag",     
			accept: "Please upload valid image. Only jpg, jpeg, gif, png and bmp are allowed"
			}
				
			}
		});
	var validator = $("#frmEditCountry").validate({		
		errorElement:'div',		
			rules: {
				countryname: {
					required: true,		
					alpha:true,
					minlength:2,
					maxlength:50,
					remote:SITEROOT+'/home.php?q=checkcountryexist&id='+$("#country_id").val()
				},
				flag: {
				accept: "jpg|jpeg|png|gif|bmp"
			}
			},
			messages: {
				countryname: {
					required: "Please enter country name",
					minlength: jQuery.format("Enter at least {0} characters"),
					maxlength: jQuery.format("Enter at most {0} characters"),					
					remote: jQuery.format("Country name already in use")				
				},
				flag: {
				accept: "Please upload valid image. Only jpg, jpeg, gif, png and bmp are allowed"
			}
			}
		});	
});