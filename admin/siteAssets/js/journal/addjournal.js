$(document).ready(function(){
	var validator = $("#frmJournal").validate({
		errorElement:'div',
		rules: {
			jnote: {
				required: true
			},
			jDisc: {
				required: true
			}
		},
		messages: {
			jnote: {
				required: "Please enter Journal Title"
			},
			jDisc: {
				required: "Please enter Journal Detail"
			}
		}
	});
	
});
