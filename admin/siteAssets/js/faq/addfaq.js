$(document).ready(function(){
	var validator = $("#frmFaq").submit(function() {
            // update underlying textarea before submit validation
            tinyMCE.triggerSave();
        }).validate({
		errorElement:'div',
        ignore: "", 
		rules: {
			faq_cat: {
				required: true
			},
			faq_ques: {
				required: true
			},
            faqans_content: {
                required: true
            }
		},
		messages: {
			faq_cat: {
				required: "Select faq category"
			},
			faq_ques: {
				required: "Enter faq question"
			},
            faqans_content: {
                required: "Enter faq answer"
            }
		},
            errorPlacement: function(label, element) {
                // position error label after generated textarea
                if (element.is("textarea")) {
                    label.insertAfter(element.next());
                } else {
                    label.insertAfter(element)
                }
            }
	});
	validator.focusInvalid = function() {
            // put focus on tinymce on submit validation
            if( this.settings.focusInvalid ) {
                try {
                    var toFocus = $(this.findLastActive() || this.errorList.length && this.errorList[0].element || []);
                    if (toFocus.is("textarea")) {
                        tinyMCE.get(toFocus.attr("id")).focus();
                    } else {
                        toFocus.filter(":visible").focus();
                    }
                } catch(e) {
                    // ignore IE throwing errors when focusing hidden elements
                }
            }
        }
});
