$(document).ready(function(){
	var validator = $("#frmAds").validate({
		errorElement:'div',
		rules: {
			ads_title: {
				required: true
			},
			adsURL: {
				required: true,
				url: true
			},
			adsPosition: {
				required: true
			},
			adsImageleft: {
				required: true,
				accept: 'jpg|JPG|jpeg|JPEG|png|PNG|bmp|BMP|gif|GIF'
			}
		},
		messages: {
			ads_title: {
				required: "Please enter ads title"
			},
			adsURL: {
				required: "Please enter ads title",
				url: "Please enter valid URL"
			},
			adsPosition: {
				required: "Please select ads position"
			},
			adsImageleft: {
				required: "Please select ads image",
				accept: 'Upload only jpg, jpeg, png, bmp, gif files'
			}
		}
	});
	
	var validator = $("#frmEditAds").validate({
		errorElement:'div',
		rules: {
			ads_title: {
				required: true
			},
			adsURL: {
				required: true,
				url: true
			},
			adsPosition: {
				required: true
			},
			adsImageleft: {
				accept: 'jpg|JPG|jpeg|JPEG|png|PNG|bmp|BMP|gif|GIF'
			}
		},
		messages: {
			ads_title: {
				required: "Please enter ads title"
			},
			adsURL: {
				required: "Please enter ads title",
				url: "Please enter valid URL"
			},
			adsPosition: {
				required: "Please select ads position"
			},
			adsImageleft: {
				accept: 'Upload only jpg, jpeg, png, bmp, gif files'
			}
		}
	});
});
