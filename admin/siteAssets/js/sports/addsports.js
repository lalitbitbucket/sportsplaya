$(document).ready(function(){
	var validator = $("#frmSports").validate({
		errorElement:'div',
		rules: {
			sports_title: {
				required: true
			},
			image: {
				required: true,
				accept: 'jpg|JPG|jpeg|JPEG|png|PNG|bmp|BMP|gif|GIF'
			}
		},
		messages: {
			sports_title: {
				required: "Please enter sports name"
			},
			image: {
				required: "Please select sports image",
				accept: 'Upload only jpg, jpeg, png, bmp, gif files'
			}
		}
	});
	
	var validator = $("#frmEditSports").validate({
		errorElement:'div',
		rules: {
			sports_title: {
				required: true
			},
			image: {
				accept: 'jpg|JPG|jpeg|JPEG|png|PNG|bmp|BMP|gif|GIF'
			}
		},
		messages: {
			sports_title: {
				required: "Please enter sports name"
			},
			image: {
				accept: 'Upload only jpg, jpeg, png, bmp, gif files'
			}
		}
	});
});
