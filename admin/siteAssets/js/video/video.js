$(document).ready(function(){
	var validator = $("#videos").validate({
		errorElement:'div',
		rules: {			
			flvvideo:{
			required: function() {
					  var chk_flg1 = $('#video_type').attr("checked");
					  var chk_flg2 = $('#gallery_video').val();
					  if(chk_flg1 == true){
					  if(chk_flg2 == '')
					  {
						 return true;   
					  }
					  else
					  {
						 return false;  
					  }
					  }else{
						 
						 return false;   
					}
				},
			notEqual: true
			},
			embeded_code: {
				required: function() {
					var chk_flg1 = $('#you_tube_video').attr("checked");
					if(chk_flg1 == true){
					return true;
					}else{
					return false;	
					}
			}
			},
			youtubeurl: {
				required: function() {
					var chk_flg1 = $('#you_tube_video_url').attr("checked");
					if(chk_flg1 == true){
					return true;
					}else{
					return false;	
					}
			}
			},
			title: {
				required: true,
				minlength: 2,
				maxlength:100
			},
			description: {
				required: true,
				minlength: 2,
				maxlength:4000
				
			}
			
		},
		messages: {						
			flvvideo: {
				required: "Please select video"
			},	
			embeded_code:{
				required: "Please add an embeded code for video"
			},
			youtubeurl:{
				required: "Please add an youtube url for video"
			},		
			title: {
				required: "Please enter title",
				minlength: jQuery.format("Enter at least {0} characters"),
				maxlength: jQuery.format("Enter at most {0} characters")
			},
			description: {
				required: "Please enter description",
				minlength: jQuery.format("Enter at least {0} characters"),
				maxlength: jQuery.format("Enter at most {0} characters")
			}
		}
	});
	
		$.validator.addMethod("notEqual", function(value, element, param) {
		var xlsx = value.substr(value.length - 5);
		var xls = value.substr(value.length - 4);
		return this.optional(element) || xls == '.mp4' || xls == '.m4v' || xls == '.f4v' || xls == '.mov ' ;
		}, "Select only mp4, f4v, m4v, mov files.");

});


function limitText(limitField, limitCount, limitNum) {
	if (limitField.value.length > limitNum) {
		limitField.value = limitField.value.substring(0, limitNum);
	} else {
		limitCount.value = limitNum - limitField.value.length;
	}
}

function check_video_type(type){
	if(type == 'you_tube'){
		document.getElementById("You_tube_ID").style.display="block";		
		document.getElementById("Vedio_ID").style.display="none";
		document.getElementById("You_tube_url_ID").style.display="none";
	}else if(type == 'video'){	
		document.getElementById("Vedio_ID").style.display="block";	
		document.getElementById("You_tube_ID").style.display="none";
		document.getElementById("You_tube_url_ID").style.display="none";
	}else{
		document.getElementById("You_tube_url_ID").style.display="block";	
		document.getElementById("Vedio_ID").style.display="none";
		document.getElementById("You_tube_ID").style.display="none";
	}
}
