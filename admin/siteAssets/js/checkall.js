	$(document).ready(function()
		{
			$("#check").click(function()				
			{
				
				if(this.checked != true){
					flag=false;
				}
				var checked_status = this.checked;
				$("input:checkbox").each(function()
				{
					this.checked = checked_status;
					makeSelected(this);
				});
			});
			
			
			function makeSelected(chk)
			{
				var $tr = $(chk).parent().parent();
				if($tr.attr('id'))
				{
					
					if($tr.attr('class')=='activerow' && !chk.checked)
						$tr.removeClass('activerow').addClass('inactiverow');
					else
						$tr.removeClass('inactiverow').addClass('activerow');
				}
			}
			
			$("input:checkbox:not('#check')").click(function()
			{
				if(this.checked != true){
					flag=false;
				}
				else{
					makeSelected(this);
				}
				
			});
			
			
			var flag = false;
				$("#frm").submit(function(){
		
				if($("#action").val()=='')
				{
					$("#errormsg").text("Please select action.").show().fadeOut(4000);
					return false;
				}
				
				$("input:checkbox").each(function()
				{
					var $tr = $(this).parent().parent();
					if($tr.attr('id'))
						if(this.checked == true)
							flag = true;
				});
				
				if (flag == false) {
					$("#errormsg").text("Please select checkbox.").show().fadeOut(4000);
					return false;
				}
				
				if(confirm('Are you sure to perform "'+$("#action").attr('value')+'" action?'))
					return true;
				else
					return false;
			});
			$("#errormsg").fadeOut(5000);
			
								
		});