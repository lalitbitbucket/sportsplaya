$(document).ready(function(){
var validator = $("#frmAddBlogs").validate({
	errorElement:'div',
	rules: {
		title: {
			required: true
		},
		image: {
			required: true,
			accept: "jpg|jpeg|png|gif|bmp"
		}
	},
	messages: {
		title: {
			required: "Please enter blog title"
		},
		image: {
			required: "Please upload image",
			accept: "Please upload valid image. Only jpg, jpeg, gif, png and bmp are allowed"
		}
	}
});

/* FCKEditor validation*/
if(typeof jQuery.fn.fckEditorValidate == 'function') {
	jQuery.fn.fckEditorValidate({instanceName: 'detail'});
}
});
