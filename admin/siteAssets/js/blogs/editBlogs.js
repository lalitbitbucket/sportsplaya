$(document).ready(function(){
	var validator = $("#frmEditBlogs").validate({
		errorElement:'div',
		rules: {
			title: {
				required: true
			},
			image: {
				accept: "jpg|jpeg|png|gif|bmp"
			},
			detail:{
				required: true
			}
		},
		messages: {
			title: {
				required: "Please enter Blog Title"
			},
			image: {
			accept: "Please upload valid image. Only jpg, jpeg, gif, png and bmp are allowed"
			},
			detail:{
				required: "Please enter Blog  Detail"
			}
		}
	});
	
	/* FCKEditor validation*/
	if(typeof jQuery.fn.fckEditorValidate == 'function') {
		jQuery.fn.fckEditorValidate({instanceName: 'detail'});
	}
	
});