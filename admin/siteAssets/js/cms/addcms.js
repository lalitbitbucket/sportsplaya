$(document).ready(function(){
	var validator = $("#frmaddcms").validate({
		errorElement:'div',
		rules: {
			title: {
				required: true
			},
			contentWordCount:{
				required: true
			}
		},
		messages: {
			title: {
				required: "Enter menu title"
			},
			contentWordCount:{
				required: "Enter menu content"
			}
			
		}
	});

	
});
