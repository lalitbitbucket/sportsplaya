<?php 
## include required files
/*******************************/
require_once '../model/seo.php';
/*******************************/

## Create Objects
/*******************************/
$seoObj = new Model_SEO();
/*******************************/

$seo_id = base64_decode($_GET['id']);
$search =($_REQUEST['search']);
$order_field =($_REQUEST['order_field']);
	$order_by =($_REQUEST['order_by']);

	$smarty->assign('order_field', $order_field);
	$smarty->assign('order_by', $order_by);
	
	$smarty->assign('search', $search);
if(isset($_POST['editseo_btn'])) {
	## apply PHP validation for required filed
	if(trim($_POST['seoTitle']) != '' && trim($_POST['seoKeywords']) != '' && trim($_POST['seoDescp']) != '') {
		$seo_Array = array();
		
	
		$seo_Array['seoTitle']   		= return_post_value($seoTitle);
		$seo_Array['seoKeywords']   	= return_post_value($seoKeywords);
		$seo_Array['seoDescp']   = return_post_value($seoDescp);
		$seo_id = return_post_value($_POST['seoID']);
		$seoObj->editSeoById($seo_Array,$seo_id);
	
		$_SESSION['msg'] = "<div class='success_msg'><span>SEO updated successfully</span></div>";
		header('location:'.SITE_URL.'/admin/home.php?q=seosettings&page='.$_GET['page'].'&order_field='.$_GET['order_field'].'&order_by='.$_GET['order_by'].'&search='.$_GET['search']);
		exit;
	} else { // php validation else
		$_SESSION['msg'] = '<div class="error_msg"><span>Please enter all required fields.</span></div>';
	}
	// main if closed
}

## Fetch seo category by id
if($_GET['id'] != ''&& $_GET['action'] == 'edit') {
	
	$smarty->assign('page', $_GET['page']);
	$seoArray = $seoObj->getSeoDetailsById($seo_id);	
	$smarty->assign('seoArray', $seoArray);
}

## Assign success or error msg to smarty variable and unset session variable
if(trim($_SESSION['msg']) != '') {
	$smarty->assign('msg', $_SESSION['msg']);
	unset($_SESSION['msg']);
}

## Set active class variable for left menu
$smarty->assign('activeclass', 'seo');
$smarty->assign('mainmenu', '6');
## Unset all the objects created which are on this page
unset($seoObj);

$smarty->display(TEMPLATEDIR_ADMIN. 'controller/seo/editseo.tpl');
unset($smarty);
?>