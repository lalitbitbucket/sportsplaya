<?php 
## include required files
/*******************************/
require_once '../model/seo.php';
require_once '../model/moduleuser.php';
## Paginaton class
require_once ("../model/common/classes/pagination_class.php");
/*******************************/

## Create Objects
/*******************************/
$seoObj = new Model_SEO();
$moduleObj=new Model_ModuleUser();
/*******************************/

if($_GET['q']=='seo')
{
	$moduelArray=$moduleObj->getModuleHelpText(14);
	$smarty->assign('moduelArray', $moduelArray);
}


if($_REQUEST['order_by']!='')
{
	$order_by =$_REQUEST['order_by'];
	$smarty->assign('orderBy',$order_by);
}

if($_REQUEST['order_field']!='')
{
	$order_field =$_REQUEST['order_field'];
	$smarty->assign('orderField',$order_field);
}

/*******************************/
##Fetch all cms pages from database
/******************************/
## Get search parameters in variables - 
if($_POST['search'] != '' && $_POST['search'] != 'Search') {
	$searchindex = return_post_value($_POST['search']);
} else if($_GET['search'] != '') {
	$searchindex = return_post_value($_GET['search']);
} else {
	$searchindex = '';
}

$searchindex=addslashes(trim($searchindex));
// Assign search variable to show in search textbox
$smarty->assign('search', stripslashes(stripslashes($searchindex)));

##
if(isset($_REQUEST['order_field']) && $_REQUEST['order_field'] != '') {
	$orderField = $_REQUEST['order_field'];
}
else {
	$orderField = 'seoKeywords';
}
$smarty->assign('orderField', $orderField);

if(isset($_REQUEST['order_by']) && $_REQUEST['order_by'] != '') {
	$orderBy = $_REQUEST['order_by'];
}
else {
	$orderBy = 'ASC';
}
$smarty->assign('orderBy', $orderBy);

##  --------- Pagination part first start --------------##
	if( isset( $_GET[ 'page' ] ) && $_GET[ 'page' ] != "" ) {
		$pageNum = $_GET[ 'page' ];
	} else {
		$pageNum = 1;
	} 

	$rowsPerPage = ($_GET['setlimit']) ? $_GET['setlimit'] : ROW_PER_PAGE;
	$smarty->assign('rowsPerPage',$rowsPerPage);
	//$pageName = "home.php?q=adminusers"; 
	$pageName = "home.php?q=seosettings&setlimit=$rowsPerPage&search=$search&order_by=$order_by&order_field=$order_field";


	
	## Count all the records
	$cmsArray = $seoObj->getAllSeos($searchindex,$orderField, $orderBy);
	$total_rows = count($cmsArray);
	$offset	= ($pageNum - 1) * $rowsPerPage;
	$smarty->assign('page', $pageNum);
	
	$arr			=	array();
		$interval		=	2;
		for($i = $interval; $i <= $total_rows; $i = $i + $interval)
		{
			$arr[]	=	$i;
		}
		
		$smarty->assign('arr', $arr);
		$cnt	=	count($userArray);
		$smarty->assign('cnt', $cnt);
##  --------- Pagination part first end --------------##


##  --------- Pagination part second start --------------##
	$newid = $pageNum * $rowsPerPage - $rowsPerPage + 1;
	$smarty->assign('newid', $newid);
	$seoArray = $seoObj->getAllSeos($searchindex,$orderField, $orderBy,$rowsPerPage,$offset);
	//echo "<pre>"; print_r($seoArray);exit;
	$smarty->assign('seoArray', $seoArray);
	
	$other_id = '';
	
	if($searchindex != '') {
		$other_id = "search=".$searchindex;
	} 
	
	if($order_field != '') {
		if($other_id == '') {
			$other_id = "order_field=".$order_field;
		} else {
			$other_id.= "&order_field=".$order_field;
		}
	}
	
	if($order_by != '') {
		if($other_id == '') {
			$other_id = "order_by=".$order_by;
		} else {
			$other_id.= "&order_by=".$order_by;
		}
	}
	
	if($total_rows > ROW_PER_PAGE) {
		$pg = new pagination();					// CREATING OBJECT OF THE PAGINATION CLASS
		$pgnation = $pg->pagination_admin( $total_rows , $rowsPerPage , $pageNum , $pageName , $other_id, $class);	
		$smarty-> assign("pagination",$pgnation);
	}
##  --------- Pagination part second end --------------##	

## Assign session message to smarty variable and unset session variable
if($_SESSION['msg'] != '') {
	$smarty->assign('msg', $_SESSION['msg']);	
	unset($_SESSION['msg']);
}

## Set active class variable for left menu  (main menu and sub menu)
$smarty->assign('activeclass', 'seo');
$smarty->assign('mainmenu', '6');

## Unset all the objects created which are on this page
unset($seoObj);
unset($moduleObj);

$smarty->display(TEMPLATEDIR_ADMIN. '/controller/seo/seo.tpl');
unset($smarty);
?>