<?php
## include required files
/*******************************/
require_once '../model/moduleuser.php';
require_once "../model/common/classes/pagination_class.php";
require_once '../model/users.php';

## Initializing Objects
/**************************/
$moduleObj = new Model_ModuleUser();
$userObj = new Model_Users();

/**************************/
if(isset($_REQUEST['search']) && !empty($_REQUEST['search']))
{
$search = $_REQUEST['search'];
$search = str_replace("'","",$search);
$search = ucfirst(stripslashes($search));
$smarty->assign('search',$search);
}
if(isset($_REQUEST['order_field']) && !empty($_REQUEST['order_field']))
{
$OrderField = $_REQUEST['order_field'];
$smarty->assign('order_field',$OrderField);
}
if(isset($_REQUEST['order_by']) && !empty($_REQUEST['order_by']))
{
$OrderBy = $_REQUEST['order_by'];
$smarty->assign('order_by',$OrderBy);
}

if(isset($_POST['search']) && $_POST['search']!= 'Search')
{
$searchindex = trim($_POST['search']);
}
elseif($_GET['search']!='')
{
$searchindex = trim($_GET['search']);	
}
else{
$searchindex = '';	
}
$searchindex = str_replace("'","",$searchindex);
$searchindex = ucfirst(stripslashes($searchindex));
$smarty->assign('search',$searchindex);

##
if(isset($_REQUEST['order_field']) && $_REQUEST['order_field'] != '') {
$orderField = $_REQUEST['order_field'];
}
else {
$orderField = 'fname';
}
$smarty->assign('orderField', $orderField);

if(isset($_REQUEST['order_by']) && $_REQUEST['order_by'] != '') {
$orderBy = $_REQUEST['order_by'];
}
else {
$orderBy = 'ASC';
}
$smarty->assign('orderBy', $orderBy);

/******************************************/
##  --------- Pagination part first start --------------##
/******************************************/
if(isset($_REQUEST['page']) && !empty($_REQUEST['page']))
{
$pageNum = $_REQUEST['page'];
}else{
$pageNum = 1;
}

$rowsperpage = ($_GET['setlimit'])? ($_GET['setlimit']): ROW_PER_PAGE;
$smarty->assign('rowsperpage',$rowsperpage);
$pageName = "home.php?q=onlineusers&setlimit=$rowsperpage&search=$search&order_field=$order_field&order_by=$order_by";
$siteuserArray = $userObj->getAllOnlineUsersInSite($search,$orderField,$orderBy);
//echo "<xmp>"; print_r($siteuserArray); echo "</xmp>"; 
$total_rows = count($siteuserArray);
$offset = ($pageNum - 1)*$rowsperpage;
$smarty->assign('page',$pageNum);
$arr			=	array();
$interval		=	2;
for($i = $interval; $i <= $total_rows; $i = $i + $interval)
{
	$arr[]			=	$i;
}

$smarty->assign('arr', $arr);
$cnt	=	count($siteuserArray);
$smarty->assign('cnt', $cnt);
##  --------- Pagination part first ends --------------##

##  --------- Pagination part second start --------------##
$newid = $pageNum * $rowsperpage - $rowsperpage + 1;
$smarty->assign('newid', $newid);
$siteuserArray = $userObj->getAllOnlineUsersInSite($searchindex,$orderField, $orderBy,$rowsperpage,$offset);

$i=0;
foreach($siteuserArray as $userinfo){
$userprofileinfo = $userObj->getUserProfileDetailByUserID($userinfo['userId'],$userinfo['userType']);
//echo "<xmp>"; print_r($userprofileinfo); echo "</xmp>"; 
$siteuserArray[$i]['name'] = $userprofileinfo['fname'].' '.$userprofileinfo['lname'];
$siteuserArray[$i]['id'] = $userprofileinfo['id'];
$i++;
}
$j=0;
$smarty->assign('siteuserArray', $siteuserArray);
//echo "<xmp>"; print_r($siteuserArray); echo "</xmp>"; 

if($searchindex != '') {
$other_id = "search=".$searchindex;
} else {
$other_id = '';
}	
if($total_rows > $rowsperpage) {
$pg = new pagination();					// CREATING OBJECT OF THE PAGINATION CLASS
$pgnation = $pg->pagination_admin( $total_rows , $rowsperpage , $pageNum , $pageName , $other_id, $class);	
$smarty-> assign("pagination",$pgnation);
}
##  --------- Pagination part second end --------------##	

/*******************************/
##Fetch all admin users from database
/******************************/
if($_GET['q']=='onlineusers')
{
$moduelArray=$moduleObj->getModuleHelpText(10);
$smarty->assign('moduelArray', $moduelArray);
}

/***************************/
##Assign Success Error messages
/***************************/
if($_SESSION['msg']!='')
{
$smarty->assign('msg',$_SESSION['msg']);
unset($_SESSION['msg']);
}

$smarty->assign('activeclass','onlineusers');
$smarty->assign('mainmenu', '3');

## Unset all objects which are on this page	
unset($moduleObj);
unset($userObj);
$smarty->display(TEMPLATEDIR_ADMIN. 'controller/onlineusers/manageonlineusers.tpl');
unset($smarty);
?>
