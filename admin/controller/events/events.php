<?php 
## include required files
require_once '../model/events.php';
require_once '../model/common/classes/pagination_class.php';

## Create Objects
$eventObj = new Model_EVENTS();

## Get search parameters in variables - 
if(isset($_POST['search']) != '' && $_POST['search'] != 'Event Name') {
	$searchindex = $_POST['search'];
} else if(isset($_GET['search']) &&  $_GET['search']!= '') {
	$searchindex = $_GET['search'];
} else {
	$searchindex = '';
}
// Assign search variable to show in search textbox
$searchindex=addslashes(trim($searchindex));
$smarty->assign('search', $searchindex);

## user type parameters for searching
if(isset($_REQUEST['user_type']) != '' && $_REQUEST['user_type'] != 'User Type'){
	$user_type = $_REQUEST['user_type'];
} else {
	$user_type = '';
}
$smarty->assign('user_type', $user_type);

if(isset($_GET['order_by']) && $_GET['order_by'] != '') {
	$orderBy = $_GET['order_by'];
}
else {
	$orderBy = 'DESC';
}
$smarty->assign('orderBy', $orderBy);


if(isset($_GET['orderField']) && $_GET['orderField'] != '') {
	$orderField = $_GET['orderField'];
}
else {
	$orderField = 'u.RegDate';
}
$smarty->assign('orderField', $orderField);



## Delete Events by id
if(isset($_GET['action']) == 'delete' && isset($_GET['id']) != '') {
	echo $event_id = base64_decode($_GET['id']);
	## Delete Event
	$eventObj->deleteEventById($event_id);
	$_SESSION['msg']="<div class='success_msg'><span>Event deleted successfully.</span></div>";
	header('location:'.SITE_URL.'/admin/home.php?q=events&order_by='.$orderBy.'&search='.$searchindex.'&page='.$_GET['page']);
	exit;
}

## --------- Pagination part first start --------------##
if( isset( $_GET[ 'page' ] ) && $_GET[ 'page' ] != "" ) {
	$pageNum = $_GET[ 'page' ];
} else {
	$pageNum = 1;
} 
$rowsPerPage = ($_GET['setlimit']) ? $_GET['setlimit'] : ROW_PER_PAGE;
$pageName = "home.php?q=events&order_by=".$orderBy."&search=".$searchindex.'&setlimit='.$rowsPerPage;
## Count all the records
$eventArray = $eventObj->getAllEventsList($searchindex, $orderField, $orderBy);
$total_rows = count($eventArray);
 $offset	= ($pageNum - 1) * $rowsPerPage;
$smarty->assign('page', $pageNum);
##--------- Pagination part first end --------------##


##--------- Pagination part second start --------------##
$newid = $pageNum * $rowsPerPage - $rowsPerPage + 1;
$smarty->assign('newid', $newid);
$eventArray= $eventObj->getAllEventsList($searchindex, $orderField, $orderBy, $rowsPerPage, $offset);
//"<xmp>"; print_r($eventArray); echo "</xmp>"; 
$k= 0;
while($k<count($eventArray))
{
	$userId = $eventArray[$k]['userId'];
	$userdetails=$userObj->getUserProfileDetailsByUserIdAtAdminSide($userId);
	//echo "<pre>";print_r($userdetails);exit;
	$eventArray[$k]['host'] = $userdetails['fname'].' '.$userdetails['lname'];
	$totalcomment=$eventObj->getallEventCommtsCountByeventId($eventArray[$k]['event_id']);
    $eventArray[$k]['totalcomm'] = $totalcomment;
	$k++;
}
$smarty->assign('eventArray', $eventArray);


//echo "<pre>";print_r($eventArray);exit;

if($searchindex != '') {
	$other_id = "search=".$searchindex."&order_by=".$orderBy;
} else {
	$other_id = '';
}

if($total_rows > $rowsPerPage) {
	$pg = new pagination();
	$pgnation = $pg->pagination_admin( $total_rows , $rowsPerPage , $pageNum , $pageName , $other_id, $class);	
	$smarty-> assign("pagination",$pgnation);
}

## Assign session message to smarty variable and unset session variable
if(isset($_SESSION['msg']) &&  $_SESSION['msg']!= '') {
	$smarty->assign('msg', $_SESSION['msg']);	
	unset($_SESSION['msg']);
}

## Unset all the objects created which are on this page
unset($eventObj);
$smarty->display(TEMPLATEDIR_ADMIN. 'controller/events/events.tpl');
unset($smarty);
?>
