<?php 
## include required files
require_once '../model/events.php';
require_once '../model/common/classes/pagination_class.php';


## Create Objects
$eventObj = new Model_EVENTS();
$commId=$_REQUEST['commentId'];
$commentId = base64_decode($_REQUEST['commentId']);
$smarty->assign('commentId', $commentId);

$userId=$_REQUEST['userId'];
$uID = base64_decode($_REQUEST['userId']);
$smarty->assign('uID', $uID);

//echo '<pre>'; print_r($uID); exit;

## check user's permission
/*if($modules_new[2]['permission_status'] !=1 || $modules_new[2]['submod_10'] !=1) {
	header('location: '.SITE_URL.'/admin/home.php');
	exit;
}*/


## Get search parameters in variables - 
if(isset($_POST['search']) != '' && $_POST['search'] != 'Comment') {
	$searchindex = $_POST['search'];
} else if(isset($_GET['search']) &&  $_GET['search']!= '') {
	$searchindex = $_GET['search'];
} else {
	$searchindex = '';
}
// Assign search variable to show in search textbox
$searchindex=addslashes(trim($searchindex));
$smarty->assign('search', $searchindex);


if(isset($_GET['order_by']) && $_GET['order_by'] != '') {
	$orderBy = $_GET['order_by'];
}
else {
	$orderBy = 'DESC';
}
$smarty->assign('orderBy', $orderBy);


if(isset($_GET['orderField']) && $_GET['orderField'] != '') {
	$orderField = $_GET['orderField'];
}
else {
	$orderField = 'date';
}
$smarty->assign('orderField', $orderField);



## Delete Events by id
if(isset($_GET['action']) == 'delete' && isset($_GET['id']) != '') {
 $subcomment_id = base64_decode($_GET['id']);
	## Delete Event
	$eventObj->deleteSubcommentByCommentId($subcomment_id);
	$_SESSION['msg']="<div class='success_msg'><span>Sub Comment deleted successfully.</span></div>";
	header('location:'.SITE_URL.'/admin/home.php?q=subcomments&userId='.$userId.'&commentId='.$commId.'&order_by='.$orderBy.'&search='.$searchindex.'&page='.$_GET['page']);
	exit;
}

## --------- Pagination part first start --------------##
if( isset( $_GET[ 'page' ] ) && $_GET[ 'page' ] != "" ) {
	$pageNum = $_GET[ 'page' ];
} else {
	$pageNum = 1;
} 
$rowsPerPage = ($_GET['setlimit']) ? $_GET['setlimit'] : ROW_PER_PAGE;

 $smarty->assign('rowsperpage',$rowsperpage);
 
$pageName = "home.php?q=subcomments&userId=".$userId."&commentId=".$commId."&order_by=".$orderBy."&search=".$searchindex."&setlimit=".$rowsPerPage;
## Count all the records
$eventArray = $eventObj->getAllSubCommentListPerComment($commentId,$searchindex, $orderField, $orderBy);
$total_rows = count($eventArray);
 $offset	= ($pageNum - 1) * $rowsPerPage;
$smarty->assign('page', $pageNum);
##--------- Pagination part first end --------------##


##--------- Pagination part second start --------------##
$newid = $pageNum * $rowsPerPage - $rowsPerPage + 1;
$smarty->assign('newid', $newid);
$eventArray= $eventObj->getAllSubCommentListPerComment($commentId,$searchindex, $orderField, $orderBy, $rowsPerPage, $offset);
//"<xmp>"; print_r($eventArray); echo "</xmp>"; exit;

$k= 0;
while($k<count($eventArray))
{
	$subcommentcount=$eventObj->getTotalSubComments($eventArray[$k]['id']);
	$eventArray[$k]['totalsubcomm'] = $subcommentcount;
   $likecount=$eventObj->getAllLikecountOnComment($eventArray[$k]['id']);
	$eventArray[$k]['totallike'] = $likecount;
	$k++;
}

$smarty->assign('eventArray', $eventArray);


//echo "<pre>";print_r($eventArray);exit;

if($searchindex != '') {
	$other_id = "search=".$searchindex."&order_by=".$orderBy;
} else {
	$other_id = '';
}

if($total_rows > $rowsPerPage) {
	$pg = new pagination();
	$pgnation = $pg->pagination_admin( $total_rows , $rowsPerPage , $pageNum , $pageName , $other_id, $class);	
	$smarty-> assign("pagination",$pgnation);
}
##  --------- Pagination part second end --------------##	





//echo "<xmp>"; print_r($logindetails); echo "</xmp>"; exit;
//echo "<xmp>"; print_r($eventArray); echo "</xmp>"; exit;
## Assign session message to smarty variable and unset session variable
if(isset($_SESSION['msg']) &&  $_SESSION['msg']!= '') {
	$smarty->assign('msg', $_SESSION['msg']);	
	unset($_SESSION['msg']);
}


## Unset all the objects created which are on this page
unset($eventObj);

$smarty->display(TEMPLATEDIR_ADMIN. 'controller/events/subcomments.tpl');
unset($smarty);
?>
