<?php 
## include required files
/*******************************/
require_once '../model/events.php';
//require_once '../model/common/image_functions.php';
/*******************************/
## Create Objects
/*******************************/
$eventsObj   = new Model_EVENTS();
//$imgupload = new Model_imgaeFunction ();
/*******************************/
if(isset($_POST['addevent_btn'])) 
{
	if(trim($_POST['event_name']) != '' && trim($_POST['eventWhere']) != '') 
	    {
		$adminEvent = array();
		$adminEvent['event_name'] =trim(return_post_value($_POST['event_name'])); 
		$adminEvent['event_address'] =trim(return_post_value($_POST['eventWhere']));
		$adminEvent['event_description'] =trim(return_post_value($_POST['event_desc']));
		$adminEvent['userId'] = '1';
		$adminEvent['event_startdate'] =trim(return_post_value($_POST['start_date'])); 
		$adminEvent['event_starttime'] =trim(return_post_value($_POST['start_time']));
		$adminEvent['evnet_enddate'] =trim(return_post_value($_POST['end_date'])); 
		$adminEvent['event_endtime'] =trim(return_post_value($_POST['end_time']));
		$adminEvent['updateddate'] =  date('Y-m-d');
		$eventsObj->addEevnts($adminEvent);
		$_SESSION['msg'] = "<div class='success_msg'><span>Event added successfully</span></div>";
		header('location:'.SITE_URL.'/admin/home.php?q=events');
		exit();
	   }
	else {
		$_SESSION['msg'] = '<div class="error_msg"><span>Please fill all the required fields </span></div>';
	     }		
}
	
## Assign success or error msg to smarty variable and unset session variable
if(trim($_SESSION['msg']) != '') {
	$smarty->assign('msg', $_SESSION['msg']);
	unset($_SESSION['msg']);
}

$smarty->display(TEMPLATEDIR_ADMIN. 'controller/events/addevents.tpl');
unset($smarty);
?>
