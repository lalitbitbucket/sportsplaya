<?php 
## include required files
/*******************************/
require_once '../model/events.php';

## Create Objects
/*******************************/
$eventsObj   = new Model_EVENTS();
/*******************************/

## Get search parameters in variables - 
if(isset($_POST['search']) != '' && $_POST['search'] != 'Search') {
	$searchindex = $_POST['search'];
} else if(isset($_GET['search']) &&  $_GET['search']!= '') {
	$searchindex = $_GET['search'];
} else {
	$searchindex = '';
}
// Assign search variable to show in search textbox
$searchindex=addslashes(trim($searchindex));
$smarty->assign('search', $searchindex);

if(isset($_GET['order_by']) && $_GET['order_by'] != '') {
	$orderBy = $_GET['order_by'];
}
else {
	$orderBy = 'ASC';
}
$smarty->assign('orderBy', $orderBy);

if(count($_POST['event_name']) > 0) 
{
  if(trim($_POST['event_name']) != '' && trim($_POST['eventWhere']) != '') 
    {
	## apply PHP validation for required filed
	$event_id  = $_POST['hidden_eventid'];
	$adminEvent = array();
	$adminEvent['event_name'] =trim(return_post_value($_POST['event_name'])); 
	$adminEvent['event_address'] =trim(return_post_value($_POST['eventWhere']));
	$adminEvent['event_description'] =trim(return_post_value($_POST['event_desc']));
	$adminEvent['event_startdate'] =trim(return_post_value($_POST['start_date'])); 
	$adminEvent['event_starttime'] =trim(return_post_value($_POST['start_time']));
	$adminEvent['evnet_enddate'] =trim(return_post_value($_POST['end_date'])); 
	$adminEvent['event_endtime'] =trim(return_post_value($_POST['end_time']));
	$eventsObj->updateEventDetails($adminEvent, $event_id);
	$_SESSION['msg'] = "<div class='success_msg'><span>Event updated successfully</span></div>";
	header('location:'.SITE_URL.'/admin/home.php?q=events&order_by='.$orderBy.'&search='.$searchindex.'&page='.$_GET['page']);
	exit;
     }
	else {
		 $_SESSION['msg'] = '<div class="error_msg"><span>Please fill all the required fields </span></div>';
	     }
}
		
## Fetch admin user details by id
if($_GET['id'] != '' && $_GET['action'] == 'edit') {
$event_id = base64_decode($_GET['id']);
$event_details = $eventsObj->getEventDetailsById($event_id);
$smarty->assign('event_details', $event_details);	
}

## Assign success or error msg to smarty variable and unset session variable
if(trim($_SESSION['msg']) != '') {
	$smarty->assign('msg', $_SESSION['msg']);
	unset($_SESSION['msg']);
}

## Unset all the objects created which are on this page
unset($eventsObj);
unset($imgupload);

$smarty->display(TEMPLATEDIR_ADMIN. 'controller/events/editEvents.tpl');
unset($smarty);
?>
