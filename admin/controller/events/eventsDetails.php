<?php 
## include required files
/*******************************/
require_once '../model/events.php';
/*******************************/
## Create Objects
/*******************************/
$eventsObj   = new Model_EVENTS();
/*******************************/
## Get search parameters in variables - 
if(isset($_POST['search']) != '' && $_POST['search'] != 'Search') {
	$searchindex = $_POST['search'];
} else if(isset($_GET['search']) &&  $_GET['search']!= '') {
	$searchindex = $_GET['search'];
} else {
	$searchindex = '';
}
// Assign search variable to show in search textbox
$searchindex=addslashes(trim($searchindex));
$smarty->assign('search', $searchindex);

## user type parameters for searching
if(isset($_REQUEST['user_type']) != '' && $_REQUEST['user_type'] != 'User Type'){
	$user_type = $_REQUEST['user_type'];
} else {
	$user_type = '';
}
$smarty->assign('user_type', $user_type);

if(isset($_GET['order_by']) && $_GET['order_by'] != '') {
	$orderBy = $_GET['order_by'];
}
else {
	$orderBy = 'ASC';
}
$smarty->assign('orderBy', $orderBy);

## Fetch admin user details by id
if($_GET['id'] != '') {
  $_GET['id'];
  $event_id = base64_decode($_GET['id']);	
  $event_details = $eventsObj->getEventDetailsById($event_id);
  //echo "<pre>";  print_r($event_details); exit();
  $smarty->assign('event_details',$event_details);
  }
## --------- Pagination part first start --------------##
if(isset($_GET['order_by']) && $_GET['order_by'] != '') {
    $orderBy = $_GET['order_by'];
}
else {
    $orderBy = 'DESC';
}
$smarty->assign('orderBy', $orderBy);

if(isset($_GET['orderField']) && $_GET['orderField'] != '') {
    $orderField = $_GET['orderField'];
}
else {
    $orderField = '';
}
$smarty->assign('orderField', $orderField);

## Unset all the objects created which are on this page
unset($eventobj);

$smarty->display(TEMPLATEDIR_ADMIN. 'controller/events/eventsDetails.tpl');
unset($smarty);
?>
