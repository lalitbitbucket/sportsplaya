<?php  
## include required files
/*******************************/
require_once '../model/videos.php';

## Create Objects
/*******************************/
$videoObj = new Model_videos();

## Get Current Date of week  - 
/*******************************/
$weeklyDate = getCurrentWeeklyDate();

$PlayaDate = $weeklyDate['end_date'];
$result = $videoObj->CheckAllreadySelected($PlayaDate);
if($result!=0)
{
	header('location:'.SITE_URL.'/admin/home.php?q=playaoftheday');
	exit;
}

$current = date('Y-m-d');
if($current!=$weeklyDate['end_date'])
{ 
	header('location:'.SITE_URL.'/admin/home.php?q=playaoftheday');
	exit;
}

$smarty->assign('weeklyDate',$weeklyDate);
## Insert New Playa of the day -
/*******************************/ 
if(isset($_POST) && $_POST['submit']=='Submit'){
		$playaId = $_POST['selectplayaId'];
		$videoArr = $videoObj->getvideosDetailsById($playaId);
		$getAlbumDtl = $videoObj->getAlbumDetailsByAlbumId($videoArr['albumId']);
		$data = array();
		if($videoArr['video_type']=='youtubeurl'){
			$data['video_url'] = $videoArr['vccode'];
		}
		if($videoArr['video_type']=='video'){
			$url = SITE_URL.'/dynamicAssets/videos/'.$videoArr['video'];
			$data['video_url'] = $url;
		}
		if($videoArr['video_type']=='vimeo'){
			$data['video_url'] = $videoArr['vcode'];
		}
		if($_POST['playatitle']!=''){
			$data['title'] = $_POST['playatitle'];
		}else{
			$data['title'] = $getAlbumDtl['title'];	
		}
		$data['videoid'] = $playaId;
		$data['video_type'] = $videoArr['video_type'];
		$data['userId'] = $videoArr['userprofile_id'];
		$data['total_clicks'] = $videoArr['total_clicks'];
		$data['from_date'] = $weeklyDate['start_date'];
		$data['to_date'] = $weeklyDate['end_date'];
		$videoObj->addplayaoftheWeek($data);
		header('location:'.SITE_URL.'/admin/home.php?q=playaoftheday');
		exit;
}

## Get All Video in current week -
/*******************************/ 
$albumArray = $videoObj->getAllVideoByWeekly($orderField = 'id', $orderBy = 'DESC', $limit='',$offset=0,$startdate=$weeklyDate['start_date'],$enddate=$weeklyDate['end_date']);
$total_record = count($albumArray);
$smarty->assign('total_record', $total_record);
$i=0;

foreach ($albumArray as $albumDetails) {
	if($albumDetails['video_type']=='vimeo'){
		$albumDetails['vcode'] = str_replace("\\","",$albumDetails['vcode']);
		$video_url = explode('"', $albumDetails['vcode']);
		$albumArray[$i]['image_url'] = SITE_URL.'/dynamicAssets/videos/thumbnail/vimeo_default.jpg';
		$albumArray[$i]['url'] = $video_url[1]; 
	}
	if($albumDetails['video_type']=='youtube'){
		$albumDetails['vcode'] = str_replace("\\","",$albumDetails['vcode']);
		preg_match('/src="([^"]+)"/', $albumDetails['vcode'], $match);
		$url = $match[1];
		$arr = explode("/",$url);
		$url= $arr[count($arr)-1];
		$albumArray[$i]['image_url'] ="https://i.ytimg.com/vi/".$url."/mqdefault.jpg";	
	}
	if($albumDetails['video_type']=='youtubeurl'){
		$albumDetails['vccode'] = str_replace("\\","",$albumDetails['vccode']);
		$arr = explode("=",$albumDetails['vccode']);
		$url= $arr[count($arr)-1];
		$albumArray[$i]['image_url'] ="https://i.ytimg.com/vi/".$url."/mqdefault.jpg";	
		$albumArray[$i]['url'] = $albumDetails['vccode'];
	}
	if($albumDetails['video_type']=='video'){
	   	$file = explode('.', $albumDetails['video']);
		$video_image = $file[0];
		$albumArray[$i]['image_url'] = SITE_URL.'/dynamicAssets/videos/thumbnail/'.$video_image.'.jpg';
		$albumArray[$i]['url'] = SITE_URL.'/dynamicAssets/videos/'.$albumDetails['video'];
	}
	$total_voting = $videoObj->getVotingPercentByVideoId($albumDetails['uId']);
	$getAlbumVideos = $videoObj->getAllAlbumVideosByAlbumId($albumDetails['albumId']);
	$getAlbumDtl = $videoObj->getAlbumDetailsByAlbumId($albumDetails['albumId']);
	
	$getComments = $videoObj->getCommentsByNomineesId('', $albumDetails['uId']);
	$albumArray[$i]['albumVideoCount']  = count($getAlbumVideos); 
	$albumArray[$i]['videodated'] = $albumDetails['added_date'];
	$albumArray[$i]['albumname']  	    = stripcslashes($getAlbumDtl['title']);
	$albumArray[$i]['imageCode']  	    = substr($getAlbumVideos[0]['vccode'],16); 
	$albumArray[$i]['fname']  	    = $albumDetails['fname'];
	$albumArray[$i]['lname']  	    = $albumDetails['lname'];
	$albumArray[$i]['uId']  	    = $albumDetails['uId'];
	$albumArray[$i]['sportsid']  	    = $albumDetails['sportsid'];
	$albumArray[$i]['str_rating']  	    = $total_voting['voteper'];
	$albumArray[$i]['total_comments']  	    = count($getComments);
	$i++;
}

/*print_r($albumArray);exit;*/
$smarty->assign('albumArray', $albumArray);

## Unset all the objects created which are on this page
unset($videoObj);

## Assign success or error msg to smarty variable and unset session variable
if(trim($_SESSION['msg']) != '') {
	$smarty->assign('msg', $_SESSION['msg']);
	unset($_SESSION['msg']);
}

$smarty->display(TEMPLATEDIR_ADMIN. 'controller/video/addplayaoftheweek.tpl');
unset($smarty);
?>