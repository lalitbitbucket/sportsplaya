<?php 
## include required files
/*******************************/
require_once '../model/videos.php';
/*******************************/
## Create Objects
/*******************************/
$videosObj = new Model_videos();
/*******************************/
$video_id = base64_decode($_GET['id']);

if(isset($_POST['addvideo_btn'])) 
{
	    	$array = array();
		$uploaddir = "../../dynamicAssets/videos/"; 
		$uploadedfile = basename($_FILES['flvvideo']['name']);

		if($uploadedfile!='' || $_POST['video_type'] == 'youtube' || $_POST['video_type'] == 'youtubeurl')
		{
		$uploadedfile1    = $uploaddir.$uploadedfile;
	
		$arrImageName = explode(".",$uploadedfile);
		$uploadedfilename=time().date('Ymd').".".$arrImageName[1];		
		copy($_FILES['flvvideo']['tmp_name'], "../../dynamicAssets/videos/".$uploadedfilename);

		$video_url = $uploadedfilename;
		if($uploadedfile){
		$array["video"]    		= $video_url;
		}
		$array['user_id'] 		= '1';
		$array['title'] 		= return_post_value($_POST['title']);		
		$array['description'] 		= return_post_value($_POST['description']);		
		$array['video_type'] 		= return_post_value($_POST['video_type']);		
		if($_POST['video_type'] == 'youtube'){
		$array['vcode'] 		= return_post_value(stripslashes($_POST['embeded_code']));		
		}else{
		$array['vcode'] 		= "";			
		}
		if($_POST['video_type'] == 'youtubeurl'){
		$array['vccode'] 		= return_post_value(stripslashes($_POST['youtubeurl']));		
		}else{
		$array['vccode'] 		= "";			
		}
					
		$array['added_date'] 	= date("Y-m-d");
			
		$videosObj->editUservideosByValueimageid($array,$video_id);
		$_SESSION['msg'] = "<div class='success_msg'><span>Video Edit successfully</span></div>";
		header('location:'.SITE_URL.'/admin/home.php?q=videolist');
		exit();
		}
		else
		{
		   $_SESSION['msg'] = "<div class='alert'><center>Please select video</center></div>";      
			
		}
}

if($_GET['id'] != '' && $_GET['action'] == 'edit') {
$video_id = base64_decode($_GET['id']);
$VideoResult=$videosObj->getvideosDetailsById($video_id);
 $smarty->assign("VideoResult", $VideoResult);
}

## Assign success or error msg to smarty variable and unset session variable
if(trim($_SESSION['msg']) != '') {
	$smarty->assign('msg', $_SESSION['msg']);
	unset($_SESSION['msg']);
}

$smarty->display(TEMPLATEDIR_ADMIN. 'controller/video/edit.tpl');
unset($smarty);
?>
