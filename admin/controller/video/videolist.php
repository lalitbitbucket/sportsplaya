<?php  
## include required files
/*******************************/
require_once '../model/videos.php';
## Paginaton class
require_once ("../model/common/classes/pagination_class.php");
/*******************************/
## Create Objects
/*******************************/
$videoObj = new Model_videos();
/*******************************/
## Get search parameters in variables - 
if($_REQUEST['search'] != '' && $_REQUEST['search'] != 'Search') {
	$searchindex = $_REQUEST['search'];
} else if($_REQUEST['search'] != '') {
	$searchindex = $_REQUEST['search'];
} else {
	$searchindex = '';
}
$searchindex=trim($searchindex);
// Assign search variable to show in search textbox
$smarty->assign('search', $searchindex);
if(isset($_GET['order_field']) && $_GET['order_field'] != '') {
	$orderField = $_GET['order_field'];
}
else {
	$orderField = 'title';
}
$smarty->assign('orderField', $orderField);

if(isset($_GET['order_by']) && $_GET['order_by'] != '') {
	$orderBy = $_GET['order_by'];
}
else {
	$orderBy = 'ASC';
}
$smarty->assign('orderBy', $orderBy);

//error_reporting(E_ALL);
if(isset($_POST['go']) != '' && isset($_POST['action']) != '') {
		$ids = implode(",", $_POST['checkall']);
		## check which action is selected
		$hidden_page = $_POST['hidden_pageno'];	
		if($_POST['action'] == 'active') {
			## Active selected records
			$videoObj->updateMultipleVideoStatus($ids, '2');
			$_SESSION['msg']="<div class='success_msg'><span>video status activated successfully.</span></div>";
		} else if($_POST['action'] == 'inactive') {
			## Inactive selected records
			$videoObj->updateMultipleVideoStatus($ids, '1');
			$_SESSION['msg']="<div class='success_msg'><span>video status deactivated successfully.</span></div>";
		} else {
			## Delete selected records
			$groupObj->deleteMultipleVideo($ids);
			$_SESSION['msg']="<div class='success_msg'><span>video deleted successfully</span></div>";
		}
		header('location:'.SITE_URL.'/admin/home.php?q=videolist&page='.$hidden_page.'&order_by='.$orderBy.'&search='.$searchindex.'&order_field='.$orderField);
			exit;
		## Delete code will be here if required
		
} // if close

## Active/Inactive/Delete thourgh URL
if(isset($_GET['action']) == 'status' && isset($_GET['status']) != '') {
	$id = base64_decode($_GET['id']);		
	## Update training page status 
	$array = array();
	$array['status'] = ($_GET['status']=='1'?2:1);
	$videoObj->updateVideoStatus($id, $array['status']);
	if($_GET['status']==1) {
		$_SESSION['msg']="<div class='success_msg'><span>video status activated successfully.</span></div>";
	} else {
		$_SESSION['msg']="<div class='success_msg'><span>video status deactivated successfully.</span></div>";
	}
	header('location:'.SITE_URL.'/admin/home.php?q=videolist&page='.$_GET['page'].'&order_by='.$orderBy.'&search='.$searchindex.'&order_field='.$orderField);
	exit;
}

if(isset($_GET['action']) == 'delete' && isset($_GET['id']) != '') {
	$id = base64_decode($_GET['id']);
	## Delete Training
	$videoObj->deleteVideo($id);
	$_SESSION['msg']="<div class='success_msg'><span>video deleted successfully</span></div>";
	header('location:'.SITE_URL.'/admin/home.php?q=videolist&page='.$_GET['page']);
	exit;
}

/*******************************/
/*if($_GET['q']=='group')
{
	$moduelArray=$moduleObj->getModuleHelpText(13);
	$smarty->assign('moduelArray', $moduelArray);
}*/

##Fetch all Training pages from database
##  --------- Pagination part first start --------------##
if( isset( $_REQUEST['page'] ) && $_REQUEST['page'] != "" ) {
	$pageNum = $_REQUEST['page'];
} else {
	$pageNum = 1;
} 
//echo $pageNum;exit;
$rowsPerPage = ($_GET['setlimit']) ? $_GET['setlimit'] : ROW_PER_PAGE;

$pageName = "home.php?q=videolist&search=".$searchindex."&page=".$_GET['page']."&setlimit=".$_GET['setlimit']; 
## Count all the records

$videoArray = $videoObj->getAllVideo($searchindex,$orderField, $orderBy);
$total_rows = count($videoArray);
$offset	= ($pageNum - 1) * $rowsPerPage;
$smarty->assign('page', $pageNum);
##  --------- Pagination part first end --------------##

##  --------- Pagination part second start --------------##
$newid = $pageNum * $rowsPerPage - $rowsPerPage + 1;
$smarty->assign('newid', $newid);
$videoArray = $videoObj->getAllVideo($searchindex,$orderField, $orderBy,$rowsPerPage,$offset);

$smarty->assign('videoArray', $videoArray);
//echo "<pre>"; print_r($videoArray);exit;
if($searchindex != '') {
	$other_id = "search=".$searchindex;
} else {
	$other_id = '';
}

if($total_rows > ROW_PER_PAGE) {
	$pg = new pagination();					// CREATING OBJECT OF THE PAGINATION CLASS
	$pgnation = $pg->pagination_admin( $total_rows , $rowsPerPage , $pageNum , $pageName , $other_id, $class);		
	$smarty-> assign("pagination",$pgnation);
}
##  --------- Pagination part second end --------------##	

## Assign session message to smarty variable and unset session variable
if($_SESSION['msg'] != '') {
	$smarty->assign('msg', $_SESSION['msg']);	
	unset($_SESSION['msg']);
}

## Set active class variable for left menu  (main menu and sub menu)
$smarty->assign('activeclass', 'videolist');
$smarty->assign('mainmenu', '5');

## Unset all the objects created which are on this page
unset($videoObj);
unset($moduleObj);

$smarty->display(TEMPLATEDIR_ADMIN. 'controller/video/videolist.tpl');
unset($smarty);
?>
