<?php 
## include required files
/*******************************/
require_once '../model/videos.php';
/*******************************/
## Create Objects
/*******************************/
$videosObj = new Model_videos();
/*******************************/
## Fetch admin user details by id
if($_GET['id'] != '') {
	$video_id = base64_decode($_GET['id']);
	$VideoResult=$videosObj->getvideosDetailsById($video_id);
      $smarty->assign("VideoResult", $VideoResult);
	}

## --------- Pagination part first start --------------##
## Unset all the objects created which are on this page
unset($videosObj);

$smarty->display(TEMPLATEDIR_ADMIN. 'controller/video/details.tpl');
unset($smarty);
?>
