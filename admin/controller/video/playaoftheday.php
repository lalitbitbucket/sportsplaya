<?php  
## include required files
/*******************************/
require_once '../model/videos.php';

## Create Objects
/*******************************/
$videoObj = new Model_videos();

## Get Week Start Date and End Date.
/*******************************/
$weeklyDate = getCurrentWeeklyDate();
$current = date('Y-m-d');
if($current == $weeklyDate['end_date'])
{ 
	$addPlaya =0; /*Select New Playa of Week*/
}
else
{
	$addPlaya =1; /*sorry today not select*/
}

$smarty->assign('addPlaya',$addPlaya);
$PlayaDate = $weeklyDate['end_date'];
$result = $videoObj->CheckAllreadySelected($PlayaDate);
if($result!=0)
{
	$selectalready = 1; /*already selected*/
}
else{
	$selectalready = 0; /*New Playa of Week*/
}
$smarty->assign('selectalready',$selectalready);
## Get All Selected Playa Videos - 
/*******************************/
$videoArray = $videoObj->getPlayaOfDayAllvideos();
$i=0;
foreach ($videoArray as $videoData) {
	if($videoData['video_type']=='vimeo'){
		$videoData['video_url'] = str_replace("\\","",$videoData['video_url']);
		$video_url = explode('"', $videoData['video_url']);
	 	$videoArray[$i]['image_url'] = SITE_URL.'/dynamicAssets/videos/thumbnail/vimeo_default.jpg';
	} 
	if($videoData['video_type']=='youtube'){
		$videoData['video_url'] = str_replace("\\","",$videoData['video_url']);
		preg_match('/src="([^"]+)"/', $videoData['video_url'], $match);
	    $url = $match[1];
		$arr = explode("/",$url);
		$url= $arr[count($arr)-1];
		$videoArray[$i]['image_url'] ="https://i.ytimg.com/vi/".$url."/mqdefault.jpg";	
	}
	if($videoData['video_type']=='youtubeurl'){
		$url = $videoData['video_url'];
	   	$videoData['video_url'] = str_replace("\\","",$videoData['video_url']);
	   	$regex_pattern = "/(youtube.com|youtu.be)\/(watch)?(\?v=)?(\S+)?/";
		if(preg_match($regex_pattern, $url, $match)){
			$videoArray[$i]['image_url'] ="https://i.ytimg.com/vi/".$match[4]."/mqdefault.jpg";	
		}
	}
	if($videoData['video_type']=='video'){
	   	$file = explode('.', $videoData['video_url']);
		$video_image = $file[0];
		$videoArray[$i]['image_url'] = SITE_URL.'/dynamicAssets/videos/thumbnail/'.$video_image.'.jpg';
	}
	$videoArray[$i]['album_title'] = stripcslashes($videoData['title']);
	$i++;
}

/*print_r($videoArray);exit;*/
$smarty->assign('videoArray', $videoArray);

## Unset all the objects created which are on this page
unset($videoObj);

$smarty->display(TEMPLATEDIR_ADMIN. 'controller/video/playaoftheday.tpl');
unset($smarty);
?>