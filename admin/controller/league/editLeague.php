<?php
## include required files
/*******************************/
require_once '../model/league.php';
require_once '../model/organizer.php';
require_once '../model/common/image_functions.php';
/*******************************/
## Create Objects
/*******************************/
$leagueObj = new Model_League();
/*******************************/
//error_reporting(E_ALL);
##get training id from edit page as in the hidden form
$leagueid = base64_decode($_GET['id']);

## Get search parameters in variables - 
if($_POST['search'] != '' && $_POST['search'] != 'Search') {
	$searchindex = ucfirst($_POST['search']);
} else if($_GET['search'] != '') {
	$searchindex = $_GET['search'];
} else {
	$searchindex = '';
}
$searchindex=trim($searchindex);
// Assign search variable to show in search textbox
$smarty->assign('search', $searchindex);
if(isset($_GET['order_field']) && $_GET['order_field'] != '') {
	$orderField = $_GET['order_field'];
}
else {
	$orderField = 'leagueName';
}
$smarty->assign('orderField', $orderField);

if(isset($_GET['order_by']) && $_GET['order_by'] != '') {
	$orderBy = $_GET['order_by'];
}
else {
	$orderBy = 'ASC';
}
$smarty->assign('orderBy', $orderBy);


if(isset($_POST) && !empty($_POST)) {
	## apply PHP validation for required filed
if(trim($_POST['leagueName']) != '' && trim($_POST['description']) != '') {
		//echo "<pre/>";print_r($_POST['trainingname']); exit;	
		$leagueeditArray = array();
		if($_FILES['image']['name']){
			## check valid image extension
			$fileExt = file_extension($_FILES['image']['name']); 
			$validExt = valid_extension($fileExt);
			## upload file
			$fileName      = time()."_".$_FILES['image']['name'];
			$temp=$_FILES["image"]["tmp_name"];	
			//move_uploaded_file($_FILES["bookImage"]["tmp_name"],"../dynamicAssets/books/".$fileName);					
			$imageFolder='../dynamicAssets/league/';
			$imageName= $fileName;		
			
			
			## creating thumb images

			$thumbFolder1 ="../dynamicAssets/league/53x53/";						
			$height1 = 53;
			$width1= 53;
			$file1=uploadImageAndCreateThumb($temp,$imageName,$imageFolder,$thumbFolder1,$height1,$width1);

			$thumbFolder2 ="../dynamicAssets/league/138x138/";						
			$height2 = 138;
			$width2= 138;
			$file2=uploadImageAndCreateThumb($temp,$imageName,$imageFolder,$thumbFolder2,$height2,$width2);

			$thumbFolder3 ="../dynamicAssets/league/80x80/";						
			$height3 = 80;
			$width3= 80;
			$file3=uploadImageAndCreateThumb($temp,$imageName,$imageFolder,$thumbFolder3,$height3,$width3);

			$leagueAddArray['logo'] = $fileName;	

			/*$leagueDetails = $leagueObj->getLeagueImageByLeagueId($_POST['leagueID']);
			unlink('../dynamicAssets/league/'.$leagueDetails['logo']);
			unlink('../dynamicAssets/league/53x53/'.$leagueDetails['logo']);
			unlink('../dynamicAssets/league/138x138/'.$leagueDetails['logo']);
			unlink('../dynamicAssets/league/80x80/'.$leagueDetails['logo']);*/				
			
		}
		//$leagueAddArray['orgID'] 		= $_POST['orgID'];		
		$leagueAddArray['leagueName'] 		= return_post_value($_POST['leagueName']);
		$leagueAddArray['description'] 		 = return_post_value($_POST['description']);
		$leagueAddArray['startDate'] 		 = return_post_value($_POST['startDate']);
		$leagueAddArray['endDate'] 		 = return_post_value($_POST['endDate']);
		$leagueid = $_POST['leagueID'];
		//echo "<pre>"; print_r($trainingid);exit;
		$leagueObj->editLeagueValueById($leagueAddArray, $leagueid);
		
		$_SESSION['msg'] = "<div class='success_msg'><span>League edited successfully</span></div>";
		header('location:'.SITE_URL.'/admin/home.php?q=leagueList&page='.$_POST['page'].'&order_by='.$orderBy.'&search='.$searchindex.'&order_field='.$orderField);
		exit();
	} else {// php validation else
		$_SESSION['msg'] = '<div class="error_msg"><span>Please fill all the required fields </span></div>';
	}
	// main if closed
}

if($_GET['id'] != ''&& $_GET['action'] == 'edit') {
	$leagueID = base64_decode($_GET['id']);
	$leagueDetArray = $leagueObj->getLeagueDetailsById($leagueID);
	$smarty->assign('leagueDetArray', $leagueDetArray);
}

## Assign success or error msg to smarty variable and unset session variable
if(trim($_SESSION['msg']) != '') {
	$smarty->assign('msg', $_SESSION['msg']);
	unset($_SESSION['msg']);
}

## Set active class variable for left menu
$smarty->assign('activeclass', 'league');
$smarty->assign('mainmenu', '5');

## Unset all the objects created which are on this page
unset($catObj);
unset($leagueObj);

$smarty->display(TEMPLATEDIR_ADMIN. 'controller/league/editLeague.tpl');
unset($smarty);
?>
