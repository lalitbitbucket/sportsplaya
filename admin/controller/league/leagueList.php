<?php  
## include required files
/*******************************/
require_once '../model/league.php';
require_once '../model/organizer.php';
require_once '../model/moduleuser.php';
## Paginaton class
require_once ("../model/common/classes/pagination_class.php");
/*******************************/

## Create Objects
/*******************************/
$leagueObj = new Model_League();
$organizerObj = new Model_Organizer();
$moduleObj=new Model_ModuleUser();
/*******************************/

## Active, Inactive selected records
/*******************************/
/*## check user's permission
if($modules_new[5]['permission_status'] !=1 || $modules_new[5]['submod_28'] !=1) {
	header('location: '.SITE_URL.'/admin/home.php');
	exit;
}*/

## Get search parameters in variables - 
if($_REQUEST['search'] != '' && $_REQUEST['search'] != 'Search') {
	$searchindex = $_REQUEST['search'];
} else if($_REQUEST['search'] != '') {
	$searchindex = $_REQUEST['search'];
} else {
	$searchindex = '';
}
$searchindex=trim($searchindex);
// Assign search variable to show in search textbox
$smarty->assign('search', $searchindex);
if(isset($_GET['order_field']) && $_GET['order_field'] != '') {
	$orderField = $_GET['order_field'];
}
else {
	$orderField = 'leagueName';
}
$smarty->assign('orderField', $orderField);

if(isset($_GET['order_by']) && $_GET['order_by'] != '') {
	$orderBy = $_GET['order_by'];
}
else {
	$orderBy = 'ASC';
}
$smarty->assign('orderBy', $orderBy);


//error_reporting(E_ALL);
if(isset($_POST['go']) != '' && isset($_POST['action']) != '') {
		$ids = implode(",", $_POST['checkall']);
		## check which action is selected
		$hidden_page = $_POST['hidden_pageno'];	
		if($_POST['action'] == 'active') {			
			## Active selected records
			$leagueObj->updateMultipleLeagueStatus($ids, '2');
			$_SESSION['msg']="<div class='success_msg'><span>League status activated successfully.</span></div>";
		} else if($_POST['action'] == 'inactive') {
			## Inactive selected records
			$leagueObj->updateMultipleLeagueStatus($ids, '1');
			$_SESSION['msg']="<div class='success_msg'><span>League status deactivated successfully.</span></div>";
		} else {
			## Delete selected records
			for($i=0;$i<count($_POST['checkall']);$i++){
				$leagueDetails = $leagueObj->getLeagueImageByLeagueId($_POST['checkall'][$i]);
				unlink('../dynamicAssets/league/'.$leagueDetails['logo']);
				unlink('../dynamicAssets/league/53x53/'.$leagueDetails['logo']);
				unlink('../dynamicAssets/league/138x138/'.$leagueDetails['logo']);
				unlink('../dynamicAssets/league/80x80/'.$leagueDetails['logo']);
			}
			$leagueObj->deleteMultipleLeague($ids);
			$_SESSION['msg']="<div class='success_msg'><span>League deleted successfully</span></div>";
		}
		header('location:'.SITE_URL.'/admin/home.php?q=leagueList&page='.$hidden_page.'&order_by='.$orderBy.'&search='.$searchindex.'&order_field='.$orderField);
		exit();
		## Delete code will be here if required
		
} // if close

## Active/Inactive/Delete thourgh URL
if(isset($_GET['action']) == 'status' && isset($_GET['status']) != '') {
	$id = base64_decode($_GET['id']);		
	## Update training page status 
	$array = array();
	$array['status'] = ($_GET['status']=='1'?2:1);
	$leagueObj->updateLeagueStatus($id, $array['status']);
	if($_GET['status']==1) {
		$_SESSION['msg']="<div class='success_msg'><span>League status activated successfully.</span></div>";
	} else {
		$_SESSION['msg']="<div class='success_msg'><span>League status deactivated successfully.</span></div>";
	}
	header('location:'.SITE_URL.'/admin/home.php?q=leagueList&page='.$_GET['page'].'&order_by='.$orderBy.'&search='.$searchindex.'&order_field='.$orderField);
	exit;
}

if(isset($_GET['action']) == 'delete' && isset($_GET['id']) != '') {
	$id = base64_decode($_GET['id']);
	## Delete Training
	$leagueDetails = $leagueObj->getLeagueImageByLeagueId($id);
	unlink('../dynamicAssets/league/'.$leagueDetails['logo']);
	unlink('../dynamicAssets/league/53x53/'.$leagueDetails['logo']);
	unlink('../dynamicAssets/league/138x138/'.$leagueDetails['logo']);
	unlink('../dynamicAssets/league/80x80/'.$leagueDetails['logo']);
	$leagueObj->deleteLeague($id);
	$_SESSION['msg']="<div class='success_msg'><span>League deleted successfully</span></div>";
	header('location:'.SITE_URL.'/admin/home.php?q=leagueList&page='.$_GET['page']);
	exit;
}

/*******************************/
/*if($_GET['q']=='league')
{
	$moduelArray=$moduleObj->getModuleHelpText(13);
	$smarty->assign('moduelArray', $moduelArray);
}*/

//echo "<pre>"; print_r($moduelArray);exit;
##Fetch all training pages from database


##Fetch all Training pages from database

##  --------- Pagination part first start --------------##
	if( isset( $_REQUEST['page'] ) && $_REQUEST['page'] != "" ) {
		$pageNum = $_REQUEST['page'];
	} else {
		$pageNum = 1;
	} 
	//echo $pageNum;exit;
	$rowsPerPage = ($_GET['setlimit']) ? $_GET['setlimit'] : ROW_PER_PAGE;
	
	$pageName = "home.php?q=leagueList&order_by=".$orderBy."&search=".$searchindex."&order_field=".$orderField."&page=".$_GET['page']."&setlimit=".$_GET['setlimit']; 
	## Count all the records
	$leagueArray = $leagueObj->getAllLeague($searchindex,$orderField, $orderBy);
	$total_rows = count($leagueArray);
	$offset	= ($pageNum - 1) * $rowsPerPage;
	$smarty->assign('page', $pageNum);
##  --------- Pagination part first end --------------##

##  --------- Pagination part second start --------------##
	$newid = $pageNum * $rowsPerPage - $rowsPerPage + 1;
	$smarty->assign('newid', $newid);
	$leagueArray = $leagueObj->getAllLeague($searchindex,$orderField, $orderBy,$rowsPerPage,$offset);
	$i=0;
	foreach($leagueArray as $leagueArr){
		## league members count
		$orgDetails = $organizerObj->getorganizerDetailsById($leagueArr['orgID']);	
		$leagueArray[$i]['orgName'] = $orgDetails['orgName'];
		
		$i++;
	}
	$smarty->assign('leagueArray', $leagueArray);
	
	if($searchindex != '') {
		$other_id = "search=".$searchindex;
	} else {
		$other_id = '';
	}
	
	if($total_rows > ROW_PER_PAGE) {
		$pg = new pagination();					// CREATING OBJECT OF THE PAGINATION CLASS
		$pgnation = $pg->pagination_admin( $total_rows , $rowsPerPage , $pageNum , $pageName , $other_id, $class);		
		$smarty-> assign("pagination",$pgnation);
	}
##  --------- Pagination part second end --------------##	

## Assign session message to smarty variable and unset session variable
if($_SESSION['msg'] != '') {
	$smarty->assign('msg', $_SESSION['msg']);	
	unset($_SESSION['msg']);
}

## Set active class variable for left menu  (main menu and sub menu)
$smarty->assign('activeclass', 'league');
$smarty->assign('mainmenu', '5');

## Unset all the objects created which are on this page
unset($leagueObj);
unset($moduleObj);

$smarty->display(TEMPLATEDIR_ADMIN. 'controller/league/leagueList.tpl');
unset($smarty);
?>
