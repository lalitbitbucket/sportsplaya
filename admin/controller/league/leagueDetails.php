<?php
require_once '../model/league.php';
require_once '../model/organizer.php';
## Initializing Objects
/**************************/
$leagueObj = new Model_League();
$organizerObj = new Model_Organizer();
/**************************/
if(isset($_REQUEST['id']) && $_REQUEST['id']!='')
{
	$id = base64_decode($_REQUEST['id']);
	$leagueArray = $leagueObj->getLeagueDetailsById($id);
	$orgDetails = $organizerObj->getorganizerDetailsById($leagueArray['orgID']);
	$leagueArray['orgName'] = $orgDetails['orgName'];
	$smarty->assign('leagueArray',$leagueArray);
	//echo "<xmp>"; print_r($trainingArray); echo "</xmp>";
}


	if($_SESSION['msg']!='')
	{
		$smarty->assign('msg',$_SESSION['msg']);
		unset($_SESSION['msg']);
	}
	
	$smarty->assign('activeclass','league');
	$smarty->assign('mainmenu', '3');
	
## Unset all objects which are on this page	
unset($leagueObj);
unset($organizerObj);
$smarty->display(TEMPLATEDIR_ADMIN. 'controller/league/leagueDetails.tpl');
unset($smarty);
?>
