<?php  
## include required files
/*******************************/
require_once '../model/contactus.php';  
require_once '../model/users.php';
require_once '../model/email.php';
## Paginaton class
require_once "../model/common/classes/pagination_class.php";
require_once '../includes/phpmailer/class.phpmailer.php';
/*******************************/
## Create Objects
/*******************************/
$emailObj = new Model_EMail();
$Obj =  new Model_Contactus();		
$mail = new PHPMailer(true);
$userObj = new Model_Users();
/*******************************/
$con_id = base64_decode($_GET['id']);
$action	=	'1';
$Obj->updateEnquiry($con_id,$action);	

// main table
$contact_array=$Obj->getEnquiry($con_id);
$smarty->assign("contact_array",$contact_array);

$edit_array = array();
$edit_array['readflag']	= 'read';
$Obj->editEnquiry($edit_array,$con_id);
	
if(isset($_POST['Send']) && $_POST['Send']=='Send')
{	
	if($_POST['subject']!='' && $_POST['message']!='' && $_POST['sign']!='')
	{
	$contact_array=$Obj->getEnquiry(base64_decode($_GET['id']));
	
	$replyArray=array();
	$replyArray['replySub']			=	return_post_value($_POST['subject']);	
	$replyArray['enqID'] 			= 	base64_decode($_GET['id']);
	$replyArray['replyContent']		=	return_post_value($_POST['message']);
	$rep_id	= $Obj->addEnquiryReply($replyArray); 	
	$contactArray	        		= array();
	$contactArray['status']			= 'answered';
	$Obj->editEnquiry($contactArray,$con_id);

	## Get Admin details 
	$id=1;
	$admindetails = $userObj->getProfileDetailsByUserId($id);	
	################################### Mail Template Start ############################
	$emailArray = $emailObj->getEmailById(18);
	$message=$emailArray['emailContent'];

	//echo"<pre>"; print_r($_POST); echo"</pre>"; exit;

	## Create message
	$to=return_post_value($contact_array['enqEmail']);	
	$toname = return_post_value($contact_array['enqName']);		
	$from = $admindetails['email'];
	$fromname = $admindetails['fname']." ".$admindetails['lname'];
	$subject = return_post_value($_POST['subject']);
	$messages = nl2br($_POST['message']);					
	$messages  .= "<br/><br/><br/>";					
	$messages  .= nl2br($_POST['sign']);
	$message = str_replace('[SITENAME]',SITENAME , $message);
	$message = str_replace('[SITELINK]',SITE_URL , $message);
	$message = str_replace('[LOGO]','<img src="'.SITE_URL.'/siteAssets/images/logo.png"/>', $message);
	$message=str_replace('[NAME]',  ucfirst(return_post_value($contact_array['enqName'])), $message);
	$message=str_replace('[MESSAGE]',$messages , $message);
	
	$smtpArray = $emailObj->getActiveSMTPDetails();
	// print_r($message); exit;
	//echo $userProfileDetails['password']; exit;
	$auth  = $smtpArray['isAuth'];
	// echo $auth;exit;
			if($auth=='y')
			{     $auth = 'true';  
			
			}
			else
			{     $auth = 'false';    
			
			}
				if($auth=='true')
				{  
				$mail->IsSMTP(); // telling the class to use SMTP
				$mail->Mailer = "smtp";
				$mail->SMTPAuth   = true;                  // enable SMTP authentication
				$mail->SMTPSecure = $smtpArray['secure'];                 // sets the prefix to the servier
				$mail->Host       = $smtpArray['smtpHost'];      // sets GMAIL as the SMTP server
				$mail->Port       = $smtpArray['smtpPort'];                  // set the SMTP port for the GMAIL server
				$mail->Username   = $smtpArray['smtpUsername'];  // GMAIL username
				$mail->Password   = $smtpArray['smtpPassword']; 
				}
	       
	      			# get email back up    
	      			$backemailArray = array();
	      			$backemailArray['sendto'] = $to;
	     			$backemailArray['subject'] = $subject;
	      			$backemailArray['content'] = $message;
	      			$backemailArray['date'] = date("Y-m-d H:i:s");
	     			// print_r($backemailArray);exit;

	       			/*echo $subject."<br />";
				echo $from."<br />";
			    	echo $fromname."<br />";
				echo $to."<br />";
				echo $toname."<br />";
				echo "<pre>"; print_r($message);  exit();*/
	     
	      		try {
				  $mail->AddAddress($to, $toname);
				  $mail->SetFrom($from, $fromname);
				  $mail->Subject = $subject;
				  $mail->Body = $message; 
			          $mail->Send(); 
	      			  $backemailArray['status'] ='2';   
				  //echo"Hi"; exit;
	      			  $_SESSION['msg'] = "<div class='alert-success alert'>Your query has been posted successfully. We will get back to you shortly!</div>";
			} 
			catch (phpmailerException $e) {
			   $_SESSION['msg']= "<div class='warning'>".$e->errorMessage()."</div>"; //Pretty error messages from PHPMailer
	    		   $backemailArray['status'] ='1';
			} catch (Exception $e) {
			  $_SESSION['msg']=  "<div class='warning'>".$e->getMessage()."</div>"; //Boring error messages from anything else!
	   		  $backemailArray['status'] ='1';
			}				
			################################### Mail Template End   ############################
	 		$emailObj->getEmailBackup($backemailArray);				
			$_SESSION['msg']="<div class='success_msg'><span>Reply sent successfully</span></div>";			
			header("location:".SITE_URL."/admin/home.php?q=enquiry&id=".base64_encode($con_id));
		}
		else{
			$_SESSION['msg']='<div align="center" class="error_msg"><span>All fields are required.</span></div>';
		    }
}
	
## Assign session message to smarty variable and unset session variable
if($_SESSION['msg'] != '') {
	$smarty->assign('msg', $_SESSION['msg']);	
	unset($_SESSION['msg']);
}

## Set active class variable for left menu  (main menu and sub menu)
$smarty->assign('activeclass', 'enquiry');
$smarty->assign('mainmenu', '4');

## Unset all the objects created which are on this page
unset($newsObj);
unset($moduleObj);

$smarty->display(TEMPLATEDIR_ADMIN. 'controller/enquiry/EnquiryDetails.tpl');
unset($smarty);
?>
