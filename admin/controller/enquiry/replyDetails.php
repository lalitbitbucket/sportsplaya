<?php 
## include required files
/*******************************/
require_once '../model/users.php';
require_once '../model/contactus.php'; 
require_once '../model/email.php'; 
/*******************************/
## Create Objects
/*******************************/
$Obj =  new Model_Contactus();	
$mailObj=new Model_Email();
/*******************************/
if($_GET['id']!='') {
	$rep_id = base64_decode($_GET['id']);
	$smarty->assign('page', $_GET['page']);
	$replyArray=$Obj->getReplydetails($rep_id);
	$smarty->assign("replyArray",$replyArray);
	//echo '<pre>';print_r($replyArray);
	$contactArray=$Obj->getEnquiry($replyArray['enqID']);
	$smarty->assign("contactArray",$contactArray);
}

if($_SESSION['msg']){
$smarty->assign("msg", $_SESSION['msg']);
unset($_SESSION['msg']);
}

## Set active class variable for left menu  (main menu and sub menu)
$smarty->assign('activeclass', 'enquiry');
$smarty->assign('mainmenu', '2');

## Unset all the objects created which are on this page
unset($Obj);
unset($moduleObj);

$smarty->display(TEMPLATEDIR_ADMIN. 'controller/enquiry/replyDetails.tpl');
unset($smarty);
?>
