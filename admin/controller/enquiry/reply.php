<?php 
## include required files
/*******************************/
require_once '../model/users.php';
require_once '../model/contactus.php'; 
require_once '../model/email.php'; 
require_once '../model/moduleuser.php';
require_once '../model/common/classes/pagination_class.php';
/*******************************/
## Create Objects
/*******************************/
$Obj =  new Model_Contactus();	
$mailObj=new Model_Email();
$moduleObj=new Model_ModuleUser();
/*******************************/
$con_id = base64_decode($_GET['id']);
if($_GET['id']!='') {
	 $con_id = base64_decode($_GET['id']);
	$smarty->assign('page', $_GET['page']);
	$contact_array=$Obj->getEnquiry($con_id);
	$smarty->assign("contact_array",$contact_array);
}

$contact_id=$con_id;
$smarty->assign("contactId",$contact_id);

if(isset($_GET['action']) == 'delete' && isset($_GET['repid']) != '') {
	$repid = base64_decode($_GET['repid']);
	## Delete Reply 	
	$Obj->deleteReplyMessages($repid);
	$_SESSION['msg']="<div class='success_msg'><span>Reply deleted successfully</span></div>";
	header('location:'.SITE_URL.'/admin/home.php?q=reply&id='.$_GET['id'].'page='.$_GET['page']);
	exit;
}
	
## Delete selected records
/*******************************/
if(isset($_POST['go']) != '' && isset($_POST['action']) != '') {
		$ids = implode(",", $_POST['checkall']);
		## check which action is selected
		$hidden_page = $_POST['hidden_pageno'];			
		## Delete selected records
		$Obj->deleteReplies($ids);
		$_SESSION['msg']="<div class='success_msg'><span>Enquiry deleted successfully</span></div>";		
		header('location:'.SITE_URL.'/admin/home.php?q=reply&id='.$_GET['id'].'&page='.$hidden_page);
		exit;
		## Delete code will be here if required
		
} // if close		
		
if($_POST['search'] != '' && $_POST['search'] != 'Search') {
		$search = $_POST['search'];
		} else if($_GET['search'] != '') {
		 $search = $_GET['search'];
		} else {
		 $search = '';
		}
$smarty->assign('search', $search);		
$replyArray=$Obj->AllReplyMessagesById($con_id,$search);
//echo '<pre>';print_r($replyArray);

##  --------- Pagination part first start --------------##
if( isset( $_GET[ 'page' ] ) && $_GET[ 'page' ] != "" ) {
$pageNum = $_GET[ 'page' ];
} else {
$pageNum = 1;
} 
$rowsPerPage = ($_GET['setlimit']) ? $_GET['setlimit'] : ROW_PER_PAGE;	
$pageName = "home.php?q=reply"; 
## Count all the records
$replyArray=$Obj->AllReplyMessagesById($con_id,$search);
$total_rows = count($contactArray);
$offset	= ($pageNum - 1) * $rowsPerPage;
$smarty->assign('page', $pageNum);
##  --------- Pagination part first end --------------##

##  --------- Pagination part second start --------------##
$newid = $pageNum * $rowsPerPage - $rowsPerPage + 1;
$smarty->assign('newid', $newid);
$replyArray=$Obj->AllReplyMessagesById($con_id,$search,$rowsPerPage, $offset);

if($search!='' && $search!='Search')
{
$other_id="search=".$search;
}
else
{
$other_id="";
}

if($total_rows > ROW_PER_PAGE) {
$pg = new pagination();					// CREATING OBJECT OF THE PAGINATION CLASS
$pgnation = $pg->pagination_admin( $total_rows , $rowsPerPage , $pageNum , $pageName , $other_id, $class);	
$smarty-> assign("pagination",$pgnation);
}
##  --------- Pagination part second end --------------##	
$smarty->assign("replyArray",$replyArray);
$smarty->assign("contact_array",$contact_array);

if($_SESSION['msg']){
$smarty->assign("msg", $_SESSION['msg']);
unset($_SESSION['msg']);
}

## Set active class variable for left menu  (main menu and sub menu)
$smarty->assign('activeclass', 'enquiry');
$smarty->assign('mainmenu', '4');

## Unset all the objects created which are on this page
unset($Obj);
unset($moduleObj);
$smarty->display(TEMPLATEDIR_ADMIN. 'controller/enquiry/reply.tpl');
unset($smarty);
?>
