<?php 
// this file and function checked admin session value stored or not
require_once '../model/users.php';
require_once '../model/common/classes/pagination_class.php';
require_once '../model/contactus.php';  
/***************************************************************/

$Obj =  new Model_Contactus();	

if(isset($_GET['order_field']) && $_GET['order_field'] != '') {
	$orderField = $_GET['order_field'];
}
else {
	$orderField = 'enqDate';
}
$smarty->assign('orderField', $orderField);

if(isset($_GET['order_by']) && $_GET['order_by'] != '') {
	$orderBy = $_GET['order_by'];
}
else {
	$orderBy = 'DESC';
}
$smarty->assign('orderBy', $orderBy);
/*******************************/

if(isset($_GET['action']) == 'delete' && isset($_GET['id']) != '') {
	$id = base64_decode($_GET['id']);
	## Delete Enquiry 
	$Obj->deleteEnquiry($id);
	$_SESSION['msg']="<div class='success_msg'><span>Enquiry deleted successfully</span></div>";
	header('location:'.SITE_URL.'/admin/home.php?q=enquiry&page='.$_GET['page']);
	exit;
}
 
 ## Read, Unread selected records
/*******************************/
if(isset($_POST['go']) != '' && isset($_POST['action']) != '') {
		$ids = implode(",", $_POST['checkall']);
		## check which action is selected
		$hidden_page = $_POST['hidden_pageno'];	
		if($_POST['action'] == 'read') {
			## Read Status selected records
			$Obj->updateEnquiryStatus($ids, '1');
			$_SESSION['msg']="<div class='success_msg'><span>Enquiry read status updated successfully</span></div>";
		} else if($_POST['action'] == 'unread') {
			## Unread Status selected records
			$Obj->updateEnquiryStatus($ids, '0');
			$_SESSION['msg']="<div class='success_msg'><span>Enquiry unread status updated successfully</span></div>";
		} else {
			## Delete selected records
			$Obj->deleteEnquirys($ids);
			$_SESSION['msg']="<div class='success_msg'><span>Enquiry deleted successfully</span></div>";
		}
		header('location:'.SITE_URL.'/admin/home.php?q=enquiry&page='.$hidden_page);
			exit;
		## Delete code will be here if required
		
} 

if($_POST['search'] != '' && $_POST['search'] != 'Search') {
$search = return_fetched_value($_POST['search']);
} else if($_GET['search'] != '') {
$search = return_fetched_value($_GET['search']);
} else {
$search = '';
}
$smarty->assign('search', $search);
$contactArray= $Obj->listRecord(return_post_value($search));
	

##  --------- Pagination part first start --------------##
if( isset( $_GET[ 'page' ] ) && $_GET[ 'page' ] != "" ) {
 $pageNum = $_GET[ 'page' ];
} else {
	 $pageNum = 1;
} 
$rowsPerPage = ($_GET['setlimit']) ? $_GET['setlimit'] : ROW_PER_PAGE;	
$pageName = "home.php?q=enquiry"; 
## Count all the records
$contactArray=$Obj->listRecord(return_post_value($search),$orderField, $orderBy);
$total_rows = count($contactArray);
$offset	= ($pageNum - 1) * $rowsPerPage;
$smarty->assign('page', $pageNum);
##  --------- Pagination part first end --------------##

##  --------- Pagination part second start --------------##
$newid = $pageNum * $rowsPerPage - $rowsPerPage + 1;
$smarty->assign('newid', $newid);
$contactArray=$Obj->listRecord(return_post_value($search),$orderField, $orderBy, $rowsPerPage, $offset);

if($search!='' && $search!='Search')
{
$other_id="search=".$search;
}
else
{
	$other_id="";
}

if($total_rows > ROW_PER_PAGE) {
	$pg = new pagination();					// CREATING OBJECT OF THE PAGINATION CLASS
	$pgnation = $pg->pagination_admin( $total_rows , $rowsPerPage , $pageNum , $pageName , $other_id, $class="");	
	$smarty-> assign("pagination",$pgnation);
}
##  --------- Pagination part second end --------------##	
$smarty->assign("contactArray",$contactArray);

if($_SESSION['msg']){
	$smarty->assign("msg", $_SESSION['msg']);
	unset($_SESSION['msg']);
}

//echo '<pre>';print_r($contactArray);
$smarty->assign('contactArray',$contactArray);

## Set active class variable for left menu  (main menu and sub menu)
$smarty->assign('activeclass', 'enquiry');
$smarty->assign('mainmenu', '4');

## Unset all the objects created which are on this page
unset($Obj);
unset($moduleObj);

$smarty->display(TEMPLATEDIR_ADMIN. 'controller/enquiry/enquiry.tpl');
unset($smarty);
?>
