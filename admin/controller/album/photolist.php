<?php  
## include required files
/*******************************/
require_once '../model/album.php';
//require_once '../model/moduleuser.php';
## Paginaton class
require_once ("../model/common/classes/pagination_class.php");
/*******************************/

$albumID = base64_decode($_GET['albumId']);
	

## Create Objects
/*******************************/
$albumObj = new Model_Album();
/*******************************/

## Active, Inactive selected records


## Get search parameters in variables - 
if($_REQUEST['search'] != '' && $_REQUEST['search'] != 'Search') {
	$searchindex = $_REQUEST['search'];
} else if($_REQUEST['search'] != '') {
	$searchindex = $_REQUEST['search'];
} else {
	$searchindex = '';
}


$searchindex=trim($searchindex);
// Assign search variable to show in search textbox
$smarty->assign('search', $searchindex);


//error_reporting(E_ALL);
if(isset($_POST['go']) != '' && isset($_POST['action']) != '') {
		$ids = implode(",", $_POST['checkall']);
		## check which action is selected
		$hidden_page = $_POST['hidden_pageno'];	
		if($_POST['action'] == 'active') {
			## Active selected records
			$albumObj->updateMultiplePhotoStatus($ids, '2');
			$_SESSION['msg']="<div class='success_msg'><span>Photo status activated successfully.</span></div>";
		} else if($_POST['action'] == 'inactive') {
			## Inactive selected records
			$albumObj->updateMultiplePhotoStatus($ids, '1');
			$_SESSION['msg']="<div class='success_msg'><span>Photo status deactivated successfully.</span></div>";
		} else {
			## Delete selected records
			$groupObj->deleteMultiplePhotos($ids);
			$_SESSION['msg']="<div class='success_msg'><span>Photo deleted successfully</span></div>";
		}
		header('location:'.SITE_URL.'/admin/home.php?q=photolist&albumId='.base64_encode($_GET['albumId']).'&page='.$hidden_page.'&order_by='.$orderBy.'&search='.$searchindex.'&order_field='.$orderField);
			exit;
		## Delete code will be here if required
		
} // if close

## Active/Inactive/Delete thourgh URL
if(isset($_GET['action']) == 'status' && isset($_GET['status']) != '') {
	$id = base64_decode($_GET['id']);		
	## Update training page status 
	$array = array();
	$array['status'] = ($_GET['status']=='1'?2:1);
	$albumObj->updatePhotoStatus($id, $array['status']);
	if($_GET['status']==1) {
		$_SESSION['msg']="<div class='success_msg'><span>Photo status activated successfully.</span></div>";
	} else {
		$_SESSION['msg']="<div class='success_msg'><span>Photo status deactivated successfully.</span></div>";
	}
	header('location:'.SITE_URL.'/admin/home.php?q=photolist&albumId='.base64_encode($_GET['albumId']).'&page='.$_GET['page'].'&order_by='.$orderBy.'&search='.$searchindex.'&order_field='.$orderField);
	exit;
}

if(isset($_GET['action']) == 'delete' && isset($_GET['id'])!= '') {
	$id = base64_decode($_GET['id']);
	## Delete Training
	$albumObj->deletePhoto($id);
	//exit;
	$_SESSION['msg']="<div class='success_msg'><span>Photo deleted successfully</span></div>";
	header('location:'.SITE_URL.'/admin/home.php?q=photolist&albumId='.base64_encode($_GET['albumId']).'&page='.$_GET['page']);
	exit;
}

/*******************************/
/*if($_GET['q']=='group')
{
	$moduelArray=$moduleObj->getModuleHelpText(13);
	$smarty->assign('moduelArray', $moduelArray);
}*/

//echo "<pre>"; print_r($moduelArray);exit;
##Fetch all training pages from database


##Fetch all Training pages from database

##  --------- Pagination part first start --------------##
	if( isset( $_REQUEST['page'] ) && $_REQUEST['page'] != "" ) {
		$pageNum = $_REQUEST['page'];
	} else {
		$pageNum = 1;
	} 
	//echo $pageNum;exit;
	$rowsPerPage = ($_GET['setlimit']) ? $_GET['setlimit'] : ROW_PER_PAGE;
	
	$pageName = "home.php?q=photolist&search=".$searchindex."&page=".$_GET['page']."&setlimit=".$_GET['setlimit']; 
	## Count all the records
       
	$albumArray = $albumObj->getAllAlbumPhotosByAId($albumID,$searchindex);
	$total_rows = count($albumArray);
	$offset	= ($pageNum - 1) * $rowsPerPage;
	$smarty->assign('page', $pageNum);
##  --------- Pagination part first end --------------##

##  --------- Pagination part second start --------------##
	$newid = $pageNum * $rowsPerPage - $rowsPerPage + 1;
	$smarty->assign('newid', $newid);
	$albumArray = $albumObj->getAllAlbumPhotosByAId($albumID,$searchindex,$rowsPerPage,$offset);

	$j=0;
	foreach($albumArray as $aArray)
	{
	$cnt = $albumObj->getPhotoLikeByPhotoId($aArray['id']);
	$albumArray[$j]['photoCount'] = $cnt;
	$j++;
	}	

	//echo "<pre>";print_r($albumArray);exit;
	$smarty->assign('albumArray', $albumArray);
	
	if($searchindex != '') {
		$other_id = "search=".$searchindex;
	} else {
		$other_id = '';
	}
	
	if($total_rows > ROW_PER_PAGE) {
		$pg = new pagination();					// CREATING OBJECT OF THE PAGINATION CLASS
		$pgnation = $pg->pagination_admin( $total_rows , $rowsPerPage , $pageNum , $pageName , $other_id, $class);		
		$smarty-> assign("pagination",$pgnation);
	}
##  --------- Pagination part second end --------------##	

## Assign session message to smarty variable and unset session variable
if($_SESSION['msg'] != '') {
	$smarty->assign('msg', $_SESSION['msg']);	
	unset($_SESSION['msg']);
}

## Set active class variable for left menu  (main menu and sub menu)
$smarty->assign('activeclass', 'photolist');
$smarty->assign('mainmenu', '5');

## Unset all the objects created which are on this page
unset($albumObj);
unset($moduleObj);

$smarty->display(TEMPLATEDIR_ADMIN. 'controller/album/photolist.tpl');
unset($smarty);
?>
