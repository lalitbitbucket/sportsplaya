<?php 
## include required files
include_once("../model/states.php");
## Create Objects
$stateObj  = new Model_States();
## Get all states by country id
$country_id = $_POST['countryId'];
$stateArray = $stateObj->getAllActiveStateByCountryId($country_id);
$str ='<option value="">Select State </option>';
foreach($stateArray as $state){
	$str .="<option value=".$state['stateId'].">".ucfirst($state['stateName'])."</option>";
}
echo $str;
?>
