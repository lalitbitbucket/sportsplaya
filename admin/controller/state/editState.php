<?php 
## include required files
/*******************************/
require_once '../model/states.php';
require_once '../model/country.php';

/*******************************/
## Create Objects
/*******************************/
$stateObj = new Model_States();
$country  = new Model_Country();
/*******************************/

if(isset($_POST) && !empty($_POST) ) {
	## apply PHP validation for required filed
	if( trim($_POST['statename']) != '')
	{	

		$stateeditArray['statName']   = return_post_value($_POST['statename']);		
		$stateeditArray['cntrID']      = return_post_value($_POST['country']);
		$stateObj->editStateById($stateeditArray,$_POST['stateId']);
		$_SESSION['msg'] = "<div class='success_msg'><span>State edited successfully</span></div>";
		header('location:'.SITE_URL.'/admin/home.php?q=state&page='.$_GET['page']);
		exit;
	} else {// php validation else
		$_SESSION['msg'] = '<div class="error_msg"><span>Please fill all required fields</span></div>';
	}
	// main if closed
}

if($_GET['id'] != ''&& $_GET['action'] == 'edit') {
	$state_id = base64_decode($_GET['id']);
	$stateDetArray = $stateObj->getStateDetailsById($state_id);	
	$smarty->assign('stateDetArray', $stateDetArray);
}

## Fetch all country list
$countryArr = $country->getAllCountryWithActiveStatus();
$smarty->assign('countryArr', $countryArr);

## Fetch all country list
if($stateDetArray['cntrID']) {
$countryDet = $country->getCountryDetailsById($stateDetArray['cntrID']);
$smarty->assign('countryDet', $countryDet);
}

## Assign success or error msg to smarty variable and unset session variable
if(trim($_SESSION['msg']) != '') {
	$smarty->assign('msg', $_SESSION['msg']);
	unset($_SESSION['msg']);
}

## Set active class variable for left menu
$smarty->assign('activeclass', 'state');
$smarty->assign('mainmenu', '2');
## Unset all the objects created which are on this page
unset($countryObj);
unset($stateObj);

$smarty->display(TEMPLATEDIR_ADMIN. 'controller/state/editState.tpl');
unset($smarty);
?>
