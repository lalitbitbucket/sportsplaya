<?php 
/*******************************/
require_once '../model/states.php';
require_once '../model/country.php';
/*******************************/
## Create Objects
/*******************************/
$stateObj = new Model_States();
$country  = new Model_Country();
/*******************************/

if(isset($_POST) && !empty($_POST)) {	
	## apply PHP validation for required filed
	if(trim($_POST['statename']) != '') {
		$stateAddArray['cntrID']      	= return_post_value($_POST['country']);
		$stateAddArray['statName']      = return_post_value($_POST['statename']);
		$stateAddArray['statStatus']    = '2';
		$stateObj->addStateByValue($stateAddArray);
		$_SESSION['msg'] = "<div class='success_msg'><span>State added successfully</span></div>";
		header('location:'.SITE_URL.'/admin/home.php?q=state&page='.$_POST['page']);
		exit;
	} else {// php validation else
		$_SESSION['msg'] = '<div class="error_msg"><span>Please fill all required fields</span></div>';
	}
	// main if closed
}

## Fetch all country list
$countryArr = $country->getAllCountryWithActiveStatus();
$smarty->assign('countryArr', $countryArr);

## Assign success or error msg to smarty variable and unset session variable
if(trim($_SESSION['msg']) != '') {
	$smarty->assign('msg', $_SESSION['msg']);
	unset($_SESSION['msg']);
}

## Set active class variable for left menu
$smarty->assign('activeclass', 'state');
$smarty->assign('mainmenu', '2');
## Unset all the objects created which are on this page
unset($countryObj);
unset($stateObj);

$smarty->display(TEMPLATEDIR_ADMIN. 'controller/state/addState.tpl');
unset($smarty);
?>
