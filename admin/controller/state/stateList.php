<?php  
## include required files
/*******************************/
require_once '../model/states.php';
require_once '../model/moduleuser.php';
## Paginaton class
require_once ("../model/common/classes/pagination_class.php");
/*******************************/
## Create Objects
/*******************************/
$stateObj = new Model_States();
$moduleObj=new Model_ModuleUser();
/*******************************/

if(isset($_GET['order_field']) && $_GET['order_field'] != '') {
	$orderField = $_GET['order_field'];
}
else {
	$orderField = 'statName';
}
$smarty->assign('orderField', $orderField);

if(isset($_GET['order_by']) && $_GET['order_by'] != '') {
	$orderBy = $_GET['order_by'];
}
else {
	$orderBy = 'ASC';
}
$smarty->assign('orderBy', $orderBy);

## Active, Inactive selected records
/*******************************/
if(isset($_POST['go']) != '' && isset($_POST['action']) != '') {
		$ids = implode(",", $_POST['checkall']);
		## check which action is selected
		$hidden_page = $_POST['hidden_pageno'];	
		
		if($_POST['action'] == 'active') {
			## Active selected records
			$stateObj->updateMultipleStateStatus($ids, '2');
			$_SESSION['msg'] = "<div class='success_msg'><span>State status activated successfully.</span></div>";
		} else if($_POST['action'] == 'inactive') {
			## Inactive selected records
			$stateObj->updateMultipleStateStatus($ids, '1');
			$_SESSION['msg'] = "<div class='success_msg'><span>State status deactivated successfully.</span></div>";
		} else {
			## Delete selected records
			$stateObj->deleteMultipleState($ids);
			$_SESSION['msg'] = "<div class='success_msg'><span>State deleted successfully</span></div>";
		}
		header('location:'.SITE_URL.'/admin/home.php?q=state&page='.$hidden_page);
		exit;
		## Delete code will be here if required
		
} // if close

## Active/Inactive/Delete thourgh URL
if(isset($_GET['action']) == 'status' && isset($_GET['status']) != '') {
	$id = base64_decode($_GET['id']);	
	## Update state page status 
	$array = array();
	$array['stateStatus'] = ($_GET['status']=='1'?2:1);
	$stateObj->updateStateStatus($id, $array['stateStatus']);
	if($_GET['status']==2) {
		$_SESSION['msg']="<div class='success_msg'><span>State status deactivated successfully.</span></div>";
	} else {
		$_SESSION['msg']="<div class='success_msg'><span>State status activated successfully.</span></div>";
	}
	header('location:'.SITE_URL.'/admin/home.php?q=state&page='.$_GET['page']);
	exit;
}

if(isset($_GET['action']) == 'delete' && isset($_GET['id']) != '') {
	$id = base64_decode($_GET['id']);
	## Delete State category
	$stateObj->deleteStateValueById($id);
	$_SESSION['msg'] = "<div class='success_msg'><span>State  deleted successfully</span></div>";
	header('location:'.SITE_URL.'/admin/home.php?q=state&page='.$_GET['page']);
	exit;
}
/*******************************/

if($_GET['q']=='state')
{
	$moduelArray=$moduleObj->getModuleHelpText(7);
	$smarty->assign('moduelArray', $moduelArray);
}
## Get search parameters in variables - 
if($_POST['search'] != '' && $_POST['search'] != 'State') {
	$searchindex = return_post_value($_POST['search']);
} else if($_GET['search'] != '' && $_GET['search'] != 'State') {
	$searchindex = return_post_value($_GET['search']);
} else {
	$searchindex = '';
}
$searchindex=trim($searchindex);
// Assign search variable to show in search textbox
$smarty->assign('search', $searchindex);
##Fetch all State pages from database

##  --------- Pagination part first start --------------##
	if( isset( $_GET[ 'page' ] ) && $_GET[ 'page' ] != "" ) {
		$pageNum = $_GET[ 'page' ];
	} else {
		$pageNum = 1;
	}
	
	if($_GET['setlimit']!=''){
		$rowsPerPage = $_GET['setlimit'];
	}
	else{
		$rowsPerPage = 25;
	}
	$pageName = "home.php?q=state".'&order_by='.$orderBy.'&search='.$searchindex.'&order_field='.$orderField.'&search_country='.$search_country.'$setlimit='.$_GET['setlimit']; ; 
	## Count all the records
	$stateArray = $stateObj->getAllStates($searchindex,$search_country,$orderField, $orderBy);
	$total_rows = count($stateArray);
	$offset	= ($pageNum - 1) * $rowsPerPage;
	$smarty->assign('page', $pageNum);
##  --------- Pagination part first end --------------##

##  --------- Pagination part second start --------------##
	$newid = $pageNum * $rowsPerPage - $rowsPerPage + 1;
	$smarty->assign('newid', $newid);
	$stateArray = $stateObj->getAllStates($searchindex,$search_country,$orderField, $orderBy,$rowsPerPage,$offset);
	$smarty->assign('stateArray', $stateArray);
	//echo "<pre>";print_r($stateArray);exit;
	if($searchindex != '') {
		$other_id = "search=".$searchindex;
	} else {
		$other_id = '';
	}
	
	if($search_country) {
		if($other_id == '') {
			$other_id = "country=".$search_country;
		} else {
			$other_id.= "&country=".$search_country;
		}
	}
	
	if($total_rows > 25) {
		$pg = new pagination();					// CREATING OBJECT OF THE PAGINATION CLASS
		$pgnation = $pg->pagination_admin( $total_rows , $rowsPerPage , $pageNum , $pageName , $other_id, $class);	
		$smarty-> assign("pagination",$pgnation);
	}
##  --------- Pagination part second end --------------##	
$totalStates = $stateObj-> getAllActiveStates();
$smarty->assign('totalStates', $totalStates);

## Assign session message to smarty variable and unset session variable
if($_SESSION['msg'] != '') {
	$smarty->assign('msg', $_SESSION['msg']);
	unset($_SESSION['msg']);
}

## Set active class variable for left menu  (main menu and sub menu)
$smarty->assign('activeclass', 'state');
$smarty->assign('mainmenu', '2');

## Unset all the objects created which are on this page
unset($stateObj);
unset($moduleObj);

$smarty->display(TEMPLATEDIR_ADMIN. 'controller/state/stateList.tpl');
unset($smarty);
?>
