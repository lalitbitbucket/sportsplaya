<?php
## include required files
/*******************************/
require_once '../model/features.php';
/*******************************/

## Create Objects
/*******************************/
$featObj = new Model_Features();

/*******************************/

## For page back pagination, searching and sorting
if($_REQUEST['page']){
	$page = $_REQUEST['page'];
	$smarty->assign('page',$page);
} else {
	$search = '';
}
if($_REQUEST['search']){
	$search = $_REQUEST['search'];
	$smarty->assign('search',$search);
} else {
	$search = '';
}
if($_REQUEST['order_by']){
	$order_by = $_REQUEST['order_by'];
	$smarty->assign('order_by',$order_by);
} else {
	$order_by = '';
}


if(isset($_POST) && !empty($_POST)) {
	## apply PHP validation for required filed
	if(trim($_POST['features_name']) != '') {
		## check feature already exists
		
		$extFeatArr = $featObj->checkfeaturesNameExists(stripslashes(trim($_POST['features_name'])), ''); 
		
		if(count($extFeatArr) == 0) {
			$FeatArray = array();
	
			$FeatArray['featName'] 	= stripslashes(trim($_POST['features_name']));
			$FeatArray['status'] 	= 2;	
			## add product features
			$featObj->addfeaturesByValue($FeatArray);
			$_SESSION['msg'] = "<div class='success_msg'><span>Feature added successfully</span></div>";
			header('location:'.SITE_URL.'/admin/home.php?q=managefeatures');
			exit;
		} else {
			$_SESSION['msg'] = '<div class="error_msg"><span>Feature name already exists</span></div>';
		}
	} else { 
		$_SESSION['msg'] = '<div class="error_msg"><span>Please fill all required fields</span></div>';
	}
	// main if closed
}

## Assign success or error msg to smarty variable and unset session variable
if(trim($_SESSION['msg']) != '') {
	$smarty->assign('msg', $_SESSION['msg']);
	unset($_SESSION['msg']);
}

## Set active class variable for left menu
$smarty->assign('activeclass', 'managefeatures');
$smarty->assign('mainmenu', '2');
## Unset all the objects created which are on this page
unset($featObj);

$smarty->display(TEMPLATEDIR_ADMIN. 'controller/features/addfeatures.tpl');
unset($smarty);
?>
