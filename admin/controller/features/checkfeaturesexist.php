<?php
## include required files
/*******************************/ 
include_once("../model/features.php");
/*******************************/
## Create Objects
/*******************************/
$featObj = new Model_Features();

if ($_GET['features_name']) {
	$featName = stripslashes($_GET['features_name']); 
}
if ($_GET['id']!='' && $_GET['id']!='0'){
	$id = $_GET['id'];
}

if($featName) {
	// this function check features name Exist ot not
	$result = $featObj->checkfeaturesNameExists(trim($featName), $id); 
	if(count($result) > 0){ 	
		if(strtolower($result['featName']) == strtolower ($featName)){  
			echo 'false';
		}
	} else {
		echo "true";	
	}
}
unset($featObj);
?>
