<?php 
## include required files
/*******************************/
require_once '../model/features.php';
/*******************************/

## Create Objects
/*******************************/
$featObj = new Model_Features();
/*******************************/

## For page back pagination, searching and sorting
if($_REQUEST['page']){
	$page = $_REQUEST['page'];
	$smarty->assign('page',$page);
} else {
	$search = '';
}

if($_REQUEST['search']){
	$search = $_REQUEST['search'];
	$smarty->assign('search',$search);
} else {
	$search = '';
}

if($_REQUEST['order_by']){
	$order_by = $_REQUEST['order_by'];
	$smarty->assign('order_by',$order_by);
} else {
	$order_by = '';
}

if(isset($_GET['order_field']) && $_GET['order_field'] != '') {
	$orderField = $_GET['order_field'];
}
else {
	$orderField = 'featName';
}
$smarty->assign('orderField', $orderField);
if(isset($_POST) && !empty($_POST)) {
	## apply PHP validation for required filed
	if(trim($_POST['features_name']) != '') {
		$featuresArray = array();
		
		$featuresArray['featName'] = return_post_value($features_name);		
		## update features details
		$featObj->editfeaturesValueById($featuresArray,$features_id);
		$_SESSION['msg'] = "<div class='success_msg'><span>Feature updated successfully</span></div>";
		header('location:'.SITE_URL.'/admin/home.php?q=managefeatures&page='.$page.'&search='.$search.'&order_by='.$order_by);
		exit;
	} else { 
		$_SESSION['msg'] = '<div class="error_msg"><span>Please enter feature name</span></div>';
	}
	// main if closed
}

//echo "Hello"; exit;

## Fetch admin user details by id
if($_GET['id'] != '' && $_GET['action'] == 'edit') {
	$features_id = base64_decode($_GET['id']);	
	$features_details = $featObj->getfeaturesDetailByfeatID($features_id);
	$smarty->assign('features_details', $features_details);
}
## Assign success or error msg to smarty variable and unset session variable
if(trim($_SESSION['msg']) != '') {
	$smarty->assign('msg', $_SESSION['msg']);
	unset($_SESSION['msg']);
}

## Set active class variable for left menu
$smarty->assign('activeclass', 'managefeatures');
$smarty->assign('mainmenu', '2');
## Unset all the objects created which are on this page
unset($featObj);

$smarty->display(TEMPLATEDIR_ADMIN. 'controller/features/updatefeatures.tpl');
unset($smarty);
?>
