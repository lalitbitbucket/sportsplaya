<?php 
## include required files
/*******************************/
require_once '../model/faqs.php';
/*******************************/
## Create Objects
/*******************************/
$faqObj = new Model_FAQ();
/*******************************/
## Get search parameters in variables - 
if($_POST['search'] != '' && $_POST['search'] != 'Search') {
	$searchindex = $_POST['search'];
} else if($_GET['search'] != '') {
	$searchindex = $_GET['search'];
} else {
	$searchindex = '';
}
$searchindex=trim($searchindex);
$smarty->assign('search', $searchindex);

if($_POST['search_cat'] != '') {
	$search_faqcat = $_POST['search_cat'];
} else if($_GET['search_faqcat'] != '') {
	$search_faqcat = $_GET['search_faqcat'];
} else {
	$search_faqcat = '';
}
$smarty->assign('search_faqcat', $search_faqcat);


$faq_id = base64_decode($_REQUEST['id']);

if(isset($_POST['editfaq_btn'])) {
	## apply PHP validation for required filed
	if(trim($_POST['faq_ques']) != '' && trim($_POST['faqans_content']) != '') {
		$submenuArray = array();

		$faqArray['question']      = return_post_value($_POST['faq_ques']);
		$faqArray['answer']        = return_post_value($_POST['faqans_content']);
		$faqId = base64_decode(return_post_value($_POST['id']));
		$faqArray['updateDate'] = date("Y-m-d H:i:s");
		$faqObj->editFaqById($faqArray,$faqId);
		$_SESSION['msg'] = "<div class='success_msg'><span>FAQ updated successfully</span></div>";
		header('location:'.SITE_URL.'/admin/home.php?q=FAQ&page='.$_POST['page'].'&order_by='.$orderBy.'&search='.$searchindex.'&order_field='.$orderField.'&search_faqcat='.$search_faqcat);
		exit;
	} else {// php validation else
		$_SESSION['msg'] = '<div class="error_msg"><span>Please enter faq question and answer</span></div>';
	}
	// main if closed
}

if($_GET['id']!=''&& $_GET['action']=='edit') {
	
	$faqArray = $faqObj->getFaqDetailsById($faq_id);
	$smarty->assign('faqArray', $faqArray);
}
//echo "<pre>";print_r($faqArray);exit;

## Assign success or error msg to smarty variable and unset session variable
if(trim($_SESSION['msg']) != '') {
	$smarty->assign('msg', $_SESSION['msg']);
	unset($_SESSION['msg']);
}

## Set active class variable for left menu
$smarty->assign('activeclass', 'FAQ');
$smarty->assign('mainmenu', '2');
## Unset all the objects created which are on this page
unset($faqObj);

$smarty->display(TEMPLATEDIR_ADMIN. 'controller/faq/editFAQ.tpl');
unset($smarty);
?>
