<?php  
## include required files
/*******************************/
require_once '../model/faqs.php';
require_once '../model/moduleuser.php';
## Paginaton class
require_once ("../model/common/classes/pagination_class.php");
/*******************************/

## Create Objects
/*******************************/
$faqObj = new Model_FAQ();
$moduleObj=new Model_ModuleUser();
/*******************************/

## Get search parameters in variables - 
if($_POST['search'] != '' && $_POST['search'] != 'Search') {
	$searchindex = $_POST['search'];
} else if($_GET['search'] != '') {
	$searchindex = $_GET['search'];
} else {
	$searchindex = '';
}
$searchindex=trim($searchindex);
$smarty->assign('search', $searchindex);

if($_POST['search_cat'] != '') {
	$search_faqcat = $_POST['search_cat'];
} else if($_GET['search_faqcat'] != '') {
	$search_faqcat = $_GET['search_faqcat'];
} else {
	$search_faqcat = '';
}
$smarty->assign('search_faqcat', $search_faqcat);


//error_reporting(E_ALL);
if(isset($_POST['go']) != '' && isset($_POST['action']) != '') {
		$ids = implode(",", $_POST['checkall']);
		## check which action is selected
		$hidden_page = $_POST['hidden_pageno'];	
		if($_POST['action'] == 'active') {
			## Active selected records
			//echo "<pre>";print_r($ids);exit;
			$faqObj->updateFaqStatus($ids,'2');
			$_SESSION['msg']="<div class='success_msg'><span>FAQ status activated successfully.</span></div>";
		} else if($_POST['action'] == 'inactive') {
			## Inactive selected records
				//echo "<pre>";print_r($ids);exit;
			$faqObj->updateFaqStatus($ids, '1');
			$_SESSION['msg']="<div class='success_msg'><span>FAQ status deactivated successfully.</span></div>";
		} else {
			## Delete selected records
				$faqObj->deleteFaq($ids);
			$_SESSION['msg']="<div class='success_msg'><span>FAQ deleted successfully</span></div>";
		}
		header('location:'.SITE_URL.'/admin/home.php?q=FAQ&page='.$hidden_page.'&order_by='.$orderBy.'&search='.$searchindex.'&order_field='.$orderField);
			exit;
		## Delete code will be here if required
		
} // if close

## Active/Inactive/Delete thourgh URL
if(isset($_GET['action']) == 'status' && isset($_GET['status']) != '') {
	
	$id = base64_decode($_GET['id']);		
	## Update country page status 
	$array = array();
	$array['updateDate'] = date("Y-m-d H:i:s");
	$array['status'] = ($_GET['status']=='1'?2:1);
	$faqObj->editFaqById($array, $id);
	if($array['status']==2) {
		
		$_SESSION['msg']="<div class='success_msg'><span>FAQ status activated successfully.</span></div>";
	} else {
		
		$_SESSION['msg']="<div class='success_msg'><span>FAQ status deactivated successfully.</span></div>";
	}
	header('location:'.SITE_URL.'/admin/home.php?q=FAQ&page='.$_GET['page'].'&order_by='.$orderBy.'&search='.$searchindex.'&order_field='.$orderField);
	exit;
}

if(isset($_GET['action']) == 'delete' && isset($_GET['id']) != '') {
	$id = base64_decode($_GET['id']);
	## Delete FAQ 
	//$faqObj->deleteFaq($id);
	$array = array();
	$array['status'] = '0';
	$faqObj->editFaqById($array, $id);
	$_SESSION['msg']="<div class='success_msg'><span>FAQ deleted successfully</span></div>";
	header('location:'.SITE_URL.'/admin/home.php?q=FAQ&page='.$_GET['page']);
	exit;
}

	/*******************************/
	##  --------- Pagination part first start --------------##
	if( isset( $_GET[ 'page' ] ) && $_GET[ 'page' ] != "" ) {
		$pageNum = $_GET[ 'page' ];
	} else {
		$pageNum = 1;
	} 
	$rowsPerPage = ($_GET['setlimit']) ? $_GET['setlimit'] : ROW_PER_PAGE;
	$pageName = "home.php?q=FAQ&page=".$_GET['page'].'&order_by='.$orderBy.'&search='.$searchindex.'&order_field='.$orderField.'&search_faqcat='.$search_faqcat.'&setlimit='.$rowsPerPage; 
	
	## Count all the records
	$faqArray = $faqObj->getAllFaqs(return_post_value($searchindex), $search_faqcat);
	$total_rows = count($faqArray);
	$offset	= ($pageNum - 1) * $rowsPerPage;
	$smarty->assign('page', $pageNum);
##  --------- Pagination part first end --------------##

##  --------- Pagination part second start --------------##
	$newid = $pageNum * $rowsPerPage - $rowsPerPage + 1;
	$smarty->assign('newid', $newid);
	$faqArray = $faqObj->getAllFaqs(return_post_value($searchindex),$search_faqcat,$rowsPerPage,$offset);
	$smarty->assign('faqArray', $faqArray);
	
	if($searchindex != '') {
		$other_id = "search=".$searchindex;
	} else {
		$other_id = '';
	}
	
	if($search_faqcat) {
		if($other_id == '') {
			$other_id = "category=".$search_faqcat;
		} else {
			$other_id.= "&category=".$search_faqcat;
		}
	}
	
	if($total_rows > $rowsPerPage) {
		$pg = new pagination();					// CREATING OBJECT OF THE PAGINATION CLASS
		$pgnation = $pg->pagination_admin( $total_rows , $rowsPerPage , $pageNum , $pageName , $other_id, $class);	
		$smarty-> assign("pagination",$pgnation);
	}
##  --------- Pagination part second end --------------##	
$getAllFAQs = $faqObj->getAllFaqss();
$smarty->assign('getAllFAQs', $getAllFAQs);

## Assign session message to smarty variable and unset session variable
if($_SESSION['msg'] != '') {
	$smarty->assign('msg', $_SESSION['msg']);	
	unset($_SESSION['msg']);
}

## Set active class variable for left menu  (main menu and sub menu)
$smarty->assign('activeclass', 'FAQ');
$smarty->assign('mainmenu', '2');

## Unset all the objects created which are on this page
unset($faqObj);
unset($moduleObj);

$smarty->display(TEMPLATEDIR_ADMIN. 'controller/faq/FAQ.tpl');
unset($smarty);
?>
