<?php  
## include required files
/*******************************/
require_once '../model/faqs.php';
/*******************************/
## Create Objects
/*******************************/
$faqObj = new Model_FAQ();
/*******************************/
## Get search parameters in variables - 
if($_POST['search'] != '' && $_POST['search'] != 'Search') {
	$searchindex = $_POST['search'];
} else if($_GET['search'] != '') {
	$searchindex = $_GET['search'];
} else {
	$searchindex = '';
}
$searchindex=trim($searchindex);
$smarty->assign('search', $searchindex);

if($_POST['search_cat'] != '') {
	$search_faqcat = $_POST['search_cat'];
} else if($_GET['search_faqcat'] != '') {
	$search_faqcat = $_GET['search_faqcat'];
} else {
	$search_faqcat = '';
}
$smarty->assign('search_faqcat', $search_faqcat);

if(isset($_POST['addfaq_btn'])) {
 	## apply PHP validation for required filed
	if(trim($_POST['faq_ques']) != '' && trim($_POST['faqans_content']) != '') {
		$faqArray = array();

		//$faqArray['categoryId']   = $_POST['faq_cat'];
		$faqArray['question']      = return_post_value($_POST['faq_ques']);
		$faqArray['answer']        = return_post_value($_POST['faqans_content']);
		$faqArray['updateDate']    = date("Y-m-d H:i:s");
		$faqArray['status']        = 2;
      		$faqObj->addFaq($faqArray);
		$_SESSION['msg'] = "<div class='success_msg'><span>FAQ added successfully</span></div>";
		header('location:'.SITE_URL.'/admin/home.php?q=FAQ');
		exit;
	} else {// php validation else
		$_SESSION['msg'] = 'Please enter faq question and answer';
	}
	// main if closed
}

## Assign success or error msg to smarty variable and unset session variable
if(trim($_SESSION['msg']) != '') {
	$smarty->assign('msg', $_SESSION['msg']);
	unset($_SESSION['msg']);
}

## Set active class variable for left menu
$smarty->assign('activeclass', 'FAQ');
$smarty->assign('mainmenu', '2');
## Unset all the objects created which are on this page
unset($faqObj);

$smarty->display(TEMPLATEDIR_ADMIN. 'controller/faq/addFAQ.tpl');
unset($smarty);
?>
