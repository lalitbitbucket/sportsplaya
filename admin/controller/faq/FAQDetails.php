<?php 
## include required files
/*******************************/
require_once '../model/faqs.php';
/*******************************/
## Create Objects
/*******************************/
$faqObj = new Model_FAQ();
/*******************************/
## Get search parameters in variables - 
if($_POST['search'] != '' && $_POST['search'] != 'Search') {
	$searchindex = $_POST['search'];
} else if($_GET['search'] != '') {
	$searchindex = $_GET['search'];
} else {
	$searchindex = '';
}
$searchindex=trim($searchindex);
$smarty->assign('search', $searchindex);

if($_POST['search_cat'] != '') {
	$search_faqcat = $_POST['search_cat'];
} else if($_GET['search_faqcat'] != '') {
	$search_faqcat = $_GET['search_faqcat'];
} else {
	$search_faqcat = '';
}
$smarty->assign('search_faqcat', $search_faqcat);


if($_GET['id']!='') {
	$faq_id = base64_decode($_GET['id']);
	$faqArray = $faqObj->getFaqDetailsById($faq_id);
	
	$smarty->assign('faqArray', $faqArray);
	$smarty->assign('faqcatName', $faqcatName);
}

## Set active class variable for left menu
$smarty->assign('activeclass', 'FAQ');
$smarty->assign('mainmenu', '2');

## Unset all the objects created which are on this page
unset($faqObj);

$smarty->display(TEMPLATEDIR_ADMIN. 'controller/faq/FAQDetails.tpl');
unset($smarty);
?>
