<?php
require_once '../model/users.php';
require_once '../model/useraddress.php';
require_once '../model/states.php';


## Initializing Objects
/**************************/
$userObj = new Model_Users();
$addressObj 	= new Model_User_Address();
$stateObj = new Model_States();
/**************************/

if($_GET['id'] != '') {
	$user_id = base64_decode($_GET['id']);	
	$user_details = $userObj->getUserDetailsByUserId($user_id);
	## get user's default address
        $user_address = $addressObj->getDefaultAddressDetailByUserID($user_id);
        $user_details['addrID'] = $user_address['addrID'];
	$user_details['address'] = $user_address['address'];
	$user_details['city'] = $user_address['city'];
	$stDtl = $stateObj->getStateDetailsByStateId($user_address['state']);
        $user_details['statName'] =  $stDtl['statName'];
	$user_details['zip'] = $user_address['zip'];
	$user_details['contact'] = $user_address['contact'];
	//echo "<pre>";print_r($user_details);exit;
	$smarty->assign('user_details', $user_details);
}

/*******************************/
##Fetch all admin users from database
/******************************/
	
	if($_GET['q']=='siteuser')
	{
		$moduelArray=$moduleObj->getModuleHelpText(9);
		$smarty->assign('moduelArray', $moduelArray);
	}
/***************************/
##Assign Success Error messages
/***************************/
	if($_SESSION['msg']!='')
	{
		$smarty->assign('msg',$_SESSION['msg']);
		unset($_SESSION['msg']);
	}
	
	$smarty->assign('activeclass','siteusers');
	$smarty->assign('mainmenu', '3');
	
## Unset all objects which are on this page	
	unset($moduleObj);
	unset($userObj);
	$smarty->display(TEMPLATEDIR_ADMIN. 'controller/siteuser/viewsiteuser.tpl');
	unset($smarty);



?>