<?php 
## include required files
/*******************************/
require_once '../model/country.php';
require_once '../model/states.php';
require_once '../model/users.php';
require_once '../model/email.php';
require_once '../model/useraddress.php';
require_once '../includes/phpmailer/class.phpmailer.php';
/*******************************/

## Create Objects
/*******************************/
$stateObj = new Model_States();
$emailObj = new Model_Email();
$mail 		= new PHPMailer(true);
$userObj = new Model_Users();
$addressObj 	= new Model_User_Address();
/*******************************/

## Fetch admin user details by id
if($_GET['id'] != '' && $_GET['action'] == 'edit') {
	$user_id = base64_decode($_GET['id']);	
	$user_details = $userObj->getUserDetailsByUserId($user_id);
	## get user's default address
	$user_address = $addressObj->getDefaultAddressDetailByUserID($user_id);
	$user_details['addrID'] = $user_address['addrID'];
	$user_details['address'] = $user_address['address'];
	$user_details['city'] = $user_address['city'];
	$user_details['state'] = $user_address['state'];
	$user_details['zip'] = $user_address['zip'];
	$user_details['contact'] = $user_address['contact'];
	$smarty->assign('user_details', $user_details);
}

if($_POST['last_name']!='' || $_POST['first_name']!='') {
	## apply PHP validation for required filed
	if(trim($_POST['first_name']) != '' && trim($_POST['last_name']) != '' && trim($_POST['email']) != '' && trim($_POST['address']) != '' && trim($_POST['city']) != '' && trim($_POST['state']) != '' && trim($_POST['zip']) != '' && trim($_POST['contact']) != '') {
		
		

	$userArray = array();
	
	$user_id = $_POST['hidden_userid'];
	$addrID = $_POST['hidden_addressid'];
	$userArray['fName']   = return_post_value($first_name);
	$userArray['lName']   = return_post_value($last_name);
	$userArray['email']   = return_post_value($email);
	if(return_post_value($password)!='password'){
		$userArray['paswd']      = md5(return_post_value($password));
		$pass = return_post_value($password);
		
	}						
	if($_POST['password']!='password' || $_POST['email']!=$user_details['email']){
			
	      ################################### Mail Template Start ############################
				## Get Admin details 
			$id=1;
			$admindetails = $userObj->getUserDetailsByUserId($id);
			$smarty->assign('admindetails',$admindetails);
		
			## Fetch email content
			$emailArray = $emailObj->getEmailById(7);
			
			$to= $user_details['email'];
			$toname= $user_details['fName'];
			
			$subject=$emailArray['emailSub'];
			$subject = str_replace('[SITENAME]', SITENAME, $subject);
			
			$message=$emailArray['emailContent'];
			$message=str_replace('[SITENAME]', SITENAME, $message);
			$message=str_replace('[EMAIL]', return_post_value(trim($email)), $message);
			$message=str_replace('[NAME]', ucfirst(return_post_value(trim($first_name))), $message);
			$message = str_replace('[Email_Address]',$userArray['email'] , $message);
			$message = str_replace('[PASSWORD]',$pass , $message);
			$message=str_replace('[SITENAME]', SITENAME, $message);		
			$message=str_replace('[SITELINK]',SITE_URL , $message);
			
			$from = $emailArray['fEmail'];
			$fromname = $emailArray['fName'];
			
			$emailTemplate=file_get_contents('../siteAssets/mailTemplates/email.html');
			
			$template_msg = str_replace('[EMAIL]',$userDetails['email'] , $emailTemplate);
			$template_msg = str_replace('[PASSWORD]',$pass, $template_msg);
			$template_msg = str_replace('[NAME]',$userDetails['fName'] , $template_msg);
			$template_msg = str_replace('[SITENAME]',SITENAME , $template_msg);
			$template_msg = str_replace('[LOGO]','<img src="'.SITE_URL.'/siteAssets/images/logo.png">', $template_msg);
			$template_msg = str_replace('[MESSAGE]',$message , $template_msg);
			$template_msg = str_replace('[SITELINK]',SITE_URL , $template_msg);
			$template_msg = str_replace('[SUBJECT]',$subject , $template_msg);
			$template_msg = str_replace('[SITEROOT]',SITE_URL , $template_msg);					
			
			
		//        echo $subject."<br />";
		//	echo $from."<br />";
		//	echo $fromname."<br />";
		//	echo $to."<br />";
		//	echo $toname."<br />";
		//	echo "<pre>"; print_r($template_msg);exit;
		       
		
		try {
			$mail->AddAddress($to, $toname);
			$mail->SetFrom($from, $fromname);
			$mail->Subject = $subject;
			$mail->Body = $template_msg; 
			$mail->Send();  
			$_SESSION['msg'] = '<section class="success_msg">Site user updated successfully.</section>';
		} 
		catch (phpmailerException $e) {
			$_SESSION['msg']= $e->errorMessage(); //Pretty error messages from PHPMailer
		} catch (Exception $e) {
			$_SESSION['msg']= $e->getMessage(); //Boring error messages from anything else!
		}	

	
	}
	
	
	//echo "<pre>";print_r($userArray);exit;
	$userObj->editUserValueById($userArray, $user_id);
	
	## update user address
	$userAddressArray = array();
	$userAddressArray['address'] 		= return_post_value($_POST['address']);
	$userAddressArray['city'] 		= return_post_value($_POST['city']);
	$userAddressArray['state'] 		= return_post_value($_POST['state']);
	$userAddressArray['zip'] 		= return_post_value($_POST['zip']);
	$userAddressArray['contact'] 		= return_post_value($_POST['contact']);
	$userAddressID = $addressObj->updateUserAddressByValue($userAddressArray, $addrID);	
	$_SESSION['msg'] = "<div class='success_msg'><span>Site user updated successfully</span></div>";
	header('location:'.SITE_URL.'/admin/home.php?q=siteuser&page='.$_GET['page'].'&order_field='.$orderField.'&order_by='.$orderBy.'&search='.$searchindex);
	exit();

	
	} else { // php validation else
		$_SESSION['msg'] = '<div class="error_msg"><span>Please fill all required fields</span></div>';
	}
	// main if closed
}


$stateArray = $stateObj->getAllActiveStates();
$smarty->assign('stateArray', $stateArray);
## Assign success or error msg to smarty variable and unset session variable
if(trim($_SESSION['msg']) != '') {
	$smarty->assign('msg', $_SESSION['msg']);
	unset($_SESSION['msg']);
}

## Set active class variable for left menu
$smarty->assign('activeclass', 'siteusers');
$smarty->assign('mainmenu', '3');
## Unset all the objects created which are on this page
unset($userObj);

$smarty->display(TEMPLATEDIR_ADMIN. 'controller/siteuser/editsiteuser.tpl');
unset($smarty);
?>