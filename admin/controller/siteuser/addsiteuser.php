<?php 
## include required files
/*******************************/
require_once '../model/users.php';
require_once '../model/email.php';
require_once '../model/useraddress.php';
require_once "../model/common/classes/pagination_class.php";
require_once '../includes/phpmailer/class.phpmailer.php';
require_once '../model/states.php';
/*******************************/

## Create Objects
/*******************************/

$emailObj = new Model_Email();
$userObj = new Model_Users();
$stateObj = new Model_States();
$addressObj = new Model_User_Address();
$mail 	= new PHPMailer(true);
/*******************************/

if(sizeof($_POST)>0) 
{
	 ## apply PHP validation for required filed
	if(trim($_POST['first_name']) != '' && trim($_POST['last_name']) != '' && trim($_POST['email']) != '' && trim($_POST['address']) != '' && trim($_POST['city']) != '' && trim($_POST['state']) != '' && trim($_POST['zip']) != '' && trim($_POST['contact']) != '') 
	{
		$userArray = array();

		$pass = return_post_value($password);
		$userArray['fName']   = return_post_value($first_name);
		$userArray['lName']   = return_post_value($last_name);
		$userArray['email']   = return_post_value($email);
		$userArray['paswd']   = md5(return_post_value($password));
		$userArray['userType']   = '3';
		$userArray['status']   = '2';
		$userArray['regDate']   = date("Y-m-d");
		$userArray['regIP']   = $_SERVER['REMOTE_ADDR'];
		$userid = $userObj->addUserByValue($userArray);
		
		## add user address
		$userAddressArray = array();
		$userAddressArray['uID'] 		= $userid;
		$userAddressArray['address'] 		= return_post_value($_POST['address']);
		$userAddressArray['city'] 		= return_post_value($_POST['city']);
		$userAddressArray['state'] 		= return_post_value($_POST['state']);
		$userAddressArray['zip'] 		= return_post_value($_POST['zip']);
		$userAddressArray['contact'] 		= return_post_value($_POST['contact']);
		$userAddressArray['isDefault'] 		= 'y';
		$userAddressID = $addressObj->addUserAddressByValue($userAddressArray);
		//echo "<pre>";print_r($userArray);exit;
		## Get user details by id
		$userDetails = $userObj->getDetailsByUserId($userid);
		
			################################### Mail Template Start ############################
					## Get Admin details 
				$id=1;
				$admindetails = $userObj->getUserDetailsByUserId($id);
				$smarty->assign('admindetails',$admindetails);
			
				## Fetch email content
				$emailArray = $emailObj->getEmailById(6);
				$to= $userArray['email'];
				$toname= $userArray['email'];
				$subject=$emailArray['emailSub'];
				$subject = str_replace('[SITENAME]', SITENAME, $subject);
				$message=$emailArray['emailContent'];
				$message=str_replace('[SITENAME]', SITENAME, $message);
				$message=str_replace('[EMAIL]', return_post_value(trim($email)), $message);
				$message=str_replace('[NAME]', ucfirst(return_post_value(trim($first_name)))." ".ucfirst(return_post_value(trim($last_name))), $message);
				$message = str_replace('[Email_Address]',$userArray['email'] , $message);
				$message = str_replace('[PASSWORD]',$pass , $message);
				$message=str_replace('[SITENAME]', SITENAME, $message);		
				$message=str_replace('[SITELINK]',SITE_URL , $message);
				
				/*$fromname = ucfirst((trim($admindetails['fName'])))." ".ucfirst((trim($admindetails['lName'])));
				$from = $admindetails['email'];*/
				
				$from = $emailArray['fEmail'];
				$fromname = $emailArray['fName'];
				
				$emailTemplate=file_get_contents('../siteAssets/mailTemplates/email.html');
				
				$template_msg = str_replace('[EMAIL]',$userDetails['email'] , $emailTemplate);
				$template_msg = str_replace('[PASSWORD]',$pass, $template_msg);
				$template_msg = str_replace('[NAME]',$userDetails['fName'] , $template_msg);
				$template_msg = str_replace('[SITENAME]',SITENAME , $template_msg);
				$template_msg = str_replace('[LOGO]','<img src="'.SITE_URL.'/siteAssets/images/logo.png">', $template_msg);
				$template_msg = str_replace('[MESSAGE]',$message , $template_msg);
				$template_msg = str_replace('[SITELINK]',SITE_URL , $template_msg);
				$template_msg = str_replace('[SUBJECT]',$subject , $template_msg);
				$template_msg = str_replace('[SITEROOT]',SITE_URL , $template_msg);					
				
				
			//      echo $subject."<br />";
			//	echo $from."<br />";
			//	echo $fromname."<br />";
			//	echo $to."<br />";
			//	echo "<pre>"; print_r($template_msg);exit;
			       
			
			try {
				$mail->AddAddress($to, $toname);
				$mail->SetFrom($from, $fromname);
				$mail->Subject = $subject;
				$mail->Body = $template_msg; 
				$mail->Send();  
				$_SESSION['msg'] = '<section class="success_msg">Siteuser added successfully.</section>';
			} 
			catch (phpmailerException $e) {
				$_SESSION['msg']= $e->errorMessage(); //Pretty error messages from PHPMailer
			} catch (Exception $e) {
				$_SESSION['msg']= $e->getMessage(); //Boring error messages from anything else!
			}	
	
		//echo $template_msg;exit;
		//$_SESSION['msg'] = "<div class='success_message'><span>siteuser added successfully</span></div>";
		header('location:'.SITE_URL.'/admin/home.php?q=siteuser&page='.$_GET['page'].'&order_field='.$orderField.'&order_by='.$orderBy.'&search='.$searchindex);
		exit();
		
	} 
	else { // php validation else
		$_SESSION['msg'] = '<div class="error_msg"><span>Please fill all required fields</span></div>';
	}
	// main if closed
}

$stateArray = $stateObj->getAllActiveStates();
$smarty->assign('stateArray', $stateArray);

if($_GET['q']=='siteuser')
		{
			$moduelArray=$moduleObj->getModuleHelpText(10);
			$smarty->assign('moduelArray', $moduelArray);
		}
	/***************************/
	##Assign Success Error messages
	/***************************/
		if($_SESSION['msg']!='')
		{
			$smarty->assign('msg',$_SESSION['msg']);
			unset($_SESSION['msg']);
		}
		
		$smarty->assign('activeclass','siteusers');
		$smarty->assign('mainmenu', '3');
	
$smarty->display(TEMPLATEDIR_ADMIN. 'controller/siteuser/addsiteuser.tpl');
unset($smarty);
?>