<?php 
## include required files
/*******************************/
require_once '../model/group.php';
require_once '../model/common/image_functions.php';
/*******************************/
## Create Objects
/*******************************/
$groupObj = new Model_Group();
/*******************************/
//error_reporting(E_ALL);

## Get search parameters in variables - 
if($_POST['search'] != '' && $_POST['search'] != 'Search') {
	$searchindex = ucfirst($_POST['search']);
} else if($_GET['search'] != '') {
	$searchindex = $_GET['search'];
} else {
	$searchindex = '';
}
$searchindex=trim($searchindex);
// Assign search variable to show in search textbox
$smarty->assign('search', $searchindex);
if(isset($_GET['order_field']) && $_GET['order_field'] != '') {
	$orderField = $_GET['order_field'];
}
else {
	$orderField = 'groupname';
}
$smarty->assign('orderField', $orderField);

if(isset($_GET['order_by']) && $_GET['order_by'] != '') {
	$orderBy = $_GET['order_by'];
}
else {
	$orderBy = 'ASC';
}
$smarty->assign('orderBy', $orderBy);


if(isset($_POST['addgroup_btn'])) {
## apply PHP validation for required filed
	if(trim($_POST['name']) != '' && trim($_POST['detail']) != '') {
		$groupAddArray = array();	
		//echo '<pre>'; print_r($_POST); exit;	
		if($_FILES['image']['name']){
			## check valid image extension
			$fileExt = file_extension($_FILES['image']['name']); 
			$validExt = valid_extension($fileExt);
			## upload file
			$fileName      = time()."_".$_FILES['image']['name'];
			$temp=$_FILES["image"]["tmp_name"];	
			$imageFolder='../dynamicAssets/groups/';
			copy($_FILES["image"]["tmp_name"],$imageFolder.$fileName);
			$groupAddArray['image'] = $fileName;
			
			## creating thumb images
			$imageName= $fileName;
			$thumbFolder1 ="../dynamicAssets/groups/61x61/";						
			$height1 = 61;
			$width1= 61;
			$file1=uploadImageAndCreateThumb($imageName,$imageFolder,$thumbFolder1,$height1,$width1,$fileExt);
			
			$thumbFolder2 ="../dynamicAssets/groups/138x138/";						
			$height2 = 138;
			$width2= 138;
			$file2=uploadImageAndCreateThumb($imageName,$imageFolder,$thumbFolder2,$height2,$width2,$fileExt);
			
			$thumbFolder3 ="../dynamicAssets/groups/80x80/";						
			$height3 = 80;
			$width3= 80;
			$file3=uploadImageAndCreateThumb($imageName,$imageFolder,$thumbFolder3,$height3,$width3,$fileExt);
		}
		
		$groupAddArray['groupname'] 		= return_post_value($_POST['name']);
		$groupAddArray['description'] 		= return_post_value($_POST['detail']);
		$groupAddArray['addedDate'] 		= date('Y-m-d');
		$groupAddArray['userId']      		= '1';
		$groupAddArray['status']      		= '2';
		
		$groupID = $groupObj->addGroupByValue($groupAddArray);
		/*## add to group member table
		$groupMemArray = array();
		$groupMemArray['groupId'] = $groupID;
		$groupMemArray['uID'] = $_SESSION["acAdminId"];
		$groupMemArray['joinDate'] = date('Y-m-d');
		$groupMemArray['isModerator'] = 'y';
		$groupObj->addUsersGroupByValue($groupMemArray);*/
		$_SESSION['msg'] = "<div class='success_msg'><span>Group added successfully</span></div>";
		header('location:'.SITE_URL.'/admin/home.php?q=group&page='.$_POST['page']);
		exit();
	}
	else {
		$_SESSION['msg'] = '<div class="error_msg"><span>Please fill all the required fields </span></div>';
	}
}


## Assign success or error msg to smarty variable and unset session variable
if(trim($_SESSION['msg']) != '') {
	$smarty->assign('msg', $_SESSION['msg']);
	unset($_SESSION['msg']);
}

## Set active class variable for left menu
$smarty->assign('activeclass', 'group');
$smarty->assign('mainmenu', '5');

## Unset all the objects created which are on this page
unset($catObj);
unset($groupObj);

$smarty->display(TEMPLATEDIR_ADMIN. 'controller/group/addGroup.tpl');
unset($smarty);
?>
