<?php

require_once '../model/group.php';


$groupObj = new Model_Group();
$groupID = base64_decode($_REQUEST['id']);

//echo "<pre>";print_r($groupID);exit;

$goldCnt = $groupObj->getLikeCountBygroupIDandLikeType($groupID,'1');
$silverCnt = $groupObj->getLikeCountBygroupIDandLikeType($groupID,'2');
$bronzeCnt = $groupObj->getLikeCountBygroupIDandLikeType($groupID,'3');

$total = $goldCnt + $silverCnt + $bronzeCnt;

$goldPercentage1 = $goldCnt * 100 / $total;
$silverPercentage1 = $silverCnt * 100 / $total;
$bronzePercentage1 = $bronzeCnt * 100 / $total;


$goldPercentage = round($goldPercentage1,2);
$silverPercentage = round ($silverPercentage1,2);
$bronzePercentage = round($bronzePercentage1,2);
?>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script src="http://code.highcharts.com/highcharts.js"></script>
<script src="http://code.highcharts.com/modules/exporting.js"></script>
<script type="text/javascript">
$(function () {
    var chart;
    $(document).ready(function() {
        chart = new Highcharts.Chart({
            chart: {
                renderTo: 'container',
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false
            },
            title: {
                text: 'Group Like'
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage}%</b>',
                percentageDecimals: 1
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        color: '#000000',
                        connectorColor: '#000000',
                        formatter: function() {
                            return '<b>'+ this.point.name +'</b>: '+ Highcharts.numberFormat(this.percentage, 2) +' %';
                        }
                    }
                }
            },
            series: [{
                type: 'pie',
                name: 'Total',
                data: [
                                      
                    ['Gold',<?php echo $goldPercentage; ?>],
                    ['Silver',<?php echo $silverPercentage; ?>],
                    ['Bronze',<?php echo $bronzePercentage; ?>]
 		]
            }]
        });
    });
    
});
</script>
<div id="container" style="min-width: 400px; height: 400px; margin: 0 auto"></div>
<?php
exit;
?>

