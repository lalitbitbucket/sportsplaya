<?php  
## include required files
/*******************************/
require_once '../model/group.php';
require_once '../model/users.php';
## Paginaton class
require_once ("../model/common/classes/pagination_class.php");
/*******************************/

## Create Objects
/*******************************/
$groupObj = new Model_Group();
$usersObj=new Model_Users();
/*******************************/
$memberId = base64_decode($_GET['memberId']);		

$groupId = base64_decode($_GET['groupId']);		

## Active, Inactive selected records
/*******************************/
/*## check user's permission
if($modules_new[5]['permission_status'] !=1 || $modules_new[5]['submod_28'] !=1) {
	header('location: '.SITE_URL.'/admin/home.php');
	exit;
}*/

## Get search parameters in variables - 
if($_REQUEST['search'] != '' && $_REQUEST['search'] != 'Search') {
	$searchindex = $_REQUEST['search'];
} else if($_REQUEST['search'] != '') {
	$searchindex = $_REQUEST['search'];
} else {
	$searchindex = '';
}
$searchindex=trim($searchindex);
// Assign search variable to show in search textbox
$smarty->assign('search', $searchindex);
if(isset($_GET['order_field']) && $_GET['order_field'] != '') {
	$orderField = $_GET['order_field'];
}
else {
	$orderField = 'groupname';
}
$smarty->assign('orderField', $orderField);

if(isset($_GET['order_by']) && $_GET['order_by'] != '') {
	$orderBy = $_GET['order_by'];
}
else {
	$orderBy = 'ASC';
}
$smarty->assign('orderBy', $orderBy);


//error_reporting(E_ALL);
if(isset($_POST['go']) != '' && isset($_POST['action']) != '') {
		$ids = implode(",", $_POST['checkall']);
		## check which action is selected
		$hidden_page = $_POST['hidden_pageno'];	
		if($_POST['action'] == 'active') {
			## Active selected records
			$groupObj->updateMultipleGroupStatus($ids, '2');
			$_SESSION['msg']="<div class='success_msg'><span>Group status activated successfully.</span></div>";
		} else if($_POST['action'] == 'inactive') {
			## Inactive selected records
			$groupObj->updateMultipleGroupStatus($ids, '1');
			$_SESSION['msg']="<div class='success_msg'><span>Group status deactivated successfully.</span></div>";
		} else {
			## Delete selected records
			$groupObj->deleteMultipleGroup($ids);
			$_SESSION['msg']="<div class='success_msg'><span>Group deleted successfully</span></div>";
		}
		header('location:'.SITE_URL.'/admin/home.php?q=group&page='.$hidden_page.'&order_by='.$orderBy.'&search='.$searchindex.'&order_field='.$orderField);
			exit;
		## Delete code will be here if required
		
} // if close

## Active/Inactive/Delete thourgh URL
if(isset($_GET['action']) == 'status' && isset($_GET['status']) != '') {
	$id = base64_decode($_GET['id']);		
	## Update training page status 
	$array = array();
	$array['status'] = ($_GET['status']=='1'?2:1);
	$groupObj->updateGroupStatus($id, $array['status']);
	if($_GET['status']==1) {
		$_SESSION['msg']="<div class='success_msg'><span>Group status activated successfully.</span></div>";
	} else {
		$_SESSION['msg']="<div class='success_msg'><span>Group status deactivated successfully.</span></div>";
	}
	header('location:'.SITE_URL.'/admin/home.php?q=group&page='.$_GET['page'].'&order_by='.$orderBy.'&search='.$searchindex.'&order_field='.$orderField);
	exit;
}

if(isset($_GET['action']) == 'delete' && isset($_GET['id']) != '') {
	$id = base64_decode($_GET['id']);
	## Delete Training
	$groupObj->deleteGroup($id);
	$_SESSION['msg']="<div class='success_msg'><span>Group deleted successfully</span></div>";
	header('location:'.SITE_URL.'/admin/home.php?q=group&page='.$_GET['page']);
	exit;
}

/*******************************/
/*if($_GET['q']=='group')
{
	$moduelArray=$moduleObj->getModuleHelpText(13);
	$smarty->assign('moduelArray', $moduelArray);
}*/

//echo "<pre>"; print_r($moduelArray);exit;
##Fetch all training pages from database


##Fetch all Training pages from database

##  --------- Pagination part first start --------------##
	if( isset( $_REQUEST['page'] ) && $_REQUEST['page'] != "" ) {
		$pageNum = $_REQUEST['page'];
	} else {
		$pageNum = 1;
	} 
	//echo $pageNum;exit;
	$rowsPerPage = ($_GET['setlimit']) ? $_GET['setlimit'] : ROW_PER_PAGE;
	
	$pageName = "home.php?q=group&order_by=".$orderBy."&search=".$searchindex."&order_field=".$orderField."&page=".$_GET['page']."&setlimit=".$_GET['setlimit']; 
	## Count all the records
	$groupArray = $groupObj->getAllGroupMemberByGroupId($groupId,$memberId,$searchindex,$orderField, $orderBy);
	$total_rows = count($groupArray);
	$offset	= ($pageNum - 1) * $rowsPerPage;
	$smarty->assign('page', $pageNum);
##  --------- Pagination part first end --------------##

##  --------- Pagination part second start --------------##
	$newid = $pageNum * $rowsPerPage - $rowsPerPage + 1;
	$smarty->assign('newid', $newid);
	$groupArray = $groupObj->getAllGroupMemberByGroupId($groupId,$memberId,$searchindex,$orderField, $orderBy,$rowsPerPage,$offset);

     $i=0;
	foreach($groupArray as $groupArr){
		## group members count
		$groupUserDetails = $usersObj->getProfileDetailsByUserProfileId($groupArr['memberId']);  
        $groupArray[$i]['name'] = $groupUserDetails['fname'].' '.$groupUserDetails['lname'];
        $groupArray[$i]['username'] = $groupUserDetails['username'];
        $groupArray[$i]['avtar'] = $groupUserDetails['avatar'];
        $groupArray[$i]['sex'] = $groupUserDetails['sex'];
		$groupArray[$i]['sportName'] = $groupUserDetails['sportName'];
		$i++;
	}
	//echo "<pre>"; print_r($groupArray);exit;
	$smarty->assign('groupArray', $groupArray);
	
	if($searchindex != '') {
		$other_id = "search=".$searchindex;
	} else {
		$other_id = '';
	}
	
	if($total_rows > ROW_PER_PAGE) {
		$pg = new pagination();					// CREATING OBJECT OF THE PAGINATION CLASS
		$pgnation = $pg->pagination_admin( $total_rows , $rowsPerPage , $pageNum , $pageName , $other_id, $class);		
		$smarty-> assign("pagination",$pgnation);
	}
##  --------- Pagination part second end --------------##	

## Assign session message to smarty variable and unset session variable
if($_SESSION['msg'] != '') {
	$smarty->assign('msg', $_SESSION['msg']);	
	unset($_SESSION['msg']);
}

## Set active class variable for left menu  (main menu and sub menu)
$smarty->assign('activeclass', 'group');
$smarty->assign('mainmenu', '5');

## Unset all the objects created which are on this page
unset($groupObj);
unset($moduleObj);

$smarty->display(TEMPLATEDIR_ADMIN. 'controller/group/groupMember.tpl');
unset($smarty);
?>
