<?php  
## include required files
/*******************************/
require_once '../model/group.php';
require_once '../model/users.php';

//require_once '../model/moduleuser.php';
## Paginaton class
require_once ("../model/common/classes/pagination_class.php");
/*******************************/

## Create Objects
/*******************************/
$groupObj = new Model_Group();
$usersObj      = new Model_Users();
/*******************************/


$groupId = $_REQUEST['groupId'];
$smarty->assign('groupId', $groupId);


$groupID = base64_decode($_REQUEST['groupId']);
$smarty->assign('groupID', $groupID);

//echo "<pre>";print_r($groupID);exit;

## Get search parameters in variables - 
if($_REQUEST['search'] != '' && $_REQUEST['search'] != 'Search') {
	$searchindex = $_REQUEST['search'];
} else if($_REQUEST['search'] != '') {
	$searchindex = $_REQUEST['search'];
} else {
	$searchindex = '';
}
$searchindex=trim($searchindex);
// Assign search variable to show in search textbox
$smarty->assign('search', $searchindex);
if(isset($_GET['order_field']) && $_GET['order_field'] != '') {
	$orderField = $_GET['order_field'];
}
else {
	$orderField = 'id';
}
$smarty->assign('orderField', $orderField);

if(isset($_GET['order_by']) && $_GET['order_by'] != '') {
	$orderBy = $_GET['order_by'];
}
else {
	$orderBy = 'ASC';
}
$smarty->assign('orderBy', $orderBy);

if(isset($_GET['action']) == 'delete' && isset($_GET['id']) != '') 

{
	$id = base64_decode($_GET['id']);
	$groupObj->deleteGroupLikeByID($id);
	$_SESSION['msg']="<div class='success_msg'><span>Group deleted successfully</span></div>";
	header('location:'.SITE_URL.'/admin/home.php?q=groupLike&groupId='.$_REQUEST['groupId	']);
	exit;
}



##Fetch all Training pages from database

##  --------- Pagination part first start --------------##
	if( isset( $_REQUEST['page'] ) && $_REQUEST['page'] != "" ) {
		$pageNum = $_REQUEST['page'];
	} else {
		$pageNum = 1;
	} 
	//echo $pageNum;exit;
	$rowsPerPage = ($_GET['setlimit']) ? $_GET['setlimit'] : ROW_PER_PAGE;
	
	$pageName = "home.php?q=groupLike&order_by=".$orderBy."&search=".$searchindex."&order_field=".$orderField."&page=".$_GET['page']."&setlimit=".$_GET['setlimit']; 
	## Count all the records
	$groupArray = $groupObj->getGroupLikesByGroupId($groupID,$searchindex,$orderField, $orderBy);
	$total_rows = count($groupArray);
	$offset	= ($pageNum - 1) * $rowsPerPage;
	$smarty->assign('page', $pageNum);
##  --------- Pagination part first end --------------##

##  --------- Pagination part second start --------------##
	$newid = $pageNum * $rowsPerPage - $rowsPerPage + 1;
	$smarty->assign('newid', $newid);
	$groupArray = $groupObj->getGroupLikesByGroupId($groupID,$searchindex,$orderField, $orderBy,$rowsPerPage,$offset);
	//echo "<pre>"; print_r($groupArray);exit;
         
	$i=0;
	foreach($groupArray as $groupArr){
		## group members count
	
		$userDetails = $usersObj->getUserDetailsInfoByUserId($groupArr['userProfileId']);
		$groupArray[$i]['fname'] = $userDetails['fname'];
		$groupArray[$i]['lname'] = $userDetails['lname'];
		$groupArray[$i]['email'] = $userDetails['email'];
		
		$i++;
	}
	$smarty->assign('groupArray', $groupArray);

	//echo "<pre>";print_r($groupArray);exit;

	if($searchindex != '') {
		$other_id = "search=".$searchindex;
	} else {
		$other_id = '';
	}
	
	if($total_rows > ROW_PER_PAGE) {
		$pg = new pagination();					// CREATING OBJECT OF THE PAGINATION CLASS
		$pgnation = $pg->pagination_admin( $total_rows , $rowsPerPage , $pageNum , $pageName , $other_id, $class);		
		$smarty-> assign("pagination",$pgnation);
	}
##  --------- Pagination part second end --------------##	

## Assign session message to smarty variable and unset session variable
if($_SESSION['msg'] != '') {
	$smarty->assign('msg', $_SESSION['msg']);	
	unset($_SESSION['msg']);
}

## Set active class variable for left menu  (main menu and sub menu)
$smarty->assign('activeclass', 'group');
$smarty->assign('mainmenu', '5');

## Unset all the objects created which are on this page
unset($groupObj);
unset($moduleObj);

$smarty->display(TEMPLATEDIR_ADMIN. 'controller/group/groupLike.tpl');
unset($smarty);
?>
