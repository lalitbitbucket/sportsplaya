<?php
require_once '../model/group.php';

$groupObj = new Model_Group();

if(isset($_REQUEST['id']) && $_REQUEST['id']!='')
{
	$id = base64_decode($_REQUEST['id']);
	$groupArray = $groupObj->getGroupDetailsById($id);
	$smarty->assign('groupArray',$groupArray);
}



$smarty->display(TEMPLATEDIR_ADMIN. 'controller/group/groupChart.tpl');
?>
