<?php 
## include required files
/*******************************/
require_once '../model/ads.php';
//require_once '../model/seo.php';
require_once '../model/common/image_functions.php';
/*******************************/

## Create Objects
/*******************************/
$adsObj = new Model_Ads();
//$seoObj = new Model_SEO();
/*******************************/

## get all cms menus
//$seoArray = $seoObj->getAllSeosList();
//$smarty->assign("seoArray",$seoArray);

## For page back pagination, searching and sorting
if($_REQUEST['page']){
	$page = $_REQUEST['page'];
	$smarty->assign('page',$page);
} else {
	$search = '';
}

if($_REQUEST['search']){
	$search = $_REQUEST['search'];
	$smarty->assign('search',$search);
} else {
	$search = '';
}

if($_REQUEST['order_field']){
	$order_field = $_REQUEST['order_field'];
	$smarty->assign('order_field',$order_field);
} else {
	$order_field = '';
}

if($_REQUEST['order_by']){
	$order_by = $_REQUEST['order_by'];
	$smarty->assign('order_by',$order_by);
} else {
	$order_by = '';
}


## Fetch ads details by id
if($_GET['id'] != '' && $_GET['action'] == 'edit') {
	$ads_id = base64_decode($_GET['id']);	
	$ads_details = $adsObj->getAdsDetailById($ads_id);
	$pages = explode(",",$ads_details['pages']);
	$smarty->assign('pages', $pages); 
	//echo "<pre>";print_r($pages);exit;	          
	$smarty->assign('ads_details', $ads_details);
}

if(isset($_POST) && !empty($_POST)) {
	## apply PHP validation for required filed
	
	//echo "<pre>";print_r($_POST);exit;
	if(trim($_POST['adsTitle']) != '' && trim($_POST['adsURL'])!='') {
		
	$adsArray = array();
	$adsArray['adsTitle'] 		= return_post_value($_POST['adsTitle']);
	$adsArray['adsURL'] 		= return_post_value($_POST['adsURL']);
	$adsArray['pages'] 	    	= implode(",",$_POST['pages']);


		if($_FILES['image']['name']!= '')

		{


		$imageName    = $_FILES['image']['name'];
		$fileName     = date('Ymdhis').".".$imageName;		
		$temp=$_FILES["image"]["tmp_name"];					
		$fileName      = time()."_".$_FILES['image']['name'];
		$imageFolder='../dynamicAssets/ads/';
		$imageName= $fileName;
		$adsArray['adsImage'] = $imageName;


		$thumbFolder1 ="../dynamicAssets/ads/160x600/";						
		$height1 = 600;
		$width1= 160;
		$file1=uploadImageAndCreateThumb($temp,$imageName,$imageFolder,$thumbFolder1,$height1,$width1);
		}


	$adsObj->editAdsValueById($adsArray,$ads_id);
	$_SESSION['msg'] = "<div class='success_msg'><span>Ads updated successfully</span></div>";
		header('location:'.SITE_URL.'/admin/home.php?q=manageads&page='.$page.'&search='.$search.'&order_field='.$order_field.'&order_by='.$order_by);
		exit();
}
	 else { 
		$_SESSION['msg'] = '<div class="error_msg"><span>Please enter ads title</span></div>';
	}
	// main if closed
}


$pagesArray = $adsObj->getAllActivePagesForAds();
$smarty->assign('pagesArray',$pagesArray);



## Assign success or error msg to smarty variable and unset session variable
if(trim($_SESSION['msg']) != '') {
	$smarty->assign('msg', $_SESSION['msg']);
	unset($_SESSION['msg']);
}

## Set active class variable for left menu
$smarty->assign('activeclass', 'manageads');
$smarty->assign('mainmenu', '2');
## Unset all the objects created which are on this page
unset($prodCatObj);

$smarty->display(TEMPLATEDIR_ADMIN. 'controller/ads/updateads.tpl');

unset($smarty);
?>
