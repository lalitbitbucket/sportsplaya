<?php

require_once '../model/ads.php';
/*******************************/

$maxDays=date('t'); 
  //echo date('m');

## Create Objects
/*******************************/
$adsObj = new Model_ads();

$i=1; 
while($i<=($maxDays))
{
   $date = date('Y').'-'.date('m').'-'.$i.'';
   $date1 = date('Y-m-d', strtotime("$date -1 days"));
   $date2 = date('Y-m-d', strtotime("$date +1 days")); 
  
   $adsdetails[$i] = $adsObj->getTotalCountOfDailyClickedAds($adsID,$date,$date2);
  
   $i++;
}

$topadsdetails = $adsObj->getAdsStatisticsByAdsId($adsID);
 //echo  "<pre>";print_r($adsdetails);exit;


?>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script src="<?php echo SITEURL;?>/siteAssets/charts/js/highcharts.js"></script>
<script src="<?php echo SITEURL;?>/siteAssets/charts/js/exporting.js"></script>
<script type="text/javascript">
$(function () {
    var chart;
    $(document).ready(function() {
        chart = new Highcharts.Chart({
            chart: {
                renderTo: 'container',
                type: 'line',
                marginRight: 130,
                marginBottom: 25
            },
            title: {
                text: 'Daily Statistics',
                x: -20 //center
            },
            subtitle: {
                text: '',
                x: -20
            },
            xAxis: {
                categories: [
                 <?php   $k=1;
                    while($k<=$maxDays)
                    {
                         if($k!='1')
                      {
                          echo',';
                      }
                         echo "'".$k."'";
                         $k++;
                    }
                  ?>  
                    ]
            },
            yAxis: {
                title: {
                    text: 'Total Clicks'
                },
                plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080'
                }]
            },
            tooltip: {
                formatter: function() {
                        return '<b>'+ this.series.name +'</b><br/>'+
                        this.x +': '+ this.y +'';
                }
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'top',
                x: -10,
                y: 100,
                borderWidth: 0
            },
            series: [{
                name: '<?php echo $topadsdetails['title'];?>',
                data: [<?php
                  $k=1;
                  while($k<=count($adsdetails))
                  {
                      if($k!='1')
                      {
                          echo',';
                      }
                      if($adsdetails[$k]['total']==0)
                      echo 0;
                      else
                      echo $adsdetails[$k]['total'];
                      $k++;
                  }
                
                ?>]
            }]
        });
    });
    
});
</script>
<div id="container" style="min-width: 400px; height: 400px; margin: 0 auto"></div>
