<?php 
## include required files
/*******************************/
require_once '../model/ads.php';
/*******************************/

## Create Objects
/*******************************/
$adsObj = new Model_ads();
/*******************************/

## Fetch admin user details by id
if($_GET['id'] != '') {
	$adsID = base64_decode($_GET['id']);
   // echo "<pre>";print_r($_POST);exit;	
   if($_GET['startTime']!='' && $_GET['endTime']!='' )
   {
       $stime =  $_GET['startTime'];
       $etime =  $_GET['endTime'];
   }
   $adsDetailsInfo = $adsObj->getAdsDetailById($adsID);
   $smarty->assign('adsDetailsInfo', $adsDetailsInfo);
   $adsdetails = $adsObj->getAdsStatisticsByAdsId($adsID,$stime,$etime);	
   $smarty->assign('adsArray', $adsdetails);
}
//print_r($adsdetails);

## Unset all the objects created which are on this page
unset($adsObj);

$smarty->display(TEMPLATEDIR_ADMIN. 'controller/ads/adstatistics.tpl');
unset($smarty);
?>
