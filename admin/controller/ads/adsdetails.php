<?php 
## include required files
/*******************************/
require_once '../model/ads.php';
/*******************************/

## Create Objects
/*******************************/
$adsObj = new Model_Ads();
/*******************************/

## For page back pagination, searching and sorting
if($_REQUEST['page']){
	$page = $_REQUEST['page'];
	$smarty->assign('page',$page);
} else {
	$search = '';
}
if($_REQUEST['search']){
	$search = $_REQUEST['search'];
	$smarty->assign('search',$search);
} else {
	$search = '';
}

if($_REQUEST['order_field']){
	$order_field = $_REQUEST['order_field'];
	$smarty->assign('order_field',$order_field);
} else {
	$order_field = '';
}

if($_REQUEST['order_by']){
	$order_by = $_REQUEST['order_by'];
	$smarty->assign('order_by',$order_by);
} else {
	$order_by = '';
}

## Fetch ads details by id
if($_GET['id'] != '') {
	$ads_id = base64_decode($_GET['id']);	
	$ads_details = $adsObj->getAdsDetailById($ads_id);
	$smarty->assign('ads_details', $ads_details);
}

## Set active class variable for left menu
$smarty->assign('activeclass', 'manageads');
$smarty->assign('mainmenu', '2');
## Unset all the objects created which are on this page
unset($adsObj);
$smarty->display(TEMPLATEDIR_ADMIN. 'controller/ads/adsdetails.tpl');
unset($smarty);
?>