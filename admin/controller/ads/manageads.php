<?php 
## include required files
require_once '../model/moduleuser.php';
require_once '../model/ads.php';
require_once "../model/common/classes/pagination_class.php";

## Create Objects
$moduleObj  = new Model_ModuleUser();
$adsObj = new Model_Ads();

//echo 'here';exit;

/*## check user's permission
if($modules_new[15]['perStatus'] !=1 || $modules_new[15]['submod_58'] !=1) {
	header('location: '.SITE_URL.'/admin/home.php');
	exit;
}*/

## Get search parameters in variables - 
if(isset($_REQUEST['search']) != '' && $_REQUEST['search'] != 'Search') {
	$searchindex = return_post_value($_REQUEST['search']);
} else {
	$searchindex = '';
}
$smarty->assign('search', return_fetched_value($searchindex));

if($_REQUEST['order_by']!='') {
	$order_by = $_REQUEST['order_by'];
} else {
	$order_by = 'ASC';
}
$smarty->assign('order_by',$order_by);

if($_REQUEST['order_field']!='') {
	$order_field = $_REQUEST['order_field'];
} else {
	$order_field = 'adsTitle';
}
$smarty->assign('order_field',$order_field);


//## session user id & type
//$user_id = $_SESSION['admin_id'];
//$user_type = $_SESSION["admin_type"];

##Fetch all admin users from database
if($_GET['q']=='manageads') {
	$moduelArray = $moduleObj->getModuleHelpText(11);
	$smarty->assign('moduelArray', $moduelArray);
}

## Active, Inactive selected records
/*******************************/
if(isset($_POST['go']) != '' && isset($_POST['action']) != '') {
		$ids = implode(",", $_POST['checkall']);
		## check which action is selected
		if(trim($ids) != '' ) {
			$hidden_page = $_POST['hidden_pageno'];	
			if($_POST['action'] == 'active') {
				## Active selected records
				$adsObj->updateAdsStatus($ids, '2');
				$_SESSION['msg']="<div class='success_msg'><span>Ads activated successfully.</span></div>";
			} else if($_POST['action'] == 'inactive') {
				## Inactive selected records
				$adsObj->updateAdsStatus($ids, '1');
				$_SESSION['msg']="<div class='success_msg'><span>Ads deactivated successfully.</span></div>";
			} else {
				## Delete selected records
				$adsObj->updateAdsStatus($ids, '0');
				$_SESSION['msg']="<div class='success_msg'><span>Ads deleted successfully.</span></div>";
			}
				header('location:'.SITE_URL.'/admin/home.php?q=manageads&order_field='.$order_field.'&order_by='.$order_by.'&search='.$searchindex.'&page='.$hidden_page);
				exit;
		}
		
}

## Active/Inactive ads Status by id
if(isset($_GET['action']) == 'status' && isset($_GET['status']) != '') {
	$id = base64_decode($_GET['id']);
	if($_GET['status']==1){
		## Update ads status	
		$array = array();
		$array['status'] = ($_GET['status']=='1'?2:1);
		$adsObj->editAdsValueById($array, $id);
			
		$_SESSION['msg']="<div class='success_msg'><span>Ads activated successfully.</span></div>";
		header('location:'.SITE_URL.'/admin/home.php?q=manageads&search='.$searchindex.'&order_field='.$order_field.'&order_by='.$order_by.'&page='.$_GET['page']);
		exit;
	} 
	if($_GET['status']==2)
	{
		## Update ads status
		$array = array();
		$array['status'] = ($_GET['status']=='2'?1:2);
		
		$adsObj->editAdsValueById($array, $id);
		$_SESSION['msg']="<div class='success_msg'><span>Ads deactivated successfully.</span></div>";
		header('location:'.SITE_URL.'/admin/home.php?q=manageads&search='.$searchindex.'&order_field='.$order_field.'&order_by='.$order_by.'&page='.$_GET['page']);
		exit;
	}
}

## Delete ads by id
if(isset($_GET['action']) == 'delete' && isset($_GET['id']) != '') 
{
	$id = base64_decode($_GET['id']);
	
		$array = array();
		$array['status'] = '0';
		
		$adsObj->editAdsValueById($array, $id);	
	$_SESSION['msg']="<div class='success_msg'><span>Ads deleted successfully.</span></div>";
	header('location:'.SITE_URL.'/admin/home.php?q=manageads&search='.$searchindex.'&order_field='.$order_field.'&order_by='.$order_by.'&page='.$_GET['page']);
	exit;
}

## --------- Pagination part first start --------------##
if( isset( $_GET[ 'page' ] ) && $_GET[ 'page' ] != "" ) {
		$pageNum = $_GET[ 'page' ];
} else {
		$pageNum = 1;
} 
	$rowsPerPage = ROW_PER_PAGE;
	$pageName = "home.php?q=manageads&order_field=".$order_field."&order_by=".$order_by."&search=".$searchindex;
	## Count all the records
	$adsArray = $adsObj->getAllAdsForAds($searchindex,$order_field,$order_by);
    
	$total_rows = count($adsArray);
	$offset	= ($pageNum - 1) * $rowsPerPage;
	$smarty->assign('page', $pageNum);
##--------- Pagination part first end --------------##


##--------- Pagination part second start --------------##
	$newid = $pageNum * $rowsPerPage - $rowsPerPage + 1;
	$smarty->assign('newid', $newid);
	$adsArray = $adsObj->getAllAdsForAds($searchindex,$order_field,$order_by,$rowsPerPage,$offset);
	$smarty->assign('adsArray', $adsArray);

	$other_id = '';

if($total_rows > ROW_PER_PAGE) {
	$pg = new pagination();
	$pgnation = $pg->pagination_admin( $total_rows , $rowsPerPage , $pageNum , $pageName , $other_id, $class);	
	$smarty-> assign("pagination",$pgnation);
}
##  --------- Pagination part second end --------------##	

## Assign session message to smarty variable and unset session variable
if(isset($_SESSION['msg']) &&  $_SESSION['msg']!= '') {
	$smarty->assign('msg', $_SESSION['msg']);	
	unset($_SESSION['msg']);
}

## Set active class variable for left menu  (main menu and sub menu)
$smarty->assign('activeclass', 'manageads');
$smarty->assign('mainmenu', '2');

## Unset all the objects created which are on this page
unset($adsObj);
unset($moduleObj);

$smarty->display(TEMPLATEDIR_ADMIN. 'controller/ads/manageads.tpl');
unset($smarty);
?>
