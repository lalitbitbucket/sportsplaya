<?php 
## include required files
/*******************************/
require_once '../model/ads.php';
//require_once '../model/seo.php';
require_once "../model/common/classes/pagination_class.php";
require_once '../model/common/image_functions.php';
/*******************************/

## Create Objects
/*******************************/
$adsObj = new Model_Ads();
//$seoObj = new Model_SEO();
/*******************************/

## get all cms menus
//$seoArray = $seoObj->getAllSeosList();
//$smarty->assign("seoArray",$seoArray);


## For page back pagination, searching and sorting
if($_REQUEST['page']){
	$page = $_REQUEST['page'];
	$smarty->assign('page',$page);
} else {
	$search = '';
}
if($_REQUEST['search']){
	$search = $_REQUEST['search'];
	$smarty->assign('search',$search);
} else {
	$search = '';
}

if($_REQUEST['order_field']){
	$order_field = $_REQUEST['order_field'];
	$smarty->assign('order_field',$order_field);
} else {
	$order_field = '';
}

if($_REQUEST['order_by']){
	$order_by = $_REQUEST['order_by'];
	$smarty->assign('order_by',$order_by);
} else {
	$order_by = '';
}

if(count($_POST)>0) 
{
## apply PHP validation for required filed

if(trim($_POST['ads_title']) != ''  && trim($_POST['adsURL'])!='') 
			{
			$adsArray = array();
			$adsArray['adsTitle'] 		= return_post_value($_POST['ads_title']);
			$adsArray['adsURL'] 		= return_post_value($_POST['adsURL']);
			$adsArray['pages'] 	    	= implode(",",$_POST['pages']);
			$adsArray['status'] 	    	= 2;

			
			if($_FILES['image']['name']!= '')

			{

			
			$imageName    = $_FILES['image']['name'];
			$fileName     = date('Ymdhis').".".$imageName;		
			$temp=$_FILES["image"]["tmp_name"];					
			$fileName      = time()."_".$_FILES['image']['name'];
			$imageFolder='../dynamicAssets/ads/';
			$imageName= $fileName;
			$adsArray['adsImage'] = $fileName;	


			$thumbFolder1 ="../dynamicAssets/ads/160x600/";						
			$height1 = 600;
			$width1= 160;
			$file1=uploadImageAndCreateThumb($temp,$imageName,$imageFolder,$thumbFolder1,$height1,$width1);

		
			}

			
			
			$adsId = $adsObj->addNewAds($adsArray);
			$_SESSION['msg'] = "<div class='success_msg'><span>Ads added successfully</span></div>";
			header('location:'.SITE_URL.'/admin/home.php?q=manageads&page='.$page.'&search='.$search.'&order_field='.$order_field.'&order_by='.$order_by);
			exit;
			}
	 else { 
		$_SESSION['msg'] = '<div class="error_msg"><span>Please fill all required fields</span></div>';
	}
	// main if closed
}

## Assign success or error msg to smarty variable and unset session variable
if(trim($_SESSION['msg']) != '') {
	$smarty->assign('msg', $_SESSION['msg']);
	unset($_SESSION['msg']);
}

$pagesArray = $adsObj->getAllActivePagesForAds();
$smarty->assign('pagesArray',$pagesArray);

//echo "<pre>";print_r($pagesArray);exit;
   

## Set active class variable for left menu
$smarty->assign('activeclass', 'manageads');
$smarty->assign('mainmenu', '2');
## Unset all the objects created which are on this page
unset($adsObj);

$smarty->display(TEMPLATEDIR_ADMIN. 'controller/ads/addnewads.tpl');
unset($smarty);
?>
