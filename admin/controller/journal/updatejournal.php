<?php 
## include required files
/*******************************/
require_once '../model/journal.php';
/*******************************/

## Create Objects
/*******************************/
$journalObj = new Model_journal();
/*******************************/

## For page back pagination, searching and sorting
if($_REQUEST['page']){
	$page = $_REQUEST['page'];
	$smarty->assign('page',$page);
} else {
	$search = '';
}

if($_REQUEST['search']){
	$search = $_REQUEST['search'];
	$smarty->assign('search',$search);
} else {
	$search = '';
}

if($_REQUEST['order_by']){
	$order_by = $_REQUEST['order_by'];
	$smarty->assign('order_by',$order_by);
} else {
	$order_by = '';
}

if(isset($_GET['order_field']) && $_GET['order_field'] != '') {
	$orderField = $_GET['order_field'];
}
else {
	$orderField = 'jnote';
}
$smarty->assign('orderField', $orderField);
if(isset($_POST) && !empty($_POST)) {
	## apply PHP validation for required filed
	if(trim($_POST['jnote']) != '') {
		## check feature already exists
		
	
			$journalArray = array();

			$journalArray['jnote'] 	= stripslashes(trim($_POST['jnote']));
			$journalArray['jDisc'] 	= stripslashes(trim($_POST['jDisc']));
	
		## update features details
		$journalObj->editJournalValueById($journalArray,$journal_id);
		$_SESSION['msg'] = "<div class='success_msg'><span>Journal updated successfully</span></div>";
		header('location:'.SITE_URL.'/admin/home.php?q=managejournal&page='.$page.'&search='.$search.'&order_by='.$order_by);
		exit;
	} else { 
		$_SESSION['msg'] = '<div class="error_msg"><span>Please enter Journal Title</span></div>';
	}
	// main if closed
}

//echo "Hello"; exit;

## Fetch admin user details by id
if($_GET['id'] != '' && $_GET['action'] == 'edit') {
	$journal_id = base64_decode($_GET['id']);	
	$journal_details = $journalObj->getJournalDetailsById($journal_id);
	$smarty->assign('journal_details', $journal_details);
}
## Assign success or error msg to smarty variable and unset session variable
if(trim($_SESSION['msg']) != '') {
	$smarty->assign('msg', $_SESSION['msg']);
	unset($_SESSION['msg']);
}

## Set active class variable for left menu
$smarty->assign('activeclass', 'managejournal');
$smarty->assign('mainmenu', '2');
## Unset all the objects created which are on this page
unset($journalObj);

$smarty->display(TEMPLATEDIR_ADMIN. 'controller/journal/updatejournal.tpl');
unset($smarty);
?>
