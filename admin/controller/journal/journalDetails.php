<?php 
## include required files
/*******************************/
require_once '../model/journal.php';
/*******************************/

## Create Objects
/*******************************/
$journalObj = new Model_journal();
/*******************************/


## Journal admin user details by id
if($_GET['id'] != '' && $_GET['action'] == 'edit') {
	$journal_id = base64_decode($_GET['id']);	
	$journal_details = $journalObj->getJournalDetailsById($journal_id);
	$smarty->assign('journal_details', $journal_details);
}
## Assign success or error msg to smarty variable and unset session variable
if(trim($_SESSION['msg']) != '') {
	$smarty->assign('msg', $_SESSION['msg']);
	unset($_SESSION['msg']);
}

## Set active class variable for left menu
$smarty->assign('activeclass', 'managejournal');
$smarty->assign('mainmenu', '2');
## Unset all the objects created which are on this page
unset($journalObj);

$smarty->display(TEMPLATEDIR_ADMIN. 'controller/journal/journalDetails.tpl');
unset($smarty);
?>
