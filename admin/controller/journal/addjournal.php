<?php
## include required files
/*******************************/
require_once '../model/journal.php';
/*******************************/

## Create Objects
/*******************************/
$journalObj = new Model_journal();

/*******************************/

## For page back pagination, searching and sorting
if($_REQUEST['page']){
	$page = $_REQUEST['page'];
	$smarty->assign('page',$page);
} else {
	$search = '';
}
if($_REQUEST['search']){
	$search = $_REQUEST['search'];
	$smarty->assign('search',$search);
} else {
	$search = '';
}
if($_REQUEST['order_by']){
	$order_by = $_REQUEST['order_by'];
	$smarty->assign('order_by',$order_by);
} else {
	$order_by = '';
}


if(isset($_POST) && !empty($_POST)) {
	## apply PHP validation for required filed
	if(trim($_POST['jnote']) != '') {
		## check feature already exists
		
	
			$journalArray = array();
		
			$journalArray['jnote'] 	= stripslashes(trim($_POST['jnote']));
			$journalArray['jDisc'] 	= stripslashes(trim($_POST['jDisc']));
			$journalArray['userId'] 	= 1;	
			$journalArray['status'] 	= 2;
			## add product features
			$journalObj->addJournal($journalArray);
			$_SESSION['msg'] = "<div class='success_msg'><span>Journal added successfully</span></div>";
			header('location:'.SITE_URL.'/admin/home.php?q=managejournal');
			exit;
		 
	} else { 
		$_SESSION['msg'] = '<div class="error_msg"><span>Please fill all required fields</span></div>';
	}
	// main if closed
}

## Assign success or error msg to smarty variable and unset session variable
if(trim($_SESSION['msg']) != '') {
	$smarty->assign('msg', $_SESSION['msg']);
	unset($_SESSION['msg']);
}

## Set active class variable for left menu
$smarty->assign('activeclass', 'managejournal');
$smarty->assign('mainmenu', '2');
## Unset all the objects created which are on this page
unset($featObj);

$smarty->display(TEMPLATEDIR_ADMIN. 'controller/journal/addjournal.tpl');
unset($smarty);
?>
