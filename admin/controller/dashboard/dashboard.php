<?php
## include required files
/*******************************/
require_once '../model/users.php';
/*******************************/
## Create Objects
/*******************************/
$userObj = new Model_Users();

//Sport Person
$userArray = $userObj->getRecentRegisterdUsersByType(3);
$i=0;
foreach($userArray as $userinfo){
$userprofileinfo = $userObj->getProfileDetailsByUserId($userinfo['userId']);
//echo "<xmp>"; print_r($userprofileinfo); echo "</xmp>"; 
$userArray[$i]['name'] = $userprofileinfo['fname'].' '.$userprofileinfo['lname'];
$i++;
}
$smarty->assign('userArray',$userArray);

//Fan
$fanArray = $userObj->getRecentRegisterdUsersByType(4);
$i=0;
foreach($fanArray as $userinfo){
$userprofileinfo = $userObj->getProfileDetailsByUserId($userinfo['userId']);
//echo "<xmp>"; print_r($userprofileinfo); echo "</xmp>"; 
$fanArray[$i]['name'] = $userprofileinfo['fname'].' '.$userprofileinfo['lname'];
$i++;
}
$smarty->assign('fanArray',$fanArray);

//Coach
$coachArray = $userObj->getRecentRegisterdUsersByType(5);
$i=0;
foreach($coachArray as $userinfo){
$userprofileinfo = $userObj->getProfileDetailsByUserId($userinfo['userId']);
//echo "<xmp>"; print_r($userprofileinfo); echo "</xmp>"; 
$coachArray[$i]['name'] = $userprofileinfo['fname'].' '.$userprofileinfo['lname'];
$i++;
}
$smarty->assign('coachArray',$coachArray);

//Club
$clubArray = $userObj->getRecentRegisterdUsersByType(6);
$i=0;
foreach($clubArray as $userinfo){
$userprofileinfo = $userObj->getProfileDetailsByUserId($userinfo['userId']);
//echo "<xmp>"; print_r($userprofileinfo); echo "</xmp>"; 
$clubArray[$i]['name'] = $userprofileinfo['fname'].' '.$userprofileinfo['lname'];
$i++;
}
$smarty->assign('clubArray',$clubArray);

unset($userObj);
$smarty->display(TEMPLATEDIR_ADMIN. 'controller/dashboard/dashboard.tpl');
unset($smarty);
?>
