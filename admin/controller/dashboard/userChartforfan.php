<?php
require_once '../model/users.php';
$userObj = new Model_Users();

$months=array("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sept","Oct","Nov","Dec");

$monthrange=range(1,12);

$kk=0;
while($kk<count($monthrange))
{
	$startDay="1";
	$startMonth=$monthrange[$kk];
	if($startMonth<10)
		$startMonth='0'.$startMonth;
		
	$stratYear=date("Y");
 	$date=$stratYear."-".$startMonth."-".$startDay;
	$from[$kk] = date("Y-m-d",strtotime(date("Y-m-d", strtotime($date)) . " -1 day"));	
	$date1 = date("Y-m-d",strtotime(date("Y-m-d", strtotime($date)) . " +1 month"));	
	$to[$kk]=$date1;
	$from1=$from[$kk];
	$to1=$to[$kk];
	$userDetails[$kk]=$userObj->getAllFanUsersForChart($from1,$to1);
	$kk++;
}
//echo "<pre>";print_r($userDetails);exit;

?>
<script type="text/javascript" src="<?php echo SITE_URL;?>/siteAssets/js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="<?php echo SITE_URL;?>/admin/siteAssets/js/jquery-ui-1.8.17.custom.min.js"></script>
<script src="<?php echo SITE_URL;?>/siteAssets/charts/js/highcharts.js"></script>
<script src="<?php echo SITE_URL;?>/siteAssets/charts/js/exporting.js"></script>

<div id="container1" style="min-width: 400px; height: 400px; margin: 0 auto"></div>

<script type="text/javascript">
$(function () {
    var chart;
    $(document).ready(function() {
    
        var colors = Highcharts.getOptions().colors,
            categories = [<?php
		$k=0;
		while($k<count($months))
		{
			if($k!=0)
			{
				echo ',';
			}
		?><?php echo "'".$months[$k]."'";?><?php
			$k++;
	}
	
	?>],
            name = 'Browser Users',
            data = [
		<?php
		$k=0;
		while($k<count($months))
		{
			if($k!=0)
			{
				echo ',';
			}
		?>	
			
			{
                    y: <?php  if($userDetails[$k]['total']<1){ echo '0';}else { echo $userDetails[$k]['total']; }?>,
                    color: colors[0],
                    drilldown: {
                        name: '<?php echo $months[$k];?>',
                        categories: ['MSIE 6.0', 'MSIE 7.0', 'MSIE 8.0', 'MSIE 9.0'],
                        data: [<?php  echo $userDetails[$k]['total'];?>],
                        color: colors[0]
                    }
                }
			<?php
			$k++;
	}
	
	?>
				];
    
        function setChart(name, categories, data, color) {
            chart.xAxis[0].setCategories(categories, false);
            chart.series[0].remove(false);
            chart.addSeries({
                name: name,
                data: data,
                color: color || 'white'
            }, false);
            chart.redraw();
        }
    
        chart = new Highcharts.Chart({
            chart: {
                renderTo: 'container1',
                type: 'column'
            },
            title: {
                text: 'Total users on this year <?php  echo date("Y");?> '
            },
            subtitle: {
                text: 'Click the columns to view no of users. Click again to view no of users.'
            },
            xAxis: {
                categories: categories
            },
            yAxis: {
                title: {
                    text: 'Total no of users'
                }
            },
            plotOptions: {
                column: {
                    cursor: 'pointer',
                    point: {
                        events: {
                            click: function() {
                                var drilldown = this.drilldown;
                                if (drilldown) { // drill down
                                    setChart(drilldown.name, drilldown.categories, drilldown.data, drilldown.color);
                                } else { // restore
                                    setChart(name, categories, data);
                                }
                            }
                        }
                    },
                    dataLabels: {
                        enabled: true,
                        color: colors[0],
                        style: {
                            fontWeight: 'bold'
                        },
                        formatter: function() {
                            return this.y +'%';
                        }
                    }
                }
            },
            tooltip: {
                formatter: function() {
                    var point = this.point,
                        s = this.x +':<b>'+ this.y +'% no of users</b><br/>';
                    if (point.drilldown) {
                        s += 'Click to view no of '+ point.category +' users';
                    } else {
                        s += 'Click to return to browser users';
                    }
                    return s;
                }
            },
            series: [{
                name: name,
                data: data,
                color: 'white'
            }],
            exporting: {
                enabled: false
            }
        });
    });
    
});
		</script>




