<?php
require_once '../model/blogs.php';
require_once '../model/moduleuser.php';
require_once "../model/common/classes/pagination_class.php";
require_once '../model/users.php';
## Initializing Objects
/**************************/
$blogsObj = new Model_Blogs();
$moduleObj = new Model_ModuleUser();
$userObj = new Model_Users();
/**************************/
if(isset($_REQUEST['id']) && $_REQUEST['id']!='')
{
$id = base64_decode($_REQUEST['id']);
$blogsArray = $blogsObj->getBlogsDetailsById($id);
$smarty->assign('blogsArray',$blogsArray);
}

if($_SESSION['msg']!='')
{
$smarty->assign('msg',$_SESSION['msg']);
unset($_SESSION['msg']);
}

$smarty->assign('activeclass','siteusers');
$smarty->assign('mainmenu', '3');
	
## Unset all objects which are on this page	
	unset($moduleObj);
	unset($userObj);
	$smarty->display(TEMPLATEDIR_ADMIN. 'controller/blogs/blogDetails.tpl');
	unset($smarty);
?>
