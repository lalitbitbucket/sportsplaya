<?php 
## include required files
/*******************************/
require_once '../model/blogs.php';
require_once '../model/common/image_functions.php';
/*******************************/
## Create Objects
/*******************************/
$blogsObj = new Model_Blogs();
/*******************************/
$user_id= $admin_id;
## Get search parameters in variables - 
if($_POST['search'] != '' && $_POST['search'] != 'Search') {
	$searchindex = ucfirst($_POST['search']);
} else if($_GET['search'] != '') {
	$searchindex = $_GET['search'];
} else {
	$searchindex = '';
}
$searchindex=trim($searchindex);
// Assign search variable to show in search textbox
$smarty->assign('search', $searchindex);
if(isset($_GET['order_field']) && $_GET['order_field'] != '') {
	$orderField = $_GET['order_field'];
}
else {
	$orderField = 'title';
}
$smarty->assign('orderField', $orderField);

if(isset($_GET['order_by']) && $_GET['order_by'] != '') {
	$orderBy = $_GET['order_by'];
}
else {
	$orderBy = 'ASC';
}
$smarty->assign('orderBy', $orderBy);

if(isset($_POST['addblogs_btn'])) {
## apply PHP validation for required filed
	if(trim($_POST['title']) != '' && trim($_POST['detail']) != '') {
		$blogAddArray = array();
		if($_FILES['image']['name']){
			## upload file
			$fileName      = time()."_".$_FILES['image']['name'];
			$temp=$_FILES["image"]["tmp_name"];	
			$imageFolder='../dynamicAssets/blogs/';
			copy($_FILES["image"]["tmp_name"],$imageFolder.$fileName);
			$blogAddArray['image'] = $fileName;
			
			## creating thumb images
			$imageName= $fileName;
			$thumbFolder1 ="../dynamicAssets/blogs/160x140/";						
			$height1 = 140;
			$width1= 160;
			$file1=uploadImageAndCreateThumb($temp,$imageName,$imageFolder,$thumbFolder1,$height1,$width1);
			
			$thumbFolder3 ="../dynamicAssets/blogs/200x200/";						
			$height3 = 200;
			$width3= 200;
			$file3=uploadImageAndCreateThumb($temp,$imageName,$imageFolder,$thumbFolder3,$height3,$width3);
			
			$thumbFolder2 ="../dynamicAssets/blogs/308x350/";						
			$height2 = 350;
			$width2= 308;
			$file2=uploadImageAndCreateThumb($temp,$imageName,$imageFolder,$thumbFolder2,$height2,$width2);
		}
			
		$blogAddArray['detail'] = return_post_value($_POST['detail']);
		$blogAddArray['title'] 		= return_post_value($_POST['title']);
		$blogAddArray['uID'] 		= $user_id;
		$blogAddArray['addedDate']  =  date('Y-m-d H:i:s');
		$blogAddArray['status']      = '2';
		$blogsObj->addBlogsByValue($blogAddArray);
		$_SESSION['msg'] = "<div class='success_msg'><span>Blog added successfully</span></div>";
		header('location:'.SITE_URL.'/admin/home.php?q=blogs&page='.$_POST['page']);
		exit();
	}
	else {
		$_SESSION['msg'] = '<div class="error_msg"><span>Please fill all the required fields </span></div>';
	}
}

## Assign success or error msg to smarty variable and unset session variable
if(trim($_SESSION['msg']) != '') {
	$smarty->assign('msg', $_SESSION['msg']);
	unset($_SESSION['msg']);
}

## Set active class variable for left menu
$smarty->assign('activeclass', 'blogs');
$smarty->assign('mainmenu', '5');

## Unset all the objects created which are on this page
unset($blogsObj);
$smarty->display(TEMPLATEDIR_ADMIN. 'controller/blogs/addBlogs.tpl');
unset($smarty);
?>
