<?php
require_once '../model/blogs.php';
require_once '../model/moduleuser.php';
require_once "../model/common/classes/pagination_class.php";
require_once '../model/users.php';
## Initializing Objects
/**************************/
$blogsObj = new Model_Blogs();
$moduleObj = new Model_ModuleUser();
$userObj = new Model_Users();
/**************************/
$blogID = base64_decode($_GET['blogID']);	
	
if($blogID!='')
{
   $blogsArray = $blogsObj->getBlogsDetailsById($blogID);
   $smarty->assign('blogsArray',$blogsArray);
}

$commentArray= $blogsObj->getcommentDetailsByBlogId(base64_decode($_GET['blogID']));
$userCnt=0;
foreach($commentArray as $cmtArr)
{
	$userDtl=$userObj->getUserProfileDetailsByUserIdAtAdminSide($cmtArr['uID']);
	$commentArray[$userCnt]['uName'] =  $userDtl['fname']." ".$userDtl['lname'];
	$commentArray[$userCnt]['avatar'] =  $userDtl['avatar'];
	$userCnt++;	
}
$smarty->assign('commentArray',$commentArray);
//echo"<pre>"; print_r($commentArray); exit;
if(isset($_POST['Send']) && $_POST['Send']=='Send') {		
		if(trim($_POST['answer'])!='') {
		$replyArray = array();
		$replyArray['blogID'] = $blogID;
		$replyArray['detail']		   = return_post_value($_POST['answer']);
		$replyArray['uID']			 =$admin_id;		
		$replyArray['addedDate'] = date('Y-m-d H:i:s');
		$replyArray['status'] = '2';			
		$blogsObj->addAnswer($replyArray); 
		$_SESSION['msg'] = '<section class="success_msg">Comment successfully posted.</section>';
		header('location:'.SITE_URL.'/admin/home.php?q=blogs&blogID='.$_GET['blogID']);
		exit();
		//echo "<xmp>"; print_r($replyArray); echo "</xmp>";
	} else {
		$_SESSION['msg'] = '<section class="error_msg">Please enter Comment detail.</section>';
	}
}

if(isset($_GET['action']) == 'delete' && isset($_GET['blogCommentID']) != '') {
	$id = base64_decode($_GET['blogCommentID']);
	## Delete comment
	$blogsObj->deleteComment($id);
	$_SESSION['deletemsg']="<div class='success_msg'><span>Comment deleted successfully</span></div>";
	header('location:'.SITE_URL.'/admin/home.php?q=commentDetails&blogID='.$_GET['blogID']);
	exit;
}
if($_SESSION['msg']!='')
	{
		$smarty->assign('msg',$_SESSION['msg']);
		unset($_SESSION['msg']);
	}
	
if($_SESSION['deletemsg']!='')
	{
		$smarty->assign('deletemsg',$_SESSION['deletemsg']);
		unset($_SESSION['deletemsg']);
	}
	
	$smarty->assign('activeclass','blogs');
	$smarty->assign('mainmenu', '3');
	
## Unset all objects which are on this page	
	unset($moduleObj);
	unset($userObj);
	$smarty->display(TEMPLATEDIR_ADMIN. 'controller/blogs/commentDetails.tpl');
	unset($smarty);
?>
