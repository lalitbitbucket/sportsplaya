<?php  
## include required files
/*******************************/
require_once '../model/blogs.php';
require_once '../model/moduleuser.php';
require_once '../model/users.php';
## Paginaton class
require_once ("../model/common/classes/pagination_class.php");
/*******************************/

## Create Objects
/*******************************/
$blogsObj = new Model_Blogs();
$moduleObj=new Model_ModuleUser();
$userObj = new Model_Users();
/*******************************/
//echo ROW_PER_PAGE;
## Active, Inactive selected records
/*******************************/
## check user's permission
//if($modules_new[5]['perStatus'] !=1 || $modules_new[5]['submod_29'] !=1) {
//	header('location: '.SITE_URL.'/admin/home.php');
//	exit;
//}

## Get search parameters in variables - 
if($_POST['search'] != '' && $_POST['search'] != 'Search') {
	$searchindex = $_POST['search'];
} else if($_GET['search'] != '') {
	$searchindex = $_GET['search'];
} else {
	$searchindex = '';
}
$searchindex=trim($searchindex);
// Assign search variable to show in search textbox
$smarty->assign('search', $searchindex);
if(isset($_GET['order_field']) && $_GET['order_field'] != '') {
	$orderField = $_GET['order_field'];
}
else {
	$orderField = 'title';
}
$smarty->assign('orderField', $orderField);

if(isset($_GET['order_by']) && $_GET['order_by'] != '') {
	$orderBy = $_GET['order_by'];
}
else {
	$orderBy = 'ASC';
}
$smarty->assign('orderBy', $orderBy);

//error_reporting(E_ALL);
if(isset($_POST['go']) != '' && isset($_POST['action']) != '') {
		$ids = implode(",", $_POST['checkall']);
		## check which action is selected
		$hidden_page = $_POST['hidden_pageno'];	
		if($_POST['action'] == 'active') {
			## Active selected records
			$blogsObj->updateMultipleBlogsStatus($ids, '2');
			$_SESSION['msg']="<div class='success_msg'><span>Blogs status activated successfully.</span></div>";
		} else if($_POST['action'] == 'inactive') {
			## Inactive selected records
			$blogsObj->updateMultipleBlogsStatus($ids, '1');
			$_SESSION['msg']="<div class='success_msg'><span>Blogs status deactivated successfully.</span></div>";
		} else {
			## Delete selected records
			$blogsObj->deleteMultipleBlogs($ids);
			$_SESSION['msg']="<div class='success_msg'><span>Blog deleted successfully</span></div>";
		}
		header('location:'.SITE_URL.'/admin/home.php?q=blogs&page='.$hidden_page.'&order_by='.$orderBy.'&search='.$searchindex.'&order_field='.$orderField);
			exit;
		## Delete code will be here if required
		
} // if close

## Active/Inactive/Delete thourgh URL
if(isset($_GET['action']) == 'status' && isset($_GET['status']) != '') {
	$id = base64_decode($_GET['id']);		
	## Update blogs page status 
	$array = array();
	$array['status'] = ($_GET['status']=='1'?2:1);
	$blogsObj->updateBlogsStatus($id, $array['status']);
	if($_GET['status']==1) {
		$_SESSION['msg']="<div class='success_msg'><span>Blogs status activated successfully.</span></div>";
	} else {
		$_SESSION['msg']="<div class='success_msg'><span>Blogs status deactivated successfully.</span></div>";
	}
	header('location:'.SITE_URL.'/admin/home.php?q=blogs&page='.$_GET['page'].'&order_by='.$orderBy.'&search='.$searchindex.'&order_field='.$orderField);
	exit;
}

if(isset($_GET['action']) == 'delete' && isset($_GET['id']) != '') {
	$id = base64_decode($_GET['id']);
	## Delete blogs
	$blogsObj->deleteBlogs($id);
	$_SESSION['msg']="<div class='success_msg'><span>Blogs deleted successfully</span></div>";
	header('location:'.SITE_URL.'/admin/home.php?q=blogs&page='.$_GET['page']);
	exit;
}

/*******************************/
if($_GET['q']=='blogs')
{
	$moduelArray=$moduleObj->getModuleHelpText(13);
	$smarty->assign('moduelArray', $moduelArray);
}

##Fetch all blogs pages from database
##  --------- Pagination part first start --------------##
	if( isset( $_GET[ 'page' ] ) && $_GET[ 'page' ] != "" ) {
		$pageNum = $_GET[ 'page' ];
	} else {
		$pageNum = 1;
	} 
	$rowsPerPage = ($_GET['setlimit']) ? $_GET['setlimit'] : ROW_PER_PAGE;	
	$pageName = "home.php?q=blogs&order_by=".$orderBy."&search=".$searchindex."&order_field=".$orderField."&page=".$_GET['page']."&setlimit=".$_GET['setlimit']; 
	## Count all the records
	$blogArray = $blogsObj->getAllBlogs($searchindex,$orderField, $orderBy);
	$total_rows = count($blogArray);
	$offset	= ($pageNum - 1) * $rowsPerPage;
	$smarty->assign('page', $pageNum);
##  --------- Pagination part first end --------------##

##  --------- Pagination part second start --------------##
	$newid = $pageNum * $rowsPerPage - $rowsPerPage + 1;
	$smarty->assign('newid', $newid);
	$blogArray = $blogsObj->getAllBlogs($searchindex,$orderField, $orderBy,$rowsPerPage,$offset);
	//echo "<pre>"; print_r($blogArray);exit;
	$userCnt = 0;
	foreach($blogArray as $blogArr){
		$userDtl = $userObj->getUserDetailsInfoByUserId($blogArr['uID']);
		//echo "<pre>"; print_r($userDtl);exit;
		$blogArray[$userCnt]['uName'] = $userDtl['fname'].' '.$userDtl['lname'];
		$commentDtl=$blogsObj->getTotalCommentCountByBlogID($blogArr['blogID']);
		$blogArray[$userCnt]['commentCount'] = $commentDtl;
		$userCnt++;	
	}
	
	//$count = $blogsObj->getTotalCommentCountByBlogID(base64_decode($_GET['id']));
	$smarty->assign('blogArray',$blogArray);
	
	//echo "<pre>";print_r($blogArray);exit;
	
	if($searchindex != '') {
		$other_id = "search=".$searchindex;
	} else {
		$other_id = '';
	}
	
	if($total_rows > $rowsPerPage) {
		$pg = new pagination();					// CREATING OBJECT OF THE PAGINATION CLASS
		$pgnation = $pg->pagination_admin( $total_rows , $rowsPerPage , $pageNum , $pageName , $other_id, $class);	
		$smarty-> assign("pagination",$pgnation);
	}
##  --------- Pagination part second end --------------##	

## Assign session message to smarty variable and unset session variable
if($_SESSION['msg'] != '') {
	$smarty->assign('msg', $_SESSION['msg']);	
	unset($_SESSION['msg']);
}

## Set active class variable for left menu  (main menu and sub menu)
$smarty->assign('activeclass', 'blogs');
$smarty->assign('mainmenu', '5');

## Unset all the objects created which are on this page
unset($blogsObj);
unset($moduleObj);

$smarty->display(TEMPLATEDIR_ADMIN. 'controller/blogs/blogList.tpl');
unset($smarty);
?>
