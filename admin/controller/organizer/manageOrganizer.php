<?php  
## include required files
/*******************************/
require_once '../model/organizer.php';
require_once '../model/moduleuser.php';
## Paginaton class
require_once ("../model/common/classes/pagination_class.php");
/*******************************/

## Create Objects
/*******************************/
$organizerObj = new Model_Organizer();
$moduleObj=new Model_ModuleUser();
/*******************************/

## check user's permission
#if($modules_new[2]['perStatus'] !=1 || $modules_new[2]['submod_7'] !=1) {
#	header('location: '.SITE_URL.'/admin/home.php');
#	exit;
#}


## Get search parameters in variables - 
if($_POST['search'] != '' && $_POST['search'] != 'Search') {
	$searchindex = $_POST['search'];
} else if($_GET['search'] != '') {
	$searchindex = $_GET['search'];
} else {
	$searchindex = '';
}
$searchindex=trim($searchindex);
// Assign search variable to show in search textbox
$smarty->assign('search', $searchindex);
if(isset($_GET['order_field']) && $_GET['order_field'] != '') {
	$orderField = $_GET['order_field'];
}
else {
	$orderField = 'orgName';
}
$smarty->assign('orderField', $orderField);

if(isset($_GET['order_by']) && $_GET['order_by'] != '') {
	$orderBy = $_GET['order_by'];
}
else {
	$orderBy = 'ASC';
}
$smarty->assign('orderBy', $orderBy);


//error_reporting(E_ALL);
if(isset($_POST['go']) != '' && isset($_POST['action']) != '') {
		$ids = implode(",", $_POST['checkall']);
		## check which action is selected
		$hidden_page = $_POST['hidden_pageno'];	
		if($_POST['action'] == 'active') {
			## Active selected records
			$organizerObj->updateorganizerStatus($ids, '2');
			$_SESSION['msg']="<div class='success_msg'><span>Organizer status activated successfully.</span></div>";
		} else if($_POST['action'] == 'inactive') {
			## Inactive selected records
			$organizerObj->updateorganizerStatus($ids, '1');
			$_SESSION['msg']="<div class='success_msg'><span>Organizer status deactivated successfully.</span></div>";
		} else {
			## Delete selected records
			$organizerObj->deleteorganizer($ids);
			$_SESSION['msg']="<div class='success_msg'><span>Organizer deleted successfully</span></div>";
		}
		header('location:'.SITE_URL.'/admin/home.php?q=manageOrganizer&page='.$hidden_page.'&order_by='.$orderBy.'&search='.$searchindex.'&order_field='.$orderField);
			exit;
		## Delete code will be here if required
		
} // if close

## Active/Inactive/Delete thourgh URL
if(isset($_GET['action']) == 'status' && isset($_GET['status']) != '') {
	$id = base64_decode($_GET['id']);		
	## Update organizer page status 
	$array = array();
	$array['status'] = ($_GET['status']=='1'?2:1);
	$organizerObj->editorganizerValueById($array,$id);
	if($_GET['status']==2) {
		$_SESSION['msg']="<div class='success_msg'><span>Organizer status activated successfully.</span></div>";
	} else {
		$_SESSION['msg']="<div class='success_msg'><span>Organizer status deactivated successfully.</span></div>";
	}
	header('location:'.SITE_URL.'/admin/home.php?q=manageOrganizer&page='.$_GET['page'].'&order_by='.$orderBy.'&search='.$searchindex.'&order_field='.$orderField);
	exit;
}

if(isset($_GET['action']) == 'delete' && isset($_GET['id']) != '') {
	$id = base64_decode($_GET['id']);
	## Delete Organizer
	$organizerObj->deleteorganizerById($id);
	$_SESSION['msg']="<div class='success_msg'><span>Organizer  deleted successfully</span></div>";
	header('location:'.SITE_URL.'/admin/home.php?q=manageOrganizer&page='.$_GET['page']);
	exit;
}

/*******************************/
if($_GET['q']=='organizer')
{
	$moduelArray=$moduleObj->getModuleHelpText(3);
	$smarty->assign('moduelArray', $moduelArray);
}

//echo "<pre>"; print_r($moduelArray);exit;
##Fetch all organizer pages from database


##Fetch all Organizer pages from database

##  --------- Pagination part first start --------------##
	if( isset( $_GET[ 'page' ] ) && $_GET[ 'page' ] != "" ) {
		$pageNum = $_GET[ 'page' ];
	} else {
		$pageNum = 1;
	} 
	$rowsPerPage = ($_GET['setlimit']) ? $_GET['setlimit'] : ROW_PER_PAGE;	
	$pageName = "home.php?q=manageOrganizer&order_by=".$orderBy."&search=".$searchindex."&order_field=".$orderField."&page=".$_GET['page']."&setlimit=".$_GET['setlimit']; 
	## Count all the records
	$organizerArray = $organizerObj->getAllorganizer($searchindex,$orderField, $orderBy);
	$total_rows = count($organizerArray);
	$offset	= ($pageNum - 1) * $rowsPerPage;
	$smarty->assign('page', $pageNum);
##  --------- Pagination part first end --------------##


##  --------- Pagination part second start --------------##
	$newid = $pageNum * $rowsPerPage - $rowsPerPage + 1;
	$smarty->assign('newid', $newid);
	$organizerArray = $organizerObj->getAllorganizer($searchindex,$orderField, $orderBy,$rowsPerPage,$offset);
	//echo "<pre>"; print_r($organizerArray);exit;
#	$k=0;
#	foreach($organizerArray as $ca)
#	{
#		$league	= $organizerObj->getCountFromLeague($ca['orgID']);		
#		$organizerArray[$k]['leagueCount']= $league['total'];
#		$k++;
#	}
	$smarty->assign('organizerArray', $organizerArray);
	
	if($searchindex != '') {
		$other_id = "search=".$searchindex;
	} else {
		$other_id = '';
	}
	
	if($total_rows > ROW_PER_PAGE) {
		$pg = new pagination();					// CREATING OBJECT OF THE PAGINATION CLASS
		$pgnation = $pg->pagination_admin( $total_rows , $rowsPerPage , $pageNum , $pageName , $other_id, $class);	
		$smarty-> assign("pagination",$pgnation);
	}
##  --------- Pagination part second end --------------##	

## Assign session message to smarty variable and unset session variable
if($_SESSION['msg'] != '') {
	$smarty->assign('msg', $_SESSION['msg']);	
	unset($_SESSION['msg']);
}

## Set active class variable for left menu  (main menu and sub menu)
$smarty->assign('activeclass', 'organizer');
$smarty->assign('mainmenu', '2');

## Unset all the objects created which are on this page
unset($organizerObj);
unset($moduleObj);

$smarty->display(TEMPLATEDIR_ADMIN. 'controller/organizer/manageOrganizer.tpl');
unset($smarty);
?>
