<?php
## include required files
/*******************************/
require_once '../model/organizer.php';
/*******************************/
## Create Objects
/*******************************/
$organizerObj = new Model_Organizer();
/*******************************/

## check user's permission
#if($modules_new[2]['perStatus'] !=1 || $modules_new[2]['submod_9'] !=1) {
#	header('location: '.SITE_URL.'/admin/home.php');
#	exit;
#}

//error_reporting(E_ALL);
##get organizer id from edit page as in the hidden form
$editorganizerid = base64_decode($_GET['id']);

## Get search parameters in variables - 
if($_POST['search'] != '' && $_POST['search'] != 'Search') {
	$searchindex = ucfirst($_POST['search']);
} else if($_GET['search'] != '') {
	$searchindex = $_GET['search'];
} else {
	$searchindex = '';
}
$searchindex=trim($searchindex);
// Assign search variable to show in search textbox
$smarty->assign('search', $searchindex);
if(isset($_GET['order_field']) && $_GET['order_field'] != '') {
	$orderField = $_GET['order_field'];
}
else {
	$orderField = 'orgName';
}
$smarty->assign('orderField', $orderField);

if(isset($_GET['order_by']) && $_GET['order_by'] != '') {
	$orderBy = $_GET['order_by'];
}
else {
	$orderBy = 'ASC';
}
$smarty->assign('orderBy', $orderBy);


if(isset($_POST) && !empty($_POST)) {
	## apply PHP validation for required filed
	if(trim($_POST['orgName']) != '' )
	 {	
	 //echo "<pre/>";print_r($_POST['orgName']); exit;	

		$organizereditArray['orgName'] = return_post_value($orgName);		
		$editorganizerid = $_POST['organizerId'];
		//echo "<pre>"; print_r($editorganizerid);exit;
		$organizerObj->editorganizerById($organizereditArray, $editorganizerid);
		
		$_SESSION['msg'] = "<div class='success_msg'><span>Organizer edited successfully</span></div>";
		header('location:'.SITE_URL.'/admin/home.php?q=manageOrganizer&page='.$_POST['page'].'&order_by='.$orderBy.'&search='.$searchindex.'&order_field='.$orderField);
		exit;
	} else {// php validation else
		$_SESSION['msg'] = '<div class="error_msg"><span>Please enter organizer </span></div>';
	}
	// main if closed
}

if($_GET['id'] != ''&& $_GET['action'] == 'edit') {
	$organizer_id = base64_decode($_GET['id']);
	$organizerDetArray = $organizerObj->getorganizerDetailsById($organizer_id);
	$smarty->assign('organizerDetArray', $organizerDetArray);
}

## Assign success or error msg to smarty variable and unset session variable
if(trim($_SESSION['msg']) != '') {
	$smarty->assign('msg', $_SESSION['msg']);
	unset($_SESSION['msg']);
}

## Set active class variable for left menu
$smarty->assign('activeclass', 'organizer');
$smarty->assign('mainmenu', '2');
## Unset all the objects created which are on this page
unset($organizerObj);

$smarty->display(TEMPLATEDIR_ADMIN. 'controller/organizer/editOrganizer.tpl');
unset($smarty);
?>
