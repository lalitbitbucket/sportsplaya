<?php 
## include required files
/*******************************/
require_once '../model/users.php';
/*******************************/
## Create Objects
/*******************************/
$adminObj = new Model_Users();
/*******************************/
if(isset($_POST['changeemail_btn'])) {
	## apply PHP validation for required filed
	if(trim($_POST['email']) != '' && trim($_POST['password']) != '') {
			$userId=$_SESSION["adminSporttagId"];
			$adminArr=$adminObj->checkpassword($userId,$_POST['password']);
			if(count($adminArr) >0){
		
			$userArray = array();
			$userArray['email']   = return_post_value($email);
			$adminObj->editUserValueById($userArray,$userId);
			$_SESSION['admin_email'] = return_post_value($email);
			$_SESSION['msg'] = "<div class='success_msg'><span>You have changed email address successfully.</span></div>";
			header('location:'.SITE_URL.'/admin/home.php?q=changeEmail');
			exit;
		} else { // checking password is valid
			$_SESSION['msg'] = "<div class='error_msg'><span>You have enter wrong password.</span></div>";
		}
	} else {// php validation else
		$_SESSION['msg'] = "<div class='error_msg'><span>Please enter new email address and password.</span></div>";
	}
	// main if closed
}

## Assign success or error msg to smarty variable and unset session variable
if(trim($_SESSION['msg']) != '') {
	$smarty->assign('msg', $_SESSION['msg']);
	unset($_SESSION['msg']);
}

## Unset all the objects created which are on this page
unset($adminObj);

$smarty->display(TEMPLATEDIR_ADMIN. 'controller/adminUsers/superAdmin/changeEmail.tpl');
unset($smarty);
?>
