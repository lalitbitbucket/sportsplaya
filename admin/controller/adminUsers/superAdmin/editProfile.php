<?php 
/* date :21 jan 2013  S.S.B. */
## include required files
/*******************************/
require_once '../model/users.php';
require_once '../model/common/image_functions.php';
/*******************************/
## Create Objects
/*******************************/
$userObj = new Model_Users();
/*******************************/
##profile image dimension message
$smarty->assign('profileimage_dimension', PROFILEIMAGE_WIDTHHEIGHT);

$userId=$_SESSION["adminSporttagId"];
$smarty->assign('userId', $userId);
//echo"<pre>"; print_r($userId); exit;

## fetching admin information
$adminArray=$userObj->getUserDetailsByUserId($userId);	
$smarty->assign('adminArray', $adminArray);

$userinfoArray=$userObj->getUserProfileInformationByUserID($adminArray['userId'],$adminArray['userType']);
$smarty->assign('userinfoArray', $userinfoArray);
//echo"<pre>"; print_r($adminArray); echo"</pre>";

if(sizeof($_POST)>0) {
	## apply PHP validation for required filed
	if(trim($_POST['first_name']) != '' && trim($_POST['last_name']) != '') {
		
		
			$userArray = array();		
			$_SESSION["admin_firstname"] = return_post_value($first_name);
			$_SESSION["admin_lastname"] = return_post_value($last_name);
			
                        //echo "<pre>";print_r($userArray);exit;
			//echo "<pre>";print_r($_SESSION["adminSporttagId"]);exit;
			$userObj->editUserValueById($userArray,$_SESSION["adminSporttagId"]);	

               
             $userinfoArray = array();		
		    $userinfoArray["fName"] = return_post_value($first_name);
			$userinfoArray["lName"] = return_post_value($last_name);	
			
			if($_FILES['user_avatar']['name']!='') {
				
				$size =	getimagesize($_FILES['user_avatar']['tmp_name']);
				if($_FILES['user_avatar']['name']!= '' && ($size[0]>=PROFILEIMAGEWIDTH && $size[1]>=PROFILEIMAGEHEIGHT) ) {
               
					##unlink the prevoius logo							
					$getUserPreviousProfileImage = $userObj->getAdminUserProfileImageByUserId($userId);		

					if(count($getUserPreviousProfileImage)>0){
						@unlink ("../dynamicAssets/users/".$getUserPreviousProfileImage['avatar']);			
						@unlink ("../dynamicAssets/users/thumb/".$getUserPreviousProfileImage['avatar']);							
					}
					
					## Upload New Profile Image
						//$fileExt = file_extension($_FILES["user_avatar"]["name"]);	
						// echo "<pre>";print_r($fileExt);exit;					
						//error_reporting(E_ALL);	
						$imageName    = $_FILES['user_avatar']['name'];
						$arrImageName = explode(".",$imageName);
						$position     = count($arrImageName);
						$fileName     = date('Ymdhis').".".$imageName;
				
				              // echo $fileName;exit;
								
						$temp=$_FILES["user_avatar"]["tmp_name"];
						
						//copy($temp,"../admin/dynamicAssets/adminUser/".$fileName);
						copy($temp,"../dynamicAssets/users/".$fileName);
						$imageFolder="../dynamicAssets/users/";						
						$thumbFolder ="../dynamicAssets/users/thumb/";						
						$height = 100;
						$width= 100;
						$file1=uploadImageAndCreateThumb($temp,$imageName,$imageFolder,$thumbFolder,$height,$width);						
						
						$imageName= $fileName;
			            $thumbFolder2 ="../dynamicAssets/users/57x56/";						
			            $height2 = 56;
			            $width2= 57;
			            $file1=uploadImageAndCreateThumb($temp,$imageName,$imageFolder,$thumbFolder2,$height2,$width2);
						
					## Update user profile image record
					$userinfoArray['avatar']=$fileName;
					
					//echo "<pre>";print_r($userArray);exit;
     				//@unlink ("../admin/dynamicAssets/adminUser/".$fileName);
							
				} else {
					$_SESSION['msg'] = "<div class='error_msg'><span>".PROFILEIMAGE_WIDTHHEIGHT."</span></div>";
				}	
			}
			
             $userObj->editUserProfileValueByUserId($userinfoArray,$_SESSION["adminSporttagId"]);	   


			
			$_SESSION['msg'] = "<div class='success_msg'><span>You have edited profile information successfully.</span></div>";
			header('location:'.SITE_URL.'/admin/home.php?q=editProfile');
			exit;
	} else {// php validation else
		$_SESSION['msg'] = "<div class='error_msg'><span>Please enter first and last name.</span></div>";
	}
	// main if closed
}


if(isset($_GET['action']) && $_GET['action']='reset'){

  $userDtl=$userObj->getImageByUserId($userId);	
  if($userDtl['avatar']!= '')
  {
  @unlink ("../admin/dynamicAssets/adminUser/".$userDtl['avatar']);
  @unlink("../admin/dynamicAssets/adminUser/thumb/".$userDtl['avatar']);	
  }
  $userImageArray= array();
  $userImageArray['avatar']='';
  $userObj->editUserValueById($userImageArray,$userId);
  $_SESSION['msg'] = "<div class='success_msg'><span>You have deleted profile image successfully.</span></div>";
  header('location:'.SITE_URL.'/admin/home.php?q=editProfile');
  exit();

}


## Assign success or error msg to smarty variable and unset session variable
if(trim($_SESSION['msg']) != '') {
	$smarty->assign('msg', $_SESSION['msg']);
	unset($_SESSION['msg']);
}

## Unset all the objects created which are on this page
unset($userObj);

$smarty->display(TEMPLATEDIR_ADMIN. 'controller/adminUsers/superAdmin/editProfile.tpl');
unset($smarty);
?>
