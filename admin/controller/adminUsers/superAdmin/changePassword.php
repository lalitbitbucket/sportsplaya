<?php 
## include required files
/*******************************/
require_once '../model/users.php';
require_once '../model/common/functions.php';
/*******************************/
## Create Objects
/*******************************/
$adminObj = new Model_Users();
/*******************************/
$userId=$_SESSION["adminSporttagId"];
$userDetails = $adminObj->getUserDetailsByUserId($userId);

if(isset($_POST['changepassword_btn'])) {
	## apply PHP validation for required filed
	if(trim($_POST['cpassword']) != '' && trim($_POST['npassword']) != '') {
		$oldPass = return_fetched_value($_POST['cpassword']);
	        $newPass = return_fetched_value($_POST['npassword']);
	        $confirmPass = return_fetched_value($_POST['confirmpassword']);	
		$getpassword = genenrate_password($userDetails['salt'], $_POST['cpassword']);		
		//echo '<pre>';print_r($getpassword);exit;
		if($userDetails['password'] == $getpassword)
		{					
			$cpArray = array();
			$salt = genenrate_salt();
			$newpassword = genenrate_password($salt ,$newPass);	
			$cpArray['password'] 		= $newpassword;				
			$cpArray['salt']     		= $salt;			
			$adminObj->editUserValueById($cpArray,$userDetails['userId']);
			$_SESSION['msg'] = "<div class='success_msg'><span>You have changed Password successfully.</span></div>";
			header('location:'.SITE_URL.'/admin/home.php?q=changePassword');
			exit();				
		}
		else { // checking password is valid
			$_SESSION['msg'] = "<div class='error_msg'><span>You have entered wrong current password.</span></div>";
		}
	} else {// php validation else
		$_SESSION['msg'] = "<div class='error_msg'><span>Please enter current and new password.</span></div>";
	}
	// main if closed
}

## Assign success or error msg to smarty variable and unset session variable
if(trim($_SESSION['msg']) != '') {
	$smarty->assign('msg', $_SESSION['msg']);
	unset($_SESSION['msg']);
}

## Unset all the objects created which are on this page
unset($adminObj);

$smarty->display(TEMPLATEDIR_ADMIN. 'controller/adminUsers/superAdmin/changePassword.tpl');
unset($smarty);
?>
