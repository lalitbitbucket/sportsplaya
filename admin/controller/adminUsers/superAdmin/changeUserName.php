<?php 
## include required files
/*******************************/
require_once '../model/users.php';
/*******************************/
## Create Objects
/*******************************/
$adminObj = new Model_Users();
/*******************************/
if(isset($_POST['changeusername_btn'])) {
	## apply PHP validation for required filed
	if(trim($_POST['username']) != '' && trim($_POST['password']) != '') {
			$userId=$_SESSION["adminSporttagId"];
			$adminArr=$adminObj->checkpassword($userId,$_POST['password']);
			if(count($adminArr) >0){

			$userArray = array();
			$userArray['username']   	= return_post_value($username);
			$_SESSION["admin_username"] 	= return_post_value($username);
			$adminObj->editUserValueById($userArray,$userId);
			$_SESSION['msg'] = "<div class='success_msg'><span>You have changed username successfully.</span></div>";
			header('location:'.SITE_URL.'/admin/home.php?q=changeUserName');
			exit;
		} else { // checking password is valid
			$_SESSION['msg'] = "<div class='error_msg'><span>You entered wrong password.</span></div>";
		}
	} else {// php validation else
		$_SESSION['msg'] = "<div class='error_msg'><span>Please enter new Username and password.</span></div>";
	}
	// main if closed
}

## Assign success or error msg to smarty variable and unset session variable
if(trim($_SESSION['msg']) != '') {
	$smarty->assign('msg', $_SESSION['msg']);
	unset($_SESSION['msg']);
}

## Unset all the objects created which are on this page
unset($adminObj);

$smarty->display(TEMPLATEDIR_ADMIN. 'controller/adminUsers/superAdmin/changeUserName.tpl');
unset($smarty);
?>
