<?php 
## include required files
/*******************************/
require_once '../model/users.php';
require_once '../model/states.php';
require_once '../model/useraddress.php';
/*******************************/

## Create Objects
/*******************************/
$userObj = new Model_Users();
$stateObj 	= new Model_States();
$addressObj 	= new Model_User_Address();
/*******************************/

## Get search parameters in variables - 
if(isset($_REQUEST['search']) != '' && $_REQUEST['search'] != 'Search') {
	$searchindex = $_REQUEST['search'];
}else {
	$searchindex = '';
}
// Assign search variable to show in search textbox
$searchindex=addslashes(trim($searchindex));
$smarty->assign('search', $searchindex);

## user type parameters for searching
if(isset($_REQUEST['user_type']) != '' && $_REQUEST['user_type'] != 'User Type'){
	$user_type = $_REQUEST['user_type'];
} else {
	$user_type = '';
}
$smarty->assign('user_type', $user_type);

if(isset($_REQUEST['order_field']) && $_REQUEST['order_field'] != '') {
	$orderField = $_REQUEST['order_field'];
}
else {
	$orderField = 'fName';
}
$smarty->assign('orderField', $orderField);

if(isset($_REQUEST['order_by']) && $_REQUEST['order_by'] != '') {
	$orderBy = $_REQUEST['order_by'];
}
else {
	$orderBy = 'ASC';
}
$smarty->assign('orderBy', $orderBy);

if($_POST['last_name']!='' || $_POST['first_name']!='') {
	## apply PHP validation for required filed
	if(trim($_POST['first_name']) != '' && trim($_POST['last_name']) != '' && trim($_POST['email']) != '') {
		
			$userArray = array();
			
			$user_id = $_POST['hidden_userid'];
			$userArray['fName']   = return_post_value($first_name);
			$userArray['lName']   = return_post_value($last_name);
			$userArray['email']   = return_post_value($email);
			$userObj->editUserValueById($userArray, $user_id);
	
			$_SESSION['msg'] = "<div class='success_msg'><span>Sub user updated successfully</span></div>";
			header('location:'.SITE_URL.'/admin/home.php?q=managesubusers&page='.$_GET['page'].'&order_field='.$orderField.'&order_by='.$orderBy.'&search='.$searchindex);
			exit;
		
			
	} else { // php validation else
		$_SESSION['msg'] = '<div class="error_msg"><span>Please fill all required fields</span></div>';
	}
	// main if closed
}

## Fetch admin user details by id
if($_GET['id'] != '' && $_GET['action'] == 'edit') {
	$user_id = base64_decode($_GET['id']);	
	$user_details = $userObj->getUserDetailsByUserId($user_id);
	$smarty->assign('user_details', $user_details);
}

## get all active state by country id
$stateArray = $stateObj->getAllActiveStateByCountryId(1);
$smarty->assign('stateArray', $stateArray);

## Assign success or error msg to smarty variable and unset session variable
if(trim($_SESSION['msg']) != '') {
	$smarty->assign('msg', $_SESSION['msg']);
	unset($_SESSION['msg']);
}

## Set active class variable for left menu
$smarty->assign('activeclass', 'managesubusers');
$smarty->assign('mainmenu', '3');
## Unset all the objects created which are on this page
unset($userObj);

$smarty->display(TEMPLATEDIR_ADMIN. 'controller/adminUsers/subuser/updatesubuser.tpl');
unset($smarty);
?>