<?php
## include required files
require_once '../model/users.php';
require_once '../model/common/classes/pagination_class.php';
## Create Objects
$userObj    = new Model_Users();


//echo "<pre>";print_r($_SESSION);exit;

//# check user's permission
//if($modules_new[6]['perStatus'] !=1 || $modules_new[6]['submod_32'] !=1) {
//	header('location: '.SITE_URL.'/admin/home.php');
//	exit;
//}

## Get search parameters in variables - 
if(isset($_REQUEST['search']) != '' && $_REQUEST['search'] != 'Search') {
	$searchindex = $_REQUEST['search'];
} else {
	$searchindex = '';
}
// Assign search variable to show in search textbox
//$searchindex=addslashes(trim($searchindex));
$searchindex=trim(return_fetched_value($searchindex));
$smarty->assign('search', $searchindex);

## user type parameters for searching
if(isset($_REQUEST['user_type']) != '' && $_REQUEST['user_type'] != 'User Type'){
	$user_type = $_REQUEST['user_type'];
} else {
	$user_type = '';
}
$smarty->assign('user_type', $user_type);

if(isset($_REQUEST['order_field']) && $_REQUEST['order_field'] != '') {
	$orderField = $_REQUEST['order_field'];
}
else {
	$orderField = 'fName';
}
$smarty->assign('orderField', $orderField);

if(isset($_REQUEST['order_by']) && $_REQUEST['order_by'] != '') {
	$orderBy = $_REQUEST['order_by'];
}
else {
	$orderBy = 'ASC';
}
$smarty->assign('orderBy', $orderBy);



## Active, Inactive selected records
/*******************************/
if(isset($_POST['go']) != '' && isset($_POST['action']) != '') {
	//print_r($_REQUEST);exit;
		$ids = implode(",", $_POST['checkall']);
		## check which action is selected
		if(trim($ids) != '' ) {
			$hidden_page = $_POST['hidden_pageno'];	
			if($_POST['action'] == 'active') {
				## Active selected records
				$userObj->updateUserStatus($ids, '2');
				$_SESSION['msg']="<div class='success_msg'><span>Sub admin user(s) activated successfully.</span></div>";
			} else if($_POST['action'] == 'inactive') {
				## Inactive selected records
				$userObj->updateUserStatus($ids, '1');
				$_SESSION['msg']="<div class='success_msg'><span>Sub admin user(s) deactivated successfully.</span></div>";
			} else {
				## Delete selected records
				$userObj->updateUserStatus($ids, '0');
				$_SESSION['msg']="<div class='success_msg'><span>Sub admin  user(s) deleted successfully.</span></div>";
			}
				header('location:'.SITE_URL.'/admin/home.php?q=managesubusers&order_field='.$orderField.'&order_by='.$orderBy.'&search='.$searchindex.'&page='.$hidden_page);
				exit;
		}
		
}

## Active/Inactive User Status by id
if(isset($_GET['action']) == 'status' && isset($_GET['status']) != '') {
	
	$id = base64_decode($_GET['id']);
	## Update user status
	$array = array();
	$array['status'] = ($_GET['status']=='2'?1:2);
	$userObj->editUserValueById($array, $id);
	if($_GET['status']==2){
		$_SESSION['msg']="<div class='success_msg'><span>Sub admin user deactivated successfully.</span></div>";
	} else {
		$_SESSION['msg']="<div class='success_msg'><span>Sub admin user activated successfully.</span></div>";
	}
	
	header('location:'.SITE_URL.'/admin/home.php?q=managesubusers&order_field='.$orderField.'&order_by='.$orderBy.'&search='.$searchindex.'&page='.$_GET['page']);
	exit;
}

## Delete User by id
if(isset($_GET['action']) == 'delete' && isset($_GET['id']) != '') {
	$id = base64_decode($_GET['id']);
	## Delete User
	$array = array();
	$array['status'] = '0';
	$userObj->editUserValueById($array, $id);
	$_SESSION['msg']="<div class='success_msg'><span>Sub admin user deleted successfully.</span></div>";
	header('location:'.SITE_URL.'/admin/home.php?q=managesubusers&order_field='.$orderField.'&order_by='.$orderBy.'&search='.$searchindex.'&page='.$_GET['page']);
	exit;
}

## --------- Pagination part first start --------------##
if( isset( $_GET[ 'page' ] ) && $_GET[ 'page' ] != "" ) {
	$pageNum = $_GET[ 'page' ];
} else {
	$pageNum = 1;
} 
$rowsPerPage = ($_GET['setlimit']) ? $_GET['setlimit'] : ROW_PER_PAGE;
$pageName = "home.php?q=managesubusers&order_by=".$_GET['page'].'&order_by='.$orderBy.'&search='.$searchindex.'&order_field='.$orderField.'&order_by='.$orderBy.'&setlimit='.$rowsPerPage; 
## Count all the records
$userArray = $userObj->getAllSubUsers($searchindex, $orderField, $orderBy);
$total_rows = count($userArray);
$offset	= ($pageNum - 1) * $rowsPerPage;
$smarty->assign('page', $pageNum);
##--------- Pagination part first end --------------##


##--------- Pagination part second start --------------##
$newid = $pageNum * $rowsPerPage - $rowsPerPage + 1;
$smarty->assign('newid', $newid);
$userArray = $userObj->getAllSubUsers($searchindex, $orderField, $orderBy, $rowsPerPage, $offset);
//echo "<xmp>"; print_r($userArray); echo "</xmp>";
$smarty->assign('userArray', $userArray);

if($searchindex != '') {
	$other_id = "search=".$searchindex."&order_by=".$orderBy;
} else {
	$other_id = '';
}

if($total_rows > ROW_PER_PAGE) {
	$pg = new pagination();
	$pgnation = $pg->pagination_admin( $total_rows , $rowsPerPage , $pageNum , $pageName , $other_id, $class);	
	$smarty-> assign("pagination",$pgnation);
}
##  --------- Pagination part second end --------------##	

## Assign session message to smarty variable and unset session variable
if(isset($_SESSION['msg']) &&  $_SESSION['msg']!= '') {
	$smarty->assign('msg', $_SESSION['msg']);	
	unset($_SESSION['msg']);
}


## Unset all the objects created which are on this page
unset($userObj);


$smarty->display(TEMPLATEDIR_ADMIN. 'controller/adminUsers/subuser/manageusers.tpl');
unset($smarty);
?>