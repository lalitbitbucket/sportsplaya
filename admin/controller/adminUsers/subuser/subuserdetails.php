<?php 
## include required files
/*******************************/
require_once '../model/users.php';
require_once '../model/useraddress.php';
/*******************************/

## Create Objects
/*******************************/
$userObj = new Model_Users();
$addressObj 	= new Model_User_Address();
/*******************************/

## Get search parameters in variables - 
if(isset($_REQUEST['search']) != '' && $_REQUEST['search'] != 'Search') {
	$searchindex = $_REQUEST['search'];
} else {
	$searchindex = '';
}
// Assign search variable to show in search textbox
$searchindex=addslashes(trim($searchindex));
$smarty->assign('search', $searchindex);

## user type parameters for searching
if(isset($_REQUEST['user_type']) != '' && $_REQUEST['user_type'] != 'User Type'){
	$user_type = $_REQUEST['user_type'];
} else {
	$user_type = '';
}
$smarty->assign('user_type', $user_type);

if(isset($_REQUEST['order_field']) && $_REQUEST['order_field'] != '') {
	$orderField = $_REQUEST['order_field'];
}
else {
	$orderField = 'fName';
}
$smarty->assign('orderField', $orderField);

if(isset($_REQUEST['order_by']) && $_REQUEST['order_by'] != '') {
	$orderBy = $_REQUEST['order_by'];
}
else {
	$orderBy = 'ASC';
}
$smarty->assign('orderBy', $orderBy);
## Fetch admin user details by id
if($_GET['id'] != '') {
	$user_id = base64_decode($_GET['id']);	
	$user_details = $userObj->getUserDetailsByUserId($user_id);
	## get user's default address
	$user_address = $addressObj->getDefaultAddressDetailByUserID($user_id);
	$user_details['addrID'] = $user_address['addrID'];
	$user_details['address1'] = $user_address['address1'];
	$user_details['address2'] = $user_address['address2'];
	$user_details['city'] = $user_address['city'];
	$user_details['state'] = $user_address['state'];
	$user_details['zip'] = $user_address['zip'];
	$user_details['contact'] = $user_address['contact'];
	$smarty->assign('user_details', $user_details);
}


## Set active class variable for left menu
$smarty->assign('activeclass', 'managesubusers');
$smarty->assign('mainmenu', '3');
## Unset all the objects created which are on this page
unset($userObj);

$smarty->display(TEMPLATEDIR_ADMIN. 'controller/adminUsers/subuser/subuserdetails.tpl');
unset($smarty);
?>