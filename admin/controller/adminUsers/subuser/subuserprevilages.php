<?php 
## include required files
/*******************************/
require_once '../model/users.php';
require_once '../model/moduleuser.php';
/*******************************/
## Create Objects
/*******************************/
$userObj=new Model_Users();
$moduleObj  = new Model_ModuleUser();
/*******************************/



## For pageback searching and sorting
if($_REQUEST['search'] != '') {
		$searchindex = $_REQUEST['search'];	
	} else {
		$searchindex = '';
	}
$smarty->assign('search', $searchindex);

if($_REQUEST['order_by'] != '') {
		$order_by = $_REQUEST['order_by'];	
	} else {
		$order_by = '';
	}
$smarty->assign('order_by', $order_by);

if($_GET['id']){
	$user_id=base64_decode($_GET['id']);	
} else if($_POST['hidden_userid']){
	$user_id=base64_decode($_POST['hidden_userid']);	
} else {
	$user_id='';	
}

$userArr=$userObj->getUserDetailsByUserId($user_id);
$smarty->assign('userArr',$userArr);


## Update user permission values
if(isset($_POST['update']) && $_POST['update']=='Update'){
		
	if($user_id!=''){
		## check existing permission
		$chkArray=$moduleObj->getModuleUserValueByUserIdWithActiveStatus($user_id);
		
		if(count($chkArray) > 0){
			## delete existing permissions
			$moduleObj->deleteModuleUserValueByUserId($user_id);
			$chk = $_POST['checkallsub'];
			## add permissions to user
			foreach($chk as $pre_id) {
				$get_parent_id = $moduleObj->getParentId($pre_id);
				
				$array = array();
				$array['moduleId'] = $get_parent_id['moduleId'];
				$array['submoduleId'] = $pre_id;
				$array['userId'] = $user_id;
				$array['perStatus'] = "1";
				$moduleObj->addModuleUserValue($array);	
			}
		} else {
			$chk = $_POST['checkallsub'];
			## add permissions to user
			foreach($chk as $pre_id) {
				$get_parent_id = $moduleObj->getParentId($pre_id);
				$array = array();
				$array['moduleId'] = $get_parent_id['moduleId'];
				$array['submoduleId'] = $pre_id;
				$array['userId'] = $user_id;
				$array['perStatus'] = "1";
				$moduleObj->addModuleUserValue($array);	
			}
		}
		$_SESSION['msg']="<div class='success_msg'><span>Previlages Updated For Sub Admin Successfully</span></div>";
		header('location:'.SITE_URL.'/admin/home.php?q=subuserprevilages&id='.base64_encode($user_id).'&page='.$_POST['page'].'&order_by='.$order_by.'&search='.$searchindex);
		exit;
	}
}

## Get all modules
$moduleArray = $moduleObj->getAllMasterActiveModules();

$i=0;
foreach($moduleArray as $moduleArr){
		
	$submoduleArray = $moduleObj->getAllAdminSubModules($moduleArr['moduleId']);

	$moduleArray[$i]['submodule'] = $submoduleArray;
	$j=0;
	$sub_module_str_arr = array();
	foreach($submoduleArray as $submoduleArr){		
		$sub_module_str_arr[] = $submoduleArr['subModID'];
		$sub_module_str=implode(",",$sub_module_str_arr);
		## check permission
		$sub_module_permission = $moduleObj->getAdminAssignedSubModules($user_id,$submoduleArr['subModID']);
		$moduleArray[$i]['submodule'][$j]['permission'] = $sub_module_permission['perStatus'];
		$moduleArray[$i]['mod_permission'] = $sub_module_permission['perStatus'];
		$j++;
	}
	$moduleArray[$i]['sub_module_str'] = $sub_module_str;	
	$i++;
}
$smarty->assign('moduleArray',$moduleArray);

//echo"<xmp>"; print_r($moduleArray); echo"</xmp>";

## Assign success or error msg to smarty variable and unset session variable
if(trim($_SESSION['msg']) != '') {
	$smarty->assign('msg', $_SESSION['msg']);
	unset($_SESSION['msg']);
}
## Set active class variable for left menu  (main menu and sub menu)
$smarty->assign('activeclass', 'managesubusers');
$smarty->assign('mainmenu', '3');

## Unset all the objects created which are on this page
unset($adminObj);
unset($moduleObj);

$smarty->display(TEMPLATEDIR_ADMIN. 'controller/adminUsers/subuser/subuserprevilages.tpl');
unset($smarty);
?>