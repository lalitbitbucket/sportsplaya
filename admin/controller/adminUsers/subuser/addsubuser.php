<?php 
## include required files
/*******************************/
require_once '../model/users.php';
require_once '../model/states.php';
require_once '../model/useraddress.php';
require_once '../model/email.php';
require_once '../includes/phpmailer/class.phpmailer.php';
/*******************************/

## Create Objects
/*******************************/
$userObj = new Model_Users();
$stateObj 	= new Model_States();
$addressObj 	= new Model_User_Address();
$emailObj = new Model_Email();
$mail 		= new PHPMailer(true);
/*******************************/

## Get search parameters in variables - 
if(isset($_REQUEST['search']) != '' && $_REQUEST['search'] != 'Search') {
	$searchindex = $_REQUEST['search'];
} else {
	$searchindex = '';
}
// Assign search variable to show in search textbox
$searchindex=addslashes(trim($searchindex));
$smarty->assign('search', $searchindex);

## user type parameters for searching
if(isset($_REQUEST['user_type']) != '' && $_REQUEST['user_type'] != 'User Type'){
	$user_type = $_REQUEST['user_type'];
} else {
	$user_type = '';
}
$smarty->assign('user_type', $user_type);

if(isset($_REQUEST['order_field']) && $_REQUEST['order_field'] != '') {
	$orderField = $_REQUEST['order_field'];
}
else {
	$orderField = 'fName';
}
$smarty->assign('orderField', $orderField);

if(isset($_REQUEST['order_by']) && $_REQUEST['order_by'] != '') {
	$orderBy = $_REQUEST['order_by'];
}
else {
	$orderBy = 'ASC';
}
$smarty->assign('orderBy', $orderBy);


if(sizeof($_POST)>0) 
{
	## apply PHP validation for required filed
	if(trim($_POST['first_name']) != '' && trim($_POST['last_name']) != '' && trim($_POST['email']) != '') 
	{

			$userArray = array();

			$pass = rand(999999, 2);
			## Generate Password
			$userArray['uName']   = return_post_value($uname);
			$userArray['fName']   = return_post_value($first_name);
			$userArray['lName']   = return_post_value($last_name);
			$userArray['email']   = return_post_value($email);
			$userArray['paswd']   = md5($pass);
			$userArray['userType']   = '2';
			$userArray['status']   = '2';
			$userArray['regDate']   = date("Y-m-d");
			$userArray['regIP']   = $_SERVER['REMOTE_ADDR'];			
			$userid = $userObj->addUserByValue($userArray);
			
		
			
				################################### Mail Template Start ############################
						## Get Admin details 
					$id=1;
					$admindetails = $userObj->getUserDetailsByUserId($id);
					$smarty->assign('admindetails',$admindetails);
				
					## Fetch email content
					$emailArray = $emailObj->getEmailById(2);
					$to= $userArray['email'];
					$toname= $userArray['email'];
					$subject=$emailArray['emailSub'];
					$subject = str_replace('[SITENAME]', SITENAME, $subject);
     				        $message=$emailArray['emailContent'];
    				        $message=str_replace('[SITENAME]', SITENAME, $message);
					$message=str_replace('[USERNAME]', $userArray['uName'], $message);
					$message=str_replace('[NAME]', ucfirst(return_post_value(trim($first_name)))." ".ucfirst(return_post_value(trim($last_name))), $message);
					$message = str_replace('[Email_Address]',$userArray['email'] , $message);
					$message = str_replace('[PASSWORD]',$pass , $message);
					$message=str_replace('[SITENAME]', SITENAME, $message);		
					$message=str_replace('[SITELINK]',SITE_URL , $message);
					
					$fromname = ucfirst((trim($admindetails['fName'])))." ".ucfirst((trim($admindetails['lName'])));
					$from = $admindetails['email'];
					$emailTemplate=file_get_contents('../siteAssets/mailTemplates/email.html');
					
					$template_msg = str_replace('[USERNAME]',$userArray['uName'], $emailTemplate);
					$template_msg = str_replace('[PASSWORD]',$pass, $template_msg);
					$template_msg = str_replace('[NAME]',$userDetails['fName'] , $template_msg);
					$template_msg = str_replace('[SITENAME]',SITENAME , $template_msg);
					$template_msg = str_replace('[LOGO]','<img src="'.SITE_URL.'/siteAssets/images/logo.png">', $template_msg);
					$template_msg = str_replace('[MESSAGE]',$message , $template_msg);
					$template_msg = str_replace('[SITELINK]',SITE_URL , $template_msg);
					$template_msg = str_replace('[SUBJECT]',$subject , $template_msg);
					$template_msg = str_replace('[SITEROOT]',SITE_URL , $template_msg);					
					
	//                                echo $subject."<br />";
	//			        echo $from."<br />";
	//		                echo $fromname."<br />";
	//				echo $to."<br />";
	//				echo "<pre>"; print_r($template_msg);  exit();
				
				
				try {
					$mail->AddAddress($to, $toname);
					$mail->SetFrom($from, $fromname);
					$mail->Subject = $subject;
					$mail->Body = $template_msg; 
					$mail->Send();  
					$_SESSION['msg'] = '<section class="success_msg">Sub user added successfully.</section>';
				} 
				catch (phpmailerException $e) {
					$_SESSION['msg']= $e->errorMessage(); //Pretty error messages from PHPMailer
				} catch (Exception $e) {
					$_SESSION['msg']= $e->getMessage(); //Boring error messages from anything else!
				}	
		
			//echo $template_msg;exit;
			//$_SESSION['msg'] = "<div class='success_message'><span>Sub user added successfully</span></div>";
			header('location:'.SITE_URL.'/admin/home.php?q=managesubusers&page='.$_GET['page'].'&order_field='.$orderField.'&order_by='.$orderBy.'&search='.$searchindex);
			exit();
		
	} 
	else { // php validation else
		$_SESSION['msg'] = '<div class="error_msg"><span>Please fill all required fields</span></div>';
	}
	// main if closed
}

## get all active state by country id
$stateArray = $stateObj->getAllActiveStateByCountryId(1);
$smarty->assign('stateArray', $stateArray);

## Assign success or error msg to smarty variable and unset session variable
if(trim($_SESSION['msg']) != '') {
	$smarty->assign('msg', $_SESSION['msg']);
	unset($_SESSION['msg']);
}

## Unset all the objects created which are on this page
unset($userObj);
unset($emailObj);

$smarty->display(TEMPLATEDIR_ADMIN. 'controller/adminUsers/subuser/addsubuser.tpl');
unset($smarty);
?>