<?php 
## include required files
/*******************************/
require_once '../model/users.php';
require_once '../model/email.php';
//require_once '../model/useraddress.php';
require_once '../includes/phpmailer/class.phpmailer.php';
/*******************************/
## Create Objects
/*******************************/
$emailObj 	= new Model_Email();
$mail 		= new PHPMailer(true);
$userObj 	= new Model_Users();
/*******************************/
if($_POST['last_name']!='' || $_POST['first_name']!='') {
	## apply PHP validation for required filed
	if(trim($_POST['first_name']) != '' && trim($_POST['last_name']) != '' && trim($_POST['email']) != '' && trim($_POST['birthdate']) != '' && trim($_POST['sex']) != '') {
		    	$userEArray = array();
		
			$user_id = $_POST['hidden_userid'];
			$addrID = $_POST['hidden_addressid'];
			$userEArray['email']   = return_post_value($email);
			if(return_post_value($password)!='password'){
			$userEArray['password']      = md5(return_post_value($password));
			$pass = return_post_value($password);
			}	
	      		$userObj->editUserValueById($userEArray, $user_id);
			$userArray = array();
	
			$userArray['fName']   = return_post_value($first_name);
			$userArray['lName']   = return_post_value($last_name);
			$userArray['email']   = return_post_value($email);
			$userArray['username']   = return_post_value($email);
			$userArray['birthdate']   = return_post_value($birthdate);
			$userArray['sex']   = return_post_value($sex);
					
			if($_POST['password']!='password'){
			## Get Admin details 
			$id=1;
			$admindetails = $userObj->getUserDetailsByUserId($id);
			$smarty->assign('admindetails',$admindetails);

			## Fetch email content
			$emailArray = $emailObj->getEmailById(7);

			$to= $userArray['email'];
			$toname= $userArray['email'];

			$subject=$emailArray['emailSub'];
			$subject = str_replace('[SITENAME]', SITENAME, $subject);

			$message=$emailArray['emailContent'];
			$message=str_replace('[SITENAME]', SITENAME, $message);
			$message=str_replace('[EMAIL]', return_post_value(trim($email)), $message);
			$message=str_replace('[NAME]', ucfirst(return_post_value(trim($first_name))), $message);
			$message=str_replace('[USERNAME]', return_post_value(trim($email)), $message);
			$message = str_replace('[Email_Address]',$userArray['email'] , $message);
			$message = str_replace('[PASSWORD]',$pass , $message);
			$message=str_replace('[SITENAME]', SITENAME, $message);		
			$message=str_replace('[SITELINK]',SITE_URL , $message);
             		$message=str_replace('[MESSAGE]',$message , $message);
			$from = $emailArray['fEmail'];
			$fromname = $emailArray['fName'];
		       $smtpArray = $emailObj->getActiveSMTPDetails();
		       $auth  = $smtpArray['isAuth'];
				if($auth=='y')
				{     $auth = 'true';  
					
				}
				else
				{     $auth = 'false';    
					
				}
					if($auth=='true')
					{  
				        $mail->IsSMTP(); // telling the class to use SMTP
				        $mail->Mailer = "smtp";
				        $mail->SMTPAuth   = true;                  // enable SMTP authentication
				        $mail->SMTPSecure = $smtpArray['secure'];                 // sets the prefix to the servier
				        $mail->Host       = $smtpArray['smtpHost'];      // sets GMAIL as the SMTP server
				        $mail->Port       = $smtpArray['smtpPort'];                  // set the SMTP port for the GMAIL server
				        $mail->Username   = $smtpArray['smtpUsername'];  // GMAIL username
				        $mail->Password   = $smtpArray['smtpPassword']; 
					}
                      			# get email back up    
                      			$backemailArray = array();
                      			$backemailArray['sendto'] = $to;
                     			$backemailArray['subject'] = $subject;
                      			$backemailArray['content'] = $message;
                      			$backemailArray['date'] = date("Y-m-d H:i:s");
                     			// print_r($backemailArray);exit;

		       			/*echo $subject."<br />";
				        echo $from."<br />";
			            	echo $fromname."<br />";
					echo $to."<br />";
					echo $toname."<br />";
					echo "<pre>"; print_r($message);  exit();*/
                     
                      		try {
					  $mail->AddAddress($to, $toname);
					  $mail->SetFrom($from, $fromname);
					  $mail->Subject = $subject;
					  $mail->Body = $message; 
				      	  $mail->Send(); 
                      			  $backemailArray['status'] ='2';   
				} 
				catch (phpmailerException $e) {
				   $_SESSION['msg']= "<div class='warning'>".$e->errorMessage()."</div>"; //Pretty error messages from PHPMailer
                    $backemailArray['status'] ='1';
				} catch (Exception $e) {
				  $_SESSION['msg']=  "<div class='warning'>".$e->getMessage()."</div>"; //Boring error messages from anything else!
                   $backemailArray['status'] ='1';
				}				
				################################### Mail Template End   ############################
                 $emailObj->getEmailBackup($backemailArray);   
				 
						}
			$userObj->editUserprofilesValueById($userArray, $user_id); 
	       		$_SESSION['msg'] = "<div class='success_msg'><span>Fan user update successfully</span></div>";
			header('location:'.SITE_URL.'/admin/home.php?q=fans&page='.$_GET['page'].'&order_field='.$orderField.'&order_by='.$orderBy.'&search='.$searchindex);
			exit;
		
			
	} else { // php validation else
		$_SESSION['msg'] = '<div class="error_msg"><span>Please fill all required fields</span></div>';
	}
	// main if closed
}

## Fetch admin user details by id
if($_GET['id'] != '' && $_GET['action'] == 'edit') {
	$user_id = base64_decode($_GET['id']);	
	$user_details = $userObj->getUserDetailsByUserId($user_id);
	$smarty->assign('user_details', $user_details);

	/********* User Profile Information *****************/
	$userprofileinfo = $userObj->getUserProfileDetailByUserID($user_details['userId'],$user_details['userType']);
	$smarty->assign('userprofileinfo', $userprofileinfo);
}

## Assign success or error msg to smarty variable and unset session variable
if(trim($_SESSION['msg']) != '') {
	$smarty->assign('msg', $_SESSION['msg']);
	unset($_SESSION['msg']);
}

## Set active class variable for left menu
$smarty->assign('activeclass', 'fans');
$smarty->assign('mainmenu', '3');
## Unset all the objects created which are on this page
unset($userObj);

$smarty->display(TEMPLATEDIR_ADMIN. 'controller/fans/editfans.tpl');
unset($smarty);
?>
