<?php
## include required files
/*******************************/
require_once '../model/news.php';
require_once '../model/sports.php';
require_once "../model/common/classes/pagination_class.php";
## Initializing Objects
/**************************/
$newsObj 	= new Model_News();
$sportsObj      = new Model_Sports();
/**************************/
if(isset($_REQUEST['id']) && $_REQUEST['id']!='')
	{
		$id = base64_decode($_REQUEST['id']);
		$newsArray = $newsObj->getNewsDetailsById($id);
		$smarty->assign('newsArray',$newsArray);
	}
	if($_SESSION['msg']!='')
	{
		$smarty->assign('msg',$_SESSION['msg']);
		unset($_SESSION['msg']);
	}
	
	$smarty->assign('activeclass','news');
	$smarty->assign('mainmenu', '3');

## Get selected sports category
$selectedSports  = explode(",",$newsArray['sportIds']);
$smarty->assign("selectedSports",$selectedSports);

## Fetch all sports list 
$sportsArray =  $sportsObj->getAllActiveSports();
$smarty->assign('sportsArray', $sportsArray);
	
## Unset all objects which are on this page	
unset($newsObj);
$smarty->display(TEMPLATEDIR_ADMIN. 'controller/news/details.tpl');
unset($smarty);
?>
