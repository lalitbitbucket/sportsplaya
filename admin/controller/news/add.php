<?php 
## include required files
/*******************************/
require_once '../model/news.php';
require_once '../model/sports.php';
require_once '../model/common/image_functions.php';
/*******************************/
## Create Objects
/*******************************/
$newsObj   = new Model_News();
$sportsObj  = new Model_Sports();
/*******************************/

## Get search parameters in variables - 
if($_POST['search'] != '' && $_POST['search'] != 'Search') {
	$searchindex = ucfirst($_POST['search']);
} else if($_GET['search'] != '') {
	$searchindex = $_GET['search'];
} else {
	$searchindex = '';
}
$searchindex=trim($searchindex);
// Assign search variable to show in search textbox
$smarty->assign('search', $searchindex);
if(isset($_GET['order_field']) && $_GET['order_field'] != '') {
	$orderField = $_GET['order_field'];
}
else {
	$orderField = 'title';
}
$smarty->assign('orderField', $orderField);

if(isset($_GET['order_by']) && $_GET['order_by'] != '') {
	$orderBy = $_GET['order_by'];
}
else {
	$orderBy = 'ASC';
}
$smarty->assign('orderBy', $orderBy);

if(isset($_POST['addnews_btn'])) {
## apply PHP validation for required filed
	if(trim($_POST['title']) != '' && trim($_POST['description']) != '') {
		$newsAddArray = array();	
		//echo '<pre>'; print_r($_POST); exit;	
		if($_FILES['image']['name']){
		## check valid image extension
		//$fileExt = file_extension($_FILES['image']['name']); 
		//$validExt = valid_extension($fileExt);
		## upload file
		$fileName      = time()."_".$_FILES['image']['name'];
		$temp=$_FILES["image"]["tmp_name"];	
		$imageFolder='../dynamicAssets/news/';
		copy($_FILES["image"]["tmp_name"],$imageFolder.$fileName);
		$newsAddArray['image'] = $fileName;
		
		## creating thumb images
		$imageName= $fileName;
		$thumbFolder1 ="../dynamicAssets/news/150x150/";						
		$height1 = 150;
		$width1= 150;
		$file1=uploadImageAndCreateThumb($imageName,$imageFolder,$thumbFolder1,$height1,$width1,$fileExt);
		
		$thumbFolder2 ="../dynamicAssets/news/90x90/";						
		$height2 = 90;
		$width2= 90;
		$file2=uploadImageAndCreateThumb($imageName,$imageFolder,$thumbFolder2,$height2,$width2,$fileExt);
		}
		
		$newsAddArray['title'] 			 = return_post_value($_POST['title']);
		$newsAddArray['description'] 		 = return_post_value($_POST['description']);
		$newsAddArray['sportIds'] 		 = implode(",",$_POST['sportIds']);
		$newsAddArray['addedDate'] 		 = date('Y-m-d');
		$newsAddArray['status']      		 = '2';
		//echo"Hello"; exit;
		$newsID = $newsObj->addNewsByValue($newsAddArray);
		
		$_SESSION['msg'] = "<div class='success_msg'><span>News added successfully</span></div>";
		header('location:'.SITE_URL.'/admin/home.php?q=news&page='.$_POST['page']);
		exit();
	}
	else {
		$_SESSION['msg'] = '<div class="error_msg"><span>Please fill all the required fields </span></div>';
	}
}

## Assign success or error msg to smarty variable and unset session variable
if(trim($_SESSION['msg']) != '') {
	$smarty->assign('msg', $_SESSION['msg']);
	unset($_SESSION['msg']);
}

## Fetch all sports list 
$sportsArray =  $sportsObj->getAllActiveSports();
$smarty->assign('sportsArray', $sportsArray);

## Set active class variable for left menu
$smarty->assign('activeclass', 'news');
$smarty->assign('mainmenu', '5');

## Unset all the objects created which are on this page
unset($sportsObj);
unset($newsObj);

$smarty->display(TEMPLATEDIR_ADMIN. 'controller/news/add.tpl');
unset($smarty);
?>
