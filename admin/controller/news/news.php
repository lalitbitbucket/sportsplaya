<?php  
## include required files
/*******************************/
require_once '../model/news.php';
//require_once '../model/moduleuser.php';
## Paginaton class
require_once ("../model/common/classes/pagination_class.php");
/*******************************/
## Create Objects
/*******************************/
$newsObj = new Model_News();
//$moduleObj=new Model_ModuleUser();
/*******************************/

## Active, Inactive selected records
/*******************************/
/*## check user's permission
if($modules_new[5]['permission_status'] !=1 || $modules_new[5]['submod_28'] !=1) {
	header('location: '.SITE_URL.'/admin/home.php');
	exit;
}*/

## Get search parameters in variables - 
if($_REQUEST['search'] != '' && $_REQUEST['search'] != 'Search') {
	$searchindex = $_REQUEST['search'];
} else if($_REQUEST['search'] != '') {
	$searchindex = $_REQUEST['search'];
} else {
	$searchindex = '';
}
$searchindex=trim($searchindex);
// Assign search variable to show in search textbox
$smarty->assign('search', $searchindex);
if(isset($_GET['order_field']) && $_GET['order_field'] != '') {
	$orderField = $_GET['order_field'];
}
else {
	$orderField = 'title';
}
$smarty->assign('orderField', $orderField);

if(isset($_GET['order_by']) && $_GET['order_by'] != '') {
	$orderBy = $_GET['order_by'];
}
else {
	$orderBy = 'ASC';
}
$smarty->assign('orderBy', $orderBy);

//error_reporting(E_ALL);
if(isset($_POST['go']) != '' && isset($_POST['action']) != '') {
		$ids = implode(",", $_POST['checkall']);
		## check which action is selected
		$hidden_page = $_POST['hidden_pageno'];	
		if($_POST['action'] == 'active') {
			## Active selected records
			$newsObj->updateMultipleNewstatus($ids, '2');
			$_SESSION['msg']="<div class='success_msg'><span>News status activated successfully.</span></div>";
		} else if($_POST['action'] == 'inactive') {
			## Inactive selected records
			$newsObj->updateMultipleNewstatus($ids, '1');
			$_SESSION['msg']="<div class='success_msg'><span>News status deactivated successfully.</span></div>";
		} else {
			## Delete selected records
			$newsObj->deleteMultipleNews($ids);
			$_SESSION['msg']="<div class='success_msg'><span>News deleted successfully</span></div>";
		}
		header('location:'.SITE_URL.'/admin/home.php?q=news&page='.$hidden_page.'&order_by='.$orderBy.'&search='.$searchindex.'&order_field='.$orderField);
		exit;
		## Delete code will be here if required
} // if close

## Active/Inactive/Delete thourgh URL
if(isset($_GET['action']) == 'status' && isset($_GET['status']) != '') {
	$id = base64_decode($_GET['id']);		
	## Update training page status 
	$array = array();
	$array['status'] = ($_GET['status']=='1'?2:1);
	$newsObj->updateNewstatus($id, $array['status']);
	if($_GET['status']==1) {
		$_SESSION['msg']="<div class='success_msg'><span>News status activated successfully.</span></div>";
	} else {
		$_SESSION['msg']="<div class='success_msg'><span>News status deactivated successfully.</span></div>";
	}
	header('location:'.SITE_URL.'/admin/home.php?q=news&page='.$_GET['page'].'&order_by='.$orderBy.'&search='.$searchindex.'&order_field='.$orderField);
	exit;
}

if(isset($_GET['action']) == 'delete' && isset($_GET['id']) != '') {
	$id = base64_decode($_GET['id']);
	## Delete Training
	$newsObj->deleteNews($id);
	$_SESSION['msg']="<div class='success_msg'><span>News deleted successfully</span></div>";
	header('location:'.SITE_URL.'/admin/home.php?q=news&page='.$_GET['page']);
	exit;
}

/*******************************/
/*if($_GET['q']=='news')
{
	$moduelArray=$moduleObj->getModuleHelpText(13);
	$smarty->assign('moduelArray', $moduelArray);
}*/

##  --------- Pagination part first start --------------##
	if( isset( $_REQUEST['page'] ) && $_REQUEST['page'] != "" ) {
		$pageNum = $_REQUEST['page'];
	} else {
		$pageNum = 1;
	} 
	//echo $pageNum;exit;
	$rowsPerPage = ($_GET['setlimit']) ? $_GET['setlimit'] : ROW_PER_PAGE;
	
	$pageName = "home.php?q=news&order_by=".$orderBy."&search=".$searchindex."&order_field=".$orderField."&page=".$_GET['page']."&setlimit=".$_GET['setlimit']; 
	## Count all the records
	$newsArray = $newsObj->getAllNews($searchindex,$orderField, $orderBy);
	$total_rows = count($newsArray);
	$offset	= ($pageNum - 1) * $rowsPerPage;
	$smarty->assign('page', $pageNum);
##  --------- Pagination part first end --------------##

##  --------- Pagination part second start --------------##
	$newid = $pageNum * $rowsPerPage - $rowsPerPage + 1;
	$smarty->assign('newid', $newid);
	$newsArray = $newsObj->getAllNews($searchindex,$orderField, $orderBy,$rowsPerPage,$offset);
	//echo "<pre>"; print_r($newsArray);exit;
    
	$i=0;
	foreach($newsArray as $array)
	{
	$newshitcount = $newsObj->getNewsHitCountByNewsId($array['id']);
	$newsArray[$i]['totalhitcount']=$newshitcount;
	$userIdArray = $newsObj->getUserIdByNewsId($array['id']);
	$newsArray[$i]['userId']=$userIdArray['userprofileId'];
	$i++;
	}
	
	//echo "<pre>"; print_r($newsArray);exit;
	$smarty->assign('newsArray', $newsArray);
	
	if($searchindex != '') {
		$other_id = "search=".$searchindex;
	} else {
		$other_id = '';
	}
	
	if($total_rows > ROW_PER_PAGE) {
		$pg = new pagination();					// CREATING OBJECT OF THE PAGINATION CLASS
		$pgnation = $pg->pagination_admin( $total_rows , $rowsPerPage , $pageNum , $pageName , $other_id, $class);		
		$smarty-> assign("pagination",$pgnation);
	}
##  --------- Pagination part second end --------------##	

## Assign session message to smarty variable and unset session variable
if($_SESSION['msg'] != '') {
	$smarty->assign('msg', $_SESSION['msg']);	
	unset($_SESSION['msg']);
}

## Set active class variable for left menu  (main menu and sub menu)
$smarty->assign('activeclass', 'news');
$smarty->assign('mainmenu', '5');

## Unset all the objects created which are on this page
unset($newsObj);
unset($moduleObj);
$smarty->display(TEMPLATEDIR_ADMIN. 'controller/news/news.tpl');
unset($smarty);
?>
