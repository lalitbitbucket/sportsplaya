<?php
## include required files
/*******************************/
require_once '../model/news.php';
require_once '../model/sports.php';
require_once '../model/common/image_functions.php';
/*******************************/
## Create Objects
/*******************************/
$newsObj 	= new Model_News();
$sportsObj      = new Model_Sports();
/*******************************/

##get training id from edit page as in the hidden form
//print_r($_GET); exit;
$newsid = base64_decode($_GET['id']);
$smarty->assign('newsid', $newsid);

## Get search parameters in variables - 
if($_POST['search'] != '' && $_POST['search'] != 'Search') {
	$searchindex = ucfirst($_POST['search']);
} else if($_GET['search'] != '') {
	$searchindex = $_GET['search'];
} else {
	$searchindex = '';
}
$searchindex=trim($searchindex);
// Assign search variable to show in search textbox
$smarty->assign('search', $searchindex);
if(isset($_GET['order_field']) && $_GET['order_field'] != '') {
	$orderField = $_GET['order_field'];
}
else {
	$orderField = 'title';
}
$smarty->assign('orderField', $orderField);

if(isset($_GET['order_by']) && $_GET['order_by'] != '') {
	$orderBy = $_GET['order_by'];
}
else {
	$orderBy = 'ASC';
}
$smarty->assign('orderBy', $orderBy);

//echo "<pre/>";print_r($_POST); exit;	

if(isset($_POST) && !empty($_POST)) {
	## apply PHP validation for required filed
if(trim($_POST['title']) != '' && trim($_POST['description']) != '') {
		//echo "<pre/>";print_r($_POST); exit;	
		$newseditArray = array();

		if($_FILES['image']['name']){
			## check valid image extension
			## upload file
			$fileName      = time()."_".$_FILES['image']['name'];
			$temp=$_FILES["image"]["tmp_name"];	
			$imageFolder='../dynamicAssets/news/';
			copy($_FILES["image"]["tmp_name"],$imageFolder.$fileName);
			$newseditArray['image'] = $fileName;
			
			## creating thumb images
			$imageName= $fileName;
			$thumbFolder1 ="../dynamicAssets/news/150x150/";						
			$height1 = 150;
			$width1= 150;
			$file1=uploadImageAndCreateThumb($temp,$imageName,$imageFolder,$thumbFolder1,$height1,$width1);
			
			$thumbFolder3 ="../dynamicAssets/news/90x90/";						
			$height3 = 90;
			$width3= 90;
			$file3=uploadImageAndCreateThumb($temp,$imageName,$imageFolder,$thumbFolder3,$height3,$width3);
			
		}
		
		$newseditArray['title'] 		= return_post_value($_POST['title']);
		$newseditArray['description'] 		 = return_post_value($_POST['description']);
		$newseditArray['sportIds'] 		 = implode(",",$_POST['sportIds']);
		//$newseditArray['addedDate'] 		 = date('Y-m-d');
		$newseditArray['status']      		 = '2';
		//echo "<pre>"; print_r($trainingid);exit;
		
		$newsObj->editNewsValueById($newseditArray,$_POST['news_id']);
		
		$_SESSION['msg'] = "<div class='success_msg'><span>News edited successfully</span></div>";
		header('location:'.SITE_URL.'/admin/home.php?q=news&page='.$_POST['page'].'&order_by='.$orderBy.'&search='.$searchindex.'&order_field='.$orderField);
		exit();
	} else {// php validation else
		$_SESSION['msg'] = '<div class="error_msg"><span>Please fill all the required fields </span></div>';
	}
	// main if closed
}

if($_GET['id'] != ''&& $_GET['action'] == 'edit') {
	$news_id = base64_decode($_GET['id']);
	$newsDetArray = $newsObj->getNewsDetailsById($news_id);
	//echo "<pre>"; print_r($newsDetArray);exit;
	$smarty->assign('newsDetArray', $newsDetArray);
}

## Get selected sports category
$selectedSports  = explode(",",$newsDetArray['sportIds']);
$smarty->assign("selectedSports",$selectedSports);

## Fetch all sports list 
$sportsArray =  $sportsObj->getAllActiveSports();
$smarty->assign('sportsArray', $sportsArray);

## Assign success or error msg to smarty variable and unset session variable
if(trim($_SESSION['msg']) != '') {
	$smarty->assign('msg', $_SESSION['msg']);
	unset($_SESSION['msg']);
}

## Set active class variable for left menu
$smarty->assign('activeclass', 'news');
$smarty->assign('mainmenu', '5');

## Unset all the objects created which are on this page
unset($sportsObj);
unset($newsObj);
$smarty->display(TEMPLATEDIR_ADMIN. 'controller/news/edit.tpl');
unset($smarty);
?>
