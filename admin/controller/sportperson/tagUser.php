<?php
require_once '../model/moduleuser.php';
require_once "../model/common/classes/pagination_class.php";
require_once '../model/users.php';
## Initializing Objects
/**************************/
$moduleObj 	= new Model_ModuleUser();
$userObj 	= new Model_Users();
/**************************/
$userID = base64_decode($_GET['id']);

$userprofileinfo = $userObj->getUserProfileDetailsByUserIdAtAdminSide($userID);
$smarty->assign('userprofileinfo', $userprofileinfo);

if(isset($_POST['search_from']) != '' && $_POST['search_from'] != 'From Date') {
	$search_from = trim(addslashes($_POST['search_from']));
} else if(isset($_GET['search_from']) &&  $_GET['search_from']!= 'From Date') {
	$search_from = trim(addslashes($_GET['search_from']));
} else {
	$search_from = '';
}

if(isset($_POST['search_to']) != '' && $_POST['search_to'] != 'To Date') {
	$search_to = trim(addslashes($_POST['search_to']));
} else if(isset($_GET['search_to']) &&  $_GET['search_to']!= 'To Date') {
	$search_to = trim(addslashes($_GET['search_to']));
} else {
	$search_to = '';
}

#-------- Assign search variable to show in search textbox -------#
$smarty->assign('search_from', $search_from);
$smarty->assign('search_to', $search_to);
##------------ Active/Inactive and delete  by multiple ids -----------------##
if(isset($_REQUEST['go']) && $_REQUEST['go']!='' && $_REQUEST['action']!=''  )
{
 $ids = $_REQUEST['checkall'];
 $ids = implode(',',$ids);
 if(trim($ids)!='')
 {
	 $hidden_pageno = $_REQUEST['hidden_pageno'];
	 $userObj->deleteLoginHistory($ids);
	 $_SESSION['msg'] = "<div class='success_msg'><span>Tag user deleted successfully </span></div>";
 }
}
## --------------------------Delete single user by id ----------------------------##
 if(isset($_GET['action']) && $_GET['action']=='delete' && $_GET['id']!='')
 {
	 $id = base64_decode($_GET['id']);
	 $userObj->deleteUserLoginHistory($id);
	 $_SESSION['msg'] = "<div class='success_msg'><span>Tag user deleted successfully.</span></div>";
	 header('location:'.SITE_URL.'/admin/home.php?q=tagUser&userid='.$_GET['userid'].'&page='.$_GET['page'].'&setlimit='.$_GET['setlimit']);
	 exit;
 }
 
##
if(isset($_REQUEST['order_field']) && $_REQUEST['order_field'] != '') 
{
	$orderField = $_REQUEST['order_field'];
}
else 
{
	$orderField = '';
}
$smarty->assign('orderField', $orderField);

if(isset($_REQUEST['order_by']) && $_REQUEST['order_by'] != '') {
	$orderBy = $_REQUEST['order_by'];
}
else {
	$orderBy = '';
}
$smarty->assign('orderBy', $orderBy);
/******************************************/
##  --------- Pagination part first start --------------##
/******************************************/
 if(isset($_REQUEST['page']) && !empty($_REQUEST['page']))
 {
	 $pageNum = $_REQUEST['page'];
 }else{
	 $pageNum = 1;
 }
  $rowsperpage = ($_GET['setlimit'])? ($_GET['setlimit']): ROW_PER_PAGE;
 $smarty->assign('rowsperpage',$rowsperpage);
 $pageName = "home.php?q=tagUser&userid=".$_GET['userid']."&setlimit=$rowsperpage&search=$search&order_field=$order_field&order_by=$order_by";

 $siteuserArray = $userObj->getTagUsersFromAdminSide($userprofileinfo['id'],$search_from,$search_to,$orderField,$orderBy);
//echo "<xmp>"; print_r($buyerArray); echo "</xmp>"; 
 $total_rows = count($siteuserArray);
 $offset = ($pageNum - 1)*$rowsperpage;
 $smarty->assign('page',$pageNum);
 
 $arr			=	array();
 $interval		=	2;
 for($i = $interval; $i <= $total_rows; $i = $i + $interval)
 {
  $arr[]	=	$i;
 }
$smarty->assign('arr', $arr);
$cnt	=	count($siteuserArray);
$smarty->assign('cnt', $cnt);
##  --------- Pagination part first ends --------------##
		
##  --------- Pagination part second start --------------##
$newid = $pageNum * $rowsperpage - $rowsperpage + 1;
$smarty->assign('newid', $newid);
$siteuserArray = $userObj->getTagUsersFromAdminSide($userprofileinfo['id'],$search_from,$search_to,$orderField, $orderBy,$rowsperpage,$offset);
$i=0;
foreach($siteuserArray as $tagArr){
## group members count
$tagUserDetails = $userObj->getProfileDetailsByUserProfileId($tagArr['frndid']);  
$siteuserArray[$i]['name'] 		= $tagUserDetails['fname'].' '.$tagUserDetails['lname'];
$siteuserArray[$i]['username'] 		= $tagUserDetails['username'];
$siteuserArray[$i]['avtar'] 		= $tagUserDetails['avatar'];
$siteuserArray[$i]['sex'] 		= $tagUserDetails['sex'];
$siteuserArray[$i]['sportName'] 	= $tagUserDetails['sportName'];
$i++;
}
$smarty->assign('siteuserArray', $siteuserArray);

if($searchindex != '') {
	$other_id = "search=".$searchindex;
} else {
	$other_id = '';
}	
if($total_rows > $rowsperpage) {
	$pg = new pagination();					// CREATING OBJECT OF THE PAGINATION CLASS
	$pgnation = $pg->pagination_admin( $total_rows , $rowsperpage , $pageNum , $pageName , $other_id, $class);	
	$smarty-> assign("pagination",$pgnation);
	}
##  --------- Pagination part second end --------------##	
	
##Assign Success Error messages
/***************************/
if($_SESSION['msg']!='') {
	$smarty->assign('msg',$_SESSION['msg']);
	unset($_SESSION['msg']);
}

$smarty->assign('activeclass','users');
$smarty->assign('mainmenu', '3');
	
## Unset all objects which are on this page	
unset($moduleObj);
unset($userObj);
$smarty->display(TEMPLATEDIR_ADMIN. 'controller/sportperson/tagUser.tpl');
unset($smarty);
?>
