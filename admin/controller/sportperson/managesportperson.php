<?php
## include required files
/*******************************/
require_once '../model/moduleuser.php';
require_once "../model/common/classes/pagination_class.php";
require_once '../model/users.php';
require_once '../model/videos.php';
require_once '../model/album.php';
require_once '../model/email.php';

## Initializing Objects
/**************************/
$moduleObj = new Model_ModuleUser();
$userObj   = new Model_Users();
$videoalbumObj  = new Model_videos();
$photoalbumObj  = new Model_Album();
$emailObj  = new Model_Email();
/**************************/
if(isset($_REQUEST['search']) && !empty($_REQUEST['search']))
{
$search = $_REQUEST['search'];
$search = str_replace("'","",$search);
$search = ucfirst(stripslashes($search));
$smarty->assign('search',$search);
}

if(isset($_REQUEST['order_field']) && !empty($_REQUEST['order_field']))
{
$OrderField = $_REQUEST['order_field'];
$smarty->assign('order_field',$OrderField);
}

if(isset($_REQUEST['order_by']) && !empty($_REQUEST['order_by']))
{
$OrderBy = $_REQUEST['order_by'];
$smarty->assign('order_by',$OrderBy);
}
##------------ Active/Inactive and delete  by multiple ids -----------------##
if(isset($_REQUEST['go']) && $_REQUEST['go']!='' && $_REQUEST['action']!=''  )
{
$ids = $_REQUEST['checkall'];
$ids = implode(',',$ids);
if(trim($ids)!='')
{
$hidden_pageno = $_REQUEST['hidden_pageno'];
if($_POST['action']=='active')
{
$sellerArray = $userObj->updateUserStatus($ids,2);
$sellerArray = $userObj->updateUserProfileStatus($ids,2);				 
$_SESSION['msg']="<div class='success_msg'><span>Sportsperson(s) activated successfully</span></div>";
}elseif($_POST['action']=='inactive')
{
$sellerArray = $userObj->updateUserStatus($ids,1);
$sellerArray = $userObj->updateUserProfileStatus($ids,1);
$_SESSION['msg']="<div class='success_msg'><span>Sportsperson(s) deactivated successfully</span></div>";
}
else{
$userObj->deleteUsers($ids);
$userObj->deleteUserprofile($ids);
$_SESSION['msg'] = "<div class='success_msg'><span>Sportsperson(s) deleted successfully </span></div>";
}
}
}
## --------------------------Delete reset default cover image ----------------------------##
if(isset($_GET['action']) && $_GET['action']=='setcoverimage' && $_GET['id']!='')
{
$id = base64_decode($_GET['id']);
$array = array();
$array['isDefault'] = '0';
$userObj->editCoverimageValueById($array,$id);
$_SESSION['msg'] = "<div class='success_msg'><span>Reset cover image successfully</span></div>";
header('location:'.SITE_URL.'/admin/home.php?q=sportperson&page='.$_GET['page']);
exit;
}

## --------------------------Delete single user by id ----------------------------##
if(isset($_GET['action']) && $_GET['action']=='delete' && $_GET['id']!='')
{
$id = base64_decode($_GET['id']);
$array = array();
$array['status'] = '0';
$userObj->editUserValueById($array, $id);
$userObj->editUserProfile($array, $id);
$_SESSION['msg'] = "<div class='success_msg'><span>Sportsperson  deleted succesfully </span></div>";
header('location:'.SITE_URL.'/admin/home.php?q=sportperson&page='.$_GET['page']);
exit;
}

## --------------------------Active/Inactive  single user by id ----------------------------##
if(isset($_GET['action']) && $_GET['action']=='status' && $_GET['id']!='')
{
$id = base64_decode($_GET['id']);
$array = array();
$array['status'] = ($_GET['status']=='2'?1:2);
$userObj->editUserValueById($array, $id);
$userObj->editUserProfile($array,$id);
## Fetch email content
$_SESSION['msg']="<div class='success_msg'><span>Sportsperson updated successfully</span></div>";
header('location:'.SITE_URL.'/admin/home.php?q=sportperson&page='.$_GET['page']);
exit;
}

if(isset($_POST['search']) && $_POST['search']!= 'Username and Email')
{
$searchindex = trim($_POST['search']);
}
elseif($_GET['search']!='')
{
$searchindex = trim($_GET['search']);	
}
else{
$searchindex = '';	
}
$searchindex = str_replace("'","",$searchindex);
$searchindex = ucfirst(stripslashes($searchindex));
$smarty->assign('search',$searchindex);

##
if(isset($_REQUEST['order_field']) && $_REQUEST['order_field'] != '') {
$orderField = $_REQUEST['order_field'];
}
else {
$orderField = 'fname';
}
$smarty->assign('orderField', $orderField);

if(isset($_REQUEST['order_by']) && $_REQUEST['order_by'] != '') {
$orderBy = $_REQUEST['order_by'];
}
else {
$orderBy = 'ASC';
}
$smarty->assign('orderBy', $orderBy);

/******************************************/
##  --------- Pagination part first start --------------##
/******************************************/
if(isset($_REQUEST['page']) && !empty($_REQUEST['page']))
{
$pageNum = $_REQUEST['page'];
}else{
$pageNum = 1;
}
$rowsperpage = ($_GET['setlimit'])? ($_GET['setlimit']): ROW_PER_PAGE;
$smarty->assign('rowsperpage',$rowsperpage);
$pageName = "home.php?q=sportperson&setlimit=$rowsperpage&search=$search&order_field=$OrderField&order_by=$OrderBy";

$siteuserArray = $userObj->getSiteUserDetailsByUserType($searchindex,$OrderField,$OrderBy);
$total_rows = count($siteuserArray);
$offset = ($pageNum - 1)*$rowsperpage;
$smarty->assign('page',$pageNum);
$arr = array();
$interval =2;
for($i = $interval; $i <= $total_rows; $i = $i + $interval)
{
$arr[]	= $i;
}

$smarty->assign('arr', $arr);
$cnt	= count($siteuserArray);
$smarty->assign('cnt', $cnt);
##  --------- Pagination part first ends --------------##

##  --------- Pagination part second start --------------##
$newid = $pageNum * $rowsperpage - $rowsperpage + 1;
$smarty->assign('newid', $newid);
$siteuserArray = $userObj->getSiteUserDetailsByUserType($searchindex,$OrderField, $OrderBy,$rowsperpage,$offset);
$i=0;
foreach($siteuserArray as $userinfo)
{
//echo"<pre>"; print_r($userinfo); exit;
$userprofileinfo = $userObj->getUserProfileDetailByUserID($userinfo['userId'],$userinfo['userType']);
$siteuserArray[$i]['name'] = $userprofileinfo['fname'].' '.$userprofileinfo['lname'];
$siteuserArray[$i]['id'] = $userprofileinfo['id'];
## Display user public profile default image
if($userprofileinfo['id']!= '')
{
$publicProfileDefaultImage = $userObj->getNeedPublicProfileimageById($userprofileinfo['id']);
$siteuserArray[$i]['userTimeLineId'] = $publicProfileDefaultImage['id'];
$siteuserArray[$i]['userTimeLineImage'] = $publicProfileDefaultImage['imageName'];
}
if($userprofileinfo['id']!= '')
{
$albumCount = $videoalbumObj->getVideoAlbumCountByUserId($userprofileinfo['id']);
$siteuserArray[$i]['userVidoAlbumCount'] = $albumCount;
}
if($userprofileinfo['id']!= '')
{
$photoalbumCount = $photoalbumObj->getPhotoAlbumCountByUserId($userprofileinfo['id']);
$siteuserArray[$i]['userVidoPhotoCount'] = $photoalbumCount;
}
$i++;
}
//echo"<pre>"; print_r($siteuserArray); exit;
$j=0;
foreach($siteuserArray as $clubArr)
{
$userprofileinfo = $userObj->getUserProfileDetailByUserID($clubArr['userId'],$clubArr['userType']);
$cnt = $userObj->getClubMembersPostCount($userprofileinfo['id']);
$siteuserArray[$j]['sportCount'] = $cnt;
$siteuserArray[$j]['userProfileId'] = $userprofileinfo['id'];
$j++;
}	

$smarty->assign('siteuserArray', $siteuserArray);



if($searchindex != '') {
$other_id = "search=".$searchindex;
} else {
$other_id = '';
}	
if($total_rows > $rowsperpage) {
$pg = new pagination();					// CREATING OBJECT OF THE PAGINATION CLASS
$pgnation = $pg->pagination_admin( $total_rows , $rowsperpage , $pageNum , $pageName , $other_id, $class);	
$smarty-> assign("pagination",$pgnation);
}
##  --------- Pagination part second end --------------##

## List all sportperson users
$getAllSportpersons = $userObj->getAllSportpersonUsers();
$smarty->assign('getAllSportpersons', $getAllSportpersons);

/*******************************/
##Fetch all admin users from database
/******************************/
if($_GET['q']=='siteuser')
{
$moduelArray=$moduleObj->getModuleHelpText(10);
$smarty->assign('moduelArray', $moduelArray);
}
/***************************/
##Assign Success Error messages
/***************************/
if($_SESSION['msg']!='')
{
$smarty->assign('msg',$_SESSION['msg']);
unset($_SESSION['msg']);
}

$smarty->assign('activeclass','sportperson');
$smarty->assign('mainmenu', '3');

## Unset all objects which are on this page	
unset($moduleObj);
unset($userObj);
$smarty->display(TEMPLATEDIR_ADMIN. 'controller/sportperson/managesportperson.tpl');
unset($smarty);
?>
