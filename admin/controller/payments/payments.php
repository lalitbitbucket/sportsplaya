<?php 
## include required files
/*******************************/
require_once '../model/payments.php';
## Create Objects
/*******************************/
$paymentsObj = new Model_Payments();
/*******************************/
$paymentArray = $paymentsObj->getAllPayments();
$smarty->assign('paymentArray', $paymentArray);	

## Assign session message to smarty variable and unset session variable
if($_SESSION['msg'] != '') {
	$smarty->assign('msg', $_SESSION['msg']);	
	unset($_SESSION['msg']);
}

## Set active class variable for left menu (main menu and sub menu)
$smarty->assign('activeclass', 'CMS');
$smarty->assign('mainmenu', '1');


## Unset all the objects created which are on this page
unset($paymentsObj);
$smarty->display(TEMPLATEDIR_ADMIN. 'controller/payments/payments.tpl');
unset($smarty);
?>
