<?php 
## include required files
/*******************************/
require_once '../model/payments.php';
## Create Objects
/*******************************/
$paymentsObj = new Model_Payments();
/*******************************/
	$id= base64_decode($_GET['id']);
	$paymentArray = $paymentsObj->getPaymentsOptionsById($id);
	$smarty->assign('paymentArray', $paymentArray);	
	$paymentDetailsArray = $paymentsObj->getPaymentDetailsById($id);
	$smarty->assign('paymentDetailsArray', $paymentDetailsArray);	
	
	if(isset($_POST) && !empty($_POST)) {	
	## apply PHP validation for required filed
		
		$paymentArray = array();
		$optionIds= ($_POST['optionId']);
		$status= ($_POST['status']);
		$id= base64_decode(($_POST['id']));
		$optionNames= ($_POST['optionName']);
		$i=0;
		while($i<count($optionIds))
		{
			//echo $optionIds[$i];
			$paymentArray['value'] = $optionNames[$i];
			$paymentsObj->editPaymentOptionsById($paymentArray,$optionIds[$i]);
			$paymentArray['value'] = '';
			$i++;
		}
		$pay = array();
		$pay['status'] = $status;
		$paymentsObj->editPaymentById($pay,$id);
		//exit;	
		$_SESSION['msg'] = "<div class='success_msg'><span>Payments options  edited successfully!</span></div>";
		header('location:'.SITE_URL.'/admin/home.php?q=payments&page='.$_GET['page'].'&order_by='.$orderBy.'&search='.$searchindex.'&order_field='.$orderField.'&search_country='.$search_country.'&search_state='.$search_state);
		exit;
}
## Assign success or error msg to smarty variable and unset session variable
if(trim($_SESSION['msg']) != '') {
	$smarty->assign('msg', $_SESSION['msg']);
	unset($_SESSION['msg']);
}
## Set active class variable for left menu
$smarty->assign('activeclass', 'CMS');
$smarty->assign('mainmenu', '1');

## Unset all the objects created which are on this page
unset($cmsObj);

$smarty->display(TEMPLATEDIR_ADMIN. 'controller/payments/editPayments.tpl');
unset($smarty);
?>
