<?php 
## include required files
/*******************************/
require_once '../model/users.php';
require_once '../model/socialsites.php';
/*******************************/

## Create Objects
/*******************************/
$userObj = new Model_Users();
$socialObj = new Model_SocialSites();

## Check super admin
if($_SESSION["admin_type"]!='1'){
	header("Location:".SITE_URL."/admin/");
	exit();
}

$socialArrayArray=$socialObj->getAllsocialSites();
$smarty->assign('socialArrayArray', $socialArrayArray);


unset($userObj);
$smarty->display(TEMPLATEDIR_ADMIN. '/controller/sitesettings/socialsettings.tpl');
unset($smarty);
?>