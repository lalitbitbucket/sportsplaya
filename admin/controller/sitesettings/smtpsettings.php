<?php 
## include required files
/*******************************/
require_once '../model/users.php';
require_once '../model/email.php';
/*******************************/

## Create Objects
/*******************************/
$userObj = new Model_Users();
$emailObj = new Model_Email();

## Check super admin
if($_SESSION["admin_type"]!='1'){
	header("Location:".SITE_URL."/admin/");
	exit();
}

$smtpArray=$emailObj->getAllSMTPServers();
$smarty->assign('smtpArray', $smtpArray);


unset($userObj);
$smarty->display(TEMPLATEDIR_ADMIN. '/controller/sitesettings/smtpsettings.tpl');
unset($smarty);
?>