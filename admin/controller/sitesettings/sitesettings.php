<?php 
## include required files
/*******************************/
require_once '../model/users.php';
/*******************************/

## Create Objects
/*******************************/
$userObj = new Model_Users();


## Check super admin
if($_SESSION["admin_type"]!='1'){
	header("Location:".SITE_URL."/admin/");
	exit();
}

unset($userObj);
$smarty->display(TEMPLATEDIR_ADMIN. '/controller/sitesettings/sitesettings.tpl');
unset($smarty);
?>