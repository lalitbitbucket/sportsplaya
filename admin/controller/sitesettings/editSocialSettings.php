<?php 
## include required files
/*******************************/

require_once '../model/users.php';
require_once '../model/socialsites.php';
/*******************************/

## Create Objects
/*******************************/
$userObj = new Model_Users();
$socialObj = new Model_SocialSites();

## Check super admin
if($_SESSION["admin_type"]!='1'){
	header("Location:".SITE_URL."/admin/");
	exit();
}

## fetch dettings values
if(isset($_GET['id'])) {

	$id = base64_decode($_GET['id']);
	
	
	$socialOptionsArray =$socialObj->getSocialOptionsById($id);
	$socialArray = $socialObj->getSocialDetailsById($id);
	$smarty->assign('socialArray', $socialArray);
	$smarty->assign('socialOptionsArray', $socialOptionsArray);
}
//print_r($_POST);
if($_POST['socialId']!='')
{
	//print_r($_POST);exit;
	
	if($_FILES['icon']['name']!='')
	{
		$filename = time().$_FILES['icon']['name'];
		move_uploaded_file($_FILES['icon']['tmp_name'],'../dynamicAssets/socialicons/'.$filename);
	}
	$array=array();
	$socialId = base64_decode(($_POST['socialId']));
	//$array['name']=($_POST['configName']);
	$array['title']=($_POST['title']);
	$array['logo']=$filename;
	$array['url']=($_POST['url']);
	$ids=$socialObj->editSocialSettingsByValueId($array,$socialId);
	
	//exit;
	$socialoptionsArray = array();
		$optionIds= ($_POST['optionId']);
		$status= ($_POST['status']);
		$id= base64_decode(($_POST['id']));
		$optionNames= ($_POST['optionName']);
		$i=0;
		while($i<count($optionIds))
		{
			//echo $optionIds[$i];
			$socialoptionsArray['value'] = $optionNames[$i];
			$socialObj->editSocialOptionsById($socialoptionsArray,$optionIds[$i]);
			$socialoptionsArray['value'] = '';
			$i++;
		}
		
		
	$_SESSION['msg'] = "<div class='success_msg'>Social Site settings updated successfully!</div>";
	header("location:".SITE_URL."/admin/home.php?q=socialsettings");
}

//print_r($settingsk);
unset($userObj);

$smarty->display(TEMPLATEDIR_ADMIN. '/controller/sitesettings/editSocialSettings.tpl');
unset($smarty);
?>