<?php 
## include required files
/*******************************/
require_once '../model/users.php';
/*******************************/

## Create Objects
/*******************************/
$userObj = new Model_Users();


## Check super admin
if($_SESSION["admin_type"]!='1'){
	header("Location:".SITE_URL."/admin/");
	exit();
}

## fetch dettings values
if(isset($_GET['id1'])) {

	$id = base64_decode($_GET['id1']);
	$settingsk = $commonObj->getSettingValueById($id);
	$smarty->assign('settings', $settingsk);
}
//print_r($_POST);
if($_POST['settingId']!='')
{
	//print_r($_POST);exit;
	$array=array();
	$settingId = ($_POST['settingId']);
	//$array['name']=($_POST['configName']);
	$array['value']=($_POST['configValue']);
	$ids=$commonObj->editSettingsByValueId($array,$settingId);
	$_SESSION['msg'] = "<div class='success_msg'>Site settings updated successfully!</div>";
	header("location:".SITE_URL."/admin/home.php?q=sitesettings");
}

//print_r($settingsk);
unset($userObj);

$smarty->display(TEMPLATEDIR_ADMIN. '/controller/sitesettings/editSiteSettings.tpl');
unset($smarty);
?>