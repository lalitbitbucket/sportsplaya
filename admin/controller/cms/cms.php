<?php
## include required files
/*******************************/
require_once '../model/cms.php';
require_once '../model/moduleuser.php';

## Paginaton class
require_once ("../model/common/classes/pagination_class.php");

/*******************************/
## Create Objects
/*******************************/
$cmsObj = new Model_CMS();
$moduleObj=new Model_ModuleUser();
/*******************************/
## Active, Inactive selected records
/*******************************/
if(isset($_POST['go']) != '' && isset($_POST['action']) != '') {
$ids = implode(",", $_POST['checkall']);
## check which action is selected
$hidden_page = $_POST['hidden_pageno'];	
if($_POST['action'] == 'active') {
## Active selected records
$cmsObj->updateMultipleCmsStatus($ids, '1');
$_SESSION['msg']="<div class='success_msg'><span>CMS pages activated successfully</span></div>";
} else { 
## Inactive selected records
$cmsObj->updateMultipleCmsStatus($ids, '0');
$_SESSION['msg']="<div class='success_msg'><span>CMS pages deactivated successfully</span></div>";
} 
header('location:'.SITE_URL.'/admin/home.php?q=cms&page='.$hidden_page);
exit;
}

## Active/Inactive thourgh URL
if(isset($_GET['action']) == 'status' && isset($_GET['status']) != '') {
$id = base64_decode($_GET['id']);
## Update cms page status 
$array = array();
$array['status'] = ($_GET['status']=='1'?0:1);
$cmsObj->editUserCmsById($array, $id);
$_SESSION['msg']="<div class='success_msg'><span>CMS page status updated successfully</span></div>";
header('location:'.SITE_URL.'/admin/home.php?q=cms&page='.$_GET['page']);
exit;
}
/*******************************/
## Get search parameters in variables - 
if($_REQUEST['search'] != '' && $_REQUEST['search'] != 'Search') {
	$searchindex = $_REQUEST['search'];
} else {
	$searchindex = '';
}
$searchindex=trim(return_fetched_value($searchindex));
$smarty->assign('search', $searchindex);

## get order field & order by parameters for sorting
if($_REQUEST['order_field'] != '') {
	$orderField = $_REQUEST['order_field'];
} else {
	$orderField = 'pageTitle';
}
$smarty->assign('orderField', $orderField);

if($_REQUEST['order_by'] != '') {
	$orderBy = $_REQUEST['order_by'];
} else {
	$orderBy = 'ASC';
}
$smarty->assign('orderBy', $orderBy);

##  --------- Pagination part first start --------------##
	if( isset( $_GET[ 'page' ] ) && $_GET[ 'page' ] != "" ) {
		$pageNum = $_GET[ 'page' ];
	} else {
		$pageNum = 1;
	} 
	$rowsPerPage =($_GET['setlimit']) ? $_GET['setlimit'] : ROW_PER_PAGE;
	$pageName = "home.php?q=cms&search=$searchindex&order_field=$orderField&order_by=$orderBy"; 

	## Count all the records
	$cmsArray = $cmsObj->getAllSubMenu($searchindex, $orderField, $orderBy);
		
	$total_rows = count($cmsArray);
	$offset	= ($pageNum - 1) * $rowsPerPage;
	$smarty->assign('page', $pageNum);
	##  --------- Pagination part first end --------------##

	##  --------- Pagination part second start --------------##
	$newid = $pageNum * $rowsPerPage - $rowsPerPage + 1;
	$smarty->assign('newid', $newid);
	$cmsArray = $cmsObj->getAllSubMenu($searchindex, $orderField, $orderBy, $rowsPerPage, $offset);

	$smarty->assign('cmsArray', $cmsArray);//ho "<pre>"; print_r($cmsArray);exit;  
	if($total_rows > ROW_PER_PAGE) {
		$pg = new pagination();					// CREATING OBJECT OF THE PAGINATION CLASS
		$pgnation = $pg->pagination_admin( $total_rows , $rowsPerPage , $pageNum , $pageName , $other_id, $class);	
		$smarty-> assign("pagination",$pgnation);
	}
##  --------- Pagination part second end --------------##	

## List all CMS pages
$allCMS = $cmsObj->getAllCMSs();
$smarty->assign('allCMS', $allCMS);

## Assign session message to smarty variable and unset session variable
if($_SESSION['msg'] != '') {
	$smarty->assign('msg', $_SESSION['msg']);	
	unset($_SESSION['msg']);
}

## Set active class variable for left menu (main menu and sub menu)
$smarty->assign('activeclass', 'CMS');
$smarty->assign('mainmenu', '1');

## Unset all the objects created which are on this page
unset($cmsObj);
unset($moduleObj);

$smarty->display(TEMPLATEDIR_ADMIN. 'controller/cms/cms.tpl');
unset($smarty);
?>
