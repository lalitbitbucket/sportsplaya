<?php 
## include required files
/*******************************/
require_once '../model/cms.php';
/*******************************/

## check is another acessing this module
//if($_SESSION['adminWooTronicsId'] != 2) {
//	## techmodi admin user
//	header('location:'.SITE_URL.'/admin/home.php');	
//	exit;
//}

## Create Objects
/*******************************/
$cmsObj = new Model_CMS();
/*******************************/
if(isset($_POST['addcms_btn'])) {
	## apply PHP validation for required filed
	if(trim($_POST['title']) != '' && trim($_POST['content']) != '') {
		$submenuArray = array();

		$submenuArray['pageTitle']   = return_post_value($submenu_title);
		$submenuArray['pageContents'] = return_post_value($submenu_content);
		//$submenuArray['metaTitle']      = return_post_value($meta_title);
		//$submenuArray['metaKeyword']    = return_post_value($meta_keyword);
		//$submenuArray['metaDescription']= return_post_value($meta_description);
		$cmsObj->addSubmenu($submenuArray);
		$_SESSION['msg'] = "<div class='success_msg'><span>CMS page added successfully</span></div>";
		header('location:'.SITE_URL.'/admin/home.php?q=cms');
		exit;
	} else {// php validation else
		$_SESSION['msg'] = "<div class='error_msg'><span>Please enter sub menu title and content</span></div>";
	}
	// main if closed
}

## Assign success or error msg to smarty variable and unset session variable
if(trim($_SESSION['msg']) != '') {
	$smarty->assign('msg', $_SESSION['msg']);
	unset($_SESSION['msg']);
}

## Set active class variable for left menu
$smarty->assign('activeclass', 'addCMS');
$smarty->assign('mainmenu', '1');
## Unset all the objects created which are on this page
unset($cmsObj);

$smarty->display(TEMPLATEDIR_ADMIN. 'cms/addcms.tpl');
unset($smarty);
?>