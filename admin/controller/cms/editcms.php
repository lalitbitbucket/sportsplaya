<?php 
## include required files
/*******************************/
require_once '../model/cms.php';
/*******************************/
## Create Objects
/*******************************/
$cmsObj = new Model_CMS();
/*******************************/

## Get parameters for page back searching, sorting and pagination
//get search parameters in variables - 
if($_REQUEST['search'] != '' && $_REQUEST['search'] != 'Search') {
	$search = $_REQUEST['search'];
} else {
	$search = '';
}
$smarty->assign('search', $search);

//get order field & order by parameters for sorting
if($_REQUEST['order_field'] != '') {
	$orderField = $_REQUEST['order_field'];
} else {
	$orderField = 'faq_category';
}
$smarty->assign('orderField', $orderField);

if($_REQUEST['order_by'] != '') {
	$orderBy = $_REQUEST['order_by'];
} else {
	$orderBy = 'ASC';
}
$smarty->assign('orderBy', $orderBy);

//get page number
$smarty->assign('page', $_REQUEST['page']);

if($_GET['id']!=''&& $_GET['action']=='edit') {
	$submenu_id=base64_decode($_GET['id']);
	$cmsArray=$cmsObj->getSubmenuDetailsById($submenu_id);
	$smarty->assign('cmsArray', $cmsArray);
			//echo "<pre>"; print_r($cmsArray);exit;
}

if(isset($_POST['editcms_btn'])) {
	## apply PHP validation for required filed
	if(trim($_POST['title']) != '' && trim($_POST['content']) != '') {
		$submenuArray = array();
		$submenuArray['pageTitle']   = return_post_value($_POST['title']);
		$submenuArray['pageContents'] = return_post_value($_POST['content']);
		$cmsObj->editUserCmsById($submenuArray,$_POST['id']);
		
		$_SESSION['msg']="<div class='success_msg'><span>CMS detail updated successfully!</span></div>";
		header('location:'.SITE_URL.'/admin/home.php?q=cms&search='.$search.'&order_field='.$orderField.'&order_by='.$orderBy.'&page='.$_POST['page']);
		exit;
		
	} else {// php validation else
		$_SESSION['msg']="<div class='error_msg'><span> Sub menu title and Content can't be blank</span></div>";
	}
	// main if closed
}

## Assign success or error msg to smarty variable and unset session variable
if(trim($_SESSION['msg']) != '') {
	$smarty->assign('msg', $_SESSION['msg']);
	unset($_SESSION['msg']);
}

## Set active class variable for left menu
$smarty->assign('activeclass', 'CMS');
$smarty->assign('mainmenu', '1');

## Unset all the objects created which are on this page
unset($cmsObj);

$smarty->display(TEMPLATEDIR_ADMIN. 'controller/cms/editcms.tpl');
unset($smarty);
?>
