<?php 
## include required files
/*******************************/
require_once '../model/cms.php';
/*******************************/
## Create Objects
/*******************************/
$cmsObj = new Model_CMS();
/*******************************/

if($_GET['id']!='') {
	$submenu_id=base64_decode($_GET['id']);
	$smarty->assign('page', $_GET['page']);
	$cmsArray=$cmsObj->getSubmenuDetailsById($submenu_id);
	$smarty->assign('cmsArray', $cmsArray); 
}

## Set active class variable for left menu
$smarty->assign('activeclass', 'CMS');
$smarty->assign('mainmenu', '1');

## Unset all the objects created which are on this page
unset($cmsObj);

$smarty->display(TEMPLATEDIR_ADMIN. 'controller/cms/cmsdetails.tpl');
unset($smarty);
?>