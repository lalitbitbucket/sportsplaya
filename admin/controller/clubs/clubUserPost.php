<?php  
## include required files
/*******************************/
require_once '../model/users.php';

//require_once '../model/moduleuser.php';
## Paginaton class
require_once ("../model/common/classes/pagination_class.php");
/*******************************/

## Create Objects
/*******************************/
$usersObj      = new Model_Users();
/*******************************/

$userId = $_REQUEST['userId'];
$smarty->assign('userId', $userId);

$uID = base64_decode($_REQUEST['userId']);
$smarty->assign('uID', $uID);

## Get search parameters in variables - 
if($_REQUEST['search'] != '' && $_REQUEST['search'] != 'Search') {
	$searchindex = $_REQUEST['search'];
} else if($_REQUEST['search'] != '') {
	$searchindex = $_REQUEST['search'];
} else {
	$searchindex = '';
}
$searchindex=trim($searchindex);
// Assign search variable to show in search textbox
$smarty->assign('search', $searchindex);
if(isset($_GET['order_field']) && $_GET['order_field'] != '') {
	$orderField = $_GET['order_field'];
}
else {
	$orderField = 'id';
}
$smarty->assign('orderField', $orderField);

if(isset($_GET['order_by']) && $_GET['order_by'] != '') {	
	$orderBy = $_GET['order_by'];
}
else {
	$orderBy = 'ASC';
}
$smarty->assign('orderBy', $orderBy);

if(isset($_GET['action']) == 'delete' && isset($_GET['id']) != '') 
{
	$id = base64_decode($_GET['id']);
	$usersObj->deletePostByID($id);
	$_SESSION['msg']="<div class='success_msg'><span>User Post deleted successfully</span></div>";
	header('location:'.SITE_URL.'/admin/home.php?q=clubUserPost&userId='.$_REQUEST['userId']);
	exit();
}


if(isset($_GET['action']) == 'delete' && isset($_GET['id']) != '') 
{
	$id = base64_decode($_GET['id']);
	$usersObj->deletePostByID($id);
	$_SESSION['msg']="<div class='success_msg'><span>User Post deleted successfully</span></div>";
	header('location:'.SITE_URL.'/admin/home.php?q=sportUserPost&userId='.$_REQUEST['userId']);
	exit();
}



##Fetch all Training pages from database

##  --------- Pagination part first start --------------##
	if( isset( $_REQUEST['page'] ) && $_REQUEST['page'] != "" ) {
		$pageNum = $_REQUEST['page'];
	} else {
		$pageNum = 1;
	} 
	//echo $pageNum;exit;
	$rowsPerPage = ($_GET['setlimit']) ? $_GET['setlimit'] : ROW_PER_PAGE;
	
	$pageName = "home.php?q=clubUserPost&order_by=".$orderBy."&search=".$searchindex."&order_field=".$orderField."&page=".$_GET['page']."&setlimit=".$_GET['setlimit']; 
	## Count all the records
	$clubArray = $usersObj->getPostByUserId($uID,$searchindex,$orderField, $orderBy);
	$total_rows = count($clubArray);
	$offset	= ($pageNum - 1) * $rowsPerPage;
	$smarty->assign('page', $pageNum);
##  --------- Pagination part first end --------------##

##  --------- Pagination part second start --------------##
	$newid = $pageNum * $rowsPerPage - $rowsPerPage + 1;
	$smarty->assign('newid', $newid);
	$clubArray = $usersObj->getPostByUserId($uID,$searchindex,$orderField, $orderBy,$rowsPerPage,$offset);
	$smarty->assign('clubArray', $clubArray);

	//echo "<pre>";print_r($clubArray);exit;

	if($searchindex != '') {
		$other_id = "search=".$searchindex;
	} else {
		$other_id = '';
	}
	
	if($total_rows > ROW_PER_PAGE) {
		$pg = new pagination();					// CREATING OBJECT OF THE PAGINATION CLASS
		$pgnation = $pg->pagination_admin( $total_rows , $rowsPerPage , $pageNum , $pageName , $other_id, $class);		
		$smarty-> assign("pagination",$pgnation);
	}
##  --------- Pagination part second end --------------##	

## Assign session message to smarty variable and unset session variable
if($_SESSION['msg'] != '') {
	$smarty->assign('msg', $_SESSION['msg']);	
	unset($_SESSION['msg']);
}

## Set active class variable for left menu  (main menu and sub menu)
$smarty->assign('activeclass', 'club');
$smarty->assign('mainmenu', '5');

## Unset all the objects created which are on this page
unset($clubObj);
unset($moduleObj);

$smarty->display(TEMPLATEDIR_ADMIN. 'controller/clubs/clubUserPost.tpl');
unset($smarty);
?>
