<?php 
## include required files

include_once("../model/states.php");
## Create Objects
$stateObj  = new Model_States();
## Get all states by country id
$cntrID = $_POST['cntrID'];
$stateArray = $stateObj->getAllActiveStateByCountryId($cntrID);
$str ='<option value="">Select State </option>';
foreach($stateArray as $state){
	$str .="<option value=".$state['statID'].">".ucfirst($state['statName'])."</option>";
}
echo $str;
?>
