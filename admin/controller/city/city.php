<?php 
## include required files
require_once '../model/city.php';

## Create Objects
$cityObj  = new Model_City();

## Get all city by state id
$state_id = $_POST['stateId'];
$cityArray = $cityObj->getAllActiveCityByStateId($state_id);

$str ='<option value="">Select City </option>';
foreach($cityArray as $city){
	$str .="<option value=".$city['cityId'].">".ucfirst($city['cityName'])."</option>";
}
echo $str;
?>
