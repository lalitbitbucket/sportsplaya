<?php 
## include required files
include_once("../model/states.php");

## Create Objects
$stateObj  = new Model_States();

## Get all states by country id
$cntrID = $_POST['cntrID'];

## Fetch list of states using country id
$stateArray = $stateObj->getAllActiveStateByCountryId($cntrID);

$str ='<option value="">Select State </option>';
foreach($stateArray as $state){
	$str .="<option value=".$state['statID'].">".ucfirst(return_fetched_value($state['statName']))."</option>";
}
echo $str;
?>
