<?php 
## include required files
/*******************************/
require_once '../model/country.php';
require_once '../model/states.php';
require_once '../model/city.php';

/*******************************/
## Create Objects
/*******************************/
$countryObj = new Model_Country();
$stateObj=new Model_States();
$cityObj=new Model_City();
/*******************************/

## Get search parameters in variables - 
if($_POST['search'] != '' && $_POST['search'] != 'City') {
$searchindex = $_POST['search'];
} else if($_GET['search'] != '') {
$searchindex = $_GET['search'];
}else {
$searchindex = '';
}
$searchindex=trim($searchindex);

// Assign search variable to show in search textbox
$smarty->assign('search', $searchindex);

if(isset($_POST['cntrID']) != '') {
	$cntrID = $_POST['cntrID'];
} else if($_GET['cntrID'] != '') {
	$cntrID = $_GET['cntrID'];
} else {
	$cntrID = '';
}
$smarty->assign('cntrID', $cntrID);

if(isset($_POST['statID']) != '') {
	$statID = $_POST['statID'];
} else if($_GET['statID'] != '') {
	$statID = $_GET['statID'];
} else {
	$statID = '';
}
$smarty->assign('statID', $statID);

if(isset($_POST) && !empty($_POST)) {	
	## apply PHP validation for required filed
	if(trim($_POST['cntrID']) != '' && trim($_POST['statID']) != '') {
		if(trim($_POST['cityName']) != '' ) {
		
		$cityArray['cntrID']       = return_post_value($cntrID);
		$cityArray['ctName']       = return_post_value($_POST['cityName']);				
		$cityArray['statID']      = return_post_value($statID);
                $cityId = return_post_value($_POST['city_id']); 
                $cityObj->editCityById($cityArray,$cityId);
		$_SESSION['msg'] = "<div class='success_msg'><span>City edited successfully!</span></div>";
		header('location:'.SITE_URL.'/admin/home.php?q=city&page='.$_GET['page'].'&order_by='.$orderBy.'&search='.$searchindex.'&order_field='.$orderField.'&search_country='.$cntrID.'&search_state='.$statID);
		exit;
		} else {// php validation else
		$_SESSION['msg'] = "<div class='error_msg'><span>Please enter city name</span></div>";
	}
	} else {// php validation else
		$_SESSION['msg'] = "<div class='error_msg'>Please fill all required fields</span></div>";
	}
	// main if closed
}

if($_GET['id']!='')
{
	$city_id=base64_decode($_GET['id']);
	$cityArr=$cityObj->getCityDetailsById($city_id);
	$smarty->assign('cityArr', $cityArr);
}
//echo"<pre>"; print_r($cityArr); echo"<pre>"; exit;

## fetching all the country
$countryArray=$countryObj->getAllCountry();
$smarty->assign('countryArray', $countryArray);
//echo"<pre>"; print_r($countryArray); echo"<pre>"; exit;

##fetching all the state
if($cityArr['cntrID']) {
	$stateArray = $stateObj->getAllActiveStateByCountryId($cityArr['cntrID']);
        $smarty->assign('stateArray', $stateArray);
}

## Assign success or error msg to smarty variable and unset session variable
if(trim($_SESSION['msg']) != '') {
	$smarty->assign('msg', $_SESSION['msg']);
	unset($_SESSION['msg']);
}

## Set active class variable for left menu
$smarty->assign('activeclass', 'city');
$smarty->assign('mainmenu', '2');
## Unset all the objects created which are on this page
unset($countryObj);
unset($stateObj);
unset($cityObj);

$smarty->display(TEMPLATEDIR_ADMIN. 'controller/city/editCity.tpl');
unset($smarty);
?>
