<?php 
## include required files
/*******************************/
require_once '../model/country.php';
require_once '../model/states.php';
require_once '../model/city.php';

/*******************************/
## Create Objects
/*******************************/
$countryObj = new Model_Country();
$stateObj=new Model_States();
$cityObj=new Model_City();
/*******************************/
if(isset($_POST) && !empty($_POST)) {	
	## apply PHP validation for required filed
	if(trim($_POST['statID']) != '') {
		if(trim($_POST['cityName']) != '' ) {			
		
			$cityArray['cntrID']       = return_post_value($cntrID);	
			$cityArray['ctName']       = return_post_value($cityName);				
			$cityArray['statID']       = return_post_value($statID);
			$cityArray['ctStatus']     = 2;
			$cityObj->addCityByValue($cityArray);
			$_SESSION['msg'] = "<div class='success_msg'><span>City added successfully!</span></div>";
			header('location:'.SITE_URL.'/admin/home.php?q=city');
			exit;
		} else {// php validation else
		$_SESSION['msg'] = '<div class="error_msg"><span>Please enter city name</span></div>';
	}
	} else {// php validation else
		$_SESSION['msg'] = '<div class="error_msg">Please fill all required fields</span></div>';
	}
	// main if closed
}

## fetching all the country
$countryArray = $countryObj->getAllCountryWithActiveStatus();
$smarty->assign('countryArray',$countryArray);

##fetching all the state
if(isset($_POST['countryId'])) {
$stateArray = $stateObj->getAllStateByCountryId($_POST['countryId']);
$smarty->assign('stateArray', $stateArray);
}

##List of active states
$statesArray = $stateObj->getAllActiveStates();
$smarty->assign('statesArray', $statesArray);

## Assign success or error msg to smarty variable and unset session variable
if(trim($_SESSION['msg']) != '') 
{
$smarty->assign('msg', $_SESSION['msg']);
unset($_SESSION['msg']);
}

## Set active class variable for left menu
$smarty->assign('activeclass', 'city');
$smarty->assign('mainmenu', '2');
## Unset all the objects created which are on this page
unset($countryObj);
unset($stateObj);
unset($cityObj);
$smarty->display(TEMPLATEDIR_ADMIN. 'controller/city/addCity.tpl');
unset($smarty);
?>
