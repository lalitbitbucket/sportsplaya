<?php
## include required files
/*******************************/
require_once '../model/city.php';
require_once '../model/country.php';
require_once '../model/states.php';
require_once '../model/moduleuser.php';

## Paginaton class
require_once ("../model/common/classes/pagination_class.php");

## Create Objects
/*******************************/
$cityObj = new Model_City();
$country= new Model_Country();
$stateObj=new Model_States();
$moduleObj=new Model_ModuleUser();
/*******************************/

## Get search parameters in variables - 
if($_POST['search'] != '' && $_POST['search'] != 'City') {
		$searchindex = return_post_value($_POST['search']);
} else if($_GET['search'] != '') {
	$searchindex = return_post_value($_GET['search']);
}else {
	$searchindex = '';
}
$searchindex=trim($searchindex);
// Assign search variable to show in search textbox
$smarty->assign('search', $searchindex);

if(isset($_POST['search_country']) != '') {
	$search_country = $_POST['search_country'];
} else if($_GET['search_country'] != '') {
	$search_country = $_GET['search_country'];
} else {
	$search_country = '';
}

$smarty->assign('search_country', $search_country);

if(isset($_POST['search_state']) != '') {
	$search_state = $_POST['search_state'];
} else if($_GET['search_state'] != '') {
	$search_state = $_GET['search_state'];
} else {
	$search_state = '';
}

## Active, Inactive selected records
/*******************************/
if(isset($_POST['action']) && !empty($_POST['action'])) {
		$ids = implode(",", $_POST['checkall']);
		## check which action is selected
		$hidden_page = $_POST['hidden_pageno'];	
		if($_POST['action'] == 'active') {
			## Active selected records
			//echo '<pre>';print_r($ids);exit;
			$cityObj->updateMultipleCityStatus($ids, '2');
			$_SESSION['msg']="<div class='success_msg'><span>City status activated successfully.</span></div>";
		} else if($_POST['action'] == 'inactive') {
			## Inactive selected records
			$cityObj->updateMultipleCityStatus($ids, '1');
			$_SESSION['msg']="<div class='success_msg'><span>City status deactivated successfully.</span></div>";
		} else {
			## Delete selected records
			$cityObj->deleteMultipleCity($ids);
			$_SESSION['msg']="<div class='success_msg'><span>City deleted successfully</span></div>";
		}
	header('location:'.SITE_URL.'/admin/home.php?q=city&page='.$_GET['page']);
	exit;
		## Delete code will be here if required
		
} // if close

## Active/Inactive/Delete thourgh URL
if(isset($_GET['action']) == 'status' && isset($_GET['status']) != '') {
	$id = base64_decode($_GET['id']);	
	## Update country page status 
	$array = array();
	$array['cityStatus'] = ($_GET['status']=='1'?2:1);
	$cityObj->updateCityStatus($id, $array['cityStatus']);
	if($array['cityStatus']==2) {
		$_SESSION['msg']="<div class='success_msg'><span>City status activated successfully.</span></div>";
	} else {
		$_SESSION['msg']="<div class='success_msg'><span>City status deactivated successfully.</span></div>";
	}
	header('location:'.SITE_URL.'/admin/home.php?q=city&page='.$_GET['page']);
	exit;
}

if(isset($_GET['action']) == 'delete' && isset($_GET['id']) != '') {
	$id = base64_decode($_GET['id']);
	## Delete Country category
	$cityObj->deleteCityValueById($id);
	$_SESSION['msg']="<div class='success_msg'><span>City deleted successfully.</span></div>";
	header('location:'.SITE_URL.'/admin/home.php?q=city&page='.$_GET['page']);
	exit;
}

/*******************************/
if($_GET['q']=='city')
{
	$moduelArray=$moduleObj->getModuleHelpText(8);
	$smarty->assign('moduelArray', $moduelArray);
}
##Fetch all country pages from database

if($search_country!=''){
	$stateArray = $stateObj->getAllActiveStateByCountryId($search_country);
	$smarty->assign('stateArray',$stateArray);
}

$smarty->assign('search_state', $search_state);
##Fetch all Country pages from database

##  --------- Pagination part first start --------------##
	if( isset( $_GET[ 'page' ] ) && $_GET[ 'page' ] != "" ) {
		$pageNum = $_GET[ 'page' ];
	} else {
		$pageNum = 1;
	}
	if($_GET['setlimit']!=''){
		$rowsPerPage = $_GET['setlimit'];
	}
	else{
		$rowsPerPage = 25;	
	}
	
	$pageName = "home.php?q=city".'&order_by='.$orderBy.'&search='.$searchindex.'&order_field='.$orderField.'&search_country='.$search_country.'&search_state='.$search_state.'&setlimit='.$_GET['setlimit']; 
	## Count all the records
	$cityArray = $cityObj->getAllCity($searchindex,$search_country,$search_state);
	$total_rows = count($cityArray);
	$offset	= ($pageNum - 1) * $rowsPerPage;
	$smarty->assign('page', $pageNum);
	##  --------- Pagination part first end --------------##

	##  --------- Pagination part second start --------------##
	$newid = $pageNum * $rowsPerPage - $rowsPerPage + 1;
	$other_id="search=".$_POST['search'];
	$smarty->assign('newid', $newid);
	$cityArray = $cityObj->getAllCity($searchindex,$search_country,$search_state,$rowsPerPage,$offset);
	$smarty->assign('cityArray', $cityArray);

	//echo"<pre>"; print_r($cityArray); exit;

	if($searchindex != '') {
		$other_id = "search=".$searchindex;
	} else {
		$other_id = '';
	}
	
	if($search_country) {
		if($other_id == '') {
			$other_id = "search_country=".$search_country;
		} else {
			$other_id.= "&search_country=".$search_country;
		}
	}
	if($search_state) {
		if($other_id == '') {
			$other_id = "search_state=".$search_state;
		} else {
			$other_id.= "&search_state=".$search_state;
		}
	}
	
	if($total_rows > 25) {
		$pg = new pagination();					// CREATING OBJECT OF THE PAGINATION CLASS
		$pgnation = $pg->pagination_admin( $total_rows , $rowsPerPage , $pageNum , $pageName , $other_id, $class);	
		$smarty-> assign("pagination",$pgnation);
	}
##  --------- Pagination part second end --------------##	

##fecting all country
$countryArr = $country->getAllCountryWithActiveStatus($val);
$smarty->assign('countryArr', $countryArr);

$stateArr = $stateObj->getAllStateWithActiveStatus();
$smarty->assign('stateArr', $stateArr);

## Assign session message to smarty variable and unset session variable
if($_SESSION['msg'] != '') {
	$smarty->assign('msg', $_SESSION['msg']);	
	unset($_SESSION['msg']);
}

## Fetch list of total number of city
$totalCity = $cityObj->getAllActiveCity();
$smarty->assign('totalCity', $totalCity);

## Set active class variable for left menu  (main menu and sub menu)
$smarty->assign('activeclass', 'city');
$smarty->assign('mainmenu', '2');

## Unset all the objects created which are on this page
unset($cityObj);
unset($country);
unset($moduleObj);
unset($stateObj);

$smarty->display(TEMPLATEDIR_ADMIN. '/controller/city/cityList.tpl');
unset($smarty);
?>
