<?php 
## include required files
/*******************************/

require_once '../model/users.php';
require_once '../model/email.php';
require_once '../model/common/classes/pagination_class.php';   
/*******************************/
//error_reporting(E_ALL);
## Create Objects
/*******************************/
$userObj = new Model_Users();
$emailObj = new Model_Email();


## Get search parameters in variables - 
if(isset($_POST['search']) != '' && $_POST['search'] != 'Email/Name') {
    $searchindex = $_POST['search'];
} else if(isset($_GET['search']) &&  $_GET['search']!= '') {
    $searchindex = $_GET['search'];
} else {
    $searchindex = '';
}
// Assign search variable to show in search textbox
$searchindex=addslashes(trim($searchindex));
$smarty->assign('search', $searchindex);


if(isset($_GET['order_by']) && $_GET['order_by'] != '') {
    $orderBy = $_GET['order_by'];
}
else {
    $orderBy = 'DESC';
}
$smarty->assign('orderBy', $orderBy);


if(isset($_GET['orderField']) && $_GET['orderField'] != '') {
    $orderField = $_GET['orderField'];
}
else {
    $orderField = 'date';
}

if(isset($_GET['status']) && $_GET['status'] != '') {
    $status = $_GET['status'];
}
else {
    $status = '';
}

if(isset($_GET['date']) && $_GET['date'] != '') {
    $datesort  = $_GET['date'];
    
    $currentdate = date("Y-m-d");
    if($datesort==1)
    {
         $startdate = date(date("Y-m-d",strtotime(date("Y-m-d", strtotime($currentdate)) . " -1 day"))); 
         $enddate =  date(date("Y-m-d",strtotime(date("Y-m-d", strtotime($currentdate)) . " +1 day")));      
    }
    else if($datesort==2)
    {
         $startdate = date(date("Y-m-d",strtotime(date("Y-m-d", strtotime($currentdate)) . " -7 day"))); 
         $enddate =  date(date("Y-m-d",strtotime(date("Y-m-d", strtotime($currentdate)) . " +1 day")));      
    }
     else if($datesort==3)
    {
         $startdate = date(date("Y-m-d",strtotime(date("Y-m-d", strtotime($currentdate)) . " -30 day"))); 
         $enddate =  date(date("Y-m-d",strtotime(date("Y-m-d", strtotime($currentdate)) . " +1 day")));      
    }
}
else {
    $datesort = '';
}

$smarty->assign('orderField', $orderField);

## --------- Pagination part first start --------------##
if( isset( $_GET[ 'page' ] ) && $_GET[ 'page' ] != "" ) {
    $pageNum = $_GET[ 'page' ];
} else {
    $pageNum = 1;
} 
$rowsPerPage = ($_GET['setlimit']) ? $_GET['setlimit'] : ROW_PER_PAGE;
$pageName = "home.php?q=emailbackup&id1=".$_GET['id1']."&order_by=".$orderBy."&search=".$searchindex.'&setlimit='.$rowsPerPage;
## Count all the records
$emailbackupArray=$emailObj->getAllEmailBackups($orderField, $orderBy,$status,$startdate,$enddate);  
$total_rows = count($emailbackupArray);
 $offset    = ($pageNum - 1) * $rowsPerPage;
$smarty->assign('page', $pageNum);
##--------- Pagination part first end --------------##


##--------- Pagination part second start --------------##
$newid = $pageNum * $rowsPerPage - $rowsPerPage + 1;
$smarty->assign('newid', $newid);
$emailbackupArray = $emailObj->getAllEmailBackups($orderField, $orderBy,$status,$startdate,$enddate, $rowsPerPage, $offset);

//echo "<xmp>"; print_r($emailbackupArray); echo "</xmp>";

if($searchindex != '') {
    $other_id = "search=".$searchindex."&order_by=".$orderBy;
} else {
    $other_id = '';
}

if($total_rows > $rowsPerPage) {
    $pg = new pagination();
    $pgnation = $pg->pagination_admin( $total_rows , $rowsPerPage , $pageNum , $pageName , $other_id, $class);    
    $smarty-> assign("pagination",$pgnation);
}
##  --------- Pagination part second end --------------##    

//echo '<pre>';print_r();exit;
$smarty->assign('emailbackupArray', $emailbackupArray);


//print_r($settingsk);
unset($userObj);
$smarty->display(TEMPLATEDIR_ADMIN. '/controller/emailbackup/emailbackup.tpl');
unset($smarty);
?>
