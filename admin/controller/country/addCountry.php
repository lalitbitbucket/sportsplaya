<?php 
## include required files
/*******************************/
require_once '../model/country.php';
require_once '../model/common/image_functions.php';
/*******************************/
## Create Objects
/*******************************/
$countryObj = new Model_Country();
/*******************************/

## Get search parameters in variables - 
if($_POST['search'] != '' && $_POST['search'] != 'Search') {
	$searchindex = ucfirst($_POST['search']);
} else if($_GET['search'] != '') {
	$searchindex = $_GET['search'];
} else {
	$searchindex = '';
}
$searchindex=trim($searchindex);
// Assign search variable to show in search textbox
$smarty->assign('search', $searchindex);
if(isset($_GET['order_field']) && $_GET['order_field'] != '') {
	$orderField = $_GET['order_field'];
}
else {
	$orderField = 'countryName';
}
$smarty->assign('orderField', $orderField);

if(isset($_GET['order_by']) && $_GET['order_by'] != '') {
	$orderBy = $_GET['order_by'];
}
else {
	$orderBy = 'ASC';
}
$smarty->assign('orderBy', $orderBy);

if(isset($_POST['addcountry_btn'])) {
## apply PHP validation for required filed
	if(trim($_POST['countryname']) != '') {

		$countryAddArray['countryId'] = $_POST['countryname'];
		$countryAddArray['countryName'] = $_POST['countryname'];
		if($_FILES['flag']['name']){
		## check valid image extension
		$fileExt = file_extension($_FILES['flag']['name']); 
		$validExt = valid_extension($fileExt);
		## upload file
		$fileName      = time()."_".$_FILES['flag']['name'];
		$temp=$_FILES["flag"]["tmp_name"];	
		$imageFolder='../dynamicAssets/flags/';
		copy($_FILES["flag"]["tmp_name"],$imageFolder.$fileName);
		$countryAddArray['flag'] = $fileName;
		}
		$countryAddArray['countryStatus']      = '2';
		$countryObj->addCountry($countryAddArray);
		$_SESSION['msg'] = "<div class='success_msg'><span>Country name added successfully</span></div>";
		header('location:'.SITE_URL.'/admin/home.php?q=country&page='.$_POST['page']);
		exit;
	}
	else
	{
		$_SESSION['msg'] = '<div class="error_msg"><span>Please enter country name </span></div>';
	}
}


## Assign success or error msg to smarty variable and unset session variable
if(trim($_SESSION['msg']) != '') {
	$smarty->assign('msg', $_SESSION['msg']);
	unset($_SESSION['msg']);
}

## Set active class variable for left menu
$smarty->assign('activeclass', 'country');
$smarty->assign('mainmenu', '2');
## Unset all the objects created which are on this page
unset($countryObj);

$smarty->display(TEMPLATEDIR_ADMIN. 'controller/country/addCountry.tpl');
unset($smarty);
?>
