<?php  
## include required files
/*******************************/
require_once '../model/country.php';
require_once '../model/moduleuser.php';
## Paginaton class
require_once ("../model/common/classes/pagination_class.php");
/*******************************/

## Create Objects
/*******************************/
$countryObj = new Model_Country();
$moduleObj=new Model_ModuleUser();
/*******************************/

## Get search parameters in variables - 
if($_POST['search'] != '' && $_POST['search'] != 'Search') {
	$searchindex = $_POST['search'];
} else if($_GET['search'] != '') {
	$searchindex = $_GET['search'];
} else {
	$searchindex = '';
}

 if($_POST['searchstatus'] != '') {
    $searchstatus = $_POST['searchstatus'];
} else {
    $searchstatus = '';
}
  $smarty->assign('searchstatus', $searchstatus); 

$searchindex=trim($searchindex);
// Assign search variable to show in search textbox
$smarty->assign('search', $searchindex);
if(isset($_GET['order_field']) && $_GET['order_field'] != '') {
	$orderField = $_GET['order_field'];
}
else {
	$orderField = 'countryName';
}
$smarty->assign('orderField', $orderField);

if(isset($_GET['order_by']) && $_GET['order_by'] != '') {
	$orderBy = $_GET['order_by'];
}
else {
	$orderBy = 'ASC';
}
$smarty->assign('orderBy', $orderBy);


//error_reporting(E_ALL);
if(isset($_POST['go']) != '' && isset($_POST['action']) != '') {
		$ids = implode(",", $_POST['checkall']);
		## check which action is selected
		$hidden_page = $_POST['hidden_pageno'];	
		if($_POST['action'] == 'active') {
			## Active selected records
			//echo "<pre>";print_r($ids);exit;
			$countryObj->updateMultipleCountryStatus($ids,'2');
			$_SESSION['msg']="<div class='success_msg'><span>Country status activated successfully.</span></div>";
		} else if($_POST['action'] == 'inactive') {
			## Inactive selected records
				//echo "<pre>";print_r($ids);exit;
			$countryObj->updateMultipleCountryStatus($ids, '1');
			$_SESSION['msg']="<div class='success_msg'><span>Country status deactivated successfully.</span></div>";
		} else {
			## Delete selected records
			$countryObj->updateMultipleCountryStatus($ids, '0'); 
			$_SESSION['msg']="<div class='success_msg'><span>Country deleted successfully</span></div>";
		}
		header('location:'.SITE_URL.'/admin/home.php?q=country&page='.$hidden_page.'&order_by='.$orderBy.'&search='.$searchindex.'&order_field='.$orderField);
		exit;
		## Delete code will be here if required
		
} // if close

## Active/Inactive/Delete thourgh URL
if(isset($_GET['action']) == 'status' && isset($_GET['status']) != '') {
	
	$id = base64_decode($_GET['id']);		
	## Update country page status 
	$array = array();
	$array['countryStatus'] = ($_GET['status']=='1'?2:1);
	$countryObj->updateCountryStatus($id, $array['countryStatus']);
	if($array['countryStatus']==2) {
		
		$_SESSION['msg']="<div class='success_msg'><span>Country status activated successfully.</span></div>";
	} else {
		
		$_SESSION['msg']="<div class='success_msg'><span>Country status deactivated successfully.</span></div>";
	}
	header('location:'.SITE_URL.'/admin/home.php?q=country&page='.$_GET['page'].'&order_by='.$orderBy.'&search='.$searchindex.'&order_field='.$orderField);
	exit;
}
//echo "<pre>";print_r($_GET);exit;
if(isset($_GET['action']) == 'delete' && isset($_GET['id']) != '') {
	$id = base64_decode($_GET['id']);
	//echo "<pre>";print_r($id);exit;
	## Delete Country category
	 $countryObj->updateCountryStatus($id, '0'); 
	$_SESSION['msg']="<div class='success_msg'><span>Country  deleted successfully</span></div>";
	header('location:'.SITE_URL.'/admin/home.php?q=country&page='.$_GET['page']);
	exit;
}


if(isset($_GET['actions']) == 'block' && isset($_GET['id']) != '') {
    $id = base64_decode($_GET['id']);
    $countryObj->updateCountryStatus($id, '3');
    $_SESSION['msg']="<div class='success_msg'><span>Country  blocked successfully</span></div>";
    header('location:'.SITE_URL.'/admin/home.php?q=country&page='.$_GET['page']);
    exit;
}

 if(isset($_GET['uaction']) == 'unblock' && isset($_GET['id']) != '') {
    $id = base64_decode($_GET['id']);
    $countryObj->updateCountryStatus($id, '2');
    $_SESSION['msg']="<div class='success_msg'><span>Country  unblocked successfully</span></div>";
    header('location:'.SITE_URL.'/admin/home.php?q=country&page='.$_GET['page']);
    exit;
}
/*******************************/
if($_GET['q']=='country')
{
	$moduelArray=$moduleObj->getModuleHelpText(6);
	$smarty->assign('moduelArray', $moduelArray);
}

##Fetch all Country pages from database
##  --------- Pagination part first start --------------##
	if( isset( $_GET[ 'page' ] ) && $_GET[ 'page' ] != "" ) {
		$pageNum = $_GET[ 'page' ];
	} else {
		$pageNum = 1;
	} 
if($_GET['setlimit']!=''){
		$rowsPerPage = $_GET['setlimit'];
		//echo "<pre>";print_r($rowsPerPage);exit;
	}
	else{
		$rowsPerPage = 25;
	}
	$pageName = "home.php?q=country&order_by=".$orderBy."&search=".$searchindex."&order_field=".$orderField."&page=".$_GET['page'].'&setlimit='.$_GET['setlimit'].'&searchstatus='.$searchstatus; 
	## Count all the records
	$countryArray = $countryObj->getAllCountry($searchindex,$orderField, $searchstatus,$orderBy);
	$total_rows = count($countryArray);
	$offset	= ($pageNum - 1) * $rowsPerPage;
	$smarty->assign('page', $pageNum);
##  --------- Pagination part first end --------------##

##  --------- Pagination part second start --------------##
	$newid = $pageNum * $rowsPerPage - $rowsPerPage + 1;
	$smarty->assign('newid', $newid);
	$countryArray = $countryObj->getAllCountry($searchindex,$orderField, $searchstatus,$orderBy,$rowsPerPage,$offset);
	//echo "<pre>"; print_r($countryArray);exit;
	$smarty->assign('countryArray', $countryArray);
	
	if($searchindex != '') {
		$other_id = "search=".$searchindex;
	} else {
		$other_id = '';
	}
	
	if($total_rows > $rowsPerPage) {
		$pg = new pagination();					// CREATING OBJECT OF THE PAGINATION CLASS
		$pgnation = $pg->pagination_admin( $total_rows , $rowsPerPage , $pageNum , $pageName , $other_id, $class);	
		$smarty-> assign("pagination",$pgnation);
	}
##  --------- Pagination part second end --------------##	

$totalCountry = $countryObj->getAllActiveCountry();
$smarty->assign('totalCountry', $totalCountry);

## Assign session message to smarty variable and unset session variable
if($_SESSION['msg'] != '') {
	$smarty->assign('msg', $_SESSION['msg']);	
	unset($_SESSION['msg']);
}

## Set active class variable for left menu  (main menu and sub menu)
$smarty->assign('activeclass', 'country');
$smarty->assign('mainmenu', '2');

## Unset all the objects created which are on this page
unset($countryObj);
unset($moduleObj);

$smarty->display(TEMPLATEDIR_ADMIN. 'controller/country/countryList.tpl');
unset($smarty);
?>
