<?php
## include required files
/*******************************/
require_once '../model/country.php';
require_once '../model/common/image_functions.php';
/*******************************/
## Create Objects
/*******************************/
$countryObj = new Model_Country();
/*******************************/
//error_reporting(E_ALL);
##get country id from edit page as in the hidden form
$editcountryid = base64_decode($_GET['id']);

## Get search parameters in variables - 
if($_POST['search'] != '' && $_POST['search'] != 'Search') {
	$searchindex = ucfirst($_POST['search']);
} else if($_GET['search'] != '') {
	$searchindex = $_GET['search'];
} else {
	$searchindex = '';
}
$searchindex=trim($searchindex);
// Assign search variable to show in search textbox
$smarty->assign('search', $searchindex);
if(isset($_GET['order_field']) && $_GET['order_field'] != '') {
	$orderField = $_GET['order_field'];
}
else {
	$orderField = 'countryName';
}
$smarty->assign('orderField', $orderField);

if(isset($_GET['order_by']) && $_GET['order_by'] != '') {
	$orderBy = $_GET['order_by'];
}
else {
	$orderBy = 'ASC';
}
$smarty->assign('orderBy', $orderBy);


if(isset($_POST) && !empty($_POST)) {

	
	if(trim($_POST['countryname']) != '' )
	 {
		
	
	
		$eid=($_POST['country_id']);
		
		$cid=base64_decode($eid);
		

		
		$countryeditArray['countryName'] = return_post_value($_POST['countryname']);
		
		if($_FILES['flag']['name']){
			## check valid image extension
			$fileExt = file_extension($_FILES['flag']['name']); 
			$validExt = valid_extension($fileExt);
			## upload file
			$fileName      = time()."_".$_FILES['flag']['name'];
			$temp=$_FILES["flag"]["tmp_name"];	
			$imageFolder='../dynamicAssets/flags/';
			copy($_FILES["flag"]["tmp_name"],$imageFolder.$fileName);
			$countryeditArray['flag'] = $fileName;
		}
		
				//echo "<pre>";print_r($countryeditArray);exit;
		
		$countryObj->editCountryValueById($countryeditArray,$cid);
		
		$_SESSION['msg'] = "<div class='success_msg'><span>Country edited successfully</span></div>";
		header('location:'.SITE_URL.'/admin/home.php?q=country&page='.$_POST['page'].'&order_by='.$orderBy.'&search='.$searchindex.'&order_field='.$orderField);
		exit;
	} else {// php validation else
		$_SESSION['msg'] = '<div class="error_msg"><span>Please enter country </span></div>';
	}
	// main if closed
}

if($_GET['id'] != ''&& $_GET['action'] == 'edit') {
	$country_id = base64_decode($_GET['id']);
	$countryDetArray = $countryObj->getCountryDetailsById($country_id);
	$smarty->assign('countryDetArray', $countryDetArray);
}

## Assign success or error msg to smarty variable and unset session variable
if(trim($_SESSION['msg']) != '') {
	$smarty->assign('msg', $_SESSION['msg']);
	unset($_SESSION['msg']);
}

## Set active class variable for left menu
$smarty->assign('activeclass', 'country');
$smarty->assign('mainmenu', '2');
## Unset all the objects created which are on this page
unset($countryObj);

$smarty->display(TEMPLATEDIR_ADMIN. 'controller/country/editCountry.tpl');
unset($smarty);
?>