<?php 
## include required files
/*******************************/
require_once '../model/users.php';
require_once '../model/email.php';
require_once '../model/common/functions.php';
require_once "../model/common/classes/pagination_class.php";
require_once '../includes/phpmailer/class.phpmailer.php';
/*******************************/
## Create Objects
/*******************************/
$emailObj = new Model_Email();
$userObj = new Model_Users();
$mail 	= new PHPMailer(true);
/*******************************/
if(sizeof($_POST)>0) 
{
	 ## apply PHP validation for required filed
	if(trim($_POST['first_name']) != '' && trim($_POST['last_name']) != '' && trim($_POST['email']) != '' && trim($_POST['birthdate']) != '' && trim($_POST['sex']) != '') 
	{    
	    $newPass = return_fetched_value($_POST['password']);  
		$userArray = array();
		extract($_POST);
		$salt = genenrate_salt();
        $newpassword = genenrate_password($salt ,$newPass);
		$userArray['email']   	 = return_post_value($email);
		$userArray['password']   	= $newpassword;	
        $userArray['salt']     		= $salt;
		$userArray['userType']   = '5';
		$userArray['status']     = '2';
		$userArray['regDate']    = date("Y-m-d");
		$userArray['regIP']      = $_SERVER['REMOTE_ADDR'];			
		$userid = $userObj->addUserByValue($userArray);

		/************** Insert data in user profile table ********/
		$userprofileArray = array();
		extract($_POST);
		$userprofileArray['fname']   	= return_post_value($first_name);
		$userprofileArray['lname']   	= return_post_value($last_name);
		$userprofileArray['email']   	= return_post_value($email);
		$userprofileArray['birthdate']  = return_post_value($birthdate);
		$userprofileArray['sex']   	= return_post_value($sex);
	    $userprofileArray['userId']     = $userid;
		$userprofileArray['userType']   = '6';
		$userprofileArray['status']     = '2';
		$userprofileArray['regDate']    = date("Y-m-d");
		$userprofileArray['regIP']      = $_SERVER['REMOTE_ADDR'];			
		$userObj->addUserProfileByValue($userprofileArray);

		/*************** Insert User Name with special characters start **************/
		$userNameDetail = array();
		$username  = strtolower($first_name).''.strtolower($last_name); 
		$username1 = strtolower($first_name).'.'.strtolower($last_name);  
		$username2 = strtolower($first_name).'-'.strtolower($last_name);
		
		//Remove empty space if exist
		$uname  = str_replace(" ","",$username);
		$uname1 = str_replace(" ","",$username1);
		$uname2 = str_replace(" ","",$username2);
  		
  		//Check already exist username in the database
		$usernameexist  = $usersObj->checkUserNameExists($uname);
		$usernameexist1 = $usersObj->checkUserNameExists($uname1);
		$usernameexist2 = $usersObj->checkUserNameExists($uname2);

		if(count($usernameexist) < 1){ 
		$userNameDetail['username']= $uname;
		}elseif(count($usernameexist1) < 1){  
		$userNameDetail['username']= $uname1;
		}elseif(count($usernameexist2) < 1){ 
		$userNameDetail['username']= $uname2;
		}else{
		$username = strtolower($fname).''.strtolower($lname).''.$userprofileId;
		$uname = str_replace(" ","",$username);  
		$userNameDetail['username']=$uname;
		}
		$usersObj->editUserProfileValueById($userNameDetail, $userprofileId);

		/*************** Insert User Name with special characters End **************/
		$userDetails = $userObj->getDetailsByUserId($userid);
		## Get Admin details 
		$id=1;
		$admindetails = $userObj->getUserDetailsByUserId($id);
		$smarty->assign('admindetails',$admindetails);

		## Fetch email content
		$emailArray = $emailObj->getEmailById(2);
		$to= $userArray['email'];
		$toname= $userArray['email'];
		$subject=$emailArray['emailSub'];
		$subject = str_replace('[SITENAME]', SITENAME, $subject);
		$message=$emailArray['emailContent'];
		$message=str_replace('[SITENAME]', SITENAME, $message);
		$message=str_replace('[EMAIL]', return_post_value(trim($email)), $message);
		$message=str_replace('[NAME]', ucfirst(return_post_value(trim($first_name))), $message);
		$message=str_replace('[USERTYPE]', 'coach', $message);
		$message=str_replace('[USERNAME]', return_post_value(trim($email)), $message);
		$message = str_replace('[Email_Address]',$userArray['email'] , $message);
		$message = str_replace('[PASSWORD]',$newPass , $message);
		$message=str_replace('[SITENAME]', SITENAME, $message);		
		$message=str_replace('[SITELINK]',SITE_URL , $message);
                $message=str_replace('[MESSAGE]',$message , $message);
		$from = $emailArray['fEmail'];
		$fromname = $emailArray['fName'];

		$smtpArray = $emailObj->getActiveSMTPDetails();
		$auth  = $smtpArray['isAuth'];
		if($auth=='y')
		{     $auth = 'true';  

		}
		else
		{     $auth = 'false';    

		}
		if($auth=='true')
		{  
		$mail->IsSMTP(); // telling the class to use SMTP
		$mail->Mailer = "smtp";
		$mail->SMTPAuth   = true;                  // enable SMTP authentication
		$mail->SMTPSecure = $smtpArray['secure'];                 // sets the prefix to the servier
		$mail->Host       = $smtpArray['smtpHost'];      // sets GMAIL as the SMTP server
		$mail->Port       = $smtpArray['smtpPort'];                  // set the SMTP port for the GMAIL server
		$mail->Username   = $smtpArray['smtpUsername'];  // GMAIL username
		$mail->Password   = $smtpArray['smtpPassword']; 
		}

		# get email back up    
		$backemailArray = array();
		$backemailArray['sendto'] = $to;
		$backemailArray['subject'] = $subject;
		$backemailArray['content'] = $message;
		$backemailArray['date'] = date("Y-m-d H:i:s");

		/*echo $subject."<br />";
		echo $from."<br />";
		echo $fromname."<br />";
		echo $to."<br />";
		echo $toname."<br />";
		echo "<pre>"; print_r($message);  exit();*/

		try {
		$mail->AddAddress($to, $toname);
		$mail->SetFrom($from, $fromname);
		$mail->Subject = $subject;
		$mail->Body = $message; 
		$mail->Send(); 
		$backemailArray['status'] ='2';   
		} 
		catch (phpmailerException $e) {
		$_SESSION['msg']= "<div class='warning'>".$e->errorMessage()."</div>"; //Pretty error messages from PHPMailer
		$backemailArray['status'] ='1';
		} catch (Exception $e) {
		$_SESSION['msg']=  "<div class='warning'>".$e->getMessage()."</div>"; //Boring error messages from anything else!
		$backemailArray['status'] ='1';
		}				
		$emailObj->getEmailBackup($backemailArray);   
	       	$_SESSION['msg'] = "<div class='success_msg'><span>Coach user added successfully</span></div>";
		header('location:'.SITE_URL.'/admin/home.php?q=coach&page='.$_GET['page'].'&order_field='.$orderField.'&order_by='.$orderBy.'&search='.$searchindex);
		exit();
		} 
	else { // php validation else
		$_SESSION['msg'] = '<div class="error_msg"><span>Please fill all required fields</span></div>';
	}
	// main if closed
}

if($_GET['q']=='coach')
{
$moduelArray=$moduleObj->getModuleHelpText(10);
$smarty->assign('moduelArray', $moduelArray);
}
/***************************/
##Assign Success Error messages
/***************************/
if($_SESSION['msg']!='')
{
$smarty->assign('msg',$_SESSION['msg']);
unset($_SESSION['msg']);
}

$smarty->assign('activeclass','coach');
$smarty->assign('mainmenu', '3');
	
$smarty->display(TEMPLATEDIR_ADMIN. 'controller/coach/addcoach.tpl');
unset($smarty);
?>
