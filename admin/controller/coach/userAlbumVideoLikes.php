<?php  
## include required files
/*******************************/
require_once '../model/users.php';
require_once '../model/videos.php';
//require_once '../model/moduleuser.php';
## Paginaton class
require_once ("../model/common/classes/pagination_class.php");
/*******************************/

## Create Objects
/*******************************/
$usersObj      = new Model_Users();
$videoalbumObj  = new Model_videos();
/*******************************/

$userId = $_REQUEST['userId'];
$smarty->assign('userId', $userId);

$uID = base64_decode($_REQUEST['userId']);
$smarty->assign('uID', $uID);

$videoId = $_REQUEST['id'];
$smarty->assign('videoId', $videoId);

$AlbumvideoId = base64_decode($_REQUEST['id']);
$smarty->assign('AlbumvideoId', $AlbumvideoId);

$videoBigId = $_REQUEST['vID'];
$smarty->assign('videoBigId', $videoBigId);

## Get search parameters in variables - 
if($_REQUEST['search'] != '' && $_REQUEST['search'] != 'Search') {
	$searchindex = $_REQUEST['search'];
} else if($_REQUEST['search'] != '') {
	$searchindex = $_REQUEST['search'];
} else {
	$searchindex = '';
}
$searchindex=trim($searchindex);
// Assign search variable to show in search textbox
$smarty->assign('search', $searchindex);
if(isset($_GET['order_field']) && $_GET['order_field'] != '') {
	$orderField = $_GET['order_field'];
}
else {
	$orderField = 'id';
}
$smarty->assign('orderField', $orderField);

if(isset($_GET['order_by']) && $_GET['order_by'] != '') {	
	$orderBy = $_GET['order_by'];
}
else {
	$orderBy = 'DESC';
}
$smarty->assign('orderBy', $orderBy);



if(isset($_REQUEST['go']) && $_REQUEST['go']!='' && $_REQUEST['action']!=''  )
{
	$ids = $_REQUEST['checkall'];
	$ids = implode(',',$ids);
	$videoalbumObj->deleteMultipleVideoLike($ids);
	$_SESSION['msg'] = "<div class='success_msg'><span> Video like(s) deleted successfully </span></div>";
}



if(isset($_GET['action']) == 'delete' && isset($_GET['lid']) != '') 
{
	$id = base64_decode($_GET['lid']);
	$videoalbumObj->deleteVideoLike($id);
	$_SESSION['msg']="<div class='success_msg'><span>Video Likes deleted successfully</span></div>";
	header('location:'.SITE_URL.'/admin/home.php?q=coachuserAlbumVideoLikes&id='.$_REQUEST['id'].'&vID='.$_REQUEST['vID'].'&userId='.$_REQUEST['userId']);
	exit();
}



##Fetch all Training pages from database

##  --------- Pagination part first start --------------##
	if( isset( $_REQUEST['page'] ) && $_REQUEST['page'] != "" ) {
		$pageNum = $_REQUEST['page'];
	} else {
		$pageNum = 1;
	} 
	//echo $pageNum;exit;
	$rowsPerPage = ($_GET['setlimit']) ? $_GET['setlimit'] : ROW_PER_PAGE;
	
	$pageName = "home.php?q=coachuserAlbumVideoLikes&id=".$_REQUEST['id']."&vID=".$_REQUEST['vID']."&userId=".$_REQUEST['userId']."&order_by=".$orderBy."&search=".$searchindex."&order_field=".$orderField."&page=".$_GET['page']."&setlimit=".$_GET['setlimit']; 
	## Count all the records
	$videolikeArray = $videoalbumObj->getAllVideosLikeByuserIdandvideoId($AlbumvideoId,$uID);
	$total_rows = count($videolikeArray);
	$offset	= ($pageNum - 1) * $rowsPerPage;
	$smarty->assign('page', $pageNum);
##  --------- Pagination part first end --------------##

##  --------- Pagination part second start --------------##
	$newid = $pageNum * $rowsPerPage - $rowsPerPage + 1;
	$smarty->assign('newid', $newid);
	$videolikeArray = $videoalbumObj->getAllVideosLikeByuserIdandvideoId($AlbumvideoId,$uID,$rowsPerPage,$offset);
	$i=0;
	foreach($videolikeArray as $videlikeoinfo)
	{
	
		$userprofileinfo = $usersObj->getUserDetailsInfoByUserId($videlikeoinfo['videolikeProfileId']);
	$videolikeArray[$i]['name'] = $userprofileinfo['fname'].' '.$userprofileinfo['lname'];
	
	$i++;
	}
	$smarty->assign('videolikeArray', $videolikeArray);

  // echo '<pre>'; print_r($videolikeArray);exit;

	if($searchindex != '') {
		$other_id = "search=".$searchindex;
	} else {
		$other_id = '';
	}
	
	if($total_rows > ROW_PER_PAGE) {
		$pg = new pagination();					// CREATING OBJECT OF THE PAGINATION CLASS
		$pgnation = $pg->pagination_admin( $total_rows , $rowsPerPage , $pageNum , $pageName , $other_id, $class);		
		$smarty-> assign("pagination",$pgnation);
	}
##  --------- Pagination part second end --------------##	

## Assign session message to smarty variable and unset session variable
if($_SESSION['msg'] != '') {
	$smarty->assign('msg', $_SESSION['msg']);	
	unset($_SESSION['msg']);
}

## Set active class variable for left menu  (main menu and sub menu)
$smarty->assign('activeclass', 'userAlbumVideoLikes');
$smarty->assign('mainmenu', '5');

## Unset all the objects created which are on this page
unset($clubObj);
unset($moduleObj);

$smarty->display(TEMPLATEDIR_ADMIN. 'controller/coach/userAlbumVideoLikes.tpl');
unset($smarty);
?>
