<?php
require_once '../model/moduleuser.php';
require_once "../model/common/classes/pagination_class.php";
require_once '../model/users.php';
require_once '../model/address.php';


## Initializing Objects
/**************************/
$moduleObj = new Model_ModuleUser();
$userObj = new Model_Users();
$addObj = new Model_Address();


/**************************/


if($_REQUEST['id'])
{
	$uID = base64_decode($_REQUEST['id']);
	
}

	if(isset($_REQUEST['search']) && !empty($_REQUEST['search']))
	{
 		 $search = $_REQUEST['search'];
		 $search = str_replace("'","",$search);
         $search = ucfirst(stripslashes($search));
		 $smarty->assign('search',$search);
	}
	if(isset($_REQUEST['order_field']) && !empty($_REQUEST['order_field']))
	{
 		 $OrderField = $_REQUEST['order_field'];
		 $smarty->assign('order_field',$OrderField);
	}
	if(isset($_REQUEST['order_by']) && !empty($_REQUEST['order_by']))
	{
 		 $OrderBy = $_REQUEST['order_by'];
		 $smarty->assign('order_by',$OrderBy);
	}


##------------ Active/Inactive and delete  by multiple ids -----------------##
    if(isset($_REQUEST['go']) && $_REQUEST['go']!='' && $_REQUEST['action']!=''  )
	{
		 $ids = $_REQUEST['checkall'];
		 $ids = implode(',',$ids);
		 $userObj->deleteUsers($ids);
		 $_SESSION['msg'] = "<div class='success_msg'><span>Shipping Addresses  deleted successfully </span></div>";
			 	  header('location:'.SITE_URL.'/admin/home.php?q=shippingadd&page='.$_GET['page']);
				  exit();
			 
}
		 
	
	
## --------------------------Delete single Seller by id ----------------------------##
	 if(isset($_GET['action']) && $_GET['action']=='delete' && $_GET['AddID']!='')
	 {
	 	  $id = base64_decode($_GET['AddID']);
	      $addObj->deleteAddress($id);
		  $_SESSION['msg'] = "<div class='success_msg'><span>Shipping Addresses deleted succesfully </span></div>";
		  header('location:'.SITE_URL.'/admin/home.php?q=shippingadd&page='.$_GET['page']);
		 exit;
	 }





	if(isset($_POST['search']) && $_POST['search']!= 'Username and Email')
	{
		$searchindex = trim($_POST['search']);
	}
	elseif($_GET['search']!='')
	{
	    $searchindex = trim($_GET['search']);	
	}
	else{
	    $searchindex = '';	
	}
	$searchindex = str_replace("'","",$searchindex);
    $searchindex = ucfirst(stripslashes($searchindex));
	$smarty->assign('search',$searchindex);
	
	##
	if(isset($_REQUEST['order_field']) && $_REQUEST['order_field'] != '') {
		$orderField = $_REQUEST['order_field'];
	}
	else {
		$orderField = 'addressId';
	}
	$smarty->assign('orderField', $orderField);
	
	if(isset($_REQUEST['order_by']) && $_REQUEST['order_by'] != '') {
		$orderBy = $_REQUEST['order_by'];
	}
	else {
		$orderBy = 'ASC';
	}
	$smarty->assign('orderBy', $orderBy);
	
		
/******************************************/
##  --------- Pagination part first start --------------##
/******************************************/
	 if(isset($_REQUEST['page']) && !empty($_REQUEST['page']))
	 {
		 $pageNum = $_REQUEST['page'];
	 }else{
		 $pageNum = 1;
	 }
	 
	 $rowsperpage = ($_GET['setlimit'])? ($_GET['setlimit']): ROW_PER_PAGE;

	 $smarty->assign('rowsperpage',$rowsperpage);
	
	 $pageName = "home.php?q=siteuser&setlimit=$rowsperpage&search=$search&order_field=$order_field&order_by=$order_by";
	 
		$shippArray = $userObj->getAllShippingAddress($uID,$search,$orderField,$orderBy);
	   $total_rows = count($shippArray);
	 $offset = ($pageNum - 1)*$rowsperpage;
	 $smarty->assign('page',$pageNum);
	 
	     $arr			=	array();
		$interval		=	2;
		for($i = $interval; $i <= $total_rows; $i = $i + $interval)
		{
			$arr[]	=	$i;
		}
		
		$smarty->assign('arr', $arr);
		$cnt	=	count($shippArray);
		$smarty->assign('cnt', $cnt);
##  --------- Pagination part first ends --------------##
		
##  --------- Pagination part second start --------------##
	$newid = $pageNum * $rowsperpage - $rowsperpage + 1;
	$smarty->assign('newid', $newid);
	$shippArray = $userObj->getAllShippingAddress($uID,$searchindex,$orderField, $orderBy,$rowsperpage,$offset);
	$smarty->assign('shippArray', $shippArray);
	
	
	
	if($searchindex != '') {
		$other_id = "search=".$searchindex;
	} else {
		$other_id = '';
	}	
	if($total_rows > $rowsperpage) {
		$pg = new pagination();					// CREATING OBJECT OF THE PAGINATION CLASS
		$pgnation = $pg->pagination_admin( $total_rows , $rowsperpage , $pageNum , $pageName , $other_id, $class);	
		$smarty-> assign("pagination",$pgnation);
		}
##  --------- Pagination part second end --------------##	
	
	 
		
/*******************************/
##Fetch all admin users from database
/******************************/
	
	if($_GET['q']=='siteuser')
	{
		$moduelArray=$moduleObj->getModuleHelpText(10);
		$smarty->assign('moduelArray', $moduelArray);
	}
/***************************/
##Assign Success Error messages
/***************************/
	if($_SESSION['msg']!='')
	{
		$smarty->assign('msg',$_SESSION['msg']);
		unset($_SESSION['msg']);
	}
	
	$smarty->assign('activeclass','siteuser');
	$smarty->assign('mainmenu', '3');
	
## Unset all objects which are on this page	
	unset($moduleObj);
	unset($userObj);
	$smarty->display(TEMPLATEDIR_ADMIN. 'controller/siteuser/shippingadd.tpl');
	unset($smarty);



?>