<?php 
## include required files
/*******************************/
require_once '../model/faqs.php';
/*******************************/

## Create Objects
/*******************************/
$faqObj = new Model_FAQ();
/*******************************/
## apply PHP validation for required filed
$faqArray1 = array();

## Get search parameters in variables - 
if($_REQUEST['search'] != '' && $_REQUEST['search'] != 'Search') {
	$searchindex = $_REQUEST['search'];
} else {
	$searchindex = '';
}
$searchindex=trim(return_post_value($searchindex));
$smarty->assign('search', $searchindex);

## get order field & order by parameters for sorting
if($_REQUEST['order_field'] != '') {
	$orderField = $_REQUEST['order_field'];
} else {
	$orderField = 'category';
}
$smarty->assign('orderField', $orderField);

if($_REQUEST['order_by'] != '') {
	$orderBy = $_REQUEST['order_by'];
} else {
	$orderBy = 'ASC';
}
$smarty->assign('orderBy', $orderBy);


if(count($_POST) > 0)
{
	if(trim($_POST['cat_name']) != '') 
	{
		$faqArray1['category']   = return_post_value($_POST['cat_name']);
		$faqArray1['status']   = '1';
		$faqObj->addFaqCategory($faqArray1);
		$_SESSION['msg'] = "<div class='success_msg'><span>FAQ category added successfully</span></div>";
		header('location:'.SITE_URL.'/admin/home.php?q=faqCategories');
		exit;
	} else { // php validation else
		$_SESSION['msg'] = '<div class="error_msg"><span>Please enter faq category</span></div>';
	}
}
// main if closed

## Assign success or error msg to smarty variable and unset session variable
if(trim($_SESSION['msg']) != '') {
	$smarty->assign('msg', $_SESSION['msg']);
	unset($_SESSION['msg']);
}

## Set active class variable for left menu
$smarty->assign('activeclass', 'faqs_cats');
$smarty->assign('mainmenu', '2');
## Unset all the objects created which are on this page
unset($faqObj);

$smarty->display(TEMPLATEDIR_ADMIN. 'controller/faqCategories/addfaqCategory.tpl');
unset($smarty);
?>