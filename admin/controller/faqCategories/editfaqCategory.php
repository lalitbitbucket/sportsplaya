<?php 
## include required files
/*******************************/
require_once '../model/faqs.php';
/*******************************/

## Create Objects
/*******************************/
$faqObj = new Model_FAQ();
/*******************************/

$faq_id = base64_decode($_GET['id']);

## Get parameters for page back searching, sorting and pagination
//get search parameters in variables - 
if($_REQUEST['search'] != '' && $_REQUEST['search'] != 'Search') {
	$search = $_REQUEST['search'];
} else {
	$search = '';
}
$smarty->assign('search', $search);

//get order field & order by parameters for sorting
if($_REQUEST['order_field'] != '') {
	$orderField = $_REQUEST['order_field'];
} else {
	$orderField = 'category';
}
$smarty->assign('orderField', $orderField);

if($_REQUEST['order_by'] != '') {
	$orderBy = $_REQUEST['order_by'];
} else {
	$orderBy = 'ASC';
}
$smarty->assign('orderBy', $orderBy);

//get page number
$smarty->assign('page', $_REQUEST['page']);

if(count($_POST) > 0)
{
	## apply PHP validation for required filed
	if(trim($_POST['cat_name']) != '') {
		$cat_name = array();
	
		$faqArray['category']   = return_post_value($cat_name);
		$faqObj->editFaqCategoryById($faqArray, $faq_id);
		$_SESSION['msg'] = "<div class='success_msg'><span>FAQ category Edited successfully</span></div>";
		header('location:'.SITE_URL.'/admin/home.php?q=faqCategories&search='.$search.'&order_field='.$orderField.'&order_by='.$orderBy.'&page='.$_REQUEST['page']);
		exit;
	} else { // php validation else
		$_SESSION['msg'] = '<div class="error_msg"><span>Please enter faq category</span></div>';
	}
}
	// main if closed

## Fetch faq category by id
if($_GET['id'] != ''&& $_GET['action'] == 'edit') {
	$faqcatArray = $faqObj->getFaqCategoryDetailsById($faq_id);
	$smarty->assign('faqcatArray', $faqcatArray);
}

## Assign success or error msg to smarty variable and unset session variable
if(trim($_SESSION['msg']) != '') {
	$smarty->assign('msg', $_SESSION['msg']);
	unset($_SESSION['msg']);
}

## Set active class variable for left menu
$smarty->assign('activeclass', 'faqs_cats');
$smarty->assign('mainmenu', '2');
## Unset all the objects created which are on this page
unset($faqObj);

$smarty->display(TEMPLATEDIR_ADMIN. 'controller/faqCategories/editfaqCategory.tpl');
unset($smarty);
?>