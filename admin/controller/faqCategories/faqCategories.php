<?php 
## include required files
/*******************************/
require_once '../model/faqs.php';
require_once '../model/moduleuser.php';
## Paginaton class
require_once ("../model/common/classes/pagination_class.php");
/*******************************/
## Create Objects
/*******************************/
$faqObj = new Model_FAQ();
$moduleObj=new Model_ModuleUser();
/*******************************/

/*## check user's permission
if($modules_new[3]['permission_status'] !=1 || $modules_new[3]['submod_16'] !=1) {
	header('location: '.SITE_URL.'/admin/home.php');
	exit;
}*/
## Get search parameters in variables - 
if($_REQUEST['search'] != '' && $_REQUEST['search'] != 'Search') {
	$searchindex = $_REQUEST['search'];
} else {
	$searchindex = '';
}
$searchindex=trim(return_fetched_value($searchindex));
$smarty->assign('search', $searchindex);

## get order field & order by parameters for sorting
if($_REQUEST['order_field'] != '') {
	$orderField = $_REQUEST['order_field'];
} else {
	$orderField = 'category';
}
$smarty->assign('orderField', $orderField);

if($_REQUEST['order_by'] != '') {
	$orderBy = $_REQUEST['order_by'];
} else {
	$orderBy = 'ASC';
}
$smarty->assign('orderBy', $orderBy);



## Active, Inactive selected records
/*******************************/
if(isset($_POST['go']) != '' && isset($_POST['action']) != '') {
		$ids = @implode(",", $_POST['checkall']);
		## check which action is selected
		$hidden_page = $_POST['hidden_pageno'];	
		if($_POST['action'] == 'active') {
			## Active selected records
			$faqObj->updateFaqCategoryStatus($ids, '2');
			$_SESSION['msg']="<div class='success_msg'><span>FAQ Category status activated successfully</span></div>";
		} else if($_POST['action'] == 'inactive') {
			## Inactive selected records
			$faqObj->updateFaqCategoryStatus($ids, '1');
			$_SESSION['msg']="<div class='success_msg'><span>FAQ Category status deactivated successfully</span></div>";
		} else {
			
			## Delete selected records
			foreach($_POST['checkall'] as $id)
			{
				$cnt = count($faqObj->getFaqByCatId($id));
				if($cnt <= 0)
				{
					$faqObj->deleteFaqCategoryById($id);
					$_SESSION['msg']="<div class='success_msg'><span>FAQ deleted successfully</span></div>";
				}
			}
		}
		header('location:'.SITE_URL.'/admin/home.php?q=faqCategories&page='.$hidden_page.'&order_by='.$orderBy.'&search='.$searchindex.'&order_field='.$orderField);
			exit;
		## Delete code will be here if required
		
} // if close

## Active/Inactive/Delete thourgh URL
if(isset($_GET['action']) == 'status' && isset($_GET['status']) != '') {
	$id = base64_decode($_GET['id']);
	## Update cms page status 
	$array = array();
	$array['status'] = ($_GET['status']=='1'?2:1);
	$faqObj->editFaqCategoryById($array, $id);
	if($array['status']==2) {
		
		$_SESSION['msg']="<div class='success_msg'><span>FAQ Category status activated successfully.</span></div>";
	} else {
		
		$_SESSION['msg']="<div class='success_msg'><span>FAQ Category status deactivated successfully.</span></div>";
	}
	header('location:'.SITE_URL.'/admin/home.php?q=faqCategories&page='.$_GET['page'].'&order_by='.$orderBy.'&search='.$searchindex.'&order_field='.$orderField);
	exit;
}
if(isset($_GET['action']) == 'delete' && isset($_GET['id']) != '') {
	//print_r($_GET);exit;
	$id = base64_decode($_GET['id']);
	$faqObj->deleteFaqCategoryById($id);
	$_SESSION['msg']="<div class='success_msg'><span>FAQ category deleted successfully</span></div>";
	header('location:'.SITE_URL.'/admin/home.php?q=faqCategories&page='.$_GET['page'].'&order_by='.$orderBy.'&search='.$searchindex.'&order_field='.$orderField);
	exit;
}

//


/*if($_GET['q']=='faqCategories')
{
	$moduelArray=$moduleObj->getModuleHelpText(3);
	$smarty->assign('moduelArray', $moduelArray);
	//echo '<pre>';print_r($moduelArray);exit;
}
*/
/*******************************/
##Fetch all cms pages from database
/******************************/

##  --------- Pagination part first start --------------##
	if( isset( $_REQUEST['page'] ) && $_REQUEST['page'] != "" ) {
		$pageNum = $_REQUEST['page'];
	} else {
		$pageNum = 1;
	} 
	//echo $pageNum;exit;
	$rowsPerPage = ($_GET['setlimit']) ? $_GET['setlimit'] : ROW_PER_PAGE;
	$pageName = "home.php?q=faqCategories&search=$searchindex&order_field=$orderField&order_by=$orderBy&page=".$_GET['page'].'&setlimit='.$rowsPerPage; ; 
	## Count all the records
	$cmsArray = $faqObj->getAllFaqsCategory($searchindex, $orderField, $orderBy);
	$total_rows = count($cmsArray);
	$offset	= ($pageNum - 1) * $rowsPerPage;
	$smarty->assign('page', $pageNum);
##  --------- Pagination part first end --------------##


##  --------- Pagination part second start --------------##
	$newid = $pageNum * $rowsPerPage - $rowsPerPage + 1;
	$smarty->assign('newid', $newid);
	$faqArray = $faqObj->getAllFaqsCategory($searchindex, $orderField, $orderBy, $rowsPerPage,$offset);
	//echo "<pre>"; print_r($cmsArray);exit;
	$i=0;
	foreach($faqArray as $arr)
	{
		$faqArray[$i]['cnt'] = count($faqObj->getFaqByCatId($arr['id']));
		$i++;
	}
	$smarty->assign('faqArray', $faqArray);
	
	if($searchindex != '') {
		$other_id = "search=".$searchindex;
	} else {
		$other_id = '';
	}
	
	if($total_rows > $rowsPerPage) {
		$pg = new pagination();					// CREATING OBJECT OF THE PAGINATION CLASS
		$pgnation = $pg->pagination_admin( $total_rows , $rowsPerPage , $pageNum , $pageName , $other_id, $class);	
		$smarty-> assign("pagination",$pgnation);
	}
##  --------- Pagination part second end --------------##	

## Assign session message to smarty variable and unset session variable
if($_SESSION['msg'] != '') {
	$smarty->assign('msg', $_SESSION['msg']);	
	unset($_SESSION['msg']);
}

## Set active class variable for left menu  (main menu and sub menu)
$smarty->assign('activeclass', 'faqs_cats');
$smarty->assign('mainmenu', '2');

## Unset all the objects created which are on this page
unset($faqObj);
unset($moduleObj);

$smarty->display(TEMPLATEDIR_ADMIN. 'controller/faqCategories/faqCategories.tpl');
unset($smarty);
?>