<?php 
## include required files
require_once '../model/moduleuser.php';
require_once '../model/sports.php';
require_once "../model/common/classes/pagination_class.php";

## Create Objects
$moduleObj  = new Model_ModuleUser();
$sportsObj = new Model_Sports();

//echo 'here';exit;

/*## check user's permission
if($modules_new[15]['perStatus'] !=1 || $modules_new[15]['submod_58'] !=1) {
	header('location: '.SITE_URL.'/admin/home.php');
	exit;
}*/

## Get search parameters in variables - 
if(isset($_REQUEST['search']) != '' && $_REQUEST['search'] != 'Search') {
	$searchindex = return_post_value($_REQUEST['search']);
} else {
	$searchindex = '';
}
$smarty->assign('search', return_fetched_value($searchindex));

if($_REQUEST['order_by']!='') {
	$order_by = $_REQUEST['order_by'];
} else {
	$order_by = 'ASC';
}
$smarty->assign('order_by',$order_by);

if($_REQUEST['order_field']!='') {
	$order_field = $_REQUEST['order_field'];
} else {
	$order_field = 'sportsTitle';
}
$smarty->assign('order_field',$order_field);

##Fetch all admin users from database
if($_GET['q']=='managesports') {
	$moduelArray = $moduleObj->getModuleHelpText(11);
	$smarty->assign('moduelArray', $moduelArray);
}

## Active, Inactive selected records
/*******************************/
if(isset($_POST['go']) != '' && isset($_POST['action']) != '') {
		$ids = implode(",", $_POST['checkall']);
		## check which action is selected
		if(trim($ids) != '' ) {
			$hidden_page = $_POST['hidden_pageno'];	
			if($_POST['action'] == 'active') {
				## Active selected records
				$sportsObj->updateSportsStatus($ids, '2');
				$_SESSION['msg']="<div class='success_msg'><span>Sports activated successfully.</span></div>";
			} else if($_POST['action'] == 'inactive') {
				## Inactive selected records
				$sportsObj->updateSportsStatus($ids, '1');
				$_SESSION['msg']="<div class='success_msg'><span>Sports deactivated successfully.</span></div>";
			} else {
				## Delete selected records
				$sportsObj->updateSportsStatus($ids, '0');
				$_SESSION['msg']="<div class='success_msg'><span>Sports deleted successfully.</span></div>";
			}
				header('location:'.SITE_URL.'/admin/home.php?q=managesports&order_field='.$order_field.'&order_by='.$order_by.'&search='.$searchindex.'&page='.$hidden_page);
				exit;
		}
		
}

## Active/Inactive sports Status by id
if(isset($_GET['action']) == 'status' && isset($_GET['status']) != '') {
	$id = base64_decode($_GET['id']);
	if($_GET['status']==1){
		## Update sports status	
		$array = array();
		$array['status'] = ($_GET['status']=='1'?2:1);
		$sportsObj->editSportsValueById($array, $id);
			
		$_SESSION['msg']="<div class='success_msg'><span>Sports activated successfully.</span></div>";
		header('location:'.SITE_URL.'/admin/home.php?q=managesports&search='.$searchindex.'&order_field='.$order_field.'&order_by='.$order_by.'&page='.$_GET['page']);
		exit;
	} 
	if($_GET['status']==2)
	{
		## Update sports status
		$array = array();
		$array['status'] = ($_GET['status']=='2'?1:2);
		$sportsObj->editSportsValueById($array, $id);
		$_SESSION['msg']="<div class='success_msg'><span>Sports deactivated successfully.</span></div>";
		header('location:'.SITE_URL.'/admin/home.php?q=managesports&search='.$searchindex.'&order_field='.$order_field.'&order_by='.$order_by.'&page='.$_GET['page']);
		exit;
	}
}

	## Delete sports by id
	if(isset($_GET['action']) == 'delete' && isset($_GET['id']) != '') 
	{
			$id = base64_decode($_GET['id']);
			$array = array();
			$array['status'] = '0';
			$sportsObj->editSportsValueById($array, $id);	
			$_SESSION['msg']="<div class='success_msg'><span>Sports deleted successfully.</span></div>";
			header('location:'.SITE_URL.'/admin/home.php?q=managesports&search='.$searchindex.'&order_field='.$order_field.'&order_by='.$order_by.'&page='.$_GET['page']);
			exit;
	}

## --------- Pagination part first start --------------##
if( isset( $_GET[ 'page' ] ) && $_GET[ 'page' ] != "" ) {
		$pageNum = $_GET[ 'page' ];
} else {
		$pageNum = 1;
} 
	$rowsPerPage = ROW_PER_PAGE;
	$pageName = "home.php?q=managesports&order_field=".$order_field."&order_by=".$order_by."&search=".$searchindex;
	## Count all the records
	$sportsArray = $sportsObj->getAllSportsForSports($searchindex,$order_field,$order_by);
    
	$total_rows = count($sportsArray);
	$offset	= ($pageNum - 1) * $rowsPerPage;
	$smarty->assign('page', $pageNum);
	##--------- Pagination part first end --------------##


	##--------- Pagination part second start --------------##
	$newid = $pageNum * $rowsPerPage - $rowsPerPage + 1;
	$smarty->assign('newid', $newid);
	$sportsArray = $sportsObj->getAllSportsForSports($searchindex,$order_field,$order_by,$rowsPerPage,$offset);
	$smarty->assign('sportsArray', $sportsArray);

	$other_id = '';

	if($total_rows > ROW_PER_PAGE) {
	$pg = new pagination();
	$pgnation = $pg->pagination_admin( $total_rows , $rowsPerPage , $pageNum , $pageName , $other_id, $class);	
	$smarty-> assign("pagination",$pgnation);
	}
	##  --------- Pagination part second end --------------##	

## Assign session message to smarty variable and unset session variable
if(isset($_SESSION['msg']) &&  $_SESSION['msg']!= '') {
	$smarty->assign('msg', $_SESSION['msg']);	
	unset($_SESSION['msg']);
}

## Set active class variable for left menu  (main menu and sub menu)
$smarty->assign('activeclass', 'managesports');
$smarty->assign('mainmenu', '2');

## Unset all the objects created which are on this page
unset($sportsObj);
unset($moduleObj);

$smarty->display(TEMPLATEDIR_ADMIN. 'controller/sports/managesports.tpl');
unset($smarty);
?>
