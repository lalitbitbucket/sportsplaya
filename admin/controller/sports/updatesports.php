<?php 
## include required files
/*******************************/
require_once '../model/sports.php';
require_once '../model/common/image_functions.php';
/*******************************/
## Create Objects
/*******************************/
$sportsObj = new Model_Sports();
/*******************************/
## For page back pagination, searching and sorting
if($_REQUEST['page']){
	$page = $_REQUEST['page'];
	$smarty->assign('page',$page);
} else {
	$search = '';
}

if($_REQUEST['search']){
	$search = $_REQUEST['search'];
	$smarty->assign('search',$search);
} else {
	$search = '';
}

if($_REQUEST['order_field']){
	$order_field = $_REQUEST['order_field'];
	$smarty->assign('order_field',$order_field);
} else {
	$order_field = '';
}

if($_REQUEST['order_by']){
	$order_by = $_REQUEST['order_by'];
	$smarty->assign('order_by',$order_by);
} else {
	$order_by = '';
}

## Fetch sports details by id
if($_GET['id'] != '' && $_GET['action'] == 'edit') 
    	{
    
	 $sports_id = base64_decode($_GET['id']);
	 $sports_details = $sportsObj->getSportsDetailById($sports_id);
	 $smarty->assign('sports_details', $sports_details);
	}

	if(isset($_POST) && !empty($_POST)) {
	
		if(trim($_POST['sportsTitle']) != '') {
		$sportsArray = array();
		$sportsArray['sportsTitle'] = return_post_value($_POST['sportsTitle']);
	
	if($_FILES['image']['name']!= '')
	{
		$imageName    = $_FILES['image']['name'];
		$fileName     = date('Ymdhis').".".$imageName;		
		$temp=$_FILES["image"]["tmp_name"];					
		$fileName      = time()."_".$_FILES['image']['name'];
		$imageFolder='../dynamicAssets/sports/';
		$imageName= $fileName;
		$sportsArray['sportsImage'] = $imageName;

		$thumbFolder1 ="../dynamicAssets/sports/27x27/";						
		$height1 = 27;
		$width1= 27;
		$file1=uploadImageAndCreateThumb($temp,$imageName,$imageFolder,$thumbFolder1,$height1,$width1);

		$thumbFolder2 ="../dynamicAssets/sports/61x61/";						
		$height2 = 61;
		$width2= 61;
		$file1=uploadImageAndCreateThumb($temp,$imageName,$imageFolder,$thumbFolder2,$height2,$width2);

		$thumbFolder3 ="../dynamicAssets/sports/15x15/";						
		$height3 = 15;
		$width3 = 15;
		$file1=uploadImageAndCreateThumb($temp,$imageName,$imageFolder,$thumbFolder3,$height3,$width3);
			
 	}
	$sportsObj->editsportsValueById($sportsArray,$sports_id);
	$_SESSION['msg'] = "<div class='success_msg'><span>Sports updated successfully</span></div>";
		header('location:'.SITE_URL.'/admin/home.php?q=managesports&page='.$page.'&search='.$search.'&order_field='.$order_field.'&order_by='.$order_by);
		exit();
}
	 else { 
		$_SESSION['msg'] = '<div class="error_msg"><span>Please enter sports title</span></div>';
	}
	// main if closed
}
 
## Assign success or error msg to smarty variable and unset session variable
if(trim($_SESSION['msg']) != '') {
	$smarty->assign('msg', $_SESSION['msg']);
	unset($_SESSION['msg']);
}

## Set active class variable for left menu
$smarty->assign('activeclass', 'managesports');
$smarty->assign('mainmenu', '2');
## Unset all the objects created which are on this page
unset($prodCatObj);
$smarty->display(TEMPLATEDIR_ADMIN. 'controller/sports/updatesports.tpl');
unset($smarty);
?>
