<?php 
## include required files
/*******************************/
require_once '../model/sports.php';
require_once "../model/common/classes/pagination_class.php";
require_once '../model/common/image_functions.php';
/*******************************/

## Create Objects
/*******************************/
$sportsObj = new Model_Sports();
/*******************************/

## For page back pagination, searching and sorting
if($_REQUEST['page']){
	$page = $_REQUEST['page'];
	$smarty->assign('page',$page);
} else {
	$search = '';
}
if($_REQUEST['search']){
	$search = $_REQUEST['search'];
	$smarty->assign('search',$search);
} else {
	$search = '';
}

if($_REQUEST['order_field']){
	$order_field = $_REQUEST['order_field'];
	$smarty->assign('order_field',$order_field);
} else {
	$order_field = '';
}

if($_REQUEST['order_by']){
	$order_by = $_REQUEST['order_by'];
	$smarty->assign('order_by',$order_by);
} else {
	$order_by = '';
}

if(count($_POST)>0) 
{
## apply PHP validation for required filed
if(trim($_POST['sports_title']) != '') 
		 {
			$sportsArray = array();
			$sportsArray['sportsTitle'] = return_post_value($_POST['sports_title']);
			$sportsArray['status'] 	    = 2;

			if($_FILES['image']['name']!= '')
			{
			$imageName    = $_FILES['image']['name'];
			$fileName     = date('Ymdhis').".".$imageName;		
			$temp=$_FILES["image"]["tmp_name"];					
			$fileName      = time()."_".$_FILES['image']['name'];
			$imageFolder='../dynamicAssets/sports/';
			$imageName= $fileName;
			$sportsArray['sportsImage'] = $fileName;	

			$thumbFolder1 ="../dynamicAssets/sports/27x27/";						
			$height1 = 27;
			$width1= 27;
			$file1=uploadImageAndCreateThumb($temp,$imageName,$imageFolder,$thumbFolder1,$height1,$width1);

			$thumbFolder2 ="../dynamicAssets/sports/61x61/";						
			$height2 = 61;
			$width2= 61;
			$file1=uploadImageAndCreateThumb($temp,$imageName,$imageFolder,$thumbFolder2,$height2,$width2);

			$thumbFolder3 ="../dynamicAssets/sports/15x15/";						
			$height3 = 15;
			$width3 = 15;
			$file1=uploadImageAndCreateThumb($temp,$imageName,$imageFolder,$thumbFolder3,$height3,$width3);

			}
			
			$sportsId = $sportsObj->addNewSports($sportsArray);
			$_SESSION['msg'] = "<div class='success_msg'><span>Sports added successfully</span></div>";
			header('location:'.SITE_URL.'/admin/home.php?q=managesports&page='.$page.'&search='.$search.'&order_field='.$order_field.'&order_by='.$order_by);
			exit;
		}
	 else { 
		$_SESSION['msg'] = '<div class="error_msg"><span>Please fill all required fields</span></div>';
	}
	// main if closed
}

## Assign success or error msg to smarty variable and unset session variable
if(trim($_SESSION['msg']) != '') {
	$smarty->assign('msg', $_SESSION['msg']);
	unset($_SESSION['msg']);
}

## Set active class variable for left menu
$smarty->assign('activeclass', 'managesports');
$smarty->assign('mainmenu', '2');
## Unset all the objects created which are on this page
unset($sportsObj);

$smarty->display(TEMPLATEDIR_ADMIN. 'controller/sports/addnewsports.tpl');
unset($smarty);
?>
