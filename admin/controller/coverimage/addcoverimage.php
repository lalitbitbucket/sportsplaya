<?php 
## include required files
/*******************************/
require_once '../model/coverimage.php';
require_once "../model/common/classes/pagination_class.php";
require_once '../model/common/image_functions.php';
/*******************************/
## Create Objects
/*******************************/
$coverimageObj = new Model_Coverimage();
/*******************************/
## For page back pagination, searching and sorting
if($_REQUEST['page']){
	$page = $_REQUEST['page'];
	$smarty->assign('page',$page);
} else {
	$search = '';
}
if($_REQUEST['search']){
	$search = $_REQUEST['search'];
	$smarty->assign('search',$search);
} else {
	$search = '';
}

if($_REQUEST['order_field']){
	$order_field = $_REQUEST['order_field'];
	$smarty->assign('order_field',$order_field);
} else {
	$order_field = '';
}

if($_REQUEST['order_by']){
	$order_by = $_REQUEST['order_by'];
	$smarty->assign('order_by',$order_by);
} else {
	$order_by = '';
}

if(count($_POST)>0) 
{
## apply PHP validation for required filed
if(trim($_POST['coverimage_title']) != '') 
		 {
			$coverimageArray = array();
			$coverimageArray['coverimageTitle'] = return_post_value($_POST['coverimage_title']);
			$coverimageArray['status'] 	    = 2;

			if($_FILES['image']['name']!= '')
			{
			$imageName    = $_FILES['image']['name'];
			$fileName     = date('Ymdhis').".".$imageName;		
			$temp=$_FILES["image"]["tmp_name"];					
			$fileName      = time()."_".$_FILES['image']['name'];
			$imageFolder='../dynamicAssets/coverimage/';
			$imageName= $fileName;
			$coverimageArray['coverimageImage'] = $fileName;	

			$thumbFolder1 ="../dynamicAssets/coverimage/27x27/";						
			$height1 = 27;
			$width1= 27;
			$file1=uploadImageAndCreateThumb($temp,$imageName,$imageFolder,$thumbFolder1,$height1,$width1);
			}
			
			$coverimageId = $coverimageObj->addNewCoverimage($coverimageArray);
			$_SESSION['msg'] = "<div class='success_msg'><span>Coverimage added successfully</span></div>";
			header('location:'.SITE_URL.'/admin/home.php?q=managecoverimage&page='.$page.'&search='.$search.'&order_field='.$order_field.'&order_by='.$order_by);
			exit;
		}
	 else { 
		$_SESSION['msg'] = '<div class="error_msg"><span>Please fill all required fields</span></div>';
	}
	// main if closed
}

## Assign success or error msg to smarty variable and unset session variable
if(trim($_SESSION['msg']) != '') {
	$smarty->assign('msg', $_SESSION['msg']);
	unset($_SESSION['msg']);
}

## Set active class variable for left menu
$smarty->assign('activeclass', 'coverimage');
$smarty->assign('mainmenu', '2');
## Unset all the objects created which are on this page
unset($coverimageObj);

$smarty->display(TEMPLATEDIR_ADMIN. 'controller/coverimage/addnewcoverimage.tpl');
unset($smarty);
?>
