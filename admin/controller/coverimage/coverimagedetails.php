<?php 
## include required files
/*******************************/
require_once '../model/coverimage.php';
/*******************************/
## Create Objects
/*******************************/
$coverimageObj = new Model_coverimage();
/*******************************/

## For page back pagination, searching and sorting
if($_REQUEST['page']){
	$page = $_REQUEST['page'];
	$smarty->assign('page',$page);
} else {
	$search = '';
}
if($_REQUEST['search']){
	$search = $_REQUEST['search'];
	$smarty->assign('search',$search);
} else {
	$search = '';
}

if($_REQUEST['order_field']){
	$order_field = $_REQUEST['order_field'];
	$smarty->assign('order_field',$order_field);
} else {
	$order_field = '';
}

if($_REQUEST['order_by']){
	$order_by = $_REQUEST['order_by'];
	$smarty->assign('order_by',$order_by);
} else {
	$order_by = '';
}

## Fetch coverimage details by id
if($_GET['id'] != '') {
	$coverimage_id = base64_decode($_GET['id']);	
	$coverimage_details = $coverimageObj->getcoverimageDetailById($coverimage_id);
	$smarty->assign('coverimage_details', $coverimage_details);
}

## Set active class variable for left menu
$smarty->assign('activeclass', 'coverimage');
$smarty->assign('mainmenu', '2');
## Unset all the objects created which are on this page
unset($coverimageObj);
$smarty->display(TEMPLATEDIR_ADMIN. 'controller/coverimage/coverimagedetails.tpl');
unset($smarty);
?>
