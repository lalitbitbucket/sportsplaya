<?php 
## include required files
/*******************************/
require_once '../model/coverimage.php';
require_once '../model/common/image_functions.php';
/*******************************/

## Create Objects
/*******************************/
$coverimageObj = new Model_Coverimage();
/*******************************/
## For page back pagination, searching and sorting
if($_REQUEST['page']){
	$page = $_REQUEST['page'];
	$smarty->assign('page',$page);
} else {
	$search = '';
}

if($_REQUEST['search']){
	$search = $_REQUEST['search'];
	$smarty->assign('search',$search);
} else {
	$search = '';
}

if($_REQUEST['order_field']){
	$order_field = $_REQUEST['order_field'];
	$smarty->assign('order_field',$order_field);
} else {
	$order_field = '';
}

if($_REQUEST['order_by']){
	$order_by = $_REQUEST['order_by'];
	$smarty->assign('order_by',$order_by);
} else {
	$order_by = '';
}

//echo"<pre>"; print_r($_GET); exit;

## Fetch coverimage details by id
if($_GET['id'] != '' && $_GET['action'] == 'edit') 
    	{
 	 $coverimage_id = base64_decode($_GET['id']);
	 $coverimage_details = $coverimageObj->getCoverimageDetailById($coverimage_id);
	 $smarty->assign('coverimage_details', $coverimage_details);
	}

	if(isset($_POST) && !empty($_POST)) 
	{
	 ## apply PHP validation for required filed
	 /*if(trim($_POST['usertype']) != '') {
	  //echo"<pre>"; print_r($_POST); exit;
	 $coverimageArray = array();
	 $coverimageArray['usertype'] 	= return_post_value($_POST['usertype']);
	 $coverimageArray['gender'] 	= return_post_value($_POST['gender']);*/
	 //$coverimageArray['status'] 	= 2;
	
	if($_FILES['image']['name']!= '')
	{	
		$imageName    = $_FILES['image']['name'];
		$fileName     = date('Ymdhis').".".$imageName;		
		$temp=$_FILES["image"]["tmp_name"];					
		$fileName      = time()."_".$_FILES['image']['name'];
		$imageFolder='../dynamicAssets/coverimage/';
		$imageName= $fileName;
		$coverimageArray['image'] = $imageName;

		$thumbFolder1 ="../dynamicAssets/coverimage/1170x411/";						
		$height1 = 411;
		$width1= 1170;
		$file1=uploadImageAndCreateThumb($temp,$imageName,$imageFolder,$thumbFolder1,$height1,$width1);
 	}

	$coverimageObj->editCoverimageValueById($coverimageArray,$coverimage_id);
	$_SESSION['msg'] = "<div class='success_msg'><span>Cover image updated successfully</span></div>";
	header('location:'.SITE_URL.'/admin/home.php?q=coverimage&page='.$page.'&search='.$search.'&order_field='.$order_field.'&order_by='.$order_by);
	exit();
	/*}
	 else { 
		$_SESSION['msg'] = '<div class="error_msg"><span>Please enter required fields</span></div>';
	}*/
	// main if closed
}

## Assign success or error msg to smarty variable and unset session variable
if(trim($_SESSION['msg']) != '') {
	$smarty->assign('msg', $_SESSION['msg']);
	unset($_SESSION['msg']);
}

## Set active class variable for left menu
$smarty->assign('activeclass', 'coverimage');
$smarty->assign('mainmenu', '2');
## Unset all the objects created which are on this page
unset($prodCatObj);
$smarty->display(TEMPLATEDIR_ADMIN. 'controller/coverimage/updatecoverimage.tpl');
unset($smarty);
?>
