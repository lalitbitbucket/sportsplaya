<?php
## include required files
/*******************************/ 
include_once("../model/plan.php");
/*******************************/
## Create Objects
/*******************************/
$planObj = new Model_Plan();

if ($_GET['plan_name']) {
	$planName = stripslashes($_GET['plan_name']); 
}
if ($_GET['id']!='' && $_GET['id']!='0'){
	$id = $_GET['id'];
}

if($planName) {
	// this function check plan name Exist ot not
	$result = $planObj->checkplanNameExists(trim($planName), $id); 
	if(count($result) > 0){ 	
		if(strtolower($result['planName']) == strtolower ($planName)){  
			echo 'false';
		}
	} else {
		echo "true";	
	}
}
unset($planObj);
?>
