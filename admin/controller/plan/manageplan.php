<?php 
## include required files
require_once '../model/plan.php';
## Paginaton class
require_once ("../model/common/classes/pagination_class.php");

## Create Objects
$planObj = new Model_Plan();

## Get search parameters in variables - 
if(isset($_POST['search']) != '' && $_POST['search'] != 'Search') {

	$searchindex = trim(($_POST['search']));
} else if(isset($_GET['search']) &&  $_GET['search']!= '') {
	$searchindex = trim(($_GET['search']));
} else {
	$searchindex = '';
}
// Assign search variable to show in search textbox
$smarty->assign('search', $searchindex);

if(isset($_GET['order_field']) && $_GET['order_field'] != '') {
	$orderField = $_GET['order_field'];
}
else {
	$orderField = 'membership_plan';
}
$smarty->assign('orderField', $orderField);

if(isset($_GET['order_by']) && $_GET['order_by'] != '') {
	$orderBy = $_GET['order_by'];
}
else {
	$orderBy = 'ASC';
}
$smarty->assign('orderBy', $orderBy);

## Active, Inactive selected records
/*******************************/
if(isset($_POST['go']) != '' && isset($_POST['action']) != '') {
		$ids = @implode(",", $_POST['checkall']);
		## check which action is selected
		$hidden_page = $_POST['hidden_pageno'];
		if($ids != '') {
			if($_POST['action'] == 'active') {
				## Active selected records
				$planObj->updateplanuresStatus($ids, '2');
				$_SESSION['msg']="<div class='success_msg'><span>Plan status activated successfully.</span></div>";
			} else if($_POST['action'] == 'inactive') {
				## Inactive selected records
				$planObj->updateplanuresStatus($ids, '1');
				$_SESSION['msg']="<div class='success_msg'><span>Plan status deactivated successfully.</span></div>";
			} else {
				## Delete selected records
				$planObj->updateplanuresStatus($ids, '0');
				$_SESSION['msg']="<div class='success_msg'><span>Plan deleted successfully</span></div>";
			}
		}
		header('location:'.SITE_URL.'/admin/home.php?q=manageplan&order_by='.$orderBy.'&search='.$searchindex.'&page='.$hidden_page);
			exit;
		## Delete code will be here if required
		
} // if close


## Active/Inactive User Status by id
if(isset($_GET['action']) == 'status' && isset($_GET['status']) != '') {

		$id = base64_decode($_GET['id']);

		if($_GET['status']==1){
		## Update user status	
		$array = array();
		$array['status'] = ($_GET['status']=='2'?1:2);
		
		$planObj->editplanValueById($array, $id);
			
			$_SESSION['msg']="<div class='success_msg'><span>Plan activated successfully.</span></div>";
			header('location:'.SITE_URL.'/admin/home.php?q=manageplan&order_by='.$orderBy.'&search='.$searchindex.'&page='.$_GET['page']);
		exit;
		} 
	
		## Update user status
		$array = array();
		$array['status'] = ($_GET['status']=='2'?1:2);
		
		$planObj->editplanValueById($array, $id);
		$_SESSION['msg']="<div class='success_msg'><span>Plan deactivated successfully.</span></div>";
		header('location:'.SITE_URL.'/admin/home.php?q=manageplan&order_by='.$orderBy.'&search='.$searchindex.'&page='.$_GET['page']);
		exit;
}

## Delete Product planures by id
if(isset($_GET['action']) == 'delete' && isset($_GET['id']) != '') {
		$id = base64_decode($_GET['id']);
		## Get Count Presenr In Product and Status as active
		
		## Delete Product planures
		$planObj->deleteplanValueById($id);	
		$_SESSION['msg']="<div class='success_msg'><span>Plan deleted successfully.</span></div>";
		header('location:'.SITE_URL.'/admin/home.php?q=manageplan&order_by='.$orderBy.'&search='.$searchindex.'&page='.$_GET['page']);
		exit;		
		
}

## --------- Pagination part first start --------------##
if( isset( $_REQUEST[ 'page' ] ) && $_REQUEST[ 'page' ] != "" ) {
	$pageNum = $_REQUEST[ 'page' ];
} else {
	$pageNum = 1;
}  
$rowsPerPage = ($_REQUEST['setlimit']) ? $_REQUEST['setlimit'] : ROW_PER_PAGE;
$pageName = "home.php?q=manageplan&order_by=".$orderBy."&order_field=".$orderField."&search=".$searchindex."&setlimit=".$_REQUEST['setlimit']; 
## Count all the records

$planArray = $planObj->getAllplan($searchindex,$orderField,$orderBy);

$total_rows = count($planArray);
$offset	= ($pageNum - 1) * $rowsPerPage;
$smarty->assign('page', $pageNum);
##--------- Pagination part first end --------------##


##--------- Pagination part second start --------------##
$newid = $pageNum * $rowsPerPage - $rowsPerPage + 1;
$smarty->assign('newid', $newid);
$planArray = $planObj->getAllplan($searchindex, $orderField, $orderBy, $rowsPerPage,$offset);
$smarty->assign('planArray', $planArray);
//echo"<xmp>"; print_r($planArray); echo"</xmp>"; 

$smarty->assign('planArray', $planArray);
$smarty-> assign("newidd",1);
if($searchindex != '') {
	$other_id = "search=".$searchindex."&order_by=".$orderBy."&order_field=".$orderField;
} else {
	$other_id = '';
}
if($total_rows > ROW_PER_PAGE) {
	$pg = new pagination();
	$pgnation = $pg->pagination_admin( $total_rows , $rowsPerPage , $pageNum , $pageName , $other_id, $class);	
	$smarty-> assign("pagination",$pgnation);
}
##  --------- Pagination part second end --------------##	

## Assign session message to smarty variable and unset session variable
if(isset($_SESSION['msg']) &&  $_SESSION['msg']!= '') {
	$smarty->assign('msg', $_SESSION['msg']);	
	unset($_SESSION['msg']);
}

## Set active class variable for left menu  (main menu and sub menu)
$smarty->assign('activeclass', 'manageplan');
$smarty->assign('mainmenu', '2');

## Unset all the objects created which are on this page
unset($planObj);

$smarty->display(TEMPLATEDIR_ADMIN. 'controller/plan/manageplan.tpl');
unset($smarty);
?>
