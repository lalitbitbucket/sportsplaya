<?php
## include required files
/*******************************/
require_once '../model/plan.php';
require_once '../model/features.php';
/*******************************/

## Create Objects
/*******************************/
$planObj = new Model_Plan();
$featObj = new Model_Features();
/*******************************/

/************* Features List ******************/
$featuresList = $featObj->getAllActivefeatures();
$smarty->assign('featuresList', $featuresList);

## For page back pagination, searching and sorting
if($_REQUEST['page']){
	$page = $_REQUEST['page'];
	$smarty->assign('page',$page);
} else {
	$search = '';
}
if($_REQUEST['search']){
	$search = $_REQUEST['search'];
	$smarty->assign('search',$search);
} else {
	$search = '';
}
if($_REQUEST['order_by']){
	$order_by = $_REQUEST['order_by'];
	$smarty->assign('order_by',$order_by);
} else {
	$order_by = '';
}

if(isset($_POST) && !empty($_POST)) {
	## apply PHP validation for required filed
	if(trim($_POST['plan_name']) != '') {
		## check planure already exists
		
		$planArr = $planObj->checkplanNameExists(stripslashes(trim($_POST['plan_name'])), ''); 
		
		if(count($planArr) == 0) {
			$planArray = array();

			$planArray['membership_plan'] 	= stripslashes(trim($_POST['plan_name']));
			$planArray['status'] 		= 2;	
			$planArray['added_date'] 	= date("Y-m-d H:i:s");	
			## add product plan
			$planId = $planObj->addplanByValue($planArray);

			/*********** Insert data into plan features table *********/
							
			for($i=0; $i < count($_POST['features']); $i++)
			 {
				if(trim($_POST['features'][$i]) != '')
				{
				  $featuresArr = array();
				  $featuresArr['membPlanId'] = $planId;
				  $featuresArr['features'] = return_post_value($_POST['features'][$i]);
				  $planObj->addplanFeaturesByValue($featuresArr); 
			       }
			 }

			/*************************************************************/

			$_SESSION['msg'] = "<div class='success_msg'><span>Plan details added successfully</span></div>";
			header('location:'.SITE_URL.'/admin/home.php?q=manageplan');
			exit;
		} else {
			$_SESSION['msg'] = '<div class="error_msg"><span>Plan already exists</span></div>';
		}
	} else { 
		$_SESSION['msg'] = '<div class="error_msg"><span>Please fill all required fields</span></div>';
	}
	// main if closed
}

## Assign success or error msg to smarty variable and unset session variable
if(trim($_SESSION['msg']) != '') {
	$smarty->assign('msg', $_SESSION['msg']);
	unset($_SESSION['msg']);
}

## Set active class variable for left menu
$smarty->assign('activeclass', 'manageplan');
$smarty->assign('mainmenu', '2');
## Unset all the objects created which are on this page
unset($planObj);

$smarty->display(TEMPLATEDIR_ADMIN. 'controller/plan/addplan.tpl');
unset($smarty);
?>
