<?php 
## include required files
/*******************************/
require_once '../model/email.php'; 
require_once '../model/moduleuser.php';
## Paginaton class
require_once ("../model/common/classes/pagination_class.php");
/*******************************/

## Create Objects
/*******************************/
$emailObj  =  new Model_email();
$moduleObj =  new Model_ModuleUser();

if($_GET['q']=='emailTemplates')
{
   $moduelArray=$moduleObj->getModuleHelpText(1);
   $smarty->assign('moduelArray', $moduelArray);
}

if(isset($_POST['search']) != '' && $_POST['search'] != 'Email Type or From Email') {
//	echo 'aaaaaa';exit;
	$search = $_POST['search'];
} else if($_GET['search'] != '') {
	$search = $_GET['search'];
} else {
	$search = '';
}

$smarty->assign("search",$search);
//print_r($_POST);exit;	
##Fetch all email from database
##  --------- Pagination part first start --------------##
$result = $emailObj->getAllEmailByValue(return_post_value($search));	
$pageName = "home.php?q=emailTemplates";
if( isset( $_GET[ 'page' ] ) && $_GET[ 'page' ] != "" ) {
	$pageNum = $_GET[ 'page' ];
} else {
	$pageNum = 1;
}
$smarty->assign('page', $pageNum);
$rowsPerPage =ROW_PER_PAGE;
$total_rows = count($result);
$offset	= ($pageNum - 1) * $rowsPerPage;
## Count all the records
$array = $emailObj->getAllEmailByValue(return_post_value($search),$rowsPerPage, $offset);
$smarty->assign("array",$array);
##  --------- Pagination part first end --------------##

##  --------- Pagination part second start --------------##
$newid = $pageNum * $rowsPerPage - $rowsPerPage + 1;
$smarty-> assign("newid",$newid);
$class 	= 	"";
$other_id = "search".$search;
if($total_rows > ROW_PER_PAGE) {
	$pg = new pagination();					// CREATING OBJECT OF THE PAGINATION CLASS
	$pgnation = $pg->pagination_admin($total_rows , $rowsPerPage , $pageNum , $pageName , $other_id, $class);		
	$smarty-> assign("pagination",$pgnation);
}
##  --------- Pagination part second end --------------##	

## List all emails
$getAllEmails = $emailObj->getAllEmails();
$smarty->assign('getAllEmails', $getAllEmails);

## Assign session message to smarty variable and unset session variable
if($_SESSION['msg']){
	$smarty->assign("msg", $_SESSION['msg']);
	unset($_SESSION['msg']);
}

## Set active class variable for left menu  (main menu and sub menu)
$smarty->assign('activeclass', 'email');
$smarty->assign('mainmenu', '2');

## Unset all the objects created which are on this page
unset($emailObj);
unset($moduleObj);
	
$smarty->display(TEMPLATEDIR_ADMIN . 'controller/emailTemplates/emailTemplates.tpl');
unset($smarty);
?>
