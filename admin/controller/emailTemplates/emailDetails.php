<?php 
## include required files
/*******************************/
require_once '../model/email.php';	
/*******************************/
## Create Objects
/*******************************/
$emailObj 	= new Model_email();
$page = $_GET['page'];

// fetching the email detail by id
$email_id=base64_decode($_GET['id']);
$array = $emailObj->getEmailById($email_id);
$smarty->assign("array",$array);

## Set active class variable for left menu  (main menu and sub menu)
$smarty->assign('activeclass', 'email');
$smarty->assign('mainmenu', '2');

## Unset all the objects created which are on this page
unset($emailObj);

$smarty->display(TEMPLATEDIR_ADMIN . 'controller/emailTemplates/emailDetails.tpl');
unset($smarty);
?>
