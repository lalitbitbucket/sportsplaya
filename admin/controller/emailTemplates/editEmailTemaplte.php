<?php 
## include required files
/*******************************/
require_once '../model/email.php';
/*******************************/
## Create Objects
/*******************************/
$emailObj 	= new Model_email();

$email_id=base64_decode($_GET['id']);



if( isset($_POST['editemail_btn'] ))	{
	if($_POST['email_subject']!='' && $_POST['email_content']!='')
	{ ## check the php validation
		
	$eid=base64_decode($_GET['id']);

		$emailArray = array();
		$emailArray['emailType'] = return_post_value($_POST['email_type']);
		$emailArray['emailSub'] = return_post_value($_POST['email_subject']);
		$emailArray['fName'] = return_post_value($_POST['from_name']);
		$emailArray['fEmail'] = return_post_value($_POST['from_email']);
		$emailArray['emailContent'] = return_post_value($_POST['email_content']);
		
		$emailObj->editEmailById($emailArray,$eid) ;
		$_SESSION['msg']="<div class='success_msg'><span>Email content updated successfully!</span></div>";
		header('Location: '.SITE_URL.'/admin/home.php?q=emailTemplates&page='.$_POST['page']);
		exit;
	}## php validation else
	else
	{
		$_SESSION['msg'] = "<div class='error_msg'><span>Please enter email subject and content</span></div>";
	}
}## main if close


## Assign session message to smarty variable and unset session variable
if($_SESSION['msg']){
	$smarty->assign("msg", $_SESSION['msg']);
	unset($_SESSION['msg']);
}


// get email values for edit
$emailList 	= $emailObj->getEmailById($email_id);	
$smarty->assign("emailList",$emailList);

## Set active class variable for left menu  (main menu and sub menu)
$smarty->assign('activeclass', 'email');
$smarty->assign('mainmenu', '2');

unset($emailObj);

$smarty->display(TEMPLATEDIR_ADMIN . 'controller/emailTemplates/editEmailTemaplte.tpl');
unset($smarty);
?>