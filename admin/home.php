<?php 
//error_reporting(E_ALL);
## include required files
/*******************************/
require_once 'config/settings.php';
require_once 'chkadminsession.php';

## Check admin login
CheckAdminLogin();
require_once '../model/common/functions.php';
require_once '../model/moduleuser.php';
/*******************************/

## Create Objects
/*******************************/
$moduleObj = new Model_ModuleUser();
/*******************************/
if(isset($_SESSION["admin_type"]) && $_SESSION["admin_type"]!=""){
    $result = $moduleObj->getModuleUserValueByUserIdWithActiveStatus($_SESSION["adminSporttagId"]);
	$rowcnt = count($result);				
	foreach($result as $rs) {		
	 	$mod_per[$rs['moduleId']]=$rs['status'];		 
	}	
	$_SESSION["mod_per"]=$mod_per;
	$smarty->assign("mod_per",$mod_per);
}
          
$admin_id=$_SESSION['adminSporttagId'];

## Stored current time in smarty variable
//$currTime = date("F d, Y H:i:s", time());
$currTime = date("F j, Y, g:i:s a");
$smarty->assign("currTime", $currTime);

## Stored session username	
if(isset($_SESSION["admin_user_name"])) {
	$adminusername = $_SESSION["admin_user_name"];
	$smarty->assign("adminusername", $adminusername);	
}

## Fetch all modules
$modules = $moduleObj->getAllMasterModules();
//$smarty->assign('modules', $modules);
//echo "<xmp>"; print_r($modules); echo "</xmp>";

#Check Previlages

if($admin_id!='') {
	$modules_new = $moduleObj->getAllMasterModules();
	$i=0;
	foreach($modules_new as $rs) {
		$submodArr=array();
		$sub_module_str="";
		$k=0;
		$s=0;
		$row = $moduleObj->getAllAdminSubModules($rs['moduleId']);

		foreach($row as $rs_row) {
			$submodArr[$s]=$rs_row['moduleId'];
			$s++;
		}		
		if($submodArr) {
			$sub_module_str=implode(",",$submodArr);
			$modules_new[$i]['sub_module_str']=$sub_module_str;
		}
		//echo'<pre>';print_r($row);
		foreach($row as $rs_row) {
			$perStatus = $moduleObj->getAdminAssignedSubModules($admin_id,$rs_row['subModID']);
			//echo '<br><pre>';print_r($perStatus);	
			if(!empty($perStatus) && $perStatus['perStatus']==1)
				$modules_new[$i]['submod_'.$rs_row['subModID']]=1;
			else
				$modules_new[$i]['submod_'.$rs_row['subModID']]=0;
			$k++;
		}
		$modules_new[$i]["subModName"] = $row;
		$mainmodule_perStatus = $moduleObj->getAdminAssignedMainModules($admin_id,$rs_row['moduleId']);
		//print_r($mainmodule_perStatus);
		//echo "<br> ".count($row)." module". $rs['id'];
		if($mainmodule_perStatus) {
			if(count($mainmodule_perStatus)<=count($row))
			$modules_new[$i]['perStatus']=1;
		elseif(count($row)==0)
			$modules_new[$i]['perStatus']=1;
		}
		$i++;
	}
}
$smarty->assign('modules', $modules_new);
//echo "<pre>"; print_r($modules_new); echo"</pre>";

# Assign success or error msg to smarty variable and unset session variable
if(isset($_SESSION['msg']) && ($_SESSION['msg']) != '') {
	$smarty->assign('msg', $_SESSION['msg']);
	unset($_SESSION['msg']);
}


## Unset all the objects created which are on this page
unset($moduleObj);

## Include file which contains all the page names
require_once 'includeFile.php';
?>
