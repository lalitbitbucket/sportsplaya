<?php
## include required files
/*******************************/ 
require_once 'config/settings.php';
require_once '../model/users.php';
require_once '../model/email.php';
require_once '../includes/phpmailer/class.phpmailer.php';
/*******************************/
## Create Objects
/*******************************/
$userObj = new Model_Users();
$emailObj=new Model_Email();
$mail = new PHPMailer(true);
/*******************************/
if(isset($_POST['forgot_btn'])){
		## apply PHP validation for required fields whenever you submit the form in your project
		if(trim($_POST['email']) != '') {
			$user_res=$userObj->checkAdminUserEmailExists(return_post_value($_POST['email']));
				if(count($user_res) >0){
					if($user_res['userType']=='1'){
						// fetching user detail
						$userArray = $userObj->getUserDetailsByUserId($user_res['userId']);
						//echo"<pre>"; print_r($userArray); echo"</pre>"; echo"<br>";
						$userinfoArray=$userObj->getProfileDetailsByUserId($user_res['userId']);
						//echo"<pre>"; print_r($userinfoArray); echo"</pre>"; echo"<br>";
						$urlDtlArr = array();
						$urlDtlArr['uID'] = $userArray['userId'];
						$urlDtlArr['urlType'] = 'forgot';
						$urlDtlArr['reqDate'] = date('Y-m-d');
						$urlDtlArr['status'] = '0';
						$userObj->addUserUrlDetailsByValue($urlDtlArr);
						$link = '<a href="'.SITE_URL.'/admin/resetpassword.php?id='.md5($userArray['userId']).'">'.SITE_URL.'/admin/resetpassword.php?id='.md5($userArray['userId']).'</a>';
						$link1 = SITE_URL.'/admin/resetpassword.php?id='.md5($userArray['userId']);
						$name=$userinfoArray['fname'];
						$username= $userinfoArray['username'];
						$emailArray=$emailObj->getEmailById(1);
						$to=$userArray['email'];
						$subject=$emailArray['emailSub'];
						$subject=str_replace('[SITENAME]', SITENAME, $subject);
						$message=$emailArray['emailContent'];						
						$message=str_replace('[USEREMAIL]', $username, $message);						
						$message=str_replace('[NAME]', $name, $message);
						$message=str_replace('[ACTIVATIONURL]', $link, $message);
						$message=str_replace('[ACTIVATIONURL1]',$link1, $message);
						$message=str_replace('[SITENAME]',SITENAME , $message);
						$message=str_replace('[SITE_LINK]',SITE_URL , $message);						
						$from = $emailArray['fEmail'];
						$fromname = $emailArray['fName'];
			            		$message = str_replace('[SITENAME]',SITENAME , $message);
		                		$message = str_replace('[SITELINK]',SITE_URL , $message);
		                		$message = str_replace('[LOGO]','<img src="'.SITE_URL.'/siteAssets/images/logo.png"/>', $message);
		                		$message=str_replace('[MESSAGE]',$message , $message);
						$smtpArray = $emailObj->getActiveSMTPDetails();
        					//print_r($smtpArray); exit;
        	 				//echo $userProfileDetails['password']; exit;
		       				$auth  = $smtpArray['isAuth'];
               					// echo $auth;exit;
						if($auth=='y')
						{     $auth = 'true';  
					
						}
						else
						{     $auth = 'false';    
					
						}
						//echo $auth;
						//echo '<pre>'; print_r($smtpArray);exit;
						if($auth=='true')
						{  
						$mail->IsSMTP(); // telling the class to use SMTP
						$mail->Mailer = "smtp";
						$mail->SMTPAuth   = true;                  	// enable SMTP authentication
						$mail->SMTPSecure = $smtpArray['secure'];       // sets the prefix to the servier
						$mail->Host       = $smtpArray['smtpHost'];     // sets GMAIL as the SMTP server
						$mail->Port       = $smtpArray['smtpPort'];     // set the SMTP port for the GMAIL server
						$mail->Username   = $smtpArray['smtpUsername']; // GMAIL username
						$mail->Password   = $smtpArray['smtpPassword']; 
                       
						//$mail->Username   = 'test@gmail.com';  // GMAIL username
						// $mail->Password   = 'Jo923h1z8923'; 
						// $from = $smtpArray['smtpUsername'];			
						}
		              			# get email back up    
		              			$backemailArray = array();
		              			$backemailArray['sendto'] = $to;
		             			$backemailArray['subject'] = $subject;
		              			$backemailArray['content'] = $message;
		              			$backemailArray['date'] = date("Y-m-d H:i:s");
		             			// print_r($backemailArray);exit;
			       			/*echo $subject."<br />";
						echo $from."<br />";
					    	echo $fromname."<br />";
						echo $to."<br />";
						echo $toname."<br />";
						echo "<pre>"; print_r($message);  exit();*/
		              		try {
						  $mail->AddAddress($to, 'Admin');
						  $mail->SetFrom($from, $fromname);
						  $mail->Subject = $subject;
						  $mail->Body = $message; 
					      	  $mail->Send(); 
		              		      	  $backemailArray['status'] ='2';   
						  $_SESSION['msg'] = '<div class="success_msg"><span>Reset password link is sent successfully.</span></div>';	
					} 
				catch (phpmailerException $e) {
				   $_SESSION['msg']= "<div class='warning'>".$e->errorMessage()."</div>"; //Pretty error messages from PHPMailer
                    		   $backemailArray['status'] ='1';
				} catch (Exception $e) {
				  $_SESSION['msg']=  "<div class='warning'>".$e->getMessage()."</div>"; //Boring error messages from anything else!
                   		  $backemailArray['status'] ='1';
				}				
				################################### Mail Template End   ############################
                 		$emailObj->getEmailBackup($backemailArray); 
				} else {
						$_SESSION['msg'] = '<div class="error_msg"><span>You are not an admin user.</span></div>';	
					}
				} else {// email isexit else
					$_SESSION['msg'] = '<div class="error_msg"><span>You are not valid user.</span></div>';
				}
			} else { // username else
				$_SESSION['msg'] = '<div class="error_msg"><span>Please enter your email.</span></div>';
		}
// main if closed	
}

## Assign success or error msg to smarty variable and unset session variable
if(trim($_SESSION['msg']) != '') {
	$smarty->assign('msg', $_SESSION['msg']);
	unset($_SESSION['msg']);
}

## Unset all the objects created which are on this page
unset($userObj);
unset($emailObj);

$smarty->display(TEMPLATEDIR_ADMIN. 'forgotpassword.tpl');
unset($smarty);
?>
