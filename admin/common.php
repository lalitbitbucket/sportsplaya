<?php
require(DOC_ROOT.'/model/users.php');
$userObj = new Model_Users();
# ----------------login details-----------------------------
$loginDetails =$userObj->getLoginTimeByLoginId($_SESSION['loginId']);
$end_date = date("Y-m-d H:i:s");
$start_date = $loginDetails['loginTime'];
$loginDetails['loginFrom'] = calculateDateTimeDifference($start_date, $end_date);
$smarty->assign("loginDetails", $loginDetails);
#----------------------login details ---------------------------
#-----------------User Detials ------------------
$userDetails =$userObj->getDetailsByUserId($_SESSION['adminSporttagId']);
$smarty->assign("userDetails", $userDetails);
#-----------------User Detials ------------------
#----------------- Get Online Users ------------------
$getOnlineUsers = $userObj->getAllOnlineUsersInSite();
$smarty->assign("getOnlineUsers", $getOnlineUsers);
$totalOnlineUsers = count($getOnlineUsers);
$smarty->assign("totalOnlineUsers",$totalOnlineUsers);
#----------------------------------
$smarty->assign('loginPopup', "{width:600, height:300}");
?>
