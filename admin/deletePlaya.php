<?php 
## include required files
/*******************************/
require_once 'config/settings.php';
require_once '../model/videos.php';
/*******************************/

## Create Objects
/*******************************/
$videosObj = new Model_videos();
/*******************************/

## Get Videos Id
/*******************************/
$video_id = base64_decode($_GET['vId']);
/*******************************/

## function for delete playa video  -
/*******************************/
$deleted = $videosObj->removePlayaVideosByAdmin($video_id);

header('location:'.SITE_URL.'/admin/home.php?q=playaoftheday');
exit;
?>