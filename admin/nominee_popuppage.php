<?php

require_once 'config/settings.php';

require_once '../model/videos.php';
$videoObj = new Model_videos();

$page_body_class = 'home_page';
$smarty->assign ('page_body_class', $page_body_class);

if($_SESSION['stagUserId']!=''){
    $Logged_userId  =   $_SESSION['stagUserProfileId'];
    $userProfileId = $_SESSION['stagUserId'];
	$checkLogin =1;
}
else{
	$checkLogin =0;
}

$uID = $_REQUEST['uId'];
$total_views = $videoObj->countTotalClickOnVideoByVideoId($uID);
$videoArray = $videoObj->getVideoByVideoId($uID);
$getComments = $videoObj->getCommentsByNomineesId(1, $uID);
$votingDtl = $videoObj->getVotingCountByUserId($userProfileId,$uID);
$getAlbumDtl = $videoObj->getAlbumDetailsByAlbumId($videoArray['albumId']);

$total_voting = $videoObj->getVotingPercentByVideoId($uID);
    if($videoArray['video_type']=='vimeo')
    {
        $videoArray['vcode'] = str_replace("\\","",$videoArray['vcode']);
        $video_url = explode('"', $videoArray['vcode']);
        $videoArray['video_url'] = $video_url[1]."?autoplay=1"; 
    }
    if($videoArray['video_type']=='youtube')
    {
        $videoArray['vcode'] = str_replace("\\","",$videoArray['vcode']);
        preg_match('/src="([^"]+)"/', $videoArray['vcode'], $match);
        $url = $match[1];
        $arr = explode("/",$url);
        $url= $arr[count($arr)-1];
        $videoArray['video_url'] ="https://www.youtube.com/embed/".$url."?autoplay=1";
    }
    
    $data =0;
	if($videoArray['video_type']=='youtubeurl'){
        $data =1;
        $url = $videoArray['vccode'];
        $videoArray['vccode'] = str_replace("\\","",$videoArray['vccode']);
        $regex_pattern = "/(youtube.com|youtu.be)\/(watch)?(\?v=)?(\S+)?/";
        //$match;
        if(preg_match($regex_pattern, $url, $match)){
            $videoArray['video_url'] = 'https://www.youtube.com/embed/'.$match[4].'?autoplay=1';
        } 
	   	/*$videoArray['vccode'] = str_replace("\\","",$videoArray['vccode']);
	   	$arr = explode("=",$videoArray['vccode']);
	 	$url= $arr[count($arr)-1];
		$videoArray['video_url'] = "https://www.youtube.com/embed/".trim($url)."?autoplay=1";*/	
	}
    
    if($videoArray['video_type']=='video'){
        $videoArray['video_url'] = SITE_URL.'/dynamicAssets/videos/'.$videoArray['video'].'?autoplay=1';
    }
    $album  = trim(strtolower(str_replace(" ","_",$getAlbumDtl['title'])));
?>
<div class="row-fluid">
    <div class="span4">
        <div class="span8">
            <iframe width="100%" height="80%" src="<?php echo $videoArray['video_url'];?>" frameborder="0"    class="popup_video" allowfullscreen></iframe>
        </div>
    </div>        
</div>