<?php 
			if(isset($_GET['q'])) {
        		switch($_GET['q']) {
			## Dashboard pages
			case 'dashboard':
				$fileInclude = 'controller/dashboard/dashboard.php';
			break;			
			case 'userDetails':
				$fileInclude = 'controller/dashboard/userDetails.php';
			break;
			case 'sitesettings':
				$fileInclude = 'controller/sitesettings/sitesettings.php';
			break;
			case 'editSiteSettings':
				$fileInclude = 'controller/sitesettings/editSiteSettings.php';
			break;
			
			case 'smtpsettings':
				$fileInclude = 'controller/sitesettings/smtpsettings.php';
			break;
			
			case 'editSMTPSettings':
				$fileInclude = 'controller/sitesettings/editSMTPSettings.php';
			break;

			case 'resendEmail':
                		$fileInclude = 'controller/sitesettings/resendEmail.php';
           		break;
			
			case 'socialsettings':
				$fileInclude = 'controller/sitesettings/socialsettings.php';
			break;
			
			case 'editSocialSettings':
			$fileInclude = 'controller/sitesettings/editSocialSettings.php';
			break;
					
			case 'editProfile':
				$fileInclude = 'controller/adminUsers/superAdmin/editProfile.php';
			break;
		
			case 'changeUserName':
				$fileInclude = 'controller/adminUsers/superAdmin/changeUserName.php';
			break;
		
			case 'checkUserNameExist':
			 $fileInclude   = 'controller/adminUsers/superAdmin/checkUserNameExist.php';
			break;
		
			case 'changePassword':
			 $fileInclude   = 'controller/adminUsers/superAdmin/changePassword.php';
			break;
		
			case 'changeEmail':
			 $fileInclude   = 'controller/adminUsers/superAdmin/changeEmail.php';
			break;
		
			case 'checkEmailExist':
			  $fileInclude   = 'controller/adminUsers/superAdmin/checkEmailExist.php';
			break;			 			 
			
			case 'checkUsernameExist':
			      $fileInclude = 'controller/adminUsers/superAdmin/checkUserNameExist.php';
			break;
			case 'checkEmailExist':
			      $fileInclude = '/controller/adminUsers/superAdmin/checkEmailExist.php';
			break;
			case 'stateList':
			      $fileInclude = 'controller/seller/stateList.php';
			break;
			
			case 'emailTemplates':
			  $fileInclude   = 'controller/emailTemplates/emailTemplates.php';
			break;			 
			case 'editEmailTemaplte':
			  $fileInclude   = 'controller/emailTemplates/editEmailTemaplte.php';
			break;		 
			case 'emailDetails':
			  $fileInclude   = 'controller/emailTemplates/emailDetails.php';
			break;

			# CMS content
			
			case 'cms':
			 $fileInclude   = 'controller/cms/cms.php';
			break;			 
			case 'addcms':
			  $fileInclude   = 'controller/cms/addcms.php';
			break;			 
			case 'editcms':
			  $fileInclude   = 'controller/cms/editcms.php';
			break;			 
			case 'cmsdetails':
			  $fileInclude   = 'controller/cms/cmsdetails.php';
			break;

			## Country
			 
			case'country':
			     $fileInclude = 'controller/country/countryList.php';
			break;	 
			
			case 'addCountry':
			     $fileInclude = 'controller/country/addCountry.php';
			break;	 
			
			case 'editCountry':
			     $fileInclude = 'controller/country/editCountry.php';
			break;
			
			case 'checkcountryexist':
			     $fileInclude = 'controller/country/checkcountryexist.php';
			break;
						
			##state
			case 'state':
			      $fileInclude = 'controller/state/stateList.php';
			break;
			case 'addState':
			      $fileInclude = 'controller/state/addState.php';
			break;
			case 'editState':
			      $fileInclude = 'controller/state/editState.php';
			break;
			case 'deleteState':
			     $fileInclude = 'controller/state/deleteState.php';
			break;
			
			case 'checkstateexist':
			     $fileInclude = 'controller/state/checkstateexist.php';
			break;
			
		        ##City
			case 'city':
			      $fileInclude = 'controller/city/cityList.php';
			break;
			case 'addCity':
			      $fileInclude = 'controller/city/addCity.php';
			break;
			case 'editCity':
			      $fileInclude = 'controller/city/editCity.php';
			break;
			case 'deleteCity':
			     $fileInclude = 'controller/city/deleteCity.php';
			break;
			
			case 'checkExistingCity':
			     $fileInclude = 'controller/city/checkExistingCity.php';
			break;

			case 'checkExistState':
			      $fileInclude = 'controller/city/state.php';
			break;

			## Manage Features
			case 'managefeatures':
				$fileInclude = 'controller/features/managefeatures.php';
			break;
			
			case 'addfeatures':
				$fileInclude = 'controller/features/addfeatures.php';
			break;
			
			case 'updatefeatures':
				$fileInclude = 'controller/features/updatefeatures.php';
			break;
			
			case 'checkfeaturesexist':
				$fileInclude = 'controller/features/checkfeaturesexist.php';
			break;
            
			## Manage journal
			case 'managejournal':
				$fileInclude = 'controller/journal/managejournal.php';
			break;
			
			case 'addjournal':
				$fileInclude = 'controller/journal/addjournal.php';
			break;
			
			case 'updatejournal':
				$fileInclude = 'controller/journal/updatejournal.php';
			break;
			
			case 'journalDetails':
				$fileInclude = 'controller/journal/journalDetails.php';
			break;

			## Manage Plan
			case 'manageplan':
				$fileInclude = 'controller/plan/manageplan.php';
			break;
			
			case 'addplan':
				$fileInclude = 'controller/plan/addplan.php';
			break;
			
			case 'updateplan':
				$fileInclude = 'controller/plan/updateplan.php';
			break;
			
			case 'checkplanexist':
				$fileInclude = 'controller/plan/checkplanexist.php';
			break;			
			
			#sport person
			## Manage sport person
			case 'sportperson':	
				 $fileInclude = 'controller/sportperson/managesportperson.php';
		        break;
			case 'addsportperson':
			     $fileInclude = 'controller/sportperson/addsportperson.php';
			break;
			case 'editsportperson':
			      $fileInclude = 'controller/sportperson/editsportperson.php';
			break;
			case 'viewsportperson':
			      $fileInclude = 'controller/sportperson/viewsportperson.php';
			break;
		        case 'loginHistory':
			      $fileInclude = 'controller/sportperson/loginHistory.php';
			break;
		        case 'tagUser':
			      $fileInclude = 'controller/sportperson/tagUser.php';
			break;
			
			case 'sportUserPost':
			      $fileInclude = 'controller/sportperson/sportUserPost.php';
			break;
	
			case 'postChart':
			      $fileInclude = 'controller/sportperson/postChart.php';
			break;
			case 'sportUserVideoAlbum':
			      $fileInclude = 'controller/sportperson/sportUserVideoAlbum.php';
			break;
			case 'userAlbumVideos':
			      $fileInclude = 'controller/sportperson/userAlbumVideos.php';
			break;
			case 'userAlbumVideoLikes':
			      $fileInclude = 'controller/sportperson/userAlbumVideoLikes.php';
			break;
			case 'userAlbumVideoComment':
			      $fileInclude = 'controller/sportperson/userAlbumVideoComment.php';
			break;
			case 'sportUserPhotoAlbum':
			      $fileInclude = 'controller/sportperson/sportUserPhotoAlbum.php';
			break;
			case 'userAlbumPhotos':
			      $fileInclude = 'controller/sportperson/userAlbumPhotos.php';
			break;
			case 'userAlbumPhotoLikes':
			      $fileInclude = 'controller/sportperson/userAlbumPhotoLikes.php';
			break;
			case 'userAlbumPhotoComment':
			      $fileInclude = 'controller/sportperson/userAlbumPhotoComment.php';
			break;

                       #coach
			## Manage coach
			case 'coach':	
				 $fileInclude = 'controller/coach/managecoach.php';
		        break;
			case 'addcoach':
			     $fileInclude = 'controller/coach/addcoach.php';
			break;
			case 'editcoach':

			      $fileInclude = 'controller/coach/editcoach.php';
			break;
			case 'viewcoach':
			      $fileInclude = 'controller/coach/viewcoach.php';
			break;
		        case 'loginHistory':
			      $fileInclude = 'controller/coach/loginHistory.php';
			break;      
			case 'coachUserPost':
			      $fileInclude = 'controller/coach/coachUserPost.php';
			break;  
            case 'coachUserVideoAlbum':
			      $fileInclude = 'controller/coach/coachUserVideoAlbum.php';
			break;
			case 'coachuserAlbumVideos':
			      $fileInclude = 'controller/coach/userAlbumVideos.php';
			break;
			case 'coachuserAlbumVideoLikes':
			      $fileInclude = 'controller/coach/userAlbumVideoLikes.php';
			break;
			case 'coachuserAlbumVideoComment':
			      $fileInclude = 'controller/coach/userAlbumVideoComment.php';
			break;	
            case 'coachUserPhotoAlbum':
			      $fileInclude = 'controller/coach/coachUserPhotoAlbum.php';
			break;
			case 'coachuserAlbumPhotos':
			      $fileInclude = 'controller/coach/userAlbumPhotos.php';
			break;
			case 'coachuserAlbumPhotoLikes':
			      $fileInclude = 'controller/coach/userAlbumPhotoLikes.php';
			break;
			case 'coachuserAlbumPhotoComment':
			      $fileInclude = 'controller/coach/userAlbumPhotoComment.php';
			break;			
                 
             #fans
			## Manage fans
			case 'fans':	
				 $fileInclude = 'controller/fans/managefans.php';
		        break;
			case 'addfans':
			     $fileInclude = 'controller/fans/addfans.php';
			break;
			case 'editfans':

			      $fileInclude = 'controller/fans/editfans.php';
			break;
			case 'viewfans':
			      $fileInclude = 'controller/fans/viewfans.php';
			break;
		        case 'loginHistory':
			      $fileInclude = 'controller/fans/loginHistory.php';
			break;
			  case 'fanUserPost':
			      $fileInclude = 'controller/fans/fanUserPost.php';
			break;   

            case 'fansUserVideoAlbum':
			      $fileInclude = 'controller/fans/fansUserVideoAlbum.php';
			break;
			case 'fansuserAlbumVideos':
			      $fileInclude = 'controller/fans/userAlbumVideos.php';
			break;
			case 'fansuserAlbumVideoLikes':
			      $fileInclude = 'controller/fans/userAlbumVideoLikes.php';
			break;
			case 'fansuserAlbumVideoComment':
			      $fileInclude = 'controller/fans/userAlbumVideoComment.php';
			break;
             case 'fansUserPhotoAlbum':
			      $fileInclude = 'controller/fans/fansUserPhotoAlbum.php';
			break;
			case 'fansuserAlbumPhotos':
			      $fileInclude = 'controller/fans/userAlbumPhotos.php';
			break;
			case 'fansuserAlbumPhotoLikes':
			      $fileInclude = 'controller/fans/userAlbumPhotoLikes.php';
			break;
			case 'fansuserAlbumPhotoComment':
			      $fileInclude = 'controller/fans/userAlbumPhotoComment.php';
			break;					

			#Clubs
			## Manage Clubs
			case 'clubs':	
				 $fileInclude = 'controller/clubs/manageclubs.php';
		        break;
			case 'addclubs':
			     $fileInclude = 'controller/clubs/addclubs.php';
			break;
			case 'editclubs':

			      $fileInclude = 'controller/clubs/editclubs.php';
			break;
			case 'viewclubs':
			      $fileInclude = 'controller/clubs/viewclubs.php';
			break;
		        case 'loginHistory':
			      $fileInclude = 'controller/clubs/loginHistory.php';
			break;  
			  case 'clubUserPost':
			      $fileInclude = 'controller/clubs/clubUserPost.php';
			break; 
			 case 'clubsUserVideoAlbum':
			      $fileInclude = 'controller/clubs/clubsUserVideoAlbum.php';
			break;
			case 'clubsuserAlbumVideos':
			      $fileInclude = 'controller/clubs/userAlbumVideos.php';
			break;
			case 'clubsuserAlbumVideoLikes':
			      $fileInclude = 'controller/clubs/userAlbumVideoLikes.php';
			break;
			case 'clubsuserAlbumVideoComment':
			      $fileInclude = 'controller/clubs/userAlbumVideoComment.php';
			break;	
			break;
             case 'clubsUserPhotoAlbum':
			      $fileInclude = 'controller/clubs/clubsUserPhotoAlbum.php';
			break;
			case 'clubsuserAlbumPhotos':
			      $fileInclude = 'controller/clubs/userAlbumPhotos.php';
			break;
			case 'clubsuserAlbumPhotoLikes':
			      $fileInclude = 'controller/clubs/userAlbumPhotoLikes.php';
			break;
			case 'clubsuserAlbumPhotoComment':
			      $fileInclude = 'controller/clubs/userAlbumPhotoComment.php';
			break;	

			## Manage Online Users
			case 'onlineusers':	
				 $fileInclude = 'controller/onlineusers/manageonlineusers.php';
		        break;
			
                        ## Blogs
			case 'blogs':
				$fileInclude = 'controller/blogs/blogList.php';
			break;
			
			case 'addBlogs':
				$fileInclude = 'controller/blogs/addBlogs.php';
			break;
			
			case 'editBlogs':
				$fileInclude = 'controller/blogs/editBlogs.php';
			break;
			
			case 'blogDetails':
				$fileInclude = 'controller/blogs/blogDetails.php';
			break;
		
			 case 'commentDetails':
			 $fileInclude   = 'controller/blogs/commentDetails.php';
			 break;
			
			## Manage Brands
			case 'managebrand':
				$fileInclude = 'controller/brand/managebrand.php';
			break;
			
			case 'addnewbrand':
				$fileInclude = 'controller/brand/addnewbrand.php';
			break;
			
			case 'updatebrand':
				$fileInclude = 'controller/brand/updatebrand.php';	
			break;
			
			case 'branddetails':
				$fileInclude = 'controller/brand/branddetails.php';	
			break;
			case 'checkbrandexist':
			    $fileInclude = 'controller/brand/checkbrandexist.php';
			break;
		
			## Manage Enquiry
			 case 'enquiry':
			 $fileInclude   = 'controller/enquiry/enquiry.php';
			 break;
			 
			   case 'EnquiryDetails':
			  $fileInclude   = 'controller/enquiry/EnquiryDetails.php';
			 break;
			 
			  case 'reply':
			 $fileInclude   = 'controller/enquiry/reply.php';
			 break;
			 
			  case 'replyDetails':
			 $fileInclude   = 'controller/enquiry/replyDetails.php';
			 break;
			
			// start of event menu added
			 
			case 'events':
			  $fileInclude   = 'controller/events/events.php';
			 break;	
			 case 'addEvents':
			  $fileInclude   = 'controller/events/addEvents.php';
			 break;	
			 
			case 'editEvents':
			  $fileInclude   = 'controller/events/editEvents.php';
			 break;	 
			 
			case 'eventsDetails':
			$fileInclude   = 'controller/events/eventsDetails.php';
			break;
			
			case 'comments':
			$fileInclude   = 'controller/events/comments.php';
			break;
			case 'subcomments':
			$fileInclude   = 'controller/events/subcomments.php';
			break;
			case 'commentslike':
			$fileInclude   = 'controller/events/commentslike.php';
			break;
			case 'postEventChart':
			$fileInclude   = 'controller/events/postEventChart.php';
			break;
			
			// end of event menu added 
			##FAQ Category
			 case 'faqCategories':
			 $fileInclude   = 'controller/faqCategories/faqCategories.php';
			 break;			 
			  case 'addfaqCategory':
			  $fileInclude   = 'controller/faqCategories/addfaqCategory.php';
			 break;			 
			  case 'editfaqCategory':
			  $fileInclude   = 'controller/faqCategories/editfaqCategory.php';
			 break;			 
			
			
			 ##FAQ
			 case 'FAQ':
			 $fileInclude   = 'controller/faq/FAQ.php';
			 break;	
			 		 
			 case 'addFAQ':
			  $fileInclude   = 'controller/faq/addFAQ.php';
			 break;
			 
			 case 'faqcheckfaqexist':
			  $fileInclude   = 'controller/faqCategories/checkFAQexist.php';
			 break;
			 
			 case 'editFAQ':
			 $fileInclude   = 'controller/faq/editFAQ.php';
			 break;
			  
			 case 'FAQDetails':
			  $fileInclude   = 'controller/faq/FAQDetails.php';
			 break;
			
			## Manage Group			 
			case'group':
			     $fileInclude = 'controller/group/groupList.php';
			break;	 
			
			case 'addGroup':
			     $fileInclude = 'controller/group/addGroup.php';
			break;	 
			
			case 'editGroup':
			     $fileInclude = 'controller/group/editGroup.php';
			break;
			
			case 'checkgroupnameexist':
			     $fileInclude = 'controller/group/checkgroupnameexist.php';
			break;		
		
			case 'groupDetails':
				$fileInclude = 'controller/group/groupDetails.php';	
			break;
			case 'groupLike':
				$fileInclude = 'controller/group/groupLike.php';	
			break;
			case 'groupMember':
				$fileInclude = 'controller/group/groupMember.php';	
			break;
			case 'groupComment':
				$fileInclude = 'controller/group/groupComment.php';	
			break;

			case 'groupChart':
				$fileInclude = 'controller/group/groupChart.php';	
			break;

			## Manage News
			case'news':
			     $fileInclude = 'controller/news/news.php';
			break;	 
			
			case 'add':
			     $fileInclude = 'controller/news/add.php';
			break;	 
			
			case 'edit':
			     $fileInclude = 'controller/news/edit.php';
			break;
			
			case 'checknewsnameexist':
			     $fileInclude = 'controller/news/checknewsnameexist.php';
			break;		
		
			case 'details':
				$fileInclude = 'controller/news/details.php';	
			break;
			case 'newsHitsList':
				$fileInclude = 'controller/news/newsHitsList.php';	
			break;
			
			## Ads
			case 'manageads':
				$fileInclude = 'controller/ads/manageads.php';
			break;
			
			case 'addnewads':
				$fileInclude = 'controller/ads/addnewads.php';
			break;
			
			case 'updateads':
				$fileInclude = 'controller/ads/updateads.php';
			break;
			
			case 'adsdetails':
				$fileInclude = 'controller/ads/adsdetails.php';
			break;	
			
			case 'adstatistics':
				$fileInclude = 'controller/ads/adstatistics.php';    
			break;
			
			case 'ajaxstatistics':
				$fileInclude = 'controller/ads/ajaxstatistics.php';    
			break;  

			## Manage Sports
			case 'managesports':
				$fileInclude = 'controller/sports/managesports.php';
			break;
			
			case 'addnewsports':
				$fileInclude = 'controller/sports/addnewsports.php';
			break;
			
			case 'updatesports':
				$fileInclude = 'controller/sports/updatesports.php';
			break;
			
			case 'sportsdetails':
				$fileInclude = 'controller/sports/sportsdetails.php';
			break;	
				
			## Manage Cover Image
			case 'coverimage':
				$fileInclude = 'controller/coverimage/coverimage.php';
			break;
			
			case 'addnewcoverimage':
				$fileInclude = 'controller/coverimage/addnewcoverimage.php';
			break;
			
			case 'updatecoverimage':
				$fileInclude = 'controller/coverimage/updatecoverimage.php';
			break;
			
			case 'coverimagedetails':
				$fileInclude = 'controller/coverimage/coverimagedetails.php';
			break;

                        ## Manage album			 
			case'album':
			     $fileInclude = 'controller/album/albumList.php';
			break;	 
			case'photolist':
			     $fileInclude = 'controller/album/photolist.php';
			break;
			
                        ## Manage Video			 
			case'videolist':
			     $fileInclude = 'controller/video/videolist.php';
			break;	 
			case'addVideo':
			     $fileInclude = 'controller/video/add.php';
			break;
			case'editVideo':
			     $fileInclude = 'controller/video/edit.php';
			break;
			 case'detailsVideo':
			     $fileInclude = 'controller/video/details.php';
			break;
			 
                        ## Emailbackup			 
			case'emailbackup':
			     $fileInclude = 'controller/emailbackup/emailbackup.php';
			break;	

		         ## Products 
			case 'manageproducts':
				$fileInclude = 'controller/products/manageproducts.php';
			break;
			
			case 'addnewproduct':
				$fileInclude = 'controller/products/addnewproduct.php';
			break;
		
		        case 'productdetails':
				$fileInclude = 'controller/products/productdetails.php';
			break;
			case 'checkproductexist':
			    $fileInclude = 'controller/products/checkproductexist.php';
			break;
		        
			case 'editProduct':
				$fileInclude = 'controller/products/editproduct.php';
			break;
		
			##Manage Seo Settings
			case 'seosettings':
			$fileInclude = 'controller/seo/seo.php';
			break;	

			case 'editseo':
			$fileInclude = 'controller/seo/editseo.php';	
			break;
			
			## Manage Sub Users 
			case 'managesubusers' :
			 $fileInclude = 'controller/adminUsers/subuser/manageusers.php';
			break;
			
			case 'addsubuser' :
			 $fileInclude = 'controller/adminUsers/subuser/addsubuser.php';
			break;
			
			case 'updatesubuser' :
			 $fileInclude = 'controller/adminUsers/subuser/updatesubuser.php';
			break;
			
			case 'subuserdetails' :
			 $fileInclude = 'controller/adminUsers/subuser/subuserdetails.php';
			break;
			
			case 'subuserprevilages':
			 $fileInclude = 'controller/adminUsers/subuser/subuserprevilages.php';
			 break;
			
			case 'checkSiteUserNameExist':
			 $fileInclude = 'controller/adminUsers/subuser/checkSiteUserNameExist.php';
			 break;
			
			case 'checkAdminEmailExist':
			 $fileInclude = 'controller/adminUsers/subuser/checkSiteUserEmailExist.php';
			 break;

			 ## Organizer			 
			case'manageOrganizer':
			     $fileInclude = 'controller/organizer/manageOrganizer.php';
			break;	 
			
			case 'addOrganizer':
			     $fileInclude = 'controller/organizer/addOrganizer.php';
			break;	 
			
			case 'editOrganizer':
			     $fileInclude = 'controller/organizer/editOrganizer.php';
			break;
			
			case 'checkOrganizerExist':
			     $fileInclude = 'controller/organizer/checkOrganizerExist.php';
			break;

			 ## league			 
			case'leagueList':
			     $fileInclude = 'controller/league/leagueList.php';
			break;	 
			
			case 'addLeague':
			     $fileInclude = 'controller/league/addLeague.php';
			break;	 
			
			case 'editLeague':
			     $fileInclude = 'controller/league/editLeague.php';
			break;
			
			case 'checkLeagueNameExist':
			     $fileInclude = 'controller/league/checkLeagueNameExist.php';
			break;

			case 'leagueDetails':
			     $fileInclude = 'controller/league/leagueDetails.php';
			break;

			case 'db':
			$fileInclude = 'controller/sitesettings/db.php';
			break;

			case 'payments':
			$fileInclude   = 'controller/payments/payments.php';
			break;
			 
			case 'editPayments':
			$fileInclude   = 'controller/payments/editPayments.php';
			break;
			
			case 'playaoftheday':
			$fileInclude   = 'controller/video/playaoftheday.php';
			break;

			case 'addplayaoftheweek':
			$fileInclude   = 'controller/video/addplayaoftheweek.php';
			break;

			default:
			 $fileInclude = 'controller/dashboard/dashboard.php';
		}
	} else {
		$fileInclude = 'controller/dashboard/dashboard.php';
	}
	require_once 'common.php';
	require_once '../admin/'.$fileInclude;
?>
