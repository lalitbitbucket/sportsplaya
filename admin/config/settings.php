<?php
require_once  '../config/configuration.php';
include_once(DOC_ROOT.'/model/common/classes/Database.php');
	@session_start();
	ob_start();
	ini_set("dispaly_errors", "1");
	ini_set('error_reporting', E_ALL ^ E_NOTICE ^ E_WARNING );
//error_reporting(0);

	## Set time zone
	date_default_timezone_set('Asia/Calcutta');


	/*********** Creating Object of Smarty ***********/
	require(DOC_ROOT.'/model/common/libs/SmartyBC.class.php');
	$smarty = new SmartyBC;
	$smarty->force_compile = true;
	//$smarty->debugging = true;
	$smarty->caching = true;
	$smarty->cache_lifetime = 120;
	$smarty->setTemplateDir(DOC_ROOT.'/templates');
    $smarty->setCompileDir(DOC_ROOT.'/templates_c');
    $smarty->setCacheDir(DOC_ROOT.'/cache');
    $smarty->setConfigDir(DOC_ROOT.'/config');  
	/*********** End of Creating Object of Smarty ***********/
	
	## Below file contains all the tables of the project
	include_once(DOC_ROOT.'/config/tbl-names.php');
	//echo DOC_ROOT.'/model/common/common.php' ;exit;	
	//echo DOC_ROOT ;exit;
# get site variables and assign to smarty variables
require(DOC_ROOT.'/model/common/common.php');
//echo DOC_ROOT.'/model/common/common.php' ;exit;	
require(DOC_ROOT.'/model/common/functions.php');	

$commonObj = new Model_Common();
$allVarraibles =$commonObj->getAllVariables();
$smarty->assign("allVarraibles", $allVarraibles);

$k=0;
while($k<count($allVarraibles))
{
		## Site Name
		define($allVarraibles[$k]['name'], $allVarraibles[$k]['value']);
		$smarty->assign($allVarraibles[$k]['name'], $allVarraibles[$k]['value']);
	
$k++;
}

## Assign site url to smarty variable
$smarty->assign("SITEURL", SITE_URL);

## Assign site url to smarty variable
$smarty->assign("siteroot", SITE_URL.'/admin');
	

	
	

/********************************************/	


	## Assign header, footer and left menu files to smarty variables

		/****** Admin side  ***/
		$smarty->assign("header_start", TEMPLATEDIR_ADMIN .'commonFiles/admin_header1.tpl');
		$smarty->assign("header_end", TEMPLATEDIR_ADMIN .'commonFiles/admin_header2.tpl');
		$smarty->assign("footer", TEMPLATEDIR_ADMIN .'commonFiles/footer.tpl');
		$smarty->assign("left_menu", TEMPLATEDIR_ADMIN . "commonFiles/left_menu.tpl");

?>