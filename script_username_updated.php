<?php
## Include Required Files
/*****************************/
require_once 'config/settings.php';

## Get All Users
/*****************************/
$userdata = $usersObj->getAllUsersRecord();
foreach ($userdata as $value) {
	$userProfileId = $value['id'];
	$userId = $value['userId'];
	$fname = preg_replace('/[^A-Za-z0-9\-]/', '',$value['fname']);
	$lname = preg_replace('/[^A-Za-z0-9\-]/', '',$value['lname']);
	
	$username=trim(strtolower(str_replace(" ","",$fname))). '_' .trim(strtolower(str_replace(" ","",$lname)));
	
	/* Check If Already Exists UserName */
	$usernameexist = $usersObj->checkUserNameExists($username,$userProfileId);
    
    if (count ($usernameexist) < 1) { 
    	$userDetail['username'] = $username;
    	$usersObj->editUserProfileValueById($userDetail,$userProfileId);
    	$usersObj->editUserNameByUserId($userDetail,$userId);
	}   
}
echo'<div style="">Successfull Updated<br>All Username Replaced by underscore ( _ ).</div>';
?>