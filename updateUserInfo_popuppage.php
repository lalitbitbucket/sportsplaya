<?php
## include required files
/*******************************/
require_once 'config/settings.php';

$siteurl = SITE_URL.'/';

$countryObj = new Model_Country();
$clubObj = new Model_Club();
$citiObj   = new Model_City();

isLogin();

if(isset($_POST['submit']) && $_POST['submit']=='Update'){
	
    $data = array();
	$data['user_contact'] = $_POST['user_contact'];
	$data['country'] = $_POST['country'];
    $data['birthdate'] = $_POST['birthdate']; 

    if($_POST['city'] !=''){
        $data['city'] = $_POST['city']; 
    }
    else{
        $data['city'] = ''; 
    }
    
    $data['sports_position'] = $_POST['position']; 
    $data['sports_club'] = $_POST['sports_club']; 
    $data['sportsID'] = $_POST['sportsID']; 

    $result = $usersObj->editProfileInfoByUserId($data, $userInfoById['id']);
	$countBioArray = $usersObj->checkBioCountExists($userInfoById['id']);
        if($_POST['user_height']!='')
        {
            $bioArray = array();
            $bioArray['userId']         =  $userInfoById['id'];
            $bioArray['userType']       =  $userInfoById['userType'];
            $bioArray['height']         =  return_post_value($_POST['user_height']); 
            $bioArray['modifieddate']   =  date("Y-m-d H:i:s"); 
            if($countBioArray<='0')
            {
                $usersObj->addBioInfo($bioArray);
            }
            else{
            $usersObj->editBioDetailsByUserIdAnduserType($bioArray,$userInfoById['id'],$otheruserDetails['userType']);
            }
        }    
    if($result > 0){
		echo '<script>top.window.location.href="'.SITE_URL.'/users/viewprofile"</script>';
    	exit;
	}
}


if($userInfoById['id']!= ''){
	$userDetails = $usersObj->UserProfileDetailByuserID($userInfoById['id']);	
    $userBioInfo = $usersObj->getBioDetailsByUserId($userInfoById['id']);

    if($userDetails['country']  > 0 ){
        $getCity = $citiObj->getAllActiveCityByCountryId($userDetails['country']);
    }
}

$clubData  = $clubObj->getAllActiveClubs();
$sports  = $sportsObj->getAllActiveSports();
$countryArray = $countryObj->getAllActiveCountry();


?>
<link rel="stylesheet" type="text/css" href="/siteAssets/datepicker/css/paper-grinder.css">
<link rel="stylesheet" type="text/css" href="/siteAssets/responsive/css/bootstrap.css" />
<link rel="stylesheet" type="text/css" href="/siteAssets/css/styles.css" />
<link rel="stylesheet" type="text/css" href="/siteAssets/css/fonts.css" />
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="/siteAssets/css/chosen.css">
<style>
	@media (min-width: 1024px){
		html {
			font-size: 18px !important;
		}
	}
	.white-popup-block{background:#fff url(siteAssets/images/newimages/bg_register.png) center top no-repeat;	
	background-size:100%;}
    .col-xs-5{margin-bottom: 2%;}
	#custom-content img {max-width: 100%;margin-bottom: 10px;}
	@media screen and (min-width: 768px) and (max-width: 979px){
		.white-popup-block{background-size:130% !important;}
	}

</style>


<div class="white-popup-block">
    <div id="custom-content" class="p-t27" style="max-width:100%; padding:3rem 3rem 1rem;">
       	<div class="text-center">
            <?php if($msg!=''){echo '<p class="alert bg-transparent p-14 text-brickRed">'.$msg.'</p>';}?>
        </div>
       <div><h3>Edit Profile</h3></div>
       <form method="post" name="frmUpdate">
       <div class="row">
       	<div class="col-md-5">
        <div class="row">
                <div class="col-md-6 m-b18"> 
                    <input type="text" value="<?php echo $userDetails['email']; ?>" readonly='readonly' class="p-18 p-tb9 text-dgrey col-xs-12" placeholder="emial">
                </div>
                
                <div class="col-md-6 m-b18">
                    <input type="text" name="user_contact" id="user_contact" class="p-18 p-tb9 text-dgrey col-xs-12" placeholder="Contact" value="<?php echo $userDetails['user_contact']; ?>" 
                    required="required">
                </div>

                <div class="col-md-6 m-b18">
                    <input type="text" name="position" id="position" class="p-18 p-tb9 text-dgrey col-xs-12" placeholder="Position" value="<?php echo $userDetails['sports_position']; ?>"  required="required">
                </div>

                <div class="col-md-6 m-b18" style="height: 7%;">
                        <select name="sports_club" id="sports_club" data-placeholder="Choose a Club" class="chosen-select selectpicker p-tb9 col-xs-12" tabindex="2" >
                        <option value="0">Select Club</option>
                        <?php foreach ($clubData as $club) {?>
                        <option value="<?php echo $club['clubID'];?>"><?php echo $club['clubName']; ?></option>
                        <?php }?>
                    </select>    
                </div>
                
                <div class="col-md-6 m-b18">
                    <select name="sportsID" id="sportsID" required="required" class="selectpicker p-tb9 col-xs-12" >
                        <option value="">Select Sports</option>
                        <?php foreach ($sports as $sp) {?>
                        <option value="<?php echo $sp['id'];?>"><?php echo $sp['sportsTitle']; ?></option>
                        <?php }?>
                    </select>
                </div>
                
                <div class="col-md-6 m-b18">
                    <select name="country" id="country" required="required" class="selectpicker p-tb9 col-xs-12">
                        <option value="0">Select Country</option>
                        <?php foreach ($countryArray as $country) {?>
                        <option value="<?php echo $country['countryId'];?>"><?php echo $country['countryName']; ?></option>
                        <?php }?>
                    </select>
                </div>
                
                <div class="col-md-6 m-b18">
                    <select name="city" id="city" class="selectpicker p-tb9 col-xs-12">
                        <option value="">Select City</option>
                        <?php if($userDetails['country']  > 0 ){ ?>
                        <?php foreach ($getCity as $cty) { ?>
                        <option value="<?php echo $cty['ctID']; ?>"><?php echo $cty['ctName']; ?></option>
                        <?php } }?>
                    </select>
                </div>

                <hr style="border: 1px solid lightgray!important;clear: both!important;">

                <div class="col-md-6 m-b18">
                    <input type="text" name="birthdate" id="birthdate" class="p-18 p-tb9 text-dgrey col-xs-12" placeholder="Date Of Birth" value="<?php echo $userDetails['birthdate'];?>" required='required'>
                </div>
                
                <div class="col-md-6 m-b18">
                    <input type="text" name="user_height" id="user_height" class="p-18 p-tb9 text-dgrey col-xs-12" placeholder="Height" value="<?php echo $userBioInfo['height'];?>" required='required'>
                </div>
                <div class="col-md-12" style="padding-top: 5%!important;">
                    <input type="submit" name="submit" id="submit" class="span3 btn bg-grey p-lr9 font-uc p-18 text-white" value="Update">
                </div>    
        </div>
       </div>
       </div>
       </form>
    </div>
    </div>
      
<script src="/siteAssets/js/chosen/jquery.js" type="text/javascript"></script>
<script src="/siteAssets/js/chosen/chosen.jquery.js" type="text/javascript"></script>
<script src="/siteAssets/js/chosen/init.js" type="text/javascript" charset="utf-8"></script>  
<script src="/siteAssets/js/jquery.js"></script>
<script src="/siteAssets/js/jquery-ui.js"></script>
<script type="text/javascript">
    $('#country').change(function(){
            var cntyId = $('#country').val();
            // alert(cntyId);
            $.ajax({
                url:"/getCityByCntryId_ajax.php",
                type:'POST',
                data:'countryId='+cntyId,
                success: function(response){
                    // alert(response);
                    if(response!=0){
                        $("#city").html('');
                        $("#city").append(response);
                    }
                    else{
                        $("#city").html('<option value="0">None</option>');   
                    }
                }
            });
        });

    document.getElementById('city').value='<?php echo $userDetails["city"];?>';
    document.getElementById('sports_club').value='<?php echo $userDetails['sports_club']; ?>';
    document.getElementById('country').value='<?php echo $userDetails['country']; ?>';
    document.getElementById('sportsID').value='<?php echo $userDetails['sportsID']; ?>';
        
    jQuery( function() {
        $( "#birthdate" ).datepicker({
            changeMonth: true,
            changeYear: true,
            yearRange: "-100:+0",
            dateFormat: 'yy-mm-dd'
            });
        });
</script>
    
    