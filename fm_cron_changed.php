<?php 
## Include required Files
/***************************/
chdir("/var/www/html/redesign/");
$_SERVER['DOCUMENT_ROOT'] = dirname(__FILE__);
require_once '/var/www/html/redesign/config/settings.php';
require_once '/var/www/html/redesign/includes/phpmailer/class.phpmailer.php';
require_once '/var/www/html/redesign/model/email.php';
require_once '/var/www/html/redesign/model/sports.php';
require_once '/var/www/html/redesign/model/city.php';
require_once '/var/www/html/redesign/model/country.php';

## Create Object
/***************************/
$sportsObj = new Model_Sports();
$emailObj  = new Model_Email();
$mail 	   = new PHPMailer(true);
$countryObj= new Model_Country();
$citiObj   = new Model_City();

## Fetch All Users Data
/***************************/
$result=$usersObj->getAllUserProfileDetailOnlySubscribUser();
foreach ($result as $results)
{
   $user_IDs[]=$results['id'];
}

/*$user_IDs = array("426","621");*/
mail("psrajender@gmail.com","TEsT","TESTST");
$total_emails = 0;
foreach ($user_IDs as $user_ID)
{
	$mail 	  = new PHPMailer(true);
	$receiver = array();
    foreach ($result as $results)
    { 
 	    $row[]=$results['id'];
    }

	/*** Get Followig Data ***/
	
	$get_following_data=$usersObj->getAllFollowingsByUserId($user_ID);
	foreach ($get_following_data as $following)
	{
	  $get_following[]=$following['followUID'];
	}

	/*** Get followers data ***/

	$get_followers_data=$usersObj->getFollowersByUserId($user_ID);
	foreach ($get_followers_data as $followers)
	{
	  $get_followers[]=$followers['userID'];
	}

	$following_followers[]=$user_ID;

	/*** Merge Following & followers Array ***/

	$following_followers=array_merge($get_following,$get_followers);
	$following_followers[]=$user_ID;
	$row=array_unique($row);
	$remaining_users=array_diff($row, $following_followers);
	
	/*** Get data of already mailed users ***/    
    
    $already_sent=$usersObj->getFollowMail($user_ID);
    if(count($already_sent)>0)
    {
        foreach ($already_sent as $mail_sent)
        {
            $sent[]=$mail_sent['sentmail_id'];
        }
      	$remaining_users=array_diff($row, $sent);
    }
	
 	shuffle($remaining_users);
	//print_r($remaining_users);

	if(!empty($remaining_users))
 	{
   		$remaining_users = array_slice($remaining_users, 0, 5);	
   		foreach ($remaining_users as $random_userID)
    	{
       		$data=array(
                    'loggedin_id'=>$user_ID,
                    'sentmail_id'=>$random_userID,
                    'sent_date'  =>date("Y-m-d h:i:s")  
       	        );
				//$result=$usersObj->AddFollowMail($data); 
      		$receiver[]=$usersObj->getUserProfileDetailEmailByOnlyUserID($random_userID);
    	}
	
	/******* Setup the mail ********/      
    	
    	$follow_list = "";
		$user=$usersObj->getUserProfileDetailEmailByOnlyUserID($user_ID);
		$emailArray = $emailObj->getEmailById(20);
  
		echo $to = $user['email'];
		echo "<br>";
		echo $toname = $user['fname'].' '.$user['lname'];		
		echo "<br>";
		echo $fromname = $emailArray['fName'];
		echo "<br>";
		echo $from = $emailArray['fEmail'];
        echo "<br>";
		echo "<br>";
		echo "<br>";
				
		$subject = $emailArray['emailSub'];
		$subject = str_replace('[SITENAME]', SITENAME, $subject);
			    
        /*** Get All Countries Data ***/
		$all_countries = $countryObj->getAllCountry();
		$country_name_arr=array();
		foreach ($all_countries as $all_country)
		{
			$country_name_arr[$all_country['countryId']]=$all_country['countryName'];
		}

		/*** Get All Cities Data ***/
		$all_cities = $citiObj->getAllActiveCity();
		$city_name_arr=array();
		foreach ($all_cities as $all_city)
		{
			$city_name_arr[$all_city['ctID']]=$all_city['ctName'];
		}

		$str ="";
		foreach($receiver as $receivers)
		{	
			/*CountryName*/
			$country_ID=$receivers['country'];
		    if($country_ID >0)
			{
				$country_name=$country_name_arr[$country_ID];
			}

			/*CityName*/
			$city_ID=$receivers['city'];
		    if(count($city_ID)>0)
			{
				$city_name=$city_name_arr[$city_ID];
		  	}
			
			/*SportsName&Image*/
			$sports=$receivers['sportsID'];
		    $img_str ="";
		    if($sports!='')
	     	{
		     	$sportsResult = $sportsObj->getSportsNameAndImageUsingSportsId($sports);
                foreach($sportsResult as $image_row)
             	{
                    $img_str .= "<img id='sports_img' style='margin-right:8px;' src='".SITE_URL."/dynamicAssets/sports/27x27/".$image_row['sportsImage']."' alt='".$image_row['sportsTitle']."' title='".$image_row['sportsTitle']."'>";
                }  
		       	$sportsResult=  $img_str;
            }
		    else
         	{
             	$sportsResult="---";
         	}

		   	/*ProfileImage*/
		   	$gender=$receivers['sex'];
           	if($gender=='m')
           	{
	           	$receiver_image=$receivers['avatar'];
                if($receiver_image=='')
			    {
			     	$receiver_image=SITE_URL.'/siteAssets/images/male_default_images1.png';
			    }
			    else
		     	{
			     	$file_exist = file_exists($_SERVER['DOCUMENT_ROOT']."/dynamicAssets/avatar/238x259/".$receiver_image);
			     	if($file_exist == '0')
			     	{
			     		$receiver_image=SITE_URL.'/siteAssets/images/male_default_images1.png';
			     	}
			        else
			        {   
			            $receiver_image=SITE_URL.'/dynamicAssets/avatar/238x259/'.$receiver_image;
                    }
                }
            }
            else
            {
                $receiver_image=$receivers['avatar'];
                if($receiver_image=='')
			  	{
				   	$receiver_image=SITE_URL.'/siteAssets/images/female_default_images1.png';
			  	}
				else
			  	{
	  			    $file_exist = file_exists($_SERVER['DOCUMENT_ROOT']."/dynamicAssets/avatar/238x259/".$receiver_image);
			     	if($file_exist == '0')
			     	{
			     		$receiver_image=SITE_URL.'/siteAssets/images/female_default_images1.png';
			     	}
			        else
			        {   
			            $receiver_image=SITE_URL.'/dynamicAssets/avatar/238x259/'.$receiver_image;
                    }
              	}
            }
    
            /***************** Email Templates *******************/  
         		$str .='<tr><td width="5%">&nbsp;</td>
    						<td valign="top" style="max-width:600px;width:90%;">
    						<table border="0" cellpadding="0" cellspacing="0" style="max-width:600px;width:100%;">
    						<tbody><tr><td class="centerMobile" style="text-align: left; vertical-align: top; font-size: 0;">
					        <div style="display:inline-block;max-width:128px;vertical-align:top;width:100%;">
					        <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:128px;"><tbody>
					        <tr><td class="profileTopGap" height="30" style="height:30px;line-height:0px;font-size:0px;">&nbsp;</td>
					        </tr><tr><td align="center">';
				$str .='<a target="_blank"><img src="'.$receiver_image.'" class="centerMobile" border="0" height="128" width="128" alt="'.$receivers['fname'].' '.$receivers['lname'].'" style="height:128px;width:128px;display:block;-moz-border-radius:2px;-webkit-border-radius:2px;border-radius:2px;"></a></td></tr><tr>
            		<td class="simpleHideOnMobile" height="10" style="height:10px;line-height:0px;font-size:0px;">&nbsp;</td></tr>
			        </tbody></table></div>';
		        $str .='<div style="display:inline-block;max-width:425px;vertical-align:top;width:100%;">
        					<table class="centerMobile" width="100%" border="0" cellspacing="0" cellpadding="0" align="left" style="max-width:425px;"><tbody><tr>
            			<td colspan="3" class="profileTopGap" height="30" align="left" style="height:30px;line-height:0px; font-size:0px;">&nbsp;</td>
        				</tr><tr><td width="5%">&nbsp;</td><td class="centerMobile" width="90%" align="left">
            			<table style="max-width:380px;width:100%;" border="0" cellspacing="0" cellpadding="0">
            			<tbody><tr>';
            	$str .='<td class="centerMobile" height="25" style="height:25px;text-align:left;font-family: helvetica,arial,sans-serif;color:#333333;font-size:24px;mso-line-height-rule:exactly;line-height:30px;word-break:break-all;"><a style="text-decoration:none;font-family:helvetica,arial,sans-serif;color:#333333;font-size:24px;word-break:break-all;" target="_blank">'.$receivers['fname'].' '.$receivers['lname'].'</a></td></tr>
        		<tr><td height="8" style="text-decoration:none;font-size:0px;line-height:0px;">&nbsp;</td>
        		</tr><tr><td class="centerMobile" valign="top" style="text-decoration:none;font-family:Helvetica,Arial,sans-serif;color:#333333;font-size:14px;line-height:20px;">'.$city_name.' <b> '.$country_name.'</b><br>'.$sportsResult.'</td></tr>
        		<tr><td height="10" style="height:10px;text-decoration:none;font-size:0px;line-height:0px;">&nbsp;</td>
		        </tr><tr><td height="20" style="height:20px;text-decoration:none;font-size:0px;line-height:0px;">&nbsp;</td>
		        </tr><tr><td class="centerMobile"><table class="inlineBlock mobileBtnExpander" border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate;">
            	<tbody><tr><td class="btn" lang="x-btn" valign="middle" bgcolor="#313D50" align="center" style="border-collapse: separate;-moz-border-radius: 3px; -webkit-border-radius: 3px; border-radius: 3px;box-shadow: 0 1px 1px 0 rgba(0, 0, 0, .3);">
            	<a style="text-decoration:none;font-family:Arial,Helvetica,sans-serif;font-size:17px;color:#fff;white-space:nowrap;" href="'.SITE_URL.'/'.$user['email'].'/'.$receivers['username'].'/follow/login" target="_blank">
                <table cellpadding="0" cellspacing="0" border="0">
                <tbody><tr><td height="10" style="height:10px;line-height:0px;font-size:0px;">&nbsp;</td>
                </tr><tr><td align="center" style="padding:0px 24px;"><span style="text-decoration:none;font-family:Arial,Helvetica,sans-serif;font-size:17px;color:#fff;white-space:nowrap;">Follow</span></td>
               		</tr><tr>';
                $str .='<td height="10" style="height:10px;line-height:0px;font-size:0px;">&nbsp;</td>
                		</tr></tbody></table></td></tr></tbody></table></a></td></tr></tbody></table>
                		</td><td width="5%">&nbsp;</td></tr></tbody></table></div></td></tr></tbody></table></td><td width="5%">&nbsp;</td></tr>';
               	$str .='<tr><td width="5%">&nbsp;</td>
				        <td height="1" style="height:1px;width:90%;line-height:0px;font-size:0px;border-bottom: 1px solid #887777;">&nbsp;</td><td width="5%">&nbsp;</td></tr>';
                
                /************* End Email Templates ********************/  
        }

        /*** Setupt the Mail Templates ***/	
		$message = $emailArray['emailContent'];
		$message = str_replace('[SITEURL]', SITE_URL, $message);
		$message = str_replace('[SITENAME]', SITENAME, $message);
        $message = str_replace('[UNAME]', $user['fname'], $message);
        $message = str_replace('[follow_list]', $str , $message);
        $message = str_replace('[userid]', $user_ID , $message);

		$template_msg	= str_replace('[MESSAGE]',$message , $message);
		$template_msg 	= str_replace('[LOGO]','<img src="'.SITE_URL.'/siteAssets/images/newimages/logo.png">', $template_msg);
		$template_msg 	= str_replace('[PASSWORD]',$_SESSION['mail_psswd'], $template_msg);
		$template_msg 	= str_replace('[SITELINK]',SITE_URL , $template_msg);
		$template_msg 	= str_replace('[SUBJECT]',$subject , $template_msg);
		$template_msg	= str_replace('[SITEROOT]',SITE_URL , $template_msg);					
		$template_msg	= str_replace('[UNAME]',UNAME , $template_msg);	
        $template_msg	= str_replace('[follow_list]',$str , $template_msg);	
        $template_msg	= str_replace('[userid]',$user_ID , $template_msg);	
                
        try
      	{
		  	echo $to;
		  	echo "<br>";
			$mail->AddAddress($to, $toname);
			$mail->SetFrom("admin@sportsplaya.com", $fromname);
			//$mail->addBCC("lalit.wiseit@gmail.com","Lalit");
			//$mail->addBCC("psrajender@gmail.com","Rajendra Bhatia");
			//$mail->addBCC("phillipb@oceaniainternet.com.au","Phillip Bell");
			$mail->Subject = $subject;
			echo $mail->Body = $template_msg; 
		  	echo "<br>";
		  	echo "<br>";
		  	echo "<br>";
		  	echo "<br>";
		  	echo "<br>";
		  	echo "<br>";
			$mail->Send();
	  	} 
		catch (phpmailerException $e)
	 	{
		    echo $_SESSION['msg']= $e->errorMessage(); //Pretty error messages from PHPMailer
	 	}  
		catch (Exception $e)
	 	{
		    echo $_SESSION['msg']= $e->getMessage(); //Boring error messages from anything else!
	 	}
    }
    else{
    	echo "Mail Is Already Sent To All Suggested Users.";
    }
$total_emails++;
}   
echo "<br>";
echo "TOTAL EMAILS SENT ==>" . $total_emails;
?>