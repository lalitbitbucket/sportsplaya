{include file=$header_start}

{assign var='papergrindercss' value="datepicker/css/paper-grinder.css"}
{assign var='magnificpopup' value="siteAssets/magnificpopup/magnific-popup.css"}
{assign var='jqueryui1816custommin' value="datepicker/js/jquery-ui-1.8.16.custom.min.js"}
{assign var='jqueryvalidatemin' value="js/jquery.validate.min.js"}
{assign var='bootboxminjs' value="js/bootbox.min.js"}
{assign var='registerjs' value="js/users/register.js"}
{assign var='colorjs' value="js/jquery.colorbox-min.js"}
{assign var='colorcss' value="css/colorbox.css"}

{strip}
<link rel="stylesheet" type="text/css" href="{$siteroot}/siteAssets/magnificpopup/magnific-popup.css" type="text/css" media="screen"/>
<!--
<link rel="stylesheet" type="text/css" href="{$siteroot}/cached.php?f={$papergrindercss|base64_encode}&amp;type=text/css" media="screen"/>

<link rel="stylesheet" type="text/css" href="{$siteroot}/cached.php?f={$colorcss|base64_encode}&amp;type=text/css" media="screen"/>

<script type="text/javascript" src="{$siteroot}/cached.php?f={$jqueryui1816custommin|base64_encode}&amp;type=application/javascript"></script>

<script type="text/javascript" src="{$siteroot}/cached.php?f={$jqueryvalidatemin|base64_encode}&amp;type=application/javascript"></script>

<script type="text/javascript" src="{$siteroot}/cached.php?f={$bootboxminjs|base64_encode}&amp;type=application/javascript"></script>

<script type="text/javascript" src="{$siteroot}/cached.php?f={$registerjs|base64_encode}&amp;type=application/javascript"></script>
<script type="text/javascript" src="{$siteroot}/cached.php?f={$colorjs|base64_encode}&amp;type=application/javascript"></script>
-->
{/strip} 

{literal}       
<style>
#cboxOverlay{ background:#666666; }
#colorbox{ width: 305px !important; height: 240px !important; }
#ui-datepicker-div {position: relative !important;top: -170px !important; max-height: -1px !important;}
@media all and (min-width:480px) and (max-width:550px){ #ui-datepicker-div {margin-left:6%}}
@media all and (min-width:800px) and (max-width:990px){ #ui-datepicker-div {margin-left:6%;top: -150px !important}}
@media all and (min-width:990px) and (max-width:1200px){ #ui-datepicker-div {margin-left:55%;top: -120px !important}}
@media all and (min-width:1200px){ #ui-datepicker-div {margin-left:3%;top: -120px !important;}}
</style>
{/literal}

{include file=$header_end}

<!-- MIDDLE SECTION  -->    
	<div class="cntArea">

    	<section class="container">
	        <div class="row">
	        	<div class="col-md-4">
                    <a href="{$siteroot}/playavideo_popuppage.php?vId={$playavideo.id}" class="ajax-popup-link">
                        <img src="{$siteroot}/siteAssets/images/newimages/play_list_play_icon.png" class="play_link" style="transform: translate(-50%,-50%);top: 50%;top: 7.5rem;position: absolute;z-index: 1;left: 50%;opacity: 0.5; animation:aniFadeIn 1s ease-out" />
                        <img class="animation-element home_video_image" src="{$playavideo.image_url}" height='336' width='448' />
                    </a>
              </div>
                <div class="col-md-7">
                    <div class="animation-element featured_video_text text-small-center">
                        <p class="text-tan font-segoe p-40"><strong>Play of the day!</strong></p>
                        <p class="text-white p-111 title">{$playavideo.title|wordwrap:30:"<br />\n"}</p>
                        <p class="vote_link p-b18"><span class="text-tan">Vote now for the next </span>
                        <span class="text-white">play of the Day!</span></p>
                        <p class="down_link"><a class="btn bg-transparent text-white down_arrow block-xs p-tb27 visible-xs"></a></p>
                    </div>
                </div>
	        </div>
        </section>

        <!-- Display All Nominees -->
    	<section class="container bg-white-xs">
		    <div class="row">
                <div class="col-md-12">
                    <article class="play_list">
                        <ul>
                        {section name=a loop=$albumArray}
                            <li class="animation-element">    
                                <a href="{$siteroot}/nominee_popuppage.php?uId={$albumArray[a].uId}" class="ajax-popup-link play_thumb">
                                <img src="{$siteroot}/siteAssets/images/newimages/play_list_play_icon.png" 
                                class="play_link"/>
                                <img src="{$albumArray[a].image_url}" class="home_video_image " height='252' width='336' /></a>
                                <p class="title" style="min-height: 45px;"><strong>{$albumArray[a].albumname}</strong></p>
                                <p class="author">{$albumArray[a].fname} {$albumArray[a].lname}</p>
                                <p class="cat-desc">{$albumArray[a].sports_name} • {if $albumArray[a].date_diff le 0}Just an hour ago{else}{$albumArray[a].date_diff} days ago{/if} </p>
                                <p>
                                {if $albumArray[a].str_rating eq 0}
                                    <img src="{$siteroot}/siteAssets/images/newimages/unrate.png" class="pull-left" />
                                    <img src="{$siteroot}/siteAssets/images/newimages/unrate.png" class="pull-left" />
                                    <img src="{$siteroot}/siteAssets/images/newimages/unrate.png" class="pull-left" />
                                    <img src="{$siteroot}/siteAssets/images/newimages/unrate.png" class="pull-left" />
                                    <img src="{$siteroot}/siteAssets/images/newimages/unrate.png" class="pull-left" />
                                {elseif $albumArray[a].str_rating le '20'}
                                    <img src="{$siteroot}/siteAssets/images/newimages/rate.png" class="pull-left" />
                                    <img src="{$siteroot}/siteAssets/images/newimages/unrate.png" class="pull-left" />
                                    <img src="{$siteroot}/siteAssets/images/newimages/unrate.png" class="pull-left" />
                                    <img src="{$siteroot}/siteAssets/images/newimages/unrate.png" class="pull-left" />
                                    <img src="{$siteroot}/siteAssets/images/newimages/unrate.png" class="pull-left" />
                                {elseif $albumArray[a].str_rating le '40'}
                                    <img src="{$siteroot}/siteAssets/images/newimages/rate.png" class="pull-left" />
                                    <img src="{$siteroot}/siteAssets/images/newimages/rate.png" class="pull-left" />
                                    <img src="{$siteroot}/siteAssets/images/newimages/unrate.png" class="pull-left" />
                                    <img src="{$siteroot}/siteAssets/images/newimages/unrate.png" class="pull-left" />
                                    <img src="{$siteroot}/siteAssets/images/newimages/unrate.png" class="pull-left" />
                                {elseif $albumArray[a].str_rating le '60'}
                                    <img src="{$siteroot}/siteAssets/images/newimages/rate.png" class="pull-left" />
                                    <img src="{$siteroot}/siteAssets/images/newimages/rate.png" class="pull-left" />
                                    <img src="{$siteroot}/siteAssets/images/newimages/rate.png" class="pull-left" />
                                    <img src="{$siteroot}/siteAssets/images/newimages/unrate.png" class="pull-left" />
                                    <img src="{$siteroot}/siteAssets/images/newimages/unrate.png" class="pull-left" />
                                {elseif $albumArray[a].str_rating le '80'}
                                    <img src="{$siteroot}/siteAssets/images/newimages/rate.png" class="pull-left" />
                                    <img src="{$siteroot}/siteAssets/images/newimages/rate.png" class="pull-left" />
                                    <img src="{$siteroot}/siteAssets/images/newimages/rate.png" class="pull-left" />
                                    <img src="{$siteroot}/siteAssets/images/newimages/rate.png" class="pull-left" />
                                    <img src="{$siteroot}/siteAssets/images/newimages/unrate.png" class="pull-left" />
                                {elseif $albumArray[a].str_rating le '100'}
                                    <img src="{$siteroot}/siteAssets/images/newimages/rate.png" class="pull-left" />
                                    <img src="{$siteroot}/siteAssets/images/newimages/rate.png" class="pull-left" />
                                    <img src="{$siteroot}/siteAssets/images/newimages/rate.png" class="pull-left" />
                                    <img src="{$siteroot}/siteAssets/images/newimages/rate.png" class="pull-left" />
                                    <img src="{$siteroot}/siteAssets/images/newimages/rate.png" class="pull-left" />
                                {/if}
                                </p>
                            </li>
                            {/section} 
                        </ul>
                        <div class="row">
                            <div class="nominees_link text-center p-tb65 col-md-12">
                                <a href="{$siteroot}/nominees/all" class="btn bg-transparent text-dgrey p-30 block-xs bg-grey-xs text-white-xs">
                                VIEW ALL NOMINEES</a>
                            </div>
                        </div>
                    </article>
                </div>
            </div>        
        </section>
        <!-- End Display All Nominees -->

        <!-- Become A Playa Section -->
        <div class="become_playa_bg ">
	    	<section class="playaofDay">
            <article>
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 text-center become_Playa_video">&nbsp;
                            <!--<img src="{$siteroot}/siteAssets/images/newimages/become_playa_video.png" />-->
                        </div>
                        <div class="col-md-6 text-white become_Playa_desc">
                            <h1>
                                <p class="text-tan p-60 font-segoeLight font-normal m-b7 p-b0 text-small-center">Love the game?</p>
                                <p class="font-uc p-74 m-b50 text-small-center">Become a Playa</p>
                            </h1>
                            <div class="playa_steps font-segoe">
                                <p class="p-24">1. Create a profile</p>
                                <p class="p-24">2. Upload your video</p>
                                <p class="p-24">3. Enjoy the spotlights and be discovered!</p>
                            </div>
                            <div class="playa_steps">
                                <p class="p-29">In addition you'll also receive these benefits:</p>
                                <ul class="p-24">
                                    <li>Direct access to the best SportsPlaya videos</li>
                                    <li>The ability to vote for the next Playa of the Day</li>
                                    <li>Unlock commenting on videos</li>
                                    <li>Connect with other Playas</li>
                                </ul>
                                <p>
                                {if $otheruserDetails.id eq ''}
                                <a href="{$siteroot}/register_popuppage.php" class="ajax-popup-link btn text-uc p-20 font-uc bg-grey text-white block-xs">Sign up now</a>
                                {/if}
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </article>
        </section>
        </div>
        <!-- End / Become A Playa Section -->

        <!-- Start featuredPlaya Section -->
        <section class="featured_playa p-b40">
            <div class="container">
            	<div class="row">
                    <div class="col-md-12">
                        <h1 class="font-uc  p-74 p-b120 p-t160 text-small-center">Featured Playa</h1>
                    </div>
                    <div class="col-xs-12 col-md-9 pull-right expand-xs">
                        <a href="{$siteroot}/myplaya_popuppage.php?vId={$featured_playa.id}" class="ajax-popup-link">
                        <img src="{$siteroot}/siteAssets/images/newimages/logo.png" class="play_of_day_logo" />
                        <img src="{$siteroot}/siteAssets/images/newimages/play_of_day_btn.png" class="play_of_day_btn" />
                        <div class="play_of_day_info">
                        <p class="p-30 font-segoe font-bold m-b18">{$featured_playa.title}</p>
                        <p class="p-20">{$featured_playa.username}<br>{$featured_playa.total_clicks} Views</p>
                        </div>
						</a>
                        <a href="{$siteroot}/myplaya_popuppage.php?vId={$featured_playa.id}" class="ajax-popup-link">
                        <img src="{$featured_playa.image_url}"  class="home_video_image mainImg m-b18" width="100%" height="auto">
                        </a>
                    </div>
                    <div class="col-xs-12 col-md-3 m-l0">
                    	<div class="col-xs-4 col-md-12 p-lr0">
                            <p class="m-b9">
                                <a href="{$siteroot}/{$featured_playa.username}">
                                <img src="{$featured_playa.img_profile}"/>
                                </a>
                            </p>
                        </div>
                    	<div class="col-xs-8 col-md-12 p-lr0">
                            <a href="{$siteroot}/{$featured_playa.username}">
                            <h6 class="p-30 font-segoe m-t0"><strong>{$featured_playa.fname} {$featured_playa.lname}</strong></h6>
                            </a>
                            <p class="p-20">
                                <span class="category font-segoe text-dgrey"><strong>{$featured_playa.sportsTitle}:</strong></span><br/>
                                <span class="category text-grey">{$featured_playa.sports_position}<br/>{$featured_playa.clubName}</span>
                            </p>
                            <p class="p-20 p-b27 m-b0 hide">
                                <span class="category font-segoe text-dgrey"><strong>Play of the Day :</strong></span><br/>
                                <span class="category text-grey">{$featured_playa.from_date|date_format:'%d %B %Y'}</span>
                            </p>
                           <!--  <p class="hidden-xs">
                                <a href="{$siteroot}/{$featured_playa.username}" class="btn font-segoe bg-grey text-white font-uc p-20">Visit Profile</a>
                            </p> -->
                        </div>
                    </div>
                    <!-- <div class="visible-xs">
                        <a href="{$siteroot}/{$featured_playa.username}" class="btn font-segoe bg-grey text-white font-uc col-xs-12 p-20">Visit Profile</a>
                    </div> -->
                </div>
                <div class="row hidden"> <!-- Commented Code for hidden -->
                    <div class="col-md-9">
                        <div class="row">
                            <!-- Comments Section  Start-->
                            <div class="col-xs-12">
                                <!-- Start Form -->
                            <form method="POST" id="cmtform" name="cmtform" onsubmit="return VideoComments()">
                                <p class="category font-segoe text-dgrey"><strong>Comments</strong></p>
                                <textarea class="xtarea p-tb18 p-lr18 m-b18 col-md-11" placeholder="Add a comment..." rows="70" name="comment" id="comment" style="background:#f0f2f7;color:#676767;border:none;overflow: hidden;height: 9.4rem!important;">{$smarty.post.value}</textarea>
                                <input type="hidden" name="videoId" id="videoId" value="{$featured_playa.id}">
                                <input type="hidden" name="userId" id="userId" value="{$smarty.session.stagUserProfileId}">
                                {if $smarty.session.stagUserProfileId ne ''}
                                <p><input type="submit" name="submit" value="POST" class="btn bg-grey text-white block-xs m-b40" /></p>
                                {/if}
                            </form>
                            <!-- End Form -->
                            </div>
                            <!-- View Comments -->
                            <div class="col-xs-12">
                                <ul class="play_details" id="comment_box">
                                    {section name=a loop=5}
                                    <li class="p-b18">
                                    <p class="m-b0 text-dgrey">
                                    <strong>{$getComments[a].fname} {$getComments[a].lname}&nbsp;&nbsp;&nbsp;&nbsp;{$getComments[a].date_formated}</strong>
                                    </p>
                                    <p class="text-lgrey m-b0">{$getComments[a].comment_text}</p></li>
                                    {/section}
                                </ul>
                            </div>
                             <!-- End View Comments -->
                        </div>
                    </div>
                    <!-- View User All Videos -->
                    <div class="col-md-3 p-tb40 "> 
                    <div class="row p-tb18">
                    <article class="play_list col-xs-12">
                        <div id="notfound_msg" class="text-center" style="display: none;">Videos not found ! 
                        </div>
                        {if $total_videos ne 0}
                        <ul class="news_list">
                        {section name=a loop=1}
                            <li style="width:100%;">
                               <a href="javascript:void(0);" class="play_thumb">
                                <img src="{$siteroot}/siteAssets/images/newimages/play_list_play_icon.png"
                                class="play_link"/>
                                <img src="{$usersAllVideos[a].image_url}" class="home_video_image" height='252' width='336'/>
                                </a> 

                                <p class="title"><strong>{$usersAllVideos[a].albumname}</strong></p>
                                <p class="author">{$usersAllVideos[a].fname} {$usersAllVideos[a].lname}</p>
                                <p class="cat-desc">{$usersAllVideos[a].sports_name} • {if $usersAllVideos[a].date_diff le 0}Just an hour ago{else}{$usersAllVideos[a].date_diff} days ago{/if} </p>
                                <p>
                                {if $usersAllVideos[a].str_rating eq 0}
                                    <img src="{$siteroot}/siteAssets/images/newimages/unrate.png" class="pull-left" />
                                    <img src="{$siteroot}/siteAssets/images/newimages/unrate.png" class="pull-left" />
                                    <img src="{$siteroot}/siteAssets/images/newimages/unrate.png" class="pull-left" />
                                    <img src="{$siteroot}/siteAssets/images/newimages/unrate.png" class="pull-left" />
                                    <img src="{$siteroot}/siteAssets/images/newimages/unrate.png" class="pull-left" />
                                {elseif $usersAllVideos[a].str_rating le '20'}
                                    <img src="{$siteroot}/siteAssets/images/newimages/rate.png" class="pull-left" />
                                    <img src="{$siteroot}/siteAssets/images/newimages/unrate.png" class="pull-left" />
                                    <img src="{$siteroot}/siteAssets/images/newimages/unrate.png" class="pull-left" />
                                    <img src="{$siteroot}/siteAssets/images/newimages/unrate.png" class="pull-left" />
                                    <img src="{$siteroot}/siteAssets/images/newimages/unrate.png" class="pull-left" />
                                {elseif $usersAllVideos[a].str_rating le '40'}
                                    <img src="{$siteroot}/siteAssets/images/newimages/rate.png" class="pull-left" />
                                    <img src="{$siteroot}/siteAssets/images/newimages/rate.png" class="pull-left" />
                                    <img src="{$siteroot}/siteAssets/images/newimages/unrate.png" class="pull-left" />
                                    <img src="{$siteroot}/siteAssets/images/newimages/unrate.png" class="pull-left" />
                                    <img src="{$siteroot}/siteAssets/images/newimages/unrate.png" class="pull-left" />
                                {elseif $usersAllVideos[a].str_rating le '60'}
                                    <img src="{$siteroot}/siteAssets/images/newimages/rate.png" class="pull-left" />
                                    <img src="{$siteroot}/siteAssets/images/newimages/rate.png" class="pull-left" />
                                    <img src="{$siteroot}/siteAssets/images/newimages/rate.png" class="pull-left" />
                                    <img src="{$siteroot}/siteAssets/images/newimages/unrate.png" class="pull-left" />
                                    <img src="{$siteroot}/siteAssets/images/newimages/unrate.png" class="pull-left" />
                                {elseif $usersAllVideos[a].str_rating le '80'}
                                    <img src="{$siteroot}/siteAssets/images/newimages/rate.png" class="pull-left" />
                                    <img src="{$siteroot}/siteAssets/images/newimages/rate.png" class="pull-left" />
                                    <img src="{$siteroot}/siteAssets/images/newimages/rate.png" class="pull-left" />
                                    <img src="{$siteroot}/siteAssets/images/newimages/rate.png" class="pull-left" />
                                    <img src="{$siteroot}/siteAssets/images/newimages/unrate.png" class="pull-left" />
                                {elseif $usersAllVideos[a].str_rating le '100'}
                                    <img src="{$siteroot}/siteAssets/images/newimages/rate.png" class="pull-left" />
                                    <img src="{$siteroot}/siteAssets/images/newimages/rate.png" class="pull-left" />
                                    <img src="{$siteroot}/siteAssets/images/newimages/rate.png" class="pull-left" />
                                    <img src="{$siteroot}/siteAssets/images/newimages/rate.png" class="pull-left" />
                                    <img src="{$siteroot}/siteAssets/images/newimages/rate.png" class="pull-left" />
                                {/if}
                                </p>
                            </li>

                          {/section}
                           </ul>
                          {else}
                            <div class="text-center">Videos not found ! </div>

                          {/if}
                            <!-- <input type="hidden" name="up_offset" id="up_offset" value="12">
                            <input type="hidden" name="total_record" id="total_record" value="{$total_record}">
                            <input type="hidden" name="hidden_sports_id" id="hidden_sports_id" value="0"> -->
                        </article></div></div>
                    <!-- View User All Videos -->
                </div>
                    
                    <div class="nominees_link p-t40 border-bottom2 col-xs-12 m-b40">
                    <a href="{$siteroot}/{$featured_playa.username}" class="bg-transparent text-dgrey p-tb18 p-28 col-xs-8 col-xs-push-2 col-md-4 col-md-push-0 p-lr0 noUc" >Visit this Playa's profile <span class="col-xs-1 pull-right text-grey">&rsaquo;</span></a>
                    </div>
                    <!-- End Comments Section -->

        </section>
        <!-- end /featuredPlaya Section -->



        <section class="news_slider">
            <div class="container ">
            	<div class="row">
                    <ul class="home_news">
                        {section name=job loop=$jobsArray}
                        <li>
                            <div class="col-md-8 col-md-push-2 text-center">
                                <p class="font-segoeLight text-tan p-60 m-b27">Docklands FC is looking for a</p>
                                <p class="text-white p-74 p-b40 min-h198">{$jobsArray[job].job_title} ({$jobsArray[job].job_type})</p>
                                <p class="font-segoe p-20 text-white p-b40 col-xs-10 col-xs-push-1 col-md-12 col-md-push-0">{$jobsArray[job].job_description}</p>
                                <p>
                                    <a href="{$siteroot}/jobsdetails/{$jobsArray[job].jobsName}/{$jobsArray[job].id|base64_encode}" class="btn font-segoe bg-grey text-white font-uc p-20 block-xs">See Job Description</a>
                                </p>
                            </div>
                        </li>
                        {/section}
                        <!-- <li>
                            <div class="col-md-8 col-md-push-2text-center">
                                <p class="font-segoeLight text-tan p-60 m-b27">Docklands FC is looking for a</p>
                                <p class="text-white p-74 p-b40">Trainer (Full time)</p>
                                <p class="font-segoe p-20 text-white p-b40  col-xs-10 col-xs-push-1 col-md-12 col-md-push-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                <p class="p-20">
                                    <a href="#" class="btn font-segoe bg-grey text-white font-uc block-xs">See Job Description</a>
                                </p>
                            </div>
                        </li>
                        <li>
                            <div class="col-md-8 col-md-push-2 text-center">
                                <p class="font-segoeLight text-tan p-60 m-b27">Docklands FC is looking for a</p>
                                <p class="text-white p-74 p-b40">Trainer (Full time)</p>
                                <p class="font-segoe p-20 text-white p-b40 col-xs-10 col-xs-push-1 col-md-12 col-md-push-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                <p class="p-20">
                                    <a href="#" class="btn font-segoe bg-grey text-white font-uc block-xs">See Job Description</a>
                                </p>
                            </div>
                        </li> -->
                    </ul>
                </div>
            </div>
        </section>  

        <section class="join_now">
            <div class="container">
            	<div class="row">
                    <div class="col-md-12 padding1rem-xs text-small-center">
                        <h1 class="font-uc  p-74 p-t160">Anybody can Join</h1>
                    </div>
                </div>
            	<div class="row">
                    <div class="col-md-12 padding1rem-xs p-b40">
                        <p class="p-40 p-t18 text-grey p-b65">You don’t have to play the game to <span class="text-dgrey"><strong>become a Playa</strong></span></p>
                    </div>
				</div>
            	<div class="row">
                    <div class="col-md-4 font-uc text-white m-b55">
                        <div class="register_link">
                            <p class="p-28 font-segoeLight text-tan m-b0">Register as a</p>
                            <p class="p-70">Playa</p>
                            <p class="p-24 p-tb27">For player, fans and sport fanatics</p>
                            <p>
                                <a href="{$siteroot}/register_popuppage.php?usertype=3" class="ajax-popup-link btn font-segoe bg-grey text-white font-uc col-md-12 block-xs p-20">Register now</a>
                            </p>
                        </div>
                    </div>
                    <div class="col-md-4 font-uc text-white m-b55">
                        <div class="register_link">
                            <p class="p-28 font-segoeLight text-tan m-b0">Register as a</p>
                            <p class="p-70">Club</p>
                            <p class="p-24 p-tb27">For clubs, teams and associations</p>
                            <p>
                                <a href="{$siteroot}/register_popuppage.php?usertype=6" class="ajax-popup-link btn font-segoe bg-grey text-white font-uc col-md-12 block-xs p-20">Register now</a>
                            </p>
                        </div>
                    </div>
                    <div class="col-md-4 font-uc text-white m-b55">
	                    <div class="register_link">
                            <p class="p-28 font-segoeLight text-tan m-b0">Register as a</p>
                            <p class="p-70">Scout</p>
                            <p class="p-24 p-tb27">For scouts, coaches and mentors</p>
                            <p>
                                <a href="{$siteroot}/register_popuppage.php?usertype=5" href="#" class="ajax-popup-link btn font-segoe bg-grey text-white font-uc block-xs col-md-12 p-20">Register now</a>
                            </p>
                        </div>
                    </div>
				</div>
            </div>
        </section>
    </div>
<script src="{$siteroot}/siteAssets/js/videocomments.js"></script>
{include file=$footer}

