<?php
## Include File
/*****************************/
chdir("/var/www/html/redesign/");
chdir("/var/www/html/redesign/");
$_SERVER['DOCUMENT_ROOT'] = dirname(__FILE__);
require_once '/var/www/html/redesign/config/settings.php';
require_once '/var/www/html/redesign/model/events.php';

## Create Objects
/*****************************/
$eventsObj = new Model_EVENTS();
$jobsObj = new Model_Jobs();

## Create Xml Urls
/*****************************/
$urlData=array();
$urlData[]='<?xml version="1.0" encoding="utf-8"?>';
$urlData[]="\n";
$urlData[]='<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">';
$urlData[]="\n<url>\n<loc>https://www.sportsplaya.com/</loc>\n<lastmod>2016-09-14</lastmod>\n<changefreq>daily</changefreq>\n<priority>1.0</priority>\n</url>\n<url>\n<loc>https://www.sportsplaya.com/form.php</loc>\n<lastmod>2016-09-14</lastmod>\n<changefreq>daily</changefreq>\n<priority>0.9</priority>\n</url>\n<url>\n<loc>https://www.sportsplaya.com/facebook/login</loc>\n<lastmod>2016-09-14</lastmod>\n<changefreq>daily</changefreq>\n<priority>0.9</priority>\n</url>\n<url>\n<loc>https://www.sportsplaya.com/users/login</loc>\n<lastmod>2016-09-14</lastmod>\n<changefreq>daily</changefreq>\n<priority>0.8</priority>\n</url>\n<url>\n<loc>https://www.sportsplaya.com/users/forgotpassword</loc>\n<lastmod>2016-09-14</lastmod>\n<changefreq>daily</changefreq>\n<priority>0.9</priority>\n</url>\n<url>\n<loc>https://www.sportsplaya.com/google/login</loc>\n<lastmod>2016-09-14</lastmod>\n<changefreq>daily</changefreq>\n<priority>0.9</priority>\n</url>\n<url>\n<loc>https://www.sportsplaya.com/content/termsAndConditions</loc>\n<lastmod>2016-09-14</lastmod>\n<changefreq>daily</changefreq>\n<priority>0.9</priority>\n</url>\n<url>\n<loc>https://www.sportsplaya.com/content/faq</loc>\n<lastmod>2016-09-14</lastmod>\n<changefreq>daily</changefreq>\n<priority>0.9</priority>\n</url>\n<url>\n<loc>https://www.sportsplaya.com/content/privacy</loc>\n<lastmod>2016-09-14</lastmod>\n<changefreq>daily</changefreq>\n<priority>0.9</priority>\n</url>\n<url>\n<loc>https://www.sportsplaya.com/blog</loc>\n<lastmod>2016-09-14</lastmod>\n<changefreq>daily</changefreq>\n<priority>0.8</priority>\n</url>\n<url>\n<loc>https://www.sportsplaya.com/content/whysportsplaya</loc>\n<lastmod>2016-09-14</lastmod>\n<changefreq>daily</changefreq>\n<priority>0.8</priority>\n</url>\n<url>\n<loc>https://www.sportsplaya.com/content/contactus</loc>\n<lastmod>2016-09-14</lastmod>\n<changefreq>daily</changefreq>\n<priority>0.8</priority>\n</url>\n";

## Generate Users URL's
/*****************************/
$users=$usersObj->getAllActiveUserDetails();
$date = date('Y-m-d');
foreach($users as $user)
 {
 	if(!in_array(SITE_URL.'/'.$user['username'], $urlArr))
 	{
	if($user['regDate'] == '0000-00-00' || trim($user['regDate'])=="")
	{
	$show_date = $date;
	}
	else
	{
	$show_date = $user['regDate'];
	
	}
	$urlData[]="\n<url>\n<loc>".SITE_URL.'/'.$user['username']."</loc>\n<lastmod>".$show_date."</lastmod>\n<changefreq>daily</changefreq>\n<priority>0.8</priority>\n</url>\n";
    }
 }

## Generate Events URL's
/*****************************/
$eventsData=$eventsObj->getAllEventsList();
foreach ($eventsData as $event)
{
	$event['event_address']=str_replace("&", "", $event['event_address']);
	$event['event_address']=str_replace(" ", "-", $event['event_address']);
	$event['event_name']=str_replace("&", "", $event['event_name']);
	$event['event_name']=str_replace(" ", "-", $event['event_name']);
    $urlData[]="\n<url>\n<loc>".SITE_URL.'/sports_events/'.$event['event_id'].'/'.$event['event_address'].'/'.$event['event_name']."</loc>\n<lastmod>".date('Y-m-d')."</lastmod>\n<changefreq>daily</changefreq>\n<priority>0.8</priority>\n</url>\n"; 
}

## Events Search URL's
/*****************************/
$eventurlData=$eventsObj->getEventUrls();
foreach ($eventurlData as $searchurl)
{
	$urlData[]="\n<url>\n<loc>".SITE_URL.'/map/map/'.$searchurl['event_address_url']."</loc>\n<lastmod>".date('Y-m-d')."</lastmod>\n<changefreq>daily</changefreq>\n<priority>0.8</priority>\n</url>\n";
}

## Generate Jobs Details URL's
/*****************************/
$jobsArr=$jobsObj->getAllActiveJobsData($limit='',$offset='');
foreach ($jobsArr as $jobsVal)
{
	$jobsName = trim(strtolower(str_replace(" ","_",$jobsVal['job_title'])));

	$urlData[]="\n<url>\n<loc>".SITE_URL.'/jobsdetails/'.$jobsName.'/'.base64_encode($jobsVal['id'])."</loc>\n<lastmod>".date('Y-m-d')."</lastmod>\n<changefreq>daily</changefreq>\n<priority>0.8</priority>\n</url>\n"; 
}

## Generate Jobs URL's
/*****************************/
$urlData[]="\n<url>\n<loc>".SITE_URL.'/jobs/jobs'."</loc>\n<lastmod>".date('Y-m-d')."</lastmod>\n<changefreq>daily</changefreq>\n<priority>0.8</priority>\n</url>\n"; 

## Generate Club Search URL's
/*****************************/
$urlData[]="\n<url>\n<loc>".SITE_URL.'/club/club_search'."</loc>\n<lastmod>".date('Y-m-d')."</lastmod>\n<changefreq>daily</changefreq>\n<priority>0.8</priority>\n</url>\n"; 

## Generate Playa Search URL's
/*****************************/
$urlData[]="\n<url>\n<loc>".SITE_URL.'/search/search'."</loc>\n<lastmod>".date('Y-m-d')."</lastmod>\n<changefreq>daily</changefreq>\n<priority>0.8</priority>\n</url>\n"; 

## Generate Couch URL's
/*****************************/
$urlData[]="\n<url>\n<loc>".SITE_URL.'/content/coach'."</loc>\n<lastmod>".date('Y-m-d')."</lastmod>\n<changefreq>daily</changefreq>\n<priority>0.8</priority>\n</url>\n"; 

## Generate About Us URL's
/*****************************/
$urlData[]="\n<url>\n<loc>".SITE_URL.'/content/aboutus'."</loc>\n<lastmod>".date('Y-m-d')."</lastmod>\n<changefreq>daily</changefreq>\n<priority>0.8</priority>\n</url>\n"; 

## Generate Media URL's
/*****************************/
$urlData[]="\n<url>\n<loc>".SITE_URL.'/content/media'."</loc>\n<lastmod>".date('Y-m-d')."</lastmod>\n<changefreq>daily</changefreq>\n<priority>0.8</priority>\n</url>\n"; 

## Generate FAQ URL's
/*****************************/
$urlData[]="\n<url>\n<loc>".SITE_URL.'/content/faq'."</loc>\n<lastmod>".date('Y-m-d')."</lastmod>\n<changefreq>daily</changefreq>\n<priority>0.8</priority>\n</url>\n"; 

## Generate Contact Us URL's
/*****************************/
$urlData[]="\n<url>\n<loc>".SITE_URL.'/content/contactus'."</loc>\n<lastmod>".date('Y-m-d')."</lastmod>\n<changefreq>daily</changefreq>\n<priority>0.8</priority>\n</url>\n"; 

## Generate How it Works URL's
/*****************************/
$urlData[]="\n<url>\n<loc>".SITE_URL.'/content/howitwork'."</loc>\n<lastmod>".date('Y-m-d')."</lastmod>\n<changefreq>daily</changefreq>\n<priority>0.8</priority>\n</url>\n"; 

## Generate Terms and Condition URL's
/************************************/
$urlData[]="\n<url>\n<loc>".SITE_URL.'/content/termsAndConditions'."</loc>\n<lastmod>".date('Y-m-d')."</lastmod>\n<changefreq>daily</changefreq>\n<priority>0.8</priority>\n</url>\n";

## Generate Privacy URL's
/*****************************/ 
$urlData[]="\n<url>\n<loc>".SITE_URL.'/content/privacy'."</loc>\n<lastmod>".date('Y-m-d')."</lastmod>\n<changefreq>daily</changefreq>\n<priority>0.8</priority>\n</url>\n";

$urlData[]="</urlset>";

file_put_contents('sitemap.xml', $urlData);
echo "New Sitemap created!!.."; 
?>