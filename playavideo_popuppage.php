<?php
require_once 'config/settings.php';
require_once 'model/videos.php';

$page_body_class = 'home_page';
$smarty->assign ('page_body_class', $page_body_class);

$siteurl = SITE_URL.'/';


if($_SESSION['stagUserId']!=''){
    $userProfileId = $_SESSION['stagUserId'];
	$checkLogin =1;
}
else{
	$checkLogin =0;
}

$videosObj = new Model_videos();
$vId = $_REQUEST['vId'];
$videoArray = $videosObj->getPlayaOfDayById($vId);
if($videoArray['video_type']=='vimeo'){
    $videoArray['video_url'] = str_replace("\\","",$videoArray['video_url']);
    $video_url = explode('"', $videoArray['video_url']);
    $videoArray['videourl'] = $video_url[1]."?autoplay=1"; 
}
if($videoArray['video_type']=='youtube'){
    $videoArray['video_url'] = str_replace("\\","",$videoArray['video_url']);
    preg_match('/src="([^"]+)"/', $videoArray['video_url'], $match);
    $url = $match[1];
    $arr = explode("/",$url);
    $url= $arr[count($arr)-1];
    $videoArray['videourl'] ="https://www.youtube.com/embed/".$url."?autoplay=1";
}
$data =0;
if($videoArray['video_type']=='youtubeurl'){
    $data =1;
    $videoArray['video_url'] = str_replace("\\","",$videoArray['video_url']);
    $arr = explode("=",$videoArray['video_url']);
    $url= $arr[count($arr)-1];
    $videoArray['videourl'] = "https://www.youtube.com/embed/".trim($url)."?autoplay=1";    
}
if($videoArray['video_type']=='video'){
    $videoArray['videourl'] = SITE_URL.'/dynamicAssets/videos/'.$videoArray['video_url'].'?autoplay=1';
}
?>
<link rel="stylesheet" type="text/css" href="siteAssets/css/bootstrap.css"/>
<link rel="stylesheet" type="text/css" href="siteAssets/css/bootstrap-responsive.css" />
<link rel="stylesheet" type="text/css" href="siteAssets/css/styles.css"/>
<link rel="stylesheet" type="text/css" href="siteAssets/css/fonts.css"/>
<div class="ajax-text-and-image white-popup-block">
<style>
	.font-bold{font-weight:bold !important;}
	@media (min-width: 1024px){
	html {font-size: 18px !important;}
	}
</style>
<div class="row-fluid">
        <div class="span4">
            <div style="padding:3rem 2em 3rem 4em;">
                <h1 class="p-35 font-segoe text-tan font-bold">Play of<br/>the day!</h1>
                <p class="text-grey p-b18 m-b0"></p>
                <p class="p-20 font-segoe font-bold m-b5"><strong><?php echo stripcslashes($videoArray['title']);?></strong></p>
                <p class="text-grey p-b18 m-b0"><?php echo $videoArray['fname'].' '.$videoArray['lname'];?> (<?php echo $videoArray['age']; ?>)</p>
                <!-- <p  class="p-20 font-segoe font-bold m-b5"><strong>Running</strong></p> -->
                <!-- <p class="text-grey p-b18 m-b0"><?php //echo $videoArray['title'];?></p> -->
                <div style="margin-top: 12%;" id="button-box-area">
                <p>
                <button onclick="top.window.location.href='<?php echo SITE_URL.'/'.$videoArray['username']; ?>'" name="" class="span12 btn text-uc p-20 font-uc bg-grey text-white m-b40">visit profile</button>
                </p>
                </div>
            </div>
        </div>
        <div class="span8">
            <iframe width="100%" height="80%" src="<?php echo $videoArray['videourl'];?>" frameborder="0" class="popup_video" allowfullscreen></iframe>
    </div>
	</div>        
	<div style="clear:both; line-height: 0;"></div>
	
</div>
