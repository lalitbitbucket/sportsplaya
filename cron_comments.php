<?php
/*
* Cron Videos Comments. 
*/
## Include Require Files
/***************************/
require_once '/var/www/html/redesign/config/settings.php';
require_once '/var/www/html/redesign/includes/phpmailer/class.phpmailer.php';
require_once '/var/www/html/redesign/model/email.php';
/***************************/

## Create Objects
/***************************/
$emailObj  = new Model_Email();
$mail 	   = new PHPMailer(true);
$videosObj = new Model_Videos();
/***************************/

## Functions [get Videos users]
/*******************************/
$videoUsersArr = $videosObj->getUploadedVideosUsers();

if (count($videoUsersArr)>0){
foreach ($videoUsersArr as $userdata) {

			/*Get Email Templates*/
			$emailArray = $emailObj->getEmailById(28);
			$to = 'psrajender@gmail.com'; 
			//$to = $userdata['email'];
			$toname = $userdata['fname'].' '.$userdata['lname'];		
			$user_ID = $userdata['userprofile_id'];
			$fromname = $emailArray['fName'];
			$from = $emailArray['fEmail'];
	        
			$subject = $emailArray['emailSub'];
			$subject = str_replace('[SITENAME]', SITENAME, $subject);
			$message = $emailArray['emailContent'];
			$message = str_replace('[SITEURL]', SITE_URL, $message);
			$message = str_replace('[SITENAME]', SITENAME, $message);
            $message = str_replace('[UNAME]', $userdata['fname'].' '.$userdata['lname'], $message);
            $message = str_replace('[userid]', $user_ID , $message);

            $template_msg	= str_replace('[MESSAGE]',$message , $message);
			$template_msg 	= str_replace('[LOGO]','<img src="'.SITE_URL.'/siteAssets/images/newimages/logo.png">', $template_msg);
			$template_msg 	= str_replace('[SITELINK]',SITE_URL , $template_msg);
			$template_msg 	= str_replace('[SUBJECT]',$subject , $template_msg);
			$template_msg	= str_replace('[SITEROOT]',SITE_URL , $template_msg);					
			$template_msg	= str_replace('[UNAME]',UNAME , $template_msg);	
        	$template_msg	= str_replace('[userid]',$user_ID , $template_msg);	

        /*Get Comments User*/    
		$commentUser=$videosObj->getVideoCommentsUsersProfile($userdata['user_id'],$userdata['userprofile_id']);
		
		if (count($commentUser)>0){
		$str ="";
		foreach ($commentUser as $row) {
			
			/*Get Videos Details By VideoId*/
			$videodata = $videosObj->getVideosDetailsByVideoId($row['videoId']);
			
			$album_title  = trim(strtolower(str_replace(" ","_",$videodata['title'])));
			
			if ($row['avatar'] == "") {
				if($row['gender'] != "m")
			   	{
			   		$img_profile = SITE_URL.'/siteAssets/images/female_default_images1.png';
			   	}
			   	else
			   	{
			   		$img_profile = SITE_URL.'/siteAssets/images/male_default_images1.png';
			   	}
			}
			else
			{
				$img_profile =  SITE_URL.'/dynamicAssets/avatar/238x259/'.$row['avatar'];
			}

		$str  .= '<style>@media only screen and (max-device-width: 480px){ tr td img {max-width: 100px !important;height: auto !important;} table {width:90% !important;border:1px #555555 !important;} h1,h3{ text-indent: 0px !important;} b{font-size:16px !important;} td a img{width:100% !important;} p{padding-left:2px !important; width:90% !important;font-size:12px !important;} #follow_mob{font-size:12px !important;padding:5px 10px !important; max-width:60px !important;margin-top: 5px !important;} tr td a img{max-width:80% !important;} #sports_img{width: 15px !important;margin-right: 2px !important;} #follow_mob{display:block !important;} #follow_all{display:none !important;}}'; 

        $str  .= '<style>@media only screen and (min-device-width: 480px) and (max-device-width: 600px) {table {width:90% !important;border:1px #555555 !important;} h1,h3{ text-indent: 0px !important;} b{font-size:16px !important;} td a img{width:50% !important;} p{padding-left:2px !important; width:90% !important;}}</style>';

		$str  .= '<tr width="100%">';
        $str  .= '<td><a href="'.SITE_URL.'/'.$row['username'].'"><img src="'.$img_profile.'" width="100px" height="100px"></a></td>';
        $str  .= '<td style="width:100%"><b style="font-size:20px"><a style="text-decoration: none;color: #316db8;" href="'.SITE_URL.'/'.$row['username'].'">'.$row['fname'].' '.$row['lname'].'</a></b><br>';
        $str  .= '<p style="margin-top:10px;font-family: Trebuchet MS,Arial,sans-serif;font-size: 15px;color: #373232;line-height: 20px;">Comment :<span style="margin-top:2px;font-family: Trebuchet MS,Arial,sans-serif;font-size: 15px;color: #373232;line-height: 20px;">'.substr($row['comment_text'], 0,25).'....'.'</span>
			<a href="'.SITE_URL.'/singlenominess/'.$album_title.'/'.base64_encode($videodata['id']).'" target="_blank" id="follow_all" style="text-decoration: none;text-align:center !important;color: #ffffff;background: #3975C0;background-color: #313D50; border-radius: 8px; font-family: Arial;font-size: 16px; padding: 5px 25px 5px 25px;float: right;">View Video</a></p>
			<p><span style="width: 63%;margin-top:2px;font-family: Trebuchet MS,Arial,sans-serif;font-size: 14px;color: #808080;line-height: 20px;">'.$row['cmtDate'].'</span></p>';
        $str  .= '<td>';
        $str  .='</td></tr>';
        /*Update Cron Status is 1*/
       	
       	// $updateCron = $videosObj->updateCronStatusByCommentId($row['id'], 1);
    }/* End Foreach */

		$template_msg = str_replace('[follow_list]', $str , $template_msg);
		echo $template_msg;
		try
		{
			$mail->AddAddress($to, $toname);
			$mail->SetFrom($from, $fromname);
			$mail->addBCC("lalit.wiseit@gmail.com","SP");
			$mail->Subject = $subject;
			$mail->Body = $template_msg; 
			$mail->Send();
		} 
		catch (phpmailerException $e)
 		{
	    	echo $_SESSION['msg']= $e->errorMessage(); //Pretty error messages from PHPMailer
		}  
		catch (Exception $e)
		{
	    	echo $_SESSION['msg']= $e->getMessage(); //Boring error messages from anything else!
 		}	
	}

}/* End Foreach */
echo 'Comments Successfull Send !!';
}else
{
echo 'Sorry No Comments..';
exit;
}
?>