<?php
require_once 'config/settings.php';

$siteurl = SITE_URL.'/';


if ($_SESSION['stagUserId'] != '') {
    echo '<script>top.window.location.href="'.SITE_URL.'/"</script>';
    exit;
}

$page_body_class = 'home_page';
$smarty->assign ('page_body_class', $page_body_class);

$msg ='';
if ($_POST !='' && $_POST['login']=='Log in') {
    $username  = return_post_value($_POST['uemail']);
    $password  = return_post_value($_POST['upass']);
    
    $emailArray = $usersObj->checkUserEmailPresent($username);
    $userArr = $usersObj->checkUserEmailAddressorusername($username);
    $salt = $emailArray['salt'];
    $getpassword = genenrate_password($userArr['salt'],$password);
    
    $userArray = $usersObj->getUserProfileDetailByUserID($userArr['userId'], $userArr['userType']);
    $username = $userArray['username'];
    $smarty->assign('username', $username); 
    
    if (!empty($userArr) && ($userArr['password'] == $getpassword)) {
        if ($userArr['status'] == '2') {

            $loginArray              = array();
            $loginArray['loginTime'] = date("Y-m-d H:i:s");
            $loginArray['user_id']   = $userArray['id'];
            $loginArray['logininfo'] = implode(",", $geoIPData);
            $loginArray['loginIP']   = $_SERVER['REMOTE_ADDR'];
            $loginArrayId            = $usersObj->addLoginDetailsByValue($loginArray);
            $_SESSION['loginId']     = $loginArrayId;
                
            ## creating login session
            $_SESSION['stagUserId']        = $userArr['userId'];
            $_SESSION['stagUserName']      = $userArray['username'];
            $_SESSION['userType']          = $userArr['userType'];
            $_SESSION['stagUserFullName']  = ucfirst($userArray['fname']) . '' . ($userArray['lname']);
            $_SESSION['stagUserProfileId'] = $userArray['id'];
            $_SESSION['username']          = $userArray['username'];
            $_SESSION['advance_search']    = $userArray['advance_search'];

            
            setcookie('userid',$userArray['id'],time()+3600,'/');
            
            if(isset($_REQUEST['uId']) && !empty($_REQUEST['uId'])){
                header('location:'.SITE_URL.'/nominee_popuppage.php?uId='.$_REQUEST['uId']); 
                exit;    
            }
            elseif(isset($_REQUEST['VideoId']) && !empty($_REQUEST['VideoId'])){
              echo'<script>top.window.location.href="'.SITE_URL.'/nominees/singlenominess?vId='.$_REQUEST['VideoId'].'"</script>';
                exit;    
            }                
            else{
                echo '<script>window.parent.location.reload();</script>';
                exit;
            }
                
        }
        else {
            $msg = 'Your account is inactive please contact to site admin.';
        }
    }
    else{
        $msg = 'Error : Invalid username  and/or password.';
    }
}
?>
<link rel="stylesheet" type="text/css" href="siteAssets/responsive/css/bootstrap.css" />
<link rel="stylesheet" type="text/css" href="siteAssets/css/bootstrap-responsive.css" />
<link rel="stylesheet" type="text/css" href="siteAssets/css/styles.css" />
<link rel="stylesheet" type="text/css" href="siteAssets/css/fonts.css" />
<link rel="stylesheet" type="text/css" href="siteAssets/responsive/css/mobile.css" />
<style>
@media (min-width: 1024px){
html {font-size: 18px !important;}
}
@media (max-width: 767px) {
body {padding-right: 0 !important;padding-left: 0 !important;}
.mfp-iframe-holder{padding-top:0 !important;padding-bottom:0 !important;}
.mfp-container{padding:0 !important;}
.login_popup h1{margin-top:0;}
}
#custom-content img {max-width: 100%;margin-bottom: 10px;}

@media only screen and (max-width: 472px) {
.googlebtn {
    width: 11rem !important;
    margin-left: 15% !important;
    height: 48px !important;
    margin-top: 0rem !important;
    margin-right: 15%;
    }
}
.googlebtn {width: 13rem;margin-top: -2rem;height: 48px;}
</style>

<div class="white-popup-block login_popup">
    <div id="custom-content" class="p-t65" style="max-width:600px; margin:0 auto;">
        <div class="text-center text-brickRed p-14">
            <p class="p-18 font-bold">&#10006;</p><p>You are not Logged in</p>
        </div>
        <div class="text-center">
            <?php if($msg!=''){echo '<p class="alert bg-transparent p-14 text-brickRed">'.$msg.'</p>';}?>
            <?php if(isset($_REQUEST['uId'])){?>
            <h1 class="p-70 p-b27 m-b0">Log in to vote</h1>
            <?php }else{?>
            <h1 class="p-70 p-b27 m-b0">Log in</h1>
            <?php }?>
        </div>
        <form method="post" name="frmLogin">
            <div class="row">
                <div class="col-xs-10 col-xs-push-1 col-md-5  col-md-push-0 p-b18">
                    <input type="email" name="uemail" id="uemail" class="col-xs-12 p-18 p-tb18 text-dgrey" 
                    placeholder="Email" 
                    value="<?php if(isset($_POST['uemail'])){echo $_POST['uemail'];}?>">
                </div>
                <div class="col-xs-10 col-xs-push-1 col-md-5  col-md-push-0 p-b18">
                    <input type="password" name="upass" id="upass"  class="col-xs-12 p-18 p-tb18 text-dgrey" 
                    placeholder="Password" value="">
                </div>
                <div class="col-xs-10 col-xs-push-1 col-md-2  col-md-push-0 p-b18">
                    <input type="submit" name="login" id="login" 
                    class="col-xs-12 btn bg-tan p-lr9 font-uc p-18 text-white" 
                    value="Log in">
                </div>
                <div class="col-xs-12 col-md-6 m-l0">
                <!-- <span class="pull-left alert bg-transparent p-14 text-brickRed no-border">Invalid Username and/or password</span> -->
                </div>
                <div class="col-xs-12 col-md-6 m-l0 text-right text-small-center">
                <a href="forgotpasswordpopup.php" class="text-grey p-14 m-t9">forgot your password?</a>
                </div>
            </div>
        </form>
        <div class="marginbot_10 text-grey text-center p-tb18">
            Or sign in with:
        </div>
        <center> 
        <fb:login-button scope="public_profile,email" onlogin="checkLoginState();" id="TEST" 
                        size="xlarge">
        </fb:login-button>
        <a href="javascript:void(0);" onclick="window.parent.location = '<?php echo SITE_URL;?>/google/login'">
        <img src="<?php echo SITE_URL;?>/siteAssets/images/google_button.png" alt="SportsPlaya Google Login" title="Google Login" class="googlebtn">
        </a>

        </center>
        <div class=" text-grey" style="text-align: center;padding-bottom: 27px;">
        <p class="p-b27">Don't have an account yet?</p>
        <button onclick="window.location.href='register_popuppage.php?uId=<?php echo $_REQUEST['uId']; ?>'" class=" btn bg-grey p-lr9 font-uc p-18 text-white">REGISTER</button>
        </div>
    </div>
</div>
<script type="text/javascript">
function checkLoginState() {
    FB.getLoginStatus(function(response) {
      statusChangeCallback(response);
    });
}

window.fbAsyncInit = function() {
    FB.init({
        appId      : '1538410359534398',
        cookie     : true,  // enable cookies to allow the server to access 
                            // the session
        xfbml      : true,  // parse social plugins on this page
        version    : 'v2.8' // use graph api version 2.8
    });
};

// Load the SDK asynchronously
(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

function statusChangeCallback(response) {
   console.log('statusChangeCallback');
   console.log(response);
   // The response object is returned with a status field that lets the
   // app know the current login status of the person.
   // Full docs on the response object can be found in the documentation
   // for FB.getLoginStatus().
   if (response.status === 'connected') {
     // Logged into your app and Facebook.
     testAPI();
   } else {
     // The person is not logged into your app or we are unable to tell.
     document.getElementById('status').innerHTML = 'Please log ' +
       'into this app.';
   }
 }
  
function testAPI() {
    console.log('Welcome!  Fetching your information.... ');
    FB.api('/me?fields=id,first_name,last_name,email,gender,locale,picture', function(response) {
      console.log('Successful login for: ' + response.email);
    
    window.parent.location = '/facebook/login?fb_id='+response.id+'&email='+response.email+'&first_name='+response.first_name+'&last_name='+response.last_name+'&gender='+response.gender;
});

}
</script>