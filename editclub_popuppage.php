<?php
## include required files
/*******************************/
require_once 'config/settings.php';

isLogin();

require_once 'model/common/image_functions.php';

if (isset($_POST['editclub'])) 
{
   if ($_POST['clubName'] != '' && $_POST['aboutClub'] != '') {
        
        $clubDetailsArray = array();

        if ($_FILES['clubTimelineImage']['name'] != '') {
        
                $size        = getimagesize($_FILES['clubTimelineImage']['tmp_name']);
                $imageWidth  = '638';
                $imageHeight = '286';
            
                $imageName   = $_FILES['clubTimelineImage']['name'];
                $fileName    = date('Ymdhis') . "." . $imageName;
                $temp        = $_FILES["clubTimelineImage"]["tmp_name"];
                $fileName    = time() . "_" . trim(str_replace(" ", "_", $_FILES['clubTimelineImage']['name']));
                
                $imageFolder = SITE_URL.'/dynamicAssets/clubTimelineImages/';
                $imageName   = $fileName;
                
                $thumbFolder1 = SITE_URL."/dynamicAssets/clubTimelineImages/638x286/";
                $height1      = 286;
                $width1       = 638;
                $file1        = uploadImageAndCreateThumb($temp, $imageName, $imageFolder, $thumbFolder1, $height1, $width1);
                
                $thumbFolder2 = SITE_URL."/dynamicAssets/clubTimelineImages/1170x411/";
                $height2      = 411;
                $width2       = 1170;
                $file2        = uploadImageAndCreateThumb($temp, $imageName, $imageFolder, $thumbFolder2, $height2, $width2);
                
                $clubDetailsArray['clubTimelineImage'] = $imageName;
                unlink('/dynamicAssets/clubTimelineImages/' . $clubDetails['clubTimelineImage']);
                unlink('/dynamicAssets/clubTimelineImages/638x286/' . $clubDetails['clubTimelineImage']);
                unlink('/dynamicAssets/clubTimelineImages/1170x411/' . $clubDetails['clubTimelineImage']);
        }

        $clubDetailsArray['club_sport']   = return_post_value($_POST['club_sport']);
        $clubDetailsArray['clubName']     = return_post_value($_POST['clubName']);
        $clubDetailsArray['aboutClub']    = return_post_value($_POST['aboutClub']);
        $clubDetailsArray['headQuarters'] = return_post_value($_POST['headQuarters']);
        $clubDetailsArray['website']      = return_post_value($_POST['website']);
        $clubDetailsArray['type']         = return_post_value($_POST['type']);
        $clubDetailsArray['founded']      = return_post_value($_POST['founded']);
        $clubDetailsArray['industry']     = return_post_value($_POST['industry']);
        $clubDetailsArray['size']         = return_post_value($_POST['size']);
        $clubDetailsArray['userID']         = return_post_value($_POST['userID']);
        $clubDetailsArray['addedDate']    = date("Y-m-d");


        $countBioArray = $usersObj->checkClubInfoExists($otheruserDetails['id']);
        if($countBioArray<='0')
        {   
            $usersObj->addClubDetails($clubDetailsArray);
        }
        else{
            $usersObj->editClubDetailsByUserId($clubDetailsArray,$otheruserDetails['id']);
        }
        echo '<script>top.window.location.href="'.SITE_URL.'/controller/users/viewprofile.php"</script>';
        exit;
    } 
}

## Get All Active Sports
$sports  = $sportsObj->getAllActiveSports();
$clubDetails = $usersObj->getClubDetailByUserID($otheruserDetails['id']);
?>
<link rel="stylesheet" type="text/css" href="<?php echo SITE_URL; ?>/siteAssets/responsive/css/bootstrap.css" />
<link rel="stylesheet" type="text/css" href="<?php echo SITE_URL; ?>/siteAssets/css/styles.css" />
<link rel="stylesheet" type="text/css" href="<?php echo SITE_URL; ?>/siteAssets/css/fonts.css" />
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<style>
	@media (min-width: 1024px){
		html {
			font-size: 18px !important;
		}
	}
	.white-popup-block{background:#fff url(<?php echo SITE_URL; ?>/siteAssets/images/newimages/bg_register.png) center top no-repeat;	
	background-size:100%;}
    .span5{
            margin-bottom: 2%;
    }
	#custom-content img {max-width: 100%;margin-bottom: 10px;}
</style>


<div class="white-popup-block">
    <div id="custom-content" class="p-t27" style="max-width:100%; padding:3rem 3rem 1rem;">
        
        <div>
            <h1 class="text-dgrey font-segoe font-bold p-74 font-uc m-tb0 m-b40">Edit Club</h1>
        </div>

        <?php if($flag==1) { ?>
            <div class="" style="padding-bottom: 10%;padding-top: 12%;">
                <?php if(isset($_REQUEST['uId']) && !empty($_REQUEST['uId'])){?>

                    <button class="btn bg-grey p-lr9 font-uc p-18 text-white" onclick="window.location.href='<?php echo SITE_URL.'/nominee_popuppage.php?uId='.$_REQUEST['uId'];?>'">CLICK FOR VOTING</button>&nbsp;&nbsp;
                    <button class="btn bg-grey p-lr9 font-uc p-18 text-white" onclick="top.window.location.href='<?php echo SITE_URL;?>/users/viewprofile'">GO YOUR PROFILE</button>
                <?php }else{?>
                    <button class="btn bg-grey p-lr9 font-uc p-18 text-white" onclick="top.window.location.href='<?php echo SITE_URL;?>/users/viewprofile'">GO YOUR PROFILE</button>
                <?php }?>
            </div>
        
        <?php } else { ?>
       <form name="frmAction" id="frmAction" method="post" enctype="multipart/form-data">
       <div class="row">
       	<div class="col-xs-12 col-md-6">
        <div class="row">
            <!-- <div class="span12"><a href="#" class="text-dgrey"></a></div> -->
            <div class="col-xs-12 col-md-12">
                <input type="text" name="clubName"  id="clubName" class="col-xs-12 col-md-8 p-18 p-tb9 m-b18 text-dgrey" 
                placeholder="Club Name" required ="required" value="<?php echo $clubDetails['clubName']; ?>">
            </div>

            <div class="col-xs-12 col-md-12">
                <textarea class="textarea col-xs-12 col-md-12 p-18 p-tb9 m-b18 text-dgrey" 
                placeholder="About Club" name="aboutClub" id="aboutClub"><?php echo $clubDetails['aboutClub']; ?></textarea>
            </div>

            <div class="col-xs-12 col-md-4">
                <input type="text" name="industry"  id="industry" class="col-xs-12 col-md-12 p-18 p-tb9 m-b18 text-dgrey" placeholder="Industry" value="<?php echo $clubDetails['industry']; ?>">
            </div>
            
            <div class="col-xs-12 col-md-4">
                <input type="text" name="size"  id="size" class="col-xs-12 col-md-12 p-18 p-tb9 m-b18 text-dgrey" 
                required="required" placeholder="Number Of Employee" value="<?php echo $clubDetails['size']; ?>">
            </div>

            <div class="col-xs-12 col-md-4">
                <input type="text" name="website"  id="website" value="<?php echo $clubDetails['website']; ?>" class="col-xs-12 col-md-12 p-18 p-tb9 m-b18 text-dgrey" placeholder="Websites" required="required">
            </div>

            <div class="col-xs-12 col-md-12">
                <textarea name="headQuarters" id="headQuarters" class="textarea col-xs-12 col-md-12 p-18 p-tb9 m-b18 text-dgrey" placeholder="Head Quarters"><?php echo $clubDetails['headQuarters']; ?></textarea>
            </div>

            <div class="col-xs-12 col-md-4">
                <select name="club_sport" id="club_sport" required="required" class="col-xs-12 col-md-12 selectpicker p-tb9 m-b18"  style="background-position:97% 50%;">
                    <option value="0">Select Sports</option>
                    <?php foreach ($sports as $sp) {?>
                        <option value="<?php echo $sp['id']; ?>"><?php echo $sp['sportsTitle']; ?></option>
                    <?php }?>
                </select>
            </div>
            <div class="col-xs-12 col-md-4">
                <input type="text" name="founded"  id="founded" placeholder="Founded" value="<?php echo $clubDetails['founded']; ?>" class="col-xs-12 col-md-12 p-18 p-tb9 m-b18 text-dgrey" readonly="readonly"/>
            </div>

            <div class="col-xs-12 col-md-4">
                    <select name="type" id="type" class="select selectpicker col-xs-12 col-md-12 m-b18">
                        <option value="0">Select Club Type</option>
                        <option value="p">Profit</option>
                        <option value="n">Non Profit</option>
                    </select>
            </div>

            <div class="col-xs-12 col-md-6">
               <input type="file" name="clubTimelineImage"  id="clubTimelineImage" class="jd_upload_file col-xs-12 col-md-8 input"/>
            </div>
            <input type="hidden" name="userID" value="<?php echo $otheruserDetails['id'];?>">
            <div class="col-xs-12 col-md-6 text-center">
                <input type="submit" name="editclub" id="editclub" class="col-xs-12 col-md-6 btn bg-grey p-lr9 font-uc p-18 text-white pull-right" value="Edit Club">
            </div>
        </div>
        </div>
       </div>
        </form>
       <?php }?>
    </div>
    </div>

</div>
<script src="/siteAssets/js/timepicker/jquery-1.9.1.js"></script>
<script src="/siteAssets/js/timepicker/jquery-1.10.1-ui.js"></script>
<script type="text/javascript">

document.getElementById('club_sport').value='<?php echo $clubDetails["club_sport"];?>'; 
document.getElementById('type').value='<?php echo $clubDetails["type"];?>'; 

$(function(){
$('#founded').datepicker({
    dateFormat: 'yy-mm-dd',
    maxDate : new Date(),
});
}); 
</script>
