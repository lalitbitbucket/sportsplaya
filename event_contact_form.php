<?php
require_once 'config/settings.php';
require_once 'includes/phpmailer/class.phpmailer.php';
require_once 'model/email.php';
require_once 'model/events.php';
require_once 'model/message.php';

$emailObj  = new Model_Email();
$mail 	   = new PHPMailer(true);
$eventsObj = new Model_EVENTS();
$messageObj= new Model_Messages();

$eventID = $_GET['evtid'];
$eventData=$eventsObj->getEventDetailsById($eventID);
$userID  = $eventData['userId'];

$receiverData=$usersObj->getUserDetailsInfoByUserId($userID);

$logged_user_ID=$userInfo['id'];
$receiverID=$receiverData['id'];

if(isset($_POST['submit_query']) && $_POST['submit_query']!='')
{
          $messageArray = array(
		    'senderId'   => $logged_user_ID,
		    'recieverId' => $receiverID,
		    'subject'    => $userInfo['fname'].' '.$userInfo['lname'].' Has contacted You For event name '.$eventData['event_name'],
		    'message'    => $userInfo['fname'].' '.$userInfo['lname'].' Has contacted You For event name '.$eventData['event_name'].'<br>His message is '.trim($_POST['comment']),
		    'date'       => date('Y-m-d H:i:s'),
			);

	    $insert_message = $messageObj->sendMessageById($messageArray);

        $emailArray = $emailObj->getEmailById(22);
		
		$to       = $receiverData['email'];
		$toname   = $receiverData['fname'].' '.$receiverData['lname'];		
		$fromname = trim($_POST['cname']);
		$from     = $emailArray['fEmail'];
        
        $subject = 'Sports Playa: You have got a new message for your event - '. $eventData['event_name'];
		$subject = str_replace('[SITENAME]', SITENAME, $subject);
	    
	    $str  .= '<style>@media only screen and (max-device-width: 480px){ tr td img {max-width: 100px !important;height: auto !important;} table {width:90% !important;border:1px #555555 !important;} h1,h3{ text-indent: 0px !important;} b{font-size:16px !important;} td a img{width:100% !important;} p{padding-left:2px !important; width:90% !important;font-size:12px !important;} #follow_mob{font-size:12px !important;padding:5px 10px !important; max-width:60px !important;margin-top: 5px !important;} tr td a img{max-width:80% !important;} #sports_img{width: 15px !important;margin-right: 2px !important;} #follow_mob{display:block !important;} #follow_all{display:none !important;}}'; 
	    $str  .= '<style>@media only screen and (min-device-width: 480px) and (max-device-width: 600px) {table {width:90% !important;border:1px #555555 !important;} h1,h3{ text-indent: 0px !important;} b{font-size:16px !important;} td a img{width:50% !important;} p{padding-left:2px !important; width:90% !important;}}</style>';
		$str  .= '<tr width="100%"><td>';
	    $str  .= '<b style="color:#3975C0"> To view his message <a href="'.SITE_URL.'">Click Here</a></b><br>';
	    
	    $str  .='</td></tr>';

		$message = $emailArray['emailContent'];
		$message = str_replace('[SITEURL]', SITE_URL, $message);
		$message = str_replace('[SITENAME]', SITENAME, $message);
	    $message = str_replace('[UNAME]', $toname, $message);
	    $message = str_replace('[SENDERNAME]', trim($_POST['cname']), $message);
	    $message = str_replace('[EVENTNAME]', $eventData['event_name'], $message);
	    $message = str_replace('[LOGO]','<img src="'.SITE_URL.'/siteAssets/images/newimages/logo.png">', $message);
        $message = str_replace('[Event_contact_mail]', $str , $message);

		$template_msg	= str_replace('[MESSAGE]',$message , $message);
		$template_msg 	= str_replace('[LOGO]','<img src="'.SITE_URL.'/siteAssets/images/newimages/logo.png">', $template_msg);
		$template_msg 	= str_replace('[PASSWORD]',$_SESSION['mail_psswd'], $template_msg);
		$template_msg 	= str_replace('[SITELINK]',SITE_URL , $template_msg);
		$template_msg 	= str_replace('[SUBJECT]',$subject , $template_msg);
		$template_msg	= str_replace('[SITEROOT]',SITE_URL , $template_msg);					
		$template_msg	= str_replace('[UNAME]', $toname, $template_msg);
		$template_msg   = str_replace('[SENDERNAME]', trim($_POST['cname']), $template_msg);	
		$template_msg   = str_replace('[EVENTNAME]', $eventData['event_name'], $template_msg);
		$template_msg	= str_replace('[Event_contact_mail]',$str , $template_msg);
	    
	    //echo $template_msg;
	     try
	      {
			$mail->AddAddress($to, $toname);
			$mail->SetFrom($from, $fromname);
			//$mail->addBCC("patidar.shivam153@gmail.com","Shivam Patidar");
			$mail->Subject = $subject;
			$mail->Body = $template_msg; 
			//$mail->Send();
			echo "<b style='color:#3975C0'>Thank You For Your Submission.</b>";
		    $msg=1;
		} 
		catch (phpmailerException $e)
		 {
		    echo $_SESSION['msg']= $e->errorMessage(); //Pretty error messages from PHPMailer
		 }  
		catch (Exception $e)
		 {
		    echo $_SESSION['msg']= $e->getMessage(); //Boring error messages from anything else!
		 }
}

?>
<style>
@media screen and (max-width: 600px){table tr td{display:block !important;float: left !important;width:100% !important;} table td { padding:3px !important; } input[type="text"],input[type="email"]{width:100% !important;} textarea{width:100% !important;}}
.white-popup-block{background-color: #fff !important;}
table td { padding:10px}
input[type="text"],input[type="email"]{
color:#4B5660;	
height:45px;
border:2px solid #999999;
border-radius: 6px;
padding:10px;
font-size:16px;
font-weight: 500;
width:60%;
}
textarea{
color:#4B5660;	
border:2px solid #999999;
border-radius: 6px;
padding:10px;
font-size:16px;
font-weight: 500;
width:60%;
}
.btn
{
  text-transform:uppercase;
  background: #767B87;
  display: inline-block;
  margin-bottom: 0;
  line-height: 1rem;
  color: #fff;
  text-align: center;
  vertical-align: middle;
  cursor: pointer;
  border: 0 solid #fff;
  padding: 1rem 1.6rem;
  white-space: nowrap;

}
.btn:hover, .btn:focus{
  background:#767B87;
  color:#000;
}
b{color:#555555;}
</style>
<script src="jquery-3.1.1.min.js"></script>
<script type="text/javascript">
var msg='<?php echo $msg; ?>';
if(msg==1)
{
  parent.$.colorbox.close();
}

function isNumberKey(evt) {
	var charCode = (evt.which) ? evt.which : evt.keyCode;
	if ( charCode == 190)
	{
	  return true;
	}
	if (charCode > 31 && (charCode < 48 || charCode > 57))
	{
	  return false;
	}
	return true;
}
</script>
<?php if($msg<1) { ?> 

<form method="post" action="" class="white-popup-block">
 <table align="center" width="100%" id="event_contact_tbl">
	<tr>
	   <td><b>Name</b></td>
	   <td><input type="text" name="cname" id="cname" value="<?php echo $userInfo['fname'].' '.$userInfo['lname'];?>" readonly/></td>
	</tr>
	<tr>
	    <td><b>Email</b></td>
	    <td><input type="email" name="email" id="email" value="<?php echo $userInfo['email']; ?>" readonly></td>
	</tr>
    <tr>
	    <td><b>Phone Number</b></td>
	    <td><input type="text" name="phone" id="phone" value="" onkeypress="return isNumberKey(event);" required></td>
	</tr>
    <tr>
	    <td><b>Comments</b></td>
	    <td><textarea id="comment" name="comment" rows="6" cols="40" required></textarea></td>
	</tr>
    <tr><td colspan="5" align="center"><input type="submit" name="submit_query" id="submit_query" value="Send Contact" class="btn text-uc p-20 font-uc bg-grey text-white"></td>
    </tr>
  </table>
</form>
<?php } ?>