<?php
## include required files
/*******************************/
require_once 'config/settings.php';
require_once 'model/email.php';
require_once 'includes/phpmailer/class.phpmailer.php';

## Create Objects
/*******************************/
$emailObj = new Model_Email();
$mail     = new PHPMailer(true);
/*******************************/

$page_body_class = '';
$smarty->assign ('page_body_class', $page_body_class);


$flag=0;

if (count($_POST) > 0) {
    ##apply php validation
    if (trim($_POST['email']) != '') {
        $email = return_post_value($_POST['email']);
        ## checking for user already exsist 
        /*******************************/
        $userEmailArr = $usersObj->checkUserEmailAddressorusername($email);
        /*******************************/
        if (!empty($userEmailArr)) {
           $userDetails             = $usersObj->getUserProfileDetailByUserID($userEmailArr['userId'], $userEmailArr['userType']);
            $fname                   = $userDetails['fname'];
            $lname                   = $userDetails['lname'];
            ## update user email expired link
            $userUrlArray            = array();
            $userUrlArray['uID']     = $userEmailArr['userId'];
            $userUrlArray['urlType'] = 'forgot';
            $userUrlArray['reqDate'] = date('Y-m-d');
            $userUrlArray['status']  = '1';
            $userUrlID               = $usersObj->addUserUrlDetailsByValue($userUrlArray);
            ## Fetch email content
            $emailArray = $emailObj->getEmailById(1);
            $subject   = $emailArray['emailSub'];
            $message   = $emailArray['emailContent'];
            ## Create message                
            $reseturl1 = SITE_URL . '/users/resetpassword?id=' . base64_encode($userEmailArr['userId']) . '&logtype=forgot&logid=' . base64_encode($userUrlID);
            $reseturl2 = '<a href="' . SITE_URL . "/users/resetpassword?id=" . base64_encode($userEmailArr['userId']) . '&logtype=forgot&logid=' . base64_encode($userUrlID) . '">' . SITE_URL . "/users/resetpassword?id=" . base64_encode($userEmailArr['userId']) . '&logtype=forgot&logid=' . base64_encode($userUrlID) . '</a>';
            $subject   = str_replace('[SITENAME]', SITENAME, $subject);
            $message   = str_replace('[NAME]', ucfirst($fname), $message);
            $message   = str_replace('[SITENAME]', SITENAME, $message);
            $message   = str_replace('[SITE_LINK]', SITE_URL, $message);
            $message   = str_replace('[ACTIVATIONURL]', $reseturl2, $message);
            $message   = str_replace('[ACTIVATIONURL1]', $reseturl1, $message);
            $message   = str_replace('[SITE_URL]', SITE_URL, $message);
            $message   = str_replace('[COPYRIGHT]', COPYRIGHT, $message);
            $message   = str_replace('[SITEDOMAIN]', SITEDOMAIN, $message);
            
            $template_msg = str_replace('[EMAIL]', $userDetails['email'], $message);
            $template_msg = str_replace('[PASSWORD]', $pass, $template_msg);
            $template_msg = str_replace('[NAME]', $userDetails['fname'], $template_msg);
            $template_msg = str_replace('[SITENAME]', SITENAME, $template_msg);
            $template_msg = str_replace('[LOGO]', '<img src="' . SITE_URL . '/siteAssets/images/newimages/logo.png">', $template_msg);
            $template_msg = str_replace('[MESSAGE]', $message, $template_msg);
            $template_msg = str_replace('[SITELINK]', SITE_URL, $template_msg);
            $template_msg = str_replace('[SUBJECT]', $subject, $template_msg);
            $template_msg = str_replace('[SITEROOT]', SITE_URL, $template_msg);
            
            $to       = $userEmailArr['email'];
            $toname   = $fname . ' ' . $lname;
            $from     = $emailArray['fEmail'];
            $fromname = $emailArray['fName'];

            // echo $subject."<br />";
            // echo $from."<br />";
            // echo $fromname."<br />";
            // echo $to."<br />";
            // echo "<pre>"; print_r($template_msg);

            # get email back up    
            $backemailArray            = array();
            $backemailArray['sendto']  = $to;
            $backemailArray['subject'] = $subject;
            $backemailArray['content'] = $message;
            $backemailArray['date']    = date("Y-m-d H:i:s");
            try {
                
                $mail->AddAddress($to, $toname);
                $mail->SetFrom($from, $fromname);
                $mail->Subject = $subject;
                $mail->Body    = $template_msg;
                $mail->Send();
                
                $flag =1;    
                $backemailArray['status'] = '2';
                $succ_msg  = 'Request send successfully. Please check your inbox to reset password.';
            }
            catch (phpmailerException $e) {
                $backemailArray['status'] = '1';
                $msg     = "<div class='warning'>" . $e->errorMessage() . "</div>"; //Pretty error messages from PHPMailer
            }
            catch (Exception $e) {
                $backemailArray['status'] = '1';
                $msg          = "<div class='warning'>" . $e->getMessage() . "</div>"; //Boring error messages from anything else!
            }
            $emailObj->getEmailBackup($backemailArray);
            $flag =1;    
            $succ_msg  = 'Request send successfully. Please check your inbox to reset password.';
            //header('location:' . SITE_URL . '/users/forgotpassword');
            //exit();
        }
         else {
            $msg = 'Email address OR username not exists.';
        }
    } else {
        $msg = 'Please enter email address and confirm email address.';
    }
}
?>
<link rel="stylesheet" type="text/css" href="siteAssets/css/bootstrap.css" />
<link rel="stylesheet" type="text/css" href="siteAssets/css/bootstrap-responsive.css" />
<link rel="stylesheet" type="text/css" href="siteAssets/css/styles.css" />
<link rel="stylesheet" type="text/css" href="siteAssets/css/fonts.css" />
<style>
    @media (min-width: 1024px){
        html {
            font-size: 18px !important;
        }
    }

    #custom-content img {max-width: 100%;margin-bottom: 10px;}
body {background: url(/) center top no-repeat;background-size:100%;}
</style>

<div class="white-popup-block">
    <div id="custom-content" class="p-tb120" style="max-width:600px; margin:0 auto;padding: 7% 0 7% 0;">
        <div class="text-center text-brickRed p-14">
            <!-- <p>&#10006;</p><p>You are not Logged in</p> -->
        </div>
        <div class="text-center">
            <?php if($msg!=''){echo '<p class="alert">'.$msg.'</p>';}?>
            <?php if($flag==1){echo '<p class="alert">'.$succ_msg.'</p>';}else{?>
            <h1 class="p-70 p-b40 m-b0">Forgot Password</h1>
            <?php }?>
        </div>
    
    <?php if($flag==0){?>
        <form name="frmForgot" id="frmForgot" action="" method="post">
            <div class="row-fluid text-center">
                <div class="span2">&nbsp;
                </div>
                <div class="span6">
                    <input type="email" class="span12 p-18 p-tb27 text-dgrey" name="email" id="email" placeholder="Email" 
                        value="<?php if(isset($_POST['email'])){echo $_POST['email'];}?>" required='required'>
                </div>
                <div class="span2">
                    <input class="span12 btn bg-tan p-lr9 font-uc p-18 text-white" type="submit" name="submit" value="Submit">
                </div>
            </div>
       </form>
    <?php }?>
    </div>
</div>