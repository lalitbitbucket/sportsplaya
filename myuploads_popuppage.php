<?php
$nominee_page_type = 1;
require_once 'config/settings.php';

$siteurl = SITE_URL.'/';

$videosObj = new Model_videos();
$uID = $_REQUEST['uId'];

$getComments = $videosObj->getCommentsByNomineesId('4', $uID);
$videoArray = $videosObj->getVideoByVideoId($uID);
$votingDtl = $videosObj->getVotingCountByUserId($userProfileId,$uID);
$total_voting = $videosObj->getVotingPercentByVideoId($uID);
$getAlbumDtl = $videosObj->getAlbumDetailsByAlbumId($videoArray['albumId']);

if($videoArray['video_type']=='vimeo')
    {
        $videoArray['vcode'] = str_replace("\\","",$videoArray['vcode']);
        $video_url = explode('"', $videoArray['vcode']);
        $videoArray['video_url'] = $video_url[1]."?autoplay=1"; 
    }
    if($videoArray['video_type']=='youtube')
    {
        $videoArray['vcode'] = str_replace("\\","",$videoArray['vcode']);
        preg_match('/src="([^"]+)"/', $videoArray['vcode'], $match);
        $url = $match[1];
        $arr = explode("/",$url);
        $url= $arr[count($arr)-1];
        $videoArray['video_url'] ="https://www.youtube.com/embed/".$url."?autoplay=1";
    }

    $data =0;
if($videoArray['video_type']=='youtubeurl')
	{
        $data =1;
        $url = $videoArray['vccode'];
        $videoArray['vccode'] = str_replace("\\","",$videoArray['vccode']);
        $regex_pattern = "/(youtube.com|youtu.be)\/(watch)?(\?v=)?(\S+)?/";
        //$match;
        if(preg_match($regex_pattern, $url, $match)){
            $image_url ="https://i.ytimg.com/vi/".$match[4]."/mqdefault.jpg"; 
            $videoArray['video_url'] = 'https://www.youtube.com/embed/'. $match[4]."?autoplay=1";
        }
        /*$videoArray['vccode'] = str_replace("\\","",$videoArray['vccode']);
	   	$arr = explode("=",$videoArray['vccode']);
	 	$url= $arr[count($arr)-1];
		$videoArray['video_url'] = "https://www.youtube.com/embed/".trim($url)."?autoplay=1";	*/
	}
if($videoArray['video_type']=='video')
    {   
       $videoArray['video_url'] = SITE_URL.'/dynamicAssets/videos/'.$videoArray['video'].'?autoplay=1';
    }
    $album  = trim(strtolower(str_replace(" ","_",$getAlbumDtl['title'])));
?>
<link rel="stylesheet" type="text/css" href="siteAssets/responsive/css/bootstrap.css" />
<link rel="stylesheet" type="text/css" href="siteAssets/css/bootstrap-responsive.css" />
<link rel="stylesheet" type="text/css" href="siteAssets/css/styles.css" />
<link rel="stylesheet" type="text/css" href="siteAssets/css/fonts.css" />
<link rel="stylesheet" type="text/css" href="siteAssets/css/mobile.css" />
<style>
.font-bold{font-weight:bold !important;}
@media (min-width: 1024px){
html {font-size: 18px !important;}
}

@media (max-width: 767px) {
body {padding-right: 0 !important;padding-left: 0 !important;}
.mfp-iframe-holder{padding-top:0 !important;padding-bottom:0 !important;}
.mfp-container{padding:0 !important;}
.login_popup h1{margin-top:0;}
.video_popup #custom-content{ padding:3rem 1rem 1rem !important;}
.white-popup-block{background:#ffffff;}
}
</style>

<div class="ajax-text-and-image white-popup-block">
<div class="video_popup" >
<div class="col-xs-12 col-md-4">
            <div id="custom-content" style="padding:3rem 2em 3rem 2em;">
                <div class="col-xs-12 text-small-right p-lr0">
                	<p class="p-30 font-segoe font-bold m-b5"><?php echo stripcslashes($getAlbumDtl['title']);?></p>
                    <p class="p-40 font-segoe p-b18 text-grey m-b0"><?php echo $total_voting['voteper'];?>%</p>
                    <p class="text-grey p-b18 m-b0"><?php echo $total_voting['total'];?> Votes</p>
                </div>

                <div class="col-xs-7 col-md-12 p-lr0">
                <p class="text-grey p-b18 m-b0">
                <a onclick="top.window.location.href='<?php echo SITE_URL; ?>/singlenominess/<?php echo $album;?>/<?php echo base64_encode($uID); ?>'" href="javascript:void(0);" class="col-xs-12 col-md-12 btn text-uc p-20 font-uc bg-dgrey text-white m-b0">Add a comment</a>
                </p>

                </div>
                
                <?php if(count($getComments) <= 0){ ?>
                	<p class="text-grey p-b18 m-b0">No Comments. . .</p>
                <?php }else{	
                foreach ($getComments as $comment) { ?>
                <div class="col-xs-7 col-md-12 p-lr0">
                    <p class="p-20 font-segoe font-bold m-b5"><?php echo $comment['fname'].' '.$comment['lname'];?></p>
                    <p><?php echo $comment['date_formated'];?></p>
                    <p class="text-lgrey p-b18 m-b0"><?php echo substr($comment['comment_text'],0,75)." ....";?></p>
                </div>
                <?php } }?>
                <div style="margin-top: 12%;" id="button-box-area">
                </div>
            </div>
        </div>        
        <div class="col-xs-12 col-md-8 p-lr0">
        <iframe width="100%" height="80%" src="<?php echo $videoArray['video_url'];?>" frameborder="0" class="popup_video" allowfullscreen></iframe>
     </div>        
<div style="clear:both; line-height: 0;"></div>
</div>