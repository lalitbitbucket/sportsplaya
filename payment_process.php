<?php

require_once 'config/settings.php';

// Include the paypal library
include_once 'model/paypal.php';
require_once 'model/plan.php';
require_once 'model/payments.php';
// Create an instance of the paypal library

$myPaypal       = new Paypal();
$paymentsObj 	= new Model_Payments();
$amount  	    = 29.99;
$userDet 	    = $usersObj->getProfileDetailsByUserId($_SESSION['stagUserId']);
/*********************************************************************************/
$paymentArray = $paymentsObj->getPaymentsOptionsById(2);
//$smarty->assign('paymentArray', $paymentArray);	

$CURRENCY = $paymentArray[0]['value'];
$PAYPAL_ID = $paymentArray[1]['value'];
$PAYPAL_ENABLE_MODE_TEST =$paymentArray[2]['value'];

$smarty->assign('PAYPAL_ENABLE_MODE_TEST', $PAYPAL_ENABLE_MODE_TEST);
$smarty->assign('PAYPAL_ID', $PAYPAL_ID);
$smarty->assign('CURRENCY', $CURRENCY);


//print_r($PAYPAL_ENABLE_MODE_TEST); print_r("<br>"); 
//print_r($PAYPAL_ID); print_r("<br>"); 
//print_r($CURRENCY); exit;

/*********************************************************************************/
// Specify your paypal email
$myPaypal->addField('business', $PAYPAL_ID);

// Specify the currency
$myPaypal->addField('currency_code', $CURRENCY);
$myPaypal->addField('image_url', SITE_URL."/siteAssets/images/logo2.png");

// Specify the url where paypal will send the user on success/failure
$myPaypal->addField('return', SITE_URL.'/advance_search_success.php');
$myPaypal->addField('cancel_return', SITE_URL);

// Specify the url where paypal will send the IPN
$myPaypal->addField('notify_url', SITE_URL.'/payment/paypal_ipn/');

// Specify the product information
$myPaypal->addField('item_name', "Membership");
$myPaypal->addField('amount', $amount);
$myPaypal->addField('item_number', "1");

// Specify any custom value
$myPaypal->addField('order_id', $_GET['order_id']);

//$myPaypal->addField('address_override',1);
$myPaypal->addField('first_name', $userDet['fname']);
$myPaypal->addField('email', $userDet['email']);
//$myPaypal->addField('address1', $userDet['contactAddress']);
$myPaypal->addField('country', 'US');
$myPaypal->addField('state', 'NY');
$myPaypal->addField('city', 'New York');
// Enable test mode if needed
$paypalEnableMode = PAYPAL_ENABLE_MODE_LIVE; // for live mode
//$paypalEnableMode = $PAYPAL_ENABLE_MODE_TEST; // for test mode
if($paypalEnableMode == 'Test' || $paypalEnableMode == 'test') {
//	$myPaypal->enableTestMode();
}
// Let's start the train!
$myPaypal->submitPayment();
?>