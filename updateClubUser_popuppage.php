<?php
## include required files
/*******************************/
require_once 'config/settings.php';

$siteurl = SITE_URL.'/';

isLogin();

if(isset($_POST['submit']) && $_POST['submit']=='Update'){
	$data = array();
	
	$data['clubName'] = $_POST['clubName'];
	$data['aboutClub'] = $_POST['aboutClub'];
    $data['headQuarters'] = $_POST['headQuarters'];
	$data['website'] = $_POST['website'];
    $data['founded'] = $_POST['founded'];
    $data['industry'] = $_POST['industry'];
    $data['size'] = $_POST['size'];
    $data['userID'] = $_POST['userID'];
    $data['addedDate'] = date("Y-m-d H:i:s");
    
    if($_FILES['clubTimelineImage']['name'] !=''){
  		$uploaddir = SITE_URL."/dynamicAssets/clubTimelineImages/638x286/";   
    	$uploadedfile = basename($_FILES['clubTimelineImage']['name']);
    	$uploadedfile1    = $uploaddir.$uploadedfile;
    	$arrImageName = explode(".",$uploadedfile);
    	$uploadedfilename = time().date('Ymd').".".$arrImageName[1];
    	copy($_FILES['clubTimelineImage']['tmp_name'],"../../dynamicAssets/clubTimelineImages/638x286/".$uploadedfilename);
    	$data['clubTimelineImage']     = $uploadedfilename;  
  	}

    $countBioArray = $usersObj->checkClubInfoExists($_POST['userID']);
    if($countBioArray<='0')
    {
    	$usersObj->addClubDetails($data);
    }
    else{
    	$usersObj->editClubDetailsByUserId($data, $_POST['userID']);
    }
    echo '<script>top.window.location.href="'.SITE_URL.'/controller/users/viewprofile.php"</script>';
	exit;
	
}

if($otheruserDetails['id']!= ''){
	$userDetails = $usersObj->UserProfileDetailByuserID($otheruserDetails['id']);	
    $usersClubInfo = $usersObj->getClubDetailByUserID($otheruserDetails['id']);
}

?>					
<link rel="stylesheet" type="text/css" href="/siteAssets/datepicker/css/paper-grinder.css">
<link rel="stylesheet" type="text/css" href="/siteAssets/css/bootstrap.css" />
<link rel="stylesheet" type="text/css" href="/siteAssets/css/bootstrap-responsive.css" />
<link rel="stylesheet" type="text/css" href="/siteAssets/css/styles.css" />
<link rel="stylesheet" type="text/css" href="/siteAssets/css/fonts.css" />
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<style>
	@media (min-width: 1024px){
		html {
			font-size: 18px !important;
		}
	}
	.white-popup-block{background: url(siteAssets/images/newimages/bg_register.png) center top no-repeat;	
	background-size:100%;}
    .span5{
            margin-bottom: 2%;
    }
	#custom-content img {max-width: 100%;margin-bottom: 10px;}
</style>


<div class="white-popup-block">
    <div id="custom-content" class="p-t27" style="max-width:100%; padding:3rem 3rem 1rem;">
       	<div class="text-center">
            <?php if($msg!=''){echo '<p class="alert bg-transparent p-14 text-brickRed">'.$msg.'</p>';}?>
        </div>
        <div class="span5">&nbsp;</div>
       <form method="post" name="frmUpdate" enctype="multipart/form-data">
       	<div class="row-fluid">
       	<div class="span4">
        <div class="row-fluid">
               
            	<div class="span5 m-l0">
                    <label>Club Name : </label>
                </div>
                
                <div class="span5">
                    <input type="text" name="clubName" id="clubName" class="p-18 p-tb27 text-dgrey" 
                    placeholder="Club Name" value="<?php echo $usersClubInfo['clubName']; ?>" 
                    required="required">
                </div>

                <div class="span5 m-l0">
                    <label>About Club : </label>
                </div>
                
                <div class="span5">
                    <input type="text" name="aboutClub" id="aboutClub" class="p-18 p-tb27 text-dgrey" placeholder="About Club" value="<?php echo $usersClubInfo['aboutClub']; ?>"  required="required">
                </div>

                 <div class="span5 m-l0">
                    <label>Head Quarters:</label>
                </div>
                
                <div class="span5">
                    <input type="text" name="headQuarters" id="headQuarters" class="p-18 p-tb27 text-dgrey" placeholder="Head Quarters" value="<?php echo $usersClubInfo['headQuarters']; ?>">
                </div>

                <div class="span5 m-l0">
                    <label>Website : </label>
                </div>
                
                <div class="span5">
                    <input type="text" name="website" id="website" class="p-18 p-tb27 text-dgrey" placeholder="Websites" value="<?php echo $usersClubInfo['website']; ?>">
                </div>
                
                <div class="span5 m-l0">
                    <label>Founded : </label>
                </div>
                
                <div class="span5">
                    <input type="text" name="founded" id="founded" class="p-18 p-tb27 text-dgrey" placeholder="Founded" value="<?php echo $usersClubInfo['founded']; ?>">
                </div>
                
                <div class="span5 m-l0">
                    <label>Industry : </label>
                </div>
                
                <div class="span5">
                    <input type="text" name="industry" id="industry" class="p-18 p-tb27 text-dgrey" placeholder="Industry" value="<?php echo $usersClubInfo['industry']; ?>">
                </div>

                <div class="span5 m-l0">
                    <label>Number Of Employee(s) : </label>
                </div>
                
                <div class="span5">
                    <input type="text" name="size" id="size" class="p-18 p-tb27 text-dgrey" placeholder="Number Of Employee(s)" value="<?php echo $usersClubInfo['size'];?>" required='required'>
                </div>
                
                <div class="span5 m-l0">
                    <label>Previous Image: </label>
                </div>
                
                <div class="span5">
                    <input type="file" name="clubTimelineImage" id="clubTimelineImage" class="text-dgrey">
                </div>
                <input type="hidden" name="userID" value="<?php echo $otheruserDetails['id'];?>">
                <div class="span12 text-center" style="padding-top: 5%!important;">
                    <input type="submit" name="submit" id="submit" class="span3 btn bg-grey p-lr9 font-uc p-18 text-white" value="Update">
                </div>    
        </div>
       </div>
       </div>
       </form>
    </div>
    </div>
  <script src="/siteAssets/js/jquery.js"></script>
  <script src="/siteAssets/js/jquery-ui.js"></script>
    <script type="text/javascript">

/*$("#city").change(function(){
    if($("#city").val()=='0')
    {
        $("#othercity").show();
    }
    else
    {
        $("#othercity").hide();
    }
});*/


        document.getElementById('country').value='<?php echo $userDetails['country']; ?>';
        document.getElementById('sportsID').value='<?php echo $userDetails['sportsID']; ?>';
        
        jQuery( function() {
            $( "#birthdate" ).datepicker({
            changeMonth: true,
            changeYear: true,
            yearRange: "-100:+0",
            dateFormat: 'yy-mm-dd'
            });
        });

        $('#country').change(function(){
            var cntyId = $('#country').val();
            $.ajax({
                url:"/controller/profile/getCityByCntryId_ajax.php",
                type:'POST',
                data:'countryId='+cntyId,
                success: function(response){
                    // alert(response);
                    if(response!=''){
                        $("#city").append(response);
                    }
                }
            });
        });
    </script>


