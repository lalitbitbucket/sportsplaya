<?php
require_once 'config/settings.php';

// print_r($otheruserDetails);

//isLogin();

$sportsObj = new Model_Sports();
$videosObj = new Model_Videos();
$searchObj = new Model_Search();
$citiObj   = new Model_City();


$limit = $_POST['offset'];
if(isset($_POST['offset'])){
        
   // $sports_id      =    $_POST['sportsId'];    // Search SportsId
   // $change_sport   =    $_POST['change_sport']; 
   // $selectCountry     =   $_POST['selectCountry'];   // Search CityId
     
    $searchResultArray =  $searchObj->searchMoreData('','','',3,$limit);
    
    $change_sport_flag =0 ;
    
    $count_hidden = "";
    $change_sport = 1;
    if($change_sport!=0)
    {   
        $countalbumArr =  $searchObj->searchMoreData('','','','','');
        $total_change_sport_flag  = count($countalbumArr);
        $count_hidden = '<input type="hidden" name="hidden_total" id="hidden_total" value="'.$total_change_sport_flag.'">';
    }
    
    if($count_hidden!="")
    {
       echo $count_hidden;
    }

    $sports_images = $sportsObj->getAllActiveSports();
    $sports_image_arr =array();
    
    foreach($sports_images as $arr_row){
        $sports_image_arr[$arr_row['id']] =$arr_row['sportsTitle'];
    }

    $all_countries = $countryObj->getAllCountry();
    $country_name_arr=array();

    foreach ($all_countries as $all_country) {
        $country_name_arr[$all_country['countryId']]=$all_country['countryName'];
    }
    foreach($searchResultArray as $sportsInfo) 
    {   
        $albumArray = $videosObj->getvideosByUserId($sportsInfo['id']);  /*Count User uploaded Videos*/
        $playaDtl = $videosObj->getPlayaOfDayByUserId($sportsInfo['id']);   /*Count User Playa Videos*/

        $total_playa = $playaDtl['total'];
        $total_videos = count($albumArray);

        $city_ID=$sportsInfo['city'];   /* Check cityID */
        if(trim($city_ID) > 0)
        {
            $city_Array= $citiObj->getCityDetailsByCityId($city_ID);  // Get City Name By CityId;
            $city_name = $city_Array['ctName'];
        }
        else{
            $city_name = 'N/A';   
        }
        
        $country_ID = $sportsInfo['country'];
        if($country_ID > 0)
        {
            $country_name = $country_name_arr[$country_ID];
        }
        else{
            $country_name = 'N/A';   
        }

        $str = "";
        if(trim($sportsInfo['sportsID'])!="")
        {
            $sports_ids = explode(",",$sportsInfo['sportsID']);
            if(count($sports_ids)>0)
            {
                foreach($sports_ids as $sports_id)
                {
                    $str=$sports_image_arr[$sports_id];
                }  
            }
        }
        else{
            $str='N/A';
        }
        $sportnames = $str;
        if ($sportsInfo['avatar'] == "")    /*Check IF Avatar*/
        {   
            if($sportsInfo['sex'] != "m")
            {
                $img_profile = SITE_URL.'/siteAssets/images/female_default_images1.png';
            }
            else
            {
                $img_profile = SITE_URL.'/siteAssets/images/male_default_images1.png';
            }
        }
        else
        {
            $img_profile =  SITE_URL.'/dynamicAssets/avatar/238x259/'.$sportsInfo['avatar'];
        }


        if($sportsInfo['sports_club']!=''){
            $sports_club = $sportsInfo['sports_club'];
        }
        else{
            $sports_club = 'N/A';   
        }
         if($sportsInfo['sports_position']!=''){
            $sports_position = $sportsInfo['sports_position'];
        }
        else{
            $sports_position = 'N/A';   
        }
        echo '<div class="col-md-12 m-l0 p-tb40" style="padding-bottom: 2rem;border-bottom: rgba(158, 158, 158, 0.2) solid 0.22rem;">
                <div class="row">
                    <div class="col-md-3">
                        <a href="'.SITE_URL.'/'.$sportsInfo['username'].'" title="'.$sportsInfo['username'].'">      
                            <img title="'.$sportsInfo['username'].'" alt="'.$sportsInfo['username'].'" src="'.$img_profile.'">
                        </a>
                        <div class="col-xs-12">
                            <button class="btn text-uc p-10 font-uc bg-grey text-white m-b27" style="width: 110%;margin-left: -5%;margin-top: 3%;">
                                VISIT PROFILE</button>
                        </div>
                    </div>  
                        <div class="col-xs-9">
                            <div class="row">
                                    <p class="p-50 text-dgrey p-b18">'.$sportsInfo['fname'].''.$sportsInfo['lname'].'                                   
                                    </p>
                                    <p class="p-22 text-dgrey p-b18">
                                        Melbourne’s very own rising star '.$sportsInfo['fname'].''.$sportsInfo['lname'].' Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt.
                                    </p>
                                    <ul class="play_details p-20">
                                        <li class="col-md-5 m-l0 text-lgrey">
                                            <span class="icon sport"></span>'.$sportnames.'
                                        </li>
                                        <li class="col-md-5 m-l0">
                                            <span class="icon playofDay"></span>'.$total_playa.' x Play of the Day
                                        </li>
                                        <li class="col-md-5 m-l0">
                                            <span class="icon club"></span>'.$sports_club.'
                                        </li>
                                        <li class="col-md-5 m-l0">
                                            <span class="icon video"></span>'.$total_videos. ' Videos
                                        </li>
                                        <li class="col-md-5 m-l0">
                                            <span class="icon position"></span>'.$sports_position.'
                                        </li>
                                        <li class="col-md-5 m-l0">
                                            <span class="icon location"></span>'.$country_name.'
                                        </li>
                                    </ul>
                            </div>
                        </div>    
                </div>
            </div>
            <div class="col-xs-12">&nbsp;</div>';

    }
}
?>
