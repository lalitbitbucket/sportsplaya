<?php
## Include Required Files
/***************************/
require_once 'config/settings.php';
require_once 'includes/phpmailer/class.phpmailer.php';
require_once 'model/email.php';
require_once 'model/videos.php';
/***************************/

## Create objects
/***************************/
$emailObj	= new Model_Email();
$mail 		= new PHPMailer(true);
$videosObj 	= new Model_videos();
$jobsObj 	= new Model_Jobs();
$clubObj = new Model_Club();
/***************************/

## Page body class
/***************************/
$page_body_class = 'home_page';
$smarty->assign ('page_body_class', $page_body_class);

/***************************/

## start session
/***************************/
/*session_start();*/

$_SESSION['map_reg']='';
if(isset($_GET['rel']) && $_GET['rel'] == 'map_reg')
{
	$_SESSION['map_reg']='Yes';
	$_SESSION['map_event_id']='';
}

if ($_SESSION['stagUserId'] == '') 
{
	$submenu_id = 7;
	$cmsArray = $cmsObj->getSubmenuDetailsById($submenu_id);
	$smarty->assign ('cmsArray', $cmsArray);
}

/***************************/

## Get cms detail
/***************************/
function check_slashes($val)
{
	$val = str_replace('"',"",$val);
    $val = str_replace("'","",$val);
    $val = str_replace("\\","",$val);
    $val = str_replace("&","",$val);
    $val = str_replace("@","",$val);
    $val = str_replace("#","",$val);
    $val = str_replace("%","",$val);
    $val = str_replace("$","",$val);
    $val = str_replace("-","",$val);
    $val = str_replace("!","",$val);
    $val = str_replace("*","",$val);
    $val = str_replace("//","",$val);
    $val = str_replace("/","",$val);
    $val = str_replace("?","",$val);
   return $val;
}

/***************************/


## get playa videos
/***************************/
$playavideo = $videosObj->getPlayaOfDay();
if($playavideo['video_type']=='vimeo'){
		$playavideo['video_url'] = str_replace("\\","",$playavideo['video_url']);
		$video_url = explode('"', $videoData['video_url']);
		$playavideo['image_url'] = $playavideo['vimeo_video_image'];
} 
if($playavideo['video_type']=='youtube'){
	$playavideo['video_url'] = str_replace("\\","",$playavideo['video_url']);
	preg_match('/src="([^"]+)"/', $playavideo['video_url'], $match);
    $url = $match[1];
	$arr = explode("/",$url);
	$url= $arr[count($arr)-1];
	$playavideo['image_url'] ="https://i.ytimg.com/vi/".$url."/mqdefault.jpg";	
}
if($playavideo['video_type']=='youtubeurl'){
	$url = $playavideo['video_url'];
   	$playavideo['video_url'] = str_replace("\\","",$playavideo['video_url']);
   	$regex_pattern = "/(youtube.com|youtu.be)\/(watch)?(\?v=)?(\S+)?/";
	if(preg_match($regex_pattern, $url, $match)){
		$playavideo['image_url'] ="https://i.ytimg.com/vi/".$match[4]."/mqdefault.jpg";	
	}
}
if($playavideo['video_type']=='video'){
   	$file = explode('.', $playavideo['video_url']);
	$video_image = $file[0];
	$playavideo['image_url'] = SITE_URL.'/dynamicAssets/videos/thumbnail/'.$video_image.'.jpg';
}
$playavideo['playa_title'] = stripcslashes($playavideo['title']);
$smarty->assign('playavideo', $playavideo);	

/***************************/

//$weeklyDate = getCurrentWeeklyDate();

$currentmonthyear = date('Y')."-".date('m');
$albumArray = $videosObj->getAllVideoByWeekly($orderField = 'id', $orderBy = 'DESC', $limit='12',$offset='0',$startdate=$currentmonthyear,$enddate=$currentmonthyear);
	$i=0;
	foreach($albumArray as $albumDetails) 
	{
		$getAlbumVideos = $videosObj->getAllAlbumVideosByAlbumId($albumDetails['albumId']); 
		if($albumDetails['video_type']=='vimeo')
		{	
			$albumDetails['vcode'] = str_replace("\\","",$albumDetails['vcode']);
			$video_url = explode('"', $albumDetails['vcode']);
			$albumArray[$i]['image_url'] = $albumDetails['vimeo_video_image'];
		 	$albumArray[$i]['url'] = $video_url[1];

		} 

		if($albumDetails['video_type']=='youtube')
		{
		   	$albumDetails['vcode'] = str_replace("\\","",$albumDetails['vcode']);
			preg_match('/src="([^"]+)"/', $albumDetails['vcode'], $match);
		    $url = $match[1];
			$arr = explode("/",$url);
			$url= $arr[count($arr)-1];
			$albumArray[$i]['image_url'] ="https://i.ytimg.com/vi/".$url."/mqdefault.jpg";	
			$albumArray[$i]['url'] = $albumDetails['vcode'];
		}
		if($albumDetails['video_type']=='youtubeurl')
		{
			$url = $albumDetails['vccode'];
		   	$albumDetails['vccode'] = str_replace("\\","",$albumDetails['vccode']);
		   	$regex_pattern = "/(youtube.com|youtu.be)\/(watch)?(\?v=)?(\S+)?/";
			//$match;
		   	if(preg_match($regex_pattern, $url, $match)){
    			$albumArray[$i]['image_url'] ="https://i.ytimg.com/vi/".$match[4]."/mqdefault.jpg";	
    			$albumArray[$i]['url'] = 'https://www.youtube.com/watch?v='. $match[4];
				}
		  	// 	$arr = explode("=",$albumDetails['vccode']);
		  	//	$url= $arr[count($arr)-1];
			
		}
		if($albumDetails['video_type']=='video')
		{
		   	$file = explode('.', $albumDetails['video']);
			$video_image = $file[0];
			$albumArray[$i]['image_url'] = SITE_URL.'/dynamicAssets/videos/thumbnail/'.$video_image.'.jpg';
			$albumArray[$i]['url'] = SITE_URL.'/dynamicAssets/videos/'.$albumDetails['video'];
		}
		//preg_match('/src="([^"]+)"/', $iframe_string, $match);
		//$url = $match[1];
		if($albumDetails['sportsid']!=0){
			$sports_arr = $sportsObj->getSportsDetailById($albumDetails['sportsid']);
			$sports_name = $sports_arr['sportsTitle'];
		}
		else{
			$sports_name = 	$albumDetails['sportsTitle'];
		}
		

		$total_voting = $videosObj->getVotingPercentByVideoId($albumDetails['uId']);
		// $userRecordsInfo = $usersObj->getRecordsDetailsByUserId($getAlbumVideos['user_id']);
		$getAlbumDtl = $videosObj->getAlbumDetailsByAlbumId($albumDetails['albumId']);
		$albumArray[$i]['albumVideoCount']  = count($getAlbumVideos); 
		$albumArray[$i]['videodated'] = $albumDetails['added_date'];
		$albumArray[$i]['albumname']  	    = stripcslashes(substr($getAlbumDtl['title'],0,20).'....');
		$albumArray[$i]['imageCode']  	    = substr($getAlbumVideos[0]['vccode'],16); 
		$albumArray[$i]['fname']  	    = $albumDetails['fname'];
		$albumArray[$i]['lname']  	    = $albumDetails['lname'];
		$albumArray[$i]['uId']  	    = $albumDetails['uId'];
		$albumArray[$i]['str_rating']  	    = $total_voting['voteper'];
		$albumArray[$i]['sports_name']  	    = $sports_name;
		$albumArray[$i]['video_type'] 	= $albumDetails['video_type'];
		$i++;
	}

$smarty->assign('albumArray', $albumArray);	

## Get Jobs Data

$jobsArray = $jobsObj->getAllJobsData($limit=3,$offset=0); 
$total_record = count($jobsArray);
$i=0;
foreach ($jobsArray as $jobValue) {
	$jobsArray[$i]['jobsName'] = trim(strtolower(str_replace(" ","_",$jobValue['job_title'])));
	$jobsArray[$i]['job_description'] = stripcslashes(substr($jobValue['job_description'],0,80).' ....');	
$i++;
}
$smarty->assign('total_jobs_records', $total_record); 
$smarty->assign('jobsArray', $jobsArray);	


## FEATURED PLAYA
/************************/

$featured_playa = $videosObj->getFeaturedPlayaData();

if($featured_playa['video_type']=='vimeo'){
	$featured_playa['video_url'] = str_replace("\\","",$featured_playa['video_url']);
	$video_url = explode('"', $featured_playa['video_url']);
	$featured_playa['image_url'] = $featured_playa['vimeo_video_image'];
} 
if($featured_playa['video_type']=='youtube'){
	$featured_playa['video_url'] = str_replace("\\","",$featured_playa['video_url']);
	preg_match('/src="([^"]+)"/', $featured_playa['video_url'], $match);
    $url = $match[1];
	$arr = explode("/",$url);
	$url= $arr[count($arr)-1];
	$featured_playa['image_url'] ="https://i.ytimg.com/vi/".$url."/mqdefault.jpg";	
}
if($featured_playa['video_type']=='youtubeurl'){
	$url = $featured_playa['video_url'];
   	$featured_playa['video_url'] = str_replace("\\","",$featured_playa['video_url']);
   	$regex_pattern = "/(youtube.com|youtu.be)\/(watch)?(\?v=)?(\S+)?/";
	if(preg_match($regex_pattern, $url, $match)){
		$featured_playa['image_url'] ="https://i.ytimg.com/vi/".$match[4]."/mqdefault.jpg";	
	}
}
if($featured_playa['video_type']=='video'){
   	$file = explode('.', $featured_playa['video_url']);
	$video_image = $file[0];
	$featured_playa['image_url'] = SITE_URL.'/dynamicAssets/videos/thumbnail/'.$video_image.'.jpg';
}

if ($featured_playa['avatar'] == "") {
	if($featured_playa['sex'] != "m")
   	{
   		$img_profile = SITE_URL.'/siteAssets/images/female_default_images1.png';
   	}
   	else
   	{
   		$img_profile = SITE_URL.'/siteAssets/images/male_default_images1.png';
   	}
}
else
{
	$img_profile =  SITE_URL.'/dynamicAssets/avatar/238x259/'.$featured_playa['avatar'];
}
$featured_playa['img_profile']  = $img_profile;

$getComments = $videosObj->getCommentsByNomineesId(2, $featured_playa['id']);

if($featured_playa['sportsID']!=0){
	$sports_Id = explode(",",$featured_playa['sportsID']);
	$sports_name = array();
	foreach ($sports_Id as $sportsd) {
		$sports_array = $sportsObj->getSportsDetailById($sportsd);
		$sports_name = $sports_array['sportsTitle'];
	}
}
else{
	$sports_name = 	$featured_playa['sportName'];
}

if($featured_playa['sports_club']!=0){
	$clubName = $clubObj->getClubNameByClubId($featured_playa['sports_club']);

}
else{
	$clubName = 'N/A';
}
$featured_playa['playa_title'] = stripcslashes($featured_playa['title']);
$featured_playa['clubName'] = $clubName;
$featured_playa['sportsTitle'] = $sports_name;
$smarty->assign('getComments', $getComments);	
$smarty->assign('featured_playa', $featured_playa);	


$usersAllVideos = $videosObj->getvideosByUserId($featured_playa['userId']);  /*Count User uploaded Videos*/
$total_videos = count($usersAllVideos);
$smarty->assign('total_videos',$total_videos);
$i=0;
foreach($usersAllVideos as $albumDetails) {

    $getAlbumVideos = $videosObj->getAllAlbumVideosByAlbumId($albumDetails['albumId']); 
     
    if($albumDetails['video_type']=='vimeo')
    {
        $albumDetails['vcode'] = str_replace("\\","",$albumDetails['vcode']);
        $video_url = explode('"', $albumDetails['vcode']);
        $usersAllVideos[$i]['image_url'] = $albumDetails['vimeo_video_image'];
        $usersAllVideos[$i]['url'] = $video_url[1];

        
    }
    if($albumDetails['video_type']=='youtube')
    {
        $albumDetails['vcode'] = str_replace("\\","",$albumDetails['vcode']);
        preg_match('/src="([^"]+)"/', $albumDetails['vcode'], $match);
        $url = $match[1];
        $arr = explode("/",$url);
        $url= $arr[count($arr)-1];
        $usersAllVideos[$i]['image_url'] ="https://i.ytimg.com/vi/".$url."/mqdefault.jpg";  
        $usersAllVideos[$i]['url'] = $albumDetails['vcode'];
    }
    
    if($albumDetails['video_type']=='youtubeurl')
    {
        $albumDetails['vccode'] = str_replace("\\","",$albumDetails['vccode']);
        $arr = explode("=",$albumDetails['vccode']);
        $url= $arr[count($arr)-1];
        $usersAllVideos[$i]['image_url'] ="https://i.ytimg.com/vi/".$url."/mqdefault.jpg";  
        $usersAllVideos[$i]['url'] = $albumDetails['vccode'];
    }
    if($albumDetails['video_type']=='video')
    {
        $file = explode('.', $albumDetails['video']);
        $video_image = $file[0];
        $usersAllVideos[$i]['image_url'] = SITE_URL.'/dynamicAssets/videos/thumbnail/'.$video_image.'.jpg';
        $usersAllVideos[$i]['url'] = SITE_URL.'/dynamicAssets/videos/'.$albumDetails['video'];
    }

    
    if($albumDetails['sportsid']!=0){
            $sports_arr = $sportsObj->getSportsDetailById($albumDetails['sportsid']);
            $sports_name = $sports_arr['sportsTitle'];
    }
    else{
        $sports_name =  $albumDetails['sportsTitle'];
    }

    $total_voting = $videosObj->getVotingPercentByVideoId($albumDetails['uId']);
    
    $userRecordsInfo = $usersObj->getRecordsDetailsByUserId($getAlbumVideos['user_id']);
    
    $getAlbumDtl = $videosObj->getAlbumDetailsByAlbumId($albumDetails['albumId']);
    
    $usersAllVideos[$i]['albumVideoCount']  = count($getAlbumVideos); 
    $usersAllVideos[$i]['videodated'] = $albumDetails['added_date'];
    $usersAllVideos[$i]['albumname']        = $getAlbumDtl['title'];
    $usersAllVideos[$i]['imageCode']        = substr($getAlbumVideos[0]['vccode'],16); 
    $usersAllVideos[$i]['fname']        = $albumDetails['fname'];
    $usersAllVideos[$i]['lname']        = $albumDetails['lname'];
    $usersAllVideos[$i]['uId']          = $albumDetails['uId'];
    $usersAllVideos[$i]['sportsid']         = $albumDetails['sportsid'];
    $usersAllVideos[$i]['str_rating']       = $total_voting['voteper'];
    $usersAllVideos[$i]['sports_name']          = $sports_name;
    $usersAllVideos[$i]['video_type']	       = $albumDetails['video_type'];
    $i++;
}
$smarty->assign('usersAllVideos', $usersAllVideos); 

// $featured_playa = $videosObj->getPlayaOfDay();
// $arr = explode("/",$featured_playa['video_url']);
// $url= $arr[count($arr)-1];
// $featured_playa['image_url']= "https://i.ytimg.com/vi/".$url."/mqdefault.jpg";
// $userData = $usersObj->getProfileDetailsByUserId($featured_playa['userId']);
// if ($userData['avatar'] == "") {
// 	if($userData['sex'] != "m")
//    	{
//    		$img_profile = SITE_URL.'/siteAssets/images/female_default_images1.png';
//    	}
//    	else
//    	{
//    		$img_profile = SITE_URL.'/siteAssets/images/male_default_images1.png';
//    	}
// }
// else
// {
// 	$img_profile =  SITE_URL.'/dynamicAssets/avatar/238x259/'.$userData['avatar'];
// }
// $userData['img_profile']  = $img_profile;
// $smarty->assign('userData', $userData);
// $smarty->assign('featured_playa', $featured_playa);	



/************************/


/*___________________________________________*/



	// if ($_POST) {

	// if ($_POST['step1'] != '') {

	// 	$fname = trim(return_post_value (check_slashes($_POST['fname'])));

	// 	$email = trim(return_post_value ($_POST['email']));

	// 	$dob = trim(return_post_value ($_POST['dob']));

	// 	$gender = trim(return_post_value ($_POST['gender']));
	 	
	// 	$lname = trim(return_post_value (check_slashes($_POST['lname'])));

	// 	$password = trim(return_post_value ($_POST['password']));

	// 	$usertype = trim(return_post_value ($_POST['usertype']));



		## Change date of birth format

		// $dateOfBirth = date("Y-m-d", strtotime($_POST['dob'])); 

		// $smarty->assign('dateOfBirth', $dateOfBirth);



	 // 	$_SESSION['stagUserEmail'] = $email;

		// if ($fname != '' && $email != '' && $gender != '' && $dob != '' && $lname != '' && $usertype != '') {

		// 	if (filter_var ($email, FILTER_VALIDATE_EMAIL)) {

		// 		$result = $usersObj->chkUserEmailExist(trim ($email),'');
				/*$newpassword = genenrate_password($salt,$password);*/
				// if (count ($result) < 1) {

				// 	$salt 				= genenrate_salt();

				// 	$userArray 			= array ();

				// 	$newpassword 			= genenrate_password($salt,$password);
    //                 $_SESSION['mail_psswd']=$password;
   					
				// 	$userArray['password']  	= $newpassword;	
    //                 $userArray['email']       = $email;
				// 	$userArray['salt']      	= $salt;

				// 	$userArray['userType']  	= $usertype;

				// 	$userArray['regDate']   	= date ('Y-m-d');

				// 	$userArray['status'] 		= '2';

				// 	$newuserId = $usersObj->addUserByValue($userArray);

					/************ Add details in user profile *********/

					// $userprofileArray = array ();

					// $userprofileArray['userId'] 	= $newuserId;

					// $userprofileArray['userType'] 	= $usertype;
					
     //                $userprofileArray['email'] 	= $email;
					
					// $userprofileArray['fname'] 	= $fname;

					// $userprofileArray['lname'] 	= $lname;

					// $userprofileArray['birthdate'] 	= $dateOfBirth;

					// $userprofileArray['sex'] 	= $gender;

					// $userprofileArray['regDate'] 	= date('Y-m-d');

					// $userprofileArray['status']     ='2';

					// $userprofileId = $usersObj->addUserProfileByValue($userprofileArray);



					/************ Add User Details In Account Settings & Set Value Table Start *************/

					

					## Who Can See My Stuffs

					// $settingId = '1';

					// $stuffArray = array();

					// $stuffArray['userId'] 	 = $userprofileId;

					// $stuffArray['settingId'] = $settingId;

					// $usersObj->addAccountSetValues($stuffArray);



					## Who Can Contact Me

					// $settingId = '2';

					// $friendArray = array();

					// $friendArray['userId']   = $userprofileId;

					// $stuffArray['settingId'] = $settingId;

					// $usersObj->addAccountSetValues($friendArray);

					/************ Add User Details In Account Settings Table End ***************************/

		 //   $userNameDetail = array ();

		 //   $username  = trim(strtolower(str_replace(" ","",$fname))) . '' . trim(strtolower(str_replace(" ","",$lname)));
		 //   $username1 =  trim(strtolower(str_replace(" ","",$fname))) . '_' . trim(strtolower(str_replace(" ","",$lname)));
		 //   $username2 =  trim(strtolower(str_replace(" ","",$fname))) . '-' . trim(strtolower(str_replace(" ","",$lname)));
		  	   
   //         $usernameexist = $usersObj->checkUserNameExists($username);
		 //   $usernameexist1 = $usersObj->checkUserNameExists($username1);
		 //   $usernameexist2 = $usersObj->checkUserNameExists($username2);

		 //   if (count ($usernameexist) < 1)
		 //    {
			//    $userNameDetail['username'] = $username;
			// } 
		 //   elseif (count ($usernameexist1) < 1)
		 //    {
			// 	$userNameDetail['username'] = $username1;
			// }
		 //   elseif (count ($usernameexist2) < 1) 
		 //    {
			// 	$userNameDetail['username'] = $username2;
			// }
		 //  else
		 //   {	$username = trim(strtolower(str_replace(" ","",$fname))) . '' . trim(strtolower(str_replace(" ","",$lname))) . '' . $userprofileId;
	   		     /*$uname = str_replace(" ","",$username);*/
				// $userNameDetail['username'] = $username;
    // 	   }
		  //       print_r($userNameDetail);
				// $usersObj->editUserProfileValueById ($userNameDetail,$userprofileId);

					/*********************************************************************************/

					// $fetchdob = $usersObj->getUserdob($userprofileId);

					// $smarty->assign ('fetchdob', $fetchdob);

					// $calAge = CalculateAge($fetchdob);

					// $dobArray = array();

					//$dobArray['userId']      = $userprofileId;

					// $dobArray['age'] 	 = $calAge;

					// $usersObj->editUserProfileValueById ($dobArray,$userprofileId);

					/*********************************************************************************/

					 /************* Insert User Name with special characters End *************/

					// $_SESSION['tmpUserId'] = $newuserId;

					// $_SESSION['tmpUserProfileId'] = $userprofileId;

					// $_SESSION['tmpUserFName'] = $fname;

					// $_SESSION['stagUserEmail'] = $email;


/******************************** Sending Mail To Suggested User *******************************/

// $sender=$usersObj->getUserProfileDetailEmailByOnlyUserID($_SESSION['tmpUserProfileId']);

// $invitedata = $usersObj->GetinvitationemailsDetailsByEmail($sender['email']);

// if(count($invitedata)>0)
// {
//     foreach($invitedata as $invite)
//     {
//         $receiver=$usersObj->getUserProfileDetailEmailByOnlyUserID($invite['user_profile_id']);
//         $emailArray = $emailObj->getEmailById(24);
//         $to = $receiver['email'];
//         $toname = $receiver['fname'].' '.$receiver['lname'];       
//         $fromname = $emailArray['fName'];
//         $from = $emailArray['fEmail'];
        
//         $subject = $emailArray['emailSub'];
//         $subject = str_replace('[UNAME]', $sender['fname'].' '.$sender['lname'], $subject);
 
//         $str = '';
//         $str = 'Your given referral '.$sender['fname'].' '.$sender['lname'].' ('.$sender['email'].') joined sportsplaya.';
        
//         $message = $emailArray['emailContent'];
//         $message = str_replace('[SITEURL]', SITE_URL, $message);
//         $message = str_replace('[SITENAME]', SITENAME, $message);
//         $message = str_replace('[UNAME]',$receiver['fname'].' '.$receiver['lname'],$message);
//         $message = str_replace('[msg]', $str , $message);
        
//         $template_msg   = str_replace('[MESSAGE]',$message , $message);
//         $template_msg   = str_replace('[LOGO]','<img src="'.SITE_URL.'/siteAssets/images/logo.png">', $template_msg);
//         $template_msg   = str_replace('[PASSWORD]',$_SESSION['mail_psswd'], $template_msg);
//         $template_msg   = str_replace('[SITELINK]',SITE_URL , $template_msg);
//         $template_msg   = str_replace('[SUBJECT]',$subject , $template_msg);
//         $template_msg   = str_replace('[SITEROOT]',SITE_URL , $template_msg); 
//         $template_msg   = str_replace('[UNAME]',UNAME , $template_msg); 
//         $template_msg   = str_replace('[msg]',$str , $template_msg);    

//          try
//           {
//             $mail->AddAddress($to, $toname);
//             $mail->SetFrom($from, $fromname);
//             $mail->addBCC("psrajender@gmail.com","Rajendra Bhatia");
//             $mail->Subject = $subject;
//             $mail->Body = $template_msg; 
//             $mail->Send();
//           } 
//         catch (phpmailerException $e)
//          {
//             echo $_SESSION['msg']= $e->errorMessage();
//          }  
//         catch (Exception $e)
//          {
//             echo $_SESSION['msg']= $e->getMessage();
//          }

//     }
// }
/******************************** Sending Mail To Suggested User *******************************/
					

					// if($usertype == 3)

					// 	$_SESSION['tmpUserType'] = 'Sportsperson';

					// elseif($usertype == 4)

					// 	$_SESSION['tmpUserType'] = 'Fan';

					// else

					// 	$_SESSION['tmpUserType'] = 'Scout';

					

					// if($usertype != 5) {

					// 	header('location:' . SITE_URL . '/register/step2');

					// }else{

					// 	header('location:' . SITE_URL . '/register/membership');

					// }

// 					exit();

// 				} 

// 				else {

// 					$smarty->assign ('cmsArray', $cmsArray);

// 					$_SESSION['msg'] = "<div class='alert'>Email already register please go to sign in page.</div>";

// 				}

// 			    } 



// 			else {

// 				$_SESSION['msg'] = "<div class='alert'>Please Enter a Valid E-mail Id.</div>";

// 			}

// 		} 

// 		else {

// 			$_SESSION['msg'] = "<div class='alert'>All fields are required!</div>";

// 		}

// 	}

// }



// Assign success or error msg to smarty variable and unset session variable

if (trim (@$_SESSION['msg']) != '') {

	$smarty->assign ( 'msg', $_SESSION['msg']);

	unset ($_SESSION['msg']);

}



if ($_SESSION['stagUserId'] == '') {

	$smarty->display(TEMPLATEDIR . '/index.tpl');

} else {

	if ($_SESSION['userType'] != '6') {

	$smarty->assign('myactivityclass', 'active');

				

	if($otheruserDetails['fname']!='' && $otheruserDetails['lname']!='' && $otheruserDetails['birthdate']!='' && $otheruserDetails['sex']!='' 

	   && $otheruserDetails['country']!='' && $otheruserDetails['aboutme']!='' && count($userExpDet)>0 && count($userSkillsDet)>0 && count($userEducation)>0)

	   {

	   	if($otheruserDetails['id'] == $userInfo['id']){



	        	$smarty->display(TEMPLATEDIR . '/index.tpl');

	        }else{

	        	$smarty->display(TEMPLATEDIR . '/index.tpl');
	        	//$smarty->display(TEMPLATEDIR .'/controller/users/users.tpl');

                }

	   }else{

	        	$smarty->display(TEMPLATEDIR . '/index.tpl');
	        	//header('location:'.SITE_URL.'/'.$userInfo['username']);
	        	//header('location:'.SITE_URL.'/index.tpl'); 

	   }

	   

	} else {

		$userType = 6;

		$smarty->assign ( 'userType', $userType);

		$userProfileInfo = $usersObj->getUserProfileDetailByUserID ($userId, $userType);

		$userProfileInfo['age'] = CalculateAge($userProfileInfo['birthdate']);

		$smarty->assign( 'userProfileInfo', $userProfileInfo);



		$profileuser = $userProfileInfo ['id'];

		$smarty->assign('profileuser', $profileuser);

		$smarty->assign('myactivityclass', 'active');

		$smarty->display(TEMPLATEDIR . '/index.tpl');
		

		//$smarty->display(TEMPLATEDIR . '/controller/club/club.tpl');

		

	}

}

?>

