<?php
## include required files
/*******************************/
require_once 'config/settings.php';
## Create Object  
$CityObj = new Model_City;

$countryId = $_REQUEST['countryId'];
$getCity = $CityObj->getAllActiveCityByCountryId($countryId);
$str = "";
if(count($getCity) > 0)
{
    foreach ($getCity as $value)
    {
        $str .= '<option value="'.$value['ctID'].'">'.$value['ctName'].'</option>';
    }
    echo $str;
}
else
{
	echo 0;
}
?>
