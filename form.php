<?php
if(empty($_SERVER['HTTP_REFERER'])){
    header('location:'.SITE_URL.'/');
    exit;    
}

if(isset($_POST) && $_POST['submit']='Send' && $_POST['email']!='')
{
    $to = "Contact@sportsplaya.com";
    $subject = "Feedback";
    $txt = $_POST['mail_msg'];
    $headers = "From:".$_POST['email'];
    
	 if($mail=mail($to,$subject,$txt,$headers)){ 
    $msg = 'Feedback Successfully Send.';
    ?>
      <script>window.parent.location.reload();</script>
    <?php }
}
?> 
<link rel="stylesheet" type="text/css" href="siteAssets/css/bootstrap.css" />
<link rel="stylesheet" type="text/css" href="siteAssets/css/bootstrap-responsive.css" />
<link rel="stylesheet" type="text/css" href="siteAssets/css/fonts.css" />
<style>
.white-popup-block{background-color: #fff !important;}
.btn
{
  text-transform:uppercase;
  background: #767B87;
  display: inline-block;
  margin-bottom: 0;
  line-height: 1rem;
  color: #fff;
  text-align: center;
  vertical-align: middle;
  cursor: pointer;
  border: 0 solid #fff;
  padding: 1rem 1.6rem;
  white-space: nowrap;

}
.btn:hover, .btn:focus{
  background:#767B87;
}
</style>

<form class="white-popup-block login_popup" method="post" action="form.php">
<center>
<h2 style="margin-left: -38%;margin-top:0;
    margin-bottom: 5%;">Feedback</h2>
    <span style="color: green;margin-left: -38%;"><?php if(!empty($msg)){echo $msg;}?></span>
<table>
<tr><td>Email</td><td><input type="email" name="email" id="email_feedback" class="span4 p-18 p-tb27 text-dgrey" style="width:54%;height: 32px;" required /></td></tr>
<tr><td colspan="2">&nbsp;</td></tr>
<tr><td>Feedback</td><td><textarea cols="15" rows="7" name="mail_msg" id="mail_msg" class="span6 p-18 p-tb27 text-dgrey" required></textarea></td></tr>
<tr><td colspan="5" align="center"><input style="margin-top:2%;" type="submit" value="Send" class="btn text-uc p-20 font-uc bg-grey text-white block-xs"></td></tr>
</table>
</center>
<br><br>
</form>