<?php
## include required files
/*******************************/
require_once 'config/settings.php';

isLogin();
/*******************************/
  	
if( isset($_REQUEST['payment_status']) && in_array($_REQUEST['payment_status'],array('Pending','Success')))
  {

      $user_id=$userInfo['id'];
      $approve_adsearch=$usersObj->ApproveAdvanceSearchByCoupon('Y',$user_id);
          if($approve_adsearch>0)
          {
            $_SESSION['advance_search'] = 'Y'; 
          }
?>
<div style="margin-top:10%">
<h2 style="width:100%;text-align:center;color:#24436A">Congrats!! Your payment has been applied successfully and Adavnce Search Features Activated Successfully..</h2><br>     
<p style="text-align:center"><a class="btn" href="<?php echo SITE_URL; ?>">Go Back to Home Page</a></p>
</div>
<?php }
else
{
?>
<div style="margin-top:10%">
<h2 style="width:100%;text-align:center;color:#AA1F24">Sorry!! Your payment has been declined, try again later..</h2><br>     
<p style="text-align:center"><a class="btn" href="<?php echo SITE_URL; ?>">Go Back to Home Page</a>     
</p>
</div>
<?php 
}
?>
<style type="text/css">
.btn {
  background: #3498db;
  background-image: -webkit-linear-gradient(top, #3498db, #2980b9);
  background-image: -moz-linear-gradient(top, #3498db, #2980b9);
  background-image: -ms-linear-gradient(top, #3498db, #2980b9);
  background-image: -o-linear-gradient(top, #3498db, #2980b9);
  background-image: linear-gradient(to bottom, #3498db, #2980b9);
  -webkit-border-radius: 5;
  -moz-border-radius: 5;
  border-radius: 5px;
  font-family: Georgia;
  color: #ffffff;
  font-size: 15px;
  padding: 10px 20px 10px 20px;
  text-decoration: none;
}

.btn:hover {
  background: #3cb0fd;
  background-image: -webkit-linear-gradient(top, #3cb0fd, #3498db);
  background-image: -moz-linear-gradient(top, #3cb0fd, #3498db);
  background-image: -ms-linear-gradient(top, #3cb0fd, #3498db);
  background-image: -o-linear-gradient(top, #3cb0fd, #3498db);
  background-image: linear-gradient(to bottom, #3cb0fd, #3498db);
  text-decoration: none;
}
</style>
