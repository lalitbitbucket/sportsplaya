<?php 
require_once 'config/settings.php';
require_once 'includes/phpmailer/class.phpmailer.php';
require_once 'model/email.php';

$emailObj  = new Model_Email();
$mail 	   = new PHPMailer(true);

$users=$usersObj->getAllActiveUserDetails();

foreach($users as $user)
{	
    $userID=$user['id'];

  		$tagUpdates=$postsObj->getSevenDaysTagsActivities($userID);
    	if(empty($tagUpdates))
	    {
	    	// echo "<br>";
	    	// echo "There Are No Activies By ".$user['fname'].' '.$user['lname']." in Past 7 Days.";
	     //    echo "<br>"; 
	    }
	    //print_r($tagUpdates);
         $friends_arr = array();

		 foreach($tagUpdates as $arr_Post)
			{
			   
			   $posted_user_id  = $arr_Post['userId'];
			   $profile_details = $usersObj->getUserDetailsInfoByUserId($posted_user_id);
			   $friends_data    = $usersObj->getUserFriendsId($posted_user_id); 
        	   $followersUsers = $usersObj->getAllFollowersByUserId($posted_user_id);
		       foreach($followersUsers as $followersUser)
		       {
		       	 $followers[]=array('frndid'=>$followersUser['userID']);
		       }
                
		       foreach($followers as $follower)
		       { 
		       	if(!(in_array($follower,$friends_data)))
		       	{
		       		array_push($friends_data,$follower);
		       	}
               }

		       $post_date= $arr_Post['modify_date'];

			   $activity = $arr_Post['post_activity'];
		        
				  if(empty($activity))
				       {
					       	 if($arr_Post['image_url']!='')
					       	 {
					            $activity="<a href='".SITE_URL."/".$profile_details['username']."'>". ucfirst ($profile_details['fname']).' '. ucfirst ($profile_details['lname'])."</a>  has posted image on";
					       	 }
				           	 
				           	 if($arr_Post['share_video']!='')
					       	 {
					            $activity="<a href='".SITE_URL."/".$profile_details['username']."'>". ucfirst ($profile_details['fname']).' '. ucfirst ($profile_details['lname'])."</a>  has posted video on";
					       	 }
					       	 if($arr_Post['share_video']=='' && $arr_Post['image_url']=='' && $arr_Post['message']!='')
					       	 {
		                       $activity="<a href='".SITE_URL."/".$profile_details['username']."'>". ucfirst ($profile_details['fname']).' '. ucfirst ($profile_details['lname'])."</a>  has posted on timeline on";
					       	 
					       	 }
				       
				       }
				       else
				       {
				       	 $activity=$activity." on";
				       }
			 

			      foreach($friends_data as $friend_data)
			 	   {
					    $friend_id = $friend_data['frndid'];
			        	$friends_arr[$friend_id][] = $activity." ".$post_date."." ;
			       }
			} 


		foreach($friends_arr as $key => $rows)
		{
		       $user=$usersObj->getUserProfileDetailEmailByOnlyUserID($key);
				
			   $emailArray = $emailObj->getEmailById(21);

				echo $to = $user['email'];
				echo "<br>";
				echo $toname = $user['fname'].' '.$user['lname'];		
				echo "<br>";
				echo $fromname = $emailArray['fName'];
				echo "<br>";
				echo $from = $emailArray['fEmail'];
		        echo "<br>";
				echo "<br>";
				echo "<br>";
				
				$subject = $emailArray['emailSub'];
				$subject = str_replace('[SITENAME]', SITENAME, $subject);
				$str ="";
			    
		         $str .= '<style>@media only screen and (max-device-width: 480px){ #activity_msg{line-height:25px !important;}tr td img {max-width: 100px !important;height: auto !important;} table {width:90% !important;border:1px #555555 !important;} h1,h3{ text-indent: 0px !important;} b{font-size:16px !important;} td a img{width:100% !important;} p{padding-left:2px !important; width:90% !important;font-size:12px !important;} #follow_mob{font-size:12px !important;padding:5px 10px !important; max-width:60px !important;margin-top: 5px !important;} tr td a img{max-width:80% !important;} #sports_img{width: 15px !important;margin-right: 2px !important;} #follow_mob{display:block !important;} #follow_all{display:none !important;}}'; 
		            $str  .= '<style>@media only screen and (min-device-width: 480px) and (max-device-width: 600px) {table {width:90% !important;border:1px #555555 !important;} h1,h3{ text-indent: 0px !important;} b{font-size:16px !important;} td a img{width:50% !important;} p{padding-left:2px !important; width:90% !important;} #activity_msg{line-height:25px !important; font-size:14px !important}}</style>';
					
					$str .='<style>a{text-decoration:none !important;font-weight:600;color:#3975C0;} #activity_msg{line-height:0px;font-size:15px !important;}</style>';
					$str  .= '<tr width="100%">';
		            $str  .='<td>';
		            for($i=0 ; $i<count($rows) ; $i++)
		            {

		             	$str.='<p id="activity_msg">'.$rows[$i].'<p>';
		            	$str.='<br>';
		            }
		           $str .='</td>';

		            $str  .='</tr>';
		        
				$message = $emailArray['emailContent'];
				$message = str_replace('[SITEURL]', SITE_URL, $message);
				$message = str_replace('[SITENAME]', SITENAME, $message);
		        $message = str_replace('[UNAME]', $user['fname'], $message);
		        $message = str_replace('[follow_list]', $str , $message);

				$template_msg	= str_replace('[MESSAGE]',$message , $message);
				$template_msg 	= str_replace('[LOGO]','<img src="'.SITE_URL.'/siteAssets/images/newimages/logo.png">', $template_msg);
				$template_msg 	= str_replace('[PASSWORD]',$_SESSION['mail_psswd'], $template_msg);
				$template_msg 	= str_replace('[SITELINK]',SITE_URL , $template_msg);
				$template_msg 	= str_replace('[SUBJECT]',$subject , $template_msg);
				$template_msg	= str_replace('[SITEROOT]',SITE_URL , $template_msg);					
				$template_msg	= str_replace('[UNAME]',UNAME , $template_msg);	
		        $template_msg	= str_replace('[follow_list]',$str , $template_msg);	
		        
		        print_r($template_msg);
		   
		         try
		          {
					// $mail->AddAddress($to, $toname);
					// $mail->SetFrom($from, $fromname);
					// $mail->addBCC("psrajender@gmail.com","Rajendra Bhatia");
					// $mail->Subject = $subject;
					// $mail->Body = $template_msg; 
					// $mail->Send();
				  } 
				catch (phpmailerException $e)
				 {
				    echo $_SESSION['msg']= $e->errorMessage(); //Pretty error messages from PHPMailer
				 }  
				catch (Exception $e)
				 {
				    echo $_SESSION['msg']= $e->getMessage(); //Boring error messages from anything else!
				 }

		}
}
?>