<?php 
require_once 'config/settings.php';
require_once 'model/common/image_functions.php';

$siteurl = SITE_URL.'/';
if(empty($_SERVER['HTTP_REFERER']) || $_SERVER['HTTP_REFERER']!= $siteurl){
    header('location:'.SITE_URL.'/');
    exit;    
}

if($otheruserDetails['id']!=''){
	$fname = $otheruserDetails['fname'];
	$lname = $otheruserDetails['lname'];

	$image_url = $otheruserDetails['avatar'];
}

if(isset($_POST['submit']) && $_POST['submit'] == 'UPLOAD IMAGE'){

 	$oldAvatarName = $otheruserDetails['avatar'];
	$avatarPath = SITE_URL."/dynamicAssets/avatar/238x259/";
 	$fullAvatarNamewithPath=$avatarPath.$oldAvatarName;

	if($_FILES['avatar']['name']!= '')
 	  {

 	  	$_FILES['avatar']['name'];
	  	$size =	getimagesize($_FILES['avatar']['tmp_name']);
		$imageWidth 	= '38';
		$imageHeight 	= '38';
		

		$imageName    = $_FILES['avatar']['name'];
		$fileName     = date('Ymdhis').".".$imageName;		
		$temp=$_FILES["avatar"]["tmp_name"];					
		$fileName      = time()."_".$_FILES['avatar']['name'];
		$imageFolder= SITE_URL.'/dynamicAssets/avatar/';
		$imageName= $fileName;

		$thumbFolder1 = SITE_URL."/dynamicAssets/avatar/238x259/";						
		$height1 = 259;
		$width1= 238;
		$file1=uploadImageAndCreateThumb($temp,$imageName,$imageFolder,$thumbFolder1,$height1,$width1);

		$thumbFolder2 = SITE_URL."/dynamicAssets/avatar/38x38/";						
		$height2 = 38;
		$width2= 38;
		$file1=uploadImageAndCreateThumb($temp,$imageName,$imageFolder,$thumbFolder2,$height2,$width2);
						
		$thumbFolder3 = SITE_URL."/dynamicAssets/avatar/61x61/";						
		$height3 = 61;
		$width3= 61;
		$file1=uploadImageAndCreateThumb($temp,$imageName,$imageFolder,$thumbFolder3,$height3,$width3);				

		$userArray = array();
		$userArray['avatar'] 		=  $fileName;	
		##Unlink File From Folder
		unlink($fullAvatarNamewithPath);
		$usersObj->changeProfileUserProfileImage($otheruserDetails['id'],$imageName);

		$userImageArray = array();
		$userImageArray['profileId'] 	=  $otheruserDetails['id'];	
		$userImageArray['imageName'] 	=  $fileName;
		$usersObj->addUserProfileImagesByProfileId($userImageArray);

		echo '<script>top.window.location.href="'.SITE_URL.'/'.$otheruserDetails['username'].'/wall"</script>';
        exit;
}
}
?>




<link rel="stylesheet" type="text/css" href="siteAssets/css/bootstrap.css" />
<link rel="stylesheet" type="text/css" href="siteAssets/css/bootstrap-responsive.css" />
<link rel="stylesheet" type="text/css" href="siteAssets/css/styles.css" />
<link rel="stylesheet" type="text/css" href="siteAssets/css/fonts.css" />
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<style>
	@media (min-width: 1024px){
		html {
			font-size: 18px !important;
		}
	}
	.white-popup-block{background:#fff url(siteAssets/images/newimages/bg_register.png) center top no-repeat;	
	background-size:100%;}
    .span5{
            margin-bottom: 2%;
    }
	#custom-content img {max-width: 100%;margin-bottom: 10px;}
</style>
<div class="white-popup-block">
    <div id="custom-content" class="p-t27" style="max-width:100%; padding:3rem 3rem 1rem;">
        <form method="post" enctype="multipart/form-data">
        <h3>Edit Image</h3>
        <div>
        	<p class="text-dgrey font-segoe p-30 font-uc m-tb0 m-b40"><?php echo $fname.' '.$lname; ?>,</p>
        </div>
        <div>
        	<img src="<?php echo SITE_URL.'/dynamicAssets/avatar/238x259/'.$image_url;?>" class="userImg">	
        </div>	
        <div>
        	<input id="avatar" name="avatar" type="file" class="upload" accept="image/*">
        </div>
        <input type="submit" name="submit" id="submit" value="UPLOAD IMAGE">
        </form>
    </div>

</div>
<style type="text/css">
.upload {
    font-size: 20px;
    cursor: pointer;
}
</style>