<?php
## Include Require Files
/***************************/
require_once '../config/settings.php';

## Create Objects
/***************************/
$jobsObj = new Model_Jobs();

## Get Jobs Data By API
/***************************/

$url = "https://api.adzuna.com:443/v1/api/jobs/au/search/1?app_id=caff643f&app_key=d9a9aa346d79476b8dd46f32a70182a4&title_only=sport&content-type=application/json";

/**** Curl ****/

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
$output = curl_exec($ch);
$data=json_decode($output, true);
curl_close($ch);

foreach($data['results'] as $task){
	$checkId = $jobsObj->ifExistsJobcheck($task['id']);
	if ($checkId<=0) {
			$data =array();
			$data['userId'] 		= '228';
			$data['job_title']	= preg_replace("/[^a-zA-Z]/", " ", strip_tags($task['title']));
			$data['profession'] = preg_replace("/[^a-zA-Z]/", " ", strip_tags($task['title']));
			$data['job_description']= $task['description'];
			$data['sports'] 		= 'N/A';
			$data['job_country'] 	= '14';
			$data['posted_date'] 	= $task['created'];
			$data['adzuna_job_id']	= $task['id'];
			$data['job_address']	= $task['location']['display_name'];
			$data['company_name']	= $task['company']['display_name'];
			$data['redirect_url']	= trim($task['redirect_url']);
			$data['job_type']	= 'Full Time';
			if($task['salary_max']!=''){
				$data['salary'] 		= $task['salary_max'];
			}else{
				$data['salary'] 		= 'N/A';
			}

			echo '<br>';
			echo $task['title'].'<strong style="color:green;"> Insert Jobs</strong>';
			$jobsObj->insertJobDataByApi($data);
			echo '<br>';
			echo '<hr>';
			echo '<br>';
	}else{
		echo $task['title'].'<strong style="color:red;"> This Job Already Exists</strong>';
		echo '<br>';
		echo '<hr>';
		echo '<br>';
	}
}
?>