<?php 
/**************************************************/
## Class Name - Model_CMS (Contains all the functions related cms pages of the project)
/**************************************************/
class Model_CMS extends Database
{	
	function Model_CMS() {	
		$this->cms = CMS;
		$this->Database();
	}
	
	// Add CMS Page
	function addSubmenu($array) {
	$this->InsertData($this->cms, $array);
	}
	
	## List all submenus
	function getAllSubMenu($search, $order_field, $order_by, $limit='', $offset='') {
	$fields = array();	
	$tables = array($this->cms);
	if(trim($search)!='') {
		$where = array('pageTitle LIKE "%'.trim($search).'%"');
	} else {
		$where = array();
	}
	$order = array("$order_field $order_by");
	$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(),$limit,$offset,0); 
	$result  = $this->FetchAll($result1); 
	return $result;
	}

	## List all CMS
	function getAllCMSs() {
	$fields = array();	
	$tables = array($this->cms);
	$result1 = $this->SelectData($fields,$tables, 	$where  = array("pageStatus='1'"), $order = array(), $group=array(),$limit = "",0,0); 
	$result  = $this->FetchAll($result1); 
	return $result;
	}
	
	## Get submenu details by id
	function getSubmenuDetailsById($submenu_id) {
	$fields = array();	
	$tables = array($this->cms);
	$result1 = $this->SelectData($fields,$tables, $where= array("pageID='".$submenu_id."'"), $order = array(), $group=array(),$limit = "",0,0); 
	$result  = $this->FetchRow($result1); 
	return $result;
	}
	
	function getAllCmsMenu()
	{
	$fields = array();	
	$tables = array($this->cms);
	$result1 = $this->SelectData($fields,$tables, 	$where  = array(" pageStatus  = '1' "), $order = array(), $group=array(),$limit = "",0,0); 
	$result  = $this->FetchAll($result1); 
	return $result;	
	}
	
	## Edit menu by id
	function editUserCmsById($array, $Id){
	$this->UpdateData($this->cms,$array,"pageID",$Id,0);
	}
	
	## Update cms page pageStatus with multiple ids
	function updateMultipleCmsstatus($ids, $pageStatus) {
	$sql = "UPDATE ".$this->cms." SET pageStatus='".$pageStatus."' WHERE pageID IN (".$ids.")";
	$result1= $this->ExecuteQuery($sql);	 
	}
}
?>