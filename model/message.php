<?php  

/**************************************************/

## Class Name - Message (Contains all the functions related Message pages of the project)

/**************************************************/

class Model_Messages extends Database

{    

    function Model_Messages() {    

        $this->messages = MESSAGES;

        $this->contacts = CONTACTS; 

        $this->user = USERS;

	$this->userfriends = USERFRIENDS;

	$this->userprofile = USERPROFILE;	

        $this->Database();

    }

    

    ## List of all messages from inbox 

    function getUserInbox($userId) {

        $fields = array();    

        $tables = array($this->messages);

        $where = array("status = '2'");

        $where[] = "(recieverId = '".$userId."')";    

        $result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 

        $result  = $this->FetchAll($result1); 

        return $result;

    }

    

    # send message 

    function sendMessageById( $array )

    {

        $this->InsertData( $this->messages ,$array);        

        $insertId = mysql_insert_id();

        return $insertId;

    }

    

    # update read status 

    function updateReadstatusByMessageId($array, $Id){

        $this->UpdateData($this->messages,$array,"id",$Id,0);

    }



     # update multiple read status 

     function updateMultipleReadstatusByMessageId($Ids,$action){

	 $sql = "UPDATE ".$this->messages." SET readSent='".$action."' WHERE id IN (".$Ids.")"; 

	 $result1= $this->ExecuteQuery($sql);	 

     }



     function updateMessageReadstatusByMessageId($profileId,$action,$userId){

     	$sql = "UPDATE ".$this->messages." SET readInbox='".$action."' WHERE senderId='".$profileId."' AND recieverId='".$userId."'";

        $result1= $this->ExecuteQuery($sql);	 

     }



     # update read message alerts by msg id

     function updateReadMessageAlerts($Ids,$action){

	 $sql = "UPDATE ".$this->messages." SET readalert='".$action."' WHERE id IN (".$Ids.")"; 

	 $result1= $this->ExecuteQuery($sql);	 

     }



     ## Delete message from message ID

     function deleteMessagesBySenderAndReceiverId($senderId,$recieverId){

     	 $sql = "UPDATE ".$this->messages." SET status ='0' where (senderId ='".$senderId."' and recieverId ='".$recieverId."')";  

	 $result1= $this->ExecuteQuery($sql);	 

     }

     

     function deleteMessageById($msgID,$inboxConditionCriteria){

     	 $sql = "UPDATE ".$this->messages." SET ".$inboxConditionCriteria."='1',readInbox='0' where id ='".$msgID."'";

	 $result1= $this->ExecuteQuery($sql);

     }

    

    

     function getAllInBoxMessagesByUserId($id,$userId){
        $fields = array("m.*,u.fname,u.lname,u.id as uID,u.birthdate,u.userType,u.avatar,u.sex,u.username");    
        $tables = array($this->messages.' as m',$this->userprofile.' as u');
        $where  = array("m.senderId=u.id");	
        $where[]= " ((m.recieverId ='".$id."' and  m.senderId ='".$userId."' AND m.deleteSent='0') OR (m.recieverId ='".$userId."' and  m.senderId ='".$id."' AND m.deleteInbox='0'))";

        $sql    = $this->SelectData($fields,$tables, $where, $order = array("m.date ASC"), $group=array(),$limit='',$offset='',0); 
        $result = $this->FetchAll($sql); 
        return $result;     
    }

	

    function getRecentMessageByUserId($id,$userId,$limit='1',$offset='0'){

        $fields = array("m.*,u.fname,u.lname,u.id as uID,u.birthdate,u.userType,u.avatar,u.sex,u.username");    

	$tables = array($this->messages.' as m',$this->userprofile.' as u');

        $where  = array("m.senderId=u.id");	

        $where[]= " ((m.recieverId ='".$id."' and  m.senderId ='".$userId."' AND m.deleteSent='0') OR (m.recieverId ='".$userId."' and  m.senderId ='".$id."' AND m.deleteInbox='0'))";

        $sql    = $this->SelectData($fields,$tables, $where, $order = array("m.date DESC"), $group=array(),$limit,$offset,0); 

        $result = $this->FetchRow($sql); 

        return $result;

    }



    ## Get recent message by message alert

    function getRecentMessagesByMessageAlerts($id,$userId,$limit='1',$offset='0'){

	$fields = array("m.*,u.fname,u.lname,u.id as uID,u.birthdate,u.userType,u.avatar,u.sex,u.username");    

	$tables = array($this->messages.' as m',$this->userprofile.' as u');

	$where  = array("m.senderId=u.id");	

        $where[]= " ((m.recieverId ='".$id."' and  m.senderId ='".$userId."' AND m.deleteSent='0') OR (m.recieverId ='".$userId."' and  m.senderId ='".$id."' AND m.deleteInbox='0'))";

	$sql    = $this->SelectData($fields,$tables, $where, $order = array("m.date DESC"), $group=array(),$limit,$offset,0); 

	$result = $this->FetchRow($sql); 

	return $result;     

    }



    function getRecentMessagesByMessageAlertCount($id,$userId,$limit='1',$offset='0')

    {

	$fields = array("m.*,u.fname,u.lname,u.id as uID,u.birthdate,u.userType,u.avatar,u.sex,u.username,count(m.id) cnt");    

	$tables = array($this->messages.' as m',$this->userprofile.' as u');

	$where  = array("m.senderId=u.id");	

        $where[]= " ((m.recieverId ='".$id."' and  m.senderId ='".$userId."' AND m.deleteSent='0') OR (m.recieverId ='".$userId."' and  m.senderId ='".$id."' AND m.deleteInbox='0'))";

	$sql    = $this->SelectData($fields,$tables,$where,$order = array("m.date DESC"),$group=array(),$limit,$offset,0); 

	$result = $this->FetchAll($sql); 

	return $result['cnt'];	 

    }



    function getOnlyUserIdFromCommunicationByUserIdAlerts($userId,$limit='',$offset='')

    {

	$fields = array("senderId,recieverId");    

	$tables = array($this->messages);

        $where  = array(" (((senderId ='".$userId."' AND deleteSent='0') OR (recieverId ='".$userId."' AND deleteInbox='0')) AND readInbox='0')");

        $sql    = $this->SelectData($fields,$tables, $where, $order = array("date DESC"), $group=array(),$limit,$offset,0); 

        $result = $this->FetchAll($sql); 

        return $result;  

     }

	

     function getOnlyUserIdFromCommunicationByUserId($userId,$limit='',$offset='')

     {

	$fields = array("senderId,recieverId");    

	$tables = array($this->messages);

        $where  = array(" ((senderId ='".$userId."' AND deleteSent='0') OR (recieverId ='".$userId."' AND deleteInbox='0'))");

        $sql    = $this->SelectData($fields,$tables, $where, $order = array("date DESC"), $group=array(),$limit,$offset,0); 

        $result = $this->FetchAll($sql); 

        return $result;  

      }	



      function deleteAllMessagesByUserId($userId){

      		$sql = "UPDATE ".$this->messages." SET deleteSent='1',readInbox='0' where recieverId='".$userId."'";

	 	$result1= $this->ExecuteQuery($sql);	



	 	$sql1 = "UPDATE ".$this->messages." SET deleteInbox='1',readInbox='0' where senderId='".$userId."'";

	 	$result2= $this->ExecuteQuery($sql1);	 

      }

      

    function checkMessageInboxCount($userId){
        $fields = array("count(*) as messageInboxCount");    
        $tables = array($this->messages);
        $where  = array("recieverId ='".$userId."' AND readInbox='0' AND deleteInbox='0' AND deleteSent='0'");
        $sql    = $this->SelectData($fields,$tables, $where, $order = array("date DESC"), $group=array(),$limit='',$offset='',0); 
        $result = $this->FetchRow($sql); 
        return $result['messageInboxCount']; 
    }

      function getUnreadMessageCountByUserId($profileID,$userId){

      	$fields = array("count(*) as messageInboxCount");    

	$tables = array($this->messages);

        $where  = array("senderId='".$profileID."' AND recieverId ='".$userId."' AND readInbox='0' AND deleteInbox='0' AND deleteSent='0'");

        $sql    = $this->SelectData($fields,$tables, $where, $order = array("date DESC"), $group=array(),$limit,$offset,0); 

        $result = $this->FetchRow($sql); 

        return $result['messageInboxCount']; 

      }

     function getAllDraftMessagesByUserId($userId,$search,$limit='',$offset=''){    

        $fields = array("m.*","t.fname,t.lname","u.email");    

        $tables = array($this->messages.' as m',$this->user.' as u',$this->trader.' as t');

        $where  = array("m.senderId ='".$userId."'  and m.deleteInbox='0' and m.draftm='1' and m.senderId=u.userId and m.senderId=t.userId");

        if(trim($search)!='') {

            $where []="  ( m.subject LIKE '%".trim($search)."%' or m.message LIKE '%".trim($search)."%' )";

        }

        $sql    = $this->SelectData($fields,$tables, $where, $order = array("m.date ASC"), $group=array(),$limit,$offset,0); 

        $result = $this->FetchAll($sql); 

        return $result;      

        

    }

    

 



    

    function getMessageDetailsByMessageId($messageId)

    {

        $fields = array("m.*");    

        $tables = array($this->messages.' as m');

        $where  = array("m.id ='".$messageId."'");

        $sql    = $this->SelectData($fields,$tables, $where, $order = array("m.date ASC"), $group=array(),$limit,$offset,0); 

        $result = $this->FetchRow($sql); 

        return $result;         

    }

    

    function getAllContactsByUserIdOld($userId,$search)

    {

        $fields = array();    

        $tables = array($this->userfriends);

        $where = array("status = '2'");

        $where[] = "(userid = '".$userId."' or   frndid = '".$userId."' ) and reqstatus = '2'"; 

        if($search!='')

        {

                $where[] = "(contactname LIKE  '%".$search."%')"; 

        }   

        $result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 

        $result  = $this->FetchAll($result1); 

        return $result;

    }

    

    function getAllContactListByUserId($userId,$search,$limit='',$offset='')

    {         

        $fields = array("");    

        $tables = array($this->contacts);

        $where  = array("userId ='".$userId."'");

        if(trim($search)!='') {

            $where []="  ( detail LIKE '%".trim($search)."%')";

        }

        $sql    = $this->SelectData($fields,$tables, $where, $order = array("date ASC"), $group=array(),$limit,$offset,0); 

        $result = $this->FetchAll($sql); 

        return $result;         

    }

    

    function addContacts($array)

    {

        $this->InsertData($this->contacts, $array);  

    }

    

    function getTotalUnreadMessages($userId) {

        $fields = array("count(id) as totalinboxmessages");    

        $tables = array($this->messages);

        $where = array("status = '2' and inboxm='1' and readstatus='n'");

        $where[] = "(recieverId = '".$userId."')";    

        $result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 

        $result  = $this->FetchRow($result1); 

        return $result;

    }

    function GetUnreadMessageInboxByuserId($userId){
        $fields = array('*,DATE_FORMAT(date,"%b %d, %Y %h:%i %p") as Date_Formatted');    
        $tables = array($this->messages);
        $where  = array("recieverId ='".$userId."' AND readInbox='0' AND deleteInbox='0' AND deleteSent='0'");
        $sql    = $this->SelectData($fields,$tables, $where, $order = array("date DESC"), $group=array(),$limit='',$offset='',0); 
        $result = $this->FetchAll($sql); 
        return $result; 
    }

}

?>

