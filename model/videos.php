<?php
/**************************************************/
## Class Name - Model_Users (Contains all the functions related user and user profile)
/**************************************************/
class Model_videos extends Database {	
	
	## Constructor
	/******************/
	function Model_videos() {
		$this->videos = VIDEOS;		
		$this->userprofile = USERPROFILE;
		$this->videoalbum = VIDEOALBUM;
		$this->videovote = VIDEOVOTE;
		$this->albumvideoscomment = ALBUMVIDEO_COMMENTS;
		$this->videolikes  = VIDEOLIKE;
		$this->playatable = DAY_PLAYA;	
		$this->videoscomment = VIDEO_COMMENT;	
		$this->Database();
	}

	/****************** Videos Functions ********************************/

	## Add album details
	/******************/
	function addAlbum($array) {
		$this->InsertData($this->videoalbum,$array);		
		$insertId = mysql_insert_id();
		return $insertId;
	}

	## Delete user by album
	/******************/
	function deleteAlbumById($id){
		$this->DeleteData($this->videoalbum,"id",$id);
	}

	## Delete album with multiple videos
	/******************/
	function deleteMultipleVideos($albumId) {
		$sql = "DELETE FROM ".$this->videos." WHERE albumId IN (".$albumId.")";
		$result1= $this->ExecuteQuery($sql);	 
	}

	##
	/******************/
	function getAlbumDetailsByAlbumId($albumId,$limit='',$offset='') {
		$fields = array();
		$tables = array($this->videoalbum);	
		$where=array("id='".$albumId."'");
		$order = array();
		$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(), $limit='', $offset=0,0);
		$result= $this->FetchRow($result1); 
		return $result;		
	}

	## Album name name exist or not	
	/******************/
	function checkAlbumName($title) {
		$fields=array();
		$tables=array($this->videoalbum);
		$where=array("title='".$title."'");
		$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 
		$result= $this->FetchRow($result1);
		return $result;		
	}

	## Get user visibility details by userid
	## Getting All videos
	/******************/
	function listVideodetailsByUserID($condition, $limit, $offset, $is_page) {
		$where = "".$condition." ";	
		$sql = "SELECT * FROM ".$this->videos." WHERE ".$where." ORDER BY id ASC";
		## Paggination condition
		if($is_page == true)
			$sql.=" LIMIT ".$offset.",".$limit;			 
			$result1= $this->ExecuteQuery($sql);
			$result= $this->FetchAll($result1);
			return $result;
	}	

	## Delete videos by id
	/******************/
	function deletevideosimageById($videoId){
		$this->DeleteData($this->videos,"id",$videoId);
	}

	## Add videos in database
	/******************/
	function addUservideosByValue($Array) {
		$this->InsertData( $this->videos , $Array);		
		$insertId = mysql_insert_id();
		return $insertId;
	}	

	## Get user visibility details by userid
	/******************/
	function getvideosDetailsByUserId($albumId,$orderField='',$orderBy='',$limit,$offset) {
		$fields=array();	
		$tables=array($this->videos);
		$where = array("albumId ='".$albumId."'");
		$result1 = $this->SelectData($fields,$tables, $where, $order=array("id ASC"), $group=array("id"),$limit,$offset,0);
		$result= $this->FetchAll($result1); 
		return $result;		
	}

	## 
	/******************/
	function getAllAlbumVideosByAlbumAndUserProfileId($userProfileId,$album,$limit='',$offset='') {
		$fields = array();	
		$tables = array($this->videos);
		$where=array("user_id='".$userProfileId."' AND album='".$album."' AND id='2'");
		$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array($album),$limit,$offset,0); 
		$result  = $this->FetchAll($result1); 	
		return $result;
	}



	function getAllAlbumVideosByAlbumId($albumId,$limit='',$offset='') {

	$fields = array();	

	$tables = array($this->videos);

	$where=array("albumId='".$albumId."'");

	$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit,$offset,0); 

	$result  = $this->FetchAll($result1); 	

	return $result;

	}



	## Get video's details by album Id

	function getVideoDetailsByAlbumId($albumId,$limit='',$offset='') {

	$fields = array();	

	$tables = array($this->videos);

	$where=array("albumId='".$albumId."'");

	$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit,$offset,0); 

	$result  = $this->FetchRow($result1); 	

	return $result;

	}



	## Take video count

	function getVideoCountCount($albumId) {

  	$fields=array("count(*) as cnt");	

  	$tables=array($this->videos);

  	$where = array("albumId ='".$albumId."'");

  	$order = array("");

  	$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(), $limit='', $offset='0',0);

  	$result= $this->FetchRow($result1); 

  	return $result['cnt'];		

	}



	function getvideosDetailsByimageId($id) {

	$fields=array();	

	$tables=array($this->videos);

	$where=array("md5(id)='".$id."'");		

	$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 

	$result= $this->FetchRow($result1); 

	return $result;		

	}



	## Get video list by User Id

	function getVideosListByUserId($uId,$limit='',$offset='') {

	$fields = array();

	$tables = array($this->videos);	

	$where = array("user_id ='".$uId."'");

	$order = array();

	$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array("albumId DESC"), $limit='', $offset=0,0); 

	$result= $this->FetchAll($result1); 

	return $result;		

	}

	

	## Edit user by userid

	function editUservideosByValue($array, $Id){

	$this->UpdateData($this->videos,$array,"user_id",$Id,0);

	}

	

	## Edit user by userid

	function editUservideosByValueimageid($array, $Id){

	$this->UpdateData($this->videos,$array,"id",$Id,0);

	}

	## Update video status with multiple videoId

	function updateMultipleVideoStatus($videoID, $status) {

	$sql = "UPDATE ".$this->videos." SET status='".$status."'WHERE id IN (".$videoID.")";

	$result1= $this->ExecuteQuery($sql);	 

	}

	

    	## Delete video with multiple videoId

	function deleteMultipleVideo($videoID) {

	$sql = "DELETE FROM ".$this->videos." WHERE id IN (".$videoID.")";

	$result1= $this->ExecuteQuery($sql);	 

	}

	

	## Update video status by videoId 

	function updateVideoStatus($videoID, $status) {		

	$sql = "UPDATE ".$this->videos." SET status='".$status."' WHERE id = '".$videoID."'";

	$result1= $this->ExecuteQuery($sql);	 

	}

	## Delete video by id

	function deleteVideo($videoID) {

	$sql = "UPDATE ".$this->videos." SET status='0' WHERE id = '".$videoID."'";

	$result1= $this->ExecuteQuery($sql);

	}

	function getvideosDetailsById($id) {

	$fields=array();	

	$tables=array($this->videos);

	$where=array("id='".$id."'");		

	$result1 = $this->SelectData($fields,$tables, $where, $order = array("id ASC"), $group=array(),$limit = "",0,0); 

	$result= $this->FetchRow($result1); 

	return $result;		

	}



	function getAlbumPreviousVideoByAlbumIdAndVideoId($albumId,$userId,$iPid){

		$fields=array();	

		$tables=array($this->videos);

		$where=array("albumId='".$albumId."' AND id < ".$iPid." AND user_id='".$userId."'");		

		$result1 = $this->SelectData($fields,$tables, $where, $order = array("id DESC"), $group=array(),$limit = "1",0,0); 

		$result= $this->FetchRow($result1); 

		return $result['id'];		

	}

	function getAlbumNextVideoByAlbumIdAndVideoId($albumId,$userId,$iPid){

		$fields=array();	

		$tables=array($this->videos);

		$where=array("albumId='".$albumId."' AND id > ".$iPid." AND user_id='".$userId."'");		

		$result1 = $this->SelectData($fields,$tables, $where, $order = array("id ASC"), $group=array(),$limit = "1",0,0); 

		$result= $this->FetchRow($result1); 

		return $result['id'];		

	}

	

	function getAllCommentsByVideoID($videoID){		

		$fields=array("vc.*,u.fname,u.lname,u.email,u.avatar,u.id,username");	

		$tables=array($this->albumvideoscomment." as vc",$this->userprofile." as u");

		$where=array("u.id=vc.userID AND videoID='".$videoID."'");		

		$result1 = $this->SelectData($fields,$tables, $where, $order = array("vc.date DESC"), $group=array(),$limit = "",0,0); 

		$result= $this->FetchAll($result1); 

		return $result;			

	}	

	function addVideoComment($array){			

		$this->InsertData($this->albumvideoscomment,$array);		

		$insertId = mysql_insert_id();

		return $insertId;

	}

	function deleteVideoComment($cid,$iItemId){

		$sql = "DELETE FROM ".$this->albumvideoscomment." WHERE vcID='".$cid."' AND videoID='".$iItemId."'";

		$result1= $this->ExecuteQuery($sql);	 

	}

	/*********************************************************/

	function getVideoLikes($albVidId, $userProfileId)

	{

	$fields=array();	

	$tables=array($this->videolikes);

	$where=array("albumvideoId ='".$albVidId."' and videolikeProfileId ='".$userProfileId."'");		

	$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 

	$result= $this->FetchRow($result1); 

	return $result;	

	}



	// for delete album video like by album video id

	function deleteVideoLike($Id) {

	$this->DeleteData($this->videolikes,"id",$Id);

	}



	// Check album video like count 

	function getAllVideoLikecount($albumvideoId) {

	$fields=array("count(*) as cnt");	

	$tables=array($this->videolikes);

	$where=array("albumvideoId='".$albumvideoId."'");				

	$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 

	$result= $this->FetchRow($result1); 

	return $result['cnt'];		

	}



	// Add album photo likes

	function addToAlbumVideoLikes($array)

	{

	 $this->InsertData($this->videolikes,$array);

	 return mysql_insert_id();

	}



	/******************************************Created By Gaurav Wayal For Admin**************************************************/



	function getVideoAlbumCountByUserId($userId){

	$fields=array("count(*) as AlbumCount");	

	$tables=array($this->videoalbum);

	$where = array("userId='".$userId."'");

	$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(), $limit,$offset,0);

	$result= $this->FetchRow($result1); 

	return $result['AlbumCount'];	

	}

	function deleteMultipleVideoAlbum($videoID) {

	$sql = "DELETE FROM ".$this->videoalbum." WHERE id IN (".$videoID.")";

	$result1= $this->ExecuteQuery($sql);	 

	}

	function getAllVideosAlbumByuserId($userId,$search='',$orderField = 'title',$orderBy = 'ASC',$limit='',$offset='') {

	$fields = array();	

	$tables = array($this->videoalbum);

	$where=array("userId='".$userId."'");

	if($search != '' && $search != 'Search') {

	$where[] = "(title LIKE '%".$search."%')";

	}

	$order = array("$orderField $orderBy");

	$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(),$limit,$offset,0); 

	$result  = $this->FetchAll($result1); 	

	return $result;

	}

	function getAllVideosByuserIdandAlbumId($userId,$albumId,$search='',$orderField = 'title',$orderBy = 'ASC',$limit='',$offset='') {

	$fields = array();	

	$tables = array($this->videos);

	$where=array("user_id='".$userId."' AND albumId='".$albumId."'");

	if($search != '' && $search != 'Search') {

	$where[] = "(title LIKE '%".$search."%')";

	}

	$order = array("$orderField $orderBy");

	$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(),$limit,$offset,0); 

	$result  = $this->FetchAll($result1); 	

	return $result;

	}

	## Take video count

	function getVideoCommentCount($videoId,$userId) {

  	$fields=array("count(*) as cnt");	

  	$tables=array($this->albumvideoscomment);

  	$where = array("videoID ='".$videoId."' AND userID ='".$userId."'");

  	$order = array("");

  	$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(), $limit='', $offset='0',0);

  	$result= $this->FetchRow($result1); 

  	return $result['cnt'];		

	}

   function deleteMultipleVideoLike($videolikeID) {

	$sql = "DELETE FROM ".$this->videolikes." WHERE id IN (".$videolikeID.")";

	$result1= $this->ExecuteQuery($sql);	 

	}

	

	function getAllVideosLikeByuserIdandvideoId($albVidId, $userProfileId,$limit='',$offset='') {

	$fields = array();	

	$tables = array($this->videolikes);

	$where=array("albumvideoId ='".$albVidId."' and videolikeProfileId ='".$userProfileId."'");

	$result1 = $this->SelectData($fields,$tables, $where, $order=array(), $group=array(),$limit,$offset,0); 

	$result  = $this->FetchAll($result1); 	

	return $result;

	}

	

	function deleteMultipleVideoComment($videocommentID) {

	$sql = "DELETE FROM ".$this->albumvideoscomment." WHERE vcID IN (".$videocommentID.")";

	$result1= $this->ExecuteQuery($sql);	 

	}

	

	function deleteVideoCommentbyCommentIs($Id) {

	$this->DeleteData($this->albumvideoscomment,"vcID",$Id);

	}

	

	function getAllVideosCommentByuserIdandvideoId($albVidId, $userProfileId,$limit='',$offset='') {

	$fields = array();	

	$tables = array($this->albumvideoscomment);

	$where=array("videoID ='".$albVidId."' and userID ='".$userProfileId."'");

	$result1 = $this->SelectData($fields,$tables, $where, $order=array(), $group=array(),$limit,$offset,0); 

	$result  = $this->FetchAll($result1); 	

	return $result;

	}

	/******************************************Gaurav Wayal***************************************************/

	## Edit album by albumId

	function editVideoAlbumValueById($array, $Id){

		$this->UpdateData($this->videoalbum,$array,"id",$Id,0);

	}

	function editVideoByAlbumId($array, $Id){

		$this->UpdateData($this->videos,$array,"albumId",$Id,0);

	}

	function getAllVideosByAlbumId($albumId,$limit='',$offset='') {

	$fields = array();	

	$tables = array($this->videos);

	$where=array("albumId='".$albumId."'");

	$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit,$offset,0); 

	$result  = $this->FetchRow($result1); 	

	return $result;

	}
	function getPlayaOfDay()
	{
		$fields = array("*");
		$tables = array($this->playatable);
		$result1 = $this->SelectData($fields,$tables, $where=array(), $order=array("id DESC"), $group=array(), $limit=1,$offset=0,0);
		$result= $this->FetchRow($result1); 
		return $result;
	}
	function getAllPlayaOfDay()
	{	
		$fields=array("a.*, a.id as uId,u.fname,u.lname,u.status as ustatus");	
		$tables=array($this->playatable." as a",$this->userprofile." as u");
		$where = array("a.userId = u.id");
		$order = array("id DESC");
		$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(), 4,0,0);
		$result= $this->FetchAll($result1); 
		return $result;
	}

	function getPlayaOfDayAllvideos()
	{	
		$fields=array("a.*, a.id as uId,concat(u.fname,' ',u.lname) AS fullname,u.status as ustatus,DATE_FORMAT(a.from_date,'%b %d %Y') AS fdate,DATE_FORMAT(a.to_date,'%b %d %Y') AS ldate");	
		$tables=array($this->playatable." as a",$this->userprofile." as u");
		$where = array("a.userId = u.id");
		$order = array("from_date DESC");
		$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(), 0,0,0);
		$result= $this->FetchAll($result1);
		return $result;
	}

	function getAllVideoByWeekly($orderField='', $orderBy='',$limit='',$offset='',$startdate='',$enddate='') 
	{
	$fields=array("DISTINCT a.id as uId,u.fname,u.lname,u.status as ustatus,a.*,DATEDIFF(NOW(),added_date) AS date_diff");	
	$tables=array($this->videos." as a",$this->userprofile." as u");
	
	$where = array("a.status!='0' AND a.user_id=u.userId  AND DATE_FORMAT(added_date,'%Y-%m')>='".$startdate."' AND DATE_FORMAT(added_date,'%Y-%m')<='".$enddate."'");
	
	$order = array("$orderField $orderBy");

	$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(), $limit,$offset,0);
	$result= $this->FetchAll($result1);
	return $result;		
	}

	function getVideoByVideoId($uId){
		$where =array("u.userId = a.user_id AND a.id='".$uId."'");
		$fields=array("u.fname,u.lname,u.status as ustatus,a.*,DATEDIFF(NOW(),added_date) AS date_diff");
		$tables=array($this->videos." as a",$this->userprofile." as u");
		$order = "";
		$limit = "";
		$offset= "";
		$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(), $limit,$offset,0);
		$result= $this->FetchRow($result1); 
		return $result;
	}

	function videoVotingById($videoId,$userProfieId,$voteStatus) {
		$insertData = array();
		$insertData['userid'] = $userProfieId;
		$insertData['videoid'] = $videoId;
		$insertData['dateofvoting'] = date("Y-m-d H:i:s");
		$insertData['vote_status'] = $voteStatus;
		$result_ret = $this->InsertData( $this->videovote, $insertData);
		return $result_ret;
	}

	function getVotingCountByUserId($userProfileId,$uID){
		$where =array("videoid ='".$uID."' AND userid ='".$userProfileId."'");
		$fields=array("count(*) AS voting");
		$tables=array($this->videovote);
		$order = "";
		$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(), $limit,$offset,0);
		$result= $this->FetchRow($result1); 
		return $result;
	}

	function getVotingPercentByVideoId($uID){
		$where =array("videoid ='".$uID."'");
		$fields=array("count(*) AS voting");
		$tables=array($this->videovote);
		$order = "";
		$limit = "";
		$offset ="";
		$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(), $limit,$offset,0);
		$totalvoting = $this->FetchRow($result1); 

		
		$where =array("videoid ='".$uID."' AND vote_status='0'");
		$fields=array("count(*) AS votes ");
		$tables=array($this->videovote);
		$order = "";
		$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(),'','',0);
		$active_voting= $this->FetchRow($result1); 

		$percent = ($totalvoting['voting'] - $active_voting['votes']) * 100 / $totalvoting['voting'];
		
		$arr = array('total'=>$totalvoting['voting'],'voteper'=>round($percent));
		return $arr;
	}

	function clearVotingData(){
		$sql = "DELETE FROM ".$this->videovote;
		$result1= $this->ExecuteQuery($sql);	
	}

	function getPlayaOfDayById($vId)
	{
		$where =array("u.id = a.userId AND a.id='".$vId."'");
		$fields=array("DISTINCT u.username,u.fname,u.lname,u.age,a.*");
		$tables=array($this->playatable." as a",$this->userprofile." as u");
		$order = '';
		$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(), $limit,$offset,0);
		$result= $this->FetchRow($result1); 
		return $result;
	}

	function getAllVideo($orderField ='', $orderBy = '',$limit='',$offset='',$sports_id=0,$change_sport=0) 
	{
	
	$fields=array("DISTINCT a.id as uId,u.fname,u.lname,u.status as ustatus,a.*,DATEDIFF(NOW(),added_date) AS date_diff");	
	$tables=array($this->videos." as a",$this->userprofile." as u");
	
	if($sports_id >0){
	 	$where = array("a.status!='0' AND a.user_id=u.userId AND a.sportsid='".$sports_id."'");	
	}
	else{
		$where = array("a.status!='0' AND a.user_id=u.userId");	
	}
	
	$order = array("$orderField $orderBy");

	$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(), $limit,$offset,0);
	$result= $this->FetchAll($result1);
	return $result;		
	}
	
	function getNomineesVideoByWeekly($orderField ='', $orderBy = 'ASC', $limit='',$offset='',$sports_id=0,$change_sport=0,$startdate='',$enddate='') 
	{
	$fields=array("DISTINCT a.id as uId,u.fname,u.lname,u.status as ustatus,a.*,DATEDIFF(NOW(),added_date) AS date_diff");	
	$tables=array($this->videos." as a",$this->userprofile." as u");
	
	if($sports_id >0){
	 	$where = array("a.status!='0' AND a.userprofile_id=u.id AND DATE_FORMAT(added_date,'%Y-%m')>='$startdate' AND DATE_FORMAT(added_date,'%Y-%m')<='$enddate' AND a.sportsid='".$sports_id."'");	
	}
	else{
		$where = array("a.status!='0' AND a.userprofile_id=u.id AND DATE_FORMAT(added_date,'%Y-%m')>='$startdate' AND DATE_FORMAT(added_date,'%Y-%m')<='$enddate'");

	}

	$order = array("$orderField $orderBy");

	$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(), $limit,$offset,0);
	$result= $this->FetchAll($result1);
	return $result;		
	}

	
	function getvideosByUserId($uId) 
	{

	$fields=array("DISTINCT a.id as uId,u.fname,u.lname,u.status as ustatus,a.*,DATEDIFF(NOW(),added_date) AS date_diff");	
	$tables=array($this->videos." as a",$this->userprofile." as u");

	$where = array("a.status!='0' AND a.userprofile_id = u.id AND a.userprofile_id='".$uId."'");

	$order = array("id DESC");

	$limit = "";
	$offset = "";
	$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(),$limit,$offset,0);

	$result= $this->FetchAll($result1); 

	return $result;		

	}

	function getvideosByUserIdLIMIT($limit, $uId) 
	{
	$fields=array("DISTINCT a.id as uId,u.fname,u.lname,u.status as ustatus,a.*,DATEDIFF(NOW(),added_date) AS date_diff");	
	$tables=array($this->videos." as a",$this->userprofile." as u");

	$where = array("a.status!='0' AND a.userprofile_id = u.id AND a.userprofile_id='".$uId."'");

	$order = array("added_date DESC");

	$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(),$limit,0,0);

	$result= $this->FetchAll($result1); 

	return $result;		

	}

	function getLastPlayaVideoByUserId($userId)
	{
		$where =array("userId='".$userId."'");
		$fields=array("*");
		$tables=array($this->playatable);
		$order = array("id DESC");
		$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(), 1,0,0);
		$result= $this->FetchRow($result1); 
		return $result;
	}
	

	function getPlayaOfDayByUserId($userId)
	{
		$where =array("userId='".$userId."'");
		$fields=array("count(*) AS total");
		$tables=array($this->playatable);
		$order = '';
		$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(),$limit='',0,0);
		$result= $this->FetchRow($result1); 
		return $result;
	}

	function getLastUploadedVideoByUserId($userId)
	{	
		$fields=array('*');	
		$tables=array($this->videos);
		$where =array("user_id='".$userId."'");
		$order =array("id DESC");
		$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(),1,0,0);
		$result= $this->FetchRow($result1); 
		return $result;		
	}

	function addNomineesComments($array)
	{	
		$this->InsertData($this->videoscomment,$array);		
		$insertId = mysql_insert_id();
		return $insertId;
	}
	function getCommentsByNomineesId($limit, $vId){
		$fields=array("DISTINCT cmt.*, DATE_FORMAT(cmt.commentsdate,'%a, %e %b %I:%m %p') as date_formated, user.fname,user.lname,user.avatar,user.id,user.username,cmt.id AS comment_id");	

		$tables=array($this->videoscomment." as cmt",$this->userprofile." as user");

		$where=array("user.id=cmt.userId AND cmt.videoId='".$vId."'");		

		$result1 = $this->SelectData($fields,$tables, $where, $order = array("cmt.commentsdate DESC"), $group=array(),$limit,0,0); 

		$result= $this->FetchAll($result1); 

		return $result;		
	}

	function getLastNomineesComments($lastId){
		$fields=array("cmt.*,DATE_FORMAT(cmt.commentsdate,'%a, %e %b %I:%m %p') as date_formated, user.fname,user.lname,user.id");	
		$tables=array($this->videoscomment." as cmt",$this->userprofile." as user");
		$where=array("user.id=cmt.userId AND cmt.id	='".$lastId."'");	
		$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(),1,0,0);
		$result= $this->FetchRow($result1); 
		return $result;		
	}

	function getFeaturedPlayaData(){
		$fields=array("a.*, a.id as uId,u.fname,u.lname,u.avatar,u.sex,u.sportsID,u.username,u.sports_position,u.sports_club");	
		$tables=array($this->playatable." as a",$this->userprofile." as u");
		$where = array("a.userId = u.id");
		$order = array("id DESC");
		$limit = "";
		$offset = "";
		$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(), $limit=1,$offset=0,0);
		$result= $this->FetchRow($result1); 
		return $result;
	}
	
	/*function getLastHourVideoComments(){
		// echo date('d-m-Y h:i:s');
		$fields=array("cmt.*,cmt.userId AS cmt_user_id,user.fname AS v_user_fname,user.lname AS v_user_lname,user.id,user.email AS v_user_email,v.userprofile_id AS v_user_id");	
		$where=array("user.id = v.userprofile_id AND cmt.videoId=v.id AND cmt.commentsdate >= NOW() - INTERVAL 1 HOUR");
		$tables=array($this->videoscomment." as cmt",$this->userprofile." as user",$this->videos." as v");
		$result1=$this->SelectData($fields, $tables, $where, $order, $group=array(),0,0,0);
		$result=$this->FetchAll($result1); 
		print_r($result);
		return $result;		
	}*/

	function countTotalClickOnVideoByVideoId($vId){
		$fields=array("total_clicks	as cnt");	
		$tables=array($this->videos);
		$where = array("id='".$vId."'");
		$order = "";
		$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(), $limit='', $offset='0',0);
		$result= $this->FetchRow($result1); 
		
		$data['total_clicks'] = $result['cnt']+1;		
		$this->UpdateData($this->videos,$data,"id",$vId,0);

	}

	function getTotalClickOnVideoByVideoId($vId){
		$fields=array("total_clicks	as cnt");	
		$tables=array($this->videos);
		$where = array("id='".$vId."'");
		$order = "";
		$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(), $limit='', $offset='0',0);
		$result= $this->FetchRow($result1); 
		return $result['cnt'];
	}

	function addplayaoftheWeek($array){
		$this->InsertData($this->playatable,$array);		
		$insertId = mysql_insert_id();
		return $insertId;
	}
	function CheckAllreadySelected($last_date){
		$fields=array("count(*) AS cnt");
		$tables=array($this->playatable);
		$where=array("to_date ='".$last_date."'");
		$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(), $limit,$offset,0);
		$result= $this->FetchRow($result1);
		return $result['cnt'];		
	}
	function removePlayaVideosByAdmin($videoId){
		$this->DeleteData($this->playatable,"id",$videoId);
	}
	
	## cron Videos Comments
	function getUploadedVideosUsers(){
		$fields=array("DISTINCT a.user_id,a.userprofile_id, u.email, u.fname,u.lname");	
		$tables=array($this->videos." as a",$this->userprofile." as u ",$this->videoscomment." as cmt " );
		$where = array("u.id = a.userprofile_id AND u.unsubscribe = '0' AND cmt.videoId = a.id AND cmt.cron = '0'");
		$order = array("");
		$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(),$limit,0,0);
		$result= $this->FetchAll($result1); 
		return $result;		
	}

	## get comments users profile
	function getVideoCommentsUsersProfile($user_id,$userprofileid){
		$fields=array("DISTINCT cmt.*,DATE_FORMAT(commentsdate,'%b %D %Y, %h:%i %p') as cmtDate,u.fname,u.sex as gender,u.avatar,u.lname,u.username");	
		$tables=array($this->videos." as a",$this->userprofile." as u ",$this->videoscomment." as cmt ");
		$where = array("u.id=cmt.userId AND a.id=cmt.videoId AND cmt.cron=0 AND a.user_id='".$user_id."' AND cmt.userId NOT IN ('".$user_id."','".$userprofileid."')");
		$order = array("commentsdate DESC");
		$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(),$limit,0,0);
		$result= $this->FetchAll($result1); 
		return $result;		
	}

	## get videos Details By VideoId for Cron
	function getVideosDetailsByVideoId($videoId){
		$fields=array("a.*,albm.title");
		$tables=array($this->videos." as a", $this->videoalbum." as albm");
		$where=array("a.albumId = albm.id AND a.id = '".$videoId."'");
		$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(), $limit,$offset,0);
		$result= $this->FetchRow($result1);
		return $result;		
	}
	## Update Cron Status in Video Comments
	function updateCronStatusByCommentId($id,$status){
		$sql = "UPDATE ".$this->videoscomment." SET cron='".$status."' WHERE id ='".$id."'";
		$result= $this->ExecuteQuery($sql);	 
	}
}
?>