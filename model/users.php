<?php

/**************************************************/

## Class Name - Model_Users (Contains all the functions related user and user profile)

/**************************************************/

class Model_Users extends Database 

{	

	## Constructor

	function Model_Users()

	 {

		$this->user 		  = USERS;

		$this->user_login 	  = USERLOGIN;

		$this->user_url 	  = USERURL; 

		$this->country 		  = COUNTRY;

		$this->posts              = POSTS;

		$this->messagelikes 	  = MESSAGELIKES; 

		$this->state 		  = STATES;	

		$this->userprofile 	  = USERPROFILE;

		$this->profileavatars 	  = PROFILEAVATARS;	

		$this->experience 	  = USEREXPERIENCE;	

		$this->skills 		  = SKILLSEXPERTISE;

		$this->timelineimages 	  = TIMELINEIMAGES;	 

		$this->club 		  = CLUB;

		$this->bio 		  = BIOINFO;	

		$this->education 	  = EDUCATION;	 

		$this->awards 		  = AWARDS;	

		$this->achivements 	  = ACHIVEMENTS;	

		$this->records 		  = RECORDS;	

		$this->participatedevents = PARTICIPATEDEVENTS;	

		$this->profilevisitor	  = PROFILEVISITOR;

		$this->tagusers	  	  = USERFRIENDS;

		$this->coverlikes	  = COVERLIKES;

		$this->follows 		  = FOLLOW_USER;

		$this->likes 		  = LIKE_USER;

		$this->accountsettings 	  = ACCOUNTSETTINGS;

		$this->accountsetvalues   = ACCOUNTSETVALUES;

		$this->timeline           = TIMELINE;

		$this->timeline_assets    = TIMELINE_ASSETS;

		$this->follow_mail        = FOLLOW_MAIL;

		$this->coupons           =COUPONS;

        $this->address           = USERADDRESS;

   		$this->events            = EVENTS;

   		$this->invitation_emails = INVITATION_EMAILS;

   		$this->wishlist = WISHLIST;
   		

		$this->Database();

	}
    ##Get Details For Buddy Requet Mail
    function getUnsubscribeUserDetailsByUserProfileId($id,$field) {

	 $fields=array();	

	 $tables=array($this->userprofile);

	 $where=array("id='".$id."' AND ".$field." = '0'");		

	 $result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 

	 $result= $this->FetchRow($result1); 

	 return $result;		

	}

    ##unsubscribe User
    function UnsubscribeUserbyUserid($userId,$field)
    { 
        $sql = "UPDATE ".$this->userprofile." SET ".$field." ='1' where id ='".$userId."'"; 
    	$result = $this->ExecuteQuery($sql);
    
         return $result;
    }
 
     ##Update Email Subscriptions
    function UpdateUserEmailSubscriptions($userId,$weekly_follow,$buddy_mails,$follow_mail)
    { 
         $sql = "UPDATE ".$this->userprofile." SET unsubscribe='".$weekly_follow."', buddy_unsubscribe='".$buddy_mails."', unsubscribe_follower='".$follow_mail."' where id ='".$userId."'"; 
        
         $result = $this->ExecuteQuery($sql);
    
         return $result;
    }

	/********************************Invite Friends ******************************************************/   

    ##insert emails in to invitation email table
    function addemailtoinvitationemails($array) {

		$this->InsertData($this->invitation_emails,$array); 

		$insertId = mysql_insert_id(); 

		return $insertId;

	}

	##check invitation mail userid & email exists
	function GetinvitationemailsDetailsByEmailanduserid($email,$userid) {
        $fields=array("*");	

		$tables=array($this->invitation_emails);

		$where=array("email='".$email."' AND user_profile_id='".$userid."' ");		

		$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 

		$result= $this->FetchRow($result1); 
		
		return $result;		

	}

	##check invitation mail email exists
	function GetinvitationemailsDetailsByEmail($email) {
        $fields=array("*");	

		$tables=array($this->invitation_emails);

		$where=array("email='".$email."'");		

		$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 

		$result= $this->FetchAll($result1); 
		
		return $result;		

	}

/********************************Invite Friends ******************************************************/

    
    function getDistanceBetweenTwoCoordinates($lat1, $lon1, $lat2, $lon2, $unit) {
		  $theta = $lon1 - $lon2;
		  $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
		  $dist = acos($dist);
		  $dist = rad2deg($dist);
		  $miles = $dist * 60 * 1.1515;
		  $unit = strtoupper($unit);

		  if ($unit == "K")
		   {
		      return ($miles * 1.609344);
		   } 
		  else if ($unit == "N")
		   {
		      return ($miles * 0.8684);
		   }
		  else
		   {
		      return $miles;
		   }
		}

    function CheckAddressByUserId($uID){
	$fields=array("*");	

	$tables=array($this->address);

	$where = array("userid=".$uID);

	$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(), $limit= "",0,0);

	$result= $this->FetchRow($result1); 

	return $result;	

	}
	
	function InsertAddressWithCoordinates($array) {

		$this->InsertData($this->address,$array); 

		$insertId = mysql_insert_id(); 

		return $insertId;

	}

	function editAddressWithCoordinates($array,$userId){

		$result=$this->UpdateData($this->address,$array,"userid ",$userId,0);
        return $result;
	}



    function getAllFollowersByUserId($uID){
	
	$fields=array("*");	

	$tables=array($this->follows);

	$where = array("followUID=".$uID);

	$where = array("status ='0'");

	$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(), $limit= "",0,0);

	$result= $this->FetchAll($result1); 

	return $result;	

	}

	function getAllActiveUserDetails() {

		$fields=array("*");	

		$tables=array($this->userprofile);

		$where=array(" status='2' ");		

		$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 

		$result= $this->FetchAll($result1); 

		return $result;		

    } 

    function getAllActiveClubUserDetails($clubId) {
    	$fields=array("*");	
		$tables=array($this->userprofile);
		$where=array("status='2' AND sports_club='".$clubId."'");		
		$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 
		$result= $this->FetchAll($result1); 
		return $result;		
	} 


	function GetUserProfileDetailsByEmail($email) {

		$fields=array("*");	

		$tables=array($this->userprofile);

		$where=array("email='".$email."' ");		

		$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 

		$result= $this->FetchRow($result1); 

		return $result;		

	}
   
   function getUserCouponByCouponCode($couponcode) {

		$fields=array("");	

		$tables=array($this->coupons);

		$where=array("coupon_code='".$couponcode."' ");		

		$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 

		$result= $this->FetchRow($result1); 

		return $result;		

	}
   function ApproveAdvanceSearchByCoupon($ad_search,$userId)
    { 
         $sql = "UPDATE ".$this->userprofile." SET advance_search ='".$ad_search."' where id ='".$userId."'"; 
         $result = $this->ExecuteQuery($sql);
         return $result;
    }


	function getTimeline($userId) {

		$fields=array("startDate,headline,text,assetId");	

		$tables=array($this->timeline);

		$where=array("userId='".$userId."' ");		

		$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 

		$result= $this->FetchAll($result1); 

		return $result;		

	}

	

	function getAssetsByAssetsId($id) {

		$fields=array("");	

		$tables=array($this->timeline_assets);

		$where=array("assetId='".$id."' ");		

		$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 

		$result= $this->FetchRow($result1); 

		return $result;		

	}



	## Add Details In User TimeLine & TimeLine Assets

	function addContentInTimeLine($array) {

		$this->InsertData($this->timeline,$array); 

		$insertId = mysql_insert_id(); 

		return $insertId;

	}



	function addContentInTimeLineAssets($array) {

		$this->InsertData($this->timeline_assets,$array); 

		$insertId = mysql_insert_id(); 

		return $insertId;

	}



     	 /*****************************************************/

      	function editMystuffsSettings($array,$Id,$settingId){

		$where = "(userId='".$Id."' and settingId='".$settingId."')";

		$this->UpdateDataWithWhere($this->accountsetvalues,$array,$where,0); 

	}



	#Add Account Set Values

	function addAccountSetValues($array) {

		$this->InsertData($this->accountsetvalues,$array); 

		$insertId = mysql_insert_id(); 

		return $insertId;

	}



	function getSettingStatusBySettinIdAndUserId($userId,$settingId) {

		$fields=array();	

		$tables=array($this->accountsetvalues);

		$where=array("(userId='".$userId."' AND  settingId='".$settingId."')");		

		$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 

		$result= $this->FetchRow($result1); 

		return $result;		

	}



	## get facebook users by fbuid

	function fetchFacebookUserByfbuid($fbuid){

		$fields=array();	

		$tables=array($this->user);

		$where = array("fbUID='".$fbuid."' and status='2'");

		$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(), $limit,$offset,0);

		$result= $this->FetchRow($result1); 

		return $result;	

	}



	## get facebook users by fbid

	function getuserdetailsByuidandfbid($uid,$fid){

		$fields=array();	

		$tables=array($this->user);

		$where = array("userId='".$uid."'and fbUID='".$fid."' and status='2'");

		$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(), $limit,$offset,0);

		$result= $this->FetchRow($result1); 

		return $result;	

	}



	## Add skills

	function addUserSkills($array) {

		$this->InsertData($this->skills,$array); 

		$insertId = mysql_insert_id(); 

		return $insertId;

	}



	 #Add Experiences

	function addUserExperiences($array) {

		$this->InsertData($this->experience,$array); 

		$insertId = mysql_insert_id(); 

		return $insertId;

	}



	#Add Bio

	function addBioInfo($array) {

		$this->InsertData($this->bio,$array); 

		$insertId = mysql_insert_id(); 

		return $insertId;

	}



	#Add Awards

	function addAwardsInfo($array) {

		$this->InsertData($this->awards,$array); 

		$insertId = mysql_insert_id(); 

		return $insertId;

	}



	## Edit coverimage by id

	function editCoverimageValueById($array, $Id){

		$this->UpdateData($this->timelineimages,$array,"id",$Id,0);

	}



	#Add Achivements

	function addAchivementsInfo($array) {

		$this->InsertData($this->achivements,$array); 

		$insertId = mysql_insert_id(); 

		return $insertId;

	}



	#Add Records

	function addRecordsInfo($array) {

		$this->InsertData($this->records,$array); 

		$insertId = mysql_insert_id(); 

		return $insertId;

	}



	#Add Education

	function addEducationInfo($array) {

		$this->InsertData($this->education,$array); 

		$insertId = mysql_insert_id(); 

		return $insertId;

	}



	## Get list of skills details

	function getUserSkillsByUserId($id) {

		$fields=array("");	

		$tables=array($this->skills);

		$where=array("userId='".$id."' ");		

		$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 

		$result= $this->FetchAll($result1); 

		return $result;		

	}



	function getUserSkillsDetByprofiled($id) {

		$fields=array("");	

		$tables=array($this->skills);

		$where=array("userId='".$id."' ");		

		$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 

		$result= $this->FetchRow($result1); 

		return $result;		

	}



	## Get user bio details by userid

	function getBioDetailsByUserId($id) {

		$fields=array("");	

		$tables=array($this->bio);

		$where=array("userId='".$id."' ");		

		$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 

		$result= $this->FetchRow($result1); 

		return $result;		

	}



	## Get user awards details by userid

	function getAwardsDetailsByUserId($id) {

		$fields=array("");	

		$tables=array($this->awards);

		$where=array("userId='".$id."' ");		

		$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 

		$result= $this->FetchRow($result1); 

		return $result;		

	}



	## Get user achivements details by userid

	function getAchivementsDetailsByUserId($id) {

		$fields=array("");	

		$tables=array($this->achivements);

		$where=array("userId='".$id."' ");		

		$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 

		$result= $this->FetchRow($result1); 

		return $result;		

	}



	## Get user records details by userid

	function getRecordsDetailsByUserId($id) {

		$fields=array("");	

		$tables=array($this->records);

		$where=array("userId='".$id."' ");		

		$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 

		$result= $this->FetchRow($result1); 

		return $result;		

	}

	

	## Get user education details by userid

	function geteducationDetailsByUserId($id) {

		$fields=array("");	

		$tables=array($this->education);

		$where=array("userId='".$id."' ");		

		$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 

		$result= $this->FetchRow($result1); 

		return $result;		

	}



	function checkBioCountExists($userID) {

	  	$fields=array("count(*) as cnt");	

	  	$tables=array($this->bio);

	  	$where = array("userId='$userID'");

	  	$order = array("");

	  	$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(), $limit='', $offset='0',0);

	  	$result= $this->FetchRow($result1); 

	  	return $result['cnt'];		

	}

	function checkClubInfoExists($userID) {

	  	$fields=array("count(*) as cnt");	

	  	$tables=array($this->club);

	  	$where = array("userID='$userID'");

	  	$order = array("");

	  	$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(), $limit='', $offset='0',0);

	  	$result= $this->FetchRow($result1); 

	  	return $result['cnt'];		

	}



	function checkEducationCountExists($userID) {

	  	$fields=array("count(*) as cnt");	

	  	$tables=array($this->education);

	  	$where = array("userId='$userID'");

	  	$order = array("");

	  	$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(), $limit='', $offset='0',0);

	 	$result= $this->FetchRow($result1); 

	 	return $result['cnt'];		

	}



	function checkAwardsCountExists($userID) {

	  $fields=array("count(*) as cnt");	

	  $tables=array($this->awards);

	  $where = array("userId='$userID'");

	  $order = array("");

	  $result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(), $limit='', $offset='0',0);

	  $result= $this->FetchRow($result1); 

	  return $result['cnt'];		

	}



	function checkAchivementsCountExists($userID) {

	  $fields=array("count(*) as cnt");	

	  $tables=array($this->achivements);

	  $where = array("userId='$userID'");

	  $order = array("");

	  $result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(), $limit='', $offset='0',0);

	  $result= $this->FetchRow($result1); 

	  return $result['cnt'];		

	}



	function checkRecordsCountExists($userID) {

	  $fields=array("count(*) as cnt");	

	  $tables=array($this->records);

	  $where = array("userId='$userID'");

	  $order = array("");

	  $result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(), $limit='', $offset='0',0);

	  $result= $this->FetchRow($result1); 

	  return $result['cnt'];		

	}

		

	## Add login details database

	function addLoginDetailsByValue($Array) {

	$this->InsertData( $this->user_login , $Array);		

	$insertId = mysql_insert_id();

	return $insertId;

	}

	

	## Add user URL 

	function addUserUrlDetailsByValue($Array) {

	$this->InsertData( $this->user_url , $Array );		

	$insertId = mysql_insert_id();

	return $insertId;

	}

	

	## Edit user url by logid

	function editUserLogValueByLogId($array, $Id){

	$this->UpdateData($this->user_url,$array,"urlID",$Id,0);

	}

	

	## Delete user URL 

	function deleteURLDetailsByValue($uID) {

	$this->DeleteData($this->user_url,"uID",$uID);

	}

	

	## Delete user URL 

	function deleteURLDetailsByURLID($urlID) {

        $this->DeleteData($this->user_url,"urlID",$urlID);

	}



	## Get user experience details by userid

	function getExperienceListsByUserId($id) {

	$fields=array("");	

	$tables=array($this->experience);

	$where=array("userId=".$id." ");		

	$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 

	$result= $this->FetchAll($result1); 

	return $result;		

	}



	function getExperienceDetailsByUserProfileId($id) {

	$fields=array("");	

	$tables=array($this->experience);

	$where=array("userId=".$id." ");		

	$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 

	$result= $this->FetchRow($result1); 

	return $result;		

	}

	

	## Get experience details by exp id

	function getExperienceDetailsById($id) {

	$fields=array("");	

	$tables=array($this->experience);

	$where=array("id=".$id." ");		

	$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 

	$result= $this->FetchRow($result1); 

	return $result;		

	}

	

	## Delete user Experience

	function deleteUserExperienceDetails($id) {

        $this->DeleteData($this->experience,"id",$id);

	}



	## get user URL detail by user id

	function getUrlDetails($USERid,$URLid,$URLtype) {

	$fields=array();	

	$tables=array($this->user_url);

	$where=array("(urlID='".$URLid."' AND  urlType='".$URLtype."' AND  uID ='".$USERid."')");		

	$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 

	$result= $this->FetchRow($result1); 

	return $result;		

	}

	

	## get user URL detail by user id

	function getUserURLDetailByUserID($uID) {

	$fields=array();	

	$tables=array($this->user_url);

	$where=array("uID='".$uID."'");		

	$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 

	$result= $this->FetchRow($result1); 

	return $result;		

	}

	

	function updateLoginDetailsByValue($Array,$Id)

	{

	$this->UpdateData($this->user_login,$Array,"login_id",$Id,0);

	$insertId = mysql_insert_id();

	return $insertId;

	}

	

	## Delete User Login History(Single Record)	

	function deleteUserLoginHistory($login_id){

	$this->DeleteData($this->user_login,"login_id",$login_id);

	}

	

	## Delete User Login History(Multiple Record)	

	function deleteLoginHistory($login_ids){

	$sql = "DELETE FROM ".$this->user_login." WHERE login_id IN (".$login_ids.")";		

	$result1= $this->ExecuteQuery($sql);	 

	}

	

	## Check user login

	function getUserValueByDetailsuNameAndpaswd($uName, $paswd) {

	$fields=array();	

	$tables=array($this->user);

	$where=array("(username='".$uName."'  AND  password='".md5($paswd)."')");		

	$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0); 

	$result= $this->FetchRow($result1); 

	return $result;		

	}

	

	## Check user login by email and paswd

	function getUserValueByDetailsUsernameAndPassword($username, $paswd) {

	$fields=array();	

	$tables=array($this->user);

	$where=array("(username='".$username."'  AND  password='".md5($paswd)."') AND ( userType IN (3,4,5))");		

	$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 

	$result= $this->FetchRow($result1); 

	return $result;		

	}

	

	## Check user login by email and paswd

	function getUserValueByDetailsUsernameAndPasswordForAdmin($username, $paswd) {

	$fields=array();    

	$tables=array($this->user);

	$where=array("(username='".$username."'  AND  password='".md5($paswd)."')");        

	$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 

	$result= $this->FetchRow($result1); 

	return $result;        

	}

    

	## Check user login by email and paswd

	function getUserValueByDetailsEmailAndpaswdForSiteUser($email, $paswd) {

	$fields=array();	

	$tables=array($this->user);

	$where=array("(email='".$email."'  AND  password='".md5($paswd)."' AND userType='3')");		

	$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 

	$result= $this->FetchRow($result1); 

	return $result;		

	}

	

	## Get user details by userid

	function getUserDetailsByUserId($id) {

	$fields=array("");	

	$tables=array($this->user);

	$where=array("userId='".$id."' ");		

	$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 

	$result= $this->FetchRow($result1); 

	return $result;		

	}

	

	## Get user details by userid

	function getUserDetails($id) {

	$fields=array("u.*,c.cntrName,s.statName");	

	$tables=array($this->user. " as u",$this->country. " as c",$this->state. " as s");

	$where=array("(u.userId='".$id."') AND u.state = s.statID AND u.country= c.cntrID");		

	$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 

	$result= $this->FetchRow($result1); 

	return $result;		

	}			

	

	//Function to get user name and email

	function getuNameAndEmail($id) {

	$fields = array("user.userId,user.fname,user.lname,user.email");	

	$tables = array("$this->user as user");

	$where  = array("userId='".$id."'");		

	$sql    = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 

	$result = $this->FetchRow($sql); 

	return $result;		

	}



	## Get user detsils using User Id & User Type

	function getUserDetailsByUserIdAndUserType($id,$userType) {

	$fields = array("user.memberType,user.paidValue");	

	$tables = array("$this->user as user");

	$where=array("userId='".$id."'  AND  userType='".$userType."'");		

	$sql    = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0);

	$result = $this->FetchRow($sql); 

	return $result;		

	}

	

	## Get user details by userid

	function getAllUsers($search, $orderBy = 'ASC', $limit='',$offset='') {

	$fields=array();	

	$tables=array($this->user);

	$where = array(" userType = '3' ");

	if($search != '' && $search != 'Email') {

	$where[] = "(concat(fname,' ',lname) LIKE '%".$search."%' OR email LIKE '%".$search."%' )";

	}

	$order = array("email ".$orderBy);

	$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(), $limit="",$offset,0);

	$result= $this->FetchAll($result1); 

	return $result;		

	}

	

	## Get user details by userid

	function getRecentRegisterdUsersByType($userType,$search, $orderBy, $limit='',$offset='') {

	$fields=array();	

	$tables=array($this->user);

	$where  = array("userType='".$userType."' AND status ='2'");

	$result1 = $this->SelectData($fields,$tables, $where, $order = array("regDate DESC"), $group=array(), $limit="",$offset,0);

	$result= $this->FetchAll($result1); 

	return $result;		

	}

	

	## Get all sub users

	function getAllSubUsers($search,$orderField='', $orderBy = 'ASC', $limit='',$offset='') {

	$fields=array();	

	$tables=array($this->user);

	$where = array(' userType = "2" AND status!="0" ');

	if($search != '' && $search != 'Email') {

		$where[] = '(concat(fname," ",lname) LIKE "%'.$search.'%" OR email LIKE "%'.$search.'%" )';

	}

	$order = array("$orderField $orderBy");

	$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(), $limit,$offset,0);

	$result= $this->FetchAll($result1); 

	return $result;		

	}

	

	## Add login details database

	function getLastLoginDetailsByUserId($userId) {

	$fields=array();

	$tables=array($this->user_login);

	$where=array("user_id='".$userId."'");

	$result1 = $this->SelectData($fields,$tables, $where, $order = array("loginTime DESC"), $group=array(), 1,0,0);

	$result= $this->FetchRow($result1);

	return $result;		

	}

	

	## check email address is present

	function checkUserEmailPresent($email) {

	$fields=array("userId,salt,email,status,userType");	

	$tables=array($this->user);

	$where=array("email ='".$email."'");		

	$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 

	$result= $this->FetchRow($result1); 

	return $result;		

	}

	

	## Get user details by userid

	function getUserProfileDetailsByUserId($id) {

	$fields=array("");	

	$tables=array($this->user);

	$where=array("userId = '".$id."'");		

	$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 

	$result= $this->FetchRow($result1); 

	return $result;		

	}

	

	## Checking wheather user enetered email is already exists or not 

	function checkUserEmailExists($email,$id){

	$fields = array();

	$tables = array($this->user);

	if($id){

		$where  = array("email='".$email."' AND userId!= '".$id."' AND status!='0'");

	}else{

		$where  = array("email='".$email."' AND status!='0'");

	}

	$result = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 

	$result1 = $this->FetchRow($result);

	return $result1;

	}



	function checkAdminUserEmailExists($email){

	$fields = array();

	$tables = array($this->user);

	$where  = array("email='".$email."' AND userType ='1' AND status!='0'");

	$result = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 

	$result1 = $this->FetchRow($result);

	return $result1;

	}



	## Checking old password 

	function checkUsersOldPassword($oldpass,$id){

	$fields = array();

	$tables = array($this->user);		

	$where  = array("password='".$oldpass."' AND userId = '".$id."'");		

	$result = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 

	$result1 = $this->FetchRow($result);

	return $result1;

	}

	

	## check Admin email address is present

	function checkAdminEmailPresent($email) {

	$fields=array("userId, email, userType");	

	$tables=array($this->user);

	$where=array("email ='".$email."'","userType IN (1,2)");		

	$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 

	$result= $this->FetchRow($result1); 

	return $result;		

	}

	

	## Add user in database

	function addUserByValue($Array) {
	$this->InsertData( $this->user , $Array );		

	$insertId = mysql_insert_id();

	return $insertId;

	}

		

	## Edit user by userid

	function editUserValueById($array, $Id){

	$this->UpdateData($this->user,$array,"userId",$Id,0);

	}



	function editUserProfile($array, $Id){

	$this->UpdateData($this->userprofile,$array,"userId",$Id,0);

	}



	## Get user prodile image by userid

	function getAdminUserProfileImageByUserId($id) {

	$fields=array("avatar");	

	$tables=array($this->user);

	$where=array("userId='".$id."'");

	$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 

	$result= $this->FetchRow($result1); 

	return $result;		

	}	



	## Delete user by userid

	function deleteUserValueById($id){

	$this->DeleteData($this->user,"userId",$id);

	}

	## fetch last 5 registered user

	function getLasrFiveRegistedUser() {

	$fields=array();	

	$tables=array($this->user);

	$where=array("userType='3'");		

	$result1 = $this->SelectData($fields,$tables, $where, $order = array("regDate DESC"), $group=array(),$limit = "5",0,0); 

	$result= $this->FetchAll($result1); 

	return $result;	

	}

	

	## fetch last 5 registered subuser

	function getLasrFiveRegistedSubUser() {

	$fields=array();	

	$tables=array($this->user);

	$where=array("userType='2'");		

	$result1 = $this->SelectData($fields,$tables, $where, $order = array("regDate DESC"), $group=array(),$limit = "5",0,0); 

	$result= $this->FetchAll($result1); 

	return $result;	

	}

	

	## Update user status with multiple ids

	function updateUserStatus($ids, $status) {

	$sql = "UPDATE ".$this->user." SET status='".$status."' WHERE userId IN (".$ids.")";

	$result1= $this->ExecuteQuery($sql);	 

	}



	function updateUserProfileStatus($ids, $status) {

	$sql = "UPDATE ".$this->userprofile." SET status='".$status."' WHERE userId IN (".$ids.")";

	$result1= $this->ExecuteQuery($sql);	 

	}



	## Delete faq category with multiple ids

	function deleteUsers($ids) {

	$sql = "UPDATE ".$this->user." SET status='0' WHERE userId IN (".$ids.")";

	$result1= $this->ExecuteQuery($sql);	 

	}



	function deleteUserprofile($ids) {

	 $sql = "UPDATE ".$this->userprofile." SET status='0' WHERE userId IN (".$ids.")";

	 

	$sql= $this->ExecuteQuery($sql);	 

	}



	## Get user First name And last Name by userid

	function getuNameByUserId($id) {

	$fields=array('fname','lname');	

	$tables=array($this->user);

	$where=array("userId='".$id."'");		

	$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 

	$result= $this->FetchRow($result1); 

	return $result['fname'].' '.$result['lname'];		

	}

	

	## Get user First name And last Name by userid

	function checkUserStatus($email) {

	$fields=array();	

	$tables=array($this->user);

	$where = array(" email = '".$email."' AND  userType != '1' ");

	$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 

	$result= $this->FetchRow($result1); 

	return $result['status'];		

	}

	

	## Get all sub admin 

	function getAllSubAdmin($search, $userType = '', $orderBy = 'ASC', $limit='',$offset='') {

	$fields=array();	

	$tables=array($this->user);

	$where = array(" userType != '1' AND userType != '3' ");

	if($userType != '' && $userType != 'User Type'){

		$where[] = "userType = '".$userType."' ";

	}

	if($search != '' && $search != 'Email') {

		$where[] = "(concat(fname,' ',lname) LIKE '%".$search."%' OR email LIKE '%".$search."%' )";

	}

	$order = array("email ".$orderBy);

	$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(), $limit,$offset,0);

	$result= $this->FetchAll($result1); 

	return $result;		

	}

		

	## Get all user by user type

	function getAllUserByUserType($userType) {

	$fields=array();	

	$tables=array($this->user);

	$where = array(" userType = '".$userType."' ");

	$result1 = $this->SelectData($fields,$tables, $where, $order=array('email ASC'), $group=array(), $limit='',$offset='',0);

	$result= $this->FetchAll($result1); 

	return $result;		

	}

	

	function getDetailsByUserId($id) {

	$fields=array();	

	$tables=array($this->user);

	$where=array("userId='".$id."'");		

	$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 

	$result= $this->FetchRow($result1); 

	return $result;		

	}   

	

	 ## Get User Details by userid

	 function getUserDetailsByEmail($email,$userId)

	 {

	 $fields = array("fname,lname,userId");

	 $tables = array($this->user);

	 $where = array("email='".$email."' and status ='2' and userId <> '".$userId."'");

	 $result1 = $this->SelectData($fields,$tables,$where,$order=array(),$group=array(),$limit="",0,0);

	 return $this->FetchRow($result1);

	 }

	 

	//Function to get the details of all users

	function getAllUserDetails() {

	$fields = array("userId,fname,lname,email");	

	$tables = array($this->user);

	$where  = array("userType = '3'");		

	$sql    = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 

	$result = $this->FetchAll($sql); 

	return $result;	

	}

	

	//Function to get the details of all users

	function getAdminDetails() {

	$fields = array();	

	$tables = array($this->user);

	$where  = array("userType = '1'");		

	$sql    = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 

	$result = $this->FetchRow($sql); 

	return $result;	

	}

	

	//Function to check whethr user has alrady have a paswd

	function getUserpaswd($userId) {

	$fields = array("user.password");	

	$tables = array("$this->user as user");

	$where  = array("user.userId = '".$userId."'");		

	$sql    = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 

	$result = $this->FetchRow($sql); 

	return $result['password'];	

	}



	## Check user login by email and password

	function getUserDetailsByUsenameAndPassword($username, $password) {

	$fields=array();	

	$tables=array($this->user);

	$where=array("(email='".$username."'  AND  password='".($password)."')");		

	$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 

	$result= $this->FetchRow($result1); 

	return $result;		

	}

	

    	## Check user login by username  and password

    	function getUserValueByDetailsEmailAndPassword($email, $password) {

        $fields=array();    

        $tables=array($this->user);

        $where=array("(username='".$email."'  AND  password='".md5($password)."')");        

        $result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 

        $result= $this->FetchRow($result1); 

        return $result;        

    	}

    

      	## Check user login by username  

    	function getUserValueByDetailsUsername($username) {

        $fields=array();    

        $tables=array($this->user);

        $where=array("(username='".$username."')");        

        $result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0);

        $result= $this->FetchRow($result1); 

        return $result;        

    	}



    	function getUserProfileDetailsUsername($username) {

        $fields=array();    

        $tables=array($this->userprofile);

        $where=array("(username='".$username."')");        

        $result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 

        $result= $this->FetchRow($result1); 

        return $result;        

    	}

    

	## check Admin buyer email is present

	function checkMyEmailId($email,$uID) {

	$fields=array("email");	

	$tables=array($this->user);

	$where=array("email IN ('".$email."')","userId='".$uID."'");		

	$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 

	$result= $this->FetchRow($result1); 

	return $result;		

	}	



      	function getLoginTimeByLoginId($Id) {

	$fields = array("loginTime,loginIP");	

	$tables = array($this->user_login);

	$where  = array("login_id ='".$Id."'");		

	$sql    = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 

	$result= $this->FetchRow($sql); 

	return $result;	

	}

	

	function getUserLoginHistory($uID,$search_from='', $search_to='',$orderField='loginTime ', $orderBy='DESC', $limit='',$offset=''){

	$fields=array();	

	$tables=array($this->user_login);

	$where = array("user_id ='".$uID."'");	

	if($search_from != '' && $search_from != 'From Date' && $search_to != '' &&  $search_to != 'To Date')

	{	

		$where[] = "(DATE(loginTime ) >='".$search_from."' AND DATE(logOutTime )<='".$search_to."')";	

	} 	

	else if($search_from != '' && $search_from!='From Date'){

		$where[] = "(DATE(loginTime ) >='".$search_from."')";

	}

	else if($search_to != '' && $search_to != 'To Date'){

		$where[] = "(DATE(logOutTime ) <='".$search_to."')";

	}				

	$order = array("$orderField $orderBy");

	$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(), $limit,$offset,0);  

	$result= $this->FetchAll($result1); 

	return $result;	

	}

	

	// Globle check email exist or not	

	function chkUserEmailExist($email, $id='') {

	$fields=array('email');

	$tables=array($this->user);

	$where=array("email='".$email."' AND status!='0'");

	if($id != '') {

	$where[] = " userId!='".$id."'";	

	}

	$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0);

	$result= $this->FetchRow($result1);

	return $result;		

	}

	

	function chkSubAdminUserNameExist($uname,$id=''){

	$fields=array('username');

	$tables=array($this->user);

	$where=array("username='".$uname."' AND status!='0'");

	if($id != '') {

		$where[] = " userId!='".$id."'";	

	}

	$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0);

	$result= $this->FetchRow($result1);

	return $result;			

	}

	

	## Get user details by userid

	function getImageByUserId($id) {

	$fields=array('userId,avatar');	

	$tables=array($this->user);

	$where=array("userId='".$id."'");		

	$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 

	$result= $this->FetchRow($result1); 

	return $result;		

	}



	## check email address is present

	function checkUserEmailAddressorusername($email) {

	$fields=array();	

	$tables=array($this->user);

	$where=array("(email ='".$email."' OR username ='".$email."') AND status='2'");		

	$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 

	$result= $this->FetchRow($result1); 

	return $result;		

	}

	

	function updateUserDetailsByValue($Array,$Id)

	{

	$this->UpdateData($this->user,$Array,"userId",$Id,0);

	$insertId = mysql_insert_id();

	return $insertId;

	}



	function getSiteUserDetailsByUserType($val, $orderField='', $orderBy='', $limit='',$offset=''){

	$fields = array("");

	$tables = array("$this->user as u,$this->userprofile as p");

	$where = array('p.userType= "3"  AND p.status!="0" AND u.status!="0" AND u.userType = p.userType AND u.status = p.status');

	if($val) {

		$where[] = '(concat(p.fname," ",p.lname) LIKE "%'.$val.'%" OR p.email LIKE "%'.$val.'%" ) AND p.userId != "1"';

	}

	else{

		$where[] = 'p.userId!="1"';

	}

	$order = array("$orderField $orderBy");

	$result1 = $this->SelectData($fields,$tables,$where,$order = array("p.regDate DESC"),$group=array("p.email"),$limit,$offset,0);

	$result = $this->FetchAll($result1);

	return $result;

	}



	function getAllSportpersonUsers($limit='',$offset=''){

	$fields = array("");

	$tables = array("$this->user as u,$this->userprofile as p");

	$where = array('p.userType= "3"  AND p.status!="0" AND u.userType = p.userType AND u.status = p.status');

	$result1 = $this->SelectData($fields,$tables, $where, $order = array("p.regDate DESC"), $group=array("p.email"),$limit = "",0,0); 

	$result = $this->FetchAll($result1);

	return $result;

	}



    	function getCoachUserDetailsByUserType($val, $orderField='', $orderBy='', $limit='',$offset=''){

	$fields = array("");



	$tables = array("$this->user as u,$this->userprofile as p");

	$where = array('p.userType= "5"  AND p.status!="0" AND u.userType = p.userType AND u.status = p.status');

	if($val) {

		$where[] = '(concat(p.fname," ",p.lname) LIKE "%'.$val.'%" OR p.email LIKE "%'.$val.'%" ) AND p.userId != "1"';

	}

	else{

		$where[] = 'p.userId!="1"';

	}

	$order = array("$orderField $orderBy");

	$result1 = $this->SelectData($fields,$tables,$where,$order = array("p.regDate DESC"),$group=array("p.email"),$limit,$offset,0);

	$result = $this->FetchAll($result1);

	return $result;

	}



	function getAllCoachUsers($limit='',$offset=''){

	$fields = array("");

	$tables = array("$this->user as u,$this->userprofile as p");

	$where = array('p.userType= "5"  AND p.status!="0" AND u.userType = p.userType AND u.status = p.status');

	$result1 = $this->SelectData($fields,$tables, $where, $order = array("p.regDate DESC"), $group=array("p.email"),$limit = "",0,0); 

	$result = $this->FetchAll($result1);

	return $result;

	}



	function getFansUserDetailsByUserType($val, $orderField='', $orderBy='', $limit='',$offset=''){

	$fields = array("");

	$tables = array("$this->user as u,$this->userprofile as p");

	$where = array('p.userType= "4"  AND p.status!="0" AND u.userType = p.userType AND u.status = p.status');

	if($val) {

		$where[] = '(concat(p.fname," ",p.lname) LIKE "%'.$val.'%" OR p.email LIKE "%'.$val.'%" ) AND p.userId != "1"';

	}

	else{

		$where[] = 'p.userId!="1"';

	}

	$order = array("$orderField $orderBy");

	$result1 = $this->SelectData($fields,$tables,$where,$order = array("p.regDate DESC"),$group=array("p.email"),$limit,$offset,0);

	$result = $this->FetchAll($result1);

	return $result;

	}



	function getAllFanUsers($limit='',$offset=''){

	$fields = array("");

	$tables = array("$this->user as u,$this->userprofile as p");

	$where = array('p.userType= "4"  AND p.status!="0" AND u.userType = p.userType AND u.status = p.status');

	$result1 = $this->SelectData($fields,$tables, $where, $order = array("p.regDate DESC"), $group=array("p.email"),$limit = "",0,0); 

	$result = $this->FetchAll($result1);

	return $result;

	}





	function getClubUserDetailsByUserType($val, $orderField='', $orderBy='', $limit='',$offset=''){

	$fields = array("");

	$tables = array("$this->user as u,$this->userprofile as p");

	$where = array('p.userType= "6"  AND p.status!="0" AND u.userType = p.userType AND u.status = p.status');

	if($val) {

		$where[] = '(concat(p.fname," ",p.lname) LIKE "%'.$val.'%" OR p.email LIKE "%'.$val.'%" ) AND p.userId != "1"';

	}

	else{

		$where[] = 'p.userId!="1"';

	}

	$order = array("$orderField $orderBy");

	$result1 = $this->SelectData($fields,$tables,$where,$order = array("p.regDate DESC"),$group=array("p.email"),$limit,$offset,0);

	$result = $this->FetchAll($result1);

	return $result;

	}



	function getAllClubUsers($limit='',$offset=''){

	$fields = array("");

	$tables = array("$this->user as u,$this->userprofile as p");

	$where = array('p.userType= "6"  AND p.status!="0" AND u.userType = p.userType AND u.status = p.status');

	$result1 = $this->SelectData($fields,$tables, $where, $order = array("p.regDate DESC"), $group=array("p.email"),$limit = "",0,0); 

	$result = $this->FetchAll($result1);

	return $result;

	}



	// Globle check uName exist or not	

	function chkUserNameExist($uName, $id='') {

	$fields=array();

	$tables=array($this->user);

	$where=array("username='".$uName."'");

	if($id != '') {

		$where[] = " userId!='".$id."'";	

	}

	$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0);

	$result= $this->FetchRow($result1);

	return $result;		

	}

	

	## Get user First name And last Name by userid

	 function getUserNameByUserId($id) {

	$fields=array('fname','lname');	

	$tables=array($this->user);

	$where=array("userId='".$id."'");		

	$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 

	$result= $this->FetchRow($result1); 

	return $result['fname'].' '.$result['lname'];		

	}



     function checkpassword($id,$pass) {

        $fields=array("userId,email");

        $tables=array($this->user);

        $where=array("userId='".$id."' AND password ='".md5($pass)."'");

        $result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0);

        $result= $this->FetchRow($result1);

        return $result;         

     }

    

	//for resetpassword admin

      function getUserValueByMdUserId($mdid) {

        $fields=array("userId,email,username");

        $tables=array($this->user);

        $where=array("md5(userId) = '".$mdid."'");

        $result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0);

        $result= $this->FetchRow($result1);

        return $result;         

    }



    function getUserDetailsUsingUsername($uname) {

	$fields=array();

	$tables=array($this->user);

	$where=array("username='".$uname."'");	

	$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 

	$result= $this->FetchRow($result1); 

	return $result;		

	}



       /************* User Profile Details ***********/

	function getUserProfileInformationByUserID($uID,$type) {

	$fields=array();	

	$tables=array($this->userprofile);

	$where  = array("id='".$uID."' AND userType = '".$type."'");	

	$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0);  

	$result= $this->FetchRow($result1); 

	return $result;		

	}



	 function getProfileDetailsUsingUsername($uname) {

	  $fields=array();

	  $tables=array($this->userprofile);

	  $where=array("username='".$uname."'");	

	  $result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 

	  $result= $this->FetchRow($result1); 

	  return $result;		

	}



function getProfileDetailsByUserId($id) {
	if($id!=''){
	$fields=array();	
	$tables=array($this->userprofile);
	$where=array("id='".$id."'");		
	$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0)	; 
	$result= $this->FetchRow($result1); 
	return $result;		
	}
}



	## Get user other profiles by user Id

	function getUserOtherProfiles($id,$type) {

	 $fields=array();	

	 $tables=array($this->userprofile);

	 $where  = array("userId='".$id."' AND userType != '".$type."'");	

	 $result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 

	 $result= $this->FetchAll($result1); 

	 return $result;		

	}



function getUserDetailsInfoByUserId($id) {
	if($id!=''){
	$fields=array("id,userId,userType,email,fname,lname,sex,username,avatar,sportsID");	
	$tables=array($this->userprofile);
	$where=array("id='".$id."'");		
	$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 
	$result= $this->FetchRow($result1); 
	return $result;
	}		
}



	function getUserProfileInformationByUserIDAndUserType($uID,$type) {

	$fields=array();	

	$tables=array($this->userprofile);

	$where  = array("userId='".$uID."' AND userType = '".$type."'");	

	$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 

	$result= $this->FetchRow($result1); 

	return $result;		

	}



function getProfileDetailsByUserProfileId($id) {
	if($id!=''){
	$fields=array();	
	$tables=array($this->userprofile);
	$where=array("id=".$id);		
	$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 
	$result= $this->FetchRow($result1); 
	return $result;		
	}
}



	// Get last 30 days visitors of a user

	function getVisitorsbyUserId($userId,$num=30)

	{

	$fields = array("v.*,u.id,u.fname,u.lname,u.avatar,u.username,u.sportsID,u.sex");	

	$tables = array("$this->profilevisitor as v, $this->userprofile as u");

	$where= array("v.visitor_profile_id='".$userId."' AND v.visitor_date BETWEEN '".date("Y-m-d", strtotime('-'.$num.' days'))."' AND NOW() AND v.visitor_userid = u.id");

	$result1 = $this->SelectData($fields,$tables, $where, $order = array('visitor_date DESC'), $group=array(),$limit,$offset,0); 

	$result  = $this->FetchAll($result1); 

	return $result;

	}



	function getDefaultProfileImageByUserId($id)

    	{

        $fields = array();

        $tables = array($this->profileavatars);

        $where = array("profileId='".$id."'" ,"isDefault='1'");

        $result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0);

        $result = $this->FetchRow($result1);

        return $result;        

   	}



	function addUserProfileByValue($proArray) {

	$this->InsertData($this->userprofile,$proArray); 

	$insertId = mysql_insert_id(); 

	return $insertId;

	}



	## Update user image  

	function changeProfileUserProfileImage($id,$imageName) {

 	$sql = "UPDATE ".$this->userprofile." SET avatar='".$imageName."' WHERE id ='". $id."'";

	$result1 = $this->ExecuteQuery($sql);	

	}



	function editUserProfileValueById($array, $Id){

	 $this->UpdateData($this->userprofile,$array,"id",$Id,0);

	}

	function editUserNameByUserId($array, $Id){

	 $this->UpdateData($this->user,$array,"userId",$Id,0);

	}


	function editUserProfileValueByUserId($array, $Id){

	$this->UpdateData($this->userprofile,$array,"userId",$Id,0); 

	}

	function editUserProfileValueByUserIdAnduserType($array, $Id,$usertype){

	$where = "(userId='".$Id."' and userType='".$usertype."')";

	$this->UpdateDataWithWhere($this->userprofile,$array,$where,0); 

	}



	function editUserProfileDetailsByUserIdAnduserType($array, $Id,$usertype){

	$where = "(id='".$Id."' and userType='".$usertype."')";

	$this->UpdateDataWithWhere($this->userprofile,$array,$where,0);

	}



	function editExperienceDetailsByUserIdAnduserType($array, $Id,$usertype){

	$where = "(userId='".$Id."' and userType='".$usertype."')";

	$this->UpdateDataWithWhere($this->experience,$array,$where,0); 

	}



	function editUserExperienceDetailsId($array, $Id){

	$this->UpdateData($this->experience,$array,"id",$Id,0); 

	}



	function editBioDetailsByUserIdAnduserType($array, $Id,$usertype){

	$where = "(userId='".$Id."' and userType='".$usertype."')";

	$this->UpdateDataWithWhere($this->bio,$array,$where,0); 

	}



	function editAwardsDetailsByUserIdAnduserType($array, $Id,$usertype){

	$where = "(userId='".$Id."' and userType='".$usertype."')";

	$this->UpdateDataWithWhere($this->awards,$array,$where,0); 

	}



	function editAchivementsDetailsByUserIdAnduserType($array, $Id,$usertype){

	$where = "(userId='".$Id."' and userType='".$usertype."')";

	$this->UpdateDataWithWhere($this->achivements,$array,$where,0); 

	}



	function editRecordsDetailsByUserIdAnduserType($array, $Id,$usertype){

	$where = "(userId='".$Id."' and userType='".$usertype."')";

	$this->UpdateDataWithWhere($this->records,$array,$where,0); 

	}



	function editeducationDetailsByUserIdAnduserType($array, $Id,$usertype){

	$where = "(userId='".$Id."' and userType='".$usertype."')";

	$this->UpdateDataWithWhere($this->education,$array,$where,0); 

	}



	function changeUserProfileImage($id,$imageName) {

 	$sql = "UPDATE ".$this->userprofile." SET avatar='".$imageName."' WHERE userId ='". $id."'";

	$result1 = $this->ExecuteQuery($sql);	

	}



	function checkUserNameExists($username,$id){

	$fields = array();

	$tables = array($this->userprofile);

	if($id){

		$where  = array("username='".$username."' AND id!= '".$id."'");

	}else{

		$where  = array("username='".$username."'");

	}

	$result = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 

	$result1 = $this->FetchRow($result);

	return $result1;	

	}



	function getUserProfileDetailByUserID($uID,$type) 

	{



	$fields=array();	

	$tables=array($this->userprofile);

	$where  = array("userId='".$uID."' AND userType = '".$type."'");	

	$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 

	$result= $this->FetchRow($result1); 

	return $result;		

	}

	///////////////////////////////////////follow mail cron////////////////////////
	function getUserProfileDetailEmailByOnlyUserID($uID) 
	{
		if($uID!=''){
		$fields=array("*");	
		$tables=array($this->userprofile);
		$where  = array("id='".$uID."'");	
		$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 
		$result= $this->FetchRow($result1); 
		return $result;
		}		
	}


function getAllUserProfileDetail() 

	{
    $fields=array("*");	

	$tables=array($this->userprofile);

	$where  = array("status='2' AND unsubscribe='0'");	

	$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 

	$result= $this->FetchAll($result1); 
	return $result;		

	}	

function getAllUserProfileDetailOnlySubscribUser() 

	{
    $fields=array("*");	

	$tables=array($this->userprofile);

	$where  = array("status='2' AND unsubscribe='0' AND buddy_unsubscribe='0'");	

	$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 

	$result= $this->FetchAll($result1); 
	return $result;		

	}	

function AddFollowMail($array) 

	{
	$this->InsertData($this->follow_mail ,$array); 

	$insertId = mysql_insert_id(); 

	
	return $insertId;

	}
function getFollowMail($uID) 

	{
    $fields=array("sentmail_id");	

	$tables=array($this->follow_mail);

	$where  = array("loggedin_id='".$uID."'");	

	$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 

	$result= $this->FetchAll($result1); 
	return $result;		
	}	


///////////////////////////////////////follow mail cron////////////////////////


	

	function getAllProfileAvatars($id) {

	$fields=array();	

	$tables=array($this->profileavatars);

	$where=array("profileId='".$id."'");		

	$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 

	$result= $this->FetchAll($result1); 

	return $result;		

	}



	function getRandomFiveProfileAvatars($id) {

	 $fields=array();	

	 $tables=array($this->profileavatars);

	 $where=array("profileId='".$id."'");		

	 $result1 = $this->SelectData($fields,$tables, $where, $order = array("rand()"), $group=array(),$limit = "5",0,0); 

	 $result= $this->FetchAll($result1); 

	 return $result;		

	}



	function addUserProfileImagesByProfileId($array) 

	{

	$this->InsertData($this->profileavatars,$array); 

	$insertId = mysql_insert_id(); 

	return $insertId;

	}



	/*********** Insert Public Profile Images Start ***********/



	function addPublicrProfileImagesByProfileId($array) 

	{

	$this->InsertData($this->timelineimages,$array); 

	$insertId = mysql_insert_id(); 

	return $insertId;

	}



      function makedefaultPublicImgByuserId($imgid,$userId)

       { 

         $sql = "UPDATE ".$this->timelineimages." SET isDefault ='0' where publicProfileId ='".$userId."'"; 

         $result = $this->ExecuteQuery($sql);

         $sqlreslt = "UPDATE ".$this->timelineimages." SET isDefault ='1' where id ='".$imgid."'"; 

         $result = $this->ExecuteQuery($sqlreslt);

       }



      function getNeedPublicProfileimageById($id)

      {

        $fields = array();

        $tables = array($this->timelineimages);

        $where = array("publicProfileId='".$id."'" ,"isDefault='1'");

        $result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0);

        $result = $this->FetchRow($result1);

        return $result;        

      }



    	/*********** Insert Public Profile Images End ***********/

	function deleteProfileimgById($imgID,$userID){

	 $sql = "DELETE FROM ".$this->profileavatars." WHERE id = '".$imgID."' and profileId = '".$userID."'";  

	//echo $sql;      

	 $result1= $this->ExecuteQuery($sql); 

	 }



	function deleteSkillsByUserId($skillId,$userID)

	{

	$sql = "DELETE FROM ".$this->skills." WHERE id = '".$skillId."' and userId= '".$userID."'";  

	

	$result1= $this->ExecuteQuery($sql); 

	}



	function makedefaultImgByuserId($imgid,$userId)

	{ 

	$sql = "UPDATE ".$this->profileavatars." SET isDefault ='0' where profileId ='".$userId."'"; 

	$result = $this->ExecuteQuery($sql);

	$sqlreslt = "UPDATE ".$this->profileavatars." SET isDefault ='1' where id ='".$imgid."'"; 

	$result = $this->ExecuteQuery($sqlreslt);

	}



	function makeUndefaultImgByuserId($userId)

	{ 

	$sql = "UPDATE ".$this->profileavatars." SET isDefault ='0' where profileId =".$userId; 

	$result = $this->ExecuteQuery($sql);

	}



	function getNeedimageById($id)

	{

	$fields = array();

	$tables = array($this->profileavatars);

	$where = array("profileId='".$id."'" ,"isDefault='1'");

	$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 

	$result = $this->FetchRow($result1);

	return $result;        

	}



    	/*Profile visitor code starts here*/

	## Add user background in database

	function addProfileVisitor($Array) {

	$this->InsertData( $this->profilevisitor , $Array );

	$insertId = mysql_insert_id();

	return $insertId;

	}

	

	## Edit background by id

	function editProfileVisitor($array, $Id){

	$this->UpdateData($this->profilevisitor,$array,"visitor_id",$Id,0);

	}

	

	function getprofilevisitorcountbyuserid($userid,$enddate) 

	{

	$fields=array("count(visitor_id) cnt");	

	$tables=array($this->profilevisitor);

	$where = array("visitor_profile_id='".$userid."'  and visitor_date >= '".$enddate."'");

	$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 

	$result= $this->FetchRow($result1); 

	return $result['cnt'];	

	}



	function getprofilevisitorcountbyuseridandsessionid($userid, $sessionid) {

	$fields=array("count(visitor_id) cnt");	

	$tables=array($this->profilevisitor);

	$where = array("visitor_profile_id='".$userid."' AND visitor_userid = '".$sessionid."'");

	$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 

	$result= $this->FetchRow($result1); 

	return $result['cnt'];	

	}



	function getprofilevisitorDetailsbyuseridandsessionid($userid, $sessionid) {

	$fields=array();	

	$tables=array($this->profilevisitor);

	$where = array("visitor_profile_id='".$userid."' AND visitor_userid = '".$sessionid."'");

	$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 

	$result= $this->FetchRow($result1); 

	return $result;	

	}



        ## get club details by user id

    function getClubDetailByUserID($uID) {

	$fields=array();	

	$tables=array($this->club);

	$where=array("userID='".$uID."'");		

	$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 

	$result= $this->FetchRow($result1); 

	return $result;		

	}

	

	## Add Club Details

	function addClubDetails($array) {

	$this->InsertData($this->club,$array); 

	$insertId = mysql_insert_id(); 

	return $insertId;

	}

      

	## Edit Club Details By UserId

	function editClubDetailsByUserId($array, $Id){

	$this->UpdateData($this->club,$array,"userID",$Id,0);

	}



	function getUserProfileDetailsByUserIdAtAdminSide($id) 
	{
		if($id!=''){
		$fields=array("*");	
		$tables=array($this->userprofile);
		$where=array("Id = '".$id."'");		
		$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 
		$result= $this->FetchRow($result1); 
		return $result;		
		}
	}



	function editUserprofilesValueById($array, $Id){

	$this->UpdateData($this->userprofile,$array,"userId",$Id,0);

	}



	function getAllUsersForChart($from,$to)

	{

	$sql = "SELECT count(userId) as total FROM ".$this->user ."  where userType='3'  and ( regDate BETWEEN  '".$from."' and '".$to."') ";

	$result1 = $this->ExecuteQuery($sql);

	$result= $this->FetchRow($result1); 

	return $result;		

	}



	function getAllFanUsersForChart($from,$to)

	{ 

	$sql = "SELECT count(userId) as total FROM ".$this->user ."  where userType='4'  and ( regDate BETWEEN  '".$from."' and '".$to."') ";

	$result1 = $this->ExecuteQuery($sql);



	$result= $this->FetchRow($result1); 

	return $result;		

	}



	function getAllCoachUsersForChart($from,$to)

	{

	$sql = "SELECT count(userId) as total FROM ".$this->user ."  where userType='5'  and ( regDate BETWEEN  '".$from."' and '".$to."') ";

	$result1 = $this->ExecuteQuery($sql);

	$result= $this->FetchRow($result1); 

	return $result;		

	}



	function getAllClubUsersForChart($from,$to)

	{

	$sql = "SELECT count(userId) as total FROM ".$this->user ."  where userType='6'  and ( regDate BETWEEN  '".$from."' and '".$to."') ";

	$result1 = $this->ExecuteQuery($sql);

	$result= $this->FetchRow($result1); 

	return $result;		

	}



	function getAllTagMemberByUserId($gID,$limit='',$offset='0') 

	{

	$fields=array();	

	$tables=array($this->tagusers);

	$where = array("userid = '".$gID."' AND reqstatus='2'");

	$order = array();

	$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(), $limit, $offset,0);

	$result= $this->FetchAll($result1); 

	return $result;		

	}



	function searchUserByAjaxSearch($search)

	{

	$sql = "SELECT userId,fname,lname,username,sportsID,sex,avatar FROM ".$this->userprofile ." where userType IN (3,4,5,6) and ((concat(fname,' ',lname) LIKE '".$search."%' OR fname  LIKE '".$search."%' OR lname  LIKE '".$search."%') or username LIKE  '%".$search."%' or email LIKE  '%".$search."%') and status='2'";

	$result1 = $this->ExecuteQuery($sql);

	$result= $this->FetchAll($result1); 

	return $result;		

	}



	/************** Cover image likes functions **************/

	function getCoverLikes($coverId, $userProfileId)

	{

	$fields=array();	

	$tables=array($this->coverlikes);

	$where=array("coverId='".$coverId."' and coverlikeProfileId ='".$userProfileId."'  ");		

	$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 

	$result= $this->FetchRow($result1); 

	return $result;	

	}



	// for delete cover like by cover id

	function deleteCoverLike($Id) {

	$this->DeleteData($this->coverlikes,"id",$Id);

	}



	// Check count of state by country id

	function getAllCoverLikecountbyuserid($coverId, $userProfileId) {

	$fields=array("count(*) as cnt");	

	$tables=array($this->coverlikes);

	$where=array("coverId='".$coverId."' and coverlikeProfileId = '".$userProfileId."'");				

	$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 

	$result= $this->FetchRow($result1); 

	return $result['cnt'];		

	}



	//Get all cover likes for that cover id

	function getAllCoverlikecount($coverId) {

	$fields=array("count(*) as cnt");	

	$tables=array($this->coverlikes);

	$where=array("coverId='".$coverId."'");				

	$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 

	$result= $this->FetchRow($result1); 

	return $result['cnt'];		

	}



	// Add cover likes

	function addToCoverList($array)

	{

	$this->InsertData($this->coverlikes, $array);

	return mysql_insert_id();

	}



	function checkisIfollowHimOrNot($userId,$followId) {
		$fields=array();	
		$tables=array($this->follows);
		$where = array("userID='".$userId."' AND  followUID ='".$followId."'");
		$limit="";
		$offset="";
		$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(), $limit,$offset,0);
		$result= $this->FetchRow($result1); 
		return $result;	
	}

function getAllFollowingsByUserId($uID,$search='',$orderField='',$orderBy='',$limit='',$offset=''){
	$fields=array("f.*,u.fname,u.lname,u.email,u.avatar,u.username,u.userId,u.status,u.sex,u.userType");	
	$tables=array($this->follows." as f ",$this->userprofile." as u");
	$where = array("f.followUID=u.id AND u.status = '2'  AND f.userID='".$uID."'");
	if($search!='' && $search!='Search'){
		$where[] = ' ((LOWER(u.fname) LIKE "%'.strtolower(trim($search)).'%" ) OR (LOWER(u.lname) LIKE "%'.strtolower(trim($search)).'%" ) OR (LOWER(u.username) LIKE "%'.strtolower(trim($search)).'%" ) OR (u.email LIKE "%'.trim($search).'%" ))';
	}
	$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(), $limit,$offset,0);
	$result= $this->FetchAll($result1); 
	return $result;	

	}

	function getFollowingsByUserId($uID,$search='',$orderField='',$orderBy='',$limit='',$offset=''){

	$fields=array("f.*,u.fname,u.lname,u.email,u.avatar,u.username,u.userId,u.status,u.sex,u.userType");	

	$tables=array($this->follows." as f",$this->userprofile." as u");

	$where = array("f.followUID=u.id AND u.status='2' AND f.status='0' AND f.userID='".$uID."'");

	if($search!='' && $search!='Search'){

		$where[] = ' ((LOWER(u.fname) LIKE "%'.strtolower(trim($search)).'%" ) OR (LOWER(u.lname) LIKE "%'.strtolower(trim($search)).'%" ) OR (LOWER(u.uname) LIKE "%'.strtolower(trim($search)).'%" ) OR (u.email LIKE "%'.trim($search).'%" ))';

	}
	$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(), $limit,$offset,0);

	$result= $this->FetchAll($result1); 

	return $result;	

	}



	## Get Nine following users 



	function getNineFollowingsByUserId($uID,$search='',$orderField='',$orderBy='',$limit='',$offset=0){
		$fields=array("f.*,u.fname,u.lname,u.email,u.avatar,u.username,u.userId,u.status,u.sex,u.userType");	
		$tables=array($this->follows." as f",$this->userprofile." as u");
		$where = array("f.followUID=u.id AND u.status='2' AND f.status='0' AND f.userID='".$uID."'");
		if($search!='' && $search!='Search'){
			$where[] = ' ((LOWER(u.fName) LIKE "%'.strtolower(trim($search)).'%" ) OR (LOWER(u.lName) LIKE "%'.strtolower(trim($search)).'%" ) OR (LOWER(u.uName) LIKE "%'.strtolower(trim($search)).'%" ) OR (u.email LIKE "%'.trim($search).'%" ))';
		}
		$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit,0,0); 
		$result= $this->FetchAll($result1); 
		return $result;	
	}



	function deleteFollowingByUserId($userId,$followId){

	$sql = "delete from ".$this->follows."  WHERE userID='".$userId."' AND  followUID ='".$followId."'"; 

	$result1= $this->ExecuteQuery($sql);	 

	}

	

	function deleteFollowingByUserIdAndTagId($userId,$followId){

	 $sql = "delete from ".$this->follows."  WHERE ((userID='".$userId."' AND  followUID ='".$followId."') OR (userID='".$followId."' AND  followUID ='".$userId."'))"; 

	$result1= $this->ExecuteQuery($sql);	 

	}



	function getFollowerCountByUserId($uID){
		$fields=array("count(*) as followerCount");	
		$tables=array($this->follows." as f");
		$where = array("status='0' AND f.followUID='".$uID."'");
		$limit="";
		$offset="";
		$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit,$offset,0);
		$result= $this->FetchRow($result1); 
		return $result['followerCount'];	
	}



	function getFollowerRequestCountByUserId($uID){
		$fields=array("count(*) as followerReqCount");	
		$tables=array($this->follows." as f");
		$where = array("status='1' AND f.followUID='".$uID."'");
		$limit="";
		$offset="";
		$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(), $limit,$offset,0);
		$result= $this->FetchRow($result1); 
		return $result['followerReqCount'];	
	}



	function getFollowingCountByUserId($uID){

	$fields=array("count(*) as followerCount");	

	$tables=array($this->follows." as f",$this->user." as u");

	$where = array("status='0' AND f.userID='".$uID."'");

	$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(), $limit,$offset,0);

	$result= $this->FetchRow($result1); 

	return $result['followerCount'];	

	}



	function followAUser($array)

	{

	$this->InsertData( $this->follows , $array );		

	$insertId = mysql_insert_id();

	return $insertId;	

	}

	function getFollowersByUserId($uID,$search=''){
		$fields=array("f.*,u.fname,u.lname,u.email,u.avatar,u.username,u.userId,u.status,u.sex,u.userType");	
		$tables=array($this->follows." as f",$this->userprofile." as u");
		$where = array("f.userID=u.id AND u.status='2' AND f.status='0' AND f.followUID='".$uID."'");

		if($search!='' && $search!='Search'){
			$where[] = ' ((LOWER(u.fname) LIKE "%'.strtolower(trim($search)).'%" ) OR (LOWER(u.lname) LIKE "%'.strtolower(trim($search)).'%" ) OR (LOWER(u.username) LIKE "%'.strtolower(trim($search)).'%" ) OR (u.email LIKE "%'.trim($search).'%" ))';
		}
		$limit = "";
		$offset="";
		$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(), $limit,$offset,0); 
		$result= $this->FetchAll($result1); 
		return $result;	
	}



	## Get Nine Followers Users 
	function getNineFollowersByUserId($uID,$search='',$orderField='',$orderBy='',$limit='',$offset=''){
		$fields=array("f.*,u.fname,u.lname,u.email,u.avatar,u.username,u.userId,u.status,u.sex,u.userType");	
		$tables=array($this->follows." as f",$this->userprofile." as u");
		$where = array("f.userID=u.id AND u.status='2' AND f.status='0' AND f.followUID=".$uID);
		if($search!='' && $search!='Search'){
			$where[] = ' ((LOWER(u.fname) LIKE "%'.strtolower(trim($search)).'%" ) OR (LOWER(u.lname) LIKE "%'.strtolower(trim($search)).'%" ) OR (LOWER(u.username) LIKE "%'.strtolower(trim($search)).'%" ) OR (u.email LIKE "%'.trim($search).'%" ))';
		}
		$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),9,0,0); 
		$result= $this->FetchAll($result1); 
		return $result;	
	}



	function getRequestFollowersByUserId($uID,$search='',$orderField='',$orderBy='',$limit,$offset){

	$fields=array("f.*,u.fname,u.lname,u.email,u.avatar,u.username,u.userId,u.status,u.sex,u.userType");	

	$tables=array($this->follows." as f",$this->userprofile." as u");

	$where = array("f.userID=u.id AND u.status='2' AND f.status='1' AND f.followUID=".$uID);

	if($search!='' && $search!='Search'){

	$where[] = ' ((LOWER(u.fname) LIKE "%'.strtolower(trim($search)).'%" ) OR (LOWER(u.lname) LIKE "%'.strtolower(trim($search)).'%" ) OR (LOWER(u.username) LIKE "%'.strtolower(trim($search)).'%" ) OR (u.email LIKE "%'.trim($search).'%" ))';

	}

	$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(), $limit,$offset,0);

	$result= $this->FetchAll($result1); 

	return $result;	

	}



	/************************************************************************************************************/

	function updatefollowAUser($userIDs,$followId){

	$sql = "UPDATE ".$this->follows." SET status='0' WHERE userID='".$userIDs."' AND followUID='".$followId."'";		

	$result1= $this->ExecuteQuery($sql);	 

	}



	/*------------- Like ---------------------------------------------*/

	function likeUser($array)

	{

	$this->InsertData( $this->likes , $array );		

	$insertId = mysql_insert_id();

	return $insertId;	

	}



	function getLikeCountByUserId($uID){
		$fields=array("count(*) as likeCount");	
		$tables=array($this->likes." as f");
		$where = array("f.likeUID='".$uID."'");
		$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(), $limit='',$offset='',0);
		$result= $this->FetchRow($result1); 
		return $result['likeCount'];	
	}



	function checkisIlikeOrNot($userId,$followId) {
		$fields=array();	
		$tables=array($this->likes);
		$where = array("userID='".$userId."' AND  likeUID ='".$followId."'");
		$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(), $limit='',$offset='',0);
		$result= $this->FetchRow($result1); 
		return $result;	
	}



	function deleteLikeByUserId($userId,$followId){
		$sql = "delete from ".$this->likes."  WHERE userID='".$userId."' AND  likeUID ='".$followId."'";
		$result1= $this->ExecuteQuery($sql);	 
	}


	function getClubMembersPostCount($userID) {
		$fields=array("count(*) as cnt");	
		$tables=array($this->posts);
		$where = array("userId='$userID'");
		$order = array("");
		$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(), $limit='',$offset='0',0);
		$result= $this->FetchRow($result1); 
		return $result['cnt'];		
	}



	function getPostByUserId($userID){
		$fields=array("");	
		$tables=array($this->posts);
		$where = array("userId='".$userID."'");
		$order = array("");
		$limit="";
		$offset=0;
		$result1 = $this->SelectData($fields,$tables, $where, $order = array("id DESC"), $group=array(),$limit,$offset,0);
		$result= $this->FetchAll($result1); 
		return $result;		
	}

	function deletePostByID($userID){
		$this->DeleteData($this->posts,"id",$userID);
	}

	function deletePostsByPostId($ids){
		$sql = "DELETE FROM ".$this->posts." WHERE id IN (".$ids.")";
		$result1= $this->ExecuteQuery($sql);	 
	}



	function getPostDetailByPostId($postID){
		$fields=array("");	
		$tables=array($this->posts);
		$where = array("id='".$postID."'");
		$order = array("");
		$limit="";
		$offset=0;
		$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(),$limit,$offset,0);

  	$result= $this->FetchRow($result1); 

  	return $result;		

	}



	function getLikeCountByCommentIdandLikeType($commentId,$likeType) {	

  	$fields=array("count(*) as cnt");	

  	$tables=array($this->messagelikes);

  	$where = array("commentId='".$commentId."' AND  likeType ='".$likeType."'");

  	$order = array("");

  	$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(), $limit='', $offset='0',0);

  	$result= $this->FetchRow($result1); 

  	return $result['cnt'];		

	}



	function getUserLoginStatusByUserProfileId($userProfileId)

	{

	$fields=array("user_id,isOnline");	

	$tables=array($this->user_login);

	$where=array("user_id='".$userProfileId."'");

	$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 

	$result= $this->FetchRow($result1); 

	return $result;		

	}

	

	

	function getLiveOnlineUsers($userProfileId)

	{

	$fields=array("uf.*,ul.*");

	$tables=array($this->tagusers. " as uf", $this->user_login. " as ul");

	$where = array("(uf.frndid='$userProfileId' OR uf.userid='$userProfileId') AND uf.reqstatus='2' AND ul.isOnline='y' AND  ul.user_id NOT IN ('$userProfileId')");

	$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array("ul.user_id"),$limit = "",0,0); 

	$result= $this->FetchAll($result1); 

	return $result;		

	}



	function getAllOnlineUsersInSite($val, $orderField='', $orderBy='', $limit='',$offset=''){

	$fields = array("");

	$tables = array("$this->user as u,$this->userprofile as p,$this->user_login as ul");

	$where = array('ul.isOnline="y" AND p.status!="0" AND u.userType = p.userType AND ul.user_id = p.id');

	if($val) {

		$where[] = '(concat(p.fname," ",p.lname) LIKE "%'.$val.'%" OR p.email LIKE "%'.$val.'%" ) AND p.userId != "1"';

	}

	else{

		$where[] = 'p.userId!="1"';

	}

	$order = array("$orderField $orderBy");

	$result1 = $this->SelectData($fields,$tables,$where,$order = array("p.regDate DESC"),$group=array("p.email"),$limit,$offset,0);

	$result = $this->FetchAll($result1);

	return $result;

	}



	

	function getLoginStatusByTags($userIds,$starttime,$endtime)

	{



	$fields=array();	

	$tables=array($this->user_login);

	$where=array("((loginTime BETWEEN '$starttime' AND '$endtime') OR (logOutTime BETWEEN '$starttime' AND '$endtime')) AND (user_id IN ($userIds))");

	$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 

	$result= $this->FetchAll($result1); 

	return $result;		

	}



	function getUserdob($id) {

	$fields=array('birthdate');	

	$tables=array($this->userprofile);

	$where=array("id='".$id."'");		

	$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 

	$result= $this->FetchRow($result1); 

	return $result['birthdate'];		

	}

	

	function getfnameandlnamebyusename($id) {

	$fields=array('fname,lname');	

	$tables=array($this->userprofile);

	$where=array("id='".$id."'");		

	$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 

	$result= $this->FetchRow($result1); 

	return $result;		

	}



	## Tag user list from admin side.

	function getTagUsersFromAdminSide($uID,$search_from='', $search_to='',$orderField=' ', $orderBy='', $limit='',$offset=''){

	$fields=array();	

	$tables=array($this->tagusers);

	$where = array("(frndid='$uID' OR userid='$uID') AND reqstatus='2'");

	$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(), $limit,$offset,0); 

	$result= $this->FetchAll($result1); 

	return $result;	

	}

function CheckUserByEmail($email)
{

	$fields=array();	

	$tables=array($this->user);

	$where = array("email = '".$email."'");

	$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(), $limit,$offset,0); 

	$result= $this->FetchRow($result1); 

	return $result;	

}
function generaterandompassword($length)
  {
    $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()_-=+;:,.?";
    $password = substr( str_shuffle( $chars ), 0, $length );
    return $password;
  }

function getUserProfileDetailBysportsID($sportsID,$userId) 
{	
	if($userId!='' && $$sportsID!=''){
	$fields=array("userid");	
	$tables=array($this->userprofile);
	$where  = array("sportsID LIKE '%".$sportsID."%' AND id!='".$userId."'");	
	$result = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 
	$result1= $this->FetchAll($result);
	return $result1;
	}		
}

function getUserFriendsId($userId)
{
	if($userId!=''){
	$fields=array("frndid");	
	$tables=array("sportstag_tags");
	$where  = array(" userId='".$userId."'");	
	$result = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 
	$result1= $this->FetchAll($result);
	return $result1;
	}		
}
function UserProfileDetailByuserID($userId) 
{
	if($userId!=''){
	$fields=array();	
	$tables=array($this->userprofile);
	$where  = array("id='".$userId."'");	
	$result = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 
	$result1= $this->FetchRow($result);
	return $result1;
	}		
}

function editProfileInfoByUserId($array, $Id)
{
	$result = $this->UpdateData($this->userprofile,$array,"id",$Id,0);
	return $result;
}

function update_chkUserEmailExist($email, $id='') {

	$fields=array('email');

	$tables=array($this->userprofile);

	$where=array("email='".$email."' AND status!='0'");

	if($id != '') {

	$where[] = " id!='".$id."'";	

	}

	$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0);

	$result= $this->FetchRow($result1);

	return $result;		

	}

	function editBioDetailsByUserId($height,$id) {

	$sql = "UPDATE ".$this->bio." SET height='".$height."' WHERE userId='".$id."'";
	$result1= $this->ExecuteQuery($sql);			
	return $result;
	}

	
	## Get Wished User By UserId
	/****************************/
	function getAllWishListByUserId($userId){
		if($userId!=''){
		$fields=array("*");	
		$tables=array($this->wishlist);
		$where = array("userId='".$userId."'");
		$result1 = $this->SelectData($fields,$tables,$where,$order=array(),$group=array(),$limit,$offset,0);
		$result= $this->FetchAll($result1); 
		return $result;
		}
	}

	## Add WishList
	/***********************/
	function addWishlist($array){
		$this->InsertData( $this->wishlist,$array);
		$insertId = mysql_insert_id();
		return $insertId;	
	}

	## remove Wished User By UserId
	/****************************/
	function removeWishlist($userId, $wishuserId){	
		$sql = "DELETE FROM ".$this->wishlist."  WHERE userId='".$userId."' AND user_wishId ='".$wishuserId."'"; 
		$result1= $this->ExecuteQuery($sql);	 
	}

	## Get All Wishlist UserProfile
	/********************************/
	function getWishListUserByUserId($uID){
		$fields=array("w.*,u.fname,u.lname,u.email,u.sportsID,u.sports_position,u.avatar,u.username,u.userId,u.status,u.sex,u.userType");	
		$tables=array($this->wishlist." as w",$this->userprofile." as u");
		$where = array("w.user_wishId=u.id AND u.status='2' AND w.userId=".$uID);
		$limit = "";
		$offset="";
		$order = array("id DESC");
		$result1 = $this->SelectData($fields,$tables, $where, $order,$group=array(), $limit,$offset,0); 
		$result= $this->FetchAll($result1); 
		return $result;	
	}
	## Get Only Playa Active users
	/********************************/
	function getAllActiveUsers(){
		$fields=array("*");	
		$tables=array($this->userprofile);
		$where  = array("status='2' AND unsubscribe='0' AND userType='3' AND buddy_unsubscribe='0'");	
		$result1 = $this->SelectData($fields,$tables, $where, $order=array(),$group=array(),$limit="",0,0); 
		$result= $this->FetchAll($result1); 
		return $result;		
	}
	## Get All Active User 
	function getAllUsersRecord(){
		$fields=array("*");	
		$tables=array($this->userprofile);
		$where  = array("status='2'");	
		$result1 = $this->SelectData($fields,$tables, $where, $order=array(),$group=array(),$limit="",0,0); 
		$result= $this->FetchAll($result1); 
		return $result;		
	}
}

?>

