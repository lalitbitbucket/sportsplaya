<?php
/**************************************************/
## Class Name - Model_Categories (Contains all the functions related products)
/**************************************************/

class Model_Plan extends Database 
{	
	## Constructor
	function Model_Plan() {
		$this->plan = MEMBERSHIPPLAN;
		$this->planfeatures = PLANFEATURES;
		$this->payment = PAYMENT;
		$this->Database();
	}	
	
	## Add plan in database
	function addplanByValue($Array) {
		$this->InsertData($this->plan , $Array );		
		$insertId = mysql_insert_id();
		return $insertId;
	}

	## Add plan features in database
	function addplanFeaturesByValue($Array) {
		$this->InsertData($this->planfeatures , $Array );		
		$insertId = mysql_insert_id();
		return $insertId;
	}

	## Delete features using plan id
	function deletePlanFeaturesByPlanId($planId){
		$this->DeleteData($this->planfeatures,"membPlanId",$planId);
	}

	## Get plan features list

	function getPlanFeaturesByPlanId($id)
	{
	$fields = array();	
	$tables = array($this->planfeatures);
	$where=array("membPlanId='".$id."'");
	$result1 = $this->SelectData( $fields,$tables, $where, $order=array(), $group=array(),"",0,0);
	$result= $this->FetchAll( $result1 );
	return $result;
	}
	
	## Edit plan by planID
	function editplanValueById($array, $Id){
	$this->UpdateData($this->plan,$array,"planID",$Id,0); 
	}

	
	## Update plan status with multiple planIDs
	function updateplanStatus($planIDs, $status) {
	$sql = "UPDATE ".$this->plan." SET status = '".$status."' WHERE planID IN (".$planIDs.")";
	$result1= $this->ExecuteQuery($sql);	 
	}
	
	## Delete plan by planID
	function deleteplanValueById($planID){
	$sql = "UPDATE ".$this->plan." SET status = '0' WHERE planID =".$planID;
	$result1= $this->ExecuteQuery($sql);
	}
	
	## Checking wheather user enetered plan name is already exists or not 
	function checkplanNameExists($productName,$id){
	$fields = array();
	$tables = array($this->plan);
	if($id){
		$where  = array('TRIM(membership_plan) ="'.$productName.'" AND planID != '.$id.' AND status!="0" ');
	}else{
		$where  = array('TRIM(membership_plan) ="'.$productName.'" AND status!="0" ');
	}
	$result = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 
	$result1 = $this->FetchRow($result);
	return $result1;
	}
	
	## Checking wheather user enetered sub plan name is already exists or not 
	function checkSubplanNameExists($productName,$id){
	$fields = array();
	$tables = array($this->plan);
	if($id){
		$where  = array('TRIM(membership_plan) ="'.$productName.'" AND planID != '.$id.' AND status!="0"');
	}else{
		$where  = array('TRIM(membership_plan) ="'.$productName.'" AND status!="0"');
	}
	$result = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 
	$result1 = $this->FetchRow($result);
	return $result1;
	}

	## Getting all categories from the datbase
	function getAllplan($search,$orderField='membership_plan',$orderBy='ASC',$limit,$offset){
	$fields   = array();	
	$tables   = array($this->plan);
	$where    = array('status!="0"');	
	if($search!=''){
		$where[] = 'membership_plan LIKE "%'.$search.'%"';
	}
	$order = array("$orderField $orderBy");
	$result1  = $this->SelectData($fields,$tables, $where, $order, $group=array(),$limit,$offset,0); 
	$result   = $this->FetchAll($result1); 
	return $result;	
	}
	
	## Getting plan detail by plan id
	function getplanDetailByplanID($planID){
	$fields   = array("");	
	$tables   = array($this->plan);
	$where    = array("planID = '".$planID."'");		
	$result1  = $this->SelectData($fields,$tables, $where, $order = array(""), $group=array(),$limit = 0,0,0); 
	$result   = $this->FetchRow($result1); 
	return $result;	
	}
	
	## Getting all active categories from the datbase
	function getAllActiveplan(){
	$fields   = array();	
	$tables   = array($this->plan);
	//$where    = array("parentID = '0' AND status='2'");
	$where    = array("status='2'");	
	$order = array("membership_plan ASC");
	$result1  = $this->SelectData($fields,$tables, $where, $order, $group=array(),$limit='',0,0); 
	$result   = $this->FetchAll($result1); 
	return $result;	
	}
	
	## Getting plan name by plan id
	function getplanNameByplanID($planID){
	$fields   = array("membership_plan");	
	$tables   = array($this->plan);
	$where    = array("planID = '".$planID."'");		
	$result1  = $this->SelectData($fields,$tables, $where, $order = array(""), $group=array(),$limit = 0,0,0); 
	$result   = $this->FetchRow($result1); 
	return $result;	
	}
	
	## get all plan features 	
	function getAllPlanFeatures(){
	$fields   = array();	
	$tables   = array($this->planfeatures);
	$where    = array("status='2'");
	$order = array("featName ASC");
	$result1  = $this->SelectData($fields,$tables, $where, $order, $group=array(),$limit='',0,0); 
	$result   = $this->FetchAll($result1); 
	return $result;	
	}
	
	function addPayment($array){
	$this->InsertData($this->payment, $array);
	$insertId = mysql_insert_id();
	return $insertId;
	}
	
	function getPaymentDetailsByPaymentID($payID){
	$fields = array();	
	$tables = array($this->payment);
	$result1 = $this->SelectData($fields,$tables, $where= array("paymentID='".$payID."'"), $order = array(), $group=array(),$limit = "",0,0); 
	$result  = $this->FetchRow($result1); 
	return $result;
	}
	
	function editPaymentDetailsByPaymentID($array, $Id){
	$this->UpdateData($this->payment,$array,"paymentID",$Id,0);
      	$insertId = mysql_insert_id();
	return $insertId;
	}

	function getAllplanfeaturesByPlanList($planList){
		$fields   = array();	
		$tables   = array($this->planfeatures);
		$where    = array("membPlanId IN (".$planList.")");		
		$result1  = $this->SelectData($fields,$tables, $where, $order=array(), $group=array(),$limit='',0,0); 
		$result   = $this->FetchAll($result1); 
		return $result;	
	}
}
?>