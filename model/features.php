<?php
/**************************************************/
## Class Name - Model_Categories (Contains all the functions related products)
/**************************************************/

class Model_Features extends Database 
{	
	## Constructor
	function Model_Features() {
		$this->features = FEATURES;
		$this->Database();
	}	
	
	## Add features in database
	function addfeaturesByValue($Array) {
		$this->InsertData($this->features , $Array );		
		$insertId = mysql_insert_id();
		return $insertId;
	}
	
	## Edit features by featID
	function editfeaturesValueById($array, $Id){
		$this->UpdateData($this->features,$array,"featID",$Id,0);
	}
	
	## Update features status with multiple featIDs
	function updatefeaturesStatus($featIDs, $status) {
		$sql = "UPDATE ".$this->features." SET status = '".$status."' WHERE featID IN (".$featIDs.")";
		$result1= $this->ExecuteQuery($sql);	 
	}
	
	## Delete features by featID
	function deletefeaturesValueById($featID){
		$sql = "UPDATE ".$this->features." SET status = '0' WHERE featID ='".$featID."'";
		$result1= $this->ExecuteQuery($sql);
	}

	function getFeatureDetailsById($id) {
		$fields = array();	
		$tables = array($this->features);
		$result1 = $this->SelectData($fields,$tables, $where= array("featID=$id"), $order = array(), $group=array(),$limit = "",0,0); 
		$result  = $this->FetchRow($result1); 
		return $result;
	}

	function getFeaturesNamesByIds($ids) {
		$fields = array("featName");	
		$tables = array($this->features);
		$where= array("featID IN ($ids)");
		$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 
		$result  = $this->FetchAll($result1,"featName"); 
		return $result;
	}
	
	## Checking wheather user enetered features name is already exists or not 
	function checkfeaturesNameExists($name,$id){
		$fields = array();
		$tables = array($this->features);
		if($id){
			$where  = array('TRIM(featName) ="'.$name.'" AND featID != '.$id.' AND status!="0" ');
		}else{
			$where  = array('TRIM(featName) ="'.$name.'" AND status!="0" ');
		}
		$result = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,1); 
		$result1 = $this->FetchRow($result);
		return $result1;
	}
	
	## Checking wheather user enetered sub features name is already exists or not 
	function checkSubfeaturesNameExists($name,$id){
		$fields = array();
		$tables = array($this->features);
		if($id){
			$where  = array('TRIM(featName) ="'.$name.'" AND featID != '.$id.' AND status!="0"');
		}else{
			$where  = array('TRIM(featName) ="'.$name.'" AND status!="0"');
		}
		$result = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 
		$result1 = $this->FetchRow($result);
		return $result1;
	}

	## Getting all categories from the datbase
	function getAllfeatures($search,$orderField='featName',$orderBy='ASC',$limit,$offset){
		$fields   = array();	
		$tables   = array($this->features);
		$where    = array('status!="0"');	
		if($search!=''){
			$where[] = 'featName LIKE "%'.$search.'%"';
		}
		$order = array("$orderField $orderBy");
		$result1  = $this->SelectData($fields,$tables, $where, $order, $group=array(),$limit,$offset,0); 
		$result   = $this->FetchAll($result1); 
		return $result;	
	}
	
	## Getting features detail by features id
	function getfeaturesDetailByfeatID($featID){
		$fields   = array("");	
		$tables   = array($this->features);
		$where    = array("featID = '".$featID."'");		
		$result1  = $this->SelectData($fields,$tables, $where, $order = array(""), $group=array(),$limit = 0,0,0); 
		$result   = $this->FetchRow($result1); 
		return $result;	
	}
	
	## Getting all active categories from the datbase
	function getAllActivefeatures(){
		$fields   = array();	
		$tables   = array($this->features);
		$where    = array("status='2'");	
		$order = array("featName ASC");
		$result1  = $this->SelectData($fields,$tables, $where, $order, $group=array(),$limit='',0,0); 
		$result   = $this->FetchAll($result1); 
		return $result;	
	}
	
	## Getting features name by features id
	function getfeaturesNameByfeatID($featID){
		$fields   = array("featName");	
		$tables   = array($this->features);
		$where    = array("featID = '".$featID."'");		
		$result1  = $this->SelectData($fields,$tables, $where, $order = array(""), $group=array(),$limit = 0,0,0); 
		$result   = $this->FetchRow($result1); 
		return $result;	
	}

	## Getting features name list by features id
	function getfeaturesNameListByfeatID($featID){
		$fields   = array("featName");	
		$tables   = array($this->features);
		$where    = array("featID = '".$featID."'");		
		$result1  = $this->SelectData($fields,$tables, $where, $order = array(""), $group=array(),$limit = 0,0,0); 
		$result   = $this->FetchAll($result1); 
		return $result;	
	}	
}
?>