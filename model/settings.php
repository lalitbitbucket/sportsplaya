<?php
class Model_Settings extends Database 
{	
	## Constructor
	function Model_Settings() {
		$this->user = USERS;
		$this->usersettings = USERSETTINGTOOLS;
		$this->settingtools = USERSETTINGTOOLS;	
		$this->Database();
	}
	## Get all settings tools 
	function getAllSettingsTools($limit='',$offset='') {
		$fields = array();
		$tables = array($this->settingtools);	
		$where=array("status='2'");
		$order = array();
		$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(), $limit='', $offset=0,0);
		$result= $this->FetchAll($result1); 
		return $result;		
	}
	## Get all settings tools 
	function getAllUserSettings($userId,$limit='',$offset='') {
		$fields = array();
		$tables = array($this->usersettings);	
		$where=array("userId='".$userId."'");
		$order = array();
		$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(), $limit='', $offset=0,0);
		$result= $this->FetchAll($result1); 
		return $result;		
	}
}
?>
