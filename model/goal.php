<?php 
class Model_goal extends Database
{	
	function Model_goal() {	
	$this->goal = GOALS;
	$this->goalinvite = GOALINVITE;
	$this->Database();
	}

	function addgoalInvite($inviteArray) {
	$this->InsertData($this->goalinvite,$inviteArray);  
	$insertId = mysql_insert_id();
	return $insertId;
	}

       function getMyGoalInvite($id)
       {
	 $fields=array();	
	 $tables=array($this->goalinvite);
	 $where=array("toIDs='".$id."'");		
	 $result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 
	 $result= $this->FetchAll($result1); 
	 return $result;		
	}

	function getGoalRequestCount($toIDs) {
	$fields=array("g.*,i.*");	
	$tables=array($this->goal. " as g",$this->goalinvite. " as i");
	$where=array("(i.toIDs='".$toIDs."') AND i.goalId = g.id AND g.readstatus ='n'");		
	$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 
	$result= $this->FetchAll($result1); 
	return $result;		
	}

	function getAllgoalByUserId($userId,$search, $orderField,$orderBy ='ASC', $limit,$offset) {
        $fields=array("*");    
      	$tables=array($this->goal);
        $where = array();
        $where[] = "status = '2'";  
        if($userId!='')
        {
            $where[] = "userId='".$userId."'";    
        }
        if($search != '' && $search != 'title') 
        {
            $where[] = "(title LIKE '%".$search."%')";
        }
        $result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(), $limit,$offset,0);
        $result= $this->FetchAll($result1); 
        return $result;        
        }
    
    	//add goal in database
	function addgoal($goalArray) {
	$this->InsertData($this->goal,$goalArray);  
	$insertId = mysql_insert_id();
	return $insertId;
	}
	
	function updategoalDetails($Array,$Id)
	{
	$this->UpdateData($this->goal,$Array,"id",$Id,0);
	$insertId = mysql_insert_id();
	return $insertId;
	}
	
	## Delete user by userid
	function deletegoalById($id){
	$this->DeleteData($this->goal,"id",$id);
	}

	## Get goal Details by goal Id
        public function getgoalDetailsById($id) {
	$fields=array();	
	$tables=array($this->goal);
	$where=array("id='".$id."'");		
	$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 
	$result= $this->FetchRow($result1); 
	return $result;		
	}

	function getMyGoalInvites($toIDs) {
	$fields=array("g.*,i.*");	
	$tables=array($this->goal. " as g",$this->goalinvite. " as i");
	$where=array("(i.toIDs='".$toIDs."') AND i.goalId = g.id ");		
	$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 
	$result= $this->FetchAll($result1); 
	return $result;		
	}	
	
	# update multiple read status 
	function updateReadStatusByGoalId($Ids,$action){
	 $sql = "UPDATE ".$this->goal." SET readstatus='".$action."' WHERE id IN (".$Ids.")"; 
	// print_r($sql); exit;
	 $result1= $this->ExecuteQuery($sql);	 
	}
 }
?>