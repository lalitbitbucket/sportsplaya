<?php 

class Model_Friends extends Database

{	

	function Model_Friends() {

	  $this->userfriends = USERFRIENDS;
	  $this->user = USERS;
	  $this->userprofile = USERPROFILE;	
	   $this->followuser =FOLLOW_USER;
	  $this->Database();

	}

	function getAllMyFollowers($uID, $orderField='', $orderBy='', $limit='', $offset=0) {
	  if($uID!=''){
	  $fields=array("followUID");	
	  $tables=array($this->followuser);
	  $where = array("userID='".$uID."'");
	  $order = array("");
	  $result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(), $limit, $offset,0); 
	  $result= $this->FetchAll($result1); 
	  return $result;
	  }		
	}

	## Add new friends

	function addFriendsRequest($array) {

	  $this->InsertData($this->userfriends , $array);		

	  $insertId = mysql_insert_id();

	  return $insertId;

	}

	

	## get all other active users 

	function getAllActiveList($search='', $orderField='', $orderBy='', $limit='', $offset=0) {

	  $fields=array();		

	  $tables=array($this->user);

	  $order = array("");

	  $result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(), $limit, $offset,0); 

	  $result= $this->FetchAll($result1); 

	  return $result;		

	}



	function getAllFriendsByUserId($userId,$search)

	{  

	    $fields = array();    

	    $tables = array($this->userfriends);

	    $where = array("(uStatus = '2' AND uType!='1' AND uType!='2')");

	    $where[] = "(uID!='".$userId."')"; 

	    if($search!='')

	    {

		 $where[] = "(fName LIKE  '%".$search."%' OR lName LIKE  '%".$search."%')"; 

	    }   

	    $result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 

	    $result  = $this->FetchAll($result1); 

	    return $result;

	}

	

	function getAllActiveUsersList($uID, $search='', $orderField='', $orderBy='ASC', $limit='', $offset=0) {

	  $fields=array();		

	  $tables=array($this->user);

	  $where = array("userId!='$uID' AND status='2' AND userType IN (3,4,5)");

	  $order = array("$orderField $orderBy");

	  $result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(), $limit, $offset,0); 

	  $result= $this->FetchAll($result1); 

	  return $result;		

	}



	## check user already friend or not

	function checkFriendExists($userID,$friendID) {

	  $fields=array("*");	

	  $tables=array($this->userfriends);

	  $where = array("(userid='$userID' AND frndid='$friendID' OR userid='$friendID' AND frndid='$userID') AND reqstatus!='0'");

	  $order = array("");

	  $result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(), $limit='', $offset='0',0); 

	  $result= $this->FetchRow($result1); 

	  return $result;		

	}



	## check user already invited or not

	function checkFriendCountExists($userID) {

	  $fields=array("count(*) as cnt");	

	  $tables=array($this->userfriends);

	  $where = array("frndid='$userID' AND reqstatus='1'");

	  $order = array("");

	  $result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(), $limit='', $offset='0',0);

	  $result= $this->FetchRow($result1); 

	  return $result['cnt'];		

	}



	## Accepted friend list count

	function checkAcceptedFriendCountExists($uID) {

	  $fields=array("count(*) as cnt");	

	  $tables=array($this->userfriends);

	  $where = array("(frndid='$uID' OR userid='$uID') AND reqstatus='2'");

	  $order = array("");

	  $result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(), $limit='', $offset='0',0); 

	  $result= $this->FetchRow($result1); 

	  return $result['cnt'];		

	}



	## check for my friend exist or not

	function checkFriendExistsOrNot($userID,$friendID) {

	  $fields=array("count(*) as cnt");	

	  $tables=array($this->userfriends);

	  $where = array("((userid='$userID' AND frndid='$friendID') OR(userid='$friendID' AND frndid='$userID')) AND reqstatus='2'");

	  $order = array("");

	  $result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(), $limit='', $offset='0',0); 

	  $result= $this->FetchRow($result1); 

	  return $result['cnt'];		

	}

	

	function checkUserInvited($fromID, $toID) {

	  $fields=array("count(*) as cnt");	

	  $tables=array($this->userfriends);

	  $where = array("frndid='$toID' AND userid='$fromID'");

	  $order = array("");

	  $result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(), $limit='', $offset='0',0);

	  $result= $this->FetchRow($result1); 

	  return $result['cnt'];		

	}



	## get all active friends request

	function getAllFriendRequests($uID, $orderField='', $orderBy='', $limit='', $offset=0) {

	  $fields=array("");	

	  $tables=array($this->userfriends." as f");

	  $where = array("f.frndid='$uID' AND f.reqstatus='1'");

	  $order = array("");

	  $result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(), $limit, $offset,0); 

	  $result= $this->FetchAll($result1); 

	  return $result;		

	}



	## get all my friends

	function getAllMyFriend($uID, $orderField='', $orderBy='', $limit='', $offset=0) {

	  $fields=array("frndid,userid");	

	  $tables=array($this->userfriends);

	  $where = array("(frndid='$uID' OR userid='$uID') AND reqstatus='2'");

	  $order = array("");

	  $result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(), $limit, $offset,0); 

	  $result= $this->FetchAll($result1); 

	  return $result;		

	}



	/*********** Fetch five tags profile photos by random **********/

	function getFiveTagsProfilePhotosByRandom($uID, $orderField='', $orderBy='', $limit='', $offset=0) {

	  $fields=array("");	

	  $tables=array($this->userfriends);

	  $where = array("(frndid='$uID' OR userid='$uID') AND reqstatus='2'");

	  $order = array("");

	  $result1 = $this->SelectData($fields,$tables,$where,$order=array("rand()"), $group=array(),$limit = "5",0,0); 

	  $result= $this->FetchAll($result1); 

	  return $result;		

	}



	function getAllMyFriendList($search='',$uID,$orderField='', $orderBy='', $limit='', $offset=0) {

	$fields=array("f.*,up.*");

	$tables=array($this->userfriends. " as f", $this->userprofile. " as up");

	$where = array("(f.frndid='$uID' OR f.userid='$uID') AND f.reqstatus='2' AND (f.frndid=up.id OR f.userid=up.id)");

	if($search!= '')

	{	

		$search_value = explode(' ', $search );		

		$parts = array();

	foreach($search_value as $search_value ){

		$parts[] = '(concat(up.fname," ",up.lname) LIKE "%'.$search.'%" OR up.email LIKE "%'.$search.'%" )';

	} 		

		$parts = implode(' OR ', $parts);

		$where[] ="(".$parts.")";

	}

	$order = array("");

	$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array('up.id OR f.frndid'), $limit, $offset,0); 

	$result= $this->FetchAll($result1); 

	return $result;		

	}



	function getAllMyFriendListbyUserId($uID, $orderField='', $orderBy='', $limit='', $offset=0) {

	  $fields=array("");	

	  $tables=array($this->userfriends);

	  $where = array("(frndid='$uID' OR userid='$uID') AND reqstatus='2'");

	  $order = array("");

	  $result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(), $limit, $offset,0); 

	  $result= $this->FetchAll($result1); 

	  return $result;		

	}



	## get friends request detail by request id

	function geFriendRequestDetailsById($reqID) {

	  $fields=array("");	

	  $tables=array($this->userfriends);

	  $where = array("reqid='$reqID'");

	  $order = array("");

	  $result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(), $limit='', $offset=0,0);

	  $result= $this->FetchRow($result1); 

	  return $result;		

	}



	function getAllTagsUserIdByUsesrId($userId)

	{

	  $sql = "SELECT userid,frndid FROM ".$this->userfriends." where (userid = '".$userId."' or   frndid = '".$userId."' ) and reqstatus = '2'"; 

	  $result1= $this->ExecuteQuery($sql); 

	  $result= $this->FetchAll($result1); 

	  return $result;	

	}

	

	function getAllContactsByUserId($userIds,$search)

         {

         $sql = "SELECT * FROM ".$this->userprofile." where status = '2' and userId  IN (".$userIds.") and ( fname LIKE '%".$search."%' or lname LIKE '%".$search."%' or username LIKE '%".$search."%')";

	  $result1= $this->ExecuteQuery($sql); 

	  $result= $this->FetchAll($result1); 

	  return $result;	

         }



	function getAllTagListByUserProfileId($uID, $orderField='', $orderBy='', $limit='', $offset=0) {

	  $fields=array("");	

	  $tables=array($this->userfriends);

	  $where = array("(frndid='$uID' OR userid='$uID') AND reqstatus='2'");

	  $order = array("");

	  $result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(), $limit, $offset,0); 

	  $result= $this->FetchRow($result1); 

	  return $result;		

	}

	

	## update friends status by user id and friends id

	function updateFriendsStatusByValue($status,$uID,$friendID) {

	  $sql = "UPDATE ".$this->userfriends." SET reqstatus = '".$status."' WHERE userid = '".$uID."' AND frndid = '".$friendID."'";		

	  $result1= $this->ExecuteQuery($sql);	

	}



	function updateUnjoinFriendsStatusByValue($status,$uID,$friendID) {

	 $sql = "UPDATE ".$this->userfriends." SET reqstatus = '".$status."' WHERE  (userid = '".$uID."' AND frndid = '".$friendID."') or (userid = '".$friendID."' AND frndid = '".$uID."')"; 		

	  $result1= $this->ExecuteQuery($sql);	

	}



	## Edit user by userid

	function editFriendRequestValueById($array, $Id){

	   $this->UpdateData($this->userfriends,$array,"reqid",$Id,0);

	}



	## Delete friend request by request id

	function deleteRequestValueById($reqID){

	   $this->DeleteData($this->userfriends,"reqid",$reqID,0); 

	}



	## delete friends status by user id and friends id

	function deleteFriendsStatusByValue($uID,$friendID) {

	$sql = "DELETE FROM ".$this->userfriends." WHERE userId = '".$uID."' AND frndid = '".$friendID."'";

	//echo $sql; exit;		

	$result1= $this->ExecuteQuery($sql);	

	}



	function getMyFriendforGroup($uID,$param) {

	  $sql = "SELECT u . *,t.*,up.fname,up.lname,up.email FROM ".$this->userfriends." AS t LEFT JOIN ".$this->user." AS u ON  t.frndid =t.userId left join ".$this->userprofile." as up on  t.userid = up.id WHERE t.frndid='$uID' and up.fname LIKE  '%".$param."%' OR up.lname LIKE  '%".$param."%' and t.reqstatus ='2'"; 

	 //REGEXP '^$param'

	  $result1= $this->ExecuteQuery($sql); 

	  $result= $this->FetchAll($result1); 

	  return $result;	

	}



	function getMyAllMyTagsList($uID,$param,$orderField='', $orderBy='', $limit='', $offset=0) {

	$fields=array("f.*,up.*");

	$tables=array($this->userfriends. " as f", $this->userprofile. " as up");

	$where = array("(f.frndid='$uID' OR f.userid='$uID') AND f.reqstatus='2' AND (f.frndid=up.id OR f.userid=up.id)");

	if($param!='')

	{

	    $where[] = "(up.fName LIKE  '%".$param."%' OR up.lName LIKE  '%".$param."%')"; 

	}  

	$order = array("");

	$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array('up.id OR f.frndid'), $limit, $offset,0); 

	$result= $this->FetchAll($result1); 

	return $result;		

	}



	/**************************************************************************************************************/

	function getMyAllTagUsers($uID,$param,$orderField='', $orderBy='', $limit='', $offset=0) {

	$fields=array("up.*");

	$tables=array($this->userprofile. " as up");

	if($param!='')

	{

	    $where[] = "(up.fName LIKE  '%".$param."%' OR up.lName LIKE  '%".$param."%')"; 

	}  

	$order = array("");

	$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(), $limit, $offset,0); 

	$result= $this->FetchAll($result1); 

	return $result;		

	}

	/**************************************************************************************************************/



	// Get User Id And User Type Details

	function getUserIdAndUserTypeDetails($id) {

	 $fields=array("userId,userType");	

	 $tables=array($this->userprofile);

	 $where=array("id='".$id."'");		

	 $result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 

	 $result= $this->FetchRow($result1); 

	 return $result;		

	}

}

?>

