<?php class Model_Email extends Database

{	

	function Model_Email(){

		$this->email = EMAILS;

		$this->emailbackup  = EMAILBACKUPS;	

		$this->smtpsettings = SMTPSETTINGS;

		$this->Database();

	}	

	

	// for add emaiil

	function addEmail( $array ){

		$this->InsertData( $this->email , $array );		

		$insertId = mysql_insert_id();

		return $insertId;

	}

	

	// for edit email

	function editEmailById($array,$id){

		$this->UpdateData($this->email,$array,"emailID",$id,0);		

	}

	

	// for edit email

	function editEmailBackupById($array,$id){

		$this->UpdateData($this->emailbackup,$array,"id",$id,0);		

	}

	

	// for edit smtp

	function editSMTPSettingsByValueId($array,$id){

		$this->UpdateData($this->smtpsettings,$array,"id",$id,0);		

	}  

	

	// get SMTP By ID

	function getSMTPById($id){

		$fields=array();

		$tables=array($this->smtpsettings);

		$where=array("id='".$id."'");

		$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0);

		$result= $this->FetchRow($result1);

		return $result;		

	}

	

	// for delete email

	function deleteEmailById($Id){

		$this->DeleteData($this->email,"emailID",$Id);

	}

	

	// get all email value

	function getAllEmailByValue( $val, $limit='' , $offset=''){

		$fields = array();	

		$tables = array( $this->email);

		if($val!='Email'){

			$where =array("(emailType LIKE '%".$val."%' || fEmail LIKE '%".$val."%')" );

		}

		else{

			$where=array();

		}

		$result1 = $this->SelectData( $fields,$tables, $where, $order = array( "emailID ASC" ), $group=array(),$limit,$offset,0 );

		$result= $this->FetchAll( $result1 );

		return $result;

	}



	// get all emails

	function getAllEmails(){

		$fields = array();	

		$tables = array( $this->email);

		$result1 = $this->SelectData( $fields,$tables, $where, $order = array("emailID ASC"), $group=array(),$limit,$offset,0 );

		$result= $this->FetchAll( $result1 );

		return $result;

	}

	

	// get email By ID

	function getEmailById($id){

		$fields=array();

		$tables=array($this->email);

		$where=array("emailID='".$id."'");

		$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0);

		$result= $this->FetchRow($result1);

		return $result;		

	}



	// get smtp details

	function getActiveSMTPDetails(){

		$fields=array();

		$tables=array($this->smtpsettings);

		$where=array("status='2'");

		$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0);

		$result= $this->FetchRow($result1);

		return $result;		

	}

	

	function getAllSMTPServers()

	{

		$fields=array();

		$tables=array($this->smtpsettings);

		$where=array("status <>'0'");

		$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0);

		$result= $this->FetchAll($result1);

		return $result;	

	}

	

	// for add emaiil    backup 

    	function getEmailBackup($array){

        $this->InsertData( $this->emailbackup , $array );        

        $insertId = mysql_insert_id();

        return $insertId;

    	}

    

     	function getEmailBackupById($id){

        $fields=array();

        $tables=array($this->emailbackup);

        $where=array("id='".$id."'");

        $result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0);

        $result= $this->FetchRow($result1);

        return $result;        

        }

    

        function getAllEmailBackups($orderField,$orderBy = 'ASC',$status, $startdate,$enddate,$limit,$offset) {

        $fields=array();    

        $tables=array($this->emailbackup);  

        if($status!='')

        {

             $where=array("status = '$status'"); 

        }

        else   

        $where=array("status <> '0'");  

        if($startdate!='' && $enddate!='')

        {

              $where[]=" (date between '$startdate' and '$enddate')"; 

        } 

        $order = array($orderField.' '.$orderBy);

        $result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(), $limit,$offset,0);

        $result= $this->FetchAll($result1); 

        return $result;        

    }

    	

}

?>