<?php

/**************************************************/

## Class Name - Model_Sports (Contains all the functions related sports)

/**************************************************/

class Model_Sports extends Database 

{	

	## Constructor

	function Model_Sports() {

		$this->sports = SPORTS;

		$this->Database();

	}

	

	## Add sports in database

	function addNewSports($Array) {

		$this->InsertData($this->sports , $Array );		

		$insertId = mysql_insert_id();

		return $insertId;

	}	

	

	## Edit sports by id

	function editSportsValueById($array, $Id){

		$this->UpdateData($this->sports,$array,"id",$Id,0);

	}

	

	## Delete sports by id

	function deleteSportsValueById($id){

		$this->DeleteData($this->sports,"id",$id);

	}

	

	## Update sports status with multiple ids

	function updateSportsStatus($ids, $status) {

		$sql = "UPDATE ".$this->sports." SET status = '".$status."' WHERE id IN (".$ids.")";

		$result1= $this->ExecuteQuery($sql);	 

	}

	

	function editimageByValue($postion,$id){

		$sql = "UPDATE ".$this->sports." SET status = '1' WHERE id !='".$id."' AND sportsposition='".$postion."'";

		$result1= $this->ExecuteQuery($sql);

	}

	

	## Delete sports with multiple ids

	function deleteSports($ids) {

		$sql = "DELETE FROM ".$this->sports." WHERE id IN (".$ids.")";

		$result1= $this->ExecuteQuery($sql);	 

	}

	

	

	## Getting all categories from the datbase

	function getAllActiveSports(){

		$fields   = array();	

		$tables   = array($this->sports);

		$where    = array("status = '2' ");		

		$result1  = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = 0,0,0); 

		$result   = $this->FetchAll($result1); 

		return $result;	

	}

			

	## fetch active random sports

	function getRandomActiveSports($pos='', $limit='') {

		$fields=array();	

		$tables=array($this->sports);

		$where=array("status='2'");

		if($pos!=''){

			$where[] = "sportsPosition = '".$pos."'";

		}		

		$result1 = $this->SelectData($fields,$tables, $where, $order = array("RAND()"), $group=array(),$limit,0,0); 

		$result= $this->FetchRow($result1); 

		return $result;	

	}



	## Getting all sports from the datbase

	function getAllSportsForSports($search, $orderField = 'sportsTitle', $orderBy = 'ASC', $limit='',$offset=''){

		$fields   = array();	

		$tables   = array($this->sports);

		$where    = array("status !='0'");

		if($search!=''){

			$where[] = "sportsTitle LIKE '%".$search."%'";

		}

		$order = array("$orderField $orderBy");

		$result1  = $this->SelectData($fields,$tables, $where, $order, $group=array(),$limit,$offset,0); 

		$result   = $this->FetchAll($result1); 

		return $result;	

	}

	

	## Getting sports detail by sports id

	function getSportsDetailById($Id){

		$fields   = array("");	

		$tables   = array($this->sports);

		if($Id!='')

		{

		$where    = array("id = '".$Id."'");

		}

		$result1  = $this->SelectData($fields,$tables, $where, $order = array(""), $group=array(),$limit = 0,0,0); 

		$result   = $this->FetchRow($result1); 

		return $result;	

	}





	function getRandomAddsByPageID($Id){

		$fields   = array("");	

		$tables   = array($this->sports);

		if($Id!='')

		{

		$where    = array("pages LIKE  ('%2,3%') AND status = '2' ");

		}

		$result1  = $this->SelectData($fields,$tables, $where, $order = array("rand()"), $group=array(),$limit = 0,0,0); 

		$result   = $this->FetchRow($result1); 

		return $result;	

	}

    

	## Get sports image by id

	function getSportsImageByid($id) {

		$fields=array("id,sportsImage,sportsTitle");	

		$tables=array($this->sports);

		$where=array("id='".$id."'");		

		$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 

		$result= $this->FetchRow($result1); 

		return $result;		

	}

	

	## Getting bottom sports

	function getAllActiveSportsByValue($position){

		$fields   = array();	

		$tables   = array($this->sports);

		$where    = array("status = '2' AND sportsPosition LIKE '%".$position."%' ");		

		$result1  = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = 0,0,0); 

		$result   = $this->FetchRow($result1); 

		return $result;	

	}



	## Get sports name by multiple sports Id's

	function getSportsNameUsingSportsId($ids) {
		if($ids!=''){
		$fields = array("sportsTitle");	
		$tables = array($this->sports);
		$where= array("id IN ('".$ids."')");
		$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0);
		$result  = $this->FetchArrayAll($result1,"sportsTitle"); 
		return $result;
		}
	}

	
function getSportsNameAndImageUsingSportsId($ids) {
		$fields = array("*");	
		$tables = array($this->sports);
		$where= array("id IN ($ids)");
		$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 
		$result  = $this->FetchAll($result1); 
		return $result;
	}
}

?>