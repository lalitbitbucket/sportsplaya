<?php
/**************************************************/
## Class Name - Model_organizer (Contains all the functions related organizer and organizer profile)

/**************************************************/

class Model_organizer extends Database 
{	
	## Constructor
	function Model_organizer() {
		$this->organizer = ORGANIZER;		
		$this->Database();
	}	
	
	// for add organizer
	function addorganizer($array) {
		$this->InsertData($this->organizer, $array);
	}
	## Add organizer in database
	function addorganizerByValue($Array) {
		$this->InsertData( $this->organizer , $Array );		
		$insertId = mysql_insert_id();
		return $insertId;
	}
		
	## Edit organizer by organizer id
	function editorganizerValueById($array, $Id){
		$this->UpdateData($this->organizer,$array,"orgID",$Id,0);
	}
	
	## Delete organizer by organizer id
	function deleteorganizerValueById($id){
		$this->DeleteData($this->organizer,"orgID",$id);
	}
	
	## Get all organizer
	function getAllorganizer($search='',$orderField='orgName',$orderBy='ASC',$limit='',$offset='') {
		$fields=array();	
		$tables=array($this->organizer);
		$where = array("status <> '0'");
		if($search != '' && $search != 'Search') {
			$where[] = "(orgName LIKE '%".addslashes(trim($search))."%' )";
		}
		$order = array($orderField.' '.$orderBy);
		$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(), $limit,$offset,0);
		$result= $this->FetchAll($result1); 
		return $result;		
	}	
	
	## Get all active organizer
	function getAllActiveorganizer() {
		$fields=array();	
		$tables=array($this->organizer);
		$where = array("status = '2' ");
		$result1 = $this->SelectData($fields,$tables, $where, $order=array('orgName ASC'), $group=array(), $limit='',$offset='',0);
		$result= $this->FetchAll($result1); 
		return $result;		
	}	
	
	## Get organizer details by organizer id
	function getorganizerDetailsByorganizerId($id) {
		$fields=array();	
		$tables=array($this->organizer);
		$where=array("orgID='".$id."'");		
		$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 
		$result= $this->FetchRow($result1); 
		return $result;		
	}
	
	// Globle organizer name exist or not	
	function checkorganizerName($id='',$organizername) {
		$fields=array();
		$tables=array($this->organizer);
		$where=array("orgName='".$organizername."' AND status!='0'");
		if($id!=''){
			$where[] = " orgID!='".$id."'";
		}
		$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0);
		$result= $this->FetchRow($result1);
		return $result;		
	}
	
	## Edit organizer by organizerid
	function editorganizerById($array, $organizerid){
		$this->UpdateData($this->organizer,$array,"orgID",$organizerid,0);
	}
	
	## Update organizer status with multiple organizerids
	function updateorganizerStatus($organizerid, $status) {		
		$sql = "UPDATE ".$this->organizer." SET status='".$status."' WHERE orgID IN (".$organizerid.")";
		$result1= $this->ExecuteQuery($sql);	 
		
	}
	
	## Delete organizer with multiple organizerids
	function deleteorganizer($organizerids) {
		
		$sql = "UPDATE ".$this->organizer." SET status='0' WHERE orgID IN (".$organizerids.")";
		$result1= $this->ExecuteQuery($sql);	 
	}
	
	## Delete organizer by id
	function deleteorganizerById($organizerid){
		$sql = "UPDATE ".$this->organizer." SET status='0' WHERE orgID ='".$organizerid."'";
		$result1= $this->ExecuteQuery($sql);	 
	}
	
	## Get organizer details by organizerid
	function getorganizerDetailsById($organizerid) {
		$fields = array();	
		$tables = array($this->organizer);
		$result1 = $this->SelectData($fields,$tables, $where= array("orgID='".$organizerid."'"), $order = array(), $group=array(),$limit = "",0,0); 
		$result  = $this->FetchRow($result1); 
		return $result;
	}
	
	## Update organizer status with multiple organizerids
	function updateMultipleorganizerStatus($organizerids, $status) {
		$sql = "UPDATE ".$this->organizer." SET status='".$status."' WHERE orgID IN (".$organizerids.")";
		$result1= $this->ExecuteQuery($sql);	 
	}
	
	## Delete organizer category with multiple organizerids
	function deleteMultipleorganizer($organizerids) {
		$sql = "UPDATE ".$this->organizer." SET status='0' WHERE orgID IN (".$organizerids.")";
		$result1= $this->ExecuteQuery($sql);	 
	}
	## List all active organizer name
	function getAllorganizerWithActiveStatus() {
		$fields = array('');	
		$tables = array($this->organizer);
		$where= array("status='2'");
		$result1 = $this->SelectData($fields,$tables, $where, $order = array("orgName ASC"), $group=array(),$limit,$offset,0); 
		$result  = $this->FetchAll($result1); 
		return $result;
	}
	
}
?>
