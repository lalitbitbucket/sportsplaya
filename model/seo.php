<?php 

class Model_SEO extends Database
{	
	function Model_SEO() {	
		$this->Seos = SEO;
		$this->Database();
	}
	
	// for add SEO
	function addSeo($array) {
		$this->InsertData($this->seos, $array);
	}
	
	
	## List all seos
	function getAllSeos($search,$orderField='seoModule', $orderBy='ASC',$limit='',$offset='') {
		$fields = array();	
		$tables = array($this->Seos);
		if(trim($search)) {
			$where[] = "seoKeywords LIKE '%".$search."%'";
		}		
		$order = array("$orderField $orderBy");	
		$result1 = $this->SelectData($fields,$tables, $where , $order, $group=array(),$limit,$offset,0); 
		$result  = $this->FetchAll($result1); 
		return $result;
	}
	
	
	
	## Get Seo details by id
	function getSeoDetailsById($id) {
		$fields = array();	
		$tables = array($this->Seos);
		$result1 = $this->SelectData($fields,$tables, $where= array("seoID='$id'"), $order = array(), $group=array(),$limit = "",0,0); 
		$result  = $this->FetchRow($result1); 
		return $result;
	}
	
	## Edit Seo by id
	function editSeoById($array, $Id){
		$this->UpdateData($this->Seos,$array,"seoID",$Id,1);
	}

}
?>