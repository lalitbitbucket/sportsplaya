<?php
/**************************************************/
## Class Name - Model_States (Contains all the functions related states and states profile)
/**************************************************/
class Model_States extends Database 
{	
	## Constructor
	function Model_States() {
		$this->states = STATES;
		$this->country = COUNTRY;
		$this->Database();
	}
	
	## Add states in database
	function addStateByValue($Array) {
		$this->InsertData( $this->states, $Array );		
		$insertId = mysql_insert_id();
		return $insertId;
	}
		
	## Edit states by statesid
	function editStateValueById($array, $Id){
		$this->UpdateData($this->states,$array,"statID",$Id,0);
	}
	
	## Delete states by state id
	function deleteStateValueById($id){
		$this->DeleteData($this->states,"statID",$id);
	}
	
	## Get all states
	function getAllStates($search='',$search_country='',$orderField='',$orderBy='',$limit='',$offset='') {
		$fields=array();	
		$tables=array($this->states);
		$where = array("statStatus <> '0'");
		if($search != '' && $search != 'State Name') {
			//$where[] = "concat(first_name,' ',last_name) like '%".$search."%'";
			$where[] = "(statName LIKE '%".$search."%' )";
		}
		if($search_country!=0) {
			$where[] = "cntrID='$search_country'";
		}
		  $order = array($orderField.' '.$orderBy);
		$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(), $limit,$offset,0);
		$result= $this->FetchAll($result1); 
		return $result;		
	}	
	
	## Get all active states
	function getAllActiveStates() {
		$fields=array();	
		$tables=array($this->states);
		$where = array("statStatus <> '0'");
		$result1 = $this->SelectData($fields,$tables, $where, $order=array('statName ASC'), $group=array(), $limit='',$offset='',0);
		$result= $this->FetchAll($result1); 
		return $result;		
	}	
	
	## Get states details by state id
	function getStateDetailsByStateId($id) {
		$fields=array();	
		$tables=array($this->states);
		$where=array("statID='".$id."'");		
		$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 
		$result= $this->FetchRow($result1); 
		return $result;		
	}
	
	## Get all active states by country id
	function getAllActiveStateByCountryId($countryId) {
		
		$fields=array();	
		$tables=array($this->states);
		$where = array("cntrID = '".$countryId."'AND statStatus = '2' ");
		$result1 = $this->SelectData($fields,$tables, $where, $order=array('statName ASC'), $group=array(), $limit='',$offset='',0); 
		$result= $this->FetchAll($result1); 
		return $result;		
	}
	
	## Get all active states by country id
	function getAllStateByCountryId($countryId) {
		$fields=array();	
		$tables=array($this->states);
		$where = array("cntrID = '".$countryId."'");
		$result1 = $this->SelectData($fields,$tables, $where, $order=array('statName ASC'), $group=array(), $limit='',$offset='',0);
		$result= $this->FetchAll($result1); 
		return $result;		
	}
	
	function getStateNameByStateId($stateId) {
		$fields=array();	
		$tables=array($this->states);
		$where = array("statID = '".$stateId."'");
	        $result= $this->FetchRow($result1); 
		return $result;		
	}
	
	## List all active country name
	function getAllCountryWithActiveStatus() {
		$fields = array('');	
		$tables = array($this->country);
		$where= array("countryStatus='1'");
		$result1 = $this->SelectData($fields,$tables, $where, $order = array("countryName ASC"), $group=array(),$limit,$offset,0); 
		$result  = $this->FetchAll($result1); 
		return $result;
	}
	
	## Get state details by stateid
	function getStateDetailsById($stateid) {
		$fields = array();	
		$tables = array($this->states);
		$result1 = $this->SelectData($fields,$tables, $where= array("statID='".$stateid."'"), $order = array(), $group=array(),$limit = "",0,0); 
		$result  = $this->FetchRow($result1); 
		//print_r($result);
		return $result;
	}
	
	## Edit Country by stateid
	function editStateById($array, $stateid){
		$this->UpdateData($this->states,$array,"statID",$stateid,0);
	}
	
	## checking state name exist or not 
	function checkStateName($statename, $id='') {
		$fields = array();	
		$tables = array($this->states);
		$where= array("UPPER(statName)='".$statename."'");
		if($id != '' && $id !='0') {
			$where[] = " statID!='".$id."'";	
		}
		$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 
		$result  = $this->FetchRow($result1); 
		return $result;
	}
	
	## Update State status with multiple stateids
	function updateStateStatus($stateid, $status) {		
		$sql = "UPDATE ".$this->states." SET statStatus='".$status."' WHERE statID = (".$stateid.")";
		$result1= $this->ExecuteQuery($sql);	 
		
	}
	
		## Update country status with multiple stateids
	function updateMultipleStateStatus($stateids, $status) {
		$sql = "UPDATE ".$this->states." SET statStatus='".$status."' WHERE statID IN (".$stateids.")";
		$result1= $this->ExecuteQuery($sql);	 
	}
	
	## Delete country category with multiple stateids
	function deleteMultipleState($stateids) {
		$sql = "DELETE FROM ".$this->states." WHERE statID IN (".$stateids.")";
		$result1= $this->ExecuteQuery($sql);	 
	}
	
	## List all active state name
	function getAllStateWithActiveStatus() {
		$fields = array('');	
		$tables = array($this->states);
		$where= array("statStatus='2'");
		$result1 = $this->SelectData($fields,$tables, $where, $order = array("statName ASC"), $group=array(),$limit,$offset,0); 
		$result  = $this->FetchAll($result1); 
		return $result;
	}
}
?>
