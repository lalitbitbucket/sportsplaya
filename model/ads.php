<?php
/**************************************************/
## Class Name - Model_Ads (Contains all the functions related ads)
/**************************************************/

class Model_Ads extends Database 
{	
	## Constructor
	function Model_Ads() {
		$this->ads = ADS;
		$this->ads_stats = ADSTATS;
		$this->pages=PAGESFORADS;
		$this->Database();
	}
	
	## Add ads in database
	function addNewAds($Array) {
		$this->InsertData($this->ads , $Array );		
		$insertId = mysql_insert_id();
		return $insertId;
	}	

	 # add advertise stats 
    	function addAdvertiseStats($Array) {
		$this->InsertData( $this->ads_stats , $Array );        
		$insertId = mysql_insert_id();
		return $insertId;
    	}
    
	## Edit ads by adsId
	function editAdsValueById($array, $Id){
		$this->UpdateData($this->ads,$array,"adsId",$Id,0);
	}
	
	## Delete ads by adsId
	function deleteAdsValueById($adsId){
		$this->DeleteData($this->ads,"adsId",$adsId);
	}
	
	## Update ads status with multiple adsIds
	function updateAdsStatus($adsIds, $status) {
		$sql = "UPDATE ".$this->ads." SET status = '".$status."' WHERE adsId IN (".$adsIds.")";
		$result1= $this->ExecuteQuery($sql);	 
	}
	
	function editimageByValue($postion,$id){
		$sql = "UPDATE ".$this->ads." SET status = '1' WHERE adsId !='".$id."' AND adsposition='".$postion."'";
		$result1= $this->ExecuteQuery($sql);
	}
	
	## Delete ads with multiple adsIds
	function deleteAds($adsIds) {
		$sql = "DELETE FROM ".$this->ads." WHERE adsId IN (".$adsIds.")";
		$result1= $this->ExecuteQuery($sql);	 
	}
	
	
	## Getting all categories from the datbase
	function getAllActiveAds(){
		$fields   = array();	
		$tables   = array($this->ads);
		$where    = array("status = '1' ");		
		$result1  = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = 0,0,0); 
		$result   = $this->FetchAll($result1); 
		return $result;	
	}

	function getAllActivePagesForAds(){
		$fields   = array();	
		$tables   = array($this->pages);
		$where    = array("status = '2' ");		
		$result1  = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = 0,0,0); 
		$result   = $this->FetchAll($result1); 
		return $result;	
	}
		
	## fetch active random ads
	function getRandomActiveAds($pos='', $limit='') {
		$fields=array();	
		$tables=array($this->ads);
		$where=array("status='2'");
		if($pos!=''){
			$where[] = "adsPosition = '".$pos."'";
		}		
		$result1 = $this->SelectData($fields,$tables, $where, $order = array("RAND()"), $group=array(),$limit,0,0); 
		$result= $this->FetchRow($result1); 
		return $result;	
	}

	## Getting all ads from the datbase
	function getAllAdsForAds($search, $orderField = 'adsTitle', $orderBy = 'ASC', $limit='',$offset=''){
		$fields   = array();	
		$tables   = array($this->ads);
		$where    = array("status ='2'");
		if($search!=''){
			$where[] = "adsTitle LIKE '%".$search."%'";
		}
		$order = array("$orderField $orderBy");
		$result1  = $this->SelectData($fields,$tables, $where, $order, $group=array(),$limit,$offset,0); 
		$result   = $this->FetchAll($result1); 
		return $result;	
	}
	
	## Getting ads detail by ads id
	function getAdsDetailById($Id){
		$fields   = array("");	
		$tables   = array($this->ads);
		if($Id!='')
		{
		$where    = array("adsId = '".$Id."'");
		}
		$result1  = $this->SelectData($fields,$tables, $where, $order = array(""), $group=array(),$limit = 0,0,0); 
		$result   = $this->FetchRow($result1); 
		return $result;	
	}


	function getRandomAddsByPageID($Id){
		$fields   = array("");	
		$tables   = array($this->ads);
		if($Id!='')
		{
		$where    = array("pages LIKE  ('%2,3%') AND status = '2' ");
		}
		$result1  = $this->SelectData($fields,$tables, $where, $order = array("rand()"), $group=array(),$limit = 0,0,0); 
		$result   = $this->FetchRow($result1); 
		return $result;	
	}

    
	## Get ads image by adsId
	function getAdsImageByadsId($adsId) {
		$fields=array("adsId,adsImage,adsPosition");	
		$tables=array($this->ads);
		$where=array("adsId='".$adsId."'");		
		$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 
		$result= $this->FetchRow($result1); 
		return $result;		
	}
	
	
	## Getting bottom ads
	function getAllActiveAdsByValue($position){
		$fields   = array();	
		$tables   = array($this->ads);
		$where    = array("status = '2' AND adsPosition LIKE '%".$position."%' ");		
		$result1  = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = 0,0,0); 
		$result   = $this->FetchRow($result1); 
		return $result;	
	}
	 # ads statistics by ads Id
	function getAdsStatisticsByAdsId($adsID,$stime,$etime){
		$fields=array("");    
		$tables=array($this->ads_stats);
		$where=array("adsId='".$adsID."'");  
		if($stime!='' && $etime!='')
		{
		   $where[] = "(clickTime >=  '".$stime."' and  clickTime <= '".$etime."' )"; 
		}      
		$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 
		$result= $this->FetchAll($result1); 
		return $result;
	}
        function getTotalCountOfClickedAds($adsID,$stime,$etime){
		$fields=array("count(id) as total");    
		$tables=array($this->ads_stats);
		$where=array("adsId='".$adsID."'");  
		if($stime!='' && $etime!='')
		{
		   $where[] = "(clickTime >=  '".$stime."' and  clickTime <= '".$etime."' )"; 
		}      
		$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 
		$result= $this->FetchRow($result1); 
		return $result;
	}
	function getTotalCountOfDailyClickedAds($adsID,$stime,$etime){ 
		$fields=array("count(id) as total");    
		$tables=array($this->ads_stats);
		$where=array("adsId='".$adsID."'");  
		if($stime!='' )
		{
		 $where[] = "(clickTime >=  '".$stime."' and  clickTime < '".$etime."' )";
		}      
		$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 
		$result= $this->FetchRow($result1); 
		return $result;
	}
	function getTotalCountOfWeeklyClickedAds($adsID,$stime,$etime){ 
		$fields=array("count(id) as total");    
		$tables=array($this->ads_stats);
		$where=array("adsId='".$adsID."'");  
		if($stime!='' )
		{
		 $where[] = "(clickTime >=  '".$stime."' and  clickTime < '".$etime."' )";
		}      
		$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 
		$result= $this->FetchRow($result1); 
		return $result;
	}		
}
?>