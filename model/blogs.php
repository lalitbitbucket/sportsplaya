<?php
class Model_Blogs extends Database 
{	
	## Constructor
	function Model_Blogs() {
		$this->blogs = BLOGS;
		$this->comment = COMMENT;
		$this->Database();
	}	
	
	## Add blogs in database
	function addBlogsByValue($Array) {
		$this->InsertData( $this->blogs , $Array );		
		$insertId = mysql_insert_id();
		return $insertId;
	}	
	
	## Edit blogs by blogID
	function editBlogsValueById($array, $Id){
		$this->UpdateData($this->blogs,$array,"blogID",$Id,0);
	}
	
	## Update blogs status by blogID 
	function updateBlogsStatus($blogID, $status) {		
		$sql = "UPDATE ".$this->blogs." SET status='".$status."' WHERE blogID = '".$blogID."'";
		$result1= $this->ExecuteQuery($sql);	 
	}
	
	## Delete blogs by blogID
	function deleteBlogsValueById($blogID){
		$this->DeleteData($this->blogs,"blogID",$blogID);
	}
	
	## Delete Blogs by id
	function deleteBlogs($blogID) {
	echo $sql = "UPDATE ".$this->blogs." SET status='0' WHERE blogID = '".$blogID."'";
		$result1= $this->ExecuteQuery($sql);
	}
	
	## Update blogs status with multiple blogIDs
	function updateMultipleBlogsStatus($blogIDs, $status) {
		$sql = "UPDATE ".$this->blogs." SET status='".$status."' WHERE blogID IN (".$blogIDs.")";
		$result1= $this->ExecuteQuery($sql);	 
	}
	
	## Delete Blogs with multiple blogIDs
	function deleteMultipleBlogs($blogIDs) {
		$sql = "DELETE FROM ".$this->blogs." WHERE blogID IN (".$blogIDs.")";
		$result1= $this->ExecuteQuery($sql);	 
	}
	
	## Get blogs details by blogID
	function getBlogsDetailsById($blogID) {
		$fields=array();	
		$tables=array($this->blogs);
		$where=array("blogID='".$blogID."'");		
		$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 
		$result= $this->FetchRow($result1); 
		return $result;		
	}
	
	## Get all blogs
	function getAllBlogs($search='', $orderField = 'title', $orderBy = 'ASC', $limit='',$offset=0) {
		$fields=array();	
		$tables=array($this->blogs);
		$where = array("status!='0'");
		if($search != '' && $search != 'Search') {
			$where = array("title LIKE '%".$search."%' ");
		} 
		$order = array("$orderField $orderBy");
		$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(), $limit,$offset,0);
		$result= $this->FetchAll($result1); 
		return $result;		
	}
	
	## Get all blogs
	function getAllActiveBlogs() {
		$fields=array();	
		$tables=array($this->blogs);
		$where = array("status = '2'");
		$result1 = $this->SelectData($fields,$tables, $where, $order=array("title ASC"), $group=array(), $limit='',$offset=0,0);
		$result= $this->FetchAll($result1); 
		return $result;		
	}
	
	function getRecentBlogs($search='', $month='', $year='', $orderField = 'addedDate', $orderBy = 'DESC', $limit='2',$offset='0') {
		$fields=array();	
		$tables=array($this->blogs);
		$where=array("status = '2'");
		if($search != '' && $search != 'Blog Title') {
			$where[] = "title LIKE '%".$search."%' ";
		}
		if($month != '' && $year != '') {
			$where[] = " MONTH(addedDate) = '".$month."' AND YEAR(addedDate) = '".$year."'";
		}
		$order = array("$orderField $orderBy");
		$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(), $limit,$offset,0);
		$result= $this->FetchAll($result1); 
		return $result;		
	}
	
	function getAllExceptBlogsIds($search='',$orderField = '', $orderBy = '', $limit='',$offset='0') {
	
		$fields=array();	
		$tables=array($this->blogs);
		$where = array("status = '2'");
		if($search != '' && $search != 'Blog Title') {
			$where[] = "title LIKE '%".$search."%'";
		}
	        $order = array("$orderField $orderBy");
		$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(), $limit,$offset,0); 
		$result= $this->FetchAll($result1); 
		return $result;		
	}

       ## Comment Section
	function addComment($array)
	{
		$this->InsertData( $this->comment,$array );		
		$insertId = mysql_insert_id();
		return $insertId;
	}
	
	## Get Comment details by id
	function getcommentDetailsByBlogId($blogID) {
		$fields = array();	
		$tables = array($this->comment);
		$where = array("status!='0'and blogID = '".$blogID."'");
		$result1 = $this->SelectData($fields,$tables, $where , $order = array("addedDate DESC"), $group=array(),$limit = "",0,0); 
		$result  = $this->FetchAll($result1); 
		return $result;
	}
	
	function getTotalCommentCountByBlogID($blogID) {
		$fields = array("count(*) as cnt");	
		$tables = array($this->comment);
		$where = array("status!='0'and blogID = '".$blogID."'");
		$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 
		$result  = $this->FetchRow($result1); 
		return $result['cnt'];
	}

	function addAnswer($array)
	{
		$this->InsertData( $this->comment,$array );		
		$insertId = mysql_insert_id();
		return $insertId;
	}

	## Delete Comment by id
	function deleteComment($blogCommentID) {
	$sql = "UPDATE ".$this->comment." SET status='0' WHERE blogCommentID = '".$blogCommentID."'";
		$result1= $this->ExecuteQuery($sql);
	}
	
	function getArchiveBlogs() {
		$fields=array("EXTRACT(YEAR FROM addedDate) AS BlogYear, EXTRACT(MONTH FROM addedDate) AS BlogMonth, blogID, addedDate");	
		$tables=array($this->blogs);
		$where=array("status = '2'");
		$order = array("addedDate DESC");
		$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array("BlogMonth"), $limit='12',$offset=0,0);
		$result= $this->FetchAll($result1); 
		return $result;		
	}
}
?>
