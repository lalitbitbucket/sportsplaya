<?php
class Model_League extends Database 
{	
	## Constructor
	function Model_League() {
		$this->league = LEAGUE;		
		$this->Database();
	}
	
	## Add leagues in database
	function addLeagueByValue($Array) {
		$this->InsertData( $this->league , $Array );		
		$insertId = mysql_insert_id();
		return $insertId;
	}	
	
	## Edit leagues by trainingID
	function editLeagueValueById($array, $Id){
		$this->UpdateData($this->league,$array,"leagueID",$Id,0);
	}
	
	## Update leagues status by trainingID 
	function updateLeagueStatus($leagueID, $status) {		
		$sql = "UPDATE ".$this->league." SET status='".$status."' WHERE leagueID = '".$leagueID."'";
		$result1= $this->ExecuteQuery($sql);	 
		
	}
	
	## Delete leagues by id
	function deleteLeagueValueById($leagueID){
		$this->DeleteData($this->league,"leagueID",$leagueID);
	}
	
	## Delete Training by id
	function deleteLeague($leagueID) {
		$sql = "UPDATE ".$this->league." SET status='0' WHERE leagueID = '".$leagueID."'";
		$result1= $this->ExecuteQuery($sql);
	}
	
	## Update leagues status with multiple trainingIDs
	function updateMultipleLeagueStatus($leagueID, $status) {		
		$sql = "UPDATE ".$this->league." SET status='".$status."' WHERE leagueID IN (".$leagueID.")";
		$result1= $this->ExecuteQuery($sql);	 
	}
	
	## Delete training with multiple trainingIDs
	function deleteMultipleLeague($leagueID) {
		$sql = "UPDATE ".$this->league." SET status='0' WHERE leagueID IN (".$leagueID.")";
		$result1= $this->ExecuteQuery($sql);	 
	}
	
	## Get leagues details by 
	function getLeagueDetailsById($leagueID) {
		$fields=array();	
		$tables=array($this->league);
		$where=array("leagueID='".$leagueID."'");		
		$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 
		$result= $this->FetchRow($result1); 
		return $result;		
	}
	
	## Get all leagues
	function getAllLeague($search='', $orderField = 'leagueName', $orderBy = 'ASC', $limit='',$offset='') {
		$fields=array();	
		$tables=array($this->league);
		$where = array("status!='0'");
		if($search != '' && $search != 'Search') {
			$where = array("leagueName LIKE '%".$search."%' ");
		} 
		$order = array("$orderField $orderBy");
		$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(), $limit,$offset,0);
		$result= $this->FetchAll($result1); 
		return $result;		
	}
	
	## Get all leagues
	function getAllleagueByUserId($userId,$search, $orderField,$orderBy = 'ASC', $limit,$offset) {
        $fields=array("*");    
        $tables=array($this->league);
        $where = array();
        $where[] = "status = '2'";  
        if($userId!='')
         {
                 $where[] = "userId='".$userId."'";    
         }
        if($search != '' && $search != 'League Name') {
        $where[] = "(leagueName LIKE '%".$search."%')";
        }
       
        $result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(), $limit,$offset,0);
        $result= $this->FetchAll($result1); 
        return $result;        
      }

	function getAllActiveLeague($searchbyCharacter='') {
		$fields=array();	
		$tables=array($this->league);
		$where = array("status = '2'");
		if($searchbyCharacter != '') { 
			$where = array(" leagueName LIKE  '".$searchbyCharacter."%'");
		}
		$result1 = $this->SelectData($fields,$tables, $where, $order=array("leagueName ASC"), $group=array(), $limit='',$offset=0,0);
		$result= $this->FetchAll($result1); 
		return $result;		
	}
		
	## Get leagues logo by trainingID
	function getLeagueImageByLeagueId($leagueID) {
		$fields=array("leagueID,logo");	
		$tables=array($this->league);
		$where=array("leagueID='".$leagueID."'");		
		$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 
		$result= $this->FetchRow($result1); 
		return $result;		
	}
	
	## Checking wheather user enetered leagues leagueName is already exists or not 
	function checkLeagueNameExists($leagueName,$leagueID){
		$fields = array();
		$tables = array($this->league);
		if($leagueID) {
			$where  = array("LOWER(leagueName) = '".$leagueName."' AND leagueID != '".$leagueID."' AND status!='0' ");
		} else {
			$where  = array("LOWER(leagueName) = '".$leagueName."' AND status!='0'");
		}
		$result = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 
		$result1 = $this->FetchRow($result);
		return $result1;
	}
	## Get last five added leagues
	function lastAddedFiveLeagues() {
		$fields=array();	
		$tables=array($this->league);
		$where = array("status!='0'");
		$order = array("date DESC");
		$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(), $limit="5", $offset="0",0);
		$result= $this->FetchAll($result1); 
		return $result;		
	}
	
	## Get three random leagues
	function getRandomActiveLeagues($userID='',$limit='3', $offset='0') {
		$fields=array();	
		$tables=array($this->league);
		$where = array("(status='2')");
		if($userID!=''){
			$where[] = "(moderatorID!='".$userID."')";
			
		}
		$order = array("RAND()");
		$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(), $limit, $offset,0);
		$result= $this->FetchAll($result1); 
		return $result;		
	}	
	
}
?>