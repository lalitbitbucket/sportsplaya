<?php 
//================================================================================================================================
/*
	$total_rows		=	TOTAL NUMBER OF ROWS IN THE RECORD FETCHED
	$rowsPerPage	=	NUMBER OF ROWS TO BE DISPLAYED PER PAGE
	$pageNum		=	CURRENT PAGE NUMBER
	$pageName		=	CURRENT PAGE NAME
	$other_id		=	ANY EXTRA ID THAT USER WANTS TO SEND (EG.USER_ID)
	$class			=	CSS CLASS THAT USER WANTS TO APPLY
*/
//================================================================================================================================
//============================User Side Pagination================================================================================
# writes out page links
function quickLink ($linkHref, $desc, $accessKey, $linkTitle) {

  $theLink = '<a href="'. SITE_URL."/".$linkHref .'" title="'. $desc .'" accesskey="'.$accessKey .'">'. $linkTitle .'</a>';
   return $theLink;

}


// google_like_pagination.php
function pagination($number, $show, $showing, $firstlink, $baselink, $total_records="", $directory) {

	$disp = floor($show / 2);
    if ( $showing <= $disp) :

        //if ( ($disp - $showing) > 0 ):
        //$low  = ($disp - $showing);
        //else:
        $low = 1;
       // endif;
        $high = ($low + $show) - 1;

    elseif ( ($showing + $disp) > $number) :

        $high = $number;
        $low = ($number - $show) + 1;

    else:

        $low  = ($showing - $disp);
        $high = ($showing + $disp); 

    endif;

    // next / prev / first / last
    if ( ($showing - 1) > 0 ) :
        if ( ($showing - 1) == 1 ):
        $prev  = quickLink ($directory."/".($showing - 1)."/".$firstlink, '<', '', "<" );
        else:
        $prev  = quickLink ($directory."/".($showing - 1)."/".$baselink,
        '<', 'z', "<");
        endif;
    else:
        $prev  = '';
    endif;

    $next  = ($showing + 1) <= $number ?
    quickLink ($directory."/".($showing + 1)."/".$baselink, '>', 'x', ">") : '';
	//'<img src="'. SITE_URL.'/images/prv_btn1.png>'

    if ( $prev == '') 
    	$first = '';
    else
    	$first = quickLink ($directory."/"."1/".$firstlink, '<<', '', "<<");

    if ( $showing == $number ):
    $last = '';
    else:
    $last = quickLink ($directory."/".$number."/".$baselink, '>>', '', ">>");
    endif;


    $navi = '<div id="page_list_links">'."";
    // start the navi

        $navi .= $first . ' '. $prev ." ";

    // loop through the numbers

    foreach (range($low, $high) as $newnumber):

 
           if($newnumber < 0)
		   		continue;
		   $link = ( $newnumber == 1 ) ? "1/".$firstlink :
                $newnumber."/".$baselink;
           if ($newnumber > $number):
        $navi .= '';
        elseif ($newnumber == 0):
        $navi .= '';
        else:
        $navi .= ( $newnumber == $showing ) ?
            ' <a class="active">'. $newnumber .'</a> '."" :
            ' '. quickLink ($directory."/".$link, 'Page '. $newnumber, '', $newnumber) ." ";
        endif;
    endforeach;

	$navi .= ' '. $next ." " . $last;

	if($total_records)
		$navi .= " &nbsp; &nbsp; <span class='strong'>" . $total_records . " Records Found</span> &nbsp;";

    $navi .= '</div><br/>';

	return $navi;

}
//=========================================================================================================================================================================
class pagination
{	
	function paginate_new_with_id($total_rows , $rowsPerPage , $pageNum , $pageName , $other_id, $class)
	{
		$links_perpage 			= 	5;								// TOTAL NUMBER OF LINKS TO BE DISPLAYED PER PAGE
		$self 					= 	$pageName;						// NAME OF THE MAIN PAGE
		
		$nav  					= 	'';
		$maxPage 				= 	ceil($total_rows/$rowsPerPage);
		$lastfive 				= 	$maxPage - 3;					 
		$nextfive 				= 	$pageNum + 2;
		$oid					= 	$other_id;
		
		for($page = 1; $page <= $maxPage; $page++)
		{
			if ($page == $pageNum)									// IF ON THE CURRENT PAGE
			{
			
				$prevpg 		= 	$page - 1; 						// PREVIOUS PAGE
				$prev2pg 		= 	$prevpg - 1;					// PREVIOUS 2 PAGES
				$nxtpg 			= 	$page+1;						// NEXT PAGE
				
				$lastpg 		= 	$maxPage;						// LAST PAGE
				$last2pg 		= 	lastpg - 1;						// SECOND LAST PAGE
				$final 			= 	$maxPage;						// LAST PAGE
				
				
				if($prevpg)
				{
					if($prev2pg)
					{
						$nav 	.= 	" <a href=\"$self?page=$prev2pg&$oid\" class="."$class".">$prev2pg</a> &nbsp;"; 		// LINKS FOR PREVIOUS 2 PAGEs
						
					}
					$nav 		.= 	" <a href=\"$self?page=$prevpg&$oid\" class="."$class".">$prevpg</a> &nbsp;"; 		// FOR PREVIOUS PAGE
				}
					
				$nav 			.= 	"<b class="."pagination_selected".">$page</b>&nbsp;"; 													// no need to create a link to current page
				
				
				if($prevpg == 0)																				// IN CASE WE ARE ON FIRST PAGE FIRST 5 PAGES LINK DISPLAYED
				{
					if($maxPage <= $links_perpage)
					{
						for($k=2 ; $k <= $maxPage; $k++)
						{						
							$nav 	.= 	" <a href=\"$self?page=$k&$oid\" class="."$class".">$k</a> &nbsp;";
						}
					}
					else
					{
						for($k=$nxtpg ; $k <= $links_perpage; $k++)
						{						
							$nav 	.= 	" <a href=\"$self?page=$k&$oid\" class="."$class".">$k</a> &nbsp;";
						}
					}
				}
				
				if($prevpg >=1)																					// IF WE ARE ON ONE OF THE MIDDLE PAGES
				{
					for($k=$pageNum+1 ; $k <= $nextfive; $k++)
					{
						
						if($k > $maxPage)																		// IF K COUNT GOES BEYOND LAST PAGE NO LINK
						{
							$nav .= "";
						}
						else
						{
							$nav .= " <a href=\"$self?page=$k&$oid\" class="."$class".">$k</a> &nbsp;";
						}
					}
				}
			}
				
		}
		// creating previous and next link
		// plus the link to go straight to
		// the first and last page
		if ($pageNum > 1)
		{
			
			$page  				= 	$pageNum;
			$prevpg 			= 	$page-1;
			
			if($pageNum > 3)
			{
				if($prevpg <= $maxPage)
				{	
					$prev  		= 	" <a href=\"$self?page=$prevpg&$oid\" class="."$class".">Prev</a> &nbsp;";
				}
			}
			$first 				= 	" <a href=\"$self?page=1&$oid\" class="."$class".">First</a> &nbsp;";
		}
		else
		{
			$prev  				= 	'&nbsp;'; // we're on page one, don't print previous link
			$first 				= 	'&nbsp;'; // nor the first page link
		}
		if ($pageNum < $maxPage)
		{
			$page 				= 	$pageNum + 1;
			$nextpg 			= 	$page;
			if($maxPage > $nextpg)
			{
				if( $pageNum == 1 && $maxPage == 5 )
				{
					$nextpg 			= 	5;
				}
				if( $pageNum == 1 && $maxPage > 5 )
				{
					$nextpg 	= 	2;
					$next 		= 	" <a href=\"$self?page=$nextpg&$oid\" class="."$class".">Next</a> &nbsp;";
				}
				else
					$next 		= 	" <a href=\"$self?page=$nextpg&$oid\" class="."$class".">Next</a> &nbsp;";
			}
			$last 				= 	" <a href=\"$self?page=$maxPage&$oid\" class="."$class".">Last</a> &nbsp;";
		}
		else
		{
			$next 				= 	'&nbsp;'; // we're on the last page, don't print next link
			$last 				= 	'&nbsp;'; // nor the last page link
		}
		// print the navigation link
		return $var =  $first . $prev . $nav . $next . $last;
	}
	
	
	function pagination_user($total_rows , $rowsPerPage , $pageNum , $pageName , $other_id, $class)
	{
		$links_perpage 			= 	5;								// TOTAL NUMBER OF LINKS TO BE DISPLAYED PER PAGE
		$self 					= 	$pageName;						// NAME OF THE MAIN PAGE
		
		$nav  					= 	'';
		$maxPage 				= 	ceil($total_rows/$rowsPerPage);
		$lastfive 				= 	$maxPage - 3;					 
		$nextfive 				= 	$pageNum + 2;
		$oid					= 	$other_id;
		
		for($page = 1; $page <= $maxPage; $page++)
		{
			if ($page == $pageNum)									// IF ON THE CURRENT PAGE
			{
			
				$prevpg 		= 	$page - 1; 						// PREVIOUS PAGE
				$prev2pg 		= 	$prevpg - 1;					// PREVIOUS 2 PAGES
				$nxtpg 			= 	$page+1;						// NEXT PAGE
				
				$lastpg 		= 	$maxPage;						// LAST PAGE
				$last2pg 		= 	lastpg - 1;						// SECOND LAST PAGE
				$final 			= 	$maxPage;						// LAST PAGE
				
				
				if($prevpg)
				{
					if($prev2pg)
					{
						if($oid != '') {
							$nav.= 	" <a href=\"$self&page=$prev2pg&$oid\" class="."$class".">$prev2pg</a> &nbsp;"; 		// LINKS FOR PREVIOUS 2 PAGEs
						} else {
							$nav.= 	" <a href=\"$self&page=$prev2pg\" class="."$class".">$prev2pg</a> &nbsp;"; 		// LINKS FOR PREVIOUS 2 PAGEs	
						}
						
					}
					if($oid != '') {
						$nav 		.= 	" <a href=\"$self&page=$prevpg&$oid\" class="."$class".">$prevpg</a> &nbsp;"; 		// FOR PREVIOUS PAGE
					} else {
						$nav 		.= 	" <a href=\"$self&page=$prevpg\" class="."$class".">$prevpg</a> &nbsp;"; 		// FOR PREVIOUS PAGE	
					}
				}
					
				$nav 			.= 	"<b class="."pagination_selected".">$page</b>&nbsp;"; 													// no need to create a link to current page
				
				
				if($prevpg == 0)																				// IN CASE WE ARE ON FIRST PAGE FIRST 5 PAGES LINK DISPLAYED
				{
					if($maxPage <= $links_perpage)
					{
						for($k=2 ; $k <= $maxPage; $k++)
						{	
							if($oid != '') {					
								$nav 	.= 	" <a href=\"$self&page=$k&$oid\" class="."$class".">$k</a> &nbsp;";
							} else {
								$nav 	.= 	" <a href=\"$self&page=$k\" class="."$class".">$k</a> &nbsp;";
							}
						}
					}
					else
					{
						for($k=$nxtpg ; $k <= $links_perpage; $k++)
						{		
							if($oid != '') {				
								$nav 	.= 	" <a href=\"$self&page=$k&$oid\" class="."$class".">$k</a> &nbsp;";
							} else {
								$nav 	.= 	" <a href=\"$self&page=$k\" class="."$class".">$k</a> &nbsp;";
							}	
						}
					}
				}
				
				if($prevpg >=1)																					// IF WE ARE ON ONE OF THE MIDDLE PAGES
				{
					for($k=$pageNum+1 ; $k <= $nextfive; $k++)
					{
						
						if($k > $maxPage)																		// IF K COUNT GOES BEYOND LAST PAGE NO LINK
						{
							$nav .= "";
						}
						else
						{
							if($oid != '') {
								$nav .= " <a href=\"$self&page=$k&$oid\" class="."$class".">$k</a> &nbsp;";
							} else {
								$nav .= " <a href=\"$self&page=$k\" class="."$class".">$k</a> &nbsp;";	
							}
						}
					}
				}
			}
				
		}
		// creating previous and next link
		// plus the link to go straight to
		// the first and last page
		if ($pageNum > 1)
		{
			
			$page  				= 	$pageNum;
			$prevpg 			= 	$page-1;
			
			if($pageNum > 3)
			{
				if($prevpg <= $maxPage)
				{	
					if($oid != '') {
						$prev  		= 	" <a href=\"$self&page=$prevpg&$oid\" class="."$class".">Prev</a> &nbsp;";
					} else {
						$prev  		= 	" <a href=\"$self&page=$prevpg\" class="."$class".">Prev</a> &nbsp;";
					}
				}
			}
				if($oid != '') {
					$first 				= 	" <a href=\"$self&page=1&$oid\" class="."$class".">First</a> &nbsp;";
				} else {
					$first 				= 	" <a href=\"$self&page=1\" class="."$class".">First</a> &nbsp;";
				}
		}
		else
		{
			$prev  				= 	'&nbsp;'; // we're on page one, don't print previous link
			$first 				= 	'&nbsp;'; // nor the first page link
		}
		if ($pageNum < $maxPage)
		{
			$page 				= 	$pageNum + 1;
			$nextpg 			= 	$page;
			if($maxPage > $nextpg)
			{
				if( $pageNum == 1 && $maxPage == 5 )
				{
					$nextpg 			= 	5;
				}
				if( $pageNum == 1 && $maxPage > 5 )
				{
					$nextpg 	= 	2;
					if($oid != '') {
						$next 		= 	" <a href=\"$self&page=$nextpg&$oid\" class="."$class".">Next</a> &nbsp;";
					} else {
						$next 		= 	" <a href=\"$self&page=$nextpg\" class="."$class".">Next</a> &nbsp;";
					}
				}
				else
				{
					if($oid != '') {	
						$next 		= 	" <a href=\"$self&page=$nextpg&$oid\" class="."$class".">Next</a> &nbsp;";
					} else {
						$next 		= 	" <a href=\"$self&page=$nextpg\" class="."$class".">Next</a> &nbsp;";
					}
				}
			}
			if($oid != '') {	
				$last 				= 	" <a href=\"$self&page=$maxPage&$oid\" class="."$class".">Last</a> &nbsp;";
			} else {
				$last 				= 	" <a href=\"$self&page=$maxPage\" class="."$class".">Last</a> &nbsp;";
			}
		}
		else
		{
			$next 				= 	'&nbsp;'; // we're on the last page, don't print next link
			$last 				= 	'&nbsp;'; // nor the last page link
		}
		// print the navigation link
		return $var =  $first . $prev . $nav . $next . $last;
	}
	
	
	
/******************************************************************************************/

/******************************************************************************************/

	
/****************************************************************************************/
// Pagination code for htaccess start
/****************************************************************************************/

function paginate_new($total_rows , $rowsPerPage , $pageNum , $pageName , $other_id, $class)
	{
		$links_perpage 			= 	5;								// TOTAL NUMBER OF LINKS TO BE DISPLAYED PER PAGE
		$self 					= 	$pageName;						// NAME OF THE MAIN PAGE
		
		$nav  					= 	'';
		$maxPage 				= 	ceil($total_rows/$rowsPerPage);
		$lastfive 				= 	$maxPage - 3;					 
		$nextfive 				= 	$pageNum + 2;
		$oid					= 	$other_id;
		
		for($page = 1; $page <= $maxPage; $page++)
		{
			if ($page == $pageNum)									// IF ON THE CURRENT PAGE
			{
			
				$prevpg 		= 	$page - 1; 						// PREVIOUS PAGE
				$prev2pg 		= 	$prevpg - 1;					// PREVIOUS 2 PAGES
				$nxtpg 			= 	$page+1;						// NEXT PAGE
				
				$lastpg 		= 	$maxPage;						// LAST PAGE
				$last2pg 		= 	lastpg - 1;						// SECOND LAST PAGE
				$final 			= 	$maxPage;						// LAST PAGE
				
				
				if($prevpg)
				{
					if($prev2pg)
					{
						$nav 	.= 	" <a href=\"$self/$prev2pg/\" class="."$class".">$prev2pg</a> &nbsp;"; 		// LINKS FOR PREVIOUS 2 PAGEs
					}
					$nav 		.= 	" <a href=\"$self/$prevpg/\" class="."$class".">$prevpg</a> &nbsp;"; 		// FOR PREVIOUS PAGE
				}
					
				$nav 			.= 	"<b>$page</b>&nbsp;"; 													// no need to create a link to current page
				
				
				if($prevpg == 0)																				// IN CASE WE ARE ON FIRST PAGE FIRST 5 PAGES LINK DISPLAYED
				{
					if($maxPage <= $links_perpage)
					{
						for($k=2 ; $k <= $maxPage; $k++)
						{						
							$nav 	.= 	" <a href=\"$self/$k/\" class="."$class".">$k</a> &nbsp;";
						}
					}
					else
					{
						for($k=$nxtpg ; $k <= $links_perpage; $k++)
						{						
							$nav 	.= 	" <a href=\"$self/$k/\" class="."$class".">$k</a> &nbsp;";
						}
					}
				}
				
				if($prevpg >=1)																					// IF WE ARE ON ONE OF THE MIDDLE PAGES
				{
					for($k=$pageNum+1 ; $k <= $nextfive; $k++)
					{
						
						if($k > $maxPage)																		// IF K COUNT GOES BEYOND LAST PAGE NO LINK
						{
							$nav .= "";
						}
						else
						{
							$nav .= " <a href=\"$self/$k/\" class="."$class".">$k</a> &nbsp;";
						}
					}
				}
			}
				
		}
		// creating previous and next link
		// plus the link to go straight to
		// the first and last page
		if ($pageNum > 1)
		{
			$page  				= 	$pageNum - 1;
			$prevpg 			= 	$page - 2;
			if($pageNum > 3)
			{
				if($prevpg <= $maxPage)
				{	
					$prev  		= 	" <a href=\"$self/$prevpg/\" class="."$class".">[Prev]</a> &nbsp;";
				}
			}
			$first 				= 	" <a href=\"$self/1/\" class="."$class".">[First Page]</a> &nbsp;";
		}
		else
		{
			$prev  				= 	'&nbsp;'; // we're on page one, don't print previous link
			$first 				= 	'&nbsp;'; // nor the first page link
		}
		if ($pageNum < $maxPage)
		{
			$page 				= 	$pageNum + 1;
			$nextpg 			= 	$page + 2;
			if($maxPage > $nextpg)
			{
				if( $pageNum == 1 && $maxPage == 5 )
				{
					$nextpg 			= 	5;
				}
				if( $pageNum == 1 && $maxPage > 5 )
				{
					$nextpg 	= 	6;
					$next 		= 	" <a href=\"$self/$nextpg/\" class="."$class".">[Next]</a> &nbsp;";
				}
				else
					$next 		= 	" <a href=\"$self/$nextpg/\" class="."$class".">[Next]</a> &nbsp;";
			}
			$last 				= 	" <a href=\"$self/$maxPage/\" class="."$class".">[Last Page]</a> &nbsp;";
		}
		else
		{
			$next 				= 	'&nbsp;'; // we're on the last page, don't print next link
			$last 				= 	'&nbsp;'; // nor the last page link
		}
		// print the navigation link
		return $var =  $first . $prev . $nav . $next . $last;
	}
}
/****************************************************************************************/
// Pagination code for htaccess end
/****************************************************************************************/

// INSTANCIATION OF CLASS IN THIS FORMAT
/*
$other_id = $user_id;
$pg 		= 	new pagination();					// CREATING OBJECT OF THE PAGINATION CLASS
echo $pg->paginate( $total_rows , $rowsPerPage , $pageNum , $pageName, $other_id );		*/

	// FUNCTION WITH IMAGEES FOR NEXT AND PREV
	
	// AJAX PAGINATION CLASS
	
class ajaxPagination
{
	function paginate($total_rows , $rowsPerPage , $pageNum , $pageName , $other_id, $class)
	{
		
		$links_perpage 			= 	5;								// TOTAL NUMBER OF LINKS TO BE DISPLAYED PER PAGE
		$self 					= 	$pageName;						// NAME OF THE MAIN PAGE
		
		$nav  					= 	'';
		$maxPage 				= 	ceil($total_rows/$rowsPerPage);
		$lastfive 				= 	$maxPage - 3;					 
		$nextfive 				= 	$pageNum + 2;
		$oid					= 	$other_id;
		
		for($page = 1; $page <= $maxPage; $page++)
		{
			if ($page == $pageNum)									// IF ON THE CURRENT PAGE
			{
			
				$prevpg 		= 	$page - 1; 						// PREVIOUS PAGE
				$prev2pg 		= 	$prevpg - 1;					// PREVIOUS 2 PAGES
				$nxtpg 			= 	$page+1;						// NEXT PAGE
				
				$lastpg 		= 	$maxPage;						// LAST PAGE
				$last2pg 		= 	lastpg - 1;						// SECOND LAST PAGE
				$final 			= 	$maxPage;						// LAST PAGE
				
				
				if($prevpg)
				{
					if($prev2pg)
					{
						//$nav 	.= 	" <a href=\"$self?page=$prev2pg&val=$oid\" class="."$class".">$prev2pg</a> &nbsp;"; 		// LINKS FOR PREVIOUS 2 PAGEs
						$nav 	.= 	" <a onclick='getsearch(\"$order\",\"$self\",\"$prev2pg\",\"$oid\")' style='cursor:pointer' class="."$class".">$prev2pg</a> &nbsp;"; 		// LINKS FOR PREVIOUS 2 PAGEs
						
					}
					//$nav 		.= 	" <a href=\"$self?page=$prevpg&val=$oid\" class="."$class".">$prevpg</a> &nbsp;"; 		// FOR PREVIOUS PAGE
					$nav 		.= 	"<a onclick='getsearch(\"$order\",\"$self\",\"$prevpg\",\"$oid\")' style='cursor:pointer' class="."$class".">$prevpg</a> &nbsp;"; 		// FOR PREVIOUS PAGE
				}
					
				$nav 			.= 	"<b>$page</b>&nbsp;"; 													// no need to create a link to current page
				
				
				if($prevpg == 0)																				// IN CASE WE ARE ON FIRST PAGE FIRST 5 PAGES LINK DISPLAYED
				{
					if($maxPage <= $links_perpage)
					{
						for($k=2 ; $k <= $maxPage; $k++)
						{						
							//$nav 	.= 	" <a href=\"$self?page=$k&val=$oid\" class="."$class".">$k</a> &nbsp;";
							$nav 	.= 	"<a onclick='getsearch(\"$order\",\"$self\",\"$k\",\"$oid\")' style='cursor:pointer' class="."$class".">$k</a> &nbsp;";
						}
					}
					else
					{
						for($k=$nxtpg ; $k <= $links_perpage; $k++)
						{						
							//$nav 	.= 	" <a href=\"$self?page=$k&val=$oid\" class="."$class".">$k</a> &nbsp;";
							$nav 	.= 	"<a onclick='getsearch(\"$order\",\"$self\",\"$k\",\"$oid\")' style='cursor:pointer' class="."$class".">$k</a> &nbsp;";
						}
					}
				}
				
				if($prevpg >=1)																					// IF WE ARE ON ONE OF THE MIDDLE PAGES
				{
					for($k=$pageNum+1 ; $k <= $nextfive; $k++)
					{
						
						if($k > $maxPage)																		// IF K COUNT GOES BEYOND LAST PAGE NO LINK
						{
							$nav .= "";
						}
						else
						{
							//$nav .= " <a href=\"$self?page=$k&val=$oid\" class="."$class".">$k</a> &nbsp;";
							$nav .= "<a onclick='getsearch(\"$order\",\"$self\",\"$k\",\"$oid\")' style='cursor:pointer' class="."$class".">$k</a> &nbsp;";
						}
					}
				}
			}
				
		}
		// creating previous and next link
		// plus the link to go straight to
		// the first and last page
		if ($pageNum > 1)
		{
			$page  				= 	$pageNum - 1;
			$prevpg 			= 	$page - 2;
			if($pageNum > 3)
			{
				if($prevpg <= $maxPage)
				{	
					//$prev  		= 	" <a href=\"$self?page=$prevpg&val=$oid\" class="."$class".">[Prev]</a> &nbsp;";
					$prev  		= 	" <a onclick='getsearch(\"$order\",\"$self\",\"$prevpg\",\"$oid\")' style='cursor:pointer' class="."$class".">[Prev]</a> &nbsp;";
				}
			}
			//$first 				= 	" <a href=\"$self?page=1&val=$oid\" class="."$class".">[First Page]</a> &nbsp;";
			$first 				= 	" <a onclick='getsearch(\"$order\",\"$self\",\"1\",\"$oid\")' style='cursor:pointer' class="."$class".">[First Page]</a> &nbsp;";
		}
		else
		{
			$prev  				= 	'&nbsp;'; // we're on page one, don't print previous link
			$first 				= 	'&nbsp;'; // nor the first page link
		}
		if ($pageNum < $maxPage)
		{
			$page 				= 	$pageNum + 1;
			$nextpg 			= 	$page + 2;
			if($maxPage > $nextpg)
			{
				if( $pageNum == 1 && $maxPage == 5 )
				{
					$nextpg 			= 	5;
				}
				if( $pageNum == 1 && $maxPage > 5 )
				{
					$nextpg 	= 	6;
					//$next 		= 	" <a href=\"$self?page=$nextpg&val=$oid\" class="."$class".">[Next]</a> &nbsp;";
					$next 		= 	" <a onclick='getsearch(\"$order\",\"$self\",\"$nextpg\",\"$oid\")' style='cursor:pointer' class="."$class".">[Next]</a> &nbsp;";
				}
				else
					//$next 		= 	" <a href=\"$self?page=$nextpg&val=$oid\" class="."$class".">[Next]</a> &nbsp;";
					$next 		= 	" <a onclick='getsearch(\"$order\",\"$self\",\"$nextpg\",\"$oid\")' style='cursor:pointer' class="."$class".">[Next]</a> &nbsp;";
			}
			//$last 				= 	" <a href=\"$self?page=$maxPage&val=$oid\" class="."$class".">[Last Page]</a> &nbsp;";
			$last 				= 	" <a onclick='getsearch(\"$order\",\"$self\",\"$maxPage\",\"$oid\")' style='cursor:pointer' class="."$class".">[Last Page]</a> &nbsp;";
		}
		else
		{
			$next 				= 	'&nbsp;'; // we're on the last page, don't print next link
			$last 				= 	'&nbsp;'; // nor the last page link
		}
		// print the navigation link
		return $var =  $first . $prev . $nav . $next . $last;
	}
}

?>