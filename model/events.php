<?php 



class Model_EVENTS extends Database

{	

	function Model_EVENTS() {	

		$this->events = EVENTS;

		$this->userprofile = USERPROFILE;

		$this->event_invitation = EVENT_INVITATION;

		$this->eventcomment = EVENTCOMMENT;	

		$this->eventsubcomment = EVENTSUBCOMMENT;	

		$this->eventcommentlikes = EVENTCOMMENTLIKES;

		$this->user = USERS;

    	$this->sports=SPORTS;

		$this->events_url=EVENTS_URL;

       	$this->Database();

	}
	
    ##Get upcoming Three Events
    function getupcomingthreeevents(){
  	$fields=array("*,DATE_FORMAT(event_startdate , '%b %D %Y' ) as modify_event_startdate");	
  	$tables=array($this->events);
  	$where = array(" event_startdate >= CURDATE()");
  	$order = array("event_startdate ASC");
  	$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(), $limit='3',0,3);
  	$result= $this->FetchAll($result1); 
  	return $result;		
    }

    ## Get all events Urls
    function getEventUrls() {
  	$fields=array("*");	
  	$tables=array($this->events_url);
  	$where = array("");
  	$order = array("");
  	$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(), $limit='',0,0);
  	$result= $this->FetchAll($result1); 
  	return $result;		
	}
  
    ## Check Events Url Exists or not
    function checkEventUrlExists($url) {
  	$fields=array("*");	
  	$tables=array($this->events_url);
  	$where = array("event_address_url='".$url."'");
  	$order = array("");
  	$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(), $limit='',0,0);
  	$result= $this->FetchRow($result1); 
  	return $result;		
	}
    
    ## Insert All Url Which Have Events Data
    function addEventUrls($Array) {
	$this->InsertData( $this->events_url , $Array );		
	$insertId = mysql_insert_id();
	return $insertId;
	}


    ## Get all events list
    function getAllActiveEventsList($sportsID) {
    
  	$sql = "SELECT evnt . * , sprt.sportsImage AS sportsImage,DATE_FORMAT(evnt.event_startdate, '%D %b %y' ) as modify_start_date, DATE_FORMAT(evnt.evnet_enddate, '%D %b %y' ) as modify_end_date FROM ".$this->events." AS evnt LEFT JOIN ".$this->sports." AS sprt ON evnt.sportsid = sprt.id WHERE sportsid IN (".$sportsID.")";

	$result1= $this->ExecuteQuery($sql);	 
    
    $result = $this->FetchAll($result1); 

	return $result;
	}



	## Add event comment in database

	function addEventComment($Array) {

	$this->InsertData( $this->eventcomment , $Array );		

	$insertId = mysql_insert_id();

	return $insertId;

	}



	## Add sub event comment

	function addEventSubComment($Array) {

	$this->InsertData( $this->eventsubcomment , $Array );		

	$insertId = mysql_insert_id();

	return $insertId;

	}



	## Delete sub evevent comment

	function deleteUserSubComment($id){

	$this->DeleteData($this->eventsubcomment,"id",$id);

	}



	//Function to get sub comment count.

	function getTotalSubComments($eventCommId) {

	$fields = array("COUNT(subComm.id) as totalRecords");	

	$tables = array("$this->eventsubcomment as subComm");

	$where  = array("subComm.eventCommId = '".$eventCommId."'");		

	$sql    = $this->SelectData($fields,$tables, $where, $order, $group=array(),$limit='',0,0);  

	$result = $this->FetchRow($sql); 

	return $result['totalRecords'];

	}

	

	## Function to get last sub comment by id

	function getLastSubCommentAddedByUser($subCommId) {

	$fields = array("subComm.*,u.fname,u.lname,u.userId as uID,u.birthdate,u.userType,u.avatar,u.sex,u.username,u.email");    

	$tables = array("$this->eventsubcomment as subComm, $this->userprofile as u");

	$where  = array("subComm.id= '".$subCommId."' AND subComm.comment_by = u.id");		

	$sql    = $this->SelectData($fields,$tables, $where, $order, $group=array(),$limit='',0,0); 

	$result = $this->FetchRow($sql); 

	return $result;

	}



	//Function to get comments on activity

	function getEventAllComments($eventCommId) {

	$fields = array("subComm.*,u.fname,u.lname,u.userId as uID,u.birthdate,u.userType,u.avatar,u.sex,u.username,u.email");    

	$tables = array("$this->eventsubcomment as subComm, $this->userprofile as u");

	$where  = array("subComm.eventCommId= '".$eventCommId."' AND subComm.comment_by = u.id");		

	$sql    = $this->SelectData($fields,$tables, $where, $order, $group=array("subComm.id DESC"),$limit='',0,0);

	$result = $this->FetchAll($sql); 

	return $result;

	}

	

	function getAllEventCommtsByEventId($eventId) {

	$fields=array();	

	$tables=array($this->eventcomment);

	$where=array("eventId ='".$eventId."' AND status='2'");		

	$result1 = $this->SelectData($fields,$tables, $where, $order = array("date DESC"), $group=array(),$limit = "",0,0);

	$result= $this->FetchAll($result1); 

	return $result;		

	}



	## Get event comment details by ID

	function getAllEventCommtDetailsById($eventId) {

	$fields=array();	

	$tables=array($this->eventcomment);

	$where=array("id ='".$eventId."' AND status='2'");		

	$result1 = $this->SelectData($fields,$tables, $where, $order = array("date DESC"), $group=array(),$limit = "",0,0); 

	$result= $this->FetchAll($result1); 

	return $result;		

	}

	

	function getlastCommentAddedByUser($commentID) {

        $fields = array("e.*,u.fname,u.lname,u.userId as uID,u.birthdate,u.userType,u.avatar,u.sex,u.username");    

        $tables = array($this->eventcomment." as e",$this->userprofile." as u");

        $result1 = $this->SelectData($fields,$tables, $where= array("e.userId=u.id AND e.id='".$commentID."'"), $order = array(), $group=array(),$limit = "",0,0); 

        $result  = $this->FetchRow($result1); 

        return $result;

    	}



    	function updateJoinEventByEventAndUserId($status,$eventId,$fromId,$toId) {

	$sql = "UPDATE ".$this->event_invitation." SET status = '".$status."' WHERE eventID = '".$eventId."' AND fromID = '".$fromId."' AND toIDs = '".$toId."'";

	$result1= $this->ExecuteQuery($sql);	

	}



	## Get all Events

	// select all events

	function getAllEventsList($search, $orderField,$orderBy = 'ASC', $limit,$offset) {

	$fields=array("e.*");	

	$tables=array($this->events.' as e');

	$where = array();

	if($search != '' && $search != 'Event Name') {

	$where[] = "(e.event_name LIKE '%".$search."%')";

	}

	// $order = array($orderField.' '.$orderBy);

	$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(), $limit,$offset,0);

	$result= $this->FetchAll($result1); 

	return $result;		

	}



	/****** Get All Going Events **********/

	function getGoingEventList($userId) {

        $fields = array("e.event_name,e.event_address,e.event_startdate,e.evnet_enddate,e.event_description,e.event_starttime,e.event_endtime,ei.*");    

        $tables = array($this->events." as e",$this->event_invitation." as ei");

        $result1 = $this->SelectData($fields,$tables, $where= array("e.event_id = ei.eventID AND ei.status='3' AND ei.toIDs =".$userId), $order = array(), $group=array(),$limit = "",0,0); 

        $result  = $this->FetchAll($result1); 

        return $result;

    	}



    	function getInviteArrayByEventIDAndUserID($id,$userId){

    	$fields=array("");	  

	$tables=array($this->event_invitation);

	$where = array("eventID='".$id."' AND fromID='".$userId."' AND status='2'");			 

	$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(), $limit,$offset,0);

	$result= $this->FetchAll($result1); 

	return $result;	

    	}



    	function getJoinedArrayByEventIDAndUserID($id,$userId)

	{

    	$fields=array("");	  

	$tables=array($this->event_invitation);

	$where = array("eventID='".$id."' AND fromID='".$userId."' AND status='3'");			 

	$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(), $limit,$offset,0);

	$result= $this->FetchAll($result1); 

	return $result;	

	}



	function getEventsListByUserProfileId($userId,$orderField='',$orderBy='',$limit,$offset) {

	$fields=array();	

	$tables=array($this->events);

	$where = array("userId='".$userId."' AND status='2'");

	$result1 = $this->SelectData($fields,$tables, $where, $order=array("$orderField $orderBy"), $group=array(),$limit,$offset,0); 

	$result= $this->FetchAll($result1); 

	return $result;		

	}

    

	function getAllEventsByUserId($userId,$search, $orderField,$orderBy = 'ASC', $limit,$offset) {

	$fields=array("*");    

	$tables=array($this->events);

	$where = array();

	$where[] = "status = '2'";  

	if($userId!='')

	{

	$where[] = "userId='".$userId."'";    

	}

	if($search != '' && $search != 'Event Name') {

	$where[] = "(event_name LIKE '%".$search."%')";

	}

	$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(), $limit,$offset,0); 

	$result= $this->FetchAll($result1); 

	return $result;        

	}



	## Take event count

	function getEventCountCount($userId) {

  	$fields=array("count(*) as cnt");	

  	$tables=array($this->events);

  	$where = array("event_userId='".$userId."' AND status='2'");

  	$order = array("");

  	$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(), $limit='', $offset='0',0);

  	$result= $this->FetchRow($result1); 

  	return $result['cnt'];		

	}

    

    	//add Events in database

	function addEevnts($eventArray) {

	$this->InsertData($this->events,$eventArray);  

	$insertId = mysql_insert_id();

	return $insertId;

	}

	

	function sendInvitation($inviteArray){

	$this->InsertData($this->event_invitation,$inviteArray);  

	$insertId = mysql_insert_id();

	return $insertId;

	}



	function updateInvitationByInviteID($inviteArray,$id){

	$this->UpdateData($this->event_invitation,$inviteArray,"inviteID",$id,0);

	$insertId = mysql_insert_id();

	return $insertId;

	}



	## Add event members in database

	function addEventsMembersByValue($Array) {

	$this->InsertData( $this->event_invitation , $Array );		

	$insertId = mysql_insert_id();

	return $insertId;

	}

	

	function updateEventDetails($Array,$Id)

	{

	$this->UpdateData($this->events,$Array,"event_id",$Id,0);

	$insertId = mysql_insert_id();

	return $insertId;

	}

	

	 ## Delete user by userid

	function deleteEventById($id){

		$this->DeleteData($this->events,"event_id",$id);

	}



	## Delete event comment bt id

	function deleteEventCommById($id){

		$this->DeleteData($this->eventcomment,"id",$id);

	}

	

	## Get Event Details by Event Id

        public function getEventDetailsById($id) {

	$fields=array("*,DATE_FORMAT(event_startdate, '%D %b %y' ) as modified_event_startdate,DATE_FORMAT(evnet_enddate, '%D %b %y' ) as modified_event_enddate");	

	$tables=array($this->events);

	$where=array("event_id='".$id."'");		

	$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 

	$result= $this->FetchRow($result1); 

	return $result;		

	}

	

       ## Add user in database

	function addUserByValue($Array) {

	$this->InsertData( $this->user , $Array );		

	$insertId = mysql_insert_id();

	return $insertId;

	}

	

	function getAllEventCommtsByScrolling($eventId,$postnumbers,$offset) {

	$fields=array();	

	$tables=array($this->eventcomment);

	$where=array("eventId ='".$eventId."' AND status='2'");		

	$result1 = $this->SelectData($fields,$tables, $where, $order = array("id DESC"), $group=array(),$limit =$postnumbers,$offset,0); 

	$result= $this->FetchAll($result1); 

	return $result;		

	}

	

	function getEventCommtsCount($eventId) {

  	$fields=array("count(*) as cnt");	

  	$tables=array($this->eventcomment);

  	$where = array("eventId ='".$eventId."' AND status='2'");

  	$order = array("");

  	$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(), $limit='', $offset='0',0);

  	$result= $this->FetchRow($result1); 

  	return $result['cnt'];		

	}



	/***********************************************************************************/

	## Check user like or not on activity

	function checkUserLikeOnActvity($commentId,$userProfileId)

	{

	$fields=array();	

	$tables=array($this->eventcommentlikes);

	$where=array("commentId='".$commentId."' AND userProfileId='".$userProfileId."'");		

	$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 

	$result= $this->FetchRow($result1); 

	return $result;	

	}



	## Get event comment likes for gold,silver & count

	function getAllGoldlikecount($commentId) {

	$fields=array("count(*) as cnt");	

	$tables=array($this->eventcommentlikes);

	$where=array("commentId='".$commentId."' AND likeType='1'");				

	$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 

	$result= $this->FetchRow($result1); 

	return $result['cnt'];		

	}



	function getAllSilverlikecount($commentId) {

	$fields=array("count(*) as cnt");	

	$tables=array($this->eventcommentlikes);

	$where=array("commentId='".$commentId."' AND likeType='2'");				

	$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 

	$result= $this->FetchRow($result1); 

	return $result['cnt'];		

	}



	function getAllBronzelikecount($commentId) {

	$fields=array("count(*) as cnt");	

	$tables=array($this->eventcommentlikes);

	$where=array("commentId='".$commentId."' AND likeType='3'");				

	$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 

	$result= $this->FetchRow($result1); 

	return $result['cnt'];		

	}



	## Get gold likes

	function getGoldLikes($commentId,$userProfileId,$commType)

	{

	$fields=array();	

	$tables=array($this->eventcommentlikes);

	$where=array("commentId='".$commentId."' AND likeType ='".$commType."' AND userProfileId ='".$userProfileId."'");		

	$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 

	$result= $this->FetchRow($result1); 

	return $result;	

	}



	// for delete gold like by id

	function deleteGoldLike($Id) {

	$this->DeleteData($this->eventcommentlikes,"id",$Id);

	}



	// Add gold likes

	function addToGoldLikeList($array)

	{

	 $this->InsertData($this->eventcommentlikes, $array);

	 return mysql_insert_id();

	}



	/*********************************************************/

	## Get silver likes

	function getSilverLikes($commentId,$userProfileId,$commType)

	{

	$fields=array();	

	$tables=array($this->eventcommentlikes);

	$where=array("commentId='".$commentId."' AND likeType =".$commType." AND userProfileId =".$userProfileId."");		

	$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 

	$result= $this->FetchRow($result1); 

	return $result;	

	}



	// for delete silver like by id

	function deleteSilverLike($Id) {

	$this->DeleteData($this->eventcommentlikes,"id",$Id);

	}



	// Add silver likes

	function addToSilverLikeList($array)

	{

	    $this->InsertData($this->eventcommentlikes, $array);

	    return mysql_insert_id();

	}



	/**************************************************************/

	## Get bronze likes

	function getBronzeLikes($commentId,$userProfileId,$commType)

	{

	$fields=array();	

	$tables=array($this->eventcommentlikes);

	$where=array("commentId='".$commentId."' AND likeType =".$commType." AND userProfileId =".$userProfileId."");		

	$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 

	$result= $this->FetchRow($result1); 

	return $result;	

	}



	// for delete bronze like by id

	function deleteBronzeLike($Id) {

	$this->DeleteData($this->eventcommentlikes,"id",$Id);

	}



	// Add bronze likes

	function addToBronzeLikeList($array)

	{

	$this->InsertData($this->eventcommentlikes, $array);

	return mysql_insert_id();

	}

	/***************************************function made by gaurav****************************************************/

	

	function getallEventCommtsCountByeventId($eventId) {

  	$fields=array("count(*) as cnt");	

  	$tables=array($this->eventcomment);

  	$where = array("eventId ='".$eventId."' AND status='2'");

  	$order = array("");

  	$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(), $limit='', $offset='0',0);

  	$result= $this->FetchRow($result1); 

  	return $result['cnt'];		

	}

	

  	function getAllCommentListPerEvent($eventId,$search, $orderField,$orderBy = 'ASC', $limit,$offset) 

   	{

	$fields=array("e.*");	

	$tables=array($this->eventcomment.' as e');

	$where = array("eventId ='".$eventId."' AND status='2'");

	if($search != '' && $search != 'Comment') {

	$where[] = "(e.eventComm LIKE '%".$search."%')";

	}

	$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(), $limit,$offset,0);

	$result= $this->FetchAll($result1); 

	return $result;		

	}

	

     	function deletecommentByCommentId($Id)

	{

	 $this->DeleteData($this->eventcomment,"id",$Id);

	}

	

	function getAllLikecountOnComment($commentId)

	{

	$fields=array("count(likeType) as cnt");	

	$tables=array($this->eventcommentlikes);

	$where=array("commentId='".$commentId."'");				

	$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 

	$result= $this->FetchRow($result1); 

	return $result['cnt'];		

	}

	

   	function deleteSubcommentByCommentId($Id)

	{

	$this->DeleteData($this->eventsubcomment,"id",$Id);

	}

	

	function getAllSubCommentListPerComment($commentId,$search, $orderField,$orderBy = 'ASC', $limit='',$offset='') 

	{

	$fields=array("e.*");	

	$tables=array($this->eventsubcomment.' as e');

	$where = array("eventCommId ='".$commentId."' AND status='2'");

	if($search != '' && $search != 'Sub Cooment') {

	$where[] = "(e.comment LIKE '%".$search."%')";

	}

	// $order = array($orderField.' '.$orderBy);

	$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(), $limit,$offset,0);

	$result= $this->FetchAll($result1); 

	return $result;		

	}

	function deletelikebylikeId($Id)

	{

	$this->DeleteData($this->eventcommentlikes,"id",$Id);

	}

	

	function getAllCommentlikeListPercomm($commentId,$search, $orderField,$orderBy = 'ASC', $limit,$offset) 

	{

	$fields=array("e.*");	

	$tables=array($this->eventcommentlikes.' as e');

	$where = array("commentId ='".$commentId."'");

	// $order = array($orderField.' '.$orderBy);

	$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(), $limit,$offset,0);

	$result= $this->FetchAll($result1); 

	return $result;		

	}

	 

	public function getPostDetailByPostId($postId) {

	$fields=array();	

	$tables=array($this->eventcomment);

	$where=array("id='".$postId."'");		

	$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 

	$result= $this->FetchRow($result1); 

	return $result;		

	}



	function getLikeCountByCommentIdandLikeType($commentId,$likeType) {	

	$fields=array("count(*) as cnt");	

	$tables=array($this->eventcommentlikes);

	$where = array("commentId='".$commentId."' AND  likeType ='".$likeType."'");

	$order = array("");

	$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(), $limit='', $offset='0',0);

	$result= $this->FetchRow($result1); 

	return $result['cnt'];		

	}

 }

?>