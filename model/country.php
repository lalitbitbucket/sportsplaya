<?php
/**************************************************/
## Class Name - Model_Country (Contains all the functions related country and country profile)
/**************************************************/
class Model_Country extends Database 
{	
	## Constructor
	function Model_Country() {
		$this->country = COUNTRY;
        	$this->states = STATES;
		$this->Database();
	}	
	
	// for add Country
	function addCountry($array) {
		$this->InsertData($this->country, $array);
	}
	## Add country in database
	function addCountryByValue($Array) {
		$this->InsertData( $this->country , $Array );		
		$insertId = mysql_insert_id();
		return $insertId;
	}
		
	## Delete country by country id
	function deleteCountryValueById($id){
		$this->DeleteData($this->country,"countryId",$id);
	}
	
	## Get all country
	function getAllCountry($search='',$orderField='countryName',$searchstatus,$orderBy='ASC',$limit='',$offset='') {
	$fields=array();	
	$tables=array($this->country);
	$where = array("countryStatus <> '0'");
	if($search != '' && $search != 'Search') {
	$where[] = "(countryName LIKE '%".addslashes(trim($search))."%'  or countryFullName  LIKE '%".addslashes(trim($search))."%' or iso2  LIKE '%".addslashes(trim($search))."%' or iso3  LIKE '%".addslashes(trim($search))."%')";
	}
	if($searchstatus != '' ) {
	if($searchstatus == 'block')
	$searchstatus =3;
	else  if($searchstatus == 'active')
	$searchstatus =2;
	else
	$searchstatus =1;
	$where[] = "countryStatus = '".$searchstatus."'";
	}
 	$order = array($orderField.' '.$orderBy);
	$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(), $limit,$offset,0);
	$result= $this->FetchAll($result1); 
	return $result;		
	}	
	
	## Get all active country
	function getAllActiveCountry() {
		$fields=array();	
		$tables=array($this->country);
		$where = array("countryStatus = '2' or countryStatus = '3'");
		$result1 = $this->SelectData($fields,$tables, $where, $order=array('countryName ASC'), $group=array(), $limit='',$offset='',0);
		$result= $this->FetchAll($result1); 
		return $result;		
	}

	function getAllTenActiveCountry() {
		$fields=array();	
		$tables=array($this->country);
		$where = array("countryStatus = '2' or countryStatus = '3'");
		$result1 = $this->SelectData($fields,$tables, $where, $order=array('countryName ASC'), $group=array(),$limit = "10",0,0);
		$result= $this->FetchAll($result1); 
		return $result;		
	}	
	
	## Get country details by country id
	function getCountryDetailsByCountryId($id) {
		$fields=array();	
		$tables=array($this->country);
		$where=array("countryId='".$id."'");		
		$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 
		$result= $this->FetchRow($result1); 
		return $result;		
	}

	function getCountryNameByCountryId($id) {
		if($id!=''){
		$fields=array('countryName');	
		$tables=array($this->country);
		$where=array("countryId=".$id);		
		$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0);
		$result= $this->FetchRow($result1); 
		return $result['countryName'];
		}
	}
	
	// Globle country name exist or not	
	function checkCountryName($countryname) {
		$fields=array();
		$tables=array($this->country);
		$where=array("countryName='".$countryname."'");
		$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0);
		$result= $this->FetchRow($result1);
		return $result;		
	}
	## Edit Country by countryid
	function editCountryValueById($array, $Id){
		$this->UpdateData($this->country,$array,"countryId",$Id,0);
	}
	
	## Update Country status with multiple countryids
	function updateCountryStatus($countryid, $status) {		
		$sql = "UPDATE ".$this->country." SET countryStatus='".$status."' WHERE countryId = (".$countryid.")";
		$result1= $this->ExecuteQuery($sql);	 
		
	}
	
	## Delete country with multiple countryids
	function deleteCountry($countryids, $status) {
	//	$sql = "DELETE FROM ".$this->country." WHERE countryId IN (".$countryids.")";
		$result1= $this->ExecuteQuery($sql);	 
	}
	
	## Delete Country cat by id
	function deleteCountryById($countryid){
		//$this->DeleteData($this->country,"countryId",$countryid);
	}
	
	## Get country details by countryid
	function getCountryDetailsById($countryid) {
		$fields = array();	
		$tables = array($this->country);
		$result1 = $this->SelectData($fields,$tables, $where= array("countryId='".$countryid."'"), $order = array(), $group=array(),$limit = "",0,0); 
		$result  = $this->FetchRow($result1); 
		return $result;
	}
	
	## Update country status with multiple countryids
	function updateMultipleCountryStatus($countryids, $status) {
		 $sql = "UPDATE ".$this->country." SET countryStatus='".$status."' WHERE countryId IN (".$countryids.")"; 
		$result1= $this->ExecuteQuery($sql);	 
	}
	
	## Delete country category with multiple countryids
	function deleteMultipleCountry($countryids) {
		//$sql = "DELETE FROM ".$this->country." WHERE countryId IN (".$countryids.")";
		$result1= $this->ExecuteQuery($sql);	 
	}
	## List all active country name
	function getAllCountryWithActiveStatus() {
		$fields = array('');	
		$tables = array($this->country);
		$where= array("countryStatus='2'");
		$result1 = $this->SelectData($fields,$tables, $where, $order = array("countryName ASC"), $group=array(),$limit,$offset,0); 
		$result  = $this->FetchAll($result1); 
		return $result;
	}
	
    function getAllBlockCountrys()
     {
         $fields=array();    
        $tables=array($this->country);
        $where = array("countryStatus = '3' ");
        $result1 = $this->SelectData($fields,$tables, $where, $order=array('countryName ASC'), $group=array(), $limit='',$offset='',0);
        $result= $this->FetchAll($result1); 
        return $result;
     }
     
     function isBlockCountry($id)
     {
        $fields=array("countryId");    
        $tables=array($this->country);
        $where = array("countryStatus = '3' and countryId='".$id."'");
        $result1 = $this->SelectData($fields,$tables, $where, $order=array('countryName ASC'), $group=array(), $limit='',$offset='',0);
        $result= $this->FetchRow($result1); 
        return $result;
     }
     
    function isBlockCountryByISOORCountry($id1,$id2,$id3)
     {
	$id2=mysql_real_escape_string($id1);
	$id3=mysql_real_escape_string($id2);
	$id3=mysql_real_escape_string($id3);
	$fields=array("c.countryId");    
	$tables=array($this->country.' as c',$this->states.' as s');
	$where = array(" ( c.countryStatus = '3' and ( c.iso2='".$id1."' or c.countryName='".$id2."') ) or ( s.stateName='".$id3."'  and (s.stateStatus='3' )   )");
	$result1 = $this->SelectData($fields,$tables, $where, $order=array('c.countryName ASC'), $group=array(), $limit='',$offset='',0);
	$result= $this->FetchRow($result1); 
	return $result;
     }  
	
}
?>