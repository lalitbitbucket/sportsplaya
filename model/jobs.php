<?php
## Class Name - Model_Jobs (Contains all the functions related user and user profile)
/**************************************************/
class Model_Jobs extends Database 
{	
	## Constructor
	function Model_Jobs()
	{
        $this->user 		  = USERS;
        $this->userprofile 	  = USERPROFILE;
        $this->userprofile 	  = USERPROFILE;	
        $this->jobs 		= JOBS;	
        $this->club           = CLUB;
        $this->jobs_app     = JOBS_APP;	
        $this->country = COUNTRY;
        $this->sports = SPORTS;

		$this->Database();
    }
    
    
    ## Add Job Data
    function addjobsData($array) {
        $this->InsertData($this->jobs,$array); 
        $insertId = mysql_insert_id(); 
        return $insertId;  
    }

    ## Add Job Data
    function addJobsApplication($array) {
        $this->InsertData($this->jobs_app,$array); 
        $insertId = mysql_insert_id(); 
        return $insertId;  
    }

    ## Get Details All Job
    function getAllJobsData($limit,$offset) {
        $fields=array('j.*,u.userType,u.fname,u.lname,u.userId as userprofileId');	
    	$tables=array($this->jobs." as j ",$this->userprofile." as u ");
    	$where=array("j.userId=u.id");		
    	$result1 = $this->SelectData($fields,$tables, $where, $order = array('id DESC'), $group=array(),$limit,$offset,0); 
    	$result= $this->FetchAll($result1); 
    	return $result;		
    }

    ## Get Single Jobs By JobsId
    function getSingleJobsDataByJobId($jobId) {
        $fields=array('j.*,DATE_FORMAT(j.posted_date,"%d.%m.%Y") as Date_Formatted,u.username,u.fname,u.lname,u.email,u.userId as userprofileId');	
    	$tables=array($this->jobs." as j ",$this->userprofile." as u ");
    	$where=array("j.userId=u.id AND j.id='".$jobId."'");		
    	$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit,0,0); 
    	$result= $this->FetchRow($result1); 
    	return $result;
	}

    ## Get Jobs By SportsId or CountryId
    function getJobsDataBySpIdandCId($sports_id,$selectCountry,$change_sport,$limit,$offset) {
        $fields=array('j.*,u.fname,u.lname,u.userId as userprofileId'); 
        $tables=array($this->jobs." as j ",$this->userprofile." as u ");
        $where=array("j.userId=u.id");    
        if($sports_id > 0){
            $where[] = " sports IN  (".$sports_id.") "; 
        }

        if($selectCountry > 0){
            $where[] = " job_country IN  (".$selectCountry.") ";    
        }

        $result1 = $this->SelectData($fields,$tables, $where, $order = array("id DESC"), $group=array(),$limit,$offset,0); 
        $result= $this->FetchAll($result1); 
        return $result;     
    }

    ## get Jobs count By UserId
    function getJobsCountByUserId($userId){
        $fields=array('count(*) AS cnt'); 
        $tables=array($this->jobs);
        $where=array("userId='".$userId."'");    
        $result1 = $this->SelectData($fields,$tables, $where, $order ='', $group=array(),$limit,$offset,0); 
        $result= $this->FetchRow($result1); 
        return $result;     
    }
    ## get Jobs Data By UserId
    function getJobsDataByUserId($userId){
        $fields=array('*'); 
        $tables=array($this->jobs);
        $where=array("userId='".$userId."'");    
        $result1 = $this->SelectData($fields,$tables, $where, $order ='', $group=array(),$limit,$offset,0); 
        $result= $this->FetchAll($result1); 
        return $result;     
    }

    ## Get Jobs Data Joins To club UserId
    function getJobsDataByClubId($clubId){
        $fields=array('j.*,c.clubName,county.countryName'); 
        $tables=array($this->jobs." AS j ",$this->club." AS c",$this->country." AS county");
        $where=array("j.userId=c.userID AND j.job_country = county.countryId AND c.clubID='".$clubId."'");    
        $result1 = $this->SelectData($fields,$tables, $where, $order ='', $group=array(),$limit,$offset,0); 
        $result= $this->FetchAll($result1); 
        return $result;        
    }

    ## Get Jobs Application By Jobs Id.
    function getJobsApplicationByjobId($jobsId){
        $fields=array('j.*,u.username,u.fname,u.lname,u.avatar,u.sex AS gender,DATE_FORMAT(j.register_datetime,"%b %d %Y %h:%i:%s") as formated_datetime'); 
        $tables=array($this->jobs_app." AS j",$this->userprofile." AS u ");
        $where=array("j.userId= u.id AND j.job_id ='".$jobsId."'");    
        $result1 = $this->SelectData($fields,$tables, $where, $order ='', $group=array(),$limit,$offset,0); 
        $result= $this->FetchAll($result1); 
        return $result;        
    }
    ## get Jobs Data By UserId
    function getJobstitleByjobId($jobId){
        $fields=array('job_title'); 
        $tables=array($this->jobs);
        $where=array("id='".$jobId."'");    
        $result1 = $this->SelectData($fields,$tables, $where, $order ='', $group=array(),$limit,$offset,0); 
        $result= $this->FetchRow($result1); 
        return $result;     
    }
    ## Check if user Already apply this job.
    function checkJobsAppAndUserExist($jobid,$userid){
        $fields=array("*"); 
        $tables=array($this->jobs_app);
        $where=array("job_id='".$jobid."' AND userId='".$userid."'");      
        $result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit="",0,0);
        $result= $this->FetchAll($result1); 
        return $result; 
    }

    ## Get Jobs Application By UserId.
    function getApplicationsByUserId($userId){
        $fields=array('a.*,j.job_title,j.profession,j.job_description,j.job_image,DATE_FORMAT(a.register_datetime,"%b %d %Y %h:%i:%s") as formated_datetime'); 
        $tables=array($this->jobs_app." AS a",$this->jobs." AS j");
        $where=array("a.job_id=j.id AND a.userId ='".$userId."'");    
        $result1 = $this->SelectData($fields,$tables, $where, $order ='', $group=array(),$limit,$offset,0); 
        $result= $this->FetchAll($result1); 
        return $result;        
    }

    /********************************/
    ## Get Users Mathing Jobs
    function getUsersExistsJobsBySportsId($sportsId){
        $startWeek = date('Y-m-d');
        $lastWeek = date('Y-m-d',strtotime("-7 day")); 
        $fields=array(''); 
        $tables=array($this->jobs);
        $where=array("sports='".$sportsId."' AND DATE_FORMAT(posted_date,'%Y-%m-%d') >= '".$lastWeek."' AND DATE_FORMAT(posted_date,'%Y-%m-%d') <= '".$startWeek."'");    
        $result1 = $this->SelectData($fields,$tables,$where,$order='',$group=array(),$limit,$offset,0); 
        $result= $this->FetchAll($result1); 
        return $result;        
    }
    ## Get User Not Apply
    function getUsersExistsJobs($userId,$jobsId){
        $fields=array('*'); 
        $tables=array($this->jobs_app);
        $where=array("job_id ='".$jobsId."' AND userId ='".$userId."'"); 
        $result1 = $this->SelectData($fields,$tables,$where,$order='',$group=array(),$limit,$offset,0); 
        $result= $this->FetchRow($result1); 
        return $result;        
    }
    ## Get Jobs Details by Job Id
    function getJobsDetailsByjobId($jobId){
        $fields=array('*'); 
        $tables=array($this->jobs);
        $where=array("id='".$jobId."'");    
        $result1 = $this->SelectData($fields,$tables, $where, $order ='', $group=array(),$limit,$offset,0); 
        $result= $this->FetchAll($result1); 
        return $result;     
    }
    ## Get All Jobs Details
    function getAllActiveJobsData($limit,$offset) {
        $fields=array('*');  
        $tables=array($this->jobs);
        $where=array("");      
        $result1 = $this->SelectData($fields,$tables, $where, $order = array('job_title ASC'), $group=array(),$limit,$offset,0); 
        $result= $this->FetchAll($result1); 
        return $result;     
    }

    function ifExistsJobcheck($jobId){
        $fields=array('count(*) AS cnt'); 
        $tables=array($this->jobs);
        $where=array("adzuna_job_id='".$jobId."'");    
        $result1 = $this->SelectData($fields,$tables, $where, $order ='', $group=array(),$limit,$offset,0); 
        $result= $this->FetchRow($result1); 
        return $result['cnt'];     
    }

    function insertJobDataByApi($array){
        $this->InsertData($this->jobs,$array); 
        $insertId = mysql_insert_id(); 
        return $insertId;      
    }
}
?>