<?php 
class Model_Contactus extends Database
{
	
	function Model_Contactus()
	{
		$this->enquiries = ENQUIRIES;
		$this->enquiries_reply = ENQUIRY_REPLY;
		$this->users = USERS;
		$this->Database();
	} 
	
	function addContactus( $array )
	{
		$this->InsertData( $this->enquiries , $array );		
		$insertId = mysql_insert_id();
		return $insertId;
	}
	
	function editEnquiry( $array , $id )
	{
		$this->UpdateData( $this->enquiries , $array , "enqID" , $id );		
		
	}	
	function updateEnquiryStatus($ids, $status) {
		$sql = "UPDATE ".$this->enquiries." SET enqStatus='".$status."' WHERE enqID IN (".$ids.")";
		$result1= $this->ExecuteQuery($sql);	 
		
	}
	function updateEnquiry($ids,$action) {
		$sql = "UPDATE ".$this->enquiries." SET enqStatus='".$action."' WHERE enqID='".$ids."'";
		$result1= $this->ExecuteQuery($sql);	 
		
	}
	function deleteEnquirys($Ids)
	{
		$sql = "UPDATE ".$this->enquiries." SET isDelete ='1' WHERE enqID IN (".$Ids.")";
		$result1= $this->ExecuteQuery($sql);	
		
		$sql1 = "DELETE FROM ".$this->enquiries_reply." WHERE enqID IN (".$Ids.")";
		$result1= $this->ExecuteQuery($sql1);	 		
		
	}
	function deleteEnquiry($id) {
		$sql = "UPDATE ".$this->enquiries." SET isDelete ='1' WHERE enqID='".$id."'";
		$result1= $this->ExecuteQuery($sql);	 
		
		$sql1 = "DELETE FROM ".$this->enquiries_reply." WHERE enqID='".$id."'";
		$result1= $this->ExecuteQuery($sql1);	 	
	}
	function listRecord($searchtxt,$orderField = 'enqName',$orderBy = 'ASC',$limit='', $offset="")
	{
		$fields = array();	
		$tables = array( $this->enquiries);
		$where=array("isDelete='0'");
		if($searchtxt!='' && $searchtxt!='Search'){
		  $where[] = (" enqName LIKE '%".$searchtxt."%' OR enqEmail  LIKE '%".$searchtxt."%'");	
		}
		$order = array("$orderField $orderBy");
		$result1 = $this->SelectData( $fields,$tables, $where, $order, $group=array(),$limit,$offset,0);
		$result= $this->FetchAll( $result1 );
		return $result;
	}
	function getEnquiry($id)
	{
		$fields=array();
		$tables=array($this->enquiries);
		$where=array("enqID='".$id."'");
		$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0);
		$result= $this->FetchRow($result1);
		return $result;		
	}
	
	function addEnquiryReply($array)
	{
		$this->InsertData( $this->enquiries_reply , $array );		
		$insertId = mysql_insert_id();
		return $insertId;
	}
	
	function AllReplyMessagesById($id,$search,$limit='',$offset='')
	{	
		$fields = array();	
		$tables = array($this->enquiries_reply);
		$clause="enqID='".$id."'";
		if($search !="" && $search !="Search")
		{
			$clause.= " AND replySub like '%".$search."%'";
		}
		$where=array($clause);		
		
		$result1 = $this->SelectData( $fields,$tables, $where, $order=array(), $group=array(),$limit,$offset,0);
		$result= $this->FetchAll( $result1 );
		return $result;
	}
	function deleteReplies($ids) {
		$sql = "DELETE FROM ".$this->enquiries_reply." WHERE replyID IN (".$ids.")";
		$result1= $this->ExecuteQuery($sql);	 
	}
	function deleteReplyMessages($id)
	{
	echo 	$this->DeleteData($this->enquiries_reply,"replyID",$id);
	}
	
	function getReplydetails($id)
	{
		$fields = array();	
		$tables = array( $this->enquiries_reply );
		$result1 = $this->SelectData( $fields,$tables, $where = array("replyID='".$id."'"), $order ="", $group=array(),$limit = "",0,0);
		$result= $this->FetchRow($result1);
		return $result;
	}
}

?>