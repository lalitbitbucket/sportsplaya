<?php
class Model_Group extends Database 
{	
	## Constructor
	function Model_Group() {
		$this->group = GROUP;
		$this->grouplikes = GROUPLIKES;
		$this->groupmembers = GROUPMEMBERS;
		$this->groupactivity = GROUPACTIVITY;
		$this->groupactivitylike = GROUPACTIVITYLIKE;
		$this->groupactivitycomment = GROUPACTIVITYCOMMENT;
		$this->user = USERS;
		$this->userprofile = USERPROFILE;	
		$this->Database();
	}
	
	## Add groups in database
	function addGroupByValue($Array) {
	$this->InsertData( $this->group , $Array );		
	$insertId = mysql_insert_id();
	return $insertId;
	}

	## Add groups in database
	function addGroupMembersByValue($Array) {
	$this->InsertData( $this->groupmembers , $Array );		
	$insertId = mysql_insert_id();
	return $insertId;
	}	
	
	## Edit groups by trainingID
	function editGroupValueById($array, $Id){
	$this->UpdateData($this->group,$array,"id",$Id,0);
	}
	
	## Update groups status by trainingID 
	function updateGroupStatus($groupID, $status) {		
	$sql = "UPDATE ".$this->group." SET status='".$status."' WHERE id = '".$groupID."'";
	$result1= $this->ExecuteQuery($sql);	 
	}
	
	## Delete groups by trainingID
	function deleteGroupValueById($groupID){
	$this->DeleteData($this->group,"id",$groupID);
	}

	## Delete Training by id
	function deleteGroup($groupID) {
	$sql = "UPDATE ".$this->group." SET status='0' WHERE id = '".$groupID."'";
	$result1= $this->ExecuteQuery($sql);
	}
	
	## Update groups status with multiple trainingIDs
	function updateMultipleGroupStatus($groupID, $status) {
	$sql = "UPDATE ".$this->group." SET status='".$status."' WHERE id IN (".$groupID.")";
	$result1= $this->ExecuteQuery($sql);	 
	}

	## Delete training with multiple trainingIDs
	function deleteMultipleGroup($groupID) {
	$sql = "DELETE FROM ".$this->group." WHERE id IN (".$groupID.")";
	$result1= $this->ExecuteQuery($sql);	 
	}
	
	## Get groups details by group id
	function getGroupDetailsById($groupID) {
	$fields=array();	
	$tables=array($this->group);
	$where=array("id='".$groupID."'");		
	$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 
	$result= $this->FetchRow($result1); 
	return $result;		
	}

	function getGroupAndGroupUserDetailsByGroupId($groupID,$limit='',$offset='') {
	$fields=array("gp.*,u.id as userprofileId,u.fname,u.lname ,u.avatar");	
	$tables=array($this->group." as gp",$this->userprofile." as u");
	$where = array("gp.id = '".$groupID."' AND gp.userId=u.id");
	$order = array();
	$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(), $limit='', $offset=0,0);
	$result= $this->FetchRow($result1); 
	return $result;		
	}
	
	## Get all groups
	function getAllGroup($search='', $orderField = 'groupname', $orderBy = 'ASC', $limit='',$offset='') {
	$fields=array();	
	$tables=array($this->group);
	$where = array("status!='0'");
	if($search != '' && $search != 'Search') {
	$where = array("groupname LIKE '%".$search."%' ");
	} 
	$order = array("$orderField $orderBy");
	$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(), $limit,$offset,0);
	$result= $this->FetchAll($result1); 
	return $result;		
	}
	
	## Get groups image by trainingID
	function getGroupImageByGroupId($groupID) {
	$fields=array("groupID,image");	
	$tables=array($this->group);
	$where=array("groupID='".$groupID."'");		
	$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 
	$result= $this->FetchRow($result1); 
	return $result;		
	}

	## Checking wheather user enetered groups name is already exists or not 
	function checkGroupNameExists($name,$groupID){
	$fields = array();
	$tables = array($this->group);
	if($groupID) {
	$where  = array("LOWER(groupname) = '".$name."' AND id != '".$groupID."' AND status!='0' ");
	} else {
	$where  = array("LOWER(groupname) = '".$name."' AND status!='0'");
	}
	$result = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 
	$result1 = $this->FetchRow($result);
	return $result1;
	}
	
	## Get all active groups for user side
	function getAllActiveTrainingUserSide($catID='', $orderField = 't.name', $orderBy = 'ASC', $limit='',$offset=0) {
	$fields=array("t.*, c.catName, c.catID");	
	$tables=array($this->category." as c",$this->group." as t");
	$where = array("t.status='2' AND c.catID=t.catID");
	if($catID != '') {
	$where[] = ("t.catID = '".$catID."'");
	} 
	$order = array("$orderField $orderBy");
	$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(""), $limit,$offset,0);
	$result= $this->FetchAll($result1); 
	return $result;		
	}
	
	## Get all random groups by category id
	function getAllRandomTrainingByCatId($catID='', $limit='',$offset=0) 
	{
	$fields=array();	
	$tables=array($this->group);
	$where = array("status='2' AND catID = '".$catID."'");
	$order = array("RAND()");
	$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(), $limit,$offset,0);
	$result= $this->FetchAll($result1); 
	return $result;		
	}

	## Get all random groups by category id
	function getOtherTraining() {
	$fields=array();	
	$tables=array($this->group);
	$where = array("status='2' AND  catID !='1' AND catID !='2'");
	$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(), $limit,$offset,0);
	$result= $this->FetchAll($result1); 
	return $result;		
	}
	
	## Get Training
	function getRecentTraining($search, $orderBy = 'DESC', $limit='',$offset='') {
	$fields=array();	
	$tables=array($this->group);
	$order = array("trainingID ".$orderBy);
	$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(), $limit="",$offset,0);
	$result= $this->FetchAll($result1); 
	return $result;		
	}

	## Get last five added groups
	function lastAddedFiveGroups() {
	$fields=array();	
	$tables=array($this->group);
	$where = array("status!='0'");
	$order = array("date DESC");
	$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(), $limit="5", $offset="0",0);
	$result= $this->FetchAll($result1); 
	return $result;		
	}
	
	## Add users group in database
	function addUsersGroupByValue($Array) {
	$this->InsertData( $this->usersgroup , $Array );		
	$insertId = mysql_insert_id();
	return $insertId;
	}	
	
	## Edit users group by users group id
	function editUsersGroupValueById($array, $Id){
	$this->UpdateData($this->usersgroup,$array,"userGrpID",$Id,0);
	}
	
	## Update users group status by users group id 
	function updateUserGroupStatus($userGrpID, $status) {		
	$sql = "UPDATE ".$this->usersgroup." SET status='".$status."' WHERE userGrpID = '".$userGrpID."'";
	$result1= $this->ExecuteQuery($sql);	 	
	}
	
	## Update users group status by users group id 
	function deleteUserGroupStatusByGroupIdAndUserID($groupID, $uID) {		
	$sql = "UPDATE ".$this->usersgroup." SET status='0' WHERE groupID = '".$groupID."' AND uID = '".$uID."' ";
	$result1= $this->ExecuteQuery($sql);	 	
	}
	
	## Get all group by user id
	function getAllGroupsByUserId($uID, $limit='', $offset='0') {
	$fields=array();	
	$tables=array($this->group);
	$where = array("userId ='".$uID."' AND status!='0'");
	$order = array();
	$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(), $limit, $offset,0);
	$result= $this->FetchAll($result1); 
	return $result;		
	}
	
	function getGroupCountByUserId($uID, $limit='', $offset='0') {
	$fields=array("count(*) as cnt");	
	$tables=array($this->group);
	$where = array("userId = '".$uID."' AND status!='0'");
	$order = array();
	$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(), $limit, $offset,0);
	$result= $this->FetchRow($result1); 
	return $result['cnt'];		
	}
		
	## Get group member count by group id
	function getGroupMemberCountByGroupId($groupID) {
	$fields=array("count(*) as cnt");	
	$tables=array($this->groupmembers);
	$where = array("groupId = '".$groupID."' AND status!='0'");
	$order = array();
	$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(), $limit='', $offset=0,0);
	$result= $this->FetchRow($result1); 
	return $result['cnt'];		
	}
	
	## Get all likes count by group id
	function getGroupLikeCountByGroupId($groupID) {
	$fields=array("count(*) as cnt");	
	$tables=array($this->grouplikes);
	$where = array("groupID = '".$groupID."'");
	$order = array();
	$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(), $limit='', $offset=0,0);
	$result= $this->FetchRow($result1); 
	return $result['cnt'];		
	}
	
	function getGroupCommentCount($groupID)
	{
	$fields=array("count(*) as cnt");	
	$tables=array($this->groupactivity);
	$where = array("groupID = '".$groupID."'");
	$order = array();
	$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(), $limit='', $offset=0,0);
	$result= $this->FetchRow($result1); 
	return $result['cnt'];		
	}

	function getGroupLikesByGroupId($groupID) {
	$fields=array("");	
	$tables=array($this->grouplikes);
	$where = array("groupID = '".$groupID."'");
	$order = array();
	$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(), $limit='', $offset=0,0);
	$result= $this->FetchAll($result1); 
	return $result;		
	}

	function getGroupCommentByGroupId($groupID) {
	$fields=array("");	
	$tables=array($this->groupactivity);
	$where = array("groupID = '".$groupID."'");
	$order = array();
	$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(), $limit='', $offset=0,0);
	$result= $this->FetchAll($result1); 
	return $result;		
	}

	function deleteGroupLikeByID($id){
	$this->DeleteData($this->grouplikes,"id",$id);
	}

	function deletegroupCommentByID($id){
	$this->DeleteData($this->groupactivity,"id",$id);
	}
	
	## Get all likes count by group id and user id
	function getGroupLikeCountByGroupIdAndUserID($groupID, $uID) {
	$fields=array("count(*) as cnt");	
	$tables=array($this->grouplikes);
	$where = array("groupID = '".$groupID."' AND uID = '".$uID."' AND status!='0'");
	$order = array();
	$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(), $limit='', $offset=0,0);
	$result= $this->FetchRow($result1); 
	return $result['cnt'];		
	}
	
	## Add groups likes in database
	function addGroupLikeByValue($Array) {
	$this->InsertData( $this->grouplikes , $Array );		
	$insertId = mysql_insert_id();
	return $insertId;
	}
	
	## get all groups except joined group
	function getAllOtherGroups($groupIDs,$userId) {
	$sql = "SELECT * FROM ".$this->group." WHERE id NOT IN(".$groupIDs.") AND status='2' AND userId!='".$userId."' LIMIT 0,3";
	$result1= $this->ExecuteQuery($sql);
	$result= $this->FetchAll($result1); 
	return $result;	
	}

	function getMyTotalSuggestedGroups($groupIDs,$userId) {
	$sql = "SELECT * FROM ".$this->group." WHERE id NOT IN(".$groupIDs.") AND status='2' AND userId!='".$userId."'"; 
	$result1= $this->ExecuteQuery($sql);
	$result= $this->FetchAll($result1); 
	return $result;	
	}
	
	########################## Group Activity #####################################
	## Delete group comment bt id
	function deleteGroupCommById($id){
	$this->DeleteData($this->groupactivity,"id",$id);
	}

	## check group comments by group id
	function checkGroupCommentsByGroupId($groupID) {
	$fields=array("count(*) as cnt");	
	$tables=array($this->groupactivity);
	$where = array("groupId = '".$groupID."' AND status!='0'");
	$order = array();
	$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(), $limit='', $offset=0,0); 
	$result= $this->FetchRow($result1); 
	return $result['cnt'];		
	}
	
	## Add group comment in database
	function addGroupActivityByValue($Array) {
	$this->InsertData( $this->groupactivity , $Array );		
	$insertId = mysql_insert_id();
	return $insertId;
	}

	## Delete group activity comment
	function deleteGroupActivity($id){
	$this->DeleteData($this->groupactivity,"id",$id);
	}

	//Function to get last comment by id
	function getLastGroupCommentAddedByUser($groupCommId) {
	$fields = array("activity.*,u.fname,u.lname,u.userId as uID,u.birthdate,u.userType,u.avatar,u.sex,u.username,u.email");    
	$tables = array("$this->groupactivity as activity, $this->userprofile as u");
	$where  = array("activity.id= '".$groupCommId."' AND activity.comment_by = u.id");		
	$sql    = $this->SelectData($fields,$tables, $where, $order, $group=array(),$limit='',0,0); 
	$result = $this->FetchRow($sql); 
	return $result;
	}

	//Function to get comments on group
	function getGroupCommentAddedByUser($activityId) 
	{
	$fields = array("activity.*,u.fname,u.lname,u.userId as uID,u.birthdate,u.userType,u.avatar,u.sex,u.username,u.email");    
	$tables = array("$this->groupactivity as activity, $this->userprofile as u");
	$where  = array("activity.groupId = '".$activityId."' AND activity.comment_by = u.id");		
	$sql    = $this->SelectData($fields,$tables, $where, $order, $group=array("activity.id DESC"),$limit='',0,0); 
	$result = $this->FetchAll($sql); 
	return $result;
	}	
	
	## Get group activity by group id
	function getGroupActivityByGroupId($groupID) {
	$fields=array("");	
	$tables=array($this->groupactivity);
	$where = array("groupID = '".$groupID."'");
	$order = array("activityDate DESC");
	$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(), $limit='', $offset=0,0);
	$result= $this->FetchAll($result1); 
	return $result;		
	}
	
	## Get group activity by activity id
	function getGroupActivityByActivityId($activityID) {
	$fields=array("");	
	$tables=array($this->groupactivity);
	$where = array("activityID = '".$activityID."'");
	$order = array();
	$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(), $limit='', $offset=0,0);
	$result= $this->FetchRow($result1); 
	return $result;		
	}
	
	## Add groups activity like in database
	function addGroupActivityLikesByValue($Array) {
	$this->InsertData( $this->groupactivitylike , $Array );		
	$insertId = mysql_insert_id();
	return $insertId;
	}
	
	## Get activity likes count by group id and activity id
	function getGroupActivityLikeCountByGroupIdAndActivityID($groupID, $activityID) {
	$fields=array("count(*) as cnt");	
	$tables=array($this->groupactivitylike);
	$where = array("groupID = '".$groupID."' AND activityID = '".$activityID."'");
	$order = array();
	$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(), $limit='', $offset=0,0);
	$result= $this->FetchRow($result1); 
	return $result['cnt'];		
	}
	
	## Get activity likes count by group id, activity id and user id
	function getGroupActivityLikeCountByGroupIdAndActivityIdAndUserId($groupID, $activityID, $uID) {
	$fields=array("count(*) as cnt");	
	$tables=array($this->groupactivitylike);
	$where = array("groupID = '".$groupID."' AND activityID = '".$activityID."' AND likedBy = '".$uID."'");
	$order = array();
	$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(), $limit='', $offset=0,0);
	$result= $this->FetchRow($result1); 
	return $result['cnt'];		
	}
	
	## Add groups activity comment in database
	function addGroupActivityCommentByValue($Array) {
	$this->InsertData( $this->groupactivitycomment , $Array );		
	$insertId = mysql_insert_id();
	return $insertId;
	}
	
	## Get activity comments count by group id and activity id
	function getGroupActivityCommentsCountByGroupIdAndActivityID($groupID, $activityID) {
	$fields=array("count(*) as cnt");	
	$tables=array($this->groupactivitycomment);
	$where = array("groupID = '".$groupID."' AND activityID = '".$activityID."'");
	$order = array();
	$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(), $limit='', $offset=0,0);
	$result= $this->FetchRow($result1); 
	return $result['cnt'];		
	}
	
	## Get all activity comments by group id and activity id
	function getGroupActivityCommentsByGroupIdAndActivityID($groupID, $activityID) {
	$fields=array("gac.*,u.uID,u.fName,u.lName,u.avatar,u.gender");	
	$tables=array($this->groupactivitycomment." as gac",$this->user." as u");
	$where = array("gac.groupID = '".$groupID."' AND gac.activityID = '".$activityID."' AND gac.commentBy=u.uID");
	$order = array();
	$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(), $limit='', $offset=0,0);
	$result= $this->FetchAll($result1); 
	return $result;		
	}
	
	## Get user's own group by user id
	function getMyGroupsByUserId($uID,$search='', $orderField = 'name', $orderBy = 'ASC', $limit='',$offset='') {
	$fields=array();	
	$tables=array($this->group);
	$where = array("moderatorID = '".$uID."' AND status='2'");
	$order = array();
	$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(), $limit,$offset,0);
	$result= $this->FetchAll($result1); 
	return $result;		
	}

	## Join group members
	function getAllGroupMemberByGroupId($gID,$limit='',$offset='0') {
	$fields=array();	
	$tables=array($this->groupmembers);
	$where = array("groupID = '".$gID."' AND status='1'");
	$order = array();
	$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(), $limit, $offset,0);
	$result= $this->FetchAll($result1); 
	return $result;		
	}

	## Pending group members
	function getAllPendingGroupMembersByGroupId($gID,$limit='',$offset='0') {
	$fields=array();	
	$tables=array($this->groupmembers);
	$where = array("groupID = '".$gID."' AND status='2'");
	$order = array();
	$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(), $limit, $offset,0);
	$result= $this->FetchAll($result1); 
	return $result;		
	}

	function getGroupMemberDetailsByGroupId($gID,$userId,$limit='', $offset='0') {
	$fields=array();	
	$tables=array($this->groupmembers);
	$where = array("groupID = '".$gID."' AND userID !='".$userId."'");
	$order = array();
	$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(), $limit, $offset,0);
	$result= $this->FetchRow($result1); 
	return $result;		
	}

	## check group member exists by group id and user id
	function checkGroupMemberExistsByGroupIdAndUserId($groupID, $uID) {
	$fields=array("count(*) as cnt");	
	$tables=array($this->groupmembers);
	$where = array("groupId = '".$groupID."' AND memberId = '".$uID."' AND status!='0' AND status!='2'");
	$order = array();
	$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(), $limit='', $offset=0,0);
	$result= $this->FetchRow($result1); 
	return $result['cnt'];		
	}

	/********** Get all his group list ***********/
	function getAllMemberGroups($uID) {
	$fields=array("gm.*,gp.*");
	$tables=array($this->groupmembers." as gm",$this->group." as gp");	
	$where = array("gm.memberId = '".$uID."' AND gm.groupId=gp.id AND gp.status!='0'");
	$order = array();
	$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(), $limit='', $offset=0,0); 
	$result= $this->FetchAll($result1); 
	return $result;		
	}

	## Get total groups whose status not read
	function getTotalUnreadgroupsById($uID) {
	$fields=array("gm.*,gp.*");
	$tables=array($this->groupmembers." as gm",$this->group." as gp");	
	$where = array("gm.memberId = '".$uID."' AND gm.groupId=gp.id AND gm.status!='0' AND gp.readstatus='n'");
	$order = array();
	$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(), $limit='', $offset=0,0); 
	$result= $this->FetchAll($result1); 
	return $result;		
	}

	## Get all suggested groups
	function getAllSuggestedGroups($userID,$searchbyCharacter='') {
	$fields=array();	
	$tables=array($this->group);
	$where = array("(userId!=".$userID.") AND status = '2'");
	if($searchbyCharacter != '') { 
		$where = array("groupname LIKE  '".$searchbyCharacter."%'");
	}
	$result1 = $this->SelectData($fields,$tables, $where, $order=array("groupname ASC"), $group=array(), $limit='',$offset=0,0); 
	$result= $this->FetchAll($result1); 
	return $result;		
	}

	## Get three random groups
	function getRandomActiveGroups($userID='',$limit='3', $offset='0') {
	$fields=array();	
	$tables=array($this->group);
	$where = array("(status='2')");
	if($userID!=''){
	$where[] = "(userId!='".$userID."')";
	}
	$order = array("RAND()");
	$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(), $limit, $offset,0);
	$result= $this->FetchAll($result1); 
	return $result;		
	}

	## Update users group status by users group id [Join Group]
	function updateUserGroupStatusByGroupIdAndMemberID($groupID, $memberId) {		
	 $sql = "UPDATE ".$this->groupmembers." SET status='1' WHERE groupId = '".$groupID."' AND memberId = '".$memberId."' "; 
	 $result1= $this->ExecuteQuery($sql);	 	
	}

	## Update users group status by users group id [Unjoin Group]
	function deleteUserGroupStatusByGroupIdAndMemberID($groupID, $memberId) {		
	$sql = "UPDATE ".$this->groupmembers." SET status='0' WHERE groupId = '".$groupID."' AND memberId = '".$memberId."' ";
	$result1= $this->ExecuteQuery($sql);	 	
	}

	/***********************************************************************************/
	## Get event group likes for gold,silver & count
	function getAllGoldlikecount($groupId) {
	$fields=array("count(*) as cnt");	
	$tables=array($this->grouplikes);
	$where=array("groupId='".$groupId."' AND likeType='1'");				
	$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 
	$result= $this->FetchRow($result1); 
	return $result['cnt'];		
	}

	function getAllSilverlikecount($groupId) {
	$fields=array("count(*) as cnt");	
	$tables=array($this->grouplikes);
	$where=array("groupId='".$groupId."' AND likeType='2'");				
	$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 
	$result= $this->FetchRow($result1); 
	return $result['cnt'];		
	}

	function getAllBronzelikecount($groupId) {
	$fields=array("count(*) as cnt");	
	$tables=array($this->grouplikes);
	$where=array("groupId='".$groupId."' AND likeType='3'");				
	$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 
	$result= $this->FetchRow($result1); 
	return $result['cnt'];		
	}

	## Get gold likes
	function getGoldLikes($groupId,$userProfileId,$commType)
	{
	$fields=array();	
	$tables=array($this->grouplikes);
	$where=array("groupId='".$groupId."' AND likeType ='".$commType."' AND userProfileId ='".$userProfileId."'");		
	$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 
	$result= $this->FetchRow($result1); 
	return $result;	
	}

	// for delete gold like by id
	function deleteGoldLike($Id) {
	$this->DeleteData($this->grouplikes,"id",$Id);
	}

	// Add gold likes
	function addToGoldLikeList($array)
	{
	    $this->InsertData($this->grouplikes, $array);
	    return mysql_insert_id();
	}
	/*********************************************************/
	## Get silver likes
	function getSilverLikes($groupId,$userProfileId,$commType)
	{
	$fields=array();	
	$tables=array($this->grouplikes);
	$where=array("groupId='".$groupId."' AND likeType ='".$commType."' AND userProfileId ='".$userProfileId."'");		
	$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 
	$result= $this->FetchRow($result1); 
	return $result;	
	}

	// for delete silver like by id
	function deleteSilverLike($Id) {
	$this->DeleteData($this->grouplikes,"id",$Id);
	}

	// Add silver likes
	function addToSilverLikeList($array)
	{
	 $this->InsertData($this->grouplikes, $array);
	 return mysql_insert_id();
	}
	/**************************************************************/
	## Get bronze likes
	function getBronzeLikes($groupId,$userProfileId,$commType)
	{
	$fields=array();	
	$tables=array($this->grouplikes);
	$where=array("groupId='".$groupId."' AND likeType ='".$commType."' AND userProfileId ='".$userProfileId."'");		
	$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 
	$result= $this->FetchRow($result1); 
	return $result;	
	}

	// for delete bronze like by id
	function deleteBronzeLike($Id) {
	 $this->DeleteData($this->grouplikes,"id",$Id);
	}

	// Add bronze likes
	function addToBronzeLikeList($array)
	{
	 $this->InsertData($this->grouplikes, $array);
	 return mysql_insert_id();
	}

	/*******************************************/
	# update multiple read status 
	function updateReadStatusByGroupId($Ids,$action){
	 $sql = "UPDATE ".$this->group." SET readstatus='".$action."' WHERE id IN (".$Ids.")"; 
	 $result1= $this->ExecuteQuery($sql);	 
	}

	function getLikeCountBygroupIDandLikeType($groupId,$likeType) {	
  	$fields=array("count(*) as cnt");	
  	$tables=array($this->grouplikes);
  	$where = array("groupId='".$groupId."' AND  likeType ='".$likeType."'");
  	$order = array("");
  	$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(), $limit='', $offset='0',0);
  	$result= $this->FetchRow($result1); 
  	return $result['cnt'];		
	}

	/*******************************************/
	## Check user like or not on activity
	function checkUserLikeOnActvity($groupId,$userProfileId)
	{
	$fields=array();	
	$tables=array($this->grouplikes);
	$where=array("groupId='".$groupId."' AND userProfileId ='".$userProfileId."'");		
	$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 
	$result= $this->FetchRow($result1); 
	return $result;	
	}

	// For delete likes by groupId & user profile Id
	function deleteLikesByGroupUserProfileId($groupId,$userProfileId){
	$sql = "delete from ".$this->messagelikes."  WHERE groupId='".$groupId."' AND  userProfileId ='".$userProfileId."'";
	$result1= $this->ExecuteQuery($sql);	 
	}	
}
?>
