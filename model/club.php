<?php
## Class Name - Model_Jobs (Contains all the functions related user and user profile)
/**************************************************/
class Model_Club extends Database 
{	
	## Constructor
	function Model_Club()
	{
        $this->user 		  = USERS;
        $this->userprofile 	  = USERPROFILE;
        $this->club           = CLUB;
        $this->Database();
    }
    
    
    ## Get All Active Clubs
    function getAllActiveClubs() {
        $fields=array("*");    
        $tables=array($this->club);
        $where = array();
        $result1 = $this->SelectData($fields,$tables, $where, $order=array('clubName ASC'), $group=array(), $limit='',$offset='',0);
        $result= $this->FetchAll($result1); 
        return $result;     
    }

    ## Get Club Details By ClubId
    function getClubDetailsByClubId($id) {
        $fields=array("*");    
        $tables=array($this->club);
        $where=array("clubID='".$id."'");        
        $result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 
        $result= $this->FetchRow($result1); 
        return $result;     
    }

    ## Get Club Name By ClubId
    function getClubNameByClubId($id) {
        $fields=array('clubName');   
        $tables=array($this->club);
        $where=array("clubID='".$id."'");     
        $result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 
        $result= $this->FetchRow($result1); 
        return $result['clubName'];
    }
    ## Get Club Details By userId
    function getClubDetailsByUserId($userId) {
        $fields=array('*');   
        $tables=array($this->club);
        $where=array("userID=".$userId);     
        $result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 
        $result= $this->FetchRow($result1); 
        return $result;
    }
}
?>

