<?php  
/**************************************************/
## Class Name - Model_ModuleUser (Contains all the functions related admin and sub-admon permissions)
## Created By - Techmodi (19/08/2011)
/**************************************************/

class Model_ModuleUser extends Database
{
	## Constructor
	function Model_ModuleUser() {
		$this->moduleuser = MODULEUSER;
		$this->modules= MODULES;
		$this->submodules=ADMINSUBMODULE;
		
		$this->Database();
	}	
	
	function getStatusValueByUserIdAndModuleId($userId, $moduleId) {
		$fields=array('status');
		$tables=array($this->moduleuser);
		$where=array("userId='".$userId."'", "moduleId='".$moduleId."'");
		$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0);
		$result= $this->FetchRow($result1);
		return $result;
	}
	function getModuleHelpText($moduleId) {
		$fields=array();
		$tables=array($this->modules);
		$where=array("moduleId='".$moduleId."'");
		$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0);
		$result= $this->FetchRow($result1);
		return $result;
	}
	function editModuleHelpTextById($array, $moduleId)	{
		$this->UpdateData($this->modules,$array,"moduleId",$moduleId,0);
	}
	
	function getModuleUserValueByUserIdWithActiveStatus($userId) {
		$fields=array();
		$tables=array($this->moduleuser);
		$where=array("userId='".$userId."'");
		$result1 = $this->SelectData($fields,$tables, $where, $order = array('id'), $group=array(),$limit = "",0,0);
		$result= $this->FetchAll($result1);
		return $result;
	}
	
	function addModuleUserValue($array)	{
		$this->InsertData($this->moduleuser,$array);
	}
		
	function editModuleUserValueById($array, $moduleUserId)	{
		$this->UpdateData($this->moduleuser,$array,"id",$moduleUserId,0);
	}
	
	function deleteModuleUserValueById($moduleUserId) {
		$this->DeleteData($this->moduleuser,"id",$moduleUserId);
	}
	
	function deleteModuleUserValueByUserId($userId)	{
		$sql= "DELETE FROM ".$this->moduleuser." WHERE userId=".$userId;
		$res= $this->ExecuteQuery($sql);
	}
	
	function getAllMasterModules() {
		$fields=array();
		$tables=array($this->modules);
		$where=array();
		$result1 = $this->SelectData($fields,$tables, $where, $order = array("moduleId ASC"), $group=array(),$limit = "",0,0);
		$result= $this->FetchAll($result1);
		return $result;
	}
	
	## Update faq status with multiple ids
	function updateModulesStatusIn($ids) {
		$sql = "UPDATE ".$this->modules." SET modStatus='1' WHERE moduleId IN (".$ids.")";
		$result1= $this->ExecuteQuery($sql);	 
		
	}
	
	## Update faq status with multiple ids
	function updateModulesStatusNotIn($ids) {
		$sql = "UPDATE ".$this->modules." SET modStatus='0' WHERE moduleId NOT IN (".$ids.")";
		$result1= $this->ExecuteQuery($sql);	 
		
	}
	
	function getAllMasterActiveModules() {
		$fields=array();
		$tables=array($this->modules);
		$where=array("modStatus='1'");
		$result1 = $this->SelectData($fields,$tables, $where, $order = array('moduleId ASC'), $group=array(),$limit = "",0,0);
		$result= $this->FetchAll($result1);
		return $result;
	}
	
	function getModulePermissionStatusByUserId($userId, $moduleId)
	{
		$fields=array('status');
		$tables=array($this->moduleuser);
		$where=array("userId='".$userId."'", "moduleId='".$moduleId."'");
		$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0);
		$result= $this->FetchRow($result1);
		return $result;
	}
	
	function getAllAdminSubModules($id)
	{
		$fields=array("");	
		$tables=array($this->submodules);
		$where=array("moduleId='".$id."'");
		$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 
		$result= $this->FetchAll($result1); 
		return $result;	
	}
	
	function getAllActiveSubModulesForSubAdmin() {
		$fields=array();
		$tables=array($this->submodules);
		$where=array("1");
		$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0);
		$result= $this->FetchAll($result1);
		return $result;
	}
	
	function getAdminAssignedSubModules($admin_id,$submodule_id)
	{
		$fields=array("");	
		$tables=array($this->moduleuser);
		$where=array("userId ='".$admin_id."' and submoduleId='".$submodule_id."'");
		$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 
		$result= $this->FetchRow($result1); 
		return $result;	
	}
	
	function getAdminAssignedMainModules($admin_id,$module_id)
	{
		$fields=array("");	
		$tables=array($this->moduleuser);
		$where=array("userId='".$admin_id."' and moduleId='".$module_id."'");
		$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 
		$result= $this->FetchAll($result1); 
		return $result;	
	}
	
	function getParentId($id)
	{
		$fields=array("");	
		$tables=array($this->submodules);
		$where=array("subModID='".$id."'");
		$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 
		$result= $this->FetchRow($result1); 
		return $result;	
	}
	
	function addPrevilagesForAdminUser($array) {
		$this->InsertData($this->moduleuser,$array);
		$insertId = mysql_insert_id();
		return $insertId;
	}
	
	function getSubmoduleBYId($id)
	{
		$fields=array("");	
		$tables=array($this->submodules);
		$where=array("moduleId='".$id."'");
		$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,1); 
		$result= $this->FetchRow($result1); 
		return $result;	
	}
	
	function getAllActiveMainModulesForSubAdmin() {
		
		$fields=array("moduleId");
		$tables=array($this->modules);
		$str="moduleId NOT IN (SELECT moduleId  From ".$this->submodules.")";
		$where=array($str);
		$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0);
		$result= $this->FetchAll($result1);
		return $result;		
		
	}
}
?>