<?php
class Model_Posts extends Database 
{	
	## Constructor
	function Model_Posts() {
	$this->userprofile = USERPROFILE;
	$this->posts = POSTS;
	$this->subposts = SUBPOSTS;
	$this->userfriends = USERFRIENDS;
	$this->messagelikes = MESSAGELIKES;
	$this->useractivity = USERACTIVITY;
	$this->Database();
	}	
	## Get seven days Tag Activities
	function getSevenDaysTagsActivities($userProId,$limit='',$offset=0) {
	$fields = array("activity.*,u.fname,u.lname,u.userId as uID,u.birthdate,u.userType,u.avatar,u.sex,u.username,u.email,DATE_FORMAT(activity.date,'%D %b') AS modify_date");    
	$tables = array("$this->posts as activity, $this->userprofile as u");
	$where  = array("activity.userId = '".$userProId."' AND activity.userId=u.id AND activity.status='2' AND activity.date between NOW() - INTERVAL 7 DAY AND NOW()");	
	
	$sql    = $this->SelectData($fields,$tables, $where, $order=array("activity.id DESC"), $group=array(),$limit,$offset,0); 
	$result = $this->FetchAll($sql); 
	return $result;
	}

	## Add posts in database
	function addPostsByValue($Array) {
	$this->InsertData( $this->posts , $Array );		
	$insertId = mysql_insert_id();
	return $insertId;
	}

	## Add user Activity in database
	function addUserActivity($Array) {
	$this->InsertData( $this->useractivity , $Array );
	$insertId = mysql_insert_id();
	return $insertId;
	}

	## Add posts in database
	function addSubPostsByValue($Array) {
	$this->InsertData( $this->subposts , $Array );		
	$insertId = mysql_insert_id();
	return $insertId;
	}

	## Edit post by Id
	function editPostById($array,$Id){
	$this->UpdateData($this->posts,$array,"id",$Id,0);
	}
	
	## Check Post already exist
	function CheckPostAlreadyExists($userid, $actionid, $actiontype) {
	$fields = array();
	$tables = array($this->posts);
	$where	=	array("user_id = '".$userid."' AND post_action_id = '".$actionid."'AND postType ='".$actiontype."'");
	$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),1,0,0); 
	$result  = $this->FetchRow($result1); 
	return $result;
	}

	## Get recent post added by user
	function getrecentPostAddedByTagIds($friendIds,$lastPostID) {
	$fields = array("c.*,u.fname,u.lname,u.userId as uID,u.birthdate,u.userType,u.avatar,u.sex,u.username,u.email");    
	$tables = array($this->posts." as c",$this->userprofile." as u");
	$result1 = $this->SelectData($fields,$tables, $where= array("c.userId IN(".$friendIds.") AND c.userId=u.id AND c.userType=u.userType AND c.id >".$lastPostID), $order = array(),$group=array(),$limit = "",0,0);
	$result  = $this->FetchRow($result1); 
	return $result;
	}

	function getrecentPostAddedByUserId($userProId,$lastPostID) {	
	$fields = array("c.*,u.fname,u.lname,u.userId as uID,u.birthdate,u.userType,u.avatar,u.sex,u.username,u.email");    
	$tables = array($this->posts." as c",$this->userprofile." as u");
	$result1 = $this->SelectData($fields,$tables, $where= array("c.userId = '".$userProId."' AND c.userId=u.id AND c.userType=u.userType AND c.id >".$lastPostID), $order = array(),$group=array(),$limit = "",0,0); 
	$result  = $this->FetchRow($result1); 
	return $result;
	}

	function getPostAddedByUserId($userProId,$lastPostID) {	
	$fields = array("c.*,u.fname,u.lname,u.userId as uID,u.birthdate,u.userType,u.avatar,u.sex,u.username,u.email");    
	$tables = array($this->posts." as c",$this->userprofile." as u");
	$result1 = $this->SelectData($fields,$tables, $where= array(" c.id =".$lastPostID), $order = array(),$group=array(),$limit = "",0,0); 
	$result  = $this->FetchRow($result1); 
	return $result;
	}



	function getrecentAddedPostCountByUserAndLastPostIdAndTagIds($friendIds,$lastPostID) {
	$fields=array("count(*) as cnt");	  
	$tables = array($this->posts." as c",$this->userprofile." as u");
	$result1 = $this->SelectData($fields,$tables, $where= array("c.userId IN(".$friendIds.") AND c.userId=u.id AND c.userType=u.userType AND c.id >".$lastPostID), $order = array(),$group=array(),$limit = "",0,0); 
	$result  = $this->FetchRow($result1); 
	return $result['cnt'];		
	}


	function getrecentAddedPostCountByUserAndLastPostIdAndUserId($friendIds,$lastPostID) {
	$fields=array("count(*) as cnt");	  
	$tables = array($this->posts." as c",$this->userprofile." as u");
	$result1 = $this->SelectData($fields,$tables, $where= array("c.userId = '".$userProId."' AND c.userId=u.id AND c.userType=u.userType AND c.id >".$lastPostID), $order = array(),$group=array(),$limit = "",0,0); 
	$result  = $this->FetchRow($result1); 
	return $result['cnt'];		
	}


	function getlastid($friendIds,$lastPostID) {
	$fields=array("id");	  
	$tables = array($this->posts);
	$result1 = $this->SelectData($fields,$tables, $where= array(), $order = array("id desc"),$group=array(),1,0,0); 
	$result  = $this->FetchRow($result1); 
	return $result['id'];		
	}

	/*function getrecentPostAddedByUser($userId,$lastPostID) {
	$fields = array("c.*,u.fname,u.lname,u.userId as uID,u.birthdate,u.userType,u.avatar,u.sex,u.username,u.email");    
	$tables = array($this->posts." as c",$this->userprofile." as u");
	$result1 = $this->SelectData($fields,$tables, $where= array("c.userId=u.id AND c.userType=u.userType AND c.userId=".$userId." AND c.id >".$lastPostID), $order = array(),$group=array(),$limit = "",0,0); 
	$result  = $this->FetchRow($result1); 
	return $result;
	}*/

	## Get recently added post count
	function getrecentAddedPostCountByUserAndLastPostId($lastPostID) {
	$fields=array("count(*) as cnt");	  
	$tables = array($this->posts." as c",$this->userprofile." as u");
	$result1 = $this->SelectData($fields,$tables, $where= array("c.userId=u.id AND c.userType=u.userType AND c.id >".$lastPostID), $order = array(),$group=array(),$limit = "",0,0); 
	$result  = $this->FetchRow($result1); 
	return $result['cnt'];		
	}

	/*function getrecentAddedPostCountByUserAndLastPostId($userId,$lastPostID) {
	$fields=array("count(*) as cnt");	  
	$tables = array($this->posts." as c",$this->userprofile." as u");
	$result1 = $this->SelectData($fields,$tables, $where= array("c.userId=u.id AND c.userType=u.userType AND c.userId=".$userId." AND c.id >".$lastPostID), $order = array(),$group=array(),$limit = "",0,1); exit;
	$result  = $this->FetchRow($result1); 
	return $result['cnt'];		
	}*/

       	//Function to get last sub comment by id
	function getLastSubCommentAddedByUser($subCommId) {
	$fields = array("activity.*,u.fname,u.lname,u.userId as uID,u.birthdate,u.userType,u.avatar,u.sex,u.username,u.email");    
	$tables = array("$this->subposts as activity, $this->userprofile as u");
	$where  = array("activity.id= '".$subCommId."' AND activity.comment_by = u.id");		
	$sql    = $this->SelectData($fields,$tables, $where, $order=array(), $group=array(),$limit='',0,0); 
	$result = $this->FetchRow($sql); 
	return $result;
	}

	/**********************************/
	//Get all activities of user's friends
	function getTagsActivities($userProId,$friendIds='0',$followersIds='0',$limit='',$offset=0) {
	$fields = array("activity.*,u.fname,u.lname,u.userId as uID,u.birthdate,u.userType,u.avatar,u.sex,u.username,u.email");    
	$tables = array("$this->posts as activity, $this->userprofile as u");
	
	$where  = array("(activity.userId IN(".$friendIds.") AND activity.userId = u.id AND activity.status='2' AND activity.shareType='2') OR (activity.userId IN(".$followersIds.") AND activity.userId = u.id AND activity.status='2' AND activity.shareType='3') OR (activity.userId = '".$userProId."' AND activity.userId = u.id AND activity.status='2' AND activity.shareType='1') OR (activity.userId IN(".$friendIds.") AND activity.userId = u.id AND activity.status='2' AND activity.shareType='1')");
	
	
	$sql    = $this->SelectData($fields,$tables, $where, $order=array("activity.id DESC"), $group=array(),$limit,$offset,0); 
	$result = $this->FetchAll($sql); 
	return $result;
	}

	/*
	// Display followers activities
	function getFollowersActivities($followersIds,$limit='',$offset=0) {
	$fields = array("activity.*,u.fname,u.lname,u.userId as uID,u.birthdate,u.userType,u.avatar,u.sex,u.username,u.email");    
	$tables = array("$this->posts as activity, $this->userprofile as u");
	$where  = array("activity.userId IN(".$friendIds.") AND activity.userId = u.id AND activity.status='2' AND activity.shareType='3'");
	$sql    = $this->SelectData($fields,$tables, $where, $order=array("activity.id DESC"), $group=array(),$limit,$offset,0); 
	$result = $this->FetchAll($sql); 
	return $result;
	}*/

	function getOnlyTagsActivities($userProId,$limit='',$offset=0) {
	$fields = array("activity.*,u.fname,u.lname,u.userId as uID,u.birthdate,u.userType,u.avatar,u.sex,u.username,u.email");    
	$tables = array("$this->posts as activity, $this->userprofile as u");
	$where  = array("activity.userId = '".$userProId."' AND activity.userId=u.id AND activity.status='2'");	
	$sql    = $this->SelectData($fields,$tables, $where, $order=array("activity.id DESC"), $group=array(),$limit,$offset,0); 
	$result = $this->FetchAll($sql); 
	return $result;
	}

	function getlastCommentAddedByUser($commentID) {
	$fields = array("c.*,u.fname,u.lname,u.userId as uID,u.birthdate,u.userType,u.avatar,u.sex,u.username,u.email");    
	$tables = array($this->posts." as c",$this->userprofile." as u");
	$result1 = $this->SelectData($fields,$tables, $where= array("c.userId=u.id AND c.userType=u.userType AND c.id='".$commentID."'"), $order = array(),$group=array(),$limit = "",0,0); 
	$result  = $this->FetchRow($result1); 
	return $result;
	}

	//Function to get comments on activity
	function getActivityComments($activityId) {
	$fields = array("activity.*,u.fname,u.lname,u.userId as uID,u.birthdate,u.userType,u.avatar,u.sex,u.username,u.email");    
	$tables = array("$this->subposts as activity, $this->userprofile as u");
	$where  = array("activity.postId = '".$activityId."' AND activity.comment_by = u.id");		
	$sql    = $this->SelectData($fields,$tables, $where, $order=array(), $group=array("activity.id DESC"),$limit='',0,0);
	$result = $this->FetchAll($sql); 
	return $result;
	}

	## Delete share by id
	function deletePostById($id){
	$this->DeleteData($this->posts,"id",$id);
	}

	function deleteUserSubComment($id){
	$this->DeleteData($this->subposts,"id",$id);
	}

	//Function to get sub comment count.
	function getTotalSubComments($activityId) {
	$fields = array("COUNT(activity.id) as totalRecords");	
	$tables = array("$this->subposts as activity");
	$where  = array("activity.postId = '".$activityId."'");		
	$sql    = $this->SelectData($fields,$tables, $where, $order=array(), $group=array(),$limit='',0,0);  
	$result = $this->FetchRow($sql); 
	return $result['totalRecords'];
	}

	## Get Message gold , silver & count
	function getAllGoldlikecount($commentId) {
	$fields=array("count(*) as cnt");	
	$tables=array($this->messagelikes);
	$where=array("commentId='".$commentId."' AND likeType='1'");				
	$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 
	$result= $this->FetchRow($result1); 
	return $result['cnt'];		
	}

	function getAllSilverlikecount($commentId) {
	$fields=array("count(*) as cnt");	
	$tables=array($this->messagelikes);
	$where=array("commentId='".$commentId."' AND likeType='2'");				
	$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 
	$result= $this->FetchRow($result1); 
	return $result['cnt'];		
	}

	function getAllBronzelikecount($commentId) {
	$fields=array("count(*) as cnt");	
	$tables=array($this->messagelikes);
	$where=array("commentId='".$commentId."' AND likeType='3'");				
	$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 
	$result= $this->FetchRow($result1); 
	return $result['cnt'];		
	}

	## Get gold likes
	function getGoldLikes($commentId,$userProfileId,$commType)
	{
	$fields=array();	
	$tables=array($this->messagelikes);
	$where=array("commentId='".$commentId."' AND likeType ='".$commType."' AND userProfileId ='".$userProfileId."'");		
	$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 
	$result= $this->FetchRow($result1); 
	return $result;	
	}

	## Check user like or not on activity
	function checkUserLikeOnActvity($commentId,$userProfileId)
	{
	$fields=array();	
	$tables=array($this->messagelikes);
	$where=array("commentId='".$commentId."' AND userProfileId ='".$userProfileId."'");		
	$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 
	$result= $this->FetchRow($result1); 
	return $result;	
	}
	
	// for delete gold like by id
	function deleteGoldLike($Id) {
	$this->DeleteData($this->messagelikes,"id",$Id);
	}

	// Add gold likes
	function addToGoldLikeList($array)
	{
	    $this->InsertData($this->messagelikes, $array);
	    return mysql_insert_id();
	}
	/*********************************************************/
	## Get silver likes
	function getSilverLikes($commentId,$userProfileId,$commType)
	{
	$fields=array();	
	$tables=array($this->messagelikes);
	$where=array("commentId='".$commentId."' AND likeType ='".$commType."' AND userProfileId ='".$userProfileId."'");		
	$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 
	$result= $this->FetchRow($result1); 
	return $result;	
	}

	// For delete silver like by id
	function deleteSilverLike($Id) {
		$this->DeleteData($this->messagelikes,"id",$Id);
	}

	// For delete likes by comment & user profile Id
	function deleteLikesByCommentUserProfileId($commentId,$userProfileId){
	$sql = "delete from ".$this->messagelikes."  WHERE commentId='".$commentId."' AND  userProfileId ='".$userProfileId."'";
	$result1= $this->ExecuteQuery($sql);	 
	}

	// Add silver likes
	function addToSilverLikeList($array)
	{
	    $this->InsertData($this->messagelikes, $array);
	    return mysql_insert_id();
	}

	/**************************************************************/
	## Get bronze likes
	function getBronzeLikes($commentId,$userProfileId,$commType)
	{
	$fields=array();	
	$tables=array($this->messagelikes);
	$where=array("commentId='".$commentId."' AND likeType ='".$commType." 'AND userProfileId ='".$userProfileId."'");		
	$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 
	$result= $this->FetchRow($result1); 
	return $result;	
	}

	// for delete bronze like by id
	function deleteBronzeLike($Id) {
		$this->DeleteData($this->messagelikes,"id",$Id);
	}

	// Add bronze likes
	function addToBronzeLikeList($array)
	{
	    $this->InsertData($this->messagelikes, $array);
	    return mysql_insert_id();
	}
}
?>

<?php 
class Model_Memory extends Database
{	
	function Model_Memory() {	
		$this->memory = MEMORY;
		$this->likes = LIKES;
		$this->mjoins =  MJOINS;
		$this->tags = MEMORYTAGS;
		$this->contribution = CONTRIBUTION;
		$this->shows = SHOWS;
		$this->views = VIEWS;
		$this->showsassets =SHOWSASSETS;
		$this->brandedmemheader =BRANDEDMEMHEADER;
		$this->topPublicMemories =TOPPUBLICMEMORIES;		
		$this->user = USERS;
		$this->Database();
	}
	
	// Add memory Page
	function addMemory($array) {
		$this->InsertData($this->memory, $array);
		$insertId = mysql_insert_id();
		return $insertId;
	}
	
	function addViews($array) {
		$this->InsertData($this->views, $array);
		$insertId = mysql_insert_id();
		return $insertId;
	}
	
	function addShow($array) {
		$this->InsertData($this->shows, $array);
		$insertId = mysql_insert_id();
		return $insertId;
	}
	
	function addShowAssets($array) {
		$this->InsertData($this->showsassets, $array);
		$insertId = mysql_insert_id();
		return $insertId;
	}
	
	function addLike($array) {
		$this->InsertData($this->likes, $array);
		$insertId = mysql_insert_id();
		return $insertId;
	}
	
	function makeContribution($array) {
		$this->InsertData($this->contribution, $array);
		$insertId = mysql_insert_id();
		return $insertId;
	}
	
	function unLike($id,$userId)
	{
		$sql = "delete from ".$this->likes."  WHERE memoryId='".$id."' AND userId='".$userId."'";
		$result1= $this->ExecuteQuery($sql);	
	}
	
	function unJoinMemory($id,$userId)
	{
		$sql = "delete from ".$this->mjoins."  WHERE memoryId='".$id."' AND userId='".$userId."'";
		$result1= $this->ExecuteQuery($sql);	
	}
	
	function joinMemory($array) {
		$this->InsertData($this->mjoins, $array);
		$insertId = mysql_insert_id();
		return $insertId;
	}
	
	function getLikeStatusByMemoryIdAndUserId($id,$userId)
	{
		$fields=array("");	
		$tables=array($this->likes);
		$where = array("memoryId='".$id."' AND userId='".$userId."'");
		$order = array("addedDate  ".$orderBy);
		$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(), $limit="",$offset,0);
		$result= $this->FetchAll($result1); 
		return $result;	
	}
	
	function getJoinStatusByMemoryIdAndUserId($id,$userId)
	{
		$fields=array("");	
		$tables=array($this->mjoins);
		$where = array("memoryId='".$id."' AND userId='".$userId."'");
		$order = array("addedDate  ".$orderBy);
		$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(), $limit="",$offset,0);
		$result= $this->FetchAll($result1); 
		return $result;	
	}
	
	function getTotalLikeCountById($id)
	{
		$fields=array("count(id) as totallikes");	
		$tables=array($this->likes);
		$where = array("memoryId='".$id."' AND status='2'");
		$order = array("addedDate ".$orderBy);
		$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(), $limit="",$offset,0);
		$result= $this->FetchRow($result1); 
		return $result['totallikes'];		
	}

	## Get total likes to memory by Id
	function getTotalLikesToMemoryById($id)
	{
		$fields=array();	
		$tables=array($this->likes);
		$where = array("memoryId='".$id."' AND status='2'");
		$result1 = $this->SelectData($fields,$tables, $where, $order="addedDate DESC", $group=array(), $limit="",$offset,0);
		$result= $this->FetchAll($result1); 
		return $result;	
	}
	
	function getTotalMemberCountById($id)
	{
		$fields=array("count(id) as totalmembers");	
		$tables=array($this->mjoins);
		$where = array("memoryId='".$id."' AND status='2'");
		$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(), $limit="",$offset,0); 
		$result= $this->FetchRow($result1); 
		return $result['totalmembers'];		
	}
	
	function addMemoryTags($array) {
		$this->InsertData($this->tags, $array);
		$insertId = mysql_insert_id();
		return $insertId;
	}
	
	function checkMemoryUniqueIdExists($mcode,$id){
		$fields = array("mcode");
		$tables = array($this->memory);
		if($id){
			$where  = array("mcode='".$mcode."' AND id!= ".$id." AND status!='0'");
		}else{
			$where  = array("mcode='".$mcode."' AND status!='0'");
		}
		$result = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 
		$result1 = $this->FetchRow($result);
		return $result1;
	}
	
	function getAllTagsByMemoryId($id) {
		$fields=array();	
		$tables=array($this->tags);
		$where=array("memoryId='".$id."'");
		$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 
		$result= $this->FetchAll($result1); 
		return $result;		
	}
	
	# get all memory 
	function getAllActiveMemories($type,$search) {
		$fields=array();	
		$tables=array($this->memory);
		$where=array("privacystatus='".$type."' AND status='2'");	
		if($search=='title')
		{
			$where[]= "title LIKE '%".$search."%'";
		}
		if($search=='createdDate')
		{
			$where[]= "createdDate LIKE '%".$search."%'";
		}
		if($search=='sdate')
		{
			$where[]= "sdate LIKE '%".$search."%'";
		}
		$result1 = $this->SelectData($fields,$tables, $where, $order = array("DESC"), $group=array(),$limit = "",0,0); 
		$result= $this->FetchAll($result1); 
		return $result;		
	}

	## Function used in the search list 
        function getAllSearchActiveMemories($search,$type,$orderField, $orderBy , $sLimit) {
		$where = "where ( m.privacystatus='".$type."' AND  m.status='2' ) ";
		if($search!='')
		{
			$where .= " AND (m.title LIKE '%".$search."%' OR mt.title LIKE '%".$search."%' OR m.mcode LIKE '%".$search."%')";
		}
	
		$sSQL = "  SELECT m.*,mt.id as tId,mt.memoryId,mt.title as tagtitle FROM  ".$this->memory. " as m LEFT JOIN ".$this->tags." as mt ON m.id=mt.memoryId ".$where." GROUP BY m.id ORDER BY $orderField $orderBy ".$sLimit;
		$result1= $this->ExecuteQuery($sSQL);	
		$result  = $this->FetchAll($result1); 
		return $result;

				
		/*$fields = "m.*,mt.id as tId,mt.memoryId,mt.title as tagtitle";
		if($orderField!='')
		{
			$orderField ='order by mt.memoryId'; 
		}
		$where = "where ( m.privacystatus='".$type."' AND  m.status='2' ) ";
		if($search!='')
		{
			$where .= " AND (m.title LIKE '%".$search."%' OR mt.title LIKE '%".$search."%' OR m.mcode LIKE '%".$search."%')";
		}
		if($limit != '')
		{
			$limit="LIMIT $offset,$limit";
		}
		
		$catcond.=' ';
		if($where=='')
		{

		echo $sql = "select ".$fields." from ".$this->memory. " as m LEFT JOIN ".$this->tags." as mt ON m.id=mt.memoryId   ".$where." group by m.id ".$orderField." ".	$orderby." ".$limit; 				
		}
		else
		{	
	 	echo $sql = "select ".$fields." from ".$this->memory. " as m LEFT JOIN ".$this->tags." as mt ON m.id=mt.memoryId   ".$searchType." ".$where." group by m.id ".	$orderField." ".$orderby." ".$limit; 		
	
		}
		//echo $sql; exit;		
		$result1= $this->ExecuteQuery($sql);	 
		$result= $this->FetchAll($result1);
		return $result;		
		*/
	
	}

	   ## Function used in the mylist ajax function	
	   function getAllActiveMemoriesByAjax($search,$type,$orderField, $orderBy , $offset,$limit) {
		 $fields = "m.*,mt.id as tId,mt.memoryId,mt.title as tagtitle";
		if($orderField!='')
		{
			$orderField ='order by mt.memoryId'; 
		}
		$where = "where ( m.privacystatus='".$type."' AND  m.status='2' ) ";
		if($search!='')
		{
			$where .= " AND (m.title LIKE '%".$search."%' OR mt.title LIKE '%".$search."%')";
		}
		if($limit != '')
		{
			$limit="LIMIT $offset,$limit";
		}
		$catcond.=' ';
		if($where=='')
		{
 		$sql = "select ".$fields." from ".$this->memory. " as m INNER JOIN ".$this->tags." as mt ON m.id=mt.memoryId   ".$where." group by m.id ".$orderField." ".$orderby." ".$limit; 				
		}
		else
		{	
	 	$sql = "select ".$fields." from ".$this->memory. " as m INNER JOIN ".$this->tags." as mt ON m.id=mt.memoryId   ".$searchType." ".$where." group by m.id ".$orderField." ".$orderby." ".$limit; 		
		}
		//echo $sql; //exit;		
		$result1= $this->ExecuteQuery($sql);	 
		$result= $this->FetchAll($result1);
		return $result;		
	}
	
	function getAllActiveMemoriesCountByAjax($search,$type) {
		 $fields = "m.*,mt.id as tId,mt.memoryId,mt.title as tagtitle";
		$where = "where ( m.privacystatus='".$type."' AND  m.status='2' ) ";
		if($search!='')
		{
			$where .= " AND (m.title LIKE '%".$search."%' OR mt.title LIKE '%".$search."%')";
		}
		$catcond.=' ';
		if($where=='')
		{
		$sql = "select ".$fields." from ".$this->memory. " as m LEFT  JOIN ".$this->tags." as mt ON mt.memoryId=m.id   ".$where." ";				
		}
		else
		{	
	 	$sql = "select ".$fields." from ".$this->memory. " as m LEFT  JOIN ".$this->tags." as mt ON mt.memoryId=m.id  ".$searchType." ".$where." ";
		
		}
		//echo $sql; //exit;		
		$result1= $this->ExecuteQuery($sql);	 
		$result= $this->FetchAll($result1);
		
		return $result;		
	}
	
	function getAllActiveMemoryContributionByAjax($id,$orderField, $orderBy , $offset,$limit) {
  		if($orderField!='')
		{
			$orderField ='order by '.$orderField; 
		}
		$where='';
	
		if($limit != '')
		{
			$limits="LIMIT $offset,$limit";
		}
		$catcond.=' ';
		if($where=='')
		{
		$sql="SELECT * FROM ".$this->contribution ."    WHERE  status='2' AND memoryId='".$id."' ".$where."  ".$orderField." ".$orderBy." ".$limits."";		
		}

		else
		{	
		$sql="SELECT  * FROM ".$this->contribution ."    WHERE    status='2'  AND memoryId='".$id."' ".$searchType."".$where."  ".$orderField." ".$orderBy." ".$limits."";	
		}
		//echo $sql; exit;		
		$result1= $this->ExecuteQuery($sql);	 
		$result= $this->FetchAll($result1);
		return $result;		
	}

   function getAllActiveMemoriesByUserId($userId,$search='',$type,$orderField='',$orderBy='',$offset,$limit) {
      		if($orderField!='')
		{
			$orderField ='order by '.$orderField; 
		}
		$where='';
		if($search!='')
		{
			$where .= " AND title LIKE '%".$search."%'";
		}
		
		if($limit != '')
		{
			$limits="LIMIT $offset,$limit";
		}
			
		$catcond.=' ';
		if($where=='')
		{
		$sql="SELECT * FROM ".$this->memory ." WHERE  status='2' AND userId='".$userId."' AND (privacystatus='".$type."' OR privacystatus='2') ".$where."  ".$orderField." ".$orderBy." ".$limits."";		
		}
		else
		{	
		$sql="SELECT  * FROM ".$this->memory ." WHERE  status='2' AND userId='".$userId."' AND (privacystatus='".$type."' OR privacystatus='2') ".$searchType."".$where."  ".$orderField." ".$orderBy." ".$limits."";	
		}
		//echo $sql; exit;
		$result1= $this->ExecuteQuery($sql);	 
		$result= $this->FetchAll($result1);
		return $result;		
	}
	function getAllActiveMemoriesByUserIdForUserSide($userId,$orderField,$orderBy,$sLimit=''){
		$sSQL = "  SELECT * FROM  ".$this->memory." WHERE userId ='".$userId."' AND status='2' ORDER BY $orderField $orderBy ".$sLimit;
		$result1= $this->ExecuteQuery($sSQL);	
		$result  = $this->FetchAll($result1); 
		return $result;
	}
	
	function getMemoryDetailsById($id) {
		$fields=array();	
		$tables=array($this->memory);
		$where=array("id='".$id."'");	
		$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 
		$result= $this->FetchRow($result1); 
		return $result;		
	}

	function getMemoryDetailsByQrCode($mcode) {
		$fields=array();	
		$tables=array($this->memory);
		$where=array("mcode='".$mcode."'");	
		$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 
		$result= $this->FetchRow($result1); 
		return $result;		
	}
	
	function updateMemoryStatus($ids, $status) {
		$sql = "UPDATE ".$this->memory." SET status='".$status."' WHERE id IN (".$ids.")";
		$result1= $this->ExecuteQuery($sql);	 
	}
	
	function editMemoryInformation($Array,$Id)
	{
		$this->UpdateData($this->memory,$Array,"id",$Id,0);
	}
	
	function getAllUserMemoryes($uID,$val, $orderField='', $orderBy='', $limit='',$offset=''){
		$fields = array("");
		$tables = array($this->memory);
		$where = array("userId ='".$uID."' AND status!='0'");
		if($val) {
			$where[] = '( title LIKE "%'.$val.'%")';
		}
		$order = array("$orderField $orderBy");
		$result1 = $this->SelectData($fields,$tables,$where,$order ,$group=array(),$limit,$offset,0);
		$result = $this->FetchAll($result1);
		return $result;
	}

	function getAllUserTopMemories($uID,$limit=''){
		$sSQL = "  SELECT * FROM  ".$this->memory." WHERE userId ='".$uID."' AND status='2' ORDER BY createdDate DESC ".$limit;
		$result1= $this->ExecuteQuery($sSQL);	
		$result  = $this->FetchAll($result1); 
		return $result;
		
		/*$fields = array("");
		$tables = array($this->memory);
		$where = array("userId ='".$uID."' AND status!='0'");
		$result1 = $this->SelectData($fields,$tables,$where,$order= array("createdDate DESC"),$group=array(),$limit = "25",0,0); 
		$result = $this->FetchAll($result1);
		return $result;*/
	}

	function getPublicMemoriesForHomePage($limit=''){
		$sSQL = "  SELECT * FROM  ".$this->memory." WHERE privacystatus ='1' AND status='2' ORDER BY createdDate DESC ".$limit;
		$result1= $this->ExecuteQuery($sSQL);	
		$result  = $this->FetchAll($result1); 
		return $result;
		
		/*$fields=array();	
		$tables = array($this->memory);
		$where = array("privacystatus ='1' AND status='2'");
		$result1 = $this->SelectData($fields,$tables,$where,$order=array("createdDate DESC"),$group=array(),$limit,0,1); 
		$result = $this->FetchAll($result1);
		return $result;*/
	}
	
	function getRecentUsersMemory($search, $orderBy = 'DESC', $limit='',$offset='')
	{
		$fields=array();	
		$tables=array($this->memory);
		$where = array("status!='0'");
		$order = array("createdDate ".$orderBy);
		$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(), $limit="",$offset,0);
		$result= $this->FetchAll($result1); 
		return $result;		
	}
	
	function getAllMemoryes($status,$val, $orderField='', $orderBy='', $limit='',$offset=''){
		$fields = array("");
		$tables = array($this->memory);
		if($status!='')
		{
		     $where=array("privacystatus = '$status'"); 
		}
		else   
			$where=array("status <> '0'"); 
		if($val) {
			$where[] = '( title LIKE "%'.$val.'%")';
		}
		$order = array("$orderField $orderBy");
		$result1 = $this->SelectData($fields,$tables,$where,$order ,$group=array(),$limit,$offset,0); 
		$result = $this->FetchAll($result1);
		return $result;
	}
	
	function deleteAllMemorytagByMemoryId($id){
		$this->DeleteData($this->tags,"memoryId",$id);
	}

	function editMemorytagByMemoryId($id, $status) {
		$sql = "UPDATE ".$this->tags." SET status='".$status."' WHERE id ='".$ids."'";
		$result1= $this->ExecuteQuery($sql);	 
	}

	## Get user memories by memory date in descending order by memory date
	function getAllUserTopMemoriesByMemoryDateDescOrder($uID,$limit=''){
		$sSQL = "  SELECT * FROM  ".$this->memory." WHERE userId ='".$uID."' AND status='2' ORDER BY sdate DESC ".$limit;
		$result1= $this->ExecuteQuery($sSQL);	
		$result  = $this->FetchAll($result1); 
		return $result;
		/*$fields = array("");
		$tables = array($this->memory);
		$where = array("userId ='".$uID."' AND status!='0'");
		$result1 = $this->SelectData($fields,$tables,$where,$order= array("sdate DESC"),$group=array(),$limit = "25",0,0); 
		$result = $this->FetchAll($result1);
		return $result;*/
	}
	
	function getTotalMemberCount()
	{
		$fields=array("count(id) as totalcount");	
		$tables=array($this->memory);
		$where = array("status!='0'");
		$order = array("createdDate ".$orderBy);
		$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(), $limit="",$offset,0);
		$result= $this->FetchRow($result1); 
		return $result['totalcount'];		
	}
	
	function getTotalShowImageCount($id)
	{
		$fields=array("count(id) as totalcount");	
		$tables=array($this->contribution);
		$where = array("memoryId='".$id."' AND status='2'");
		$result1 = $this->SelectData($fields,$tables, $where, $order=array(), $group=array(), $limit="",$offset,0);
		$result= $this->FetchRow($result1); 
		return $result['totalcount'];		
	}
	
	function getTotalViewCount($id)
	{
		$fields=array("count(id) as totalcount");	
		$tables=array($this->views);
		$where = array("mId='".$id."'");
		$result1 = $this->SelectData($fields,$tables, $where, $order=array(), $group=array(), $limit="",$offset,0);
		$result= $this->FetchRow($result1); 
		return $result['totalcount'];		
	}
	
	function getTotalcontributionCountByMid($id)
	{
		$fields=array("count(id) as totalcount");	
		$tables=array($this->contribution);
		$where = array("memoryId='".$id."'");
		$result1 = $this->SelectData($fields,$tables, $where, $order=array(), $group=array(), $limit="",$offset,0);
		$result= $this->FetchRow($result1); 
		return $result['totalcount'];		
	}
	
	function updateContributionMemoryStatus($ids, $status) {
		$sql = "UPDATE ".$this->contribution." SET status='".$status."' WHERE id IN (".$ids.")";
		$result1= $this->ExecuteQuery($sql);	 
	}
	
	function editContributionMemoryInformation($Array,$Id)
	{
		$this->UpdateData($this->contribution,$Array,"id",$Id,0);
	}
	
	function getAllContributionMemoryes($id,$orderField='', $orderBy='', $limit='',$offset='')
	{
		$fields = array("");
		$tables = array($this->contribution);
		$where=array("memoryId='".$id."' AND status <> '0'"); 
		$order = array("$orderField $orderBy");
		$result1 = $this->SelectData($fields,$tables,$where,$order ,$group=array(),$limit,$offset,0);
		$result = $this->FetchAll($result1);
		return $result;
	}

	## Get user memories by created date in descending order by createdDate
	function getAllUserTopMemoriesByCreatedDateDescOrder($uID,$limit=''){
		$sSQL = "  SELECT * FROM  ".$this->memory." WHERE userId ='".$uID."' AND status='2' ORDER BY createdDate DESC ".$limit;
		$result1= $this->ExecuteQuery($sSQL);	
		$result  = $this->FetchAll($result1); 
		return $result;
		
		/*$fields = array("");
		$tables = array($this->memory);
		$where = array("userId ='".$uID."' AND status!='0'");
		$result1 = $this->SelectData($fields,$tables,$where,$order= array("createdDate DESC"),$group=array(),$limit = "25",0,0); 
		$result = $this->FetchAll($result1);
		return $result;*/
	}
	
	function updateShowsMemoryStatus($ids, $status) {
		$sql = "UPDATE ".$this->showsassets." SET status='".$status."' WHERE id IN (".$ids.")";
		$result1= $this->ExecuteQuery($sql);	 
	}
	
	function editShowsMemoryInformation($Array,$Id)
	{
		$this->UpdateData($this->showsassets,$Array,"id",$Id,0);
	}
	
	function getAllShowsMemoryes($id,$orderField='', $orderBy='', $limit='',$offset='')
	{
		$fields = array("");
		$tables = array($this->showsassets);
		$where=array("showsId='".$id."' AND status <> '0'"); 
		$order = array("$orderField $orderBy");
		$result1 = $this->SelectData($fields,$tables,$where,$order ,$group=array(),$limit,$offset,0); 
		$result = $this->FetchAll($result1);
		return $result;
	}
	function getShowAssetsByContributionId($contributionId){
		$fields = array("image");
		$tables = array($this->showsassets);
		$where=array("showsId='".$contributionId."' AND status <> '0'"); 		
		$result1 = $this->SelectData($fields,$tables,$where,$order ,$group=array(),$limit='',$offset='',0); 
		$result = $this->FetchAll($result1);
		return $result;
	}
	function getTotalShowsCountByMid($id)
	{
		$fields=array("count(id) as totalcount");	
		$tables=array($this->contribution);
		$where = array("memoryId='".$id."' AND ctype='s'");
		$result1 = $this->SelectData($fields,$tables, $where, $order=array(), $group=array(), $limit="",$offset,0);
		$result= $this->FetchRow($result1); 
		return $result['totalcount'];		
	}

	function getContributionDetByMemoryIds($id)
	{
		$fields = array("");
		$tables = array($this->contribution);
		$where=array("memoryId='".$id."' AND status='2'"); 
		$result1 = $this->SelectData($fields,$tables, $where, $order=array(), $group=array(), $limit="",$offset,0); 
		$result = $this->FetchRow($result1);
		return $result;
	}
	
	function getAllActiveMemoriesByUserIdForMobile($userId) {
		$sql="SELECT  * FROM ".$this->memory ." WHERE  status='2' AND userId='".$userId."'";		
		$result1= $this->ExecuteQuery($sql);	 
		$result= $this->FetchAll($result1);
		return $result;		
	}

	## Get user memories by title in descending order by title
	function getAllUserTopMemoriesByTitleDescOrder($uID,$limit=''){
		$sSQL = "  SELECT * FROM  ".$this->memory." WHERE userId ='".$uID."' AND status='2' ORDER BY title ASC ".$limit;
		$result1= $this->ExecuteQuery($sSQL);	
		$result  = $this->FetchAll($result1); 
		return $result;
		/*$fields = array("");
		$tables = array($this->memory);
		$where = array("userId ='".$uID."' AND status!='0'");
		$result1 = $this->SelectData($fields,$tables,$where,$order= array("title DESC"),$group=array(),$limit = "25",0,0); 
		$result = $this->FetchAll($result1);
		return $result;*/
	}
	
	function getTotalJoinCountByMid($id)
	{
		$fields=array("count(m.id) as totalcount");	
		$tables=array($this->mjoins." as m",$this->user." as u");
		$where = array("m.userId=u.userId AND m.memoryId='".$id."' AND u.userId!='0' AND m.status!='0'");
		$result1 = $this->SelectData($fields,$tables, $where, $order=array(), $group=array(), $limit="",$offset,0);
		$result= $this->FetchRow($result1); 
		return $result['totalcount'];		
	}	
	
	function getAllJoinedUsersByMemoryId($mId,$orderField, $orderBy, $offset,$limit) {
		$fields=array("m.*,u.email,u.fname,u.lname");	
		$tables=array($this->mjoins." as m",$this->user." as u");
		$where = array("m.userId=u.userId AND memoryId='".$mId."' AND u.userId!='0' AND m.status!='0'");
		$result1 = $this->SelectData($fields,$tables, $where, $order=array(), $group=array(), $limit,$offset,0);
		$result= $this->FetchAll($result1); 
		return $result;
	}
	
	function editMemoryJoinedInformation($id){
		$sql = "UPDATE ".$this->mjoins." SET status='0' WHERE id='".$id."'";
		$result1= $this->ExecuteQuery($sql);
	}
	
	function updateMemoryJoinedInformationStatus($ids,$status){
		$sql = "UPDATE ".$this->mjoins." SET status='".$status."' WHERE id IN (".$ids.")";
		$result1= $this->ExecuteQuery($sql);
	}
	
	function getContributedImagesByMemoryIdAndType($id){
		$sql="SELECT  * FROM ".$this->contribution ." WHERE ctype='i' AND status='2' AND memoryId='".$id."'";
		//echo $sql; exit;
		$result1= $this->ExecuteQuery($sql);	 
		$result= $this->FetchAll($result1);
		return $result;	
	}
	
	function getContributedImagesByMemoryId($id) {
		$sql="SELECT  * FROM ".$this->contribution ." WHERE  status='2' AND memoryId='".$id."'";
		$result1= $this->ExecuteQuery($sql);	 
		$result= $this->FetchAll($result1);
		return $result;		
	}

	function getContributionDetByContributionIds($id)
	{
		$fields = array("");
		$tables = array($this->contribution);
		$where=array("id='".$id."'"); 
		$result1 = $this->SelectData($fields,$tables, $where, $order=array(), $group=array(), $limit="",$offset,0); 
		$result = $this->FetchRow($result1);
		return $result;
	}

	## Get join count for the memory
	function getJoinCountToMemory($id)
	{
		$fields=array("");	
		$tables=array($this->mjoins);
		$where = array("memoryId='".$id."' AND status='2'");
		$result1 = $this->SelectData($fields,$tables, $where, $order = array("addedDate DESC"), $group=array(), $limit="",$offset,0);
		$result= $this->FetchAll($result1); 
		return $result;	
	}
	
	## 
	function getContributeMemoryByMemoryId($id)
	{
		$fields = array("");
		$tables = array($this->contribution);
		$where=array("memoryId='".$id."' AND status='2'"); 
		$result1 = $this->SelectData($fields,$tables, $where, $order=array(), $group=array(), $limit="",$offset,0); 
		$result = $this->FetchAll($result1);
		return $result;
	}

	function getAllMemoryJoinUsers($id) {
		$fields=array();	
		$tables=array($this->mjoins);
		$where = array("memoryId='".$id."' AND status='2'");
		$result1 = $this->SelectData($fields,$tables, $where, $order = array("addedDate DESC"), $group=array(),$limit = "6",0,0); 
		$result= $this->FetchAll($result1); 
		return $result;	
	}

	## Get total likes to memory by Id
	function getAllLikeUsersByMemoryId($id)
	{
		$fields=array();	
		$tables=array($this->likes);
		$where = array("memoryId='".$id."' AND status='2'");
		$result1 = $this->SelectData($fields,$tables, $where, $order="addedDate DESC", $group=array(),$limit = "6",0,0); 
		$result= $this->FetchAll($result1); 
		return $result;	
	}
	## 
	function getJoinedMemoriesByUserIdExceptByMemoryId($userId,$memId)
	{
		$fields=array("");	
		$tables=array($this->mjoins);
		$where = array("userId='".$userId."' AND memoryId!='".$memId."' AND status='2'");
		$order = array("id DESC");
		$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array("memoryId"), $limit="",$offset,0);
		$result= $this->FetchAll($result1); 
		return $result;	
	}
	##
	function getMemoryJoinedCountByUserIdExceptByMemoryId($userId,$memId){
		$fields=array("count(*) as memoryJoinCount");	
		$tables=array($this->mjoins);
		$where = array("userId='".$userId."' AND memoryId!='".$memId."' AND status='2'");
		$order = array("id DESC");
		$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(""), $limit="",$offset,0);
		$result= $this->FetchRow($result1); 
		return $result['memoryJoinCount'];	
	}

	## Fetch all upcoming memories those Event After 24th hour of an memory event strat
	function getAllActiveUpcomingMemories($tomorrowDate) {
		$fields=array();	
		$tables=array($this->memory);
		$where=array("sdate='".$tomorrowDate."' AND isEvent='y' AND status='2'");	
		$result1 = $this->SelectData($fields,$tables, $where, $order = array("createdDate DESC"), $group=array(),$limit = "",0,0); 
		$result= $this->FetchAll($result1); 
		return $result;		
	}

	## get all branded memory headers
	function getAllBrandedMemoryHeaders($status,$val, $orderField='', $orderBy='', $limit='',$offset=''){
		$fields = array("");
		$tables = array($this->brandedmemheader);
		if($status!='')
		{
		     $where=array("privacystatus = '$status'"); 
		}
		else   
			$where=array("status <> '0'"); 
		if($val) {
			$where[] = '( title LIKE "%'.$val.'%")';
		}
		$order = array("$orderField $orderBy");
		$result1 = $this->SelectData($fields,$tables,$where,$order ,$group=array(),$limit,$offset,0); 
		$result = $this->FetchAll($result1);
		return $result;
	}
	// Add branded memory headers
	function addBrandedMemoryHeader($array) {
		$this->InsertData($this->brandedmemheader, $array);
		$insertId = mysql_insert_id();
		return $insertId;
	}

	function getBrandedMemoryHeaderDetailsById($id) {
		$fields=array();	
		$tables=array($this->brandedmemheader);
		$where=array("id='".$id."'");	
		$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 
		$result= $this->FetchRow($result1); 
		return $result;		
	}

	function editBrandedHeaderInformation($Array,$Id)
	{
		$this->UpdateData($this->brandedmemheader,$Array,"id",$Id,0);
	}
	
	function editBrandedHeaderInformationStatusExceptId($status,$Id){
		$sql = "UPDATE ".$this->brandedmemheader." SET status='".$status."' WHERE id!='".$Id."' AND status!='0'";
		$result1= $this->ExecuteQuery($sql);	 	
	}
	
	function getBrandedMemoryHeaderDetailsByUserUniqueIdAndStatus($moid,$status){
		$fields=array();	
		$tables=array($this->brandedmemheader);
		$where=array("moid='".$moid."' AND status='".$status."'");	
		$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 
		$result= $this->FetchRow($result1); 
		return $result;	
	}
	
	function getBrandedMemoryHeaderDetailsByTitleAndStatus($memTitle='',$status){
		$fields=array();	
		$tables=array($this->brandedmemheader);
		$where=array("status='".$status."'");	
		$parts = array();
		foreach($memTitle as $search_value ){
			$parts[] = " (LOWER(keywords) LIKE '%".strtolower($search_value)."%')";
		}
		$parts = implode(' OR ', $parts);
		$where[] ="(".$parts.")";
		$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 
		$result= $this->FetchRow($result1); 
		return $result;	
	}
	
	function getBrandedMemoryHeaderDetailsByKeywordsAndStatus($tagAray='',$status){
		$fields=array();	
		$tables=array($this->brandedmemheader);
		$where=array("status='".$status."'");	
		$parts = array();
		foreach($tagAray as $search_value ){
			$parts[] = " (LOWER(keywords) LIKE '%".strtolower($search_value)."%')";
		}
		$parts = implode(' OR ', $parts);
		$where[] ="(".$parts.")";
		$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 
		$result= $this->FetchRow($result1); 
		return $result;	
	}

	## Fetch all past memories those Event before 24th hour of an memory event strat
	function getAllActivePastMemories($pastDate) {
		$fields=array();	
		$tables=array($this->memory);
		$where=array("edate='".$pastDate."' AND isEvent='y' AND status='2'");	
		$result1 = $this->SelectData($fields,$tables, $where, $order = array("createdDate DESC"), $group=array(),$limit = "",0,0); 
		$result= $this->FetchAll($result1); 
		return $result;		
	}


	function getTopTenPublicMemories($keywords,$moid,$setAll,$likeJoinFBshare,$limit='') {
		$fields=array("m.*");
		$tables=array($this->memory." as m",$this->tags." as mt");
		$where = array();
		$where.= "m.id=mt.memoryId AND m.privacystatus='1' AND  m.status='2'";
		if($setAll != '1')
		{
		if($keywords!='')
		{
		   $where.= " ( m.title LIKE '%".strtolower($keywords)."%') OR (mt.title LIKE '%".strtolower($keywords)."%') ) ";
		}
		if($moid != '')
		{
		   $where.=" ( m.userId = '".$moid."') ";
		}
		if($likeJoinFBshare=='1')
		{
		} 
		}
		$sql    = $this->SelectData($fields,$tables, $where, $order=array("m.createdDate DESC"), $group=array(),$limit = "10",0,0); 
		$result = $this->FetchAll($sql); 
		return $result;
	}

	/*function getTopTenPublicMemories($keywords,$moid,$setAll,$likeJoinFBshare,$limit='') {
		$fields=array("m.*,mt.*");
		$tables=array($this->memory." as m",$this->tags." as mt");
		if($keywords!='')
		{
			
			$where  = array("( m.id=mt.memoryId AND m.privacystatus='1' AND (LOWER(m.title) LIKE '%".strtolower($keywords)."%') OR (LOWER(mt.title) LIKE '%".strtolower($keywords)."%') AND  m.status='2')");
		}

		if($setAll=='1')
		{
		
			$where  = array("(m.id=mt.memoryId AND m.privacystatus='1' AND  m.status='2')");
		}
		

		$sql    = $this->SelectData($fields,$tables, $where, $order=array("m.createdDate DESC"), $group=array(),$limit = "10",0,0); 
		$result = $this->FetchAll($sql); 
		return $result;
	}*/


	// Edit Top Public Memory Settings
	function editTopPublicMemorySettings($Array,$Id)
	{
		$this->UpdateData($this->topPublicMemories,$Array,"id",$Id,0); 
	}

	// Get Public Memory Setting Details
	function getPublicMemorySettingDetailsById($id) {
		$fields=array();	
		$tables=array($this->topPublicMemories);
		$where=array("id='".$id."'");	
		$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 
		$result= $this->FetchRow($result1); 
		return $result;		
	}
        function getAllJoinedUsersByMemoryIdExceptUserId($mId,$userId) {
        	$fields=array("m.*,u.email,u.fname,u.lname");	
		$tables=array($this->mjoins." as m",$this->user." as u");
		$where = array("m.userId=u.userId AND memoryId='".$mId."' AND u.userId!='0' AND m.userId!='".$userId."' AND m.status!='0'");
		$result1 = $this->SelectData($fields,$tables, $where, $order=array(), $group=array(), $limit,$offset,0);
		$result= $this->FetchAll($result1); 
		return $result;
	}
}
?>
