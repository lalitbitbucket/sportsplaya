<?php

class Model_Album extends Database 
{	
	## Constructor
	function Model_Album() {
	$this->album 		= ALBUM;
	$this->albumimage 	= ALBUMIMAGES;
	$this->albumimagecomment 	= ALBUMIMAGES_COMMENTS;
	$this->albumvideo 	= ALBUMVIDEOS;
	$this->user 		= USERS;
	$this->userprofile 	= USERPROFILE;
	$this->albumphotolikes  = ALBUMPHOTOLIKES;
	$this->Database();
	}

	## Add album details
	function addAlbum($array) {
	  $this->InsertData($this->album , $array);		
	  $insertId = mysql_insert_id();
	  return $insertId;
	}

	## Add album images with created album ID
	function addAlbumImages($array) {
	  $this->InsertData($this->albumimage , $array);		
	  $insertId = mysql_insert_id();
	  return $insertId;
	}
	
	## Update Album status by AlbumId 
	function updateAlbumStatus($albumID, $status) {		
		$sql = "UPDATE ".$this->album." SET status='".$status."' WHERE id = '".$albumID."'";
		$result1= $this->ExecuteQuery($sql);	 
	}
	
	## Delete Album by id
	function deleteAlbum($albumID) {
		$sql = "UPDATE ".$this->album." SET status='0' WHERE id = '".$albumID."'";
		$result1= $this->ExecuteQuery($sql);
	}

	 ## Delete user by album
	function deleteAlbumById($id){
		$this->DeleteData($this->album,"id",$id);
	}

	function deletePhotoById($id){
		$this->DeleteData($this->albumimage,"id",$id);
	}
	
	## Update Album status with multiple AlbumId
	function updateMultipleAlbumStatus($albumID, $status) {
		$sql = "UPDATE ".$this->album." SET status='".$status."' WHERE id IN (".$albumID.")";
		$result1= $this->ExecuteQuery($sql);	 
	}
	
	## Delete Album with multiple AlbumId
	function deleteMultipleAlbum($albumID) {
		$sql = "DELETE FROM ".$this->album." WHERE id IN (".$albumID.")";
		$result1= $this->ExecuteQuery($sql);	 
	}
	
	## Get all Album
	function getAllAlbum($search='', $orderField = 'title', $orderBy = 'ASC', $limit='',$offset='') {
		$fields=array("u.id as uId,u.fname,u.lname,u.status as ustatus,a.*");	
		$tables=array($this->album." as a",$this->userprofile." as u");
		$where = array("a.status!='0' AND a.userId=u.id");
		if($search != '' && $search != 'Search') {
			$where[] = "(a.title LIKE '%".$search."%')";
		} 
		$order = array("$orderField $orderBy");
		$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(), $limit,$offset,0);
		$result= $this->FetchAll($result1); 

		return $result;		
	}

	## Get all album list via user id
	function getAllAlbumByUserId($uId,$limit='',$offset='') {
		$fields = array();
		$tables = array($this->album);	
		$where=array("userId=".$uId);
		$order = array();
		$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(), $limit='', $offset=0,0);
		$result= $this->FetchAll($result1); 
		return $result;		
	}

	function getAlbumDetailsByAlbumId($albumId,$limit='',$offset='') {
		$fields = array();
		$tables = array($this->album);	
		$where=array("id=".$albumId);
		$order = array();
		$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(), $limit='', $offset=0,0);
		$result= $this->FetchRow($result1); 
		return $result;		
	}

	## Edit album by albumId
	function editAlbumValueById($array, $Id){
		$this->UpdateData($this->album,$array,"id",$Id,0);
	}

	## Edit album by albumId
	function editAlbumImagesById($array, $Id){
		$this->UpdateData($this->albumimage,$array,"id",$Id,0);
	}

	function getAllAlbumPhotosByAlbumId($albumId,$limit='',$offset='') {
		$fields = array();	
		$tables = array($this->albumimage);
		$where=array("albumId=".$albumId);
		$result1 = $this->SelectData($fields,$tables, $where, $order = array("id ASC"), $group=array(),$limit,$offset,0); 
		$result  = $this->FetchAll($result1); 	
		return $result;
	}

	## Get groups image by albumId
	function getAlbumImageByAlbumId($albumId) {
		$fields=array();	
		$tables=array($this->albumimage);
		$where=array("albumId=".$albumId);		
		$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 
		$result= $this->FetchRow($result1); 
		return $result;		
	}

	function getAlbumImageByPhotoId($photoId) {
		$fields=array();	
		$tables=array($this->albumimage);
		$where=array("id=".$photoId);		
		$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 
		$result= $this->FetchRow($result1); 
		return $result;		
	}
	
	################################photos##############################################
	## Update Photo status by PhotoId 
	function updatePhotoStatus($photoID, $status) {		
		$sql = "UPDATE ".$this->albumimage." SET status='".$status."' WHERE id = '".$photoID."'";
		$result1= $this->ExecuteQuery($sql);	 
	}
	
	## Delete Photo by id
	function deletePhoto($photoID) {
		echo $sql = "UPDATE ".$this->albumimage." SET status='0' WHERE id = '".$photoID."'";
		$result1= $this->ExecuteQuery($sql);
	}
	
	## Update Photo status with multiple PhotoId
	function updateMultiplePhotoStatus($photoID, $status) {
		$sql = "UPDATE ".$this->albumimage." SET status='".$status."' WHERE id IN (".$photoID.")";
		$result1= $this->ExecuteQuery($sql);	 
	}
	
	## Delete Photo with multiple PhotoId
	function deleteMultiplePhoto($photoID) {
		$sql = "DELETE FROM ".$this->albumimage." WHERE albumId IN (".$photoID.")";
		$result1= $this->ExecuteQuery($sql);	 
	}

	## Delete Photo with multiple PhotoId
	function deleteMultiplePhotos($photoID) {
		$sql = "DELETE FROM ".$this->albumimage." WHERE id IN (".$photoID.")";
		$result1= $this->ExecuteQuery($sql);	 
	}
        ## Get all Photo
	function getAllPhotoByalbumID($albumID,$search='',$limit='',$offset='') {
		$fields=array("u.id as uId,a.title,ai.*");	
		$tables=array($this->album." as a",$this->userprofile." as u",$this->albumimage." as ai");
		$where = array("ai.albumId='".$albumID."' AND ai.userId=u.id AND ai.status!='0'");
		if($search != '' && $search != 'Search') {
			$where = array("a.title LIKE '%".$search."%' ");
		} 
		$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(), $limit,$offset,0);
		$result= $this->FetchAll($result1); 
		return $result;		
	}
        ################################video##############################################
	## Update video status by videoId 
	function updateVideoStatus($videoID, $status) {		
		$sql = "UPDATE ".$this->albumvideo." SET status='".$status."' WHERE videoId = '".$videoID."'";
		$result1= $this->ExecuteQuery($sql);	 
	}
	
	## Delete video by id
	function deleteVideo($videoID) {
		$sql = "UPDATE ".$this->albumvideo." SET status='0' WHERE videoId = '".$videoID."'";
		$result1= $this->ExecuteQuery($sql);
	}
	
	## Update video status with multiple videoId
	function updateMultipleVideoStatus($videoID, $status) {
		$sql = "UPDATE ".$this->albumvideo." SET status='".$status."' WHERE videoId IN (".$videoID.")";
		$result1= $this->ExecuteQuery($sql);	 
	}
	
	## Delete video with multiple videoId
	function deleteMultipleVideo($videoID) {
		$sql = "DELETE FROM ".$this->albumvideo." WHERE videoId IN (".$videoID.")";
		$result1= $this->ExecuteQuery($sql);	 
	}

        ## Get all video
	function getAllVideoByalbumID($albumID,$search='',$limit='',$offset='') {
		$fields=array("u.userId,u.fname,u.lname,u.status as ustatus,a.nameofalbum,a.locationofalbum,a.id,av.*");	
		$tables=array($this->album." as a",$this->user." as u",$this->albumvideo." as av");
		$where = array("av.albumId='".$albumID."' AND av.userID=u.userId AND av.status!='0'");
		if($search != '' && $search != 'Search') {
			$where = array("a.nameofalbum LIKE '%".$search."%' ");
		} 
		$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(), $limit,$offset,0);
		$result= $this->FetchAll($result1); 
		return $result;		
	}
	## Delete Photo by id
	function deletePhotosofAlbum($photoID) {
		$sql = "UPDATE ".$this->albumimage." SET status='0' WHERE albumId = '".$photoID."'";
		$result1= $this->ExecuteQuery($sql);
	}
	function getAllAlbumPhotosByAId($albumId,$limit='',$offset='') {
		$fields = array();	
		$tables = array($this->albumimage);
		$where=array("albumId='".$albumId."' AND status!='0'");
		$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit,$offset,0); 
		$result  = $this->FetchAll($result1); 	
		return $result;
	}

	/***************** Take album photo count ********************/
	function getAlbumPhotoLikeCount($albumPhotoId,$userProfileId) 
	{
	$fields=array("count(*) as cnt");	
	$tables=array($this->albumphotolikes);
	$where=array("albumphotoId='".$albumPhotoId."' and photolikeProfileId='".$userProfileId."'");				
	$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 
	$result= $this->FetchRow($result1); 
	return $result['cnt'];		
	}

	function getAlbumPhotoLikes($albumphotoId, $userProfileId)
	{
	$fields=array();	
	$tables=array($this->albumphotolikes);
	$where=array("albumphotoId='".$albumphotoId."' and photolikeProfileId =".$userProfileId."  ");		
	$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 
	$result= $this->FetchRow($result1); 
	return $result;	
	}

	// for delete album photo like by album photo id
	function deleteAlbumPhotoLike($Id) {
	$this->DeleteData($this->albumphotolikes,"id",$Id);
	}

	// Check album photo like count 
	function getAllAlbumPhotoLikecount($albumphotoId) {
	$fields=array("count(*) as cnt");	
	$tables=array($this->albumphotolikes);
	$where=array("albumphotoId='".$albumphotoId."'");				
	$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 
	$result= $this->FetchRow($result1); 
	return $result['cnt'];		
	}

	// Add album photo likes
	function addToAlbumPhotoLikes($array)
	{
		$this->InsertData($this->albumphotolikes,$array);
		return mysql_insert_id();
	}

	function getPhotoLikeByPhotoId($albumphotoId){
	$fields=array("count(*) as likeCount");	
	$tables=array($this->albumphotolikes." as f");
	$where = array("albumphotoId='".$albumphotoId."'");
	$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(), $limit,$offset,0);
	$result= $this->FetchRow($result1); 
	return $result['likeCount'];	
	}

	function getAlbumPreviousPhotoByAlbumIdAndPhotoId($albumId,$userId,$iPid){
		$fields=array();	
		$tables=array($this->albumimage);
		$where=array("albumId='".$albumId."' AND id < ".$iPid." AND userId='".$userId."'");		
		$result1 = $this->SelectData($fields,$tables, $where, $order = array("id DESC"), $group=array(),$limit = "1",0,0); 
		$result= $this->FetchRow($result1); 
		return $result['id'];		
	}
	function getAlbumNextPhotoByAlbumIdAndPhotoId($albumId,$userId,$iPid){
		$fields=array();	
		$tables=array($this->albumimage);
		$where=array("albumId='".$albumId."' AND id > ".$iPid." AND userId='".$userId."'");		
		$result1 = $this->SelectData($fields,$tables, $where, $order = array("id ASC"), $group=array(),$limit = "1",0,0); 
		$result= $this->FetchRow($result1); 
		return $result['id'];		
	}
	function getAllCommentsByPhotoID($photoID){		
		$fields=array("pc.*,u.fname,u.lname,u.email,u.avatar,u.id,username");	
		$tables=array($this->albumimagecomment." as pc",$this->userprofile." as u");
		$where=array("u.id=pc.userID AND photoID='".$photoID."'");		
		$result1 = $this->SelectData($fields,$tables, $where, $order = array("pc.date DESC"), $group=array(),$limit = "",0,0); 
		$result= $this->FetchAll($result1); 
		return $result;			
	}	
	function addPhotoComment($array){			
		$this->InsertData($this->albumimagecomment,$array);		
		$insertId = mysql_insert_id();
		return $insertId;
	}
	function deletePhotoComment($cid,$iItemId){
		$sql = "DELETE FROM ".$this->albumimagecomment." WHERE pcID='".$cid."' AND photoID='".$iItemId."'";
		$result1= $this->ExecuteQuery($sql);	 
	}
	/******************************************Created By Gaurav Wayal For Admin**************************************************/

	function getPhotoAlbumCountByUserId($userId){
	$fields=array("count(*) as AlbumCount");	
	$tables=array($this->album);
	$where = array("userId='".$userId."'");
	$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(), $limit,$offset,0);
	$result= $this->FetchRow($result1); 
	return $result['AlbumCount'];	
	}
	function deleteMultiplePhotoAlbum($albumID) {
	$sql = "DELETE FROM ".$this->album." WHERE id IN (".$albumID.")";
	$result1= $this->ExecuteQuery($sql);	 
	}
	function getAllPhotosAlbumByuserId($userId,$search='',$orderField = 'title',$orderBy = 'ASC',$limit='',$offset='') {
	$fields = array();	
	$tables = array($this->album);
	$where=array("userId='".$userId."'");
	if($search != '' && $search != 'Search') {
	$where[] = "(title LIKE '%".$search."%')";
	}
	$order = array("$orderField $orderBy");
	$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(),$limit,$offset,0); 
	$result  = $this->FetchAll($result1); 	
	return $result;
	}
	function getPhotoCOunt($albumId) {
  	$fields=array("count(*) as cnt");	
  	$tables=array($this->albumimage);
  	$where = array("albumId ='".$albumId."' AND status!='1'");
  	$order = array("");
  	$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(), $limit='', $offset='0',0);
  	$result= $this->FetchRow($result1); 
  	return $result['cnt'];		
	}
	function getAllPhotosByuserIdandAlbumId($userId,$albumId,$search='',$orderField = 'title',$orderBy = 'ASC',$limit='',$offset='') {
	$fields = array();	
	$tables = array($this->albumimage);
	$where=array("userId='".$userId."' AND albumId='".$albumId."'");
	$order = array("$orderField $orderBy");
	$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(),$limit,$offset,0); 
	$result  = $this->FetchAll($result1); 	
	return $result;
	}
	function getPhotoCommentCount($photoId,$userId) {
  	$fields=array("count(*) as cnt");	
  	$tables=array($this->albumimagecomment);
  	$where = array("photoID ='".$photoId."' AND userID ='".$userId."'");
  	$order = array("");
  	$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(), $limit='', $offset='0',0);
  	$result= $this->FetchRow($result1); 
  	return $result['cnt'];		
	}
	function deleteMultiplePhotoLike($photolikeID) {
	$sql = "DELETE FROM ".$this->albumphotolikes." WHERE id IN (".$photolikeID.")";
	$result1= $this->ExecuteQuery($sql);	 
	}
	function getAllVideosLikeByuserIdandphotoId($albVidId, $userProfileId,$limit='',$offset='') {
	$fields = array();	
	$tables = array($this->albumphotolikes);
	$where=array("albumphotoId ='".$albVidId."' and photolikeProfileId ='".$userProfileId."'");
	$result1 = $this->SelectData($fields,$tables, $where, $order=array(), $group=array(),$limit,$offset,0); 
	$result  = $this->FetchAll($result1); 	
	return $result;
	}
	function deleteMultiplePhotoComment($videocommentID) {
	$sql = "DELETE FROM ".$this->albumimagecomment." WHERE pcID IN (".$videocommentID.")";
	$result1= $this->ExecuteQuery($sql);	 
	}
	function deletePhotoCommentbyCommentIs($Id) {
	$this->DeleteData($this->albumimagecomment,"pcID",$Id);
	}
	function getAllPhotosCommentByuserIdandphotoId($albVidId, $userProfileId,$limit='',$offset='') {
	$fields = array();	
	$tables = array($this->albumimagecomment);
	$where=array("photoID ='".$albVidId."' and userID ='".$userProfileId."'");
	$result1 = $this->SelectData($fields,$tables, $where, $order=array(), $group=array(),$limit,$offset,0); 
	$result  = $this->FetchAll($result1); 	
	return $result;
	}
}
?>