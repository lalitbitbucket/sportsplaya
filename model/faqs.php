<?php 
/**************************************************/
class Model_FAQ extends Database
{	
	function Model_FAQ() {	
		$this->faqs = FAQ;
		$this->Database();
	}
	
	// for add FAQ
	function addFaq($array) {
		$this->InsertData($this->faqs, $array);
	}
	
	//Function to get all FAQ categories
	function getAllActiveFaqById($id,$limit='',$offset='') {
	$fields = array();	
	$tables = array($this->faqs);

	$where  = array("status ='2'" );

	$sql    = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit,$offset,0); 
	$result = $this->FetchAll($sql); 
	return $result;
	}
	
	function getUserFaqByCatId($cat_id) {
		$sql     = "SELECT f.* FROM ".$this->faqs." as f WHERE  f.status = '2' ORDER BY f.id DESC";
	 	$result1 = $this->ExecuteQuery($sql);
		$result  = $this->FetchAll($result1);
		return $result;
	}

	function getAllFaqList() {
		 $sql     = "SELECT f.* FROM ".$this->faqs." as f WHERE f.status = '2' ORDER BY f.id DESC";
	 	$result1 = $this->ExecuteQuery($sql);
		$result  = $this->FetchAll($result1);
		return $result;
	}
	
	## List all Faqs
	function getAllFaqs($search,$search_faqcat,$limit='',$offset='') {
		$fields = array();	
		$tables = array($this->faqs);
		$where[] = "status <> '0'";
		if(trim($search)) {
			$where[] = "question LIKE '%".$search."%'";
		}

		$result1 = $this->SelectData($fields,$tables, $where , $order = array("updateDate DESC"), $group=array(),$limit,$offset,0); 
		$result  = $this->FetchAll($result1); 
		return $result;
	}

	## Get all Faq's List
	function getAllFaqss() {
		$fields = array();	
		$tables = array($this->faqs);
		$where[] = "status <> '0'";
		$result1 = $this->SelectData($fields,$tables, $where , $order = array("updateDate DESC"), $group=array(),$limit,$offset,0); 
		$result  = $this->FetchAll($result1); 
		return $result;
	}
	
	function getAllFaqsList() {
		$fields = array();	
		$tables = array($this->faqs);
		$result1 = $this->SelectData($fields,$tables, $where = array("status='1'"), $order = array(), $group=array(),$limit,$offset,0); 
		$result  = $this->FetchAll($result1); 
		return $result;
	}
	
	## Get faq details by id
	function getFaqDetailsById($id) {
		$fields = array();	
		$tables = array($this->faqs);
		$result1 = $this->SelectData($fields,$tables, $where= array("id='$id'"), $order = array(), $group=array(),$limit = "",0,0); 
		$result  = $this->FetchRow($result1); 
		return $result;
	}
	
	## Edit FAQ by id
	function editFaqById($array, $Id){
		$this->UpdateData($this->faqs,$array,"id",$Id,0);
	}
	
	## Update faq status with multiple ids
	function updateFaqStatus($ids, $status) {
		$sql = "UPDATE ".$this->faqs." SET status='".$status."' WHERE id IN (".$ids.")";
		$result1= $this->ExecuteQuery($sql);	 
		
	}
	
	## Delete faq with multiple ids
	function deleteFaq($ids, $status) {
		$sql = "DELETE FROM ".$this->faqs." WHERE id IN (".$ids.")";
		$result1= $this->ExecuteQuery($sql);	 
	}
	
	## Delete faq cat by id
	function deleteFaqById($id){
		$this->DeleteData($this->faqs,"id",$id);
	}

	
}
?>