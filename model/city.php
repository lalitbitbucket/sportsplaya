<?php
/**************************************************/
## Class Name - Model_City (Contains all the functions related city and city profile)
/**************************************************/
class Model_City extends Database 
{	
	## Constructor
	function Model_City() {
		$this->city = CITY;
		$this->states = STATES;
		$this->country = COUNTRY;
		$this->Database();
	}	
	
	## Add city in database
	function addCityByValue($Array) {
		$this->InsertData( $this->city , $Array );		
		$insertId = mysql_insert_id();
		return $insertId;
	}
		
	## Edit city by cityid
	function editCityValueById($array, $Id){
		$this->UpdateData($this->city,$array,"ctID",$Id,0);
	}
	
	## Delete city by city id
	function deleteCityValueById($id){
		$sql = "UPDATE ".$this->city." SET ctStatus='0' WHERE ctID='".$id."'";
		$result1= $this->ExecuteQuery($sql);	 
	}
	
	## List all country
	function getCity($search='', $limit='', $offset='') {
		$fields = array();	
		$tables = array($this->city);		
		$where = array("ctStatus!='0'");
		if(addslashes(trim($search))) {
			$where[] = " ctName LIKE '%".$search."%' ";			
		}
		$result1 = $this->SelectData($fields,$tables, $where, $order = array("ctName"), $group=array(),$limit,$offset,0); 
		$result  = $this->FetchAll($result1); 
		return $result;
	}

	## List all City
	function getAllCity($searchindex='',$search_country='',$search_state='', $limit='', $offset='') {
		$fields = array("c.*, s.statName, cntr.countryName");
		$tables = array($this->city." as c",$this->states." as s",$this->country." as cntr");
		$where = array("c.statID=s.statID AND c.cntrID=cntr.countryId AND c.ctStatus <> '0'");
		if($searchindex!='' && $searchindex!='City') {	
			$where[]= "c.ctName LIKE '%".$searchindex."%'";
		} 
		if($search_country != '') {
			$where[] = "c.ctID = '".$search_country."'";
		}
		if($search_state != '' && $search_state!='Select State') {
			$where[] = "c.statID = '".$search_state."'";
		}
		$result1 = $this->SelectData($fields,$tables, $where, $order = array("c.ctName ASC"), $group=array(),$limit,$offset,0);  
		$result  = $this->FetchAll($result1); 
		return $result;
	}
	
	## Get all active city
	function getAllActiveCity() {
		$fields=array();	
		$tables=array($this->city);
		$where = array("ctStatus = '2'");
		$result1 = $this->SelectData($fields,$tables, $where, $order=array('ctName ASC'), $group=array(), $limit='',$offset='',0);
		$result= $this->FetchAll($result1); 
		return $result;		
	}
	
	## Get all active city by state id
	function getAllActiveCityByStateId($statID) {
		$fields=array();	
		$tables=array($this->city);
		$where = array("statID = '".$statID."' AND ctStatus = '2' ");
		$result1 = $this->SelectData($fields,$tables, $where, $order=array('ctName ASC'), $group=array(), $limit='',$offset='',0);
		$result= $this->FetchAll($result1); 
		return $result;		
	}		
	
	## Get city details by city id
	function getCityDetailsByCityId($id) {
		$fields=array();	
		$tables=array($this->city);
		$where=array("ctID='".$id."'");		
		$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 
		$result= $this->FetchRow($result1); 
		return $result;		
	}
	
	## Update City status with multiple Cityids
	function updateMultipleCityStatus($Cityids, $status) {
		$sql = "UPDATE ".$this->city." SET ctStatus='".$status."' WHERE ctID IN (".$Cityids.")";
		$result1= $this->ExecuteQuery($sql);	 
	}
	
	## Delete City category with multiple Cityids
	function deleteMultipleCity($Cityids) {
		$sql = "UPDATE ".$this->city." SET ctStatus='0' WHERE ctID IN (".$Cityids.")";
		$result1= $this->ExecuteQuery($sql);	 
	}
	## Update City status with multiple Cityids
	function updateCityStatus($Cityid, $status) {		
		$sql = "UPDATE ".$this->city." SET ctStatus='".$status."' WHERE ctID = (".$Cityid.")";
		$result1= $this->ExecuteQuery($sql);	 
	}
	
	## checking city name exit or not written by sandeep shirsat 23 aug 2011
	function checkCityName($cityname, $id='') {
		$fields = array();	
		$tables = array($this->city);
		$where= array("ctName='".$cityname."' AND ctStatus!='0'");
		if($id != '') {
			$where[] = " ctID!='".$id."'";	
		}
		$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 
		$result  = $this->FetchRow($result1); 
		return $result;
	}
	
	## Edit City by Cityid
	function editCityById($array, $Cityid){
		$this->UpdateData($this->city,$array,"ctID",$Cityid,0);
	}

	## Get faq details by Cityid
	function getCityDetailsById($Cityid) {
		$fields = array();	
		$tables = array($this->city);
		$result1 = $this->SelectData($fields,$tables, $where= array("ctID='".$Cityid."'"), $order = array(), $group=array(),$limit = "",0,0); 
		$result  = $this->FetchRow($result1); 
		return $result;
	}

	## Get all active city by Country id
	function getAllActiveCityByCountryId($countryId) {
		$fields=array("ctName,ctID");	
		$tables=array($this->city);
		$where = array("cntrID = '".$countryId."' AND ctStatus = '2' ");
		$result1 = $this->SelectData($fields,$tables, $where, $order=array('ctName ASC'), $group=array(), $limit='',$offset='',0);
		$result= $this->FetchAll($result1); 
		return $result;		
	}		
}
?>