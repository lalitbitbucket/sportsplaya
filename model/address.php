<?php
/**************************************************/
## Class Name - Model_Users (Contains all the functions related user and user profile)
/**************************************************/
class Model_Address extends Database 
{	
	## Constructor
	function Model_Address() {		
		$this->address = ADDRESS_BOOK;
		$this->shipping_address = SHIPPINGADDRESS;			
		$this->Database();
	}	
	
	## Add address in address book table
	function addAddress($Array) {
		$this->InsertData( $this->address, $Array );		
		$insertId = mysql_insert_id();
		return $insertId;
	}
	
	function addShippingAddress($Array) {	
		$this->InsertData( $this->shipping_address , $Array );		
		$insertId = mysql_insert_id();
		return $insertId;
	}	
	
	function getAllAddressByUserId($id) {
			
		$fields=array();	
		$tables=array($this->address);
		$where = array("uID = '".$id."'");
		$result1 = $this->SelectData($fields,$tables, $where, $order=array("isDefault"), $group=array(), $limit='',$offset='',0);
		$result= $this->FetchAll($result1); 
		return $result;		
	}
	function getShippingAddressById($id) {
		$fields = array();	
		$tables = array($this->shipping_address);
		$result1 = $this->SelectData($fields,$tables, $where= array("addressId='".$id."'"), $order = array(), $group=array(),$limit = "",0,0); 
		$result  = $this->FetchRow($result1); 
		return $result;
	}
	
		function getShippingAddressByShipID($id) {
		$fields = array();	
		$tables = array($this->address);
		$result1 = $this->SelectData($fields,$tables, $where= array("addressId='".$id."'"), $order = array(), $group=array(),$limit = "",0,0); 
		$result  = $this->FetchRow($result1); 
		return $result;
	}
	
	function makeAddressDefault($id,$userId)
	{
		$sql1 = "UPDATE ".$this->address." SET isDefault='n' WHERE  uID= '".$userId."'";
		$result11= $this->ExecuteQuery($sql1);	 		
		
		$sql = "UPDATE ".$this->address." SET isDefault='y' WHERE  addressId= '".$id."'";
		$result1= $this->ExecuteQuery($sql);	 
	}
	function deleteAddress($id)
	{
		$this->DeleteData($this->address,"addressId",$id,0);
	}
	## Edit addres by id
	function editAddressBookById($array, $Id){
		$this->UpdateData($this->address,$array,"addressId",$Id,0);
	}
	## fetch the row
	function getAddressBookById($id) {
		$fields = array("");	
		$tables = array($this->address);
		$result1 = $this->SelectData($fields,$tables, $where= array("addressId='".$id."'"), $order = array(), $group=array(),$limit = "",0,0); 
		$result  = $this->FetchRow($result1); 
		return $result;
	}	
	function editDefaultAddress($contactEmail,$contactName,$contactAddress,$country,$contactState,$contact,$uID){
		$sql1 = "UPDATE ".$this->address." SET contactName='".$contactName."',contactEmail='".$contactEmail."',contactAddress='".$contactAddress."',country='".$country."',contactState='".$contactState."',contact='".$contact."'  WHERE  uID= '".$uID."' AND isDefault='y' ";
		$result11= $this->ExecuteQuery($sql1);	 		
	}
}
?>
