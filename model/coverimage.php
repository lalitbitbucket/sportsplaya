<?php
/**************************************************/
## Class Name - Model_coverimage (Contains all the functions related Coverimage)
/**************************************************/
class Model_Coverimage extends Database 
{
	## Constructor
	function Model_Coverimage() {
		$this->coverimage = COVERIMAGE;
		$this->Database();
	}

	## Add coverimage in database
	function addNewCoverimage($Array) {
		$this->InsertData($this->coverimage , $Array );		
		$insertId = mysql_insert_id();
		return $insertId;
	}	

	## Edit coverimage by id
	function editCoverimageValueById($array, $Id){
		$this->UpdateData($this->coverimage,$array,"id",$Id,0);
	}

	## Delete coverimage by id
	function deleteCoverimageValueById($id){
		$this->DeleteData($this->coverimage,"id",$id);
	}

	## Update coverimage status with multiple ids
	function updateCoverimageStatus($ids, $status) {
		$sql = "UPDATE ".$this->coverimage." SET status = '".$status."' WHERE id IN (".$ids.")";
		$result1= $this->ExecuteQuery($sql);	 
	}

	function editimageByValue($postion,$id){
		$sql = "UPDATE ".$this->coverimage." SET status = '1' WHERE id !='".$id."' AND coverimageposition='".$postion."'";
		$result1= $this->ExecuteQuery($sql);
	}

	## Delete coverimage with multiple ids
	function deleteCoverimage($ids) {
		$sql = "DELETE FROM ".$this->coverimage." WHERE id IN (".$ids.")";
		$result1= $this->ExecuteQuery($sql);	 
	}

	## Getting all categories from the datbase
	function getAllActiveCoverimage(){
		$fields   = array();	
		$tables   = array($this->coverimage);
		$where    = array("status = '2' ");		
		$result1  = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = 0,0,0); 
		$result   = $this->FetchAll($result1); 
		return $result;	
	}
		
	## fetch active random coverimage
	function getRandomActiveCoverimage($pos='', $limit='') {
		$fields=array();	
		$tables=array($this->coverimage);
		$where=array("status='2'");
		if($pos!=''){
			$where[] = "coverimagePosition = '".$pos."'";
		}		
		$result1 = $this->SelectData($fields,$tables, $where, $order = array("RAND()"), $group=array(),$limit,0,0); 
		$result= $this->FetchRow($result1); 
		return $result;	
	}

	## Getting all coverimage from the datbase
	function getAllCoverimageForCoverimage($search, $orderField = 'usertype', $orderBy = 'DESC', $limit='',$offset=''){
		$fields   = array();	
		$tables   = array($this->coverimage);
		$where    = array("status !='0'");
		if($search!='')
		{
		    $where[] = "usertype LIKE '%".$search."%'";
		}
		$order = array("$orderField $orderBy");
		$result1  = $this->SelectData($fields,$tables, $where, $order, $group=array(),$limit,$offset,0); 
		$result   = $this->FetchAll($result1); 
		return $result;	
	}

	## Fetch Cover Image Details based on usertype & gender.
	function getCoverImageDetailsUsingUserTypeAndGender($uType,$uGender) {
	$fields=array();	
	$tables=array($this->coverimage);
	$where = array("usertype='".$uType."'" ,"gender='$uGender'");
	$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 
	$result= $this->FetchRow($result1); 
	return $result;		
	}

	## Getting coverimage detail by coverimage id
	function getCoverimageDetailById($Id){
	$fields   = array("");	
	$tables   = array($this->coverimage);
	if($Id!='')
	{
	$where    = array("id = '".$Id."'");
	}
	$result1  = $this->SelectData($fields,$tables, $where, $order = array(""), $group=array(),$limit = 0,0,0); 
	$result   = $this->FetchRow($result1); 
	return $result;	
	}

	function getRandomAddsByPageID($Id){
	$fields   = array("");	
	$tables   = array($this->coverimage);
	if($Id!='')
	{
	$where    = array("pages LIKE  ('%2,3%') AND status = '2' ");
	}
	$result1  = $this->SelectData($fields,$tables, $where, $order = array("rand()"), $group=array(),$limit = 0,0,0); 
	$result   = $this->FetchRow($result1); 
	return $result;	
	}

	## Get coverimage image by id
	function getCoverimageImageByid($id) {
		$fields=array("id,coverimageImage,coverimagePosition");	
		$tables=array($this->coverimage);
		$where=array("id='".$id."'");		
		$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 
		$result= $this->FetchRow($result1); 
		return $result;		
	}

	## Getting bottom coverimage
	function getAllActiveCoverimageByValue($position){
		$fields   = array();	
		$tables   = array($this->coverimage);
		$where    = array("status = '2' AND coverimagePosition LIKE '%".$position."%' ");		
		$result1  = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = 0,0,0); 
		$result   = $this->FetchRow($result1); 
		return $result;	
	}	
}
?>