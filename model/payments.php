<?php
/**************************************************/
## Class Name - Model_Users (Contains all the functions related user and user profile)
/**************************************************/
class Model_Payments extends Database 
{	
	## Constructor
	function Model_Payments() {
	$this->payments = PAYMENTS;	
	$this->payments_options =PAYMENTSOPTIONS;
	$this->Database();
	}

	function getPaymentDetailsById($id)
	{
	$fields=array();	
	$tables=array($this->payments);
	$where = array("id ='".$id."' ");
	$result1 = $this->SelectData($fields,$tables, $where, $order=array('name ASC'), $group=array(), $limit='',$offset='',0);
	$result= $this->FetchRow($result1); 
	return $result;	
	}
	
	function getAllPayments() {
	$fields=array();	
	$tables=array($this->payments);
	$where = array("status <> '0' ");
	$result1 = $this->SelectData($fields,$tables, $where, $order=array('name ASC'), $group=array(), $limit='',$offset='',0);
	$result= $this->FetchAll($result1); 
	return $result;		
	}

	function getAllActivePayments() {
	$fields=array();	
	$tables=array($this->payments);
	$where = array("status ='2' ");
	$result1 = $this->SelectData($fields,$tables, $where, $order=array('id ASC'), $group=array(), $limit='',$offset='',0);
	$result= $this->FetchAll($result1); 
	return $result;		
	}	

	function getPaymentsOptionsById($id)
	{
	$fields=array();	
	$tables=array($this->payments_options);
	$where = array("paymentId='".$id."' and status <> '0' ");
	$result1 = $this->SelectData($fields,$tables, $where, $order=array('title ASC'), $group=array(), $limit='',$offset='',0);
	$result= $this->FetchAll($result1); 
	return $result;	
	}
	
	function editPaymentOptionsById($array, $Id)
	{
	   $this->UpdateData($this->payments_options,$array,"id",$Id,0);	
	}
	function editPaymentById($array, $Id)
	{
	   $this->UpdateData($this->payments,$array,"id",$Id,0);	
	}
}
?>
