<?php
class Model_News extends Database 
{	
	## Constructor
	function Model_News() {
		$this->News = NEWS;
		$this->Newsclick = NEWSCLICK;
		$this->Database();
	}
	
	## Add News in database
	function addNewsByValue($Array) {
		$this->InsertData( $this->News , $Array );		
		$insertId = mysql_insert_id();
		return $insertId;
	}

	## Edit News by Id
	function editNewsValueById($array, $Id){
		$this->UpdateData($this->News,$array,"id",$Id,0);
	}
	
	## Update News status by Id 
	function updateNewstatus($NewsID, $status) {		
		$sql = "UPDATE ".$this->News." SET status='".$status."' WHERE id = '".$NewsID."'";
		$result1= $this->ExecuteQuery($sql);	 
	}
	
	## Delete News by Id
	function deleteNewsValueById($NewsID){
		$this->DeleteData($this->News,"id",$NewsID);
	}

	## Delete Training by id
	function deleteNews($NewsID) {
		$sql = "UPDATE ".$this->News." SET status='0' WHERE id = '".$NewsID."'";
		$result1= $this->ExecuteQuery($sql);
	}

	## Update News status with multiple Ids
	function updateMultipleNewstatus($NewsID, $status) {
		$sql = "UPDATE ".$this->News." SET status='".$status."' WHERE id IN (".$NewsID.")";
		$result1= $this->ExecuteQuery($sql);	 
	}
	
	## Delete training with multiple Ids
	function deleteMultipleNews($NewsID) {
		$sql = "DELETE FROM ".$this->News." WHERE id IN (".$NewsID.")";
		$result1= $this->ExecuteQuery($sql);	 
	}
	
	## Get News details by News id
	function getNewsDetailsById($NewsID) {
		$fields=array();	
		$tables=array($this->News);
		$where=array("id='".$NewsID."'");		
		$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 
		$result= $this->FetchRow($result1); 
		return $result;		
	}
	
    ## Get News Details By Guid
    function getNewsDetailsByguId($guid) {
		$fields=array();	
		$tables=array($this->News);
		$where=array("guid='".$guid."'");		
		$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 
		$result= $this->FetchRow($result1); 
		return $result;		
	}


	## Get all News
	function getAllNews($search='', $orderField = 'title', $orderBy = 'ASC', $limit='',$offset='') {
		$fields=array();	
		$tables=array($this->News);
		$where = array("status!='0'");
		if($search != '' && $search != 'Search') {
			$where = array("title LIKE '%".$search."%' ");
		} 
		$order = array("$orderField $orderBy");
		$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(), $limit,$offset,0);
		$result= $this->FetchAll($result1); 
		return $result;		
	}
	
	## Get News image by Id
	function getNewsImageByNewsId($NewsID) {
		$fields=array("id,image");	
		$tables=array($this->News);
		$where=array("id='".$NewsID."'");		
		$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 
		$result= $this->FetchRow($result1); 
		return $result;		
	}
	function getNews($limit=0) {

		$fields=array("*");	
		$tables=array($this->News);
		$where = array("status!='0'");
		$order = array("id DESC");
		$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(),$limit,0); 
		$result= $this->Fetchall($result1); 
		return $result;		
	}
	
	## Checking wheather user enetered News name is already exists or not 
	function checkNewsNameExists($name,$NewsID){
		$fields = array();
		$tables = array($this->News);
		if($NewsID) {
			$where  = array("LOWER(title) = '".$name."' AND id != '".$NewsID."' AND status!='0' ");
		} else {
			$where  = array("LOWER(title) = '".$name."' AND status!='0'");
		}
		$result = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 
		$result1 = $this->FetchRow($result);
		return $result1;
	}
	function isNewsExists($title)
	{
		$fields=array("count(*) as cnt");	
		$tables=array($this->News);
		$where = array("title = '".$title."' ");
		$order = array();
		$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(), $limit, $offset,0);
		$result= $this->FetchRow($result1); 
		return $result['cnt'];		
		
	}
	function getNewsByRss()
	{
		$data=file_get_contents("http://www.espnfc.com/team/australia/628/rss");
		$array = $this->xml2array($data);
		$item_array = $array['rss']['channel']['item'];
		return 		$item_array;
		}
	## Get all random News by category id
	function getAllRandomNewsBySportsIds($sportId='', $limit='',$offset=0) {
		$fields=array();	
		$tables=array($this->News);
		$where = array("status='2' AND sportIds IN (".$sportId.")");
		$order = array("RAND()");
		$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(),$limit = "",0,0); 
		$result= $this->FetchAll($result1); 
		return $result;		
	}

	## Get all random News by category id
	function getOtherTraining() {
		$fields=array();	
		$tables=array($this->News);
		$where = array("status='2' AND  catID !='1' AND catID !='2'");
		$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(), $limit,$offset,0);
		$result= $this->FetchAll($result1); 
		return $result;		
	}

	## Get News
	function getRecentNews($search, $orderBy = 'DESC', $limit='',$offset='') {
		$fields=array();	
		$tables=array($this->News);
		$order = array("Id ".$orderBy);
		$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(), $limit="",$offset,0);
		$result= $this->FetchAll($result1); 
		return $result;		
	}

	## Get last five added News
	function lastAddedFiveNews() {
		$fields=array();	
		$tables=array($this->News);
		$where = array("status!='0'");
		$order = array("date DESC");
		$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(), $limit="5", $offset="0",0);
		$result= $this->FetchAll($result1); 
		return $result;		
	}
	
	## Get all News by user id
	function getAllNewsByUserId($uID, $limit='', $offset='0') {
		$fields=array();	
		$tables=array($this->News);
		$where = array("userId ='".$uID."' AND status!='0'");
		$order = array();
		$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(), $limit, $offset,0);
		$result= $this->FetchAll($result1); 
		return $result;		
	}
	
	function getNewsCountByUserId($uID, $limit='', $offset='0') {
		$fields=array("count(*) as cnt");	
		$tables=array($this->News);
		$where = array("userId = '".$uID."' AND status!='0'");
		$order = array();
		$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(), $limit, $offset,0);
		$result= $this->FetchRow($result1); 
		return $result['cnt'];		
	}

	## get all News except joined News
	function getAllOtherNews($NewsIDs,$userId) {
		$sql = "SELECT * FROM ".$this->News." WHERE id NOT IN(".$NewsIDs.") AND status='2' AND userId!='".$userId."' LIMIT 0,3";
		$result1= $this->ExecuteQuery($sql);
		$result= $this->FetchAll($result1); 
		return $result;	
	}
	
	## Get user's own News by user id
	function getMyNewsByUserId($uID,$search='', $orderField = 'name', $orderBy = 'ASC', $limit='',$offset='') {
		$fields=array();	
		$tables=array($this->News);
		$where = array("id = '".$uID."' AND status='2'");
		$order = array();
		$result1 = $this->SelectData($fields,$tables, $where, $order, $News=array(), $limit,$offset,0);
		$result= $this->FetchAll($result1); 
		return $result;		
	}
	## Get three random News
	function getRandomActiveNews($userID='',$limit='3', $offset='0') {
		$fields=array();	
		$tables=array($this->News);
		$where = array("(status='2')");
		if($userID!=''){
			$where[] = "(userId!='".$userID."')";
		}
		$order = array("RAND()");
		$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(), $limit, $offset,0);
		$result= $this->FetchAll($result1); 
		return $result;		
	}
	/************************************************************Function added by gaurav***********************************************************************/
   	function addNewshits($Array) {
		$this->InsertData( $this->Newsclick , $Array );		
		$insertId = mysql_insert_id();
		return $insertId;
	}
  function getNewsHitCountByNewsId($ID) {
		$fields=array("count(*) as cnt");	
		$tables=array($this->Newsclick);
		$where = array("newsId = '".$ID."'");
		$order = array();
		$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(), $limit, $offset,0);
		$result= $this->FetchRow($result1); 
		return $result['cnt'];		
	}
	function getUserIdByNewsId($NewsID) {
		$fields=array("userprofileId");	
		$tables=array($this->Newsclick);
		$where=array("newsId='".$NewsID."'");		
		$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 
		$result= $this->FetchRow($result1); 
		return $result;		
	}
	function getAllNewsHitUsername($NewsID, $limit='',$offset='') 
	{
		$fields=array();	
		$tables=array($this->Newsclick);
		$where = array("newsId='".$NewsID."'");
		$result1 = $this->SelectData($fields,$tables, $where, $order=array(), $group=array('userprofileId'), $limit,$offset,0);
		$result= $this->FetchAll($result1); 
		return $result;		
	}
	function deleteMultipleNewsHituser($NewsID) 
	{
		$sql = "DELETE FROM ".$this->Newsclick." WHERE id IN (".$NewsID.")";
		$result1= $this->ExecuteQuery($sql);	 
	}
	function deleteNewsHits($NewsID)
	{
		$this->DeleteData($this->Newsclick,"id",$NewsID);
	}
	 function getNewsHitCountByUserId($ID,$NewsID,$limit='', $offset='0') {
		$fields=array("count(*) as cnt");	
		$tables=array($this->Newsclick);
		$where = array("userprofileId = '".$ID."' AND newsId = '".$NewsID."'");
		$order = array();
		$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(), $limit, $offset,0);
		$result= $this->FetchRow($result1); 
		return $result['cnt'];		
	}
	
	
function xml2array($contents, $get_attributes=1, $priority = 'tag') {
    if(!$contents) return array();

    if(!function_exists('xml_parser_create')) {
        //print "'xml_parser_create()' function not found!";
        return array();
    }

    //Get the XML parser of PHP - PHP must have this module for the parser to work
    $parser = xml_parser_create('');
    xml_parser_set_option($parser, XML_OPTION_TARGET_ENCODING, "UTF-8"); # http://minutillo.com/steve/weblog/2004/6/17/php-xml-and-character-encodings-a-tale-of-sadness-rage-and-data-loss
    xml_parser_set_option($parser, XML_OPTION_CASE_FOLDING, 0);
    xml_parser_set_option($parser, XML_OPTION_SKIP_WHITE, 1);
    xml_parse_into_struct($parser, trim($contents), $xml_values);
    xml_parser_free($parser);

    if(!$xml_values) return;//Hmm...

    //Initializations
    $xml_array = array();
    $parents = array();
    $opened_tags = array();
    $arr = array();

    $current = &$xml_array; //Refference

    //Go through the tags.
    $repeated_tag_index = array();//Multiple tags with same name will be turned into an array
    foreach($xml_values as $data) {
        unset($attributes,$value);//Remove existing values, or there will be trouble

        //This command will extract these variables into the foreach scope
        // tag(string), type(string), level(int), attributes(array).
        extract($data);//We could use the array by itself, but this cooler.

        $result = array();
        $attributes_data = array();
        
        if(isset($value)) {
            if($priority == 'tag') $result = $value;
            else $result['value'] = $value; //Put the value in a assoc array if we are in the 'Attribute' mode
        }

        //Set the attributes too.
        if(isset($attributes) and $get_attributes) {
            foreach($attributes as $attr => $val) {
                if($priority == 'tag') $attributes_data[$attr] = $val;
                else $result['attr'][$attr] = $val; //Set all the attributes in a array called 'attr'
            }
        }

        //See tag status and do the needed.
        if($type == "open") {//The starting of the tag '<tag>'
            $parent[$level-1] = &$current;
            if(!is_array($current) or (!in_array($tag, array_keys($current)))) { //Insert New tag
                $current[$tag] = $result;
                if($attributes_data) $current[$tag. '_attr'] = $attributes_data;
                $repeated_tag_index[$tag.'_'.$level] = 1;

                $current = &$current[$tag];

            } else { //There was another element with the same tag name

                if(isset($current[$tag][0])) {//If there is a 0th element it is already an array
                    $current[$tag][$repeated_tag_index[$tag.'_'.$level]] = $result;
                    $repeated_tag_index[$tag.'_'.$level]++;
                } else {//This section will make the value an array if multiple tags with the same name appear together
                    $current[$tag] = array($current[$tag],$result);//This will combine the existing item and the new item together to make an array
                    $repeated_tag_index[$tag.'_'.$level] = 2;
                    
                    if(isset($current[$tag.'_attr'])) { //The attribute of the last(0th) tag must be moved as well
                        $current[$tag]['0_attr'] = $current[$tag.'_attr'];
                        unset($current[$tag.'_attr']);
                    }

                }
                $last_item_index = $repeated_tag_index[$tag.'_'.$level]-1;
                $current = &$current[$tag][$last_item_index];
            }

        } elseif($type == "complete") { //Tags that ends in 1 line '<tag />'
            //See if the key is already taken.
            if(!isset($current[$tag])) { //New Key
                $current[$tag] = $result;
                $repeated_tag_index[$tag.'_'.$level] = 1;
                if($priority == 'tag' and $attributes_data) $current[$tag. '_attr'] = $attributes_data;


            } else { //If taken, put all things inside a list(array)
                if(isset($current[$tag][0]) and is_array($current[$tag])) {//If it is already an array...

                    // ...push the new element into that array.
                    $current[$tag][$repeated_tag_index[$tag.'_'.$level]] = $result;
                    
                    if($priority == 'tag' and $get_attributes and $attributes_data) {
                        $current[$tag][$repeated_tag_index[$tag.'_'.$level] . '_attr'] = $attributes_data;
                    }
                    $repeated_tag_index[$tag.'_'.$level]++;

                } else { //If it is not an array...
                    $current[$tag] = array($current[$tag],$result); //...Make it an array using using the existing value and the new value
                    $repeated_tag_index[$tag.'_'.$level] = 1;
                    if($priority == 'tag' and $get_attributes) {
                        if(isset($current[$tag.'_attr'])) { //The attribute of the last(0th) tag must be moved as well
                            
                            $current[$tag]['0_attr'] = $current[$tag.'_attr'];
                            unset($current[$tag.'_attr']);
                        }
                        
                        if($attributes_data) {
                            $current[$tag][$repeated_tag_index[$tag.'_'.$level] . '_attr'] = $attributes_data;
                        }
                    }
                    $repeated_tag_index[$tag.'_'.$level]++; //0 and 1 index is already taken
                }
            }

        } elseif($type == 'close') { //End of tag '</tag>'
            $current = &$parent[$level-1];
        }
    }
    
    return($xml_array);
}  
	
	}
?>