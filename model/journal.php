<?php 

class Model_journal extends Database
{	
	function Model_JOURNAL() {	
		$this->journal = JOURNAL;
		$this->userprofile = USERPROFILE;
		$this->user = USERS;
        	$this->Database();
	}

	function getAlljournalByUserId($userId,$search, $orderField,$orderBy = 'ASC', $limit,$offset) {
        $fields=array("*");    
      
         $tables=array($this->journal);
        $where = array();
        $where[] = "status = '2'";  
        if($userId!='')
         {
                 $where[] = "userId='".$userId."'";    
         }
        if($search != '' && $search != 'Journal Name') {
        $where[] = "(jName LIKE '%".$search."%')";
        }
                
        // $order = array($orderField.' '.$orderBy);
          
        $result1 = $this->SelectData($fields,$tables, $where, $order, $group=array(), $limit,$offset,0);
        $result= $this->FetchAll($result1); 
        return $result;        
    }
    
    //  add journal in database
	function addJournal($JournalArray) {
		$this->InsertData($this->journal,$JournalArray);  
		 $insertId = mysql_insert_id();
		return $insertId;
	}
	
	
	function updateJournalDetails($Array,$Id)
	{
		$this->UpdateData($this->journal,$Array,"jID",$Id,0);
		$insertId = mysql_insert_id();
		return $insertId;
	}
	
	 // Delete Doctors
	 ## Delete user by userid
	function deleteJournalById($id){
		$this->DeleteData($this->journal,"jID",$id);
	}

	
	## Get doctor details
	## Get Journal Details by Journal Id
        public function getJournalDetailsById($id) {
		$fields=array();	
		$tables=array($this->journal);
		$where=array("jID='".$id."'");		
		$result1 = $this->SelectData($fields,$tables, $where, $order = array(), $group=array(),$limit = "",0,0); 
		$result= $this->FetchRow($result1); 
		return $result;		
	}
	## Update Journal status with multiple featIDs
	function updateJournalStatus($featIDs, $status) {
		$sql = "UPDATE ".$this->journal." SET status = '".$status."' WHERE jID IN (".$featIDs.")";
		$result1= $this->ExecuteQuery($sql);	 
	}
	## Edit Journal by featID
	function editJournalValueById($array, $Id){
		$this->UpdateData($this->journal,$array,"jID",$Id,0);
	}
	## Delete Journal by featID
	function deleteJournalValueById($featID){
		$sql = "UPDATE ".$this->journal." SET status = '0' WHERE jID ='".$featID."'";
		$result1= $this->ExecuteQuery($sql);
	}
	
	function getAllJournal($search,$orderField='jnote',$orderBy='ASC',$limit,$offset){
		$fields   = array();	
		$tables   = array($this->journal);
		$where    = array('status!="0"');	
		if($search!=''){
			$where[] = 'jnote LIKE "%'.$search.'%"';
		}
		$order = array("$orderField $orderBy");
		$result1  = $this->SelectData($fields,$tables, $where, $order, $group=array(),$limit,$offset,0); 
		$result   = $this->FetchAll($result1); 
		return $result;	
	}
 }
?>