<?php

class Model_Search extends Database 
{	
	## Constructor
	function Model_Search() {
		$this->user = USERS;
		$this->user_login = USERLOGIN;
		$this->user_url = USERURL; 
		$this->country = COUNTRY; 
		$this->state = STATES;	
		$this->city = CITY; 
		$this->club 		  = CLUB;
		$this->userprofile = USERPROFILE;
    	$this->profileavatars = PROFILEAVATARS;	
    	$this->experience = USEREXPERIENCE;	
    	$this->skills = SKILLSEXPERTISE;
    	$this->publicprofileimages = PUBLICPROFILEIMAGES;	
    	$this->bioinfo = BIOINFO;	        		
		$this->Database();
	}
	
#	function searchData($search='') 
#	{	
#		$where = " where status = '2' and userType <> '1' ";
#		if($search != '') 
#		{
#			$where.= "and (concat(fname,' ',lname) LIKE '%".$search."%' or username LIKE '%".$search."%')";
#		}
#		$sql = "select * FROM ".$this->userprofile."".$where;  
#		
#		$result1= $this->ExecuteQuery($sql); 
#		$result= $this->FetchAll($result1); 
#		return $result;	
#	}

	function searchCountData($search='',$searchSport='',$searchCountry='',$searchCategory='',$orderField='', $orderBy='', $limit='', $offset=0) {
	$fields=array("DISTINCT userid");
	$tables=array($this->userprofile. "");
	$where = array("status='2' AND userType!='1'");		
	if($search!= '') {
		$search_value = explode(' ', $search );		
		$parts = array();
		foreach($search_value as $search_value ){
			$parts[] = " (concat(fname,' ',lname) LIKE '".$search."%' OR fname LIKE '%".$search."%' OR lname LIKE '%".$search."%')";
		} 		
		$parts = implode(' OR ', $parts);
		$where[] ="(".$parts.")";
	}

	if($searchSport!= ''){
		$where[] = " sportsID IN  (".$searchSport.") ";	
	}

	if($searchCountry!= ''){
		$where[] = " country IN  (".$searchCountry.") ";	
	}

	if($searchCategory!= ''){
		$where[] = " userType IN  (".$searchCategory.") ";
	}

	$order = array("");
	$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array("userId"), $limit, $offset,0); 
	$result= $this->FetchAll($result1);
	return count($result);		
	}	


	function searchData($search='',$searchSport='',$searchCountry='',$searchCategory='',$orderField='', $orderBy='', $limit='', $offset=0) {
	$fields=array("");
	$tables=array($this->userprofile);
	$where = array("status='2' AND userType='3'");		
	if($search!= '')
	{
	$search_value = explode(' ', $search );		
	$parts = array();
	foreach($search_value as $search_value ){
	$parts[] = " (concat(fname,' ',lname) LIKE '".$search."%' OR fname LIKE '%".$search."%' OR lname LIKE '%".$search."%')";
	} 		
	$parts = implode(' OR ', $parts);
	$where[] ="(".$parts.")";
	}

	if($searchSport!= '')
	{
	$where[] = " sportsID IN  (".$searchSport.") ";	
	}

	if($searchCountry!= '')
	{
	$where[] = " country IN  (".$searchCountry.") ";	
	}

	if($searchCategory!= '')
	{
	$where[] = " userType IN  (".$searchCategory.") ";
	}

	$order = array();
	$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array("userId"), $limit, $offset,0); 
	$result= $this->FetchAll($result1);
		
	return $result;		
	}

	function searchClubData($search,$searchSport='',$selectCountry=0,$change_sport=0,$limit='', $offset='') {
	$fields=array("*");
	$tables=array($this->userprofile." AS u",$this->club." AS c");
	$where = array("u.status='2' AND u.userType='6' AND u.id=c.userID");
	if($search!= '')
	{
	$search_value = explode(' ', $search );		
	$parts = array();
	foreach($search_value as $search_value ){
	$parts[] = "c.clubName LIKE '".$search."%' OR c.clubName LIKE '%".$search."%'";
	} 		
	$parts = implode(' OR ', $parts);
	$where[] ="(".$parts.")";
	}

	if($searchSport > 0)
	{
	$where[] = " c.club_sport IN  (".$searchSport.") ";	
	}

	if($searchCity > 0)
	{
	$where[] = " u.city IN  (".$searchCity.") ";	
	}
	if($selectCountry > 0)
	{
	$where[] = " u.country IN  (".$selectCountry.") ";	
	}

	if($searchCategory > 0)
	{
	$where[] = " u.userType IN  (".$searchCategory.") ";
	}		
	$order = array();
	$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array("u.userId"), $limit, $offset,0); 
	$result= $this->FetchAll($result1);
	return $result;

	}


	function searchMoreData($search,$searchSport='',$selectCountry=0,$change_sport=0,$limit='', $offset='') {
	$fields=array("*");
	$tables=array($this->userprofile);
	$where = array("status='2' AND userType='3'");		
	if($search!= '')
	{
	$search_value = explode(' ', $search );		
	$parts = array();
	foreach($search_value as $search_value ){
	$parts[] = " (concat(fname,' ',lname) LIKE '".$search."%' OR fname LIKE '%".$search."%' OR lname LIKE '%".$search."%')";
	} 		
	$parts = implode(' OR ', $parts);
	$where[] ="(".$parts.")";
	}

	if($searchSport > 0)
	{
	$where[] = " sportsID IN  (".$searchSport.") ";	
	}

	if($searchCity > 0)
	{
	$where[] = " city IN  (".$searchCity.") ";	
	}
	if($selectCountry > 0)
	{
	$where[] = " country IN  (".$selectCountry.") ";	
	}

	if($searchCategory > 0)
	{
	$where[] = " userType IN  (".$searchCategory.") ";
	}

	$order = array("");
	$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array("userId"), $limit, $offset,0); 
	$result= $this->FetchAll($result1);
		
	return $result;		
	}



	
	/*********** Advance Search ***************/
	function advancedsearchData($search='',$position='',$searchSport='',$searchCity='',$gender='',$firstage='',$secondage='',$f_height='',$s_height='',$limit='', $offset='') {
		$fields=array("*");
		$tables=array($this->userprofile);
		$where = array("status='2' AND userType='3'");		
		
		if($search!= '')
		{
			$search_value = explode(' ', $search );		
			$parts = array();
			foreach($search_value as $search_value ){
				$parts[] = " (concat(fname,' ',lname) LIKE '".$search."%' OR fname LIKE '%".$search."%' OR lname LIKE '%".$search."%')";
			} 		
			$parts = implode(' OR ', $parts);
			$where[] ="(".$parts.")";
		}
		if($searchSport!=0)
		{
			$where[] = " sportsID IN  (".$searchSport.") ";	
		}
		/*if($searchHeight!=''){
			$where[] = " b.height='".$searchHeight."'";	
		}*/
		
		if($position!=''){
			$where[] = " sports_position='".$position."'";	
		}
		if($gender!=''){
			$where[] = " sex='".$gender."'";	
		}
		if($searchCity!=0)
		{
			$where[] = " city IN  (".$searchCity.") ";	
		}
		if($firstage!= '' || $secondage !='')
		{
			$aVal1 = $firstage;
			$aVal2 = $secondage;
			$sdate = date('Y-m-d', strtotime('-'.$aVal2.'year'));
			$edate = date('Y-m-d', strtotime('-'.$aVal1.'year'));
			
			$where[]= "birthdate BETWEEN '".$sdate."' AND '".$edate."'";
		}

		if($f_height!= '' || $s_height !='')
		{	
			$where[]= "height BETWEEN '".$f_height."' AND '".$s_height."'";
		}

		$order = array("");
		$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array('userId'), $limit, $offset,0);
		$result= $this->FetchAll($result1); 
		return $result;		
	}

	/*function advancedsearchData($search='',$position='',$searchSport='',$searchCity='',$gender='',$firstage='',$secondage='',$f_height='',$s_height='',$limit='', $offset='') {
		$fields=array("*");
		$tables=array($this->userprofile." as u ",$this->bioinfo." as b ");
		$where = array("u.status='2' AND u.userType='3' AND u.id=b.userId");		
		
		if($search!= '')
		{
			$search_value = explode(' ', $search );		
			$parts = array();
			foreach($search_value as $search_value ){
				$parts[] = " (concat(u.fname,' ',u.lname) LIKE '".$search."%' OR u.fname LIKE '%".$search."%' OR u.lname LIKE '%".$search."%')";
			} 		
			$parts = implode(' OR ', $parts);
			$where[] ="(".$parts.")";
		}
		if($searchSport!=0)
		{
			$where[] = " u.sportsID IN  (".$searchSport.") ";	
		}
		if($searchHeight!=''){
			$where[] = " b.height='".$searchHeight."'";	
		}
		
		if($position!=''){
			$where[] = " u.sports_position='".$position."'";	
		}
		if($gender!=''){
			$where[] = " u.sex='".$gender."'";	
		}
		if($searchCity!=0)
		{
			$where[] = " u.city IN  (".$searchCity.") ";	
		}
		if($firstage!= '' || $secondage !='')
		{
			$aVal1 = $firstage;
			$aVal2 = $secondage;
			$sdate = date('Y-m-d', strtotime('-'.$aVal2.'year'));
			$edate = date('Y-m-d', strtotime('-'.$aVal1.'year'));
			
			$where[]= "u.birthdate BETWEEN '".$sdate."' AND '".$edate."'";
		}

		if($f_height!= '' || $s_height !='')
		{	
			$where[]= "b.height BETWEEN '".$f_height."' AND '".$s_height."'";
		}

		$order = array("");
		$result1 = $this->SelectData($fields,$tables, $where, $order, $group=array('u.userId'), $limit, $offset,0);
		$result= $this->FetchAll($result1); 
		return $result;		
	}*/

	function getClubDataByClubId($clubId) {
	$fields=array("*");
	$tables=array($this->userprofile." AS u",$this->club." AS c");
	$where = array("u.status='2' AND u.userType='6' AND u.id=c.userID AND c.clubID='".$clubId."'");
	$order = array();
	$result1 = $this->SelectData($fields,$tables, $where, $order, $group='', $limit, $offset,0); 
	$result= $this->FetchRow($result1);
	return $result;
	}
}
?>
