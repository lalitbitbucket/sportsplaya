<?php

require_once 'config/settings.php';

$videosObj = new Model_videos();

$resultsPerPage = 4;

// $weeklyDate = getCurrentWeeklyDate();
$currentmonthyear = date('Y')."-".date('m');
$limit = $_POST['offset'];

if(isset($_POST['offset'])){

$sports_id = $_POST['sportsId'];
$change_sport = $_POST['change_sport'];	
    
//$albumArr = $videosObj->getAllVideo($orderField = 'added_date',$orderBy = 'DESC',4, $limit,$sports_id,$change_sport);
$albumArr = $videosObj->getNomineesVideoByWeekly($orderField = 'id', $orderBy = 'DESC',4, $limit,$sports_id,$change_sport,$startdate=$currentmonthyear,$enddate=$currentmonthyear);

$change_sport_flag =0 ;
$count_hidden = "";
if($change_sport!=0)
{
// $countalbumArr = $videosObj->getAllVideo($orderField = 'added_date',$orderBy = 'DESC','', '',$sports_id,$change_sport);
    $countalbumArr = $videosObj->getNomineesVideoByWeekly($orderField = 'added_date', $orderBy = 'DESC','', '',$sports_id,$change_sport,$startdate=$currentmonthyear,$enddate=$currentmonthyear);

    $total_change_sport_flag  = count($countalbumArr);
    $count_hidden = '<input type="hidden" name="hidden_total" id="hidden_total" value="'.$total_change_sport_flag.'">';
}

if($count_hidden!="")
{
    echo $count_hidden;
}
    
    foreach($albumArr as $albumDetails) {
        $new_class = '';
		$getAlbumVideos = $videosObj->getAllAlbumVideosByAlbumId($albumDetails['albumId']); 
		if($albumDetails['video_type']=='vimeo')
		{
		 	$albumDetails['vcode'] = str_replace("\\","",$albumDetails['vcode']);
            $video_url = explode('"', $albumDetails['vcode']);
            $image_url = $albumDetails['vimeo_video_image'];
            $new_class = 'new_class';
            $url = $video_url[1];
            
		}
        if($albumDetails['video_type']=='youtube')
        {
            $albumDetails['vcode'] = str_replace("\\","",$albumDetails['vcode']);
            preg_match('/src="([^"]+)"/', $albumDetails['vcode'], $match);
            $url = $match[1];
            $arr = explode("/",$url);
            $url= $arr[count($arr)-1];
            $image_url ="https://i.ytimg.com/vi/".$url."/mqdefault.jpg";  
            $url = $albumDetails['vcode'];
        }
		if($albumDetails['video_type']=='youtubeurl')
		{
            $youtube_url = $albumDetails['vccode'];
            $albumDetails['vccode'] = str_replace("\\","",$albumDetails['vccode']);
            $regex_pattern = "/(youtube.com|youtu.be)\/(watch)?(\?v=)?(\S+)?/";
            //$match;
            if(preg_match($regex_pattern, $youtube_url, $match)){
                $image_url ="https://i.ytimg.com/vi/".$match[4]."/mqdefault.jpg"; 
                $url = 'https://www.youtube.com/watch?v='. $match[4];
            }
		   	/*$albumDetails['vccode'] = str_replace("\\","",$albumDetails['vccode']);
		   	$arr = explode("=",$albumDetails['vccode']);
		  	$url= $arr[count($arr)-1];
			$image_url ="https://i.ytimg.com/vi/".$url."/mqdefault.jpg";
            $url = $albumDetails['vccode'];*/	
		}
        if($albumDetails['video_type']=='video')
        {
            $file = explode('.', $albumDetails['video']);
            $video_image = $file[0];
            $image_url = SITE_URL.'/dynamicAssets/videos/thumbnail/'.$video_image.'.jpg';
            $url = SITE_URL.'/dynamicAssets/videos/'.$albumDetails['video'];
        }

        if($albumDetails['sportsid']!=0){
            $sports_arr = $sportsObj->getSportsDetailById($albumDetails['sportsid']);
            $sports_name = $sports_arr['sportsTitle'];
        }
        else{
            $sports_name =  $albumDetails['sportsTitle'];
        }

		$total_voting = $videosObj->getVotingPercentByVideoId($albumDetails['uId']);
		
		$userRecordsInfo = $usersObj->getRecordsDetailsByUserId($getAlbumVideos['user_id']);
		
		$getAlbumDtl = $videosObj->getAlbumDetailsByAlbumId($albumDetails['albumId']);
		
		$albumname  	    = stripcslashes($getAlbumDtl['title']);
		$imageCode  	    = substr($getAlbumVideos[0]['vccode'],16); 
		$fname  	    = $albumDetails['fname'];
		$lname  	    = $albumDetails['lname'];
		$uId  	    = $albumDetails['uId'];
        $sportsid          = $albumDetails['sportsid'];
        $str_rating  	    = $total_voting['voteper'];
		
        echo'<li><a href="'.SITE_URL.'/nominee_popuppage.php?uId='.$uId.'" class="ajax-popup-link play_thumb">
                <img src="'.SITE_URL.'/siteAssets/images/newimages/play_list_play_icon.png" class="play_link"/>
                <img src="'.$image_url.'" class="home_video_image '.$new_class.'" height="252" width="336"/></a>
                <p class="title"><strong>'.stripcslashes(substr($albumname,0,20).'....').'</strong></p>
                <p class="author">'.$fname.''.$lname.'</p>
                <p class="cat-desc">'.$sports_name.' • ';
                        if($albumDetails['date_diff'] <= 0){ echo 'Just an hour ago'; }
                        else{ echo $albumDetails['date_diff'].'days ago'; }
        echo '</p><p>';
                    if ($str_rating==0){
                        echo   '<img src="'.SITE_URL.'/siteAssets/images/newimages/unrate.png" class="pull-left" />
                                <img src="'.SITE_URL.'/siteAssets/images/newimages/unrate.png" class="pull-left" />
                                <img src="'.SITE_URL.'/siteAssets/images/newimages/unrate.png" class="pull-left" />
                                <img src="'.SITE_URL.'/siteAssets/images/newimages/unrate.png" class="pull-left" />
                                <img src="'.SITE_URL.'/siteAssets/images/newimages/unrate.png" class="pull-left" />';
                    }
                    elseif($str_rating <=20){
                        echo   '<img src="'.SITE_URL.'/siteAssets/images/newimages/rate.png" class="pull-left" />
                                <img src="'.SITE_URL.'/siteAssets/images/newimages/unrate.png" class="pull-left" />
                                <img src="'.SITE_URL.'/siteAssets/images/newimages/unrate.png" class="pull-left" />
                                <img src="'.SITE_URL.'/siteAssets/images/newimages/unrate.png" class="pull-left" />
                                <img src="'.SITE_URL.'/siteAssets/images/newimages/unrate.png" class="pull-left" />';
                    }
                    elseif($str_rating <=40){
                        echo   '<img src="'.SITE_URL.'/siteAssets/images/newimages/rate.png" class="pull-left" />
                                <img src="'.SITE_URL.'/siteAssets/images/newimages/rate.png" class="pull-left" />
                                <img src="'.SITE_URL.'/siteAssets/images/newimages/unrate.png" class="pull-left" />
                                <img src="'.SITE_URL.'/siteAssets/images/newimages/unrate.png" class="pull-left" />
                                <img src="'.SITE_URL.'/siteAssets/images/newimages/unrate.png" class="pull-left" />';
                    }
                    elseif($str_rating <=60){
                        echo   '<img src="'.SITE_URL.'/siteAssets/images/newimages/rate.png" class="pull-left" />
                                <img src="'.SITE_URL.'/siteAssets/images/newimages/rate.png" class="pull-left" />
                                <img src="'.SITE_URL.'/siteAssets/images/newimages/rate.png" class="pull-left" />
                                <img src="'.SITE_URL.'/siteAssets/images/newimages/unrate.png" class="pull-left" />
                                <img src="'.SITE_URL.'/siteAssets/images/newimages/unrate.png" class="pull-left" />';
                    }
                    elseif($str_rating <=80){
                        echo   '<img src="'.SITE_URL.'/siteAssets/images/newimages/rate.png" class="pull-left" />
                                <img src="'.SITE_URL.'/siteAssets/images/newimages/rate.png" class="pull-left" />
                                <img src="'.SITE_URL.'/siteAssets/images/newimages/rate.png" class="pull-left" />
                                <img src="'.SITE_URL.'/siteAssets/images/newimages/rate.png" class="pull-left" />
                                <img src="'.SITE_URL.'/siteAssets/images/newimages/unrate.png" class="pull-left" />';
                    }
                    else{
                        echo   '<img src="'.SITE_URL.'/siteAssets/images/newimages/rate.png" class="pull-left" />
                                <img src="'.SITE_URL.'/siteAssets/images/newimages/rate.png" class="pull-left" />
                                <img src="'.SITE_URL.'/siteAssets/images/newimages/rate.png" class="pull-left" />
                                <img src="'.SITE_URL.'/siteAssets/images/newimages/rate.png" class="pull-left" />
                                <img src="'.SITE_URL.'/siteAssets/images/newimages/rate.png" class="pull-left" />';
                    }
                echo '</p></li>';
	}
}
?>